﻿//'use strict';

/* App Module */

var url = "staging"; // dev || staging || prod || offline

var hCueDoctorWebApp = angular.module('hCueDoctorWebApp', [
    'ngRoute',
    'hCueDoctorWebAnimations',
    'hCueDoctorWebControllers',
    'hCueDoctorWebFilters',
    'hCueDoctorWebServices',
    'hCueDoctorPrintServices',
    'LocalStorageModule',
    'ngCookies',
    'ngSanitize',
    'pouchdb',
    'ngAnimate',
    'ui.bootstrap',
    'cfp.loadingBar',
    'bootstrapLightbox',
    'ngAutocomplete',
    'ngTouch',
    'nvd3',
    'ui.calendar',
    'ngMap',
    'ngImgCrop',
    'webcam',
    'colorpicker.module',
    'ui-rangeSlider',
    'ngSanitize',
    'ui.select',
    'ngAlertify',
    'ui.bootstrap.datetimepicker'
]);

// hCueDoctorWebApp.config(function($httpProvider) {
//     $httpProvider.interceptors.push('AuthInterceptor');
// });

// hCueDoctorWebApp.factory('AuthInterceptor', function($q, $location, AuthToken) {

//     var interceptorFactory = {};

//     interceptorFactory.request = function(config) {

//         var token = AuthToken.getToken();

//         if (token) {
//             config.headers.Authorization = token;
//         }

//         return config;
//     }

//     interceptorFactory.responseError = function(response) {
//         if (response.status == 401) {
//             AuthToken.setToken();
//             alert('Your section has expired please login');
//             $location.path('/login');
//         }

//         return $q.reject(response);
//     }

//     return interceptorFactory;

// });
hCueDoctorWebApp.factory('AuthToken', function ($window) {
    var authTokenFactory = {};

    authTokenFactory.getToken = function () {
        return $window.localStorage.getItem('token');
    }

    authTokenFactory.setToken = function (token) {
        if (token) {
            return $window.localStorage.setItem('token', token);
        } else {
            return $window.localStorage.removeItem('token');
        }
    }

    authTokenFactory.setUserDetails = function (id, name) {
        if (id) {
            $window.localStorage.setItem('userid', id);
            $window.localStorage.setItem('username', name);
        } else {
            $window.localStorage.removeItem('userid');
            $window.localStorage.removeItem('username');
        }
    }

    return authTokenFactory;
});




// hCueDoctorWebApp.factory('AuthToken', function($window) {
//     var authTokenFactory = {};

//     authTokenFactory.getToken = function() {
//         return $window.localStorage.getItem('token');
//     }

//     authTokenFactory.setToken = function(token) {
//         if (token) {
//             return $window.localStorage.setItem('token', token);
//         } else {
//             return $window.localStorage.removeItem('token');
//         }
//     }

//     authTokenFactory.setUserDetails = function(id, name) {
//         if (id) {
//             $window.localStorage.setItem('userid', id);
//             $window.localStorage.setItem('username', name);
//         } else {
//             $window.localStorage.removeItem('userid');
//             $window.localStorage.removeItem('username');
//         }
//     }

//     return authTokenFactory;
// });



//'use strict';

/* Directives */


hCueDoctorWebApp.lazyController = function () { };

hCueDoctorWebApp.run(['$rootScope', '$location', 'hcueDoctorLoginService', '$route',
    function ($rootScope, $location, hcueDoctorLoginService, $route) {

        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            var doctorinfoaccess = hcueDoctorLoginService.getDoctorInfo();

            if (current && current.$$route) {
                if (current.$$route.originalPath === "/summaryresult" && next.$$route.originalPath === "/main") {
                    $location.path('/appointment');
                }
            }

            if (next && next.$$route && next.$$route.permission) {
                var permission = next.$$route.permission;


                console.log(permission);

                if (doctorinfoaccess.isAccessRightsApplicable) {
                    console.log(doctorinfoaccess.isAccessRightsApplicable);
                    if (doctorinfoaccess.AccountAccessID.indexOf(permission) == -1) {
                        $location.path('/unauthorized');
                    }
                }

            }
        });

        if (typeof (current) !== 'undefined') {
            $templateCache.remove(current.templateUrl);
        }



    }
]);

hCueDoctorWebApp.config(['$routeProvider', 'localStorageServiceProvider', '$httpProvider', '$controllerProvider',
    function ($routeProvider, localStorageServiceProvider, $httpProvider, $controllerProvider) {

        hCueDoctorWebApp.lazyController = $controllerProvider.register;

        $httpProvider.interceptors.push('httpInterceptor');

        function loadScript(path) {
            var result = $.Deferred(),
                script = document.createElement("script");
            script.async = "async";
            script.type = "text/javascript";
            script.src = path;
            script.onload = script.onreadystatechange = function (_, isAbort) {
                if (!script.readyState || /loaded|complete/.test(script.readyState)) {
                    if (isAbort)
                        result.reject();
                    else
                        result.resolve();
                }
            };
            script.onerror = function () { result.reject(); };
            document.querySelector("head").appendChild(script);
            return result.promise();
        }

        function loader(arrayName) {
            return {
                load: function ($q) {
                    var deferred = $q.defer(),
                        map = arrayName.map(function (name) {
                            if (name.substring(0, 3) == 'c7/') {
                                return loadScript('components/' + name.split('7/')[1] + ".js");
                            } else {
                                return loadScript('js/controllers/' + name + ".js");
                            }
                        });

                    $q.all(map).then(function (r) {
                        deferred.resolve();
                    });

                    return deferred.promise;

                }
            };
        }

        function loaderWithPath(arrayName) {
            return {
                load: function ($q) {
                    var deferred = $q.defer(),
                        map = arrayName.map(function (name) {
                            return loadScript(name);
                        });

                    $q.all(map).then(function (r) {
                        deferred.resolve();
                    });

                    return deferred.promise;
                }
            };
        }

        $routeProvider.
            when('/comingbilling', {
                templateUrl: 'templates/coming-soon/billing.html'
                // controller: 'hcueBillingController',
                // resolve: loader(["billingController"])
            })

            //    .when('/Appointmentbilling', {
            //        templateUrl: 'templates/consultation/billing/Appointmentbilling.html',
            //        permission: 'APPVIWBING',
            //        controller: 'hcueAppointmentBillingController',
            //        resolve: loader(['appointmentBillingController'])
            //    })
            .when('/Nursingbilling', {
                templateUrl: 'templates/appointment/NursingBilling.html',
                permission: 'APPVIWBING',
                controller: 'hcueNursingBillingController',
                resolve: loader(['hcueNursingBillingController'])
            })

            .when('/dashboard', {
                templateUrl: "components/dashboard/dashboard.html",
                activetab: 'c7dashboard',
                controller: 'c7DashboardController',
                resolve: loader(['c7/dashboard/dashboardController']),
                // permission: 'LSTDASHBRD'
            })
            // .when('/dashboard', {
            //     templateUrl: function() {
            //         return (!JSON.parse(localStorage.getItem("isAppOnline"))) ? "templates/admin/Offline.html" : "templates/dashboard/dashboard.html"
            //     },
            //     activetab: 'comingreferral',
            //     controller: 'hCueDashboardController',
            //     resolve: loader(['dashboardController']),
            //     // permission: 'LSTDASHBRD'
            // })
            //    .when('/guide', {
            //        templateUrl: 'templates/guide/guide.html',
            //        activetab: 'guide'

            //    })

            //    .when('/getstarted', {
            //        templateUrl: 'templates/getStarted/getstarted.html',
            //        activetab: 'getstarted'

            //    })

            //    .when('/assist', {
            //        templateUrl: function() {
            //            return (!JSON.parse(localStorage.getItem("isAppOnline"))) ? "templates/admin/Offline.html" : "templates/assist/assist.html"
            //        },
            //        activetab: 'assist',

            //    })

            .when('/publish', {
                templateUrl: "components/publish/publish.html",
                activetab: 'publish',
                controller: 'c7PublishController',
                // resolve: loader(['c7/dashboard/dashboardController']),
            })

            .when('/reminder', {
                templateUrl: "components/reminder/reminder.html",
                activetab: 'reminder',
                controller: 'c7ReminderController',
            })

            .when('/reports', {
                templateUrl: function () {
                    return (!JSON.parse(localStorage.getItem("isAppOnline"))) ? "templates/admin/Offline.html" : "templates/dashboard/reports.html"
                },
                activetab: 'reports',
                permission: 'LSTDASHBRD',
                controller: 'hCueReportsController',
                resolve: loader(['reportsController'])

            })

            .when('/dailyreport', {
                templateUrl: function () {
                    return (!JSON.parse(localStorage.getItem("isAppOnline"))) ? "templates/admin/Offline.html" : "components/dashboard/dailyreport.html"
                },
                activetab: 'DailyReport',
                permission: 'LSTDASHBRD',
                controller: 'c7DailyReportController',
                resolve: loader(['c7/dashboard/dailyReportsController'])

            })
            // .when('/report', {
            //     templateUrl:  "components/report/report.html",
            //     activetab: 'c7report',
            //     // permission: 'ADDCARTEAM',
            //     controller: 'c7ReportController',
            //     resolve: loader(['c7/report/reportController'])
            // })
            //    .when('/askquestion', {
            //        templateUrl: 'templates/askquestions/askquestions.html',
            //        controller: 'hcueAskaPublicQuestionController',
            //        resolve: loader(['askaPublicQuestionController'])
            //    })
            //    .when('/help', {
            //        templateUrl: 'templates/settings/help.html',
            //        permission: 'HELP1'
            //    })
            //    .when('/settings', {
            //        templateUrl: function() {
            //            return (!JSON.parse(localStorage.getItem("isAppOnline"))) ? "templates/admin/Offline.html" : "templates/settings/settings.html"
            //        },
            //        controller: 'hCueSettingsController',
            //        resolve: loader(['settingsController']),
            //        permission: 'SETT1'
            //    })
            //------------------Edit Profile -----------------------------

            .when('/editconsultinfo/:addressid', {
                templateUrl: function () {
                    return (!JSON.parse(localStorage.getItem("isAppOnline"))) ? "templates/admin/Offline.html" : "templates/doctor/consult-info-new.html"
                },
                permission: 'EDPR4',
                controller: 'hCueDoctorProfileController',
                resolve: loader(['doctorProfileController'])
            })
            //------------------Registeration -----------------------------

            //------------------Registeration -----------------------------

            //    .when('/ModelTest', {
            //        templateUrl: 'templates/consultation/ModelTest.html'
            //    })

            //    .when('/Test', {
            //        templateUrl: 'templates/consultation/NewTest.html'
            //    })

            .when('/login', {
                templateUrl: 'templates/login.html',
                controller: 'hCueLoginController',
                resolve: loader(['loginController']),
                permission: 'LOGI1'
            })
            .when('/addorganisation', {
                templateUrl: 'templates/platformPartners/addorganisation.html',



            })
            .when('/docRegister', {
                templateUrl: 'templates/doctor/DocRegister.html',
                permission: 'DCRE1'
            })

            .when('/signup', {
                templateUrl: 'templates/signup.html',
                permission: 'SIGN1',
                controller: 'hCueLoginController',
                resolve: loader(['loginController']),
            })
            .when('/signout', {
                templateUrl: 'templates/logout.html',
                controller: 'hCueSignOutController',
                permission: 'SIGN2',
                resolve: loader(["signOutController"])
            })
            .when('/profile', {
                templateUrl: function () {
                    return (!JSON.parse(localStorage.getItem("isAppOnline"))) ? "templates/admin/Offline.html" : "templates/doctor/profile-new.html"
                },
                // permission: 'EDITPRO',
                controller: 'hCueDoctorProfileController',
                resolve: loader(['doctorProfileController'])
            })

            .when('/comingreferral', {
                templateUrl: 'templates/coming-soon/dashboard.html',
                controller: 'hCueDashboardController',
                resolve: loader(['dashboardController'])
            })
            //    .when('/communication', {
            //        templateUrl: function() {
            //            return (!JSON.parse(localStorage.getItem("isAppOnline"))) ? "templates/admin/Offline.html" : "templates/communication/communication.html"
            //        },
            //        activetab: 'communication',
            //        permission: 'COMUNICATN',
            //        controller: 'hCueCommunicationController',
            //        resolve: loader(['communicationController'])
            //    })


            /*.when('/app',
                 {
                     templateUrl: 'templates/menu.html'
                 }
             )
             .when('/appointment',
                 {
                     templateUrl: 'templates/appointment/appointmentlist.html'
                 }
             )*/
            .when('/vitals', {
                templateUrl: 'templates/appointment/vitals.html',
                controller: 'hCuevitals',
                permission: 'APPVIWVITL',
                resolve: loader(["vitalsController"])
            })
            .when('/appointmentlist', {
                templateUrl: 'templates/appointment/appointmentlist.html',
                permission: 'APPMT1'
            })
            .when('/appointment', {
                templateUrl: 'templates/appointment/appointments.html',
                activetab: 'appointment',
                permission: 'APPMT1',
                controller: 'hcueAppointmentController',
                controllerAs: 'appControl',
                resolve: loader(["appointmentController"])
            })
            .when('/appointment-handle', {
                templateUrl: 'templates/appointment/appointment-handle.html',
                permission: 'APPMT1',
                resolve: loader(["appointmentEditController"])
            }).when('/appointment-book', {
                templateUrl: 'templates/appointment/appointment-book.html',
                controller: 'addAppointmentCtrl',
                resolve: loader(["addAppointmentController"])
            }).when('/homepage', {
                templateUrl: 'templates/patient/phonenumber.html',
                controller: 'hCuePatientPhoneInfo',
                permission: 'CONSULTATN',
                resolve: loader(["patientPhoneInfoController"])
            })
            .when('/phonenumber', {
                templateUrl: 'templates/patient/phonenumber.html',
                permission: 'CONSULTATN',
                controller: 'hCuePatientPhoneInfo',
                resolve: loader(["patientPhoneInfoController"])
            })
            .when('/patientlist', {
                templateUrl: 'templates/patient/patientlist.html',
                permission: 'PALT1',
                controller: 'hCueGetPatientInfo',
                resolve: loader(['getPatientInfoController'])
            })
            // .when('/registerPatient', {
            //     templateUrl: 'templates/patient/registerPatient.html',
            //     controller: 'hCueregisterPatientController',
            //     permission: 'REGPATIENT'
            // })

            .when('/addUpdatePatient', {
                templateUrl: 'components/patient/addUpdatePatient.html',
                controller: 'c7addUpdatePatientController',
                // permission: 'REGPATIENT',
                resolve: loader(['c7/patient/addUpdatePatient'])
            })

            .when('/updatePatient', {
                templateUrl: 'templates/patient/updatePatient.html',
                controller: 'hCueupdatePatientController'
            })
            // .when('/updatePatientEnquiry', {
            //     templateUrl: 'templates/patient/updatePatientEnquiry.html',
            //     controller: 'hCueupdatePatientController'
            // })
            .when('/addpatient', {
                templateUrl: 'templates/patient/addpatient.html',
                controller: 'hCueAddNewPatientController',
                // resolve: loader(['addNewPatientController']),
                permission: 'REGPATIENT'
            })
            .when('/patientemergencycontact', {
                templateUrl: 'templates/patient/patientemergencycontact.html',
                controller: 'hCueAddNewPatientController',
                // resolve: loader(['addNewPatientController']),
                permission: 'EMRC1'
            }

            )
            .when('/patientotherdetails', {
                templateUrl: 'templates/patient/patientotherdetails.html',
                controller: 'hCueAddNewPatientController',
                // resolve: loader(['addNewPatientController']),
                permission: 'EMOT1'
            }

            )


            // .when('/searchpatient', {
            //     templateUrl: 'templates/patient/search.html',
            //     activetab: 'patients',
            //     //permission: 'PATIENT',
            //     controller: 'hCuePatientSearchResult',
            //     resolve: loader(["patientSearchResultController"])
            // })
            .when('/casehistory', {
                templateUrl: 'templates/consultation/patient-summary.html',
                permission: 'VIEWCASE'
            })

            .when('/consultationentry', {
                templateUrl: 'templates/consultation/consultation-entry.html',
                activetab: 'patients',
                permission: 'CONSULTATN',
                controller: 'hcueCaseHistoryController',
                resolve: loader(['caseHistoryController'])
            })
            .when('/main', {
                templateUrl: 'templates/consultation/main.html',
                activetab: 'patients',
                permission: 'CONSULTATN',
                controller: 'hCuePrescriptionMain',
                resolve: loader(['prescriptionMainController'])
            })
            //    .when('/notes', {
            //        templateUrl: 'templates/consultation/notes/notes-main.html',
            //        permission: 'NTSM1'
            //        // controller: 'hCuePrescriptionNotes',
            //        // resolve: loader(['prescriptionNotesController'])
            //    })
            //    .when('/notesdrawing', {
            //        templateUrl: 'templates/consultation/notes/drawing/notesdrawing.html',
            //        permission: 'NTSD1',
            //        controller: 'hCuePrescriptionDrawing',
            //        resolve: loader(['prescriptionDrawingController'])
            //    })
            //    .when('/prescription', {
            //        templateUrl: 'templates/consultation/medicine/medicine-main.html',
            //        permission: 'PRCR1'
            //    })
            //    .when('/labtest', {
            //        templateUrl: 'templates/consultation/lab/lab-main.html',
            //        permission: 'LAMN1'
            //    })
            //    .when('/scantest', {
            //        templateUrl: 'templates/consultation/scan/scan-main.html',
            //        permission: 'SCMN1'
            //    })
            //    .when('/adddoctorreferral', {
            //        templateUrl: 'templates/consultation/referral/adddoctorreferral.html',
            //        permission: 'ADRF1',
            //        controller: 'hCuePrescriptionDoctorReferral',
            //        resolve: loader(["prescriptionDoctorReferralController"])
            //    })
            .when('/summary', {
                templateUrl: 'templates/consultation/summary.html',
                permission: 'SUMM1'
                // controller: 'hCuePrescriptionSummary',
                // resolve: loader(['prescriptionSummaryController'])
            })
            .when('/summaryresult', {
                templateUrl: 'templates/consultation/consultation-result.html',
                controller: 'hCueConsultationResult',
                permission: 'SURE1'

            })
            .when('/receptionistReport', {
                templateUrl: 'templates/appointment/receptionistreport.html',
                controller: 'receptionistReportCntrl',
            })
            // .when('/mycareteam', {
            //     templateUrl: function() {
            //         return (!JSON.parse(localStorage.getItem("isAppOnline"))) ? "templates/admin/Offline.html" : "templates/mycareteam/mycareteam.html"
            //     },
            //     activetab: 'mycare',
            //     permission: 'ADDCARTEAM',
            //     controller: 'hCueDoctorReferral',
            //     resolve: loader(['doctorReferralController'])
            // })
            .when('/referralteam', {
                templateUrl: "components/referralteam/referralteam.html",
                activetab: 'doctorreferral',
                // permission: 'ADDCARTEAM',
                controller: 'c7ReferralTeam',
                resolve: loader(['c7/referralteam/referralteam'])
            })

            .when('/searchpatient', {
                templateUrl: 'components/patient/searchHistory.html',
                activetab: 'patients',
                //permission: 'PATIENT',
                controller: 'c7PatientSearchHistory',
                resolve: loader(["c7/patient/searchHistoryController"])
            })
            //blog 
            //    .when('/blog', {
            //        templateUrl: 'templates/blog/blog.html',
            //        controller: 'hcueBlogController',
            //        activetab: 'blog'
            //        //resolve:loader(['blogcontroller'])
            //    })

            .when('/patientsearch', {
                templateUrl: 'templates/appointment/patient-app.html',
                permission: 'PAAP1'
            })
            .when('/appointmentedit/:startDate', {
                templateUrl: 'templates/appointment/appointment-edit.html',
                permission: 'APED1'
            })
            .when('/appointmentcancel/:getappointmentid', {
                templateUrl: 'templates/appointment/appointment-cancel.html',
                permission: 'GTAP1'
            })
            .when('/adminbranch', {
                templateUrl: 'templates/admin/admin-branch.html',
                activetab: 'adminbranch',
                permission: 'ADBR1'
            })
            .when('/admin', {
                templateUrl: function () {
                    return (!JSON.parse(localStorage.getItem("isAppOnline"))) ? "templates/admin/Offline.html" : "templates/admin/admin.html"
                },
                activetab: 'admin',

            })

            .when('/manageusers', {
                templateUrl: 'templates/admin/manage-users.html',
                activetab: 'adminbranch',
                permission: 'MANUSRS'
            })
            .when('/manageOrganisation', {
                templateUrl: 'templates/admin/Organisation/addOrganisation.html',
                activetab: 'adminbranch',
                permission: 'MANROLES',
                controller: 'hCueAdminManageOrganisationController',
                //resolve: loader(['adminOrganisationController'])
            })
            .when('/profilesettings', {
                templateUrl: 'templates/admin/profile-settings.html',
                activetab: 'adminbranch',
                permission: 'MANROLES',
                controller: 'hCueAdminProfileController',
                resolve: loader(['adminProfileController'])
            })
            .when('/feedback', {
                templateUrl: 'templates/admin/feedback.html',
                activetab: 'adminbranch',
                permission: 'MANROLES',
                controller: 'hcueFeedBackController',
                resolve: loader(['feedBackController'])
            })
            .when('/unauthorized', {
                templateUrl: 'templates/admin/unauthorized.html',
                activetab: 'adminbranch',
                permission: 'UNAU1'

            })

            .when('/forgotpassword', {
                templateUrl: 'templates/forgot-password/forgotpassword-step1.html',
                controller: 'hCueforgotPasswordController',
                resolve: loader(['forgotPasswordController'])
            })

            .when('/updatepassword/:id', {
                templateUrl: 'templates/forgot-password/forgotpassword-step1.html',
                controller: 'hCueforgotPasswordController',
                resolve: loader(['forgotPasswordController'])
            })
            //nurse flow
            .when('/servicelist', {
                templateUrl: 'templates/appointment/servicelist.html',
                permission: 'APPMT1'
            })
            .when('/newService', {
                templateUrl: 'templates/appointment/Newservice.html',
                permission: 'APPMT1',
                controller: 'newServiceListController',
                resolve: loader(['newServiceListController'])

            })
            .otherwise({
                redirectTo: '/login',
                permission: 'LOGI1'
            });


        localStorageServiceProvider
            .setPrefix('hCueWeb')
            .setStorageType('localStorage')
            .setNotify(true, true);

    }
]);




if (url == "dev") {
    var doctor_url = 'http://dzy0ewm4b3r1q.cloudfront.net'; //http://dct4avjn1lfw.cloudfront.net';
    var patient_url = 'http://dzy0ewm4b3r1q.cloudfront.net'; //'http://d1lmwj8jm5d3bc.cloudfront.net';
    var partners_url = 'http://dzy0ewm4b3r1q.cloudfront.net'; //http://d318m5cseah7np.cloudfront.net';
    var website_url = 'https://hcue.in/oldwebsite';
} else if (url == "staging") {
    // var doctor_url = 'http://d1ye5bkaaysyzm.cloudfront.net/'; //'https://d30oe2oqdfj01g.cloudfront.net';
    // var patient_url = 'http://d1ye5bkaaysyzm.cloudfront.net/'; //'https://d3uq3il3ejsqoq.cloudfront.net';
    // var partners_url = 'http://d1ye5bkaaysyzm.cloudfront.net/'; //'https://d1o21p7csgww7s.cloudfront.net';
    // var website_url = 'https://hcue.in/oldwebsite';

    // new http
    // var doctor_url = 'http://c7health-env-doctor.eu-west-2.elasticbeanstalk.com'; //http://d1ye5bkaaysyzm.cloudfront.net/'; //'https://d30oe2oqdfj01g.cloudfront.net';
    // var patient_url = 'http://c7health-env-patient.eu-west-2.elasticbeanstalk.com'; //'http://d1ye5bkaaysyzm.cloudfront.net/'; //'https://d3uq3il3ejsqoq.cloudfront.net';
    // var partners_url = 'http://c7health-env-platformpartner.eu-west-2.elasticbeanstalk.com'; //'http://d1ye5bkaaysyzm.cloudfront.net/'; //'https://d1o21p7csgww7s.cloudfront.net';
    // var upload_url = 'http://hcuedev-hcue.ap-southeast-1.elasticbeanstalk.com';

    // new https
    // var doctor_url = 'https://d3apfmq85pvf5z.cloudfront.net/doctor';
    var doctor_url = 'https://d52o7jsjvmb4x.cloudfront.net';
    //var patient_url = 'https://d3dxiin0ycj280.cloudfront.net';
    // var patient_url = 'https://d3apfmq85pvf5z.cloudfront.net/patient';
    var patient_url = 'https://d52o7jsjvmb4x.cloudfront.net';
    // var partners_url = 'https://d18u5rojlatewc.cloudfront.net';
    // var partners_url = 'https://d3apfmq85pvf5z.cloudfront.net/platformpartners';
    var partners_url = 'https://d52o7jsjvmb4x.cloudfront.net';
    // var upload_url = 'https://dzy0ewm4b3r1q.cloudfront.net';
    // var upload_url = 'https://d3dxiin0ycj280.cloudfront.net/';
    // var upload_url = 'https://d1uutzrpqq55vj.cloudfront.net';

    var website_url = 'https://d1mwmybssgn1gb.cloudfront.net/app';
} else if (url == "offline") {
    var doctor_url = 'http://192.168.0.8:8080/playdoctor';
    var patient_url = 'http://192.168.0.8:8080/playpatient';
    var partners_url = 'http://192.168.0.8:8080/playplatformpartners';
    var website_url = 'https://hcue.in/oldwebsite';
} else {
    var doctor_url = 'https://d3c1s4h5i8j0yu.cloudfront.net';
    var patient_url = 'https://d9bfdxyln9850.cloudfront.net';
    var partners_url = 'https://d1r5yiqcw3t3fn.cloudfront.net';
    var website_url = 'https://hcue.in';
}



//doctor:http://dct4avjn1lfw.cloudfront.net 	 demo doctor:https://d30oe2oqdfj01g.cloudfront.net   Prod Doctor:https://d3c1s4h5i8j0yu.cloudfront.net
//patient:http://d1lmwj8jm5d3bc.cloudfront.net   demo patient:https://d3uq3il3ejsqoq.cloudfront.net  Prod patient:https://d9bfdxyln9850.cloudfront.net
//partners:http://d318m5cseah7np.cloudfront.net  demo partners:https://d1o21p7csgww7s.cloudfront.net Prod partners:https://d1r5yiqcw3t3fn.cloudfront.net





hCueDoctorWebApp.constant('hcueServiceUrl', {

    //c7 doctor appoinments url
    'getDoctorAppointments': doctor_url + '/doctors/getDoctorAvailableAppointments',
    'bookc7appointment': doctor_url + '/doctors/addupdateHospitalAppointments',

    //c7 upload url
    'uploadPatientDetails': patient_url + '/patient/uploadFile/enquiry',
    'uploadLetterDocument': patient_url + '/patient/uploadFile/generallettersUpload',
    'logoupload': patient_url + '/patient/logoupload',

    //c7 partners url
    'getHospitals': partners_url + '/platformPartners/getHospitals',
    'addUpdateHospital': partners_url + '/platformPartners/addUpdateHospital',
    'searchBranch': partners_url + '/platformPartners/searchBranch',
    'addorganisation': partners_url + '/platformPartners/addorganisation',
    'updateOrganisation': partners_url + '/platformPartners/updateorganisation',
    'getOrganisation': partners_url + '/platformPartners/getorganisation',
    'listallorganisation': partners_url + '/platformPartners/listallorganisation',
    'searchorganisation': partners_url + '/platformPartners/searchorganisation',

    //c7 patient url
    //'uploadPatientDetails': patient_url + '/patient/uploadFile/enquiry',
    'getPatientDetails': patient_url + '/patients/patients/getPatient',
    'listAllEnquiry': patient_url + '/patient/listAllEnquiry',
    'addNewPatient': patient_url + '/patient/addPatient',
    'updateNewPatient': patient_url + '/patient/updatePatient',
    'updatePatientEnquiry': patient_url + '/patient/updatePatientEnquiry',
    'listEnquiryStatus': patient_url + '/patient/listEnquiryStatus',
    'listManageStatus': patient_url + '/patient/listManageStatus/true',
    'listAppointmentStatus': patient_url + '/patient/listAppointmentStatus/',
    'enquiryaddstatus': patient_url + '/patient/enquiryaddstatus',
    'searchPatients': patient_url + '/patient/searchPatients',
    'searchPatientsget': patient_url + '/patient/searchPatients/',
    'listPatientAllEnquiry': patient_url + '/patient/listPatientAllEnquiry ',
    'listAllReferral': patient_url + '/patient/listAllReferral',
    'Referralcount': patient_url + '/patient/Referralcount',
    'addReminder': patient_url + '/patient/reminderupdate',
    'getReminderList': patient_url + '/patient/listReminderEnquiry',
    //c7 doctor url
    'addRegion': doctor_url + '/doctors/addupdateRegion',
    'updateRegion': doctor_url + '/doctors/addupdateRegion',
    'getRegionDetails': doctor_url + '/doctors/listRegionInfo',
    'listRegion': doctor_url + '/doctors/listRegionInfo',
    'listAllRegionInfo': doctor_url + '/doctors/listAllRegionInfo',
    'addccg': doctor_url + '/doctors/addupdateCcgroups',
    'updateccg': doctor_url + '/doctors/addupdateCcgroups',
    'getccgDetails': doctor_url + '/doctors/listCcgroupsInfo',
    'listccg': doctor_url + '/doctors/listCcgroupsInfo',
    'listAllCcgroupsInfo': doctor_url + '/doctors/listAllCcgroupsInfo',
    'addPractice': doctor_url + '/doctors/addupdatePractice',
    'updatePractice': doctor_url + '/doctors/addupdatePractice',
    'getPracticeDetails': doctor_url + '/doctors/listPracticeInfo',
    'listPractice': doctor_url + '/doctors/listPracticeInfo',
    'listAllPracticeInfo': doctor_url + '/doctors/listAllPracticeInfo',
    'newadddoctor': doctor_url + '/doctors/addDoctor',
    'newupdateDoctor': doctor_url + '/doctors/updateDoctor',
    'getDoctor': doctor_url + '/doctors/getDoctor',
    'newlistdoctor': doctor_url + '/doctors/careteam/doctorList',
    'editdoctor': doctor_url + '/doctors',
    'addSpecialitylkup': doctor_url + '/doctors/addSpecialitylkup',
    'Specialitycategory': doctor_url + '/doctors/Specialitycategory',
    'SpecialityList': doctor_url + '/doctors/SpecialityList',
    'SpecialityListNew': doctor_url + '/doctors/SpecialityList/v1',
    'downloadPractice': doctor_url + '/doctors/getallPracticereportInfo',

    'CategoryList': doctor_url + '/doctors/SpecialityCategory',

    'updateSpecialitylkup': doctor_url + '/doctors/updateSpecialitylkup',
    'getDocList': doctor_url + '/doctors/listpracticedoctors/',
    'listdoctorhospital': doctor_url + '/doctors/listdoctorhospital/',
    'listpostcodeinfo': doctor_url + '/doctors/listpostcodeinfo',
    'getNearHospitalList': doctor_url + '/doctors/listPostcodehospitalsearch',
    'deactivedoctorPractice': doctor_url + '/doctors/deactivedoctorPractice',
    'getEnquiryDashboardInfo': doctor_url + '/doctors/getEnquiryDashboardInfo',
    'getDailyReportInfo': doctor_url + '/doctors/getDailyReportInfo',
    'getDailySpecialityReportInfo': doctor_url + '/doctors/getDailySpecialityReportInfo',
    'getCostReportInfo': doctor_url + '/doctors/getCostReportInfo',
    'getForecastReportInfo': doctor_url + '/doctors/getForecastReportInfo',
    'getReferralReportInfo': doctor_url + '/doctors/getReferralReportInfo',
    'getAppointmentReport': doctor_url + '/doctors/getAppointmentPracticereport',
    'getClinicauditcriteriareport': doctor_url + '/doctors/getClinicauditcriteriareport',
    'getPatientinSystemreport': doctor_url + '/doctors/getPatientinSystemreport',
    //'getPatientinSystemreportnew': doctor_url + '/doctors/getPatientinSystemreportnew', //old URL
    'getPatientinSystemreportnew': doctor_url + '/doctors/getPatientinSystemreportnew/v1',
    'getPatientinSystemreporttotal': doctor_url + '/doctors/getPatientinSystemreporttotal',
    'getAppointmentDtl': doctor_url + '/doctors/getAppointmentDtl/',
    'getCostanalyticsreport': doctor_url + '/doctors/getCostanalyticsreport',
    'getAppointmentindicationreport': doctor_url + '/doctors/getAppointmentPracticeindicationreport',
    'getCostReportwithregionInfo': doctor_url + '/doctors/getCostReportwithregionInfo',
    'getExceptionnotesreport': doctor_url + '/doctors/getExceptionnotesreportInfo',
    // 'getForecastReportwithregionInfo': doctor_url + '/doctors/getForecastReportwithregionInfo',
    'getReferralwithoutindication': doctor_url + '/doctors/getReferralwithoutindication',
    'listAllScheduleInfo': doctor_url + '/doctors/listAllScheduleInfo',
    'hospitalscheduleadd': doctor_url + '/doctors/hospitalschedule/add',
    'hospitalscheduleupdate': doctor_url + '/doctors/hospitalschedule/update',
    'hospitalScheduleDelete': doctor_url + '/doctors/hospitalschedule/delete',
    'listHcueAccessRights': doctor_url + '/doctors/admin/listHcueAccessRights',
    'listHospitalRole': doctor_url + '/doctors/admin/listHospitalRole',
    'listProfile': doctor_url + '/doctors/admin/listProfile',
    'addUpdateProfile': doctor_url + '/doctors/admin/addUpdateProfile',
    'listDashBoardInfo': doctor_url + '/doctors/listDashBoardInfo',
    'listAllCalendarInfo': doctor_url + '/doctors/listAllCalendarInfo',
    'listAvailableAppointment': doctor_url + '/doctors/listAvailableAppointment',
    'addupdateHospitalAppointments': doctor_url + '/doctors/addupdateHospitalAppointments',
    'listHospitalAppointments': doctor_url + '/doctors/listHospitalAppointments',
    'listrejectPatients': doctor_url + '/doctors/listrejectPatients',
    'getCalendarlistviewInfo': doctor_url + '/doctors/getCalendarlistviewInfo',
    'getAgentstatsreporttotalInfo': doctor_url + '/doctors/getAgentstatsreporttotalInfo',
    'getAgentstatsreportInfo': doctor_url + '/doctors/getAgentstatsreportInfo',
    'getCustombookingreportInfo': doctor_url + '/doctors/getCustombookingreportInfo',
    'getCustomadminreportInfo': doctor_url + '/doctors/getCustomadminreportInfo',
    // 'getGeneralReport': doctor_url + '/doctors/getGeneralreport',
    'getGeneralreportfile': doctor_url + '/doctors/getGeneralreportfile',
    'GeneralStatsReportFileList': doctor_url + '/doctors/GeneralStatsReportFileList',
    //sms
    'sendAppointmentSMS': doctor_url + '/doctors/sendAppointmentSMS',
    // emails 
    'sendAppointmentEmail': doctor_url + '/doctors/sendAppointmentEmail',
    'sendPatientDeclinedEmail': doctor_url + '/doctors/sendPatientDeclinedEmail',
    'sendContactUsEmail': doctor_url + '/doctors/sendContactUsEmail',
    'sendExclusionEmail': doctor_url + '/doctors/sendExclusionEmail',
    'sendNoContactToGP': doctor_url + '/doctors/sendNoContactToGP',
    'sendPatientDNAMail': doctor_url + '/doctors/sendPatientDNAMail',
    'sendPatientWithdrawalMail': doctor_url + '/doctors/sendPatientWithdrawalMail',
    'sendUrgentNoContactToGP': doctor_url + '/doctors/sendUrgentNoContactToGP',
    'sendAppointmentdetailsMail': doctor_url + '/doctors/sendAppointmentdetailsMail',

    'publishHl7ToMesh': doctor_url + '/doctors/sendmeshmessage',
    'sendAttachmentMail': doctor_url + '/doctors/sendAttachmentMail',
    'deactiveDoctor': doctor_url + '/doctors/deactiveDoctor',
    'sendSupportworkerinfoEmail': doctor_url + '/doctors/sendSupportworkerinfoEmail',
    'sendSonographerinfoEmail': doctor_url + '/doctors/sendSonographerinfoEmail',
    'getHL7List': doctor_url + '/doctors/listHL7Messages',
    'triggerHL7MessageConnection': doctor_url + '/doctors/triggerHL7Message/',
    // ========================================================================= //
    //admingroup
    'adminGroup': doctor_url + '/doctors/admin/listHospitalRole',
    'getDoctorAvailableAppointments': doctor_url + '/doctors/getDoctorAvailableAppointments',

    //LastLogintime
    'lastLogin': doctor_url + '/doctors/updateDoctorLastLogin',
    'validateDoctorLogin': doctor_url + '/doctors/validate/doctorLogin',
    'readLookup': doctor_url + '/doctors/getreadLookup',
    'updateDoctor': doctor_url + '/doctors/updateDoctor',
    'forgotPasswordStep1': doctor_url + '/doctors/forgotPassword',
    'forgotUpdatePassword': doctor_url + '/doctors/updatePassword',

    // ========================================================================= //
    // 'registerDevice': partners_url + '/platformPartners/addUserDevice',
    // 'imageUpload': doctor_url + '/doctors/addUpdateDoctorProfileImage',
    // 'getImage': doctor_url + '/doctors/getDoctorProfileImage',
    // 'getDoctorsOffline': doctor_url + '/doctors/getDoctorsOffline',
    // 'addupdateBillingProcedure': doctor_url + '/doctors/addUpdateProcedureCatalog',
    // 'listDoctorProcedure': doctor_url + '/doctors/listDoctorProcedure',
    // 'bookAppointment': doctor_url + '/doctors/addDoctorsAppointments',
    // 'addUpdateAppointment': doctor_url + '/doctors/addUpdateDoctorsAppointments',
    // 'getDoctorsAppointments': doctor_url + '/doctors/getDoctorsAppointments',
    // 'gethcueDoctors': doctor_url + '/doctors/getDoctors',
    // 'updtDoctorAppointments': doctor_url + '/doctors/updtDoctorAppointmentsStatus',
    // 'updatePatientAppointments': doctor_url + '/doctors/updtDoctorAppointments',
    // 'getPharmaListByDoctorId': partners_url + '/platformPartners/doctors/referral/pharma/listDoctorPharmas',
    // 'getLabListByDoctorId': partners_url + '/platformPartners/doctors/referral/lab/listDoctorLabs',
    // 'getDoctorRefList': doctor_url + '/doctors/referral/hospital/list',

    // 'addMedicineTemplate': doctor_url + '/doctor/template/addPrescriptionTemplate',
    // 'updateMedicineTemplate': doctor_url + '/doctor/template/updatePrescriptionTemplate',
    // 'getTemplatebyDoctorID': doctor_url + '/doctor/template/listPrescriptionTemplateByDoctorID',
    // 'addBillingTemplate': doctor_url + '/doctor/template/addBillingTemplate',
    // 'getListBillingHistory': doctor_url + '/doctor/template/listBillingTemplate',
    // 'UpdateBillingTemplate': doctor_url + '/doctor/template/updateBillingTemplate',


    // 'addPatient': patient_url + '/patients/addPatient',
    // 'updatePatient': patient_url + '/patients/updatePatient',
    // 'getPatient': patient_url + '/patients/getPatients',
    // 'getPatientthroughmobilenumber': doctor_url + '/doctors/getDoctorPatients',
    // 'getPatientbyId': patient_url + '/patients',
    // 'getdoctorPatient': patient_url + '/patients/getDoctorPatients',
    // 'getVitals': patient_url + '/patientCase', //9884201160001/getPatientVitals
    // 'AddLabProspect': partners_url + '/platformPartners/addProspect',
    // 'AddPharmaProspect': partners_url + '/platformPartners/addProspect',
    // 'listPatientCase': patient_url + '/patientCase/listPatientCaseHistory',
    // 'getPatientCaseDetails': patient_url + '/patientCase/patientCaseDetails',
    // 'getLabCaseDetails': patient_url + '/patientCase/listPatientLabCaseHistory',
    // 'addAllPatientCase': patient_url + '/patientCase/addCompletePatientCasePrescription',
    // 'addDrawing': patient_url + '/patientCase/addUpdatePatientCaseDocumentImage',
    // 'getNotesDrawing': patient_url + '/patientCase/getPatientCaseDocumentImage',
    // 'deleteAudioNotes': patient_url + '/patientCase/deletePatientCaseDocumentImage',
    // 'addDoctorPharma': partners_url + '/platformPartners/doctors/referral/pharma/add',
    // 'addDoctorLab': partners_url + '/platformPartners/doctors/referral/lab/add',
    // 'pharmaSearch': partners_url + '/platformPartners/searchPharma',
    // 'labSearch': partners_url + '/platformPartners/searchLab',
    // //'getDoctorRefList': doctor_url+'/doctors/referral/hospital',
    // 'addpartners': partners_url + '/platformPartners/addPharmaProspect',
    // 'addConsultDoctor': doctor_url + '/doctors/addDoctor',
    // 'addDoctor': doctor_url + '/doctors/addDoctor',
    // 'addConsultation': doctor_url + '/doctors/consultation/add',
    // 'updateConsultation': doctor_url + '/doctor/consultation/update',
    // 'updateHospitalAddress': doctor_url + '/doctors/updateDoctor',

    // 'changeDoctorPassword': doctor_url + '/doctor/validate/updatePassword',
    // 'getDoctorAppoitment': doctor_url + '/doctor/consultation/availableAppointment',
    // 'addvitals': patient_url + '/patientCase/addUpdatePatientVitals',
    // //'getVitals': patient_url+'/patientCase',
    // 'GetClinicImage': doctor_url + '/doctors/getDoctorClinicImage',
    // 'GetCareerImage': doctor_url + '/doctors/getDoctorCareerImage',
    // 'getTreatmentsByDoctorId': doctor_url + '/doctor/practising/bodyChart/condition/treatment',
    // 'updateTreatmentByDoctorId': doctor_url + '/doctor/practising/bodychart/addUpdateTreatment',
    // 'getBillingInfo': patient_url + '/patientCase/getPatientBilling',
    // 'getBillingInfoReceptionist': patient_url + '/patientCase/listPatientCaseProcedure',
    // 'updateBillingProcedure': patient_url + '/patientCase/addUpdatePatientCaseProcedure',
    // 'hcueDoctor': doctor_url + '/doctors/referral/hospital/add',
    // 'addUpdateDoctorVitalSetting': doctor_url + '/doctor/addUpdateDoctorVitalSetting',
    // //'readVitals': doctor_url+'/doctor/readPatientVitalLkup/Active',
    // 'readVitals': doctor_url + '/doctor/readDoctorVitalSetting',
    // 'readVitalsByDoctorId': doctor_url + '/doctor/readDoctorVitalSetting/Active',
    // 'readDentalDetailsByDoctorId': doctor_url + '/doctor/readPatHistorySettings',
    // 'getAppointments': doctor_url + '/doctors',
    // //'addUpdateDoctorVitalSetting': doctor_url+'/doctor/addUpdateDoctorVitalSetting',
    // //'readDentalDetailsByDoctorId' :doctor_url+'/doctor/readPatienHistoryLkup/Active',
    // 'readDoctorHistorySetting': doctor_url + '/doctor/readDoctorHistorySetting',
    // 'addDoctorHistorySetting': doctor_url + '/doctor/addUpdateDoctorHistorySetting',
    // 'getcityvalues': doctor_url + '/doctor/readCityLookup/Active',
    // 'getStatusvalues': doctor_url + '/doctor/readStateLookup/Active',
    // 'getCountryvalues': doctor_url + '/doctor/readCountryLookup/Active',
    // 'addPatientvitals': patient_url + '/patientCase/addUpdatePatientCase',
    // 'getpatientvitals': patient_url + '/patientCase/getPatientCaseByAppointment',
    // 'diagnosisaddUpdate': doctor_url + '/doctor/addUpdateDoctorDiagnosisSetting',
    // 'readdiagnosis': doctor_url + '/doctor/readDoctorDiagnosisSetting',
    // 'visitReasonAddUpdate': doctor_url + '/doctor/addUpdateDoctorVisitRsnSetting',
    // 'readVisitReason': doctor_url + '/doctor/readDoctorVisitRsnSetting',
    // 'updateDoctorSettings': doctor_url + '/doctors/updateDoctor',
    // 'readDoctorReferralSetting': doctor_url + '/doctor/readDoctorReferralSetting',
    // 'getlocationvalues': doctor_url + '/doctor/readLocationLookup/active',
    // 'referalAddUpdate': doctor_url + '/doctor/addUpdateDoctorReferralSetting',
    // 'referalread': doctor_url + '/doctor/readDoctorReferralSetting',

    // //communication
    // 'communicationTab': doctor_url + '/doctors/addDoctorCommnuication',
    // 'listCommunicationGroup': doctor_url + '/doctors/listDocCommunicationGroups',
    // 'updtCommunicationGroup': doctor_url + '/doctors/updtCommunicationGroupToDoctor',
    // 'addCommunicationGroup': doctor_url + '/doctors/addCommunicationGroupToDoctor',


    // 'getClinicDoctors': doctor_url + '/doctors/getDoctors',
    // 'getWeekwiseMonthwiseAppointments': doctor_url + "/doctor/consultation/availableAppointment",
    // 'getSMSpatients': doctor_url + '/doctors/getDoctorPatients',
    // 'getTotalCashReport': doctor_url + '/doctors/getTotalCashReceivedReportInfo',
    // 'getDocClinicData': doctor_url + '/doctors/getAdminLookup/',
    // 'getDataDashboard': doctor_url + '/doctors/getDashboardInfo',
    // 'getPatientVisitReport': doctor_url + '/doctors/getPatientVisitReportInfo',
    // 'getPharmacyInventoryList': "https://hcue-api-doc-app.azurewebsites.net/products/search?",
    // 'addBranch': partners_url + '/platformPartners/addUpdateHospital',
    // 'updateBranch': partners_url + '/platformPartners/addUpdateHospital',
    // 'getBranchDetails': partners_url + '/platformPartners',
    // 'addUser': doctor_url + '/doctors/addDoctor',
    // 'updateUser': doctor_url + '/doctors/updateDoctor',
    // 'getAdminLkup': doctor_url + '/doctors/getAdminLookup/',
    // 'LocationLookup': doctor_url + '/doctor/readLocationLookup/Active',
    // 'getUsers': partners_url + '/platformPartners/getHospitals',
    // 'getSpecialities': doctor_url + '/doctors/getreadLookup', //readLookup
    // 'getRoles': doctor_url + '/doctors/admin/listProfile',
    // 'getlistHcueRole': doctor_url + '/doctors/admin/listHcueRole',
    // 'addUpdateRoles': doctor_url + '/doctors/admin/addUpdateProfile',
    // 'trendingTopBranches': doctor_url + '/doctors/getTopTrendingBranchesReportInfo',
    // 'getDutyDocReport': doctor_url + '/doctors/getTopTrendingDutyDocReportInfo',
    // 'toptrendingtreatments': doctor_url + '/doctors/getTopTrendingTreamentsReportInfo',
    // 'topoutstandingReports': doctor_url + '/doctors/getTotalOutstandingReportInfo',
    // 'toptrendingConsultReports': doctor_url + '/doctors/getTopTrendingConsultReportInfo',
    // 'addPatientRegistration': patient_url + '/patient/addPatientRegistration',
    // 'listRegistration': patient_url + '/patient/listRegistration/',
    // 'updateDoctorTimings': doctor_url + '/doctors/consultation/add',
    // 'getHospitalAppointments': doctor_url + '/doctors/getHospitalAppointments',
    // 'getHospitalLabAppointments': partners_url + '/platformPartners/listPartnerAppointments',
    // 'getManageDoctors': doctor_url + '/doctors/getDoctors',
    // 'getAddressConsultationAvailable': doctor_url + '/doctor/consultation/getAddressConsultation',
    // 'addHospitalAppointment': doctor_url + '/doctors/addHospitalAppointments',
    // 'getTypeVarietyShadeForLab': doctor_url + '/doctors/readPatientCaseLookup',
    // 'getTreatmentsByDoctorId': doctor_url + '/doctors/listDoctorProcedure',

    // //forgot password flow
    // 'forgotPasswordCheckMobi': 'https://api.checkmobi.com/v1/sms/send',
    // 'forgotPasswordStep2': doctor_url + '/doctor/updatePinPass',
    // 'forgotPasswordValidate': doctor_url + '/doctor/validate/doctorPinPass',
    // 'forgotSendEmailOTP': patient_url + '/patient/sendEmail',
    // 'sendLableads': partners_url + '/platformPartners/updateLabLeads',
    // 'GetLabDocumentsDownload': partners_url + '/platformPartners/getPatientCaseDocumentImage',

    // 'GetFileDownload': patient_url + '/patientCase/getPatientCaseDocumentImage',
    // 'getDoctorPost': doctor_url + '/doctors/getDoctor',
    // //Doctor Medicine Setting 
    // 'listDoctorMedicineSetting': doctor_url + '/doctor/listAddedMedicines',
    // 'addUpdateDoctorMedicine': doctor_url + '/doctor/addUpdateDoctorMedicineSettings',
    // 'getMedicines': doctor_url + '/doctor/MedicineListing',
    // 'addStockItems': doctor_url + '/doctor/addStockItems',

    // //Ophthal Doctor Settings
    // 'getOPTTable': doctor_url + '/doctor/practising/bodyChart/OPT',
    // 'getCountryPost': doctor_url + '/doctor/readCountryLookups',
    // 'getStatePost': doctor_url + '/doctor/readStateLookups',
    // 'getCityPost': doctor_url + '/doctor/readCityLookups',
    // 'getLocationPost': doctor_url + '/doctor/readLocationLookups',
    // //PatientIdGeneration
    // 'patientIDGenaration': partners_url + '/platformPartners/patientIDGenaration',
    // //appointmentprint
    // 'appointmentlistprint': patient_url + '/patientCase/patientCaseDetails',
    // //
    // 'patientIdgenlookup': doctor_url + '/doctors/getAdminLookup', //:hospitalID/lookup
    // //update patient
    // 'updatePatientDetails': patient_url + '/patients/getPatient',

    // 'listMembershipDetails': doctor_url + '/doctors/readHospitalMembership',
    // 'addMembershipDetails': doctor_url + '/doctors/addUpdateHospitalMembership',
    // 'FamilyIDGenaration': partners_url + '/platformPartners/familyIDGenaration',

    // //Gocliniz roster
    // 'addupdateTimeslots': doctor_url + '/doctor/addUpdateTimeSlots',
    // 'listAddedTimeSlots': doctor_url + '/doctor/listAddedTimeSlots',
    // 'addUpdateTimeSheets': doctor_url + '/doctor/addUpdateTimeSheets',
    // 'listAddedTimeSheets': doctor_url + '/doctor/listAddedTimeSheets',
    // //department
    // 'addUpdateDoctorDepartment': doctor_url + '/doctor/addUpdateDoctorDepartmentSetting',
    // 'listDoctorDepartment': doctor_url + '/doctor/listDoctorDepartmentSetting',
    // 'updateDoctorMoreInfo': doctor_url + '/doctors/updateDoctor',
    // 'PaymentCompanyListing': doctor_url + '/doctor/readHospitalPaymentCompany',
    // 'AddUpdatePaymentCompany': doctor_url + '/doctor/addUpdateHospitalPaymentCompany',
    // //setting Referral
    // 'addUpdateSubVisitRefSetting': doctor_url + '/doctor/addUpdateDoctorSubCatagorySetting',
    // 'listSubVisitRefSetting': doctor_url + '/doctor/listDoctorSubCatagorySetting',
    // 'readHospitalPaymentCompany': doctor_url + '/doctor/readHospitalPaymentCompany',


    // //Consultation Pause
    // 'consultationPause': doctor_url + '/doctor/addUpdateDoctorJsonSettings',
    // 'appointmentStatus': doctor_url + '/doctors/updtDoctorAppointmentsStatus',
    // 'listJsonSettings': doctor_url + '/doctor/listAddedJsonSettings',

    // 'listLabTest': doctor_url + '/doctor/listLabTestLukups',
    // 'appointmentReports': doctor_url + '/doctor/doctorAppointmentInfoDownload',
    // //added by arun  for nursing flow
    // 'GetListDetailsForNurse': patient_url + '/patient/listPatientServices',
    // 'AddPatientServices': patient_url + '/patient/addUpdatePatientServices',
    // 'GetDoctorForDropdown': partners_url + '/platformPartners/searchHospital',
    // 'addUpdateAppointment': doctor_url + '/doctors/addUpdateDoctorsAppointments',
    // 'referralReports': doctor_url + '/doctors/getDoctorReferrelReportInfo',

    // //Default Pharmacy Set	
    // 'listDefaultPharmacy': partners_url + '/platformPartners/doctors/referral/pharma/listAdminPharmas',
    // 'addDefaultPharmacy': partners_url + '/platformPartners/doctors/referral/adminpharma/add',
    // 'payoutReports': doctor_url + '/doctors/getBillingSplitReportInfo',
    // //Dental
    // 'getConditionsByDoctorId': doctor_url + '/doctor/practising/bodyChart/condition',

    // //ConsentFroms
    // 'consentFormUploadInSettings': doctor_url + '/doctors/consentForm/upload',
    // 'getDoctorsConsentFormList': doctor_url + '/doctors/consentForm/list',
    // 'GetPatientConsentFormDownload': patient_url + '/patientCase/getPatientCaseDocumentImage',
    // 'GetDoctorConsentFormDownload': doctor_url + '/doctors/consentForm/download',
    // 'UpdatePatientSignedConsentFroms': patient_url + '/patientCase/consertForm/updateStatus',
    // 'getPatientCaseConsentFormList': patient_url + '/patientCase/consertForm/list',
    // 'addDoctorConsentForm': doctor_url + '/doctors/consentForm/addUpdate',

    // //LabOrder- Dental
    // 'getTypeVarietyShadeForLab': doctor_url + '/doctors/readPatientCaseLookup',
    // 'getLabnames': partners_url + '/platformPartners/doctors/referral/lab',

    // //Conditions - Settings
    // 'getConditionsByDoctorId': doctor_url + '/doctor/practising/bodyChart/condition',
    // 'addUpdateCondition': doctor_url + '/doctor/practising/bodychart/addUpdateCatagoryCondition',

    // //IoT Services
    // 'startDentalSoftware': 'https://localhost:8090/start',
    // 'deleteDentalImage': 'https://localhost:8090/delete',
    // 'stopDentalSoftware': 'https://localhost:8090/stop',
    // 'postTallySoftware': 'https://localhost:8090/tally',
    // 'uploadDentalImages': patient_url + '/patient/uploadFile',
    // 'listDentalImages': patient_url + '/patient/listPatientIotFile',
    // 'deleteDentalImages': patient_url + '/patientCase/deletePatientCaseDocumentImage', //patient/addUpdatePatientIotFile

    // //Incentive
    // 'addUpdateIncentive': partners_url + '/platformPartners/incentive/addUpdateIncentive',
    // 'listIncentive': partners_url + '/platformPartners/incentive/listIncentives',
    // 'incentiveLookup': partners_url + '/platformPartners/incentive/listHospitalIncentiveLookup',
    // 'topRatedDoctorReports': doctor_url + '/doctors/getTopRatedDoctorsReportInfo',
    // 'incentiveHospitalLookup': partners_url + '/platformPartners/incentive/addUpdateHospitalIncentiveLookup',
    // 'addupdateExpenseDeposit': partners_url + '/platformPartners/addUpdateExpenseDeposit',
    // 'expensesDepositList': partners_url + '/platformPartners/listExpenseDeposit',
    // 'addupdatePettyCash': patient_url + '/patientCase/addUpdtHospitalCashDetails',
    // 'getPettyCashList': patient_url + '/patientCase/getHospitalcashDetails',
    // 'getExpDepCategoryList': doctor_url + '/doctors/getAdminLookup',
    // 'getSummaryExpenseIncome': partners_url + '/platformPartners/listHospitalIncomesExpenses',

    // //sms dashboard
    // 'smsDashboard': doctor_url + '/doctors/doctorsSmsDeliveryCountInfo',
    // 'getSMSListDetails': doctor_url + '/doctors/doctorsSmsDeliveryDtlsDownload',
    // 'updatePatientReview': patient_url + '/patientCase/addUpdtPatientCaseRating',

    // //DoctorDiaryReport
    // 'doctorDiaryReport': doctor_url + '/doctors/getDoctorDiaryReportsInfo',
    // 'consultationlistReport': doctor_url + '/doctors/getTotalConsultationReportInfo',
    // 'conversionlistReport': doctor_url + '/doctors/getTotalConversionsReportInfo',
    // 'investigationlistReport': doctor_url + '/doctors/getTotalInvestigationReportInfo',
    // 'treatmentlistReport': doctor_url + '/doctors/getTotalTreatmentsReportInfo',
    // 'incentiveReports': doctor_url + '/doctor/listAddedPrintSettings',
    // 'doctorIncentiveReport': doctor_url + '/doctors/getTotalIncetivesReportInfo',
    // 'consolidatedIncentiveReport': doctor_url + '/doctors/getTotalConsolidatesReportInfo',
    // 'nonconversionlistReport': doctor_url + '/doctors/getTotalNonConversionsReportInfo',
    // //Event 
    // 'addUpdateEvent': doctor_url + '/doctors/addUpdateEvents',
    // //VitalsOnconsultation
    // 'readPatientActive': patient_url + '/patientCase',

    // //Print Settings
    // 'uploadSettingImages': doctor_url + '/doctors/uploadSettingImages',
    // 'getPrintSettings': doctor_url + '/doctor/listAddedJsonSettings',
    // 'addUpdatePrintSettings': doctor_url + '/doctor/addUpdateDoctorJsonSettings',
    // 'getSummaryPrint': patient_url + '/patientCase/getSummaryURLs',
    // 'getPatienBillingHistory': patient_url + '/patientCase/getPatienBillingHistory',

    // //admin
    // 'searchAdminHospitals': partners_url + '/platformPartners/searchBranch',
    // 'getAdminDashboard': doctor_url + '/doctors/listDashBoardInfo',
    // 'clinicDoctorList': doctor_url + '/doctors/getDoctors',

    // //offer
    // 'addLoyaltyOffer': doctor_url + '/doctor/offers/addOffers',
    // 'listBillingOffers': doctor_url + '/doctor/offers/listOffers',
    // 'updateLoyaltyOffers': doctor_url + '/doctor/offers/updateOffers',
    // //blog
    // 'addupdateBlog': doctor_url + '/doctor/addupdateDoctorBlogInfo',
    // 'listingBlog': doctor_url + '/doctor/listDoctorBlogInfo',
    // 'uploadImageBlog': website_url + '/blog/wp-json/wp/v2/media',

    // //Dashboard
    // 'dashboardList': doctor_url + '/doctors/getNewDashboardInfo',
    // 'campaignReports': doctor_url + '/doctors/getCampaignReportInfo',
    // 'getPharmaLead': doctor_url + '/doctors/getLeadsReportInfo',
    // 'getFeedBackReport': doctor_url + '/doctors/getFeedbackReportInfo',
    // 'getCancelBillReport': doctor_url + '/doctors/getCancelBillingReportInfo',
    // 'getSmsReportinfo': doctor_url + '/doctors/getSmsReportinfo',
    // //MIS Report
    // 'addMISReportRequest': doctor_url + '/doctor/AddHcueHospitalReport',
    // 'listMISReports': doctor_url + '/doctor/HcueHospitalReportList',
    // //ask a public quetion URL
    // 'listingAskaquestion': partners_url + '/platformPartners/listQuestionsAnswersForWeb',
    // 'addAnswerService': partners_url + '/platformPartners/addupdateanswer',

    // 'getpatientTreatment': doctor_url + '/doctors/getTopTrendingPatientTreamentsReportInfo',
    // 'getreceptionistReport': doctor_url + '/doctors/getBillAmtReportInfo',
    // //Membership
    // 'addLoyaltyMembership': doctor_url + '/doctor/Membership/Add',
    // 'updateLoyaltyMembership': doctor_url + '/doctor/Membership/Update',
    // 'listLoyaltyMembership': doctor_url + '/doctor/Membership/List',

    // 'getexpensereport': doctor_url + '/doctors/getExpensesDepositsReports',
    // //TreatmentSubCatagory
    // 'listDoctorProcedureSubCatagory': doctor_url + '/doctors/listDoctorProcedureSubCatagory',
    // 'addUpdateProcedureSubCatagory': doctor_url + '/doctors/addUpdateProcedureSubCatagory',
    // 'getTotalCashByPaymentMode': doctor_url + '/doctors/getTotalCashByPaymentMode',
    // 'getDoctorIncetivesReportInfo': doctor_url + '/doctors/getDoctorIncetivesReportInfo',
    // 'getLabOrdersInfo': doctor_url + '/doctors/getLabOrdersInfo',
    // 'uploadHospitalClinicImage': doctor_url + '/doctors/uploadHospitalClinicImage',
    // 'deleteHospitalImage': doctor_url + '/doctors/deleteHospitalImage'
})


//Profile Date picker for DOB
hCueDoctorWebApp.directive('jqdobpicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(function () {
                element.datepicker({
                    dateFormat: 'dd-mm-yy',
                    yearRange: '1900:+0',
                    changeYear: true,
                    maxDate: 0,
                    onSelect: function (date) {
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    }
                });
            });
        }
    };
});

hCueDoctorWebApp.directive('ccgrenewaldatepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(function () {
                element.datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeYear: true,
                    changeMonth: true,
                    minDate: 0,
                    onSelect: function (date) {
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    }
                });
            });
        }
    };
});


hCueDoctorWebApp.directive("datepickerreport", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };
            var options = {
                dateFormat: 'dd-mm-yy',
                maxDate: 0,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});
hCueDoctorWebApp.directive("datepickerappointment", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});

//hcuedoctorwebapp.directive("datepickernurseappointmentlist", function () {
//    return {
//        restrict: "a",
//        require: "ngmodel",
//        link: function (scope, elem, attrs, ngmodelctrl) {
//            var updatemodel = function (datetext) {
//                // call $apply to bring stuff to angular model
//                scope.$apply(function () {
//                    ngmodelctrl.$setviewvalue(datetext);
//                });
//            };

//            var options = {
//                dateformat: 'yy-mm-dd',
//                mindate: 0,
//                // handle jquery date change
//                onselect: function (datetext) {
//                    updatemodel(datetext);
//                }
//            };

//            // jqueryfy the element
//            elem.datepicker(options);
//        }
//    }
//});
hCueDoctorWebApp.directive("mypatientspicker", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: 'dd-mm-yy',
                maxDate: 0,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});


hCueDoctorWebApp.directive("mypatientsdobpicker", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: 'dd-mm-yy',
                maxDate: 0,
                changeYear: true,
                changeMonth: true,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});
hCueDoctorWebApp.directive("mysettingpicker", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: 'dd-mm-yy',

                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});

//datepickerappointmentlistnurse


hCueDoctorWebApp.directive("datepickerappointmentlistnurse", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});


hCueDoctorWebApp.directive("datepickerappointmentlist", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: 'dd-mm-yy',

                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});

hCueDoctorWebApp.directive("datepickerappointmentcalendar", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});

hCueDoctorWebApp.directive("datepickerappointmentreschedule", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});

hCueDoctorWebApp.directive("datepickerappreschedule", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: 'dd-mm-yy',
                minDate: 1,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});





hCueDoctorWebApp.directive("datepickerfurthercare", function () {
    var dayval = new Date();

    var futuredate = moment(dayval).add(1, 'd').format('dd-MMM-yyyy');

    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: 'dd-mm-yy',
                minDate: futuredate,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});

hCueDoctorWebApp.directive('jqdatepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(function () {
                element.datepicker({
                    dateFormat: 'dd-mm-yy',
                    onSelect: function (date) {
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    }
                });
            });
        }
    }
});

hCueDoctorWebApp.directive('ngIncludeTemplate', function () {
    return {
        templateUrl: function (elem, attrs) { return attrs.ngIncludeTemplate; },
        restrict: 'A',
        scope: {
            'ngIncludeVariables': '&'
        },
        link: function (scope, elem, attrs) {
            var vars = scope.ngIncludeVariables();
            Object.keys(vars).forEach(function (key) {
                scope[key] = vars[key];
            });
        }
    }
})

hCueDoctorWebApp.directive('typeaheadmedicine', ['$timeout', 'AutoCompleteHelper', function ($timeout, AutoCompleteHelper) {
    return {
        restrict: 'AEC',
        scope: {
            items: '=',
            prompt: '@',
            title: '@',
            subtitle: '@',
            model: '=',
            itemselected: '=',
            onSelect: '&'
        },
        link: function (scope, elem, attrs) {

            scope.handleSelection = function (selectedItem) {
                AutoCompleteHelper.addSelectedItem(selectedItem);
                scope.model = selectedItem;
                scope.itemselected = selectedItem;
                scope.model = scope.model.Medicine;
                scope.current = 0;
                scope.selected = true;
                $timeout(function () {
                    scope.onSelect();
                }, 200);
            };
            scope.GetMedicineImage = function (type) {

                var type = type.toUpperCase();
                if (type.indexOf("INJ") != -1) {
                    return "img/medicine-icons/Injection.svg";
                } else if (type == "T" || type.indexOf("TAB") != -1) {
                    return "img/medicine-icons/Tablet.svg";
                } else if (type.indexOf("CAP") != -1) {
                    return "img/medicine-icons/Capsules.svg";
                } else if (type.indexOf("SYR") != -1 || type.indexOf("SUS") != -1 || type.indexOf("LIQ") != -1 ||
                    type.indexOf("SOL") != -1 || type.indexOf("MOU") != -1) {
                    return "img/medicine-icons/Syrup.svg";
                } else if (type.indexOf("CRE") != -1) {
                    return "img/medicine-icons/Cream.svg";
                } else if (type.indexOf("OIN") != -1) {
                    return "img/medicine-icons/Ointment.svg";
                } else if (type.indexOf("Gel") != -1) {
                    return "img/medicine-icons/Gel.svg";
                }

                return "img/medicine-icons/Others.svg";

            }
            scope.GetMedicineFromLocalDB = function () {
                if (scope.model.length > 3) {
                    scope.$parent.suggest_medicine(scope.model);
                };
            }
            scope.current = 0;
            scope.selected = true;
            scope.isCurrent = function (index) {
                return scope.current == index;
            };
            scope.setCurrent = function (index) {
                scope.current = index;
            };
        },
        templateUrl: 'templates/consultation/autocomplete/medicines.html'
    }
}])
hCueDoctorWebApp.directive('typeaheadtest', ['$timeout', 'AutoCompleteHelper', function ($timeout, AutoCompleteHelper) {
    return {
        restrict: 'AEC',
        scope: {
            items: '=',
            prompt: '@',
            title: '@',
            subtitle: '@',
            model: '=',
            itemselected: '=',
            onSelect: '&'
        },
        link: function (scope, elem, attrs) {
            scope.handleSelection = function (selectedItem) {
                AutoCompleteHelper.addSelectedItem(selectedItem);
                scope.model = selectedItem;
                scope.itemselected = selectedItem;
                scope.model = scope.model.LabTestName; // column name
                scope.current = 0;
                scope.selected = true;
                $timeout(function () {
                    scope.onSelect();
                }, 200);
            };
            scope.GetTestImage = function (type) {
                var type = type.toUpperCase();
                if (type.indexOf("MRI") != -1) {
                    return "img/lab-icons/mri-scan.png";
                } else if (type.indexOf("CT") != -1) {
                    return "img/lab-icons/ct-scan.png";
                } else if (type.indexOf("X-RAY") != -1) {
                    return "img/lab-icons/xray.png";
                } else if (type.indexOf("ULT") != -1) {
                    return "img/lab-icons/ultrasound.png";
                }


                return "img/lab-icons/other-scans.png";

            }
            scope.GetTestFromLocalDB = function () {

                if (scope.model.length > 3) {
                    scope.$parent.suggest_labtest(scope.model);
                };
            }
            scope.current = 0;
            scope.selected = true;
            scope.isCurrent = function (index) {
                return scope.current == index;
            };
            scope.setCurrent = function (index) {
                scope.current = index;
            };
        },
        templateUrl: 'templates/appointment/appointment-edit.html'
    }
}])
//-----------Currently note used , as scan tab is removed.
hCueDoctorWebApp.directive('typeaheadscanstemplates', ['$timeout', 'AutoCompleteHelper', function ($timeout, AutoCompleteHelper) {
    return {
        restrict: 'AEC',
        scope: {
            items: '=',
            prompt: '@',
            title: '@',
            subtitle: '@',
            model: '=',
            itemselected: '=',
            onSelect: '&'
        },
        link: function (scope, elem, attrs) {
            scope.handleSelection = function (selectedItem) {
                AutoCompleteHelper.addSelectedItem(selectedItem);
                scope.model = selectedItem;
                scope.itemselected = selectedItem;

                scope.model = scope.model.name;
                scope.current = 0;
                scope.selected = true;
                $timeout(function () {
                    scope.onSelect();
                }, 200);
            };
            scope.current = 0;
            scope.selected = true;
            scope.isCurrent = function (index) {
                return scope.current == index;
            };
            scope.setCurrent = function (index) {
                scope.current = index;
            };
        },
        templateUrl: 'templates/appointment/appointment-edit.html'
    }
}])

hCueDoctorWebApp.directive('toggle', function () {
    return function (scope, elem, attrs) {
        scope.$on('event:toggle', function (ev, num) {
            if (num === attrs.toggle) {
                elem.slideToggle();
            }
        });
    };
});

hCueDoctorWebApp.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});

//Discount Range 0-100 Directive
hCueDoctorWebApp.directive('discountRange', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return '';
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                var newValue = transformedInput;
                if (transformedInput != inputValue) {
                    newValue = transformedInput;
                }
                if (parseInt(transformedInput) < 0 || transformedInput == "00") {
                    newValue = "0";
                } else if (parseInt(transformedInput) > 100) {
                    newValue = transformedInput.slice(0, -1);
                } else if (transformedInput.indexOf("0") == 0 && transformedInput.length > 1) {
                    newValue = transformedInput.substr(1);
                }
                modelCtrl.$setViewValue(newValue);
                modelCtrl.$render();
                return newValue;
            });
        }
    };
});
//End of Discount Range
hCueDoctorWebApp.filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' ?');
    };
});

hCueDoctorWebApp.directive('numbersposOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});


hCueDoctorWebApp.directive('numbersdotsplusminusOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9\.+-]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});

hCueDoctorWebApp.directive('numbersdotsOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9\.:]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});

hCueDoctorWebApp.directive('numbersTime', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9\:]/g, '');
                if (inputValue.length == 2 && inputValue.indexOf(':') == -1) {
                    transformedInput = inputValue + ':'
                } else if (inputValue.indexOf(':') != -1) {
                    if (inputValue.indexOf(':') == 0 && inputValue.length == 4) {
                        transformedInput = inputValue.slice(0, 3)
                    } else if (inputValue.indexOf(':') == 1 && inputValue.length == 5) {
                        transformedInput = inputValue.slice(0, 4)
                    }
                }
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});


hCueDoctorWebApp.directive('numbersOnlyTemp', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue == undefined) return '';
                console.log(inputValue);
                var transformedInput = inputValue.replace(/[^0-9\.]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                var intInput = parseInt(transformedInput);
                if ((intInput < 90 || intInput > 108 || parseFloat(transformedInput) > 108 || inputValue == "108.") && (intInput > 10 || (intInput > 1 && intInput < 9))) {
                    //modelCtrl.$setViewValue(transformedInput);
                    transformedInput = transformedInput.slice(0, -1);
                    if (parseFloat(transformedInput) > 108 || inputValue == "108.") {
                        transformedInput = intInput + "";
                    }
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});
hCueDoctorWebApp.directive('numbersOnlyDisc', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue == undefined) return '';
                console.log(inputValue);
                var transformedInput = inputValue.replace(/[^0-9\.]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                var intInput = parseInt(transformedInput);
                if ((intInput < 1 || intInput > 100 || parseFloat(transformedInput) > 100 || inputValue == "100.") && (intInput > 10 || (intInput > 1 && intInput < 9))) {
                    //modelCtrl.$setViewValue(transformedInput);
                    transformedInput = transformedInput.slice(0, -1);
                    if (parseFloat(transformedInput) > 100 || inputValue == "100.") {
                        transformedInput = intInput + "";
                    }
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});



hCueDoctorWebApp.directive('textBoxCollection', function () {
    return {
        scope: {
            idvalue: '@idvalue',
            idname: '@idname',
            value: '@value',
            charmaxlength: '@charmaxlength',
            property: '=',
            action: '&',
            ddlchange: '&',
            placeholder: '@placeholder',
            showindicator: "="

        },
        restrict: 'EA',
        replace: true,
        templateUrl: 'views/textBoxCollection.html',
        link: function (scope, ele, attr) {
            ele.children('input').attr('maxlength', scope.charmaxlength);
            if (scope.showindicator == false) {
                ele.children('select').hide();
            }
            ele.children('select').on('change', function () {
                if (scope.property['ConditionDesc'].indexOf('-') > -1 || scope.property['ConditionDesc'].indexOf('+') > -1) {
                    scope.property['ConditionDesc'] = scope.property['ConditionDesc'].substr(1);
                }
                if (angular.element(this).val() == "pos") {
                    scope.property['Sign'] = "+";
                    if (scope.property['UnsignedValue'] != "") {
                        scope.property['ConditionDesc'] = "+" + scope.property['ConditionDesc'];
                    }
                } else {
                    scope.property['Sign'] = "-";
                    if (scope.property['UnsignedValue'] != "") {
                        scope.property['ConditionDesc'] = "-" + scope.property['ConditionDesc'];
                    }
                }
            });
            ele.children('input').on('blur', function () {
                if (scope.property['UnsignedValue'] == "") {
                    scope.property['ConditionDesc'] = "";
                } else {
                    scope.property['ConditionDesc'] = scope.property['Sign'] + scope.property['UnsignedValue'];
                }

            });
        }
    };

});

hCueDoctorWebApp.directive('checkBoxCollection', function () {
    return {
        scope: {
            descvalue: '@descvalue',
            idname: '@idname',
            value: '@value',
            property: '=',
            action: '&'
        },
        restrict: 'EA',
        replace: true,
        templateUrl: 'views/checkBoxCollection.html'
    };

});
//senthil code

//senthil code
hCueDoctorWebApp.directive('awLimitLength', function () {
    return {
        restrict: "A",
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            attrs.$set("ngTrim", "false");
            var limitLength = parseInt(attrs.awLimitLength, 10); // console.log(attrs);
            scope.$watch(attrs.ngModel, function (newValue) {
                if (ngModel.$viewValue) {
                    if (ngModel.$viewValue.length > limitLength) {
                        ngModel.$setViewValue(ngModel.$viewValue.substring(0, limitLength));
                        ngModel.$render();
                    }
                }
            });
        }
    };
});


hCueDoctorWebApp.directive('mySrc', function () {
    return {
        restrict: 'A',

        link: function (scope, elem, attrs) {
            //replace test with whatever myFunction() does
            //alert(attrs.type);

            var type = attrs.type.toUpperCase();
            var rtnImageUrl = "img/medicine-icons/Others.svg";
            if (type.indexOf("INJ") != -1) {
                rtnImageUrl = "img/medicine-icons/Injection.svg";
            } else if (type == "T" || type.indexOf("TAB") != -1) {
                rtnImageUrl = "img/medicine-icons/Tablet.svg";
            } else if (type.indexOf("CAP") != -1) {
                rtnImageUrl = "img/medicine-icons/Capsules.svg";
            } else if (type.indexOf("SYR") != -1 || type.indexOf("SUS") != -1 || type.indexOf("LIQ") != -1 ||
                type.indexOf("SOL") != -1 || type.indexOf("MOU") != -1) {
                rtnImageUrl = "img/medicine-icons/Syrup.svg";
            } else if (type.indexOf("CRE") != -1) {
                rtnImageUrl = "img/medicine-icons/Cream.svg";
            } else if (type.indexOf("OIN") != -1) {
                rtnImageUrl = "img/medicine-icons/Ointment.svg";
            } else if (type.indexOf("GEL") != -1) {
                rtnImageUrl = "img/medicine-icons/Gel.svg";
            } else if (type.indexOf("DRO") != -1) {
                rtnImageUrl = "img/medicine-icons/Drops.svg";
            } else if (type.indexOf("KIT") != -1) {
                rtnImageUrl = "img/medicine-icons/Kit.svg";
            }
            elem.attr('src', rtnImageUrl);
        }
    };
});


hCueDoctorWebApp.directive('mySrclab', function () {
    return {
        restrict: 'A',

        link: function (scope, elem, attrs) {
            //replace test with whatever myFunction() does
            //alert(attrs.type);


            var type = attrs.type.toUpperCase();
            var rtnImageUrl = "img/lab-icons/labtest.png";
            if (type.indexOf("TEST") != -1) {
                rtnImageUrl = "img/lab-icons/labtest.png";
            } else if (type.indexOf("SCAN") != -1) {
                rtnImageUrl = "img/lab-icons/CT.png";
            }
            elem.attr('src', rtnImageUrl);
        }
    };
});

hCueDoctorWebApp.directive('typeaheadscan', ['$timeout', 'AutoCompleteHelper', function ($timeout, AutoCompleteHelper) {
    return {
        restrict: 'AEC',
        scope: {
            items: '=',
            prompt: '@',
            title: '@',
            subtitle: '@',
            model: '=',
            itemselected: '=',
            onSelect: '&'
        },
        link: function (scope, elem, attrs) {
            scope.handleSelection = function (selectedItem) {
                AutoCompleteHelper.addSelectedItem(selectedItem);
                scope.model = selectedItem;
                scope.itemselected = selectedItem;
                scope.model = scope.model.LabTestName; // column name
                scope.current = 0;
                scope.selected = true;
                $timeout(function () {
                    scope.onSelect();
                }, 200);
            };
            scope.GetScanImage = function (type) {
                var type = type.toUpperCase();
                if (type.indexOf("MRI") != -1) {
                    return "img/lab-icons/mri-scan.png";
                } else if (type.indexOf("CT") != -1) {
                    return "img/lab-icons/ct-scan.png";
                } else if (type.indexOf("X-RAY") != -1) {
                    return "img/lab-icons/xray.png";
                } else if (type.indexOf("ULT") != -1) {
                    return "img/lab-icons/ultrasound.png";
                } else if (type.indexOf("Gel") != -1) {
                    return "img/lab-icons/Gel.png";
                }

                return "img/lab-icons/other-scans.png";

            }
            scope.GetScanFromLocalDB = function () {

                if (scope.model.length > 3) {
                    scope.$parent.suggest_scan(scope.model);
                };
            }
            scope.current = 0;
            scope.selected = true;
            scope.isCurrent = function (index) {
                return scope.current == index;
            };
            scope.setCurrent = function (index) {
                scope.current = index;
            };
        },
        templateUrl: 'templates/consultation/autocomplete/scan-tests.html'
    }
}])
//-----------Currently note used , as scan tab is removed.10/11/2015


//bala code
hCueDoctorWebApp.directive('dhxScheduler', function () {
    return {
        restrict: 'A',
        scope: false,
        transclude: true,
        template: '<div class="dhx_cal_navline" ng-transclude></div><div class="dhx_cal_header"></div><div class="dhx_cal_data"></div>',



        link: function ($scope, $element, $attrs, $controller) {
            //default state of the scheduler
            if (!$scope.scheduler)
                $scope.scheduler = {};
            $scope.scheduler.mode = $scope.scheduler.mode || "month";
            $scope.scheduler.date = $scope.scheduler.date || new Date();

            //watch data collection, reload on changes
            $scope.$watch($attrs.data, function (collection) {
                scheduler.clearAll();
                scheduler.parse(collection, "json");
            }, true);

            //mode or date
            $scope.$watch(function () {
                return $scope.scheduler.mode + $scope.scheduler.date.toString();
            }, function (nv, ov) {
                var mode = scheduler.getState();
                if (nv.date != mode.date || nv.mode != mode.mode)
                    scheduler.setCurrentView($scope.scheduler.date, $scope.scheduler.mode);
            }, true);

            //size of scheduler
            $scope.$watch(function () {
                return $element[0].offsetWidth + "." + $element[0].offsetHeight;
            }, function () {
                scheduler.setCurrentView();
            });

            //styling for dhtmlx scheduler
            $element.addClass("dhx_cal_container");

            //init scheduler
            scheduler.init($element[0], $scope.scheduler.mode, $scope.scheduler.date);
        }
    }
});

hCueDoctorWebApp.directive('dhxTemplate', ['$filter', function ($filter) {
    scheduler.aFilter = $filter;
    return {
        restrict: 'AE',
        terminal: true,
        link: function ($scope, $element, $attrs, $controller) {
            $element[0].style.display = 'none';
            var template = $element[0].innerHTML;
            template = template.replace(/[\r\n]/g, "").replace(/"/g, "\\\"").replace(/\{\{event\.([^\}]+)\}\}/g, function (match, prop) {
                if (prop.indexOf("|") != -1) {
                    var parts = prop.split("|");
                    return "\"+scheduler.aFilter('" + (parts[1]).trim() + "')(event." + (parts[0]).trim() + ")+\"";
                }
                return '"+event.' + prop + '+"';
            });
            var templateFunc = Function('sd', 'ed', 'event', 'return "' + template + '"');
            scheduler.templates[$attrs.dhxTemplate] = templateFunc;
        }
    };
}]);

// arun cde for search directive
hCueDoctorWebApp.directive('focusMe', function ($timeout, $parse) {
    return {
        //scope: true,   // optionally create a child scope
        link: function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
        }
    };
})
hCueDoctorWebApp.directive('emptyTypeahead', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            // this parser run before typeahead's parser
            modelCtrl.$parsers.unshift(function (inputValue) {
                var value = (inputValue ? inputValue : secretEmptyKey);

                // replace empty string with secretEmptyKey to bypass typeahead-min-length check
                modelCtrl.$viewValue = value; // this $viewValue must match the inputValue pass to typehead directive
                return value;
            });

            // this parser run after typeahead's parser
            modelCtrl.$parsers.push(function (inputValue) {
                return inputValue === secretEmptyKey ? '' : inputValue; // set the secretEmptyKey back to empty string
            });
        }
    }
})

//DATA TABLES

hCueDoctorWebApp.directive('dTable', function () {
    return function (scope, element, attrs) {

        // apply DataTable options, use defaults if none specified by user
        var options = {};
        if (attrs.dTable.length > 0) {
            options = scope.$eval(attrs.dTable);
        } else {
            options = {
                "bStateSave": false,
                "iCookieDuration": 2419200,
                /* 1 month */
                "bJQueryUI": true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": false,
                "bInfo": false,
                "bDestroy": true
            };
        }

        // Tell the dataTables plugin what columns to use
        // We can either derive them from the dom, or use setup from the controller
        var explicitColumns = [];
        angular.forEach(element.find('th'), function (elem) {
            explicitColumns.push($(elem).text());
        });
        if (explicitColumns.length > 0) {
            options["aoColumns"] = explicitColumns;
        } else if (attrs.aoColumns) {
            options["aoColumns"] = scope.$eval(attrs.aoColumns);
        }

        // aoColumnDefs is dataTables way of providing fine control over column config
        if (attrs.aoColumnDefs) {
            options["aoColumnDefs"] = scope.$eval(attrs.aoColumnDefs);
        }

        if (attrs.fnRowCallback) {
            options["fnRowCallback"] = scope.$eval(attrs.fnRowCallback);
        }

        // apply the plugin
        var dataTable = element.dataTable(options);
        scope.dataTable = dataTable;
        // watch for any changes to our data, rebuild the DataTable
        scope.$watch(attrs.aaData, function (value) {
            var val = value || null;
            if (val) {
                dataTable.fnClearTable();
                dataTable.fnAddData(scope.$eval(attrs.aaData));
            }
        });
    };
});
// end of search directive
hCueDoctorWebApp.constant("flag", { "value": 0 });
hCueDoctorWebApp.constant("flagD", { "value": 0 });
hCueDoctorWebApp.constant("flagN", { "value": 0 });
hCueDoctorWebApp.constant("flagP", { "value": 0 });
hCueDoctorWebApp.constant("flagL", { "value": 0 });
hCueDoctorWebApp.constant("flagT", { "value": 0 });
hCueDoctorWebApp.constant("flagB", { "value": 0 });
hCueDoctorWebApp.constant("flagC", { "value": 0 });
hCueDoctorWebApp.constant("flagBilling", { "value": 0 });
hCueDoctorWebApp.constant("flagEstimation", { "value": 0 });
hCueDoctorWebApp.constant("flagConditions", { "value": 0 });
hCueDoctorWebApp.constant("flagConsultation", { "value": 0 });
hCueDoctorWebApp.constant("flagNewConsultation", { "value": 0 });
hCueDoctorWebApp.constant("flagAuthorize", { "value": 0 });
hCueDoctorWebApp.constant("flagForLabOrders", { "value": 0 });
//DatePicker Directive
hCueDoctorWebApp.directive('jqdatepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(function () {
                element.datepicker({
                    dateFormat: 'dd-mm-yy',
                    //minDate: 0,
                    onSelect: function (date) {
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    }
                });
            });
        }
    };
});
hCueDoctorWebApp.directive('jqdatepickeradmin', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(function () {
                element.datepicker({
                    dateFormat: 'dd-mm-yy',
                    maxDate: 0,
                    onSelect: function (date) {
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    }
                });
            });
        }
    };
});
hCueDoctorWebApp.directive("datepickerminmaxdate", function () {
    return {
        restrict: "A",
        require: "ngModel",
        scope: { mindate: '@' },
        link: function (scope, elem, attrs, ngModelCtrl) {
            var max = "";
            var min = new Date();
            scope.$watch('mindate', function (val) {
                if (val) {
                    min = new Date(val);
                    elem.datepicker('option', 'minDate', min);
                    console.log(new Date().setDate(new Date(val).getDate() + 15));
                    max = new Date(new Date(new Date(val).getTime() + 15 * 1000 * 3600 * 24));
                    elem.datepicker('option', 'maxDate', max);
                }
            }, true);
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };
            var options = {
                dateFormat: 'dd-mm-yy',
                //minDate:min,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };
            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});
hCueDoctorWebApp.filter("dateFilter", function () {
    return function (item) {
        if (item != null) {
            //     console.log(item);
            return new Date(parseInt(item));
        }
        return "";
    };
});

// end of search directive

hCueDoctorWebApp.directive('scrollToItem', function () {
    return {
        restrict: 'A',
        scope: {
            scrollTo: "@"
        },
        link: function (scope, $elm, attr) {

            $elm.on('click', function () {
                $('html,body').animate({ scrollTop: $(scope.scrollTo).offset().top }, "slow");
            });
        }
    }
});


//loader
hCueDoctorWebApp.factory('httpInterceptor', function ($q, $rootScope, $log) {

    var numLoadings = 0;

    return {
        request: function (config) {

            /*
         numLoadings++;
         $rootScope.$broadcast("loader_show");
         return config || $q.when(config)
         */
            $rootScope.$emit('chkOfflineMode', {});
            //console.log(" chkOfflineMode emit");
            if (config.method == "GET") {
                if (config.loaderStatus == undefined) {
                    numLoadings++;
                }
                // Show loader
                //$rootScope.$broadcast("loader_show");
                return config || $q.when(config);
            } else {
                if (config.data !== undefined) {
                    if (config.data.loaderStatus == undefined) {
                        numLoadings++;
                        // Show loader
                        $rootScope.$broadcast("loader_show");
                        return config || $q.when(config)
                    } else {

                        numLoadings++;
                        // Show loader
                        //$rootScope.$broadcast("loader_show");
                        return config || $q.when(config)
                    }
                } else {
                    numLoadings++;
                    // Show loader
                    $rootScope.$broadcast("loader_show");
                    return config || $q.when(config)
                }
            }

        },
        response: function (response) {
            if (response.config.loaderStatus == undefined) {
                if ((--numLoadings) === 0) {
                    // Hide loader
                    $rootScope.$broadcast("loader_hide");
                }
            }
            return response || $q.when(response);

        },
        responseError: function (response) {
            if (response.config.loaderStatus == undefined) {
                if (!(--numLoadings)) {
                    // Hide loader
                    $rootScope.$broadcast("loader_hide");
                }
            }
            return $q.reject(response);
        }
    };
});

hCueDoctorWebApp.directive("loader", function ($rootScope) {
    return function ($scope, element, attrs) {
        $scope.$on("loader_show", function () {
            return element.show();
        });
        return $scope.$on("loader_hide", function () {
            return element.hide();
        });
    };
});

if (JSON.parse(localStorage.getItem("isAppOnline"))) {
    var placesAutoComplete = function (Attr2MapOptions, $timeout) {
        var parser = Attr2MapOptions;

        var linkFunc = function (scope, element, attrs, ngModelCtrl) {
            if (attrs.placesAutoComplete === 'false') {
                return false;
            }
            var filtered = parser.filter(attrs);
            var options = parser.getOptions(filtered, { scope: scope });
            var events = parser.getEvents(scope, filtered);
            var autocomplete = new google.maps.places.Autocomplete(element[0], options);
            for (var eventName in events) {
                google.maps.event.addListener(autocomplete, eventName, events[eventName]);
            }

            var updateModel = function () {
                $timeout(function () {
                    ngModelCtrl && ngModelCtrl.$setViewValue(element.val());
                }, 100);
            };
            google.maps.event.addListener(autocomplete, 'place_changed', updateModel);
            element[0].addEventListener('change', updateModel);

            attrs.$observe('types', function (val) {
                if (val) {
                    var optionValue = parser.toOptionValue(val, { key: 'types' });
                    autocomplete.setTypes(optionValue);
                }
            });
        };

        return {
            restrict: 'A',
            require: '?ngModel',
            link: linkFunc
        };
    };

    placesAutoComplete.$inject = ['Attr2MapOptions', '$timeout'];
    hCueDoctorWebApp.directive('placesAutoComplete', placesAutoComplete);
}


hCueDoctorWebApp.directive('popoverElem', function () {
    return {
        link: function (scope, element, attrs) {
            element.on('click', function () {
                element.addClass('trigger');
            });
        }
    }
});

// Register patient
hCueDoctorWebApp.directive("datepickeraddpatient", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: 'dd-mm-yy',
                yearRange: '1900:+0',
                maxDate: 0,
                changeYear: true,
                changeMonth: true,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});

hCueDoctorWebApp.directive('validatealphanumeric', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, elem, attr, ngModel) {

            var validator = function (value) {
                if (/^[a-zA-Z0-9]*$/.test(value)) {
                    ngModel.$setValidity('alphanumeric', true);
                    return value;
                } else {
                    ngModel.$setValidity('alphanumeric', false);
                    return undefined;
                }
            };
            ngModel.$parsers.unshift(validator);
            ngModel.$formatters.unshift(validator);
        }
    };
});
hCueDoctorWebApp.directive('onlyLettersInput', onlyLettersInput);

function onlyLettersInput() {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^a-zA-Z]/g, '');
                //console.log(transformedInput);
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
};


hCueDoctorWebApp.directive('onlyAlphabets', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^0-9a-zA-Z]/g, '');
                console.log(transformedInput);
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

hCueDoctorWebApp.directive('onlyAlphabetsforfamilyid', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^a-zA-Z]/g, '');
                console.log(transformedInput);
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

hCueDoctorWebApp.directive('jqdatepickerconsultation', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(function () {
                element.datepicker({
                    yearRange: '1900:+0',
                    dateFormat: 'dd-mm-yy',
                    maxDate: 0,
                    changeYear: true,
                    changeMonth: true,
                    onSelect: function (date) {
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    }
                });
            });
        }
    };
});

hCueDoctorWebApp.directive("datepickerappointmentbill", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: 'dd-mm-yy',
                maxDate: 0,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});


hCueDoctorWebApp.directive('jqdatepickerlaborder', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(function () {
                element.datepicker({
                    dateFormat: 'dd-mm-yy',
                    onSelect: function (date) {
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    }
                });
            });
        }
    };
});

hCueDoctorWebApp.filter('comma', function () {
    return function (input) {
        if (!isNaN(input)) {

            //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            if (result.length > 1) {
                output += "." + result[1];
            }

            return output;
        }
    }
});
//Text box growup

hCueDoctorWebApp.constant('msdElasticConfig', {
    append: ''
});
hCueDoctorWebApp.directive('msdElastic', [
    '$timeout', '$window', 'msdElasticConfig',
    function ($timeout, $window, config) {
        'use strict';

        return {
            require: 'ngModel',
            restrict: 'A, C',
            link: function (scope, element, attrs, ngModel) {
                var ta = element[0],
                    $ta = element;
                if (ta.nodeName !== 'TEXTAREA' || !$window.getComputedStyle) {
                    return;
                }
                $ta.css({
                    'overflow': 'hidden',
                    'overflow-y': 'hidden',
                    'word-wrap': 'break-word'
                });
                var text = ta.value;
                ta.value = '';
                ta.value = text;

                var append = attrs.msdElastic ? attrs.msdElastic.replace(/\\n/g, '\n') : config.append,
                    $win = angular.element($window),
                    mirrorInitStyle = 'position: absolute; top: -999px; right: auto; bottom: auto;' +
                        'left: 0; overflow: hidden; -webkit-box-sizing: content-box;' +
                        '-moz-box-sizing: content-box; box-sizing: content-box;' +
                        'min-height: 0 !important; height: 0 !important; padding: 0;' +
                        'word-wrap: break-word; border: 0;',
                    $mirror = angular.element('<textarea aria-hidden="true" tabindex="-1" ' +
                        'style="' + mirrorInitStyle + '"/>').data('elastic', true),
                    mirror = $mirror[0],
                    taStyle = getComputedStyle(ta),
                    resize = taStyle.getPropertyValue('resize'),
                    borderBox = taStyle.getPropertyValue('box-sizing') === 'border-box' ||
                        taStyle.getPropertyValue('-moz-box-sizing') === 'border-box' ||
                        taStyle.getPropertyValue('-webkit-box-sizing') === 'border-box',
                    boxOuter = !borderBox ? { width: 0, height: 0 } : {
                        width: parseInt(taStyle.getPropertyValue('border-right-width'), 10) +
                            parseInt(taStyle.getPropertyValue('padding-right'), 10) +
                            parseInt(taStyle.getPropertyValue('padding-left'), 10) +
                            parseInt(taStyle.getPropertyValue('border-left-width'), 10),
                        height: parseInt(taStyle.getPropertyValue('border-top-width'), 10) +
                            parseInt(taStyle.getPropertyValue('padding-top'), 10) +
                            parseInt(taStyle.getPropertyValue('padding-bottom'), 10) +
                            parseInt(taStyle.getPropertyValue('border-bottom-width'), 10)
                    },
                    minHeightValue = parseInt(taStyle.getPropertyValue('min-height'), 10),
                    heightValue = parseInt(taStyle.getPropertyValue('height'), 10),
                    minHeight = Math.max(minHeightValue, heightValue) - boxOuter.height,
                    maxHeight = parseInt(taStyle.getPropertyValue('max-height'), 10),
                    mirrored,
                    active,
                    copyStyle = ['font-family',
                        'font-size',
                        'font-weight',
                        'font-style',
                        'letter-spacing',
                        'line-height',
                        'text-transform',
                        'word-spacing',
                        'text-indent'
                    ];
                if ($ta.data('elastic')) {
                    return;
                }
                maxHeight = maxHeight && maxHeight > 0 ? maxHeight : 9e4;
                if (mirror.parentNode !== document.body) {
                    angular.element(document.body).append(mirror);
                }
                $ta.css({
                    'resize': (resize === 'none' || resize === 'vertical') ? 'none' : 'horizontal'
                }).data('elastic', true);

                function initMirror() {
                    var mirrorStyle = mirrorInitStyle;

                    mirrored = ta;
                    taStyle = getComputedStyle(ta);
                    angular.forEach(copyStyle, function (val) {
                        mirrorStyle += val + ':' + taStyle.getPropertyValue(val) + ';';
                    });
                    mirror.setAttribute('style', mirrorStyle);
                }

                function adjust() {
                    var taHeight,
                        taComputedStyleWidth,
                        mirrorHeight,
                        width,
                        overflow;

                    if (mirrored !== ta) {
                        initMirror();
                    }
                    if (!active) {
                        active = true;

                        mirror.value = ta.value + append; // optional whitespace to improve animation
                        mirror.style.overflowY = ta.style.overflowY;

                        taHeight = ta.style.height === '' ? 'auto' : parseInt(ta.style.height, 10);

                        taComputedStyleWidth = getComputedStyle(ta).getPropertyValue('width');
                        if (taComputedStyleWidth.substr(taComputedStyleWidth.length - 2, 2) === 'px') {
                            width = parseInt(taComputedStyleWidth, 10) - boxOuter.width;
                            mirror.style.width = width + 'px';
                        }

                        mirrorHeight = mirror.scrollHeight;

                        if (mirrorHeight > maxHeight) {
                            mirrorHeight = maxHeight;
                            overflow = 'scroll';
                        } else if (mirrorHeight < minHeight) {
                            mirrorHeight = minHeight;
                        }
                        mirrorHeight += boxOuter.height;
                        ta.style.overflowY = overflow || 'hidden';

                        if (taHeight !== mirrorHeight) {
                            scope.$emit('elastic:resize', $ta, taHeight, mirrorHeight);
                            ta.style.height = mirrorHeight + 'px';
                        }
                        $timeout(function () {
                            active = false;
                        }, 1, false);

                    }
                }

                function forceAdjust() {
                    active = false;
                    adjust();
                }
                if ('onpropertychange' in ta && 'oninput' in ta) {
                    // IE9
                    ta['oninput'] = ta.onkeyup = adjust;
                } else {
                    ta['oninput'] = adjust;
                }

                $win.bind('resize', forceAdjust);

                scope.$watch(function () {
                    return ngModel.$modelValue;
                }, function (newValue) {
                    forceAdjust();
                });

                scope.$on('elastic:adjust', function () {
                    initMirror();
                    forceAdjust();
                });

                $timeout(adjust, 0, false);

                scope.$on('$destroy', function () {
                    $mirror.remove();
                    $win.unbind('resize', forceAdjust);
                });
            }
        };
    }
]);



hCueDoctorWebApp.filter('firstUpper', function () {
    return function (input, scope) {
        return input ? input.substring(0, 1).toUpperCase() + input.substring(1) : "";
    }
});

hCueDoctorWebApp.directive('pwCheck', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.pwCheck;
            elem.add(firstPassword).on('keyup', function () {
                scope.$apply(function () {
                    // console.info(elem.val() === $(firstPassword).val());
                    ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
                });
            });
        }
    }
}]);

hCueDoctorWebApp.directive('clickOff', function ($parse, $document) {
    var dir = {
        compile: function ($element, attr) {
            // Parse the expression to be executed
            // whenever someone clicks _off_ this element.
            var fn = $parse(attr["clickOff"]);
            return function (scope, element, attr) {
                // add a click handler to the element that
                // stops the event propagation.
                element.bind("click", function (event) {
                    event.stopPropagation();
                });
                angular.element($document[0].body).bind("click", function (event) {
                    scope.$apply(function () {
                        fn(scope, { $event: event });
                    });
                });
            };
        }
    };
    return dir;
});
hCueDoctorWebApp.filter('unique', function () {

    return function (items, filterOn) {

        if (filterOn === false) {
            return items;
        }

        if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
            var hashCheck = {},
                newItems = [];

            var extractValueToCompare = function (item) {
                if (angular.isObject(item) && angular.isString(filterOn)) {
                    return item[filterOn];
                } else {
                    return item;
                }
            };

            angular.forEach(items, function (item) {
                var valueToCheck, isDuplicate = false;

                for (var i = 0; i < newItems.length; i++) {
                    if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (!isDuplicate) {
                    newItems.push(item);
                }

            });
            items = newItems;
        }
        return items;
    };
});
hCueDoctorWebApp.filter("emptyToEnd", function () {
    return function (array, key) {
        if (!angular.isArray(array)) return;
        var present = array.filter(function (item) {
            return item[key];
        });
        var empty = array.filter(function (item) {
            return !item[key]
        });
        return present.concat(empty);
    };
});
hCueDoctorWebApp.filter('startFrom', function () {
    return function (input, start) {
        if (input != undefined) {
            start = +start; //parse to int
            return input.slice(start);
        }
    }
});
hCueDoctorWebApp.filter('removeHTMLTags', function () {
    return function (text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
});
var fnIsonline = function () {
    console.log(JSON.parse(localStorage.getItem("isAppOnline")));
    return JSON.parse(localStorage.getItem("isAppOnline"));
}
hCueDoctorWebApp.filter('removeHTMLTags', function () {
    return function (text) {
        var html = document.createElement('div');
        html.innerHTML = text;
        return text ? html.innerText : '';
    };
});
var chkOffline = fnIsonline();


hCueDoctorWebApp.constant('hcueOfflineServiceUrl', {
    //'offlineHostURL': 'https://192.168.0.8:8012',
    //'offlineHostURL': 'https://192.168.1.100:8012',
    'offlineHostURL': 'https://localhost:8012',
    'chkOffLine': chkOffline,
    'pharmaurl': 'DOCPHARMAREF',
    'getLabListByDoctorId': 'DOCLABREF',
    'getDoctorRefList': 'DOCREFER',
    'updatePatient': 'UPDATPATIENT',
    'updatePatientEnquiry': 'UPDATEPATIENTENQUIRY',
    'docAddAppointment': 'DOCADDAPPOINTMENT',
    'addPatientvitals': 'ADDCASE',
    'getDoctorsAppointments': 'DOCAPPOINTMENTS',
    'getHospitalAppointments': 'DOCHOSPAPPOINTMENTS',
    'referalread': 'DOCREFERSETTING',
    'addPatient': 'ADDPATIENT',
    //'addAllPatientCase': 'ADDPATIENTCASEDETAIL',
    'getdoctorPatient': 'DOCPATIENT',
    'readDoctorHistorySetting': 'DOCHISTSETTING',
    'readdiagnosis': 'DOCDIAGNOSISSETTING',
    'readVisitReason': 'DOCVISITREASON',
    'updtDoctorAppointments': 'UPDDOCAPPOINTMENT',
    'updatePatientAppointments': 'DOCUPDAPPOINTMENT',
    'getStatusvalues': 'STATE',
    'getCountryvalues': 'COUNTRY',
    'getlocationvalues': 'LOCATION',
    'getcityvalues': 'CITY',
    'readLookup': 'LOOKUP',
    'addConsultation': 'ADDCONSULT',
    'updateConsultation': 'UPDCONSULT',
    'updateDoctor': 'UPDDOCTOR',
    'addUpdateDoctorVitalSetting': 'UPDVITALSETTING',
    'validateDoctorLogin': 'authenticate',
    'getTemplatebyDoctorID': 'DOCMEDTEMP',
    'docProfile': 'DOCPROFILE',
    'readVitals': 'DOCVITALSETTING',
    'readDentalDetailsByDoctorId': 'DOCPATSETTINGHISTORY',
    'getDoctorAvailAppointment': 'DOCAVAILABLEAPPOINTMENTS',
    'listDoctorProcedure': 'DOCPROCEDURES',
    'pharmaSearch': 'DOCPHARMASEARCH',
    'labSearch': 'DOCLABSEARCH',
    'gethcueDoctors': 'DOCTORLIST',
    'getClinicDoctors': 'APPDOCTORLIST',
    'addDoctor': 'ADDDOCTOR',
    'hcueDoctor': 'ADDHOSPITAL',
    'addDoctorLab': 'ADDREFERLAB',
    'addupdateBillingProcedure': 'UPDPROCCATALOG',
    'addDoctorHistorySetting': 'UPDDOCHISTSETTING',
    'visitReasonAddUpdate': 'UPDVISITSETTING',
    'diagnosisaddUpdate': 'UPDDOCDIAGNOSISSETTING',
    'referalAddUpdate': 'UPDDOCREFERSETTING',
    'getAppointmentStatus': 'APPOINTMENTSTATUS',
    'listPatientCase': 'PATIENTCASEHISTORY',
    'getpatientvitals': 'PATIENTVITAL',
    'getPatient': 'PATIENT',
    'addAllOffinePatientCase': 'OFFLINEADDPATIENTCASEDETAIL',
    'getSMSpatients': 'DOCPATIENT',
    'getBillingInfo': 'PATIENTBILLINFO',
    'docpastlabhistory': 'DOCPASTLABHISTORY',
    // 'docpastlabhistory': 'PASTLABHISTORYINFO',
    'onlineAppointment': 'ONLINEAPPOINTMENT',
    'offlineDoctorData': 'DOCTOR',
    'getHospitalPaymentCompanyList': 'GETHOSPITALPAYMENTCOMPANYLIST',
    'listDocSubCategorySettings': 'LISTDOCSUBCATEGORYSETTINGS',
    'offlinePFID': 'FAMILYID',
    'loginStatus': 'loginStatus'

})



// c7

hCueDoctorWebApp.directive('referraldatepicker', function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: 'dd-mm-yy',
                yearRange: '1900:+200',
                showAnim: 'slideDown',
                changeYear: true,
                changeMonth: true,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };
            //    if() {
            //        options.minDate = 
            //    }
            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});
hCueDoctorWebApp.directive('referraldatepickerone', function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                maxDate: moment().format('DD-MM-YYYY'),
                dateFormat: 'dd-mm-yy',
                yearRange: '1900:+200',
                showAnim: 'slideDown',
                changeYear: true,
                changeMonth: true,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };
            //    if() {
            //        options.minDate = 
            //    }
            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});
hCueDoctorWebApp.directive('referraldatepickermin', function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                minDate: moment().format('DD-MM-YYYY'),
                dateFormat: 'dd-mm-yy',
                yearRange: '1900:+200',
                showAnim: 'slideDown',
                changeYear: true,
                changeMonth: true,
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };
            //    if() {
            //        options.minDate = 
            //    }
            // jqueryfy the element
            elem.datepicker(options);
        }
    }
});
hCueDoctorWebApp.filter('convertdate', function () {
    return function (item) {
        if (typeof item == "string") {
            return moment(item, 'YYYY-MM-DD').format('DD-MM-YYYY');
        } else {
            return moment(item).format('DD-MM-YYYY');
        }
    };
});

hCueDoctorWebApp.filter('getspecialitycoll', function () {
    return function (item) {
        var arr = [];
        if (item.length > 0) {
            for (var i = 0; i < item.length; i++) {
                arr.push(item[i].speciality);
            }
        }
        return arr.join(', ');
    };
});

hCueDoctorWebApp.directive('convertToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return val != null ? parseInt(val, 10) : null;
            });
            ngModel.$formatters.push(function (val) {
                return val != null ? '' + val : null;
            });
        }
    };
});
hCueDoctorWebApp.directive('customOnChange', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var onChangeFunc = scope.$eval(attrs.customOnChange);
            element.bind('change', onChangeFunc);
        }
    };
});

hCueDoctorWebApp.filter('enquirystatus', function () {
    var enquiryobj = [{
        id: 'TRIAGE',
        desc: 'To Triage'
    },
    {
        id: 'AWAITING',
        desc: 'Awaiting Appointment'
    },
    {
        id: 'BOOKED',
        desc: 'Booked'
    },
    {
        id: 'SCANCOMPLETED',
        desc: 'Scan Completed'
    },
    {
        id: 'AWAIT2CALL',
        desc: 'Awaiting 2nd Call'
    },
    {
        id: 'AWAIT3CALL',
        desc: 'Awaiting 3rd Call'
    },
    {
        id: 'AWAIT4CALL',
        desc: 'Awaiting 4th Call'
    },
    {
        id: 'FINALCALL',
        desc: 'Final Call'
    },
    {
        id: 'INACTIVE',
        desc: 'Inactive'
    },
    {
        id: 'COMPLETE',
        desc: 'Completed / Published'
    },
    {
        id: 'REJECT',
        desc: 'Rejected'
    },
    {
        id: 'ISSUES',
        desc: 'Issues'
    },
    {
        id: 'DNA',
        desc: 'DNA'
    },
    {
        id: 'PUBLISH',
        desc: 'Published'
    },
    {
        id: 'ReferralForm',
        desc: 'Referral Form Opened'
    },
    {
        id: 'ManageForm',
        desc: 'Manage Form Opened'
    },
    {
        id: 'EnquirySeen',
        desc: 'Enquiry Seen'
    },
    {
        id: 'EnquiryModified',
        desc: 'Enquiry Modified'
    },
    {
        id: 'ExceptionNotes',
        desc: 'Exception Notes'
    },
    {
        id: 'appemail',
        desc: 'Email Triggered'
    },
    {
        id: 'appointEmail',
        desc: 'Appointment email sent'
    },
    {
        id: 'appointSMS',
        desc: 'Appointment SMS sent'
    },
    {
        id: 'cancelSMS',
        desc: 'Appointment Cancel SMS sent'
    },
    {
        id: 'dnaSMS',
        desc: 'DNA SMS sent'
    },
    {
        id: 'exclusion',
        desc: 'Exclusion email sent'
    },
    {
        id: 'exclusionEmail',
        desc: 'Exclusion email sent'
    },
    {
        id: 'declinedEmail',
        desc: 'Declined email sent'
    },
    {
        id: 'patientDeclinedEmail',
        desc: 'Declined email sent'
    },
    {
        id: 'dnaEmail',
        desc: 'DNA email sent'
    },
    {
        id: 'contactusEmail',
        desc: 'Contact Us email sent'
    },
    {
        id: 'nocontactEmail',
        desc: 'No Contact email sent'
    },
    {
        id: 'urgentnocontactEmail',
        desc: 'Urgent No Contact email sent'
    },
    {
        id: 'DOCTORREFERRAL',
        desc: 'Referral From Doctor'
    },
    {
        id: 'B',
        desc: 'Appointment Booked'
    },
    {
        id: 'C',
        desc: 'Cancelled'
    },
    {
        id: 'SC',
        desc: 'Scan Completed'
    },
    {
        id: 'FC',
        desc: 'Feedback Completed'
    },
    {
        id: 'VR',
        desc: 'Verification Required'
    },
    {
        id: 'RS',
        desc: 'Appointment Rescheduled'
    },
    {
        id: 'SECOND DNA',
        desc: 'Second DNA'
    },
    {
        id: 'LETTERSENT',
        desc: 'Final Call Letter Sent'
    }, {
        id: 'LATECANCELLATION',
        desc: 'Late Cancellation'
    }, {
        id: 'LC',
        desc: 'Late Cancellation'
    },
    {
        id: 'ReminderAdded',
        desc: 'Reminder Added'
    },
    {
        id: 'ReminderCompleted',
        desc: 'Reminder Completed'
    },
    {
        id: 'DocumentUpload',
        desc: 'Document Upload'
    }, {
        id: 'Reasonnotes',
        desc: 'Issue Reason Notes Updated'
    }, {
        id: 'Notes',
        desc: 'Notes Updated'
    },
    {
        id: 'PatientDetail',
        desc: 'Patient Detail Edited'
    },
    {
        id: 'ReferralDetail',
        desc: 'Referral Detail Edited'
    }
    ];

    return function (item) {
        if (item != undefined) {
            return enquiryobj.filter(data => data.id == item)[0].desc;
        }
    };
})
hCueDoctorWebApp.filter('trustUrl', function ($sce) {
    return function (url) {
        return $sce.trustAsResourceUrl(url);
    };
});

hCueDoctorWebApp.filter('patientFilter', function () {
    return function (array, data) {
        if (!array) {
            return;
        } else if (!data) {
            return array;
        } else {
            var term = data.toLowerCase();
            return array.filter(function (item) {
                var pname = item.patientname.toLowerCase().indexOf(term) > -1;
                var dwid = item.dwid.toString().indexOf(term) > -1;
                return pname || dwid;
            });
        }
    };
});
hCueDoctorWebApp.filter('patientFilterOne', function () {
    return function (array, data) {
        if (!array) {
            return;
        } else if (!data) {
            return array;
        } else {
            var term = data.toLowerCase();
            return array.filter(function (item) {
                var pname = item.PatientName.toLowerCase().indexOf(term) > -1;
                var dwid = (item.PhoneDetails !== null && item.PhoneDetails.indexOf(term) > -1) ? true : false;
                return pname || dwid;
            });
        }
    };
});

hCueDoctorWebApp.directive('daterangepicker', function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                // startDate: moment().add(1, 'days').format('DD-MM-YYYY'),
                // endDate: moment().add(5, 'days').format('DD-MM-YYYY'),
                autoApply: true,
                showWeekNumbers: false,
                // minDate: moment().format('DD-MM-YYYY'),
                locale: {
                    format: 'DD-MM-YYYY'
                },
                dateLimit: {
                    days: 7
                }
            };

            function cb(start, end, label) {
                updateModel(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
            }
            elem.daterangepicker(options, cb);
        }
    }
});

hCueDoctorWebApp.directive('daterangedashboardpicker', function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                //    startDate: moment().add(1, 'days').format('DD-MM-YYYY'),
                //    endDate: moment().add(5, 'days').format('DD-MM-YYYY'),
                autoApply: true,
                showWeekNumbers: false,
                maxDate: moment().format('DD-MM-YYYY'),
                locale: {
                    format: 'DD-MM-YYYY'
                }

            };

            function cb(start, end, label) {
                updateModel(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
            }
            elem.daterangepicker(options, cb);
        }
    }
});

hCueDoctorWebApp.directive('daterangedashboardpickers', function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                //    startDate: moment().add(1, 'days').format('DD-MM-YYYY'),
                //    endDate: moment().add(5, 'days').format('DD-MM-YYYY'),
                autoApply: true,
                showWeekNumbers: false,
                // maxDate: moment().format('DD-MM-YYYY'),
                locale: {
                    format: 'DD-MM-YYYY'
                }

            };

            function cb(start, end, label) {
                updateModel(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
            }
            elem.daterangepicker(options, cb);
        }
    }
});

hCueDoctorWebApp.directive('daterangedashboardpickerleft', function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                //    startDate: moment().add(1, 'days').format('DD-MM-YYYY'),
                //    endDate: moment().add(5, 'days').format('DD-MM-YYYY'),
                autoApply: true,
                showWeekNumbers: false,
                maxDate: moment().format('DD-MM-YYYY'),
                locale: {
                    format: 'DD-MM-YYYY'
                },
                opens: 'left',

            };

            function cb(start, end, label) {
                updateModel(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
            }
            elem.daterangepicker(options, cb);
        }
    }
});

hCueDoctorWebApp.directive('daterangereferralpicker', function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };
            var sdate = '';
            var edate = '';
            scope.$watch(attrs['ngModel'], function (val) {
                sDate = val.split(' - ')[0];
                eDate = val.split(' - ')[1];

                setDatePic(sDate, eDate);
            })

            function setDatePic(sDate, eDate) {
                var options = {
                    autoApply: true,
                    showWeekNumbers: false,
                    minDate: moment().format('DD-MM-YYYY'),
                    locale: {
                        format: 'DD-MM-YYYY'
                    },
                    opens: "left"
                };
                if (sDate != '') {
                    options.startDate = sDate;
                }
                if (eDate != '') {
                    options.endDate = eDate;
                }
                elem.daterangepicker(options, cb);
            }
            setDatePic('', '');

            function cb(start, end, label) {
                updateModel(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
            }
        }
    }
});

hCueDoctorWebApp.filter('sublistsort', function () {
    return function (item) {
        if (item != undefined && item.length > 0) {
            return item.sort(function (a, b) {
                b.timestamp - a.timestamp;
            })
        }
    };
});
hCueDoctorWebApp.directive('autoFillSync', function ($timeout) {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ngModelCtrl) {
            var origVal = elem.val();
            $timeout(function () {
                var newVal = elem.val();
                if (ngModelCtrl.$pristine && origVal !== newVal) {
                    ngModelCtrl.$setViewValue(newVal);
                }
            }, 500);
        }
    }
});

hCueDoctorWebApp.filter('patientAddress', function () {
    return function (item) {
        // var arr = [];
        // if (item.length > 0) {
        //     for (var i = 0; i < item.length; i++) {
        //         arr.push(item[i].speciality);
        //     }
        // }
        if (typeof item == 'string') {
            return item.split(',').map(v => v.trim()).filter(v => v != '').join(', ');
        }
    };
});