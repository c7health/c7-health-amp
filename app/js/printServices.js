
/* Services */
var hCueDoctorPrintServices = angular.module('hCueDoctorPrintServices', ['ngResource', 'ngCookies', 'LocalStorageModule']);
var header = {
    headers: {
        "Content-Type": "application/json",
        "Accept-Encoding": "gzip"
    
    }
};
var imageheader = {
    headers: {
        "Content-Type": "text/json",
        "Accept-Encoding": "gzip"
    }
};

var urlpath = false;
var documentURL = window.location.origin;
if (documentURL.indexOf('justdental.in'.toLowerCase()) >= 0) {
    urlpath = true;
}
var defaultFontSize = 10;
var setContentSettings=function(){
                return {
                    "patientID":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientName":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                 "patientAge":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                 "patientGender":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                 "patientNumber":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                 "patientEmail":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                 "patientAddress":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                 "billInvoice":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                 "billReceipt":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},

                "patientVisitReason":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},                
                "patientVitals":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "opthalConditions":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "dentalConditions":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientMedicalHistory":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientDentalHistory":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientPastMedical":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientDiagnosisNotes":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientAnalysisNotes":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientNotes":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientPrescription":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientLabTest":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientTreatment":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientTreatmentInProgress":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientBill":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientLabOrder":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientFollowup":{"isSelected":"Y","isBold":"N","fontSize":defaultFontSize},
                "patientReferral": { "isSelected": "Y", "isBold": "N", "fontSize": defaultFontSize },
                "waterMark": { "isSelected": "Y", "isBold": "N", "fontSize": defaultFontSize },
                "PrescriptionNotes": { "isSelected": "Y", "isBold": "N", "fontSize": defaultFontSize },
                "GenericName": { "isSelected": "N", "isBold": "N", "fontSize": 10 },
                "MedicineName": { "isSelected": "N", "isBold": "N", "fontSize": 10 },
                "AfterBeforeMeal": { "isSelected": "Y", "isBold": "N", "fontSize": 10 },
                "BillingTax": { "isSelected": "Y", "isBold": "N", "fontSize": 10 }
                }      
            }

var doctorinfo = "";
var printPageSize="A4";
var contentSetting = setContentSettings();
hCueDoctorPrintServices.service('hcuePrintService', ['datapersistanceFactory', 'datacookieFactory', 'hcueDoctorLoginService', 'hcueConsultationInfo', '$filter', 'hcueServiceUrl', '$http', 'alertify', function (datapersistanceFactory, datacookieFactory, hcueDoctorLoginService, hcueConsultationInfo, $filter, hcueServiceUrl, $http, alertify) {
    var CurrentCaseHistoryItem = [];
    var LaborderPrint = [];
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    //General Functions
    var currencyType = "INR";
    var taxType = "CGST";
    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes !== undefined) {
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.currencyCode !== undefined) {
            currencyType = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.currencyCode;
        }
        if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.taxCode !== undefined) {
            taxType = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.taxCode;
        }
    }

    //get Speciality
    function getSpeciality(doctorinfo) {
        var speciality_data;
        if (doctorinfo.doctordata.SpecilityDetails[0] !== undefined && doctorinfo.doctordata.SpecilityDetails[0] !== '') {
            if (doctorinfo.doctordata.SpecilityDetails!=undefined && doctorinfo.doctordata.SpecilityDetails[0]!=undefined && doctorinfo.doctordata.SpecilityDetails[0].CustomSpecialityDesc !== undefined && doctorinfo.doctordata.SpecilityDetails[0].CustomSpecialityDesc !== '') {
                speciality_data = doctorinfo.doctordata.SpecilityDetails[0].SepcialityDesc + "\n" + "(" + doctorinfo.doctordata.SpecilityDetails[0].CustomSpecialityDesc + ")";
            }
            else {
                speciality_data = doctorinfo.doctordata.SpecilityDetails[0].SepcialityDesc;
            }
        }
        else {
            speciality_data = '';
        }
        return speciality_data;
    }
    //End of get Speciality


    //get Patient Information Table
    function getPatientInfoTable(appointmentinfo,patientinfo,doctorinfo,newdate, displayID,marginRight, pageOrientation) {
        ageSplits={};
        if (patientinfo.PatientDetails.Patient[0].CurrentAge != undefined) {
            ageSplits = { year: patientinfo.PatientDetails.Patient[0].CurrentAge.year, month: patientinfo.PatientDetails.Patient[0].CurrentAge.month };
        } else {
            if (patientinfo.PatientDetails.Patient[0].Age.toString().indexOf('.') !== -1) {
                var ageSplitArray = patientinfo.PatientDetails.Patient[0].Age.toString().split('.');
                ageSplits.year = ageSplitArray[0].toString();
                ageSplits.month = ageSplitArray[1].toString();
            } else {
                ageSplits.year = patientinfo.PatientDetails.Patient[0].Age.toString();
                ageSplits.month = '';
            }
        }
        if (ageSplits.year == undefined) {
            ageSplits.year = '';
        } else {
            ageSplits.year = (ageSplits.year == 0) ? '' : ageSplits.year;
        }
        if (ageSplits.month == undefined) {
            ageSplits.month = '';
        } else {
            ageSplits.month = (ageSplits.month == 0) ? '' : ageSplits.month;
        }
        var Title = appointmentinfo.PatientDetails.Patient[0].Title;
        var PatientName = appointmentinfo.PatientDetails.Patient[0].FullName;
        var Gender = appointmentinfo.PatientDetails.Patient[0].Gender;
        var PatientID = displayID;
        var PatientNumber="";
        if (patientinfo.Phnumber != '') {
            PatientNumber = patientinfo.Phnumber;
        }
        var placeName = (doctorinfo.doctordata.doctorAddress[0] !== undefined) ? doctorinfo.doctordata.doctorAddress[0].Address2 : '';
        var patientDetails = {
            style: 'tableExample',
            table: {
                widths: [60, 5, '*', 60, 5, 150],
                body:[[],[],[],[]]
            },
            layout: 'noBorders'
        };
        var row=0;
        if(contentSetting.patientName.isSelected!="N")
        {   
            patientDetails.table.body[row].push({ text: 'Name'.toUpperCase(),bold:true, fontSize: defaultFontSize });
            patientDetails.table.body[row].push({ text: ':', style: 'tableHeader' });
            patientDetails.table.body[row].push({ text: ((Title !== undefined && Title !== "") ? Title + "." : "") + PatientName.toUpperCase(), color: 'black', fontSize: defaultFontSize });
        }
        patientDetails.table.body[row].push({ text: 'Date'.toUpperCase(),bold:true, fontSize: defaultFontSize });
        patientDetails.table.body[row].push({ text: ':' });
        patientDetails.table.body[row].push({ text: newdate.toUpperCase(), color: 'black', fontSize: defaultFontSize });
        row+=Math.floor(patientDetails.table.body[row].length/6);
        if(contentSetting.patientAge.isSelected!="N" && (ageSplits.year!=""||ageSplits.month!=""))
        {   
            patientDetails.table.body[row].push({ text: 'Age'.toUpperCase(),bold:true, fontSize: defaultFontSize });
            patientDetails.table.body[row].push({ text: ':' });
            patientDetails.table.body[row].push({ text: ((ageSplits.year == '') ? '' : ageSplits.year + " Years") + ' ' + ((ageSplits.month == '') ? '' : ageSplits.month + " Months"), color: 'black', fontSize: defaultFontSize });
        }
        row+=Math.floor(patientDetails.table.body[row].length/6);
        if(contentSetting.patientGender.isSelected!="N"){
            patientDetails.table.body[row].push({ text: 'Gender'.toUpperCase(),bold:true, fontSize: defaultFontSize });
            patientDetails.table.body[row].push({ text: ':' });
            patientDetails.table.body[row].push({ text: ((Gender == 'M') ? "Male" : "Female").toUpperCase(), color: 'black', fontSize: defaultFontSize });
        }
        row+=Math.floor(patientDetails.table.body[row].length/6);
        if(contentSetting.patientID.isSelected=="Y" && PatientID!=undefined && PatientID!=""){
            patientDetails.table.body[row].push({ text: 'Patient ID'.toUpperCase(),bold:true, fontSize: defaultFontSize });
            patientDetails.table.body[row].push({ text: ':', style: 'tableHeader' });
            patientDetails.table.body[row].push({ text: PatientID.toString(), color: 'black', fontSize: defaultFontSize });
        }
        row+=Math.floor(patientDetails.table.body[row].length/6);
        if (contentSetting.patientNumber.isSelected =="Y" && PatientNumber != undefined && PatientNumber != "") {
            patientDetails.table.body[row].push({ text: 'Contact #'.toUpperCase(),bold:true, fontSize: defaultFontSize });
            patientDetails.table.body[row].push({ text: ':', style: 'tableHeader' });
            patientDetails.table.body[row].push({ text: PatientNumber, color: 'black', fontSize: defaultFontSize });
        }
        row += Math.floor(patientDetails.table.body[row].length / 6);
        if (contentSetting.patientEmail.isSelected != "N" && patientinfo.EmailID != '' && patientinfo.EmailID != undefined) {
            patientDetails.table.body[row].push({ text: 'Email'.toUpperCase(), bold: true, fontSize: defaultFontSize });
            patientDetails.table.body[row].push({ text: ':' });
            patientDetails.table.body[row].push({ text: patientinfo.EmailID, color: 'black', fontSize: defaultFontSize });
        }
        row += Math.floor(patientDetails.table.body[row].length / 6);
        if ((contentSetting.patientAddress.isSelected != "N" && patientinfo.Address1 != '' && patientinfo.Address1 != undefined) || (contentSetting.patientAddress.isSelected != "N" && patientinfo.Address2 != '' && patientinfo.Address2 != undefined)) {
            patientDetails.table.body[row].push({ text: 'Address'.toUpperCase(), bold: true, fontSize: defaultFontSize });
            patientDetails.table.body[row].push({ text: ':' });
            patientDetails.table.body[row].push({ text: patientinfo.Address1 + '\n' + patientinfo.Address2, color: 'black', fontSize: defaultFontSize });
        }

        if (patientDetails.table.body[row].length == 3) {
            patientDetails.table.body[row].push({ text: "" }, { text: "" }, { text: "" })
        }
        for(var i=3;i>=0;i--)
            if(patientDetails.table.body[i].length==0)
                patientDetails.table.body.splice(i,1);
		var newLine = (pageOrientation == 'landscape' && printPageSize == 'A4') ? 825-marginRight : 585-marginRight; //A4
        var patientborder={
            "margin": [0, 0, 0, 10],
            "canvas": [{
                "type": "line",
                "x1": 0,
                "y1": 1,
                "x2": newLine,
                "y2": 1,
                "lineWidth": 0.5,
                "lineColor": "#BDBDBD"
            }]
            };
        var content=[{text:"\n"},patientDetails,patientborder];
        return content;
    }
    //End of get Patient Information Table


    //get Doctor Signature
    function getDoctorSignature(footerSettings,screenName,doctorinfo,modepayment, clinicName, doctorFullName, doctorSpeciality, isDefaultDocSign) {
        if (footerSettings != undefined && footerSettings != '' && footerSettings.text != undefined && isDefaultDocSign=="N") {
            footerSettings.bold=footerSettings.isBold=="Y";
            footerSettings.alignment=footerSettings.position.toLowerCase();
            return [{ text: screenName=="BILLING" && modepayment!=""?('Mode of Payment : '+modepayment.toUpperCase()):((screenName=="SUMMARY"&&modepayment!="")?("Next Visit : "+modepayment.toUpperCase()):"").toUpperCase(), fontSize: 10 },
             footerSettings];
        }
        printPageWidth=[100, 5,100, 5, 5, '*'];
        if(printPageSize=="A5"){
            printPageWidth=[60,5,100,5,5,'*'];
        }
        return {
            style: 'tableExample',
            table: {
                widths: printPageWidth,
                body: [
                        [{ text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                             { text: '' },
                        ], [{ text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                             { text: '' },
                        ], [{ text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                             { text: '' },
                        ], [{ text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                             { text: '' },
                        ], [{ text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                             { text: '' },
                        ], [{ text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                             { text: '' },
                        ], [{ text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                             { text: '' },
                        ], [{ text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                             { text: '' },
                        ], [{ text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                             { text: '' },
                        ], [{ text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                             { text: '' },
                        ], [{ text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                             { text: '' },
                        ], [{ text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                             { text: '' },
                        ],
                    [{ text: screenName == "BILLING" && modepayment!=""? 'Mode of Payment : ' : ((screenName == "SUMMARY" && modepayment != "") ? "Next Visit : " : "").toUpperCase(), fontSize: defaultFontSize },
                            { text: "" },
                   { text: screenName == "BILLING" ? modepayment.toUpperCase() : ((screenName == "SUMMARY" && modepayment != "") ? modepayment.toUpperCase() : ""), color: 'black', fontSize: defaultFontSize },
                            { text: '' },
                            { text: '' },
                            { text: (clinicName == undefined) ? "" : "For " + clinicName.toUpperCase(), color: 'black', alignment: 'right', fontSize: defaultFontSize },

                    ],
                            [{ text: '' },
                           { text: '' },
                           { text: '' },
                           { text: '' },
                           { text: '' },
                           { text: (doctorFullName === undefined) ? "Consulted by : Dr. " + doctorinfo.doctordata.doctor[0].FullName.toUpperCase() : "Consulted by Dr. " + doctorFullName.toUpperCase(), alignment: 'right', fontSize: defaultFontSize },
                            ],
                            [{ text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                            { text: '' },
                             { text: (doctorSpeciality===undefined?(getSpeciality(doctorinfo).toUpperCase()):(doctorSpeciality.toUpperCase())), color: 'black', alignment: 'right', fontSize: defaultFontSize },
                            ],

                ]
            },
            layout: 'noBorders'
        };
    }
    //End of get Doctor Signature


    // get Footer Notes
    function getFootNote() {
        return {
            text: (urlpath ? '' : 'This Clinic is powered by hCue \nPlease visit us @ www.hcue.co'),
            style: ['quote', 'small'],
            fontSize:10,
            color: '#ddd'
        }
    }
    // End of get Footer Notes

    //get Visit Reason
    function getVisitReasonDetails(visitreason){
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientVisitReason){
            cBold=contentSetting.patientVisitReason.isBold=="Y";
            cFont=contentSetting.patientVisitReason.fontSize;
        }
        if (visitreason) {
            ////console.log(JSON.stringify(disgnosisnotes));
            if (visitreason !== null && visitreason.length > 0) {
                var symptomnotes=[];
                for(var i=0;i<visitreason.length;i++){
                    symptomnotes.push( {
                        text:visitreason[i],
                        bold:cBold,
                        fontSize: cFont
                    })
                }
                var visitreasonDetails = [{
                    text: 'Visit Reason'.toUpperCase(),
                    bold: true,
                    fontSize: cFont
                }, symptomnotes, { text: ' ' }];
                return visitreasonDetails;
            } else {
                var visitreasonDetails = [{
                    text: ""
                }];
                return visitreasonDetails;
            }

        } else {
            return null;
        }
    }
    //End of get Visit Reason

    //get Medical History table
    function getMedicalHistoryTable(consultinfo){
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientMedicalHistory){
            cBold=contentSetting.patientMedicalHistory.isBold=="Y";
            cFont=contentSetting.patientMedicalHistory.fontSize;
        }
        if (consultinfo.DentalConditionsArray.MedicalHistoryArray==undefined || consultinfo.DentalConditionsArray.MedicalHistoryArray.length == 0)
            return;
        var bodyItem = [[{ text: 'Description'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' },
                          { text: 'Status'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' },
                          { text: 'Notes'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' }]];
        angular.forEach(consultinfo.DentalConditionsArray.MedicalHistoryArray, function (item) {
            var labTestItem = [];
            var labDescription = '';
           
            if ((item.ActiveIND == 'Y' || item.risk == 'R') || (item.Selection == 'Y' || item.Selection == 'R')) {
                labTestItem.push({ text: item.Description.toUpperCase(), color: 'black', fontSize: cFont, bold:cBold });
                if (item.Selection) {
                    var MedicalNotes = (item.Notes == '') ? '' : item.Notes;
                    
                } else {
                    var MedicalNotes = (item.notes == '' || item.notes == undefined) ? '' : item.notes;
                }
                if (item.ActiveIND == 'Y' || item.Selection == 'Y') {
                    labTestItem.push({ text:  'Yes' , color: 'black', fontSize: cFont, bold:cBold });
                  
                }
                else
                {
                    labTestItem.push({ text:  'At risk' , color: 'black', fontSize: cFont, bold:cBold });
                }
            labTestItem.push({ text: MedicalNotes.toUpperCase(), color: 'black', fontSize: cFont, bold:cBold });
                bodyItem.push(labTestItem);
            }
          
        });
        if (bodyItem.length == 1)
            return null;
        var patientLabTestTable = {
            style: 'tableExample',
            table: {
                widths: '*',
                body: bodyItem
            },
                    layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }

        };
        return patientLabTestTable;
    }
    //End of get Medical History table

    //get Dental History table
    function getDentalHistoryTable(consultinfo){
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientDentalHistory){
            cBold=contentSetting.patientDentalHistory.isBold=="Y";
            cFont=contentSetting.patientDentalHistory.fontSize;
        }
        if (consultinfo.DentalConditionsArray.DentalHistoryArray==undefined || consultinfo.DentalConditionsArray.DentalHistoryArray.length == 0)
                    return;
        var bodyItem = [[{ text: 'Description'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' },
                                { text: 'Status'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' },
                                  { text: 'Notes'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' }]];
                angular.forEach(consultinfo.DentalConditionsArray.DentalHistoryArray, function (item) {
                    var labTestItem = [];
                    var labDescription = '';

                    if (item.ActiveIND == 'Y' || item.Selection=='Y') {
                        var labnotes=item.notes||item.Notes;
                        labTestItem.push({ text: item.Description.toUpperCase(), color: 'black', fontSize: cFont,bold:cBold });
                        labTestItem.push({ text: 'Yes', color: 'black', fontSize: cFont,bold:cBold });
                        labTestItem.push({ text: (labnotes == undefined)? '': labnotes.toUpperCase(), color: 'black', fontSize: cFont,bold:cBold });
                        bodyItem.push(labTestItem);
                    }

                });
                if (bodyItem.length == 1)
                    return null;
                var patientLabTestTable = {
                    style: 'tableExample',
                    table: {
                        widths: '*',
                        body: bodyItem
                    },
                    layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
                    
                };
                return patientLabTestTable;
    }
    //End of Dental History table

    //get Vitals Table 1
    function getPatientVitalsTable1(consultinfo){
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientVitals){
            cBold=contentSetting.patientVitals.isBold=="Y";
            cFont=contentSetting.patientVitals.fontSize;
        }if (consultinfo.vitalnoteinfo.vitals.length == 0)
                    return;
                var widthItems = [];
                var headerItems = [];
                var contentItems = [];
                if (consultinfo.vitalnoteinfo.vitals.TMP) {
                    widthItems.push('*');
                    headerItems.push({ text: 'Temp (F)'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
                    contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.TMP, color: 'black', fontSize: cFont,bold:cBold , alignment: 'center' });
                }
                if (consultinfo.vitalnoteinfo.vitals.WGT) {
                    widthItems.push('*');
                    headerItems.push({ text: 'Weight'.toUpperCase()+" (Kg)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
                    contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.WGT, color: 'black', fontSize: cFont,bold:cBold , alignment: 'center' });
                }
                if (consultinfo.vitalnoteinfo.vitals.BPL) {
                    widthItems.push('*');
                    headerItems.push({ text: 'BP'.toUpperCase()+" (Mm/Hg)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
                    contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.BPL + '/' + consultinfo.vitalnoteinfo.vitals.BPH, fontSize: cFont, bold:cBold ,color: 'black', alignment: 'center' });
                }
                if (consultinfo.vitalnoteinfo.vitals.SFT) {
                    widthItems.push('*');
                    headerItems.push({ text: 'Fasting/PP'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
                    contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.SFT + '/' + consultinfo.vitalnoteinfo.vitals.SPP , color: 'black', fontSize: cFont, bold:cBold, alignment: 'center' });
                }
                if (consultinfo.vitalnoteinfo.vitals.BLG && consultinfo.vitalnoteinfo.vitals.BLG !== "select" && consultinfo.vitalnoteinfo.vitals.BLG !== "") {
                    widthItems.push('*');
                    headerItems.push({ text: 'Blood Group'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
                    contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.BLG, color: 'black', fontSize: cFont, bold:cBold, alignment: 'center' });
                }
                if (consultinfo.vitalnoteinfo.vitals.HGT) {
                    widthItems.push('*');
                    headerItems.push({ text: 'Height'.toUpperCase() +" (Cm)", fontSize: cFont, bold:true,alignment: 'center', fillColor:'#eaeaea' });
                    contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.HGT, color: 'black', fontSize: cFont, bold:cBold, alignment: 'center' });
                }
                if (contentItems.length == 0)
                    return null;
                var patientVitalNotes = {
                    style: 'tableExample',
                    table: {
                        widths: widthItems,
                        body: [headerItems, contentItems]
                    },
                    layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
                };
                return patientVitalNotes;
    }
    //End of get Vitals Table 1

    //get Vitals Table 2
    function getPatientVitalsTable2(consultinfo,Bmivalue){
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientVitals){
            cBold=contentSetting.patientVitals.isBold=="Y";
            cFont=contentSetting.patientVitals.fontSize;
        }
        var widthItems = [];
        var headerItems = [];
        var contentItems = [];
        printPageWidth="*";
        //if(printPageSize=="A5"){printPageWidth=45}

        if (consultinfo.vitalnoteinfo.vitals.LPP) {
            widthItems.push(printPageWidth);
            headerItems.push({ text: 'Lipid Profile'.toUpperCase()+" (Mg/DL)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.LPP, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (consultinfo.vitalnoteinfo.vitals.CHOL) {
            widthItems.push(printPageWidth);
            headerItems.push({ text: 'Total Cholesterol'.toUpperCase()+" (Mg/DL)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.CHOL, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (consultinfo.vitalnoteinfo.vitals.HDL) {
            widthItems.push(printPageWidth);
            headerItems.push({ text: 'HDL'.toUpperCase()+" (Mg/DL)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.HDL, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (consultinfo.vitalnoteinfo.vitals.LDL) {
            widthItems.push(printPageWidth);
            headerItems.push({ text: 'LDL'.toUpperCase()+" (Mg/DL)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.LDL, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (consultinfo.vitalnoteinfo.vitals.TRIG) {
            widthItems.push('*');
            headerItems.push({ text: 'Triglycerides'.toUpperCase()+" (Mg/DL)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.TRIG, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (consultinfo.vitalnoteinfo.vitals.SCT) {
            widthItems.push(printPageWidth);
            headerItems.push({ text: 'Serum Creatinine'.toUpperCase()+" (Mg/DL)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.SCT, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (contentItems.length == 0)
            return null;
        var patientVitalNotes = {
            style: 'tableExample',
            table: {
                widths: widthItems,
                body: [headerItems, contentItems]
            },
                    layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
        };
        return patientVitalNotes;
    }
    //End of get Vitals Table 2



    //get Vitals Table 3
    function getPatientVitalsTable3(consultinfo,Bmivalue){
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientVitals){
            cBold=contentSetting.patientVitals.isBold=="Y";
            cFont=contentSetting.patientVitals.fontSize;
        }
        var widthItems = [];
        var headerItems = [];
        var contentItems = [];
        printPageWidth="*";
        //if(printPageSize=="A5"){printPageWidth=45}

        if (consultinfo.vitalnoteinfo.vitals.SRM) {
            widthItems.push(printPageWidth);
            headerItems.push({ text: 'Sugar Random'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.SRM, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (consultinfo.vitalnoteinfo.vitals.PLS) {
            widthItems.push(printPageWidth);
            headerItems.push({ text: 'Pulse Rate'.toUpperCase()+" (Bpm)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.PLS, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (consultinfo.vitalnoteinfo.vitals.HCF) {
            widthItems.push('*');
            headerItems.push({ text: 'Head Circumference'.toUpperCase()+" (Cm)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.HCF, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (Bmivalue) {
            widthItems.push(printPageWidth);
            headerItems.push({ text: 'BMI'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: Bmivalue, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
         if (consultinfo.vitalnoteinfo.vitals.REPRT) {
            widthItems.push('*');
            headerItems.push({ text: 'Respiratory Rate'.toUpperCase()+" (Per Min)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.REPRT, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (consultinfo.vitalnoteinfo.vitals.SPO2) {
            widthItems.push('*');
            headerItems.push({ text: 'SpO2'+" (%)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.SPO2, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (contentItems.length == 0)
            return null;
        var patientVitalNotes = {
            style: 'tableExample',
            table: {
                widths: widthItems,
                body: [headerItems, contentItems]
            },
                    layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
        };
        return patientVitalNotes;
    }
    //End of get Vitals Table 3

    //get Vitals Table 4
    function getPatientVitalsTable4(consultinfo){
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientVitals){
            cBold=contentSetting.patientVitals.isBold=="Y";
            cFont=contentSetting.patientVitals.fontSize;
        }
        var widthItems = [];
        var headerItems = [];
        var contentItems = [];
        if (consultinfo.vitalnoteinfo.vitals.PEFR) {
            widthItems.push('*');
            headerItems.push({ text: 'PEFR'.toUpperCase()+" (L/Min)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.PEFR, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (consultinfo.vitalnoteinfo.vitals.HBA) {
            widthItems.push('*');
            headerItems.push({ text: "HBA1c (%)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.HBA, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (consultinfo.vitalnoteinfo.vitals.WSTC) {
            widthItems.push('*');
            headerItems.push({ text: 'Waist circumference'.toUpperCase()+" (cm)",bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea' });
            contentItems.push({ text: consultinfo.vitalnoteinfo.vitals.WSTC, color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
        }
        if (contentItems.length == 0)
            return null;
        var patientVitalNotes = {
            style: 'tableExample',
            table: {
                widths: widthItems,
                body: [headerItems, contentItems]
            },
                    layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
        };
        return patientVitalNotes;
    }
    //End of get Vitals Table 4

    //Get Diagnosis Notes
    function getDiagnosisNotes(consultinfo,disgnosisnotes) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientDiagnosisNotes){
            cBold=contentSetting.patientDiagnosisNotes.isBold=="Y";
            cFont=contentSetting.patientDiagnosisNotes.fontSize;
        }
        if (consultinfo.treatmentCollection.Diagnostic) {
            ////console.log(JSON.stringify(disgnosisnotes));
            if (disgnosisnotes !== null && disgnosisnotes.length > 0 && disgnosisnotes[0] !== "") {
                var symptomnotes=[];
                for(var i=0;i<disgnosisnotes.length;i++){
                    symptomnotes.push( {
                        text:disgnosisnotes[i],
                        bold:cBold,
                        fontSize: cFont
                    })
                }
                var disgnosisNotesDetails = [{
                    text: 'Diagnosis Notes'.toUpperCase(),
                    bold: true,
                    fontSize: cFont
                }, symptomnotes, { text: ' ' }];
                return disgnosisNotesDetails;
            } else {
                //var disgnosisnotes = "";
                var disgnosisNotesDetails = [{
                    text: ""
                }];
                return disgnosisNotesDetails;
            }

        } else {
            return null;
        }
    }
    //End of Diagnosis Notes

    //Get Investigation
    function getInvestigation(consultinfo,Investgation) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientDiagnosisNotes){
            cBold=contentSetting.patientDiagnosisNotes.isBold=="Y";
            cFont=contentSetting.patientDiagnosisNotes.fontSize;
        }
        if (consultinfo.treatmentCollection.Investigation) {
            ////console.log(JSON.stringify(Investgation));
            if (Investgation !== null && Investgation.length > 0 && Investgation[0] !== "") {
                var symptomnotes=[];
                for(var i=0;i<Investgation.length;i++){
                    symptomnotes.push( {
                        text:Investgation[i],
                        bold:cBold,
                        fontSize: cFont
                    })
                }
                var InvestgationNotesDetails = [{
                    text: 'Observations'.toUpperCase(),
                    bold: true,
                    fontSize: cFont
                }, symptomnotes,
                { text: ' ' }];
                return InvestgationNotesDetails;
            } else {
                //var Investgation = Investgation;
                var InvestgationNotesDetails = [{
                    text: ""
                }];
                return InvestgationNotesDetails;
            }

        } else {
            return null;
        }
    }
    //End of Get Investigation

    //Get Diagnosis Notes Table
    function getDiagnosisNotesTable(consultinfo) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientDiagnosisNotes){
            cBold=contentSetting.patientDiagnosisNotes.isBold=="Y";
            cFont=contentSetting.patientDiagnosisNotes.fontSize;
        }
        if (consultinfo.vitalnoteinfo.notes.symptom) {
            var disgnosisnotes = consultinfo.vitalnoteinfo.notes.symptom;
           
            var disgnosisNotesDetails = [{ text: 'Notes'.toUpperCase(), bold: true, fontSize: cFont },
                                            { text: disgnosisnotes == "" ? '' : disgnosisnotes, fontSize: cFont, color: 'black' , bold:cBold },
                                            { text: '' }];
            return disgnosisNotesDetails;
        }
        else
            return null;
    }
    //End of Get Diagnosis Notes Table

    //Get Empty Table
    function getEmptyTable() {
        var empty = [{ text: '                    ' }];
        return empty;
    }
    //End of Get Empty Table

    //Get Medicine List Table
    function getMedicineListTable(consultinfo) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientPrescription){
            cBold=contentSetting.patientPrescription.isBold=="Y";
            cFont=contentSetting.patientPrescription.fontSize;
        }
            if (consultinfo.prescription.medicines.length == 0)
                return;
            var beforeMealItems = [], afterMealItems = [];
            
                bodyItems = [
                             [{ text: 'S.No'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor: '#eaeaea', bold: true, margin: [0, 5, 0, 5] },
                             { text: 'Medicine'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor: '#eaeaea', bold: true, margin: [0, 5, 0, 5] },
                              { text: 'Dosage\nM - A - N'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor: '#eaeaea', bold: true, margin: [0, 5, 0, 5] },
                              { text: 'Days'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor: '#eaeaea', bold: true, margin: [0, 5, 0, 5] },
                              { text: 'Meal'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor: '#eaeaea', bold: true, margin: [0, 5, 0, 5] },
                              { text: 'Notes'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor: '#eaeaea', bold: true, margin: [0, 5, 0, 5] }]
                ];
                if (contentSetting.PrescriptionNotes !== undefined && contentSetting.PrescriptionNotes.isSelected == 'Y') {
                    bodyItems[0].splice(5, 1);
                }
                if (contentSetting.AfterBeforeMeal !== undefined && contentSetting.AfterBeforeMeal.isSelected == 'N') {
                    bodyItems[0].splice(4, 1);
                }
            angular.forEach(consultinfo.prescription.medicines, function (item,$index) {
                var medicineList = [];
                var numOfDys;
                var mealMode = ' - ';
                var MedicineGenericName = false;
                var GenericisBold = 'N';
                var GenericSize = 10;
                if (item.MedicineGenericName !== '' && contentSetting.GenericName !== undefined && contentSetting.GenericName.isSelected == 'Y') {
                    MedicineGenericName = true;
                    GenericSize = contentSetting.GenericName.fontSize;
                    GenericisBold = contentSetting.GenericName.isBold;

                }
                if (item.MedicineGenericName == '') {
                    MedicineGenericName = false;
                }

                if (isNaN(item.NumberofDays) || item.NumberofDays == null || item.NumberofDays == 0)
                    numOfDys = ' 0 ';
                else
                    numOfDys = item.NumberofDays;
                if (numOfDys !== ' - ')
                    if (item.MedicineType === 'GEL' || item.MedicineType === 'RES' || item.MedicineType == 'LOT' || item.MedicineType == 'INH' || item.MedicineType == 'INJ' || item.MedicineType == 'OIN' || item.MedicineType == 'KIT' || item.MedicineType == 'CRE' || item.MedicineType == 'SPR') {
                        mealMode = ''
                    }
                    else {
                        if (item.Dosage1 != '' &&  item.Dosage1 != 0|| item.Dosage2 != '' && item.Dosage2 != 0 || item.Dosage4 != '' && item.Dosage4 != 0) {
                            mealMode = ((item.BeforeAfter) ? "Before Meal" : "After Meal")
                        }
                        else {
                            mealMode = ''
                        }
                    }
                medicineList.push({ text: ($index+1)+".", color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
                
                if (contentSetting.PrescriptionNotes == undefined || contentSetting.PrescriptionNotes.isSelected == 'N') {
                    medicineList.push({
                        text: [
                            { text: getMedicineType(item) + '' + item.Medicine, fontSize: (contentSetting.MedicineName != undefined && contentSetting.MedicineName.fontSize != undefined) ? contentSetting.MedicineName.fontSize : cFont, bold: (contentSetting.MedicineName != undefined && contentSetting.MedicineName.isBold == 'Y') ? true : false },
                            { text: (MedicineGenericName == false) ? '' : "\nGeneric Name: " + item.MedicineGenericName, fontSize: GenericSize, color: 'black', bold: (GenericisBold == 'Y') ? true : false },
                             
                        ], color: 'black', fontSize: cFont, bold: cBold
                    });
                } else {
                    medicineList.push({
                        text: [
                            { text: getMedicineType(item) + '' + item.Medicine, fontSize: (contentSetting.MedicineName != undefined && contentSetting.MedicineName.fontSize != undefined) ? contentSetting.MedicineName.fontSize : cFont, bold: (contentSetting.MedicineName != undefined && contentSetting.MedicineName.isBold == 'Y') ? true : false, color: 'black' },
                            { text: (MedicineGenericName == false) ? '' : "\nGeneric Name: " + item.MedicineGenericName, fontSize: GenericSize, color: 'black', bold: (GenericisBold == 'Y') ? true : false },
                            { text: (item.Diagnostic == '' || item.Diagnostic == undefined) ? '' : "\nNotes : "+ item.Diagnostic, fontSize: contentSetting.PrescriptionNotes.fontSize, color: 'black', bold: (contentSetting.PrescriptionNotes.isBold == 'Y')? true:false }
                        ],
                    });
                }
                medicineList.push({ text: ConcatMeal(item).toUpperCase(), color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
                medicineList.push({ text: (numOfDys == ' 0 ') ? '' : numOfDys.toString().toUpperCase(), color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                
                if (contentSetting.AfterBeforeMeal == undefined || contentSetting.AfterBeforeMeal.isSelected == 'Y') {
                    medicineList.push({ text: mealMode, color: 'black', fontSize: cFont, bold: cBold, alignment: 'center' });
                }
                if (contentSetting.PrescriptionNotes == undefined || contentSetting.PrescriptionNotes.isSelected == 'N') {
                    medicineList.push({ text: item.Diagnostic, fontSize: cFont, bold: cBold, color: 'black', alignment: 'center' });
                }
                //Before Meal
                if (item.BeforeAfter)
                    beforeMealItems.push(medicineList);
                else
                    afterMealItems.push(medicineList);
            });
            var rowCount=1;
            for (var i = 0; i < beforeMealItems.length; i++){
                beforeMealItems[i][0].text=rowCount+".";rowCount++;
                bodyItems.push(beforeMealItems[i]);
            }
            for (var i = 0; i < afterMealItems.length; i++){
                afterMealItems[i][0].text=rowCount+".";rowCount++;
                bodyItems.push(afterMealItems[i]);
            }
            if (bodyItems.length == 1)
                return null;
            if (contentSetting.PrescriptionNotes !== undefined && contentSetting.PrescriptionNotes.isSelected == 'Y' && contentSetting.AfterBeforeMeal !== undefined && contentSetting.AfterBeforeMeal.isSelected == 'N') {
                if (printPageSize == "A5") {
                    printPageWidth = [25, '*', 100, 100];
                }
                else {
                    printPageWidth = [60, '*', 100, 80]
                }
            } else if (contentSetting.PrescriptionNotes !== undefined && contentSetting.PrescriptionNotes.isSelected == 'Y' || contentSetting.AfterBeforeMeal !== undefined && contentSetting.AfterBeforeMeal.isSelected == 'N') {
                if (printPageSize == "A5") {
                    printPageWidth = [25, '*', 60, 30, 50];
                }
                else {
                    printPageWidth = [40, '*', 80, 60, 80]
                }
            } else {
                if (printPageSize == "A5") {
                    printPageWidth = [25, 90, 60, 30, 50, '*'];
                }
                else {
                    printPageWidth = [40, 160, 80, 60, 80, '*']
                }
            }
            var borderSet="noBorders";
            if(contentSetting.patientPrescription!=undefined && contentSetting.patientPrescription.border=="Y"){
                borderSet={
                hLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                },
                vLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                },
                hLineColor: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                },
                vLineColor: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                },
                };
            }
            var patientVitalNotes = {
                style: 'tableExample',
                table: {
                    widths: printPageWidth,
                    body: bodyItems
                },
                    layout:borderSet
            };
            return patientVitalNotes;
        
    }
    //End of Get Medicine List Table

    //Get Medicine Type
    function getMedicineType(item) {
        //var med = (item.MedicineType == 'T' || item.MedicineType == 'TAB' ) ? 'TABLET' : item.MedicineType;
        var med = '';
        switch (item.MedicineType) {
            case "TAB": med = "Tab ";
                break;
            case "T": med = "Tab ";
                break;
            case "INJ": med = "Inj ";
                break;
            case "GEL": med = "Gel ";
                break;
            case "CAP": med = "Cap ";
                break;
            case "SUS": med = "Sus ";
                break;
            case "OIN": med = "Oin ";
                break;
            case "CRE": med = "Cre ";
                break;
            case "SOL": med = "Sol ";
                break;
            case "SYR": med = "Syr ";
                break;
            case "LIQ": med = "Liq ";
                break;
            case "DRO": med = "Dro ";
                break;
            case "VIA": med = "Via ";
                break;
            case "KIT": med = "Kit ";
                break;
            case "SPR": med = "Spr ";
                break;
            case "LOT": med = "Lot ";
                break;
            case "RES": med = "Res ";
                break;
            case "INH": med = "Inh ";
                break;
            case "SUP": med = "Sup ";
                break;
            case "INS": med = "Ins ";
                break;
        }
        return med==""?"":(med);
    }
    //End of Get Medicine Type

    //Concat Meal
    function ConcatMeal(item) {
        var meal = "";
        
        if (item.MedicineType == "SUS" ||item.MedicineType == "DRO"|| item.MedicineType == "LIQ" || item.MedicineType == "SYR" || item.MedicineType == "SOL") {
            if (item.Dosage1.toString() == "" && item.Dosage2.toString() == "" && item.Dosage4.toString() == "") {
                meal = '';
            }
            else {
                meal += item.Dosage1 == "" ? "0 - " : item.Dosage1 + " - ";
                meal += item.Dosage2 == "" ? "0 - " : item.Dosage2 + " - ";
                if (item.MedicineType == "DRO") {
                    meal += item.Dosage4 == "" ? "0 " : item.Dosage4 + " Drop (s)";
                } else {
                    meal += item.Dosage4 == "" ? "0 " : item.Dosage4 + " (ml)";
                }
            }
        }
        else {
            if ((item.Dosage1.toString() == "" && item.Dosage2.toString() == "" && item.Dosage4.toString() == "") || (item.Dosage1.toString() == 0 && item.Dosage2.toString() == 0 && item.Dosage4.toString() == 0)) {
                meal = '';
            }
            else {
                meal += item.Dosage1 == "" ? "0 - " : item.Dosage1 + " - ";
                meal += item.Dosage2 == "" ? "0 - " : item.Dosage2 + " - ";
                meal += item.Dosage4 == "" ? "0 " : item.Dosage4 + " ";
            }
        }
        return meal;
    }
    //End of Concat Meal

    //Get Lab Info Table
    function getLabInfoTable(consultinfo,labPrepration) {
        //console.log(consultinfo.labtests);
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientLabTest){
            cBold=contentSetting.patientLabTest.isBold=="Y";
            cFont=contentSetting.patientLabTest.fontSize;
        }
        if (consultinfo.labtests.length == 0)
            return;
        var bodyItem = [[{ text: 'Test Name'.toUpperCase(),bold:true, fontSize: cFont, fillColor:'#eaeaea' },
                          { text: 'Test Preparation Information'.toUpperCase(), bold: true, fontSize: cFont, fillColor: '#eaeaea' },
                          { text: 'Additional Test Instruction'.toUpperCase(), bold: true, fontSize: cFont, fillColor: '#eaeaea' }]];
        angular.forEach(consultinfo.labtests, function (item) {
            var labTestItem = [];
            var labDescription = '';
            labTestItem.push({ text: item.LabTestName.toUpperCase(), color: 'black', fontSize: cFont,bold:cBold });
            labTestItem.push({ text: item.LabNotes.toUpperCase(), color: 'black', fontSize: cFont,bold:cBold });
            //if (labPrepration.labPreprationList.length > 0) {
            //    angular.forEach(labPrepration.labPreprationList, function (desc) {
            //        if (desc.TestName == item.LabTestName)
            //            labDescription += desc.AdditionalNotes.toUpperCase() + '\n';
            //    });
            //}
            labTestItem.push({ text: item.AdditionalNotes.toUpperCase(), color: 'black', fontSize: cFont, bold: cBold });
            bodyItem.push(labTestItem);
        });
        if (bodyItem.length == 1)
            return null;
        var patientLabTestTable = {
            style: 'tableExample',
            table: {
                widths: '*',
                body: bodyItem
            },
                    layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
        };
        return patientLabTestTable;
    }
    //End of Get Lab Info Table

    //Get Dental Treatments
    function getDentalTreatments(consultinfo,scopeObj) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientTreatment){
            cBold=contentSetting.patientTreatment.isBold=="Y";
            cFont=contentSetting.patientTreatment.fontSize;
        }
        if (consultinfo.TreatmentConditionsObject.conditionforpatient==undefined || consultinfo.TreatmentConditionsObject.conditionforpatient.length == 0)
            return;
        if(scopeObj.printType=="PASTRECORD"){
            var TeethSelection = [];
            angular.forEach(consultinfo.TreatmentConditionsObject.conditionforpatient, function (item) {
               
                if (item.BodyParts && item.BodyParts.Desc) {
                    if (item.BodyParts.Desc == 'Teeth' || item.BodyParts.Desc == 'Gums Up' || item.BodyParts.Desc == 'Gums Down' || item.BodyParts.Desc == 'Gum Up' || item.BodyParts.Desc == 'Gum Down') {
                        TeethSelection.push(item);
                    }
                }
            });

            if (TeethSelection.length == 0) {
                return;
            }

            var array1 = [], bodyItems = [
              [{ text: 'Teeth / Gums'.toUpperCase(),fillColor:'#eaeaea', alignment: 'center', fontSize: cFont, bold: true },
               { text: 'Conditions'.toUpperCase(),fillColor:'#eaeaea', alignment: 'center', fontSize: cFont, bold: true },
               { text: 'Treatment'.toUpperCase(),fillColor:'#eaeaea', alignment: 'center', fontSize: cFont, bold: true },
               { text: 'Status'.toUpperCase(),fillColor:'#eaeaea', alignment: 'center', fontSize: cFont, bold: true }
              ]
            ];
            angular.forEach(TeethSelection, function (item) {
               
                var teethnumbers = "";
                for (var i = 0; i < item.BodyParts.ID.length; i++) {
                    teethnumbers += item.BodyParts.ID[i];
                }
                item.BodyParts.ID = teethnumbers;
                var Teetharray1 = [];
                var dentalcond = '';
                var TeethTreatments = [];
                var TeethTreatmentStatus = [];
                var TeethTreatmentscond = [];
                if (item.BodyParts.Desc == 'Teeth') {
                    Teetharray1.push({ text: 'Teeth ' + item.BodyParts.ID.toUpperCase(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center' });
                }
                if (item.BodyParts.Desc == 'Gums Up' || item.BodyParts.Desc == 'Gum Up') {
                    Teetharray1.push({ text: 'Gums Up ' + item.BodyParts.ID.toUpperCase(), color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
                }
                if (item.BodyParts.Desc == 'Gums Down' || item.BodyParts.Desc == 'Gum Down') {
                    Teetharray1.push({ text: 'Gums Down ' + item.BodyParts.ID.toUpperCase(), color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
                }
                
                angular.forEach(item.PatientCaseConditions, function (itm) {
                    dentalcond = itm.ConditionDesc.toUpperCase();
                    var condnotes = "";
                    if (itm.ConditionNotes == "") {
                        var condnotes = "";
                    } else {
                        condnotes = "Notes: " + itm.ConditionNotes;
                    }
                    angular.forEach(itm.ConditionTreatment, function (data) {
                        var notes = "";
                        if (data.TreatmentNotes == "") {
                            notes = "";
                        }
                        else {
                            notes = "Notes: " + data.TreatmentNotes;
                        }
                        TeethTreatments.push({ text: data.TreatmentIDDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont,bold:cBold });
                        TeethTreatments.push({ text: notes + '\n', color: 'black', fontSize: cFont,bold:cBold });
                        TeethTreatmentStatus.push({ text: data.TreatMentStatusDesc == undefined ? '' : data.TreatMentStatusDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont, bold: cBold });
                    });
                    TeethTreatmentscond.push({ text: dentalcond.toUpperCase() + '\n', color: 'black', fontSize: cFont,bold:cBold });
                    TeethTreatmentscond.push({ text: condnotes + '\n', color: 'black', fontSize: cFont,bold:cBold });
                });
                Teetharray1.push({ text: TeethTreatmentscond, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold });
                Teetharray1.push({ text: TeethTreatments, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold, });
                Teetharray1.push({ text: TeethTreatmentStatus, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold, });
                array1.push(Teetharray1);
            });
            for (var i = 0; i < array1.length; i++)
                bodyItems.push(array1[i]);
            if (bodyItems.length == 1)
                return null;
            if (printPageSize == "A5") {
                printPageWidth = [125, 80, '*', 50];
            }
            else {
                printPageWidth = [160, 160, '*', 60]
            }
            var patientVitalNotes = {
                style: 'tableExample',
                table: {
                    widths: printPageWidth,
                    body: bodyItems
                },
                layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
            };
        }
        else{
            var array1 = [], bodyItems = [
              [{ text: 'Teeth / Gums'.toUpperCase(), alignment: 'center', fontSize: cFont, fillColor: '#eaeaea', },
               { text: 'Conditions'.toUpperCase(), alignment: 'center', fontSize: cFont, fillColor: '#eaeaea', },
               { text: 'Treatment'.toUpperCase(), alignment: 'center', fontSize: cFont, fillColor: '#eaeaea', },
               { text: 'Status'.toUpperCase(), alignment: 'center', fontSize: cFont, fillColor: '#eaeaea', }
              ]
            ];
            angular.forEach(consultinfo.TreatmentConditionsObject.conditionforpatient, function (item) {
                console.log(item);
                var teethnumbers = "";
                //for (var i = 0; i < item.toothid.length; i++) {
                //    teethnumbers += item.toothid[i] + ",";
                //}
                var newid = false;
                angular.forEach(item.toothid, function (val) {
                    if (newid == false) {
                        teethnumbers += ' ' + val;
                        newid = true;
                    } else {
                        teethnumbers += ',' + val;
                    }
                });
                item.toothid = teethnumbers;
                var Teetharray1 = [];
                var dentalcond = '';
                var TeethTreatments = [];
                var TeethTreatmentStatus = [];
                var TeethTreatmentscond = [];
                if (item.flag == 'teeth') {
                    Teetharray1.push({ text: 'Teeth ' + item.toothid.toUpperCase(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center' });
                }
                if (item.flag == 'Up') {
                    Teetharray1.push({ text: 'Gums Up ' + item.toothid.toUpperCase(), color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
                }
                if (item.flag == 'down') {
                    Teetharray1.push({ text: 'Gums Down ' + item.toothid.toUpperCase(), color: 'black', fontSize: cFont,bold:cBold, alignment: 'center' });
                }
                //  Teetharray1.push({ text: 'Tooth' + item.ToothID.toUpperCase(), color: 'black', fontSize: 10, alignment: 'center' });
                angular.forEach(item.Treatment, function (itm) {
                    
                    dentalcond = itm.ProblemName.toUpperCase();
                    var condnotes = "";
                    if (itm.ConditionNotes == "") {
                        var condnotes = "";
                    } else {
                        condnotes = "Notes: " + itm.ConditionNotes;
                    }
                    angular.forEach(itm.Treatments, function (data) {
                        
                        var notes = "";
                        if (data.NotesTreatments == "") {
                            notes = "";
                        }
                        else {
                            notes = "Notes: " + data.NotesTreatments;
                        }
                        TeethTreatments.push({ text: data.TreatmentDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont,bold:cBold });
                        TeethTreatments.push({ text: notes + '\n', color: 'black', fontSize: cFont,bold:cBold });
                        TeethTreatmentStatus.push({ text: data.status.toUpperCase() + '\n', color: 'black', fontSize: cFont,bold:cBold });
                    });
                    TeethTreatmentscond.push({ text: dentalcond.toUpperCase() + '\n', color: 'black', fontSize: cFont,bold:cBold });
                    TeethTreatmentscond.push({ text: condnotes + '\n', color: 'black', fontSize: cFont,bold:cBold });
                });
                Teetharray1.push({ text: TeethTreatmentscond, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold });
                Teetharray1.push({ text: TeethTreatments, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold, });
                Teetharray1.push({ text: TeethTreatmentStatus, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold, });
                array1.push(Teetharray1);
            });
            for (var i = 0; i < array1.length; i++)
                bodyItems.push(array1[i]);
            if (bodyItems.length == 1)
                return null;
            if (printPageSize == "A5") {
                printPageWidth = [125, 80, '*', 50];
            }
            else {
                printPageWidth = [160, 160, '*', 60]
            }
            var patientVitalNotes = {
                style: 'tableExample',
                table: {
                    widths: printPageWidth,
                    body: bodyItems
                },
                layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
            };
        }
        return patientVitalNotes;
    }
    //End of Get Dental Treatments

    //Get TMJ Treatments
    function getTmjTreatments(consultinfo,scopeObj) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientTreatment){
            cBold=contentSetting.patientTreatment.isBold=="Y";
            cFont=contentSetting.patientTreatment.fontSize;
        }
        if (consultinfo.TreatmentConditionsObject.TreatmentTMJArray==undefined || consultinfo.TreatmentConditionsObject.TreatmentTMJArray.length == 0)
            return;
        if(scopeObj.printType=="PASTRECORD"){
            var TMJSelection = [];
            angular.forEach(consultinfo.TreatmentConditionsObject.TreatmentTMJArray, function (item) {
                angular.forEach(item.PatientCaseConditions, function (value) {
                    if (value.ConditionDesc) {
                        if (value.ConditionDesc == 'TMJ & Evaluations') {
                            TMJSelection.push(value);
                        }
                    }
                });
                
            });
            if (TMJSelection.length == 0) {
                return;
            }
            var array1 = [], bodyItems = [
             [
              { text: 'Conditions'.toUpperCase(),fillColor:'#eaeaea', alignment: 'center', fontSize: cFont, bold: true },
              { text: 'Treatment'.toUpperCase(),fillColor:'#eaeaea', alignment: 'center', fontSize: cFont, bold: true },
              { text: 'Status'.toUpperCase(),fillColor:'#eaeaea', alignment: 'center', fontSize: cFont, bold: true }
             ]
            ];

            angular.forEach(TMJSelection[0].SubCondition, function (item) {
                var Tmjarray1 = [];
                var TmjTreatments = [];
                var TmjTreatmentStatus = [];
                var TmjSubConditionDesc = [];
                TmjSubConditionDesc.push({ text: item.SubConditionDesc +'\n', color: 'black', fontSize: cFont, bold: cBold });
                var subnotes = "";
                if (item.SubConditionNotes == '' || item.SubConditionNotes == undefined) {
                    subnotes = "";
                } else {
                    subnotes = "Notes: " + item.SubConditionNotes;
                }
                TmjSubConditionDesc.push({ text: subnotes + '\n', color: 'black', fontSize: cFont, bold: cBold });
                angular.forEach(item.SubConditionTreatment, function (data) {
                    var notes = "";
                    if (data.TreatmentNotes == "") {
                        notes = "";
                    }
                    else {
                        notes = "Notes: " + data.TreatmentNotes;
                    }
                    TmjTreatments.push({ text: data.TreatmentIDDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                    TmjTreatments.push({ text: notes + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                    TmjTreatmentStatus.push({ text: data.TreatMentStatusDesc == undefined ? '' : data.TreatMentStatusDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont, bold: cBold });
                });
                Tmjarray1.push({ text: TmjSubConditionDesc, color: 'black', fontSize: cFont, bold: cBold });
                Tmjarray1.push({ text: TmjTreatments, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold  });
                Tmjarray1.push({ text: TmjTreatmentStatus, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold  });
                array1.push(Tmjarray1);
            });
            for (var i = 0; i < array1.length; i++)
                bodyItems.push(array1[i]);
            if (bodyItems.length == 1)
                return null;
            var patientVitalNotes = {
                style: 'tableExample',
                table: {
                    "widths": '*',
                    "body": bodyItems
                },
                layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
            };
            return patientVitalNotes;
        }
        else{
            var array1 = [], bodyItems = [
              [
               { text: 'Conditions'.toUpperCase(), alignment: 'center', fontSize: cFont,bold:cBold , fillColor: '#eaeaea', },
               { text: 'Treatment'.toUpperCase(), alignment: 'center', fontSize: cFont,bold:cBold , fillColor: '#eaeaea', },
               { text: 'Status'.toUpperCase(), alignment: 'center', fontSize: cFont,bold:cBold , fillColor: '#eaeaea', }
              ]
            ];
            angular.forEach(consultinfo.TreatmentConditionsObject.TreatmentTMJArray, function (item) {
                var Tmjarray1 = [];
                var TmjTreatments = [];
                var TmjTreatmentStatus = [];
                var TmjSubConditionDesc = [];
                TmjSubConditionDesc.push({ text: item.SubConditionDesc + '\n', color: 'black', fontSize: cFont, bold: cBold });
                var subnotes = "";
                if (item.SubConditionNotes == '' || item.SubConditionNotes == undefined) {
                    subnotes = "";
                } else {
                    subnotes = "Notes: " + item.SubConditionNotes;
                }
                TmjSubConditionDesc.push({ text: subnotes + '\n', color: 'black', fontSize: cFont, bold: cBold });
                angular.forEach(item.SubConditionTreatment, function (data) {
                    var notes = "";
                    if (data.TreatmentNotes == "") {
                        notes = "";
                    }
                    else {
                        notes = "Notes: " + data.TreatmentNotes;
                    }
                    TmjTreatments.push({ text: data.TreatmentIDDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                    TmjTreatments.push({ text: notes + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                    TmjTreatmentStatus.push({ text: data.TreatMentStatusDesc == undefined ? '' : data.TreatMentStatusDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont, bold: cBold });
                });
                Tmjarray1.push({ text: TmjSubConditionDesc, color: 'black', fontSize: cFont, bold: cBold });
                Tmjarray1.push({ text: TmjTreatments, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
                Tmjarray1.push({ text: TmjTreatmentStatus, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
                array1.push(Tmjarray1);
            });
            for (var i = 0; i < array1.length; i++)
                bodyItems.push(array1[i]);
            if (bodyItems.length == 1)
                return null;
            var patientVitalNotes = {
                style: 'tableExample',
                table: {
                    "widths": '*',
                    "body": bodyItems
                },
                layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
            };
            return patientVitalNotes;
        }
    }
    //End of Get TMJ Treatments

    //Get Mucosa Treatments
    function getMucosaTreatments(consultinfo,scopeObj) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientTreatment){
            cBold=contentSetting.patientTreatment.isBold=="Y";
            cFont=contentSetting.patientTreatment.fontSize;
        }
        if (consultinfo.TreatmentConditionsObject.TreatmentMocusaArray==undefined || consultinfo.TreatmentConditionsObject.TreatmentMocusaArray.length == 0)
                return;
        if(scopeObj.printType=="PASTRECORD"){
            var MucosaSelection = [];
            angular.forEach(consultinfo.TreatmentConditionsObject.TreatmentMocusaArray, function (item) {
                angular.forEach(item.PatientCaseConditions, function (value) {
                    if (value.ConditionDesc) {
                        if (value.ConditionDesc == 'Mucosa') {
                            MucosaSelection.push(value);
                        }
                    }
                });

            });
            if (MucosaSelection.length == 0) {
                return;
            }
            var array1 = [], bodyItems = [
              [
               { text: 'Conditions'.toUpperCase(),fillColor:'#eaeaea', alignment: 'center', fontSize: cFont, bold: true },
               { text: 'Treatment'.toUpperCase(),fillColor:'#eaeaea', alignment: 'center', fontSize: cFont, bold: true },
                 { text: 'Status'.toUpperCase(),fillColor:'#eaeaea', alignment: 'center', fontSize: cFont, bold: true }
              ]
            ];
            angular.forEach(MucosaSelection[0].SubCondition, function (item) {
                var Mucosaarray1 = [];
                var MucosaTreatments = [];
                var MucosaTreatmentStatus = []; var MucosaSubConditionDesc = [];
                MucosaSubConditionDesc.push({ text: item.SubConditionDesc + '\n', color: 'black', fontSize: cFont, bold: cBold });
                var subnotes = "";
                if (item.SubConditionNotes == '' || item.SubConditionNotes == undefined) {
                    subnotes = "";
                } else {
                    subnotes = "Notes: " + item.SubConditionNotes;
                }
                MucosaSubConditionDesc.push({ text: subnotes + '\n', color: 'black', fontSize: cFont, bold: cBold });
                angular.forEach(item.SubConditionTreatment, function (data) {
                    var notes = "";
                    if (data.TreatmentNotes == "") {
                        notes = "";
                    }
                    else {
                        notes = "Notes: " + data.TreatmentNotes;
                    }
                    //  TeethTreatments += data.TreatmentDesc.toUpperCase() + '\n' +'Notes:'+data.NotesTreatments;
                    MucosaTreatments.push({ text: data.TreatmentIDDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                    MucosaTreatments.push({ text: notes + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                    MucosaTreatmentStatus.push({ text: data.TreatMentStatusDesc == undefined ? '' :  data.TreatMentStatusDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont, bold: cBold });
                });
                Mucosaarray1.push({ text: MucosaSubConditionDesc, color: 'black', fontSize: cFont, bold: cBold });
                Mucosaarray1.push({ text: MucosaTreatments, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
                Mucosaarray1.push({ text: MucosaTreatmentStatus, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
                array1.push(Mucosaarray1);
            });
            for (var i = 0; i < array1.length; i++)
                bodyItems.push(array1[i]);
            if (bodyItems.length == 1)
                return null;
            var patientVitalNotes = {
                "style": 'tableExample',
                "table": {
                    "widths": '*',
                    "body": bodyItems
                },
                layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
            };
            return patientVitalNotes;
        }
        else{
            var array1 = [], bodyItems = [
              [
               { text: 'Conditions'.toUpperCase(), alignment: 'center', fontSize: cFont, fillColor: '#eaeaea', },
               { text: 'Treatment'.toUpperCase(), alignment: 'center', fontSize: cFont, fillColor: '#eaeaea' ,},
                 { text: 'Status'.toUpperCase(), alignment: 'center', fontSize: cFont, fillColor: '#eaeaea' ,}
              ]
            ];
            angular.forEach(consultinfo.TreatmentConditionsObject.TreatmentMocusaArray, function (item) {
                var Mucosaarray1 = [];
                var MucosaTreatments = [];
                var MucosaTreatmentStatus = [];
                var MucosaSubConditionDesc = [];
                MucosaSubConditionDesc.push({ text: item.SubConditionDesc + '\n', color: 'black', fontSize: cFont, bold: cBold });
                var subnotes = "";
                if (item.SubConditionNotes == '' || item.SubConditionNotes == undefined) {
                    subnotes = "";
                 }else{
                    subnotes = "Notes: " + item.SubConditionNotes;
                }
                MucosaSubConditionDesc.push({ text: subnotes + '\n', color: 'black', fontSize: cFont, bold: cBold });
                angular.forEach(item.SubConditionTreatment, function (data) {
                    var notes = "";
                    if (data.TreatmentNotes == "") {
                        notes = "";
                    }
                    else {
                        notes = "Notes: " + data.TreatmentNotes;
                    }
                    //  TeethTreatments += data.TreatmentDesc.toUpperCase() + '\n' +'Notes:'+data.NotesTreatments;
                    MucosaTreatments.push({ text: data.TreatmentIDDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                    MucosaTreatments.push({ text: notes + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                    MucosaTreatmentStatus.push({ text: data.TreatMentStatusDesc == undefined ? '' : data.TreatMentStatusDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont, bold: cBold });
                });
                Mucosaarray1.push({ text: MucosaSubConditionDesc, color: 'black', fontSize: cFont, bold: cBold });
                Mucosaarray1.push({ text: MucosaTreatments, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
                Mucosaarray1.push({ text: MucosaTreatmentStatus, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
                array1.push(Mucosaarray1);
            });
            for (var i = 0; i < array1.length; i++)
                bodyItems.push(array1[i]);
            if (bodyItems.length == 1)
                return null;
            var patientVitalNotes = {
                "style": 'tableExample',
                "table": {
                    "widths": '*',
                    "body": bodyItems
                },
                layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
            };
            return patientVitalNotes;
        }
        

    }
    //End of Get Mucosa Treatments

    //Get Dental Progress Treatments
    function getDentalProgressTreatments(consultinfo) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientTreatment){
            cBold=contentSetting.patientTreatment.isBold=="Y";
            cFont=contentSetting.patientTreatment.fontSize;
        }
        if (consultinfo.progressArray==undefined || consultinfo.progressArray.length == 0)
            return;
        var array1 = [], bodyItems = [
          [{ text: 'Teeth / Gums'.toUpperCase(),bold:true, alignment: 'center', fontSize: cFont, fillColor:'#eaeaea' },
           { text: 'Conditions'.toUpperCase(),bold:true, alignment: 'center', fontSize: cFont, fillColor:'#eaeaea' },
           { text: 'Treatment'.toUpperCase(),bold:true, alignment: 'center', fontSize: cFont, fillColor:'#eaeaea' },
            { text: 'Status'.toUpperCase(),bold:true, alignment: 'center', fontSize: cFont, fillColor:'#eaeaea' }
          ]
        ];
        angular.forEach(consultinfo.progressArray, function (item) {
            var Teetharray1 = [];
            var dentalcond = '';
            var TeethTreatments = [];
            var TeethTreatmentStatus = [];
            if (item.BodyParts == undefined)
                return;
            if (item.BodyParts.Desc == 'Teeth') {
                Teetharray1.push({ text: 'Teeth ' + item.BodyParts.ID.toUpperCase(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center' });
            }
            if (item.BodyParts.Desc == 'Gums Up') {
                Teetharray1.push({ text: 'Gums Up ' + item.BodyParts.ID.toUpperCase(), color: 'black', fontSize: cFont,bold:cBold , alignment: 'center' });
            }
            if (item.BodyParts.Desc == 'Gums Down') {
                Teetharray1.push({ text: 'Gums Down ' + item.BodyParts.ID.toUpperCase(), color: 'black', fontSize: cFont,bold:cBold , alignment: 'center' });
            }
            //  Teetharray1.push({ text: 'Tooth' + item.ToothID.toUpperCase(), color: 'black', fontSize: 10, alignment: 'center' });
            angular.forEach(item.PatientCaseConditions, function (itm) {
                dentalcond += itm.ConditionDesc.toUpperCase() + '\n' + '\n';
                angular.forEach(itm.ConditionTreatment, function (data) {
                    var notes = "";
                    if (data.TreatmentNotes == "") {
                        notes = "";
                    }
                    else {
                        notes = "Notes: " + data.TreatmentNotes;
                    }
                    TeethTreatments.push({ text: data.TreatmentIDDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                    TeethTreatments.push({ text: notes + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                    TeethTreatmentStatus.push({ text: data.TreatMentStatusDesc == undefined ? '' : data.TreatMentStatusDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont, bold: cBold });
                });
            });
            Teetharray1.push({ text: dentalcond, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold  });
            Teetharray1.push({ text: TeethTreatments, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
            Teetharray1.push({ text: TeethTreatmentStatus, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
            array1.push(Teetharray1);
        });
        for (var i = 0; i < array1.length; i++)
            bodyItems.push(array1[i]);
        if (bodyItems.length == 1)
            return null;
        if (printPageSize == "A5") {
            printPageWidth = [125, 80, '*', 50];
        }
        else {
            printPageWidth = [160, 160, '*', 60]
        }
        var patientVitalNotes = {
            style: 'tableExample',
            table: {
                widths: printPageWidth,
                body: bodyItems
            },
            layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
        };
        return patientVitalNotes;
    }
    //End of Get Dental Progress treatments

    //Get Tmj Progress treatments
    function getTmjProgressTreatments(consultinfo) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientTreatment){
            cBold=contentSetting.patientTreatment.isBold=="Y";
            cFont=contentSetting.patientTreatment.fontSize;
        }
        if(consultinfo.progressArray != undefined) {
            if (consultinfo.progressArray.length == 0)
                return;
            var array1 = [], bodyItems = [
              [
               { text: 'Conditions'.toUpperCase(),bold:true, alignment: 'center', fontSize: cFont, fillColor:'#eaeaea' },
               { text: 'Treatment'.toUpperCase(),bold:true, alignment: 'center', fontSize: cFont, fillColor:'#eaeaea' },
                  { text: 'Status'.toUpperCase(),bold:true, alignment: 'center', fontSize: cFont, fillColor:'#eaeaea' }
              ]
            ];
            angular.forEach(consultinfo.progressArray, function (item) {
                var Tmjarray1 = [];
                var TmjTreatments = [];
                var TmjTreatmentStatus = [];
                var tmjcond = "";
                //   console.log(item.PatientCaseConditions[0].ConditionDesc);
                if (item.PatientCaseConditions[0].ConditionDesc == "TMJ & Evaluations") {
                    angular.forEach(item.PatientCaseConditions[0].SubCondition, function (data) {
                        //  console.log(data.SubConditionDesc);
                        tmjcond += data.SubConditionDesc.toUpperCase() + '\n';
                        angular.forEach(data.SubConditionTreatment, function (itm) {
                            var notes = "";
                            if (itm.TreatmentNotes == "") {
                                notes = "";
                            }
                            else {
                                notes = "Notes: " + itm.TreatmentNotes;
                            }
                            TmjTreatments.push({ text: itm.TreatmentIDDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                            TmjTreatments.push({ text: notes + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                            TmjTreatmentStatus.push({ text: itm.TreatMentStatusDesc == undefined ? '' : itm.TreatMentStatusDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont, bold: cBold });
                        });
                    });
                    Tmjarray1.push({ text: tmjcond, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
                    Tmjarray1.push({ text: TmjTreatments, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
                    Tmjarray1.push({ text: TmjTreatmentStatus, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
                    array1.push(Tmjarray1);
                }
            });
            for (var i = 0; i < array1.length; i++)
                bodyItems.push(array1[i]);
            if (bodyItems.length == 1)
                return null;
            var patientVitalNotes = {
                style: 'tableExample',
                table: {
                    widths: '*',
                    body: bodyItems
                },
                layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
            };
            return patientVitalNotes;
        }
    }
    //End of get TMJ progress treatments

    //Get Mucosa Progress Treatments
    function getMucosaProgressTreatments(consultinfo) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientTreatment){
            cBold=contentSetting.patientTreatment.isBold=="Y";
            cFont=contentSetting.patientTreatment.fontSize;
        }
        if (consultinfo.progressArray != undefined) {
            if (consultinfo.progressArray.length == 0)
                return;
            var array1 = [], bodyItems = [
              [
               { text: 'Conditions'.toUpperCase(),bold:true, alignment: 'center', fontSize: cFont, fillColor:'#eaeaea' },
               { text: 'Treatment'.toUpperCase(),bold:true, alignment: 'center', fontSize: cFont, fillColor:'#eaeaea' },
                 { text: 'Status'.toUpperCase(),bold:true, alignment: 'center', fontSize: cFont, fillColor:'#eaeaea' }
              ]
            ];
            angular.forEach(consultinfo.progressArray, function (item) {
                var MucosaArray1 = [];
                var MucosaTreatments = [];
                var MucosaTreatmentStatus = [];
                var MucosaCond = "";
                //     console.log(item.PatientCaseConditions[0].ConditionDesc);
                if (item.PatientCaseConditions[0].ConditionDesc == "Mucosa") {
                    angular.forEach(item.PatientCaseConditions[0].SubCondition, function (data) {
                        //   console.log(data.SubConditionDesc);
                        MucosaCond += data.SubConditionDesc.toUpperCase() + '\n';
                        angular.forEach(data.SubConditionTreatment, function (itm) {
                            var notes = "";
                            if (itm.TreatmentNotes == "") {
                                notes = "";
                            }
                            else {
                                notes = "Notes: " + itm.TreatmentNotes;
                            }
                            MucosaTreatments.push({ text: itm.TreatmentIDDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                            MucosaTreatments.push({ text: notes + '\n', color: 'black', fontSize: cFont,bold:cBold  });
                            MucosaTreatmentStatus.push({ text: itm.TreatMentStatusDesc == undefined ? '' : itm.TreatMentStatusDesc.toUpperCase() + '\n', color: 'black', fontSize: cFont, bold: cBold });
                        });
                    });
                    MucosaArray1.push({ text: MucosaCond, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
                    MucosaArray1.push({ text: MucosaTreatments, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
                    MucosaArray1.push({ text: MucosaTreatmentStatus, color: 'black', alignment: 'center', fontSize: cFont,bold:cBold , });
                    array1.push(MucosaArray1);
                }
            });
            for (var i = 0; i < array1.length; i++)
                bodyItems.push(array1[i]);
            if (bodyItems.length == 1)
                return null;
            var patientVitalNotes = {
                style: 'tableExample',
                table: {
                    widths: '*',
                    body: bodyItems
                },
                layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
            };
            return patientVitalNotes;
        }
    }
    //End of Get Mucosa Progress Treatments

    //Get Referral Info
    function getReferalInfo(consultinfo) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientReferral){
            cBold=contentSetting.patientReferral.isBold=="Y";
            cFont=contentSetting.patientReferral.fontSize;
        }
     if(consultinfo.referrals !== undefined && consultinfo.referrals.length > 0) {
        var referralContent = "";
        for (var i = 0; i < consultinfo.referrals.length; i++) {
            var specialityDesc = GetSpecialityById(consultinfo.referrals[i].DoctorSpeciality[Object.keys(consultinfo.referrals[i].DoctorSpeciality)[0]]) || consultinfo.referrals[i].DoctorSpeciality
            referralContent += "Dr. " + consultinfo.referrals[i].DoctorName.toUpperCase() + "\n " + specialityDesc.toUpperCase() + "\n"
        }
         
        if (!referralContent) {
            return null;
        } 
        var FollowUpNotesDetails = [{
            text: 'You have been referred to: \n\n'.toUpperCase(), bold: true, fontSize: cFont,
            
        }, { text: referralContent, color: 'black', fontSize: cFont,bold:cBold, }, ];
        return FollowUpNotesDetails;
            
     }else{
         return null;
     }
    }
    //End of Get Referral Info

    //get Lab Test Rows
    function getLabTestRows(LabInvoiceArray,id){
        var cBold=false;
        var cFont=defaultFontSize;
        var array1 = [], bodyItems = [
                 [
                 {
                     text: 'Lab Name'.toUpperCase(),
                     fontSize: cFont,bold:true,
                     alignment: 'center', fillColor:'#eaeaea',margin:[0,5,0,5]
                 },
                 {
                     text: 'Invoice No.'.toUpperCase(),
                     fontSize: cFont,bold:true,
                     alignment: 'center', fillColor:'#eaeaea',margin:[0,5,0,5]
                 },
                 {
                     text: 'Date'.toUpperCase(),bold:true,
                     fontSize: cFont, alignment: 'center', fillColor:'#eaeaea',margin:[0,5,0,5]
                 },
                 {
                     text: 'Lab Test/Activity'.toUpperCase(),
                     fontSize: cFont,bold:true,
                     alignment: 'center', fillColor:'#eaeaea',margin:[0,5,0,5]
                 },
                 {
                     text: 'Cost'.toUpperCase(),
                     fontSize: cFont,bold:true,
                     alignment: 'center', fillColor:'#eaeaea',margin:[0,5,0,5]
                 }
                 ]
            ];

            var totalcost = LabInvoiceArray[id].cost;
            var labname = LabInvoiceArray[id].LabNameDesc;
            var invoice = "";
            for (var i in LabInvoiceArray[id].invoiceNo) {
                invoice = i;
            }


            var invoicedate = LabInvoiceArray[id].invoiceDate;
            var labactivity = LabInvoiceArray[id].labTestDesc;
            var labcost = LabInvoiceArray[id].cost;

            var LabdataArray1 = [];
            LabdataArray1.push({
                "text": labname.toUpperCase(),
                "color": 'black',
                "fontSize": cFont,bold:cBold ,
                "alignment": 'center',margin:[0,3,0,3]
            });
            LabdataArray1.push({
                "text": invoice.toUpperCase(),
                "color": 'black',
                "fontSize": cFont,bold:cBold ,
                "alignment": 'center',margin:[0,3,0,3]
            });
            LabdataArray1.push({
                "text": invoicedate,
                "color": 'black',
                "fontSize": cFont,bold:cBold ,
                "alignment": 'center',margin:[0,3,0,3]
            });
            LabdataArray1.push({
                "text": labactivity.toUpperCase(),
                "color": 'black',
                "fontSize": cFont,bold:cBold ,
                "alignment": 'center',margin:[0,3,0,3]
            });
            LabdataArray1.push({
                "text": labcost.toUpperCase(),
                "color": 'black',
                "fontSize": cFont,bold:cBold ,
                "alignment": 'center',margin:[0,3,0,3]
            });
            array1.push(LabdataArray1);

            var Total = totalcost.toString();
            var finalLabArray = [];
            finalLabArray.push({ text: '',fillColor:'#f8f8f8', margin:[0,5,0,5] });
            finalLabArray.push({ text: '',fillColor:'#f8f8f8', margin:[0,5,0,5] });
            finalLabArray.push({ text: '',fillColor:'#f8f8f8', margin:[0,5,0,5] });
            finalLabArray.push({ text: 'Total Cost : '.toUpperCase(), alignment: 'center', fontSize: cFont,bold:cBold ,fillColor:'#f8f8f8', margin:[0,5,0,5] });
            finalLabArray.push({ text: labcost.toUpperCase(), alignment: 'center',fontSize:cFont,bold:cBold ,fillColor:'#f8f8f8', margin:[0,5,0,5] });
            array1.push(finalLabArray);
            for (var i = 0; i < array1.length; i++)
                bodyItems.push(array1[i]);
            if (bodyItems.length == 1)
                return null;
            var patientVitalNotes = {
                "style": 'tableExample',
                "table": {
                    "widths": [120, 80, 70, 100, '*'],
                    "body": bodyItems
                },
            layout: "noBorders"
            };
            return patientVitalNotes;
    }
    //End of get Lab Test Rows

    //get Treatment Estimation Rows
    function getTreatmentRows(obj,val){
        var cBold=false;
        var cFont=defaultFontSize;
        var TotalBalance = val.toString();
            var array1 = [], bodyItems = [
                  [
                  { text: 'Description'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center', fillColor:'#eaeaea', margin:[0,5,0,5] },
                  { text: 'Treatment name'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] },
                  { text: 'Cost of treatment'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] },
                  { text: 'Qty'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] },
                  { text: 'Discount'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] },
                  { text: 'Total cost of treatment'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] }
                  ]
            ];
            angular.forEach(obj, function (item) {
                var Estimationarray1 = [];
                var Estimationarray2 = [];
                var Estimationarray3 = [];
                var Estimationarray4 = [];
                var Estimationarray5 = [];
                var Estimationarray6 = [];
                Estimationarray1.push({ text: "" + item.Desc + "".toUpperCase(), color: 'black', fontSize: cFont,bold:cBold , alignment: 'center', margin:[0,3,0,3] });
                angular.forEach(item.estimationTreatment, function (itm) {
                    var DiscountTypeVal = "%";
                    if (itm.discountDetails.discountType == "NUMBER") {
                        DiscountTypeVal = currencyType;
                    }
                    Estimationarray2.push({ text: "" + itm.treatmentDesc + "\n" + '\n'.toUpperCase(), color: 'black', fontSize: cFont, bold: cBold });
                    Estimationarray3.push({ text: "" + itm.discountDetails.ActualCost + "\n" + '\n', color: 'black', fontSize: cFont, bold: cBold });
                    Estimationarray4.push({ text: "" + itm.multipleFactor + "\n" + '\n', color: 'black', fontSize: cFont, bold: cBold });
                    Estimationarray5.push({ text: "" + itm.discountDetails.discountCost + " " + DiscountTypeVal + "\n" + '\n', color: 'black', fontSize: cFont, bold: cBold });
                    Estimationarray6.push({ text: "" + itm.cost + "\n" + '\n', color: 'black', fontSize: cFont, bold: cBold });
                });
                Estimationarray1.push({ text: Estimationarray2, color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 3, 0, 3] });
                Estimationarray1.push({ text: Estimationarray3, color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 3, 0, 3] });
                Estimationarray1.push({ text: Estimationarray4, color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 3, 0, 3] });
                Estimationarray1.push({ text: Estimationarray5, color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 3, 0, 3] });
                Estimationarray1.push({ text: Estimationarray6, color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 3, 0, 3] });
                array1.push(Estimationarray1);
            });
            var finalEstArray = [];
            finalEstArray.push({ text: '', fillColor: '#f8f8f8', margin: [0, 5, 0, 5] });
            finalEstArray.push({ text: '', fillColor: '#f8f8f8', margin: [0, 5, 0, 5] });
            finalEstArray.push({ text: '', fillColor: '#f8f8f8', margin: [0, 5, 0, 5] });
            finalEstArray.push({ text: '',fillColor:'#f8f8f8', margin:[0, 5, 0, 5] });
            finalEstArray.push({ text: 'Total : '.toUpperCase(), alignment: 'center', fontSize: cFont, bold: cBold, fillColor: '#f8f8f8', margin: [0, 5, 0, 5] });
            finalEstArray.push({ text: TotalBalance.toUpperCase(), alignment: 'center', fontSize: cFont, bold: cBold, fillColor: '#f8f8f8', margin: [0, 5, 0, 5] });
            array1.push(finalEstArray);
            for (var i = 0; i < array1.length; i++)
                bodyItems.push(array1[i]);
            if (bodyItems.length == 1)
                return null;
            var patientVitalNotes = {
                style: 'tableExample',
                table: {
                    widths: [120, 120, 80, 50,80,100,'*'],
                    body: bodyItems
                },
                layout: "noBorders"
            };
            return patientVitalNotes;
    }
    //End of get Treatment Estimation Rows

    //get Billing Rows
    function getBillRows(prevbill, scopeObj) {

        var currencyType = "INR";
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes !== undefined) {
            if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.currencyCode !== undefined) {
                currencyType = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.currencyCode;
            }
        }

        var cBold=false;
        var cFont=defaultFontSize;
        if (prevbill[0].ProcedureName == '')
            return;
        var consultinfo1 = hcueConsultationInfo.getConsultationInfo();
        var appointmentinfo = hcueDoctorLoginService.getAppointmentInfo();
        var tmpbillingarray = [];
        var hospCode="";
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined) {
            hospCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        }
        angular.forEach(prevbill, function (item) {
            //alert("2");
            if (item.otherinfo == undefined) {
                item.otherinfo = {};
                item.otherinfo.Status = 'Init';
                item.otherinfo.membershipamt = 0;
            }
            if (item.Status == undefined) {
                item.Status = false;
            }
            if (item.otherinfo.Status == undefined) {
                item.otherinfo.Status = 'Init'
            }
            if (item.ProcedureName !== "" && item.Status==false) {
                 if(item.InvoiceDate!=undefined){
                     item.InvoiceDate = item.InvoiceDate
                 }
                 else{
                    var dt=item.BillDate.split('-');
                    var date=dt[2]+"-"+dt[1]+"-"+dt[0];
                    item.InvoiceDate=date
                 }
                 
                 if (item.otherinfo.Status != 'Cancel') {
                     tmpbillingarray.push(item);
                 }
                //item.Previouspaid = item.Previouspaid||item.AdvancePaid,
                
            }
            else {
                console.log("ERROR");
            }
        });
        billinginfo=tmpbillingarray;
        

            var billingarray1 = [], bodyItems = [];
            var billingList= [];
            billingList.push({ text: 'S.No'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center',fillColor:'#eaeaea', margin:[0,5,0,5] });
            billingList.push({ text: 'Date'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center',fillColor:'#eaeaea', margin:[0,5,0,5] });
            billingList.push({ text: 'Treatment'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center',fillColor:'#eaeaea', margin:[0,5,0,5] });
            billingList.push({ text: 'Cost'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center',fillColor:'#eaeaea', margin:[0,5,0,5] });
            billingList.push({ text: 'Qty'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center',fillColor:'#eaeaea', margin:[0,5,0,5] });
            //billingList.push({ text: 'Total Cost'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center',fillColor:'#eaeaea', margin:[0,5,0,5] });
                if (scopeObj.discountSetting == 'N') {
                    billingList.push({ text: 'Discount'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center',fillColor:'#eaeaea', margin:[0,5,0,5] });
                }
                if (contentSetting.BillingTax == undefined || contentSetting.BillingTax.isSelected == "Y") {
                    if (taxType == 'VAT') {
                        billingList.push({ text: 'Vat%'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
                    } else {
                        billingList.push({ text: 'Cgst%'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
                        billingList.push({ text: 'Sgst%'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
                    }
                }
			billingList.push({ text: 'Total Cost'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center',fillColor:'#eaeaea', margin:[0,5,0,5] });
            billingList.push({ text: 'Received'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center',fillColor:'#eaeaea', margin:[0,5,0,5] });
                billingList.push({ text: 'Balance'.toUpperCase(),bold:true, fontSize: cFont, alignment: 'center',fillColor:'#eaeaea', margin:[0,5,0,5] });
             billingarray1.push(billingList);
            angular.forEach(billinginfo, function (item,$index) {
                var medicineList = [];
                var consultdt= $filter('date')(new Date(item.InvoiceDate), "dd-MMM-yyyy");
                var amountpaid = "";
                var stack=[{ text: item.ProcedureName.toUpperCase(), color: 'black', fontSize: cFont,bold:cBold   , alignment: 'center', margin:[0,2,0,2] }];
                if(contentSetting.billNotes!=undefined && contentSetting.billNotes.isSelected=="Y"){
                    var notes=item.otherinfo!=undefined?item.otherinfo.notes:(item.notes!=undefined?item.notes:"");
                    if(notes!=""&& notes!=undefined){
                        stack.push({ text: ("Notes: "+notes), color: 'black', fontSize: contentSetting.billNotes.fontSize, bold: (contentSetting.billNotes.isBold=="Y"?true:false), alignment: 'center', margin: [0, 2, 0, 2] });
                    }
                }
                if (item.BilledCost == 0) {
                    amountpaid = Math.round( item.Previouspaid + parseFloat(item.otherinfo.membershipamt)) ;
                    //$scope.AmountInfo.previousTotal = $scope.AmountInfo.previousTotal + parseFloat(item.BilledCost);
                }
                else {
                    amountpaid = Math.round(item.Previouspaid + parseFloat(item.BilledCost) + parseFloat(item.otherinfo.membershipamt));
                    //$scope.AmountInfo.previousTotal = $scope.AmountInfo.previousTotal + parseFloat(item.BilledCost);
                }
                if (hospCode == 'GOCLINIC') {
                    amountpaid = Math.round(parseInt(item.ClinicAmount) + amountpaid);
                    amountpaid = Math.round( parseFloat(item.PreClinicAmount) + amountpaid);
                }
                var DiscountType = "%";
                if(item.DiscountType == "NUMBER")
                {
                    DiscountType = currencyType;
                }
				  
				        if(item.otherinfo != undefined){
							 if(item.otherinfo.TaxDetails != undefined){
								 var Cgst = item.otherinfo.TaxDetails.Cgst;
								 var Sgst =  item.otherinfo.TaxDetails.Sgst;
							 }
						 } else {
							  var Cgst = item.Cgst;
							  var Sgst = item.Sgst;
						 }
                var finalamount = item.ProcedureCost * item.Multiplyfactor;
				  
			    if(item.DiscountType == "%"){
				    discountAmount = parseFloat(finalamount) * parseFloat(item.DiscountCost/ 100);
				    cgstAmount =  (finalamount - discountAmount) *(Cgst/100);
			        sgstAmount = (finalamount - discountAmount) *(Sgst/100); 
				     var totlcstaftrtax = Math.round((finalamount - discountAmount) + cgstAmount + sgstAmount);
				}else if( item.DiscountType == "NUMBER"){
			      discountAmount = parseFloat(item.DiscountCost);
				  cgstAmount =  (finalamount - discountAmount) *(Cgst/100);
			      sgstAmount = (finalamount - discountAmount) *(Sgst/100); 
				   var totlcstaftrtax =  Math.round((finalamount - discountAmount) + cgstAmount + sgstAmount);			 
				}else{
				   discountAmount= 0; 
				   cgstAmount = (finalamount - discountAmount) *(Cgst/100);
			       sgstAmount = (finalamount - discountAmount) *(Sgst/100); 
				    var totlcstaftrtax =  Math.round((finalamount - discountAmount) + cgstAmount + sgstAmount);
				 }
                var a = item.ProcedureCost;
                var AmountRecieved = "";
				var Balance = "";
                AmountRecieved = amountpaid;
				Balance = Math.round(item.Balance);
                medicineList.push({ text: ($index+1)+".", color: 'black', fontSize: cFont,bold:cBold   , alignment: 'center', margin:[0,2,0,2] });
                medicineList.push({ text: consultdt.toUpperCase(), color: 'black', fontSize: cFont,bold:cBold   , alignment: 'center', margin:[0,2,0,2] });
                medicineList.push(stack);
                medicineList.push({ text: item.ProcedureCost.toString(), color: 'black', fontSize: cFont,bold:cBold   , alignment: 'center', margin:[0,2,0,2] });
                medicineList.push({ text: item.Multiplyfactor.toString(), color: 'black', fontSize: cFont,bold:cBold   , alignment: 'center', margin:[0,2,0,2] });
                //medicineList.push({ text: finalamount.toString(), color: 'black', fontSize: cFont,bold:cBold   , alignment: 'center', margin:[0,2,0,2] });
                if (scopeObj.discountSetting == 'N') {
                    medicineList.push({ text: item.DiscountCost.toString() + " " + DiscountType, fontSize: cFont, bold: cBold, color: 'black', alignment: 'center', margin: [0, 2, 0, 2] });
                }
                if (contentSetting.BillingTax == undefined || contentSetting.BillingTax.isSelected == "Y") {
                    if (taxType == 'VAT') {
                        medicineList.push({ text: Cgst.toString(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] });
                    } else {
                        medicineList.push({ text: Cgst.toString(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] });
                        medicineList.push({ text: Sgst.toString(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] });
                    }
                }
				medicineList.push({ text: item.totlcstaftrtax.toString(), color: 'black', fontSize: cFont,bold:cBold   , alignment: 'center', margin:[0,2,0,2] });
                medicineList.push({ text: AmountRecieved.toString(), color: 'black', fontSize: cFont,bold:cBold   , alignment: 'center', margin:[0,2,0,2] });
                medicineList.push({ text: ((item.Balance === undefined || item.Balance == 0 || item.Balance === '') ? "0" : Balance.toString()), fontSize: cFont,bold:cBold   , color: 'black', alignment: 'center', margin:[0,2,0,2] });
                billingarray1.push(medicineList);
            });
            
            var finalBillArray = []; var Proccost = 0, ProcDiscountcost = 0, ProcBal = 0 ,totlcstaftrtax =0;
            Procrecvdcost = 0;
			preBillNumber = 0;
            for (var i = 0; i < billinginfo.length; i++) {

                Proccost = Math.round(Proccost + billinginfo[i].ProcedureCost * billinginfo[i].Multiplyfactor) ;
                
                totlcstaftrtax = Math.round(totlcstaftrtax + billinginfo[i].totlcstaftrtax);
                preBillNumber = preBillNumber + billinginfo[i].BilledCost;
                Procrecvdcost = Math.round(Procrecvdcost + billinginfo[i].Previouspaid + parseFloat(billinginfo[i].BilledCost) +parseInt(billinginfo[i].otherinfo.membershipamt));
                ProcDiscountcost = ProcDiscountcost + billinginfo[i].DiscountCost;
                if (billinginfo[i].Balance !== undefined) {
                    ProcBal = Math.round(ProcBal + billinginfo[i].Balance);
                   
                }
                if (hospCode == 'GOCLINIC') {
                    Procrecvdcost = Math.round(Procrecvdcost + parseFloat(billinginfo[i].ClinicAmount));
                    Procrecvdcost = Math.round(Procrecvdcost + parseFloat(billinginfo[i].PreClinicAmount));
                }
            }
            finalBillArray.push({ text: '',fillColor:'#f8f8f8' });
            finalBillArray.push({ text: 'Total : '.toUpperCase(), alignment: 'left',bold:cBold, fontSize: (cFont+1),fillColor:'#f8f8f8', margin:[3,3,0,3] });
          
            finalBillArray.push({ text: '',fillColor:'#f8f8f8' });
            finalBillArray.push({ text: '',fillColor:'#f8f8f8' });
            finalBillArray.push({ text: '',fillColor:'#f8f8f8' });
            if (scopeObj.discountSetting == 'N') {
                finalBillArray.push({ text: '', fillColor: '#f8f8f8', margin: [0, 5, 0, 5] });
            }
            if (contentSetting.BillingTax == undefined || contentSetting.BillingTax.isSelected == "Y") {
                finalBillArray.push({ text: '', fillColor: '#f8f8f8' });
                if (taxType != 'VAT') {
                    finalBillArray.push({ text: '', fillColor: '#f8f8f8' });
                }
            }
            finalBillArray.push({ text: totlcstaftrtax.toString(), alignment: 'center', bold: cBold, fontSize: (cFont + 1), fillColor: '#f8f8f8', margin: [0, 3, 0, 3] });
            finalBillArray.push({ text: Procrecvdcost.toString(), alignment: 'center',bold:cBold, fontSize: (cFont+1),fillColor:'#f8f8f8', margin:[0,3,0,3] });
            finalBillArray.push({ text: ProcBal.toString(), alignment: 'center',bold:cBold, fontSize: (cFont+1),fillColor:'#f8f8f8', margin:[0,3,0,3] });
            billingarray1.push(finalBillArray);
            for (var i = 0; i < billingarray1.length; i++)
                bodyItems.push(billingarray1[i]);
            if (bodyItems.length == 1)
                return null;
            var billtext="",invoicetext="";
            if (hospCode != "" && hospCode !== undefined) {
                if(preBillNumber > 0){
                    if (scopeObj.billno !== undefined && scopeObj.billno != "" && (contentSetting.billReceipt==undefined || contentSetting.billReceipt.isSelected=="Y")) {
                        scopeObj.billnotxt = "Receipt No: " + scopeObj.billno;
                        billtext={ text: scopeObj.billnotxt, alignment: 'right',fontSize:(contentSetting.billReceipt!=undefined?contentSetting.billReceipt.fontSize:cFont),bold:(contentSetting.billReceipt!=undefined?contentSetting.billReceipt.isBold=="Y":cBold) };
                    }
                }
				if (scopeObj.invoiceno != undefined && scopeObj.invoiceno != "" && (contentSetting.billInvoice==undefined || contentSetting.billInvoice.isSelected=="Y")) {
                    scopeObj.invoicenotxt = "Invoice No: " + scopeObj.invoiceno;
                    invoicetext={ text: scopeObj.invoicenotxt, alignment: 'left',fontSize:(contentSetting.billInvoice!=undefined?contentSetting.billInvoice.fontSize:cFont),bold:(contentSetting.billInvoice!=undefined?contentSetting.billInvoice.isBold=="Y":cBold) };
                }
            }
            if (contentSetting.BillingTax == undefined || contentSetting.BillingTax.isSelected == "Y") {
                if (taxType == 'VAT') {
                    if (printPageSize == "A5") {
                        printPageWidth = [15, 45, '*', 20, 10, 30, 10, 30, 40, 35];
                    } else {
                        printPageWidth = [15, 60, '*', 30, 20, 55, 30, 30, 60, 45];
                    }
                } else {
                    if (printPageSize == "A5") {
                        printPageWidth = [10, 40, '*', 25, 20, 40, 10, 10, 30, 40, 40];
                    } else {
                        printPageWidth = [15, 60, '*', 30, 20, 55, 30, 30, 30, 60, 45];
                    }
                }

                if (taxType == 'VAT') {
                    if (printPageSize == "A5") {
                        printNewPageWidth = [20, 50, '*', 30, 20, 20, 50, 50, 50];
                    } else {
                        printNewPageWidth = [25, 70, '*', 30, 25, 25, 70, 50, 50];
                    }
                } else {
                    if (printPageSize == "A5") {
                        printNewPageWidth = [10, 40, '*', 30, 20, 20, 20, 40, 40, 40];
                    } else {
                        printNewPageWidth = [25, 70, '*', 30, 20, 35, 35, 60, 50, 50];
                    }
                }
            } else {
                if (printPageSize == "A5") {
                    printPageWidth = [20, 45, '*', 25, 20, 40, 30, 40, 40];
                } else {
                    printPageWidth = [15, 60, '*', 30, 20, 55, 70, 60, 55];
                }

                if (printPageSize == "A5") {
                    printNewPageWidth = [20, 50, '*', 30, 20, 40, 40, 40];
                } else {
                    printNewPageWidth = [25, 70, '*', 30, 20, 60, 50, 50];
                }
            }
            var borderSet="noBorders";
            if(contentSetting.patientBill!=undefined && contentSetting.patientBill.border=="Y"){
                borderSet={
                hLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                },
                vLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                },
                hLineColor: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                },
                vLineColor: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                },
                };
            }
            if (scopeObj.discountSetting == 'N') {
                var patientVitalNotes = [{
                    style: 'tableExample',
                        table: {
                            widths: ['*', '*'],
                            body: [[ invoicetext,billtext ]]
                        },
                        layout: 'noBorders',

                    },{
                    style: 'tableExample',
                    table: {
                        
                        widths: printPageWidth,
                        body: bodyItems,
                        border:false
                    },
                    layout: borderSet,
                }];
            }
            else{
                var patientVitalNotes = [{
                    style: 'tableExample',
                        table: {
                            widths: ['*', '*'],
                            body: [[invoicetext,billtext ]]
                        },
                        layout: 'noBorders'
                    },{
                    style: 'tableExample',
                    table: {
                        
                        widths: printNewPageWidth,
                        body: bodyItems,
                    },
                    layout:borderSet
                    
                }];
            }
            return patientVitalNotes;
    }
    //End of get Billing Rows

    //get payment Billing Rows
    function getPaymentBillRows(prevbill, scopeObj) {
        var currencyType = "INR";
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes !== undefined) {
            if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.currencyCode !== undefined) {
                currencyType = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.currencyCode;
            }
        }

        var cBold = false;
        var cFont = defaultFontSize;

        var hospCode = "";
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined) {
            hospCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        }


        var billingarray1 = [], bodyItems = [];
        var billingList = [];
        billingList.push({ text: 'S.No'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
        billingList.push({ text: 'Date'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
        billingList.push({ text: 'Treatment'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
        billingList.push({ text: 'Cost'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
        billingList.push({ text: 'Qty'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
       // billingList.push({ text: 'Total Cost'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
        if (scopeObj.discountSetting == 'N') {
            billingList.push({ text: 'Discount'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
        }
        if (contentSetting.BillingTax == undefined || contentSetting.BillingTax.isSelected == "Y") {
            if (taxType == 'VAT') {
                billingList.push({ text: 'Vat%'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
            } else {
                billingList.push({ text: 'Cgst%'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
                billingList.push({ text: 'Sgst%'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
            }
        }
		 billingList.push({ text: 'Total Cost'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
        billingList.push({ text: 'Received'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
        billingList.push({ text: 'Balance'.toUpperCase(), bold: true, fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', margin: [0, 5, 0, 5] });
        billingarray1.push(billingList);

        angular.forEach(prevbill, function (item, $index) {
            var medicineList = [];
            var consultdt = $filter('date')(new Date(item.BillDate), "dd-MMM-yyyy");
            var amountpaid = "";
            var stack=[{ text: item.ProcedureName.toUpperCase(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] }];
            if(contentSetting.billNotes!=undefined && contentSetting.billNotes.isSelected=="Y"){
                var notes=item.otherinfo!=undefined?item.otherinfo.notes:(item.notes!=undefined?item.notes:"");
                if(notes!=""&& notes!=undefined){
                    stack.push({ text: ("Notes: "+notes), color: 'black', fontSize: contentSetting.billNotes.fontSize, bold: (contentSetting.billNotes.isBold=="Y"?true:false), alignment: 'center', margin: [0, 2, 0, 2] });
                }
            }
            if (item.BilledCost == 0) {
                amountpaid = Math.round((item.Previouspaid != undefined ? item.Previouspaid : 0) + parseInt(item.membershipamt)) ;
            }
            else if (item.Previouspaid != undefined) {
                amountpaid = Math.round(item.Previouspaid + parseFloat(item.BilledCost) + parseInt(item.membershipamt));
            }
            else {
                amountpaid = Math.round(parseFloat(item.BilledCost) + parseInt((item.membershipamt)? item.membershipamt:0));
            }

            var DiscountType = "%";
            if (item.DiscountType == "NUMBER") {
                DiscountType = currencyType;
            }
			
                      
            if (item.otherinfo != undefined) {
                if (item.otherinfo.TaxDetails != undefined) {
                    var Cgst = item.otherinfo.TaxDetails.Cgst;
                    var Sgst = item.otherinfo.TaxDetails.Sgst;
                }
            } else {

                var Cgst = item.Cgst;
                var Sgst = item.Sgst;
            }
						   
            var finalamount = item.ProcedureCost * item.Multiplyfactor;
			if(item.DiscountType == "%"){
				    discountAmount = parseFloat(finalamount) * parseFloat(item.DiscountCost/ 100);
				    cgstAmount =  (finalamount - discountAmount) *(Cgst/100);
			        sgstAmount = (finalamount - discountAmount) *(Sgst/100); 
				        var totlcstaftrtax =  Math.round((finalamount - discountAmount) + cgstAmount + sgstAmount);
		    }else if( item.DiscountType == "NUMBER"){
			        discountAmount = parseFloat(item.DiscountCost);
				    cgstAmount =  (finalamount - discountAmount) *(Cgst/100);
			        sgstAmount = (finalamount - discountAmount) *(Sgst/100); 
				    var totlcstaftrtax =   Math.round((finalamount - discountAmount) + cgstAmount + sgstAmount);			 
		    }else{
				    discountAmount= 0; 
				    cgstAmount = (finalamount - discountAmount) *(Cgst/100);
			        sgstAmount = (finalamount - discountAmount) *(Sgst/100); 
				    var totlcstaftrtax =  Math.round((finalamount - discountAmount) + cgstAmount + sgstAmount);
				    }
			var percentage = "%"
            var a = item.ProcedureCost;
            var AmountRecieved = "";
            AmountRecieved = amountpaid;
			var Balance = Math.round(item.Balance);
            medicineList.push({ text: ($index + 1) + ".", color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] });
            medicineList.push({ text: consultdt.toUpperCase(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] });
            medicineList.push(stack);
            medicineList.push({ text: item.ProcedureCost.toString(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] });
            medicineList.push({ text: item.Multiplyfactor.toString(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] });
           // medicineList.push({ text: finalamount.toString(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] });
            if (scopeObj.discountSetting == 'N') {
                medicineList.push({ text: item.DiscountCost.toString() + " " + DiscountType, fontSize: cFont, bold: cBold, color: 'black', alignment: 'center', margin: [0, 2, 0, 2] });
            }
            if (contentSetting.BillingTax == undefined || contentSetting.BillingTax.isSelected == "Y") {
                if (taxType == 'VAT') {
                    medicineList.push({ text: Cgst.toString(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] });
                } else {
                    medicineList.push({ text: Cgst.toString(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] });
                    medicineList.push({ text: Sgst.toString(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] });
                }
            }

			medicineList.push({ text: item.totlcstaftrtax.toString(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] });
            medicineList.push({ text: AmountRecieved.toString(), color: 'black', fontSize: cFont, bold: cBold, alignment: 'center', margin: [0, 2, 0, 2] });
            medicineList.push({ text: ((item.Balance === undefined || item.Balance == 0 || item.Balance === '') ? "0" : Balance.toString()), fontSize: cFont, bold: cBold, color: 'black', alignment: 'center', margin: [0, 2, 0, 2] });
            billingarray1.push(medicineList);
        });



        for (var i = 0; i < billingarray1.length; i++)
            bodyItems.push(billingarray1[i]);
        if (bodyItems.length == 1)
            return null;
        var hospCode = "";
        if (hospCode != "" && hospCode !== undefined) {
            if (preBillNumber > 0) {
                if (scopeObj.billno !== undefined && scopeObj.billno != "" && (contentSetting.billReceipt == undefined || contentSetting.billReceipt.isSelected == "Y")) {
                    scopeObj.billnotxt = "Receipt No: " + scopeObj.billno;
                    billtext = { text: scopeObj.billnotxt, alignment: 'right', fontSize: (contentSetting.billReceipt != undefined ? contentSetting.billReceipt.fontSize : cFont), bold: (contentSetting.billReceipt != undefined ? contentSetting.billReceipt.isBold == "Y" : cBold) };
                }
            }
            if (scopeObj.invoiceno != undefined && scopeObj.invoiceno != "" && (contentSetting.billInvoice == undefined || contentSetting.billInvoice.isSelected == "Y")) {
                scopeObj.invoicenotxt = "Invoice No: " + scopeObj.invoiceno;
                invoicetext = { text: scopeObj.invoicenotxt, alignment: 'left', fontSize: (contentSetting.billInvoice != undefined ? contentSetting.billInvoice.fontSize : cFont), bold: (contentSetting.billInvoice != undefined ? contentSetting.billInvoice.isBold == "Y" : cBold) };
            }

        }
        else {
            billtext = "";
            invoicetext = "";
        }
        if (contentSetting.BillingTax == undefined || contentSetting.BillingTax.isSelected == "Y") {
            if (taxType == 'VAT') {
                if (printPageSize == "A5") {
                    printPageWidth = [15, 45, '*', 20, 10, 30, 10, 30, 40, 35];
                } else {
                    printPageWidth = [15, 60, '*', 30, 20, 55, 30, 30, 60, 45];
                }
            } else {
                if (printPageSize == "A5") {
                    printPageWidth = [15, 45, '*', 20, 10, 30, 10, 20, 30, 40, 35];
                } else {
                    printPageWidth = [15, 60, '*', 30, 20, 55, 30, 30, 30, 60, 45];
                }
            }

            if (taxType == 'VAT') {
                if (printPageSize == "A5") {
                    printNewPageWidth = [20, 50, '*', 30, 20, 20, 50, 50, 50];
                } else {
                    printNewPageWidth = [25, 70, '*', 30, 25, 25, 70, 50, 50];
                }
            } else {
                if (printPageSize == "A5") {
                    printNewPageWidth = [10, 40, '*', 30, 20, 20, 20, 40, 40, 40];
                } else {
                    printNewPageWidth = [25, 70, '*', 30, 20, 35, 35, 60, 50, 50];
                }
            }
        } else {
            if (printPageSize == "A5") {
                printPageWidth = [20, 45, '*', 25, 20, 40, 30, 40, 40];
            } else {
                printPageWidth = [15, 60, '*', 30, 20, 55, 70, 60, 55];
            }

            if (printPageSize == "A5") {
                printNewPageWidth = [20, 50, '*', 30, 20, 40, 40, 40];
            } else {
                printNewPageWidth = [25, 70, '*', 30, 20, 60, 50, 50];
            }
        }
		
        var borderSet = "noBorders";
        if (contentSetting.patientBill != undefined && contentSetting.patientBill.border == "Y") {
            borderSet = {
                hLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 0.5 : 0.5;
                },
                vLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                },
                hLineColor: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                },
                vLineColor: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                },
            };
        }
        if (scopeObj.discountSetting == 'N') {
            var patientVitalNotes = {
                style: 'tableExample',
                table: {
                    
                    widths: printPageWidth,
                    body: bodyItems,
                    border: false
                },
                layout: borderSet,
            };
        }
        else {
            var patientVitalNotes = {
                style: 'tableExample',
                table: {
                   
                    widths: printNewPageWidth,
                    body: bodyItems,
                },
                layout: borderSet

            };
        }
        return patientVitalNotes;
    }

    //End of get payment Billing Rows

    //get Followup
    function getFollowup(notes) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientFollowup){
            cBold=contentSetting.patientFollowup.isBold=="Y";
            cFont=contentSetting.patientFollowup.fontSize;
        }
        var followupnotes = notes;
        if(contentSetting.patientFollowup.isSelected=="N"){
            var followupNotesDetails = [{
                text: '                           ',
            }, '          '];
            return followupNotesDetails;
        }

                //console.log(followupnotes.length);
        if (followupnotes.length == 0) {

            var followupNotesDetails = [{
                text: '                           ',
            }, '          '];
            return followupNotesDetails;
        } else {

            var followupNotesDetails = [{
                text: 'Follow-up Notes'.toUpperCase(), 
                bold: true,
                fontSize: cFont
            }, { text: notes, bold: cBold, fontSize:cFont }];
            return followupNotesDetails;
        }
    }
    //End of get Followup

    //get Lab Order Rows
    function getLabOrderRows(consultinfo){
        if (!consultinfo.LabTestObject.LabOrderArray) {
                return;
            }
            console.log(JSON.stringify(consultinfo.LabTestObject.LabOrderArray));
            var array1 = [], bodyItems = [
             [
             { text: 'Lab Order No'.toUpperCase(), fontSize: defaultFontSize, bold: true, alignment: 'center', fillColor:'#eaeaea' },
             { text: 'Teeth'.toUpperCase(), fontSize: defaultFontSize, bold: true, alignment: 'center', fillColor:'#eaeaea' },
             { text: 'Type '.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', bold: true, fillColor:'#eaeaea' },
             { text: 'Variety '.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', bold: true, fillColor:'#eaeaea' },
             { text: 'Shade '.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', bold: true, fillColor:'#eaeaea' },
             { text: 'Trial Date'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', bold: true, fillColor:'#eaeaea' },
             { text: 'Delivery Date '.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', bold: true, fillColor:'#eaeaea' },
             { text: 'Status '.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', bold: true, fillColor:'#eaeaea' },
             { text: 'Lab Name '.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', bold: true, fillColor:'#eaeaea' },
             ]
            ];

            angular.forEach(consultinfo.LabTestObject.LabOrderArray, function (item) {
                var Laborder = [];
                Laborder.push({ text: item.RowID.toString(), color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                Laborder.push({ text: item.bodyParts.Teeth.toString(), color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                Laborder.push({ text: item.orderedItems.typesDesc, color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                Laborder.push({ text: item.orderedItems.varietyDesc, color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                Laborder.push({ text: item.orderedItems.shadeDesc, color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                Laborder.push({ text: item.trialDate.toString(), color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                Laborder.push({ text: item.finalDelivaryDate.toString(), color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                Laborder.push({ text: item.labOrderDesc, color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                Laborder.push({ text: item.LabNameDesc, color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                array1.push(Laborder);

            });
            for (var i = 0; i < array1.length; i++)
                bodyItems.push(array1[i]);
            if (bodyItems.length == 1)
                return null;
            var patientLaborder = {
                "style": 'tableExample',
                "table": {
                    "widths": [30,30, 40, 50, 30, 60, 60, 50, '*'],
                    "body": bodyItems
                },
            layout: "noBorders"
            };
            return patientLaborder;
    }
    //End of get Lab Order Rows
    
    //get Prescription Rows
    function getPrescriptionRows(consultinfo){
        if (consultinfo.prescription.medicines.length == 0)
                        return;
        var beforeMealItems = [], afterMealItems = [];
        
            bodyItems = [
                         [{ text: 'S.No'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor: '#eaeaea', bold: true, margin: [0, 5, 0, 5] },
                         { text: 'Medicine'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor: '#eaeaea', bold: true, margin: [0, 5, 0, 5] },
                          { text: 'Dosage\nM - A - N'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor: '#eaeaea', bold: true, margin: [0, 5, 0, 5] },
                          { text: 'Days'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor: '#eaeaea', bold: true, margin: [0, 5, 0, 5] },
                          { text: 'Meal'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor: '#eaeaea', bold: true, margin: [0, 5, 0, 5] },
                          { text: 'Notes'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor: '#eaeaea', bold: true, margin: [0, 5, 0, 5] }]
            ];
            if (contentSetting.PrescriptionNotes !== undefined && contentSetting.PrescriptionNotes.isSelected == 'Y') {
                bodyItems[0].splice(5, 1);
            }
            if (contentSetting.AfterBeforeMeal !== undefined && contentSetting.AfterBeforeMeal.isSelected == 'N') {
                bodyItems[0].splice(4, 1);
            }
                    
            angular.forEach(consultinfo.prescription.medicines, function (item, $index) {
                
                        var medicineList = [];
                        var numOfDys;
                        var mealMode = ' - ';
                        var MedicineGenericName = false;
                        var GenericisBold = 'N';
                        var GenericSize = 10;
                        var bottom = 30;
                        if (item.MedicineGenericName !== '' && contentSetting.GenericName !== undefined && contentSetting.GenericName.isSelected == 'Y') {
                            MedicineGenericName = true;
                            GenericSize = contentSetting.GenericName.fontSize;
                            GenericisBold = contentSetting.GenericName.isBold;

                        }
                        if (item.MedicineGenericName == '') {
                            MedicineGenericName = false;
                        }
                        if (isNaN(item.NumberofDays) || item.NumberofDays == null || item.NumberofDays == 0)
                            numOfDys = ' 0 ';
                        else
                            numOfDys = item.NumberofDays;
                        if (numOfDys !== ' - ')
                            if (item.MedicineType === 'GEL' || item.MedicineType === 'RES' || item.MedicineType == 'LOT' || item.MedicineType == 'INH' || item.MedicineType == 'INJ' || item.MedicineType == 'OIN' || item.MedicineType == 'KIT' || item.MedicineType == 'CRE' || item.MedicineType == 'SPR') {
                                mealMode = ''
                            }
                            else if (item.Dosage1 != '' && item.Dosage1 != 0 || item.Dosage2 != '' && item.Dosage2 != 0 || item.Dosage4 != '' && item.Dosage4 != 0) {
                                mealMode = ((item.BeforeAfter) ? "Before Meal" : "After Meal")
                            }
                            else {
                                mealMode = ''
                            }
                        medicineList.push({ text: ($index + 1) + ".", color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                        if (contentSetting.PrescriptionNotes == undefined || contentSetting.PrescriptionNotes.isSelected == 'N') {
                            medicineList.push({
                                text: [
                                    { text: getMedicineType(item) + '' + item.Medicine, fontSize: (contentSetting.MedicineName != undefined && contentSetting.MedicineName.fontSize != undefined) ? contentSetting.MedicineName.fontSize : defaultFontSize, bold: (contentSetting.MedicineName != undefined && contentSetting.MedicineName.isBold == 'Y') ? true : false},
                                    { text: (MedicineGenericName == false) ? '' : "\nGeneric Name: " + item.MedicineGenericName, fontSize: GenericSize, color: 'black', bold: (GenericisBold == 'Y') ? true : false },
                                    
                                ], color: 'black', fontSize: defaultFontSize
                            });
                        } else {
                            medicineList.push({
                                text: [
                                    { text: getMedicineType(item) + '' + item.Medicine, fontSize: (contentSetting.MedicineName != undefined && contentSetting.MedicineName.fontSize != undefined) ? contentSetting.MedicineName.fontSize : defaultFontSize, bold: (contentSetting.MedicineName != undefined && contentSetting.MedicineName.isBold == 'Y') ? true : false, color: 'black' },
                                    { text: (MedicineGenericName == false) ? '' : "\nGeneric Name: " + item.MedicineGenericName, fontSize: GenericSize, color: 'black', bold: (GenericisBold == 'Y') ? true : false },
                                    { text: (item.Diagnostic == '' || item.Diagnostic == undefined) ? '' : "\nNotes :"+ item.Diagnostic, fontSize: contentSetting.PrescriptionNotes.fontSize, color: 'black', bold: (contentSetting.PrescriptionNotes.isBold == 'Y')? true:false}
                                ], 
                            });
                        }
                        medicineList.push({ text: ConcatMeal(item).toUpperCase(), color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                        medicineList.push({ text: (numOfDys == ' 0 ') ? '' : numOfDys.toString().toUpperCase(), color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                        if (contentSetting.AfterBeforeMeal == undefined || contentSetting.AfterBeforeMeal.isSelected == 'Y') {
                            medicineList.push({ text: mealMode, color: 'black', fontSize: defaultFontSize, alignment: 'center' });
                        }
                        
                        if (contentSetting.PrescriptionNotes == undefined || contentSetting.PrescriptionNotes.isSelected == 'N') {
                            medicineList.push({ text: item.Diagnostic, fontSize: defaultFontSize, color: 'black', alignment: 'center' });
                        }
                        //Before Meal
                        if (item.BeforeAfter)
                            beforeMealItems.push(medicineList);
                        else
                            afterMealItems.push(medicineList);
                    });
                    var rowCount=1;
                    for (var i = 0; i < beforeMealItems.length; i++){
                        beforeMealItems[i][0].text=rowCount+".";rowCount++;
                        bodyItems.push(beforeMealItems[i]);
                    }
                    for (var i = 0; i < afterMealItems.length; i++){
                        afterMealItems[i][0].text=rowCount+".";rowCount++;
                        bodyItems.push(afterMealItems[i]);
                    }
                    if (bodyItems.length == 1)
                        return null;
                    if (contentSetting.PrescriptionNotes !== undefined && contentSetting.PrescriptionNotes.isSelected == 'Y' && contentSetting.AfterBeforeMeal !== undefined && contentSetting.AfterBeforeMeal.isSelected == 'N') {
                        if (printPageSize == "A5") {
                            printPageWidth = [25, '*', 100, 100];
                        }
                        else {
                            printPageWidth = [60, '*', 100, 80]
                        }
                    } else if (contentSetting.PrescriptionNotes !== undefined && contentSetting.PrescriptionNotes.isSelected == 'Y' || contentSetting.AfterBeforeMeal !== undefined && contentSetting.AfterBeforeMeal.isSelected == 'N') {
                        if (printPageSize == "A5") {
                            printPageWidth = [25, '*', 60, 30, 60];
                        }
                        else {
                            printPageWidth = [40, '*', 80, 60, 100]
                        }
                    } else {
                        if (printPageSize == "A5") {
                            printPageWidth = [25, 90, 60, 30, 50, '*'];
                        }
                        else {
                            printPageWidth = [40, 160, 80, 60, 80, '*']
                        }
                    }
                    var borderSet="noBorders";
                    if(contentSetting.patientPrescription!=undefined && contentSetting.patientPrescription.border=="Y"){
                        borderSet={
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                        };
                    }
                    var patientVitalNotes = {
                        style: 'tableExample',
                        table: {
                            widths: printPageWidth,
                            body: bodyItems
                        },
                        layout: borderSet
                    };
                    return patientVitalNotes;
    }
    //End of get Prescription Rows

    //get Ophthal
    //Generate table for ophthal notes
    function getOphthalInfoTable(ophthalData){

      var emptyRow = getEmptyTable();
      var content=[];
      content.push(emptyRow);
      if(ophthalData==undefined)
        return content;
      if(ophthalData.conditions){
        var bodyItems=[
          [{text:'',fillColor:'#eaeaea'},
           { text: 'RIGHT EYE'.toUpperCase(),bold:true, fontSize: defaultFontSize, colSpan: 4, alignment: 'center', fillColor:'#eaeaea' }, {}, {}, {},
           { text: 'LEFT EYE'.toUpperCase(),bold:true, fontSize: defaultFontSize, colSpan: 4, alignment: 'center', fillColor:'#eaeaea' }, {}, {}, {}],
          [ {text:'',fillColor:'#f8f8f8'},
            { text: 'SPH'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor:'#f8f8f8' },
            { text: 'CYL'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor:'#f8f8f8' },
            { text: 'AXIS'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor:'#f8f8f8' },
            { text: 'VIS'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor:'#f8f8f8' },
            { text: 'SPH'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor:'#f8f8f8' },
            { text: 'CYL'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor:'#f8f8f8' },
            { text: 'AXIS'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor:'#f8f8f8' },
            { text: 'VIS'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor:'#f8f8f8' }]
        ];
        var distant=[];near=[];
        distant.push({ text: 'DV'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor:'#f8f8f8' });
        near.push({ text: 'NV '.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor:'#f8f8f8' });
        for(var i=0;i<2;i++){
          
          for(var j=0;j<8;j++){
            var index=(i*8)+j;
            if(j<4){
                distant.push({ text: ophthalData.conditions[index]['ConditionDesc'], fontSize: defaultFontSize, alignment: 'center' });
            }
            else{
                near.push({ text: ophthalData.conditions[index]['ConditionDesc'], fontSize: defaultFontSize, alignment: 'center' });
            }
          }

        }
        bodyItems.push(distant);
        bodyItems.push(near);
        //var leftEye=[];
      }
      //console.log(bodyItems);
      var printPageWidth=['*', 50, 50, 50, 50,50, 50, 50, 50];
      if(printPageSize=="A5"){
        printPageWidth=['*', 30, 30, 30, 30,30, 30, 30, 30];
      }
      
      var ophNotes = {
            table: {
                widths: printPageWidth,
                body: bodyItems
            },
            layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
        };
      content.push(ophNotes);
      return content;
    }
    //Generate table for ophthal lens

    function getOphthalLensInfo(ophthalData){
      
      var content=[];
      bodyItems='';
      if(ophthalData==undefined)
        return content;
      if(ophthalData.chkconditions){
        for(var i=0;i<ophthalData.chkconditions.length;i++){
          if(ophthalData.chkconditions[i]['ModalValue']=="YES"){
            bodyItems+=', '+ophthalData.chkconditions[i]['ConditionDesc'].toUpperCase();
          }
        }
        
      }
      bodyItems=bodyItems.replace(/(^\s*,)|(,\s*$)/g, '')
      var ophLens = {
            //style: 'tableExample',
            //table: {
            //    widths: [70, 40, 50, 40, 40,40, 50, 40, 40, 40],
                 text:bodyItems,
                 fontSize: defaultFontSize
            //}
        };
      content.push(ophLens);
      return content;
    }
    //End of get Ophthal
    
    //Billing Print
    //Print for Billing, Treatment and Lab Tests
    this.generateBillingPrint=function(billinfo,scopeObj){
        doctorinfo=scopeObj.doctorinfo;
        scopeHospitalID=scopeObj.hospitalID||0;  //to handle selected clinc
        var printHospCode=undefined;
        if(doctorinfo.doctordata.doctorAddress[0].ExtDetails!=undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo!=undefined){
            printHospCode=doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        }
        var param={
            "HospitalCD":printHospCode,
            "DoctorID":doctorinfo.doctorid,
            "PageType":"PRINT",
            "IsDefault":"Y",
            "SubPageType":"BILLING",
            "IncludeBase64":"Y"
        }
        this.getPrintSetting(param,function(data){
            data=data[0];
        printPageSize="A4";
        var pageSettings = { marginLeft: 40, marginTop: 100, marginRight: 40, marginBottom: 10, pageOrientation: 'Portrait' };
        var headerHeight=100;
        var footerHeight=100;
        var docSign = "";
        var isDefaultDocSign="Y";
        contentSetting = setContentSettings();
        if(data!=undefined){
            pageSettings=data.JsonSetting.pageSettings;
            headerHeight=data.JsonSetting.clinicSettings.headerSettings.dimension.height;
            footerHeight=data.JsonSetting.clinicSettings.footerSettings.dimension.height;
            contentSetting=data.JsonSetting.clinicSettings.contentSettings;
            docSign=data.JsonSetting.clinicSettings.footerSettings.custFootSign;
            isDefaultDocSign=data.JsonSetting.clinicSettings.footerSettings.isFootSignDefault;
           if (data.JsonSetting.pageSettings.pageSize == "A4") {
                defaultFontSize = 10;
            } else {
                printPageSize="A5";
                defaultFontSize = 8;
            }
        }

        var content = [];
        content.push(getPatientInfoTable(scopeObj.appointmentinfo, scopeObj.patientinfo, scopeObj.doctorinfo, scopeObj.date, scopeObj.displayID,pageSettings.marginRight,pageSettings.pageOrientation));
        if(scopeObj.printType=="BILLING"){
            content.push({ text: 'Receipt'.toUpperCase(), color: 'black', fontSize: (defaultFontSize + 3), alignment: 'center', bold: true });
            if (scopeObj.subPrintType == 'PAYMENTHISTORY') {
                var billingDetails = getPaymentBillRows(billinfo, scopeObj);
            }
            else {
                var billingDetails = getBillRows(billinfo, scopeObj);
            }
            if (billingDetails == undefined || billingDetails == "" || billingDetails == null) {
            }
            else {
                content.push(billingDetails);
            }
        }
        else if(scopeObj.printType=="TREATMENT"){
            var EstimationMainArray=billinfo;
            for (var i = 0; i < EstimationMainArray.length; i++) {
            // console.log($scope.EstimationArrayForPrint[i].RowID);
            if (scopeObj.planNumber == i) {
                content.push({ text: "\n\nEstimation " + EstimationMainArray[i].RowID + "\n\n".toUpperCase(), color: 'black', fontSize: (defaultFontSize+3), alignment: 'center', bold:true });
                var EstimationDetails = getTreatmentRows(EstimationMainArray[i].estimationBodyPart, EstimationMainArray[i].TotalEstimationCost)
                if (EstimationDetails != undefined || EstimationDetails != "" || EstimationDetails != null) {
                    content.push(EstimationDetails);
                }

                content.push({ text: "This Estimation is vaild till : " + EstimationMainArray[i].ValidTillDate + "".toUpperCase(), color: 'black', fontSize: defaultFontSize, alignment: 'left' });
            }

            }
            
        }
        else{
            content.push({ text: "\n\nLAB BILL" + "\n\n".toUpperCase(), color: 'black', fontSize: (defaultFontSize+3), alignment: 'center', bold:true });
            var LabCostdetails = getLabTestRows(billinfo,scopeObj.printindex);
            if (LabCostdetails != undefined || LabCostdetails != "" || LabCostdetails != null) {
                content.push(LabCostdetails);
            }
        }
        if(scopeObj.printType=="BILLING" && billinfo.length==1 && billinfo[0].ProcedureName=="")
            scopeObj.modepayment="";
        content.push(getDoctorSignature(docSign,scopeObj.printType,scopeObj.doctorinfo,scopeObj.modepayment,scopeObj.clinicName,scopeObj.doctorFullName,scopeObj.doctorSpeciality,isDefaultDocSign));
        if (contentSetting.waterMark == undefined || contentSetting.waterMark.isSelected == 'Y') {
            content.push(getFootNote());
        }
        var docdefination = {
            
            header:function(currentPage, pageCount){
                var headerSetting=getHeader(data,pageSettings.marginRight);
                headerSetting.margin=[pageSettings.marginLeft , pageSettings.marginTop, pageSettings.marginRight, 20 ]
                return headerSetting;
            },
            footer:function(currentPage, pageCount){
                var footerSetting=getFooter(data,pageSettings.marginRight);
                footerSetting.margin=[pageSettings.marginLeft , 5, pageSettings.marginRight, pageSettings.marginBottom ]
                return footerSetting;
            },
            "pageMargins": [pageSettings.marginLeft , headerHeight+pageSettings.marginTop, pageSettings.marginRight, footerHeight+pageSettings.marginBottom ],
            "pageSize": printPageSize,
            "pageOrientation": pageSettings.pageOrientation,
            "content": content,
            "styles": {
                "header": {
                    "fontSize": 18,
                    "bold": true,
                    "margin": [0, 0, 0, 10]
                },
                "titleHeader": {
                    "fontSize": 18,
                    "bold": true,
                    "alignment": 'left',
                    "margin": [40, 10, 10, 20],
                    "paddingBottom": 200
                },
                "footer": {
                    "alignment": 'right',
                    "margin": [20, 10, 40, 20],
                },
                "subheader": {
                    "fontSize": 16,
                    "bold": true,
                    "margin": [0, 10, 0, 5]
                },
                "tableExample": {
                    "margin": [0, 5, 0, 15]
                },
                "tableHeader": {
                    "bold": true,
                    "fontSize": 10,
                    "color": 'black'
                },
                "contentText": {
                    "fontSize": 10
                }
            },
        }
        pdfMake.createPdf(docdefination).open();
        },function(msg){
           
        });            
    }
    //End of Billing Print


    //Prescription Print
    //Print for Medicines, Ophthal and Lab Orders
    this.generatePrescriptionPrint=function(scopeObj){
        doctorinfo=scopeObj.doctorinfo;
        scopeHospitalID=scopeObj.hospitalID||0;  //to handle selected clinc
        var emptyRow = getEmptyTable();
      var printHospCode=undefined;
        if(doctorinfo.doctordata.doctorAddress[0].ExtDetails!=undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo!=undefined){
            printHospCode=doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        }
        var param={
            "HospitalCD":printHospCode,
            "DoctorID":doctorinfo.doctorid,
            "PageType":"PRINT",
            "IsDefault":"Y",
            "SubPageType":"PRESCRIPTN",
            "IncludeBase64":"Y"
        }
        this.getPrintSetting(param,function(data){
            data=data[0];
            var pageSettings = { marginLeft: 40, marginTop: 100, marginRight: 40, marginBottom: 10, pageOrientation: 'Portrait' };
        var headerHeight=100;
        var footerHeight=100;
        var docSign = "";
        var isDefaultDocSign="Y";
        printPageSize="A4";
        contentSetting = setContentSettings();
        if(data!=undefined){
            pageSettings=data.JsonSetting.pageSettings;
            headerHeight=data.JsonSetting.clinicSettings.headerSettings.dimension.height;
            footerHeight=data.JsonSetting.clinicSettings.footerSettings.dimension.height;
            contentSetting=data.JsonSetting.clinicSettings.contentSettings;
            docSign=data.JsonSetting.clinicSettings.footerSettings.custFootSign;
            isDefaultDocSign=data.JsonSetting.clinicSettings.footerSettings.isFootSignDefault;
           if (data.JsonSetting.pageSettings.pageSize == "A4") {
                defaultFontSize = 10;
            } else {
                printPageSize="A5";
                defaultFontSize = 8;
            }
        }

        var content = [];
        content.push(getPatientInfoTable(scopeObj.appointmentinfo, scopeObj.patientinfo, scopeObj.doctorinfo, scopeObj.date, scopeObj.displayID,pageSettings.marginRight,pageSettings.pageOrientation));
            if(scopeObj.printType=="MEDICINES"){
                var medicineItems = getPrescriptionRows(scopeObj.consultinfo);
                if (medicineItems == undefined || medicineItems == "" || medicineItems == null) {
                } else {
                    content.push({ text: '\nPRESCRIPTION (Rx)\n\n', color: 'black', bold: true, fontSize: (defaultFontSize+3),alignment:'center' });
                    content.push(medicineItems);
                }
                var followupNotes = getFollowup(scopeObj.followupNotes);
                //console.log(followupNotes);
                if (followupNotes == undefined || followupNotes == "" || followupNotes == null) {
                }
                else {
                    content.push(followupNotes);
                }
            }
            else if(scopeObj.printType=="LABORDERS"){
                var Laborderprint=getLabOrderRows(scopeObj.consultinfo);
                if (Laborderprint) {
                    content.push({ text: '\nLab Order(s)\n\n\n'.toUpperCase(), bold: true, color: 'black', fontSize: (defaultFontSize+3), alignment:'center' });
                content.push(Laborderprint);
                }
            }
            else if(scopeObj.printType=="OPHTHAL"){
                var ophtable=getOphthalInfoTable(scopeObj.opthal);
                var ophlens=getOphthalLensInfo(scopeObj.opthal);
                if(ophtable.length!=0 && ophlens.length!=0)
                {
                    content.push({text:"OPHTHALMOLOGY OBSERVATIONS\n\n\n",alignment:'center',fontSize:(defaultFontSize+3),bold:true});
                    content.push(ophtable);
                    content.push({text:" "});
                    content.push(ophlens);
                    content.push(emptyRow);
                }
            }
			content.push(getDoctorSignature(docSign,scopeObj.printType, scopeObj.doctorinfo, scopeObj.modepayment, scopeObj.clinicName, scopeObj.doctorFullName,scopeObj.doctorSpeciality,isDefaultDocSign));
			if (contentSetting.waterMark == undefined || contentSetting.waterMark.isSelected == 'Y') {
			    content.push(getFootNote());
			}
             var docdefination = {
                        
                        content: content,
                        header:function(currentPage, pageCount){
                            var headerSetting=getHeader(data,pageSettings.marginRight);
                            headerSetting.margin=[pageSettings.marginLeft , pageSettings.marginTop, pageSettings.marginRight, 20 ]
                            return headerSetting;
                        },
                        footer:function(currentPage, pageCount){
                            var footerSetting=getFooter(data,pageSettings.marginRight);
                            footerSetting.margin=[pageSettings.marginLeft , 5, pageSettings.marginRight, pageSettings.marginBottom ]
                            return footerSetting;
                        },
                        pageMargins: [pageSettings.marginLeft , headerHeight+pageSettings.marginTop, pageSettings.marginRight, footerHeight+pageSettings.marginBottom ],
                        pageSize: printPageSize,
                        pageOrientation: pageSettings.pageOrientation,
                        styles: {
                                header: {
                                fontSize: 18,
                                bold: true,
                                margin: [0, 0, 0, 10]
                            },
                            titleHeader: {
                                fontSize: 18,
                                bold: true,
                                alignment: 'left',
                                margin: [40, 10, 10, 20],
                                paddingBottom: 200
                            },
                            footer: {
                                alignment: 'right',
                                margin: [20, 10, 40, 20],
                            },
                            subheader: {
                                fontSize: 16,
                                bold: true,
                                margin: [0, 10, 0, 5]
                            },
                            tableExample: {
                                margin: [0, 5, 0, 15]
                            },
                            tableHeader: {
                                bold: true,
                                fontSize: 10,
                                color: 'black'
                            },
                            contentText: {
                                fontSize: 10
                            }
                        },
                    }
                    pdfMake.createPdf(docdefination).open();
                },function(msg){
                    alertify.log('Error in getting Prescription Print settings');
                });
    }
    //End of Prescription Print

    //Summary Print
    this.generateSummaryPrint=function(scopeObj){
        var emptyRow=getEmptyTable();
        doctorinfo=scopeObj.doctorinfo;
        scopeHospitalID=scopeObj.hospitalID||0;  //to handle selected clinc
        var printHospCode=undefined;
        if(doctorinfo.doctordata.doctorAddress[0].ExtDetails!=undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo!=undefined){
            printHospCode=doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        }
        var param={
            "HospitalCD":printHospCode,
            "DoctorID":scopeObj.doctorinfo.doctorid,
            "PageType":"PRINT",
            "IsDefault":"Y",
            "SubPageType":"SUMMARY",
            "IncludeBase64":"Y"
        }
        this.getPrintSetting(param,function(data){
            data=data[0];
            var pageSettings = { marginLeft: 40, marginTop: 100, marginRight: 40, marginBottom: 10, pageOrientation: 'Portrait' };
        var headerHeight=100;
        var footerHeight=100;
        var docSign = "";
        var isDefaultDocSign="";
        printPageSize="A4";
        contentSetting = setContentSettings();
        if(data!=undefined){
            pageSettings=data.JsonSetting.pageSettings;
            headerHeight=data.JsonSetting.clinicSettings.headerSettings.dimension.height;
            footerHeight=data.JsonSetting.clinicSettings.footerSettings.dimension.height;
            contentSetting=data.JsonSetting.clinicSettings.contentSettings;
            docSign=data.JsonSetting.clinicSettings.footerSettings.custFootSign;
            isDefaultDocSign=data.JsonSetting.clinicSettings.footerSettings.isFootSignDefault;
        if (data.JsonSetting.pageSettings.pageSize == "A4") {
                defaultFontSize = 10;
            } else {
                printPageSize="A5";
                defaultFontSize = 8;
            }
        }

        var content = [];
        content.push(getPatientInfoTable(scopeObj.appointmentinfo, scopeObj.patientinfo, scopeObj.doctorinfo, scopeObj.date, scopeObj.displayID,pageSettings.marginRight,pageSettings.pageOrientation));
            // added by vignesh for print 
            
        var Visitreason = getVisitReasonDetails(scopeObj.visitreason);
        if (Visitreason && contentSetting.patientVisitReason.isSelected!="N")
            content.push(Visitreason);
        var MedicalHistoryItems = getMedicalHistoryTable(scopeObj.consultinfo);
        
        var DentalHistoryItems = getDentalHistoryTable(scopeObj.consultinfo);
        if (MedicalHistoryItems == undefined || MedicalHistoryItems == "" || MedicalHistoryItems == null || contentSetting.patientMedicalHistory.isSelected == "N") {
        }
        else {
            content.push({ text: 'Medical History'.toUpperCase(), color: 'black', bold: true, fontSize: defaultFontSize });
            content.push(MedicalHistoryItems);
        }
        if (DentalHistoryItems == undefined || DentalHistoryItems == "" || DentalHistoryItems == null || contentSetting.patientDentalHistory.isSelected == "N") {
        }
        else {
            content.push({ text: 'Dental History'.toUpperCase(), color: 'black', bold: true, fontSize: defaultFontSize });
            content.push(DentalHistoryItems);
        }
        var vitals1 = getPatientVitalsTable1(scopeObj.consultinfo);
        var vitals2 = getPatientVitalsTable2(scopeObj.consultinfo, scopeObj.bmi);
        var vitals3 = getPatientVitalsTable3(scopeObj.consultinfo);
        var vitals4 = getPatientVitalsTable4(scopeObj.consultinfo);
        var optha = GetOphTablePDF(scopeObj.ophthal);
        var Lens = getOphthalLensPDF(scopeObj.opthalLens);
        if (vitals1 == undefined || vitals1 == "" || vitals1 == null ||contentSetting.patientVitals.isSelected=="N") {
        }
        else {
            content.push({ text: 'Parameters'.toUpperCase(), color: 'black', bold: true, fontSize: defaultFontSize });
            content.push(vitals1);
        }
        if (vitals2 == undefined || vitals2 == "" || vitals2 == null || contentSetting.patientVitals.isSelected=="N") {
        }
        else {
            content.push(vitals2);
        }
        if (vitals3 == undefined || vitals3 == "" || vitals3 == null || contentSetting.patientVitals.isSelected=="N") {
        }
        else {
            content.push(vitals3);
        }
        if (vitals4 == undefined || vitals4 == "" || vitals4 == null || contentSetting.patientVitals.isSelected=="N") {
        }
        else {
            content.push(vitals4);
        }
        if (optha) {
            content.push(optha);
        }
        //content.push(emptyRow);
        if (Lens) {
            content.push(Lens);
        }
        //content.push(emptyRow);

        var diagnosisNotes1 = getDiagnosisNotes(scopeObj.consultinfo,scopeObj.diagnosisNotes);
        var Investgation = getInvestigation(scopeObj.consultinfo,scopeObj.investigationNotes);
        var diagnosisNotes = getDiagnosisNotesTable(scopeObj.consultinfo);
        var emptyRecord = getEmptyTable();
        var medicineItems = getMedicineListTable(scopeObj.consultinfo);
        if (Investgation && contentSetting.patientAnalysisNotes.isSelected != "N") {
            content.push(Investgation);
        }
        if (diagnosisNotes && contentSetting.patientNotes.isSelected != "N") {
            content.push(diagnosisNotes);
        }
        if (diagnosisNotes1  && contentSetting.patientDiagnosisNotes.isSelected !="N") {
            content.push(diagnosisNotes1);
            
        }
        //content.push(emptyRecord);
        
        //content.push(emptyRecord);
        
            //content.push(emptyRecord);
        var labItems = getLabInfoTable(scopeObj.consultinfo, scopeObj.labPrepration);
        var cFont = defaultFontSize;
        var cBold = true;
        if (contentSetting.patientLabTest) {
            cFont = contentSetting.patientLabTest.fontSize;
        }
        if (labItems == undefined || labItems == "" || labItems == null || contentSetting.patientLabTest.isSelected == "N") {
        }
        else {
            content.push({ text: 'Lab Test'.toUpperCase(), color: 'black', bold: cBold, fontSize: cFont });
            content.push(labItems);
        }
        var cFont=defaultFontSize;
        var cBold=true;
        if(contentSetting.patientPrescription){
            cFont=contentSetting.patientPrescription.fontSize;
        }
        if (medicineItems == undefined || medicineItems == "" || medicineItems == null || contentSetting.patientPrescription.isSelected == "N") {
        }
        else {
            content.push({ text: '\nPRESCRIPTION (Rx)', color: 'black', bold: cBold, fontSize: cFont });
            content.push(medicineItems);
        }
        var prescriptionTreatment = GetTreatmentTable(scopeObj)
        var cFont=defaultFontSize;
        var cBold=true;
        if(contentSetting.patientPrescription){
            cFont=contentSetting.patientPrescription.fontSize;
        }
        if (prescriptionTreatment == undefined || prescriptionTreatment == "" || prescriptionTreatment == null || contentSetting.patientTreatment.isSelected == "N") {
        }
        else {
            content.push({ text: 'Treatment'.toUpperCase(), color: 'black', bold: cBold, fontSize: cFont });
            content.push(prescriptionTreatment);
        }
        
        var dentaltreatments = getDentalTreatments(scopeObj.consultinfo,scopeObj);
        var cFont=defaultFontSize;
        var cBold=true;
        if(contentSetting.patientTreatment){
            cFont=contentSetting.patientTreatment.fontSize;
        }
        if (dentaltreatments == undefined || dentaltreatments == "" || dentaltreatments == null || contentSetting.patientTreatment.isSelected == "N") {
        }
        else {
            content.push({ text: 'Treatments For Teeth / Gums'.toUpperCase(), color: 'black', bold: cBold, fontSize: cFont });
            content.push(dentaltreatments);
        }
        var TmjTreatments = getTmjTreatments(scopeObj.consultinfo,scopeObj);
        var cFont=defaultFontSize;
        var cBold=true;
        if(contentSetting.patientTreatment){
            cFont=contentSetting.patientTreatment.fontSize;
        }
        if (TmjTreatments == undefined || TmjTreatments == "" || TmjTreatments == null || contentSetting.patientTreatment.isSelected == "N") {
        }
        else {
            content.push({ text: 'Treatments For Tmj'.toUpperCase(), color: 'black', bold: cBold, fontSize: cFont });
            content.push(TmjTreatments);
        }
        var MucosaTreatments = getMucosaTreatments(scopeObj.consultinfo,scopeObj);
        var cFont=defaultFontSize;
        var cBold=true;
        if(contentSetting.patientTreatment){
            cFont=contentSetting.patientTreatment.fontSize;
        }
        if (MucosaTreatments == undefined || MucosaTreatments == "" || MucosaTreatments == null || contentSetting.patientTreatment.isSelected == "N") {
        }
        else {
            content.push({ text: 'Treatments For Mucosa'.toUpperCase(), color: 'black', bold: cBold, fontSize: cFont });
            content.push(MucosaTreatments);
        }
        var dentalProgresstreatments = getDentalProgressTreatments(scopeObj.consultinfo);
        var cFont=defaultFontSize;
        var cBold=true;
        if(contentSetting.patientTreatment){
            cFont=contentSetting.patientTreatment.fontSize;
        }
        if (dentalProgresstreatments == undefined || dentalProgresstreatments == "" || dentalProgresstreatments == null || contentSetting.patientTreatment.isSelected == "N") {
        }
        else {
            content.push({ text: 'Treatments in Progress For Teeth / Gums'.toUpperCase(), color: 'black', bold: cBold, fontSize: cFont });
            content.push(dentalProgresstreatments);
        }
        var TmjProgressTreatments = getTmjProgressTreatments(scopeObj.consultinfo);
        var cFont=defaultFontSize;
        var cBold=true;
        if(contentSetting.patientTreatment){
            cFont=contentSetting.patientTreatment.fontSize;
        }
        if (TmjProgressTreatments == undefined || TmjProgressTreatments == "" || TmjProgressTreatments == null || contentSetting.patientTreatment.isSelected == "N") {
        }
        else {
            content.push({ text: 'Treatments in Progress For Tmj'.toUpperCase(), color: 'black', bold: cBold, fontSize: cFont });
            content.push(TmjProgressTreatments);
        }
        var MucosaProgressTreatments = getMucosaProgressTreatments(scopeObj.consultinfo);
        var cFont=defaultFontSize;
        var cBold=true;
        if(contentSetting.patientTreatment){
            cFont=contentSetting.patientTreatment.fontSize;
        }
        if (MucosaProgressTreatments == undefined || MucosaProgressTreatments == "" || MucosaProgressTreatments == null || contentSetting.patientTreatment.isSelected == "N") {
        }
        else {
            content.push({ text: 'Treatments in Progress For Mucosa'.toUpperCase(), color: 'black', bold: cBold, fontSize: cFont });
            content.push(MucosaProgressTreatments);
        }
        var getReferral = getReferalInfo(scopeObj.consultinfo);
        if (getReferral || contentSetting.patientReferral.isSelected == "N") {
            content.push(getReferral);
            content.push({ text: ' ' });
        }

        var followupNotes = getFollowup(scopeObj.followupNotes);
        //console.log(followupNotes);
        if (followupNotes == undefined || followupNotes == "" || followupNotes == null || contentSetting.patientFollowup.isSelected == "N") {
        }
        else {
            content.push(followupNotes);
        }
        var followupDate = "";
        if(scopeObj.followupDays !== ""){
            followupDate = $filter('date')(new Date(scopeObj.followupDays), "dd-MMM-yyyy");
        }
        content.push(getDoctorSignature(docSign, "SUMMARY", scopeObj.doctorinfo, followupDate, scopeObj.clinicName, scopeObj.doctorFullName,scopeObj.doctorSpeciality,isDefaultDocSign));
        if (contentSetting.waterMark == undefined || contentSetting.waterMark.isSelected == 'Y') {
            content.push(getFootNote());
        }
        var docdefination = {
                    content: content,
                    header:function(currentPage, pageCount){
                        var headerSetting=getHeader(data,pageSettings.marginRight);
                        headerSetting.margin=[pageSettings.marginLeft , pageSettings.marginTop, pageSettings.marginRight, 20 ]
                        return headerSetting;
                    },
                    footer:function(currentPage, pageCount){
                        var footerSetting=getFooter(data,pageSettings.marginRight);
                        footerSetting.margin=[pageSettings.marginLeft , 5, pageSettings.marginRight, pageSettings.marginBottom ]
                        return footerSetting;
                    },
                    pageMargins: [pageSettings.marginLeft , headerHeight+pageSettings.marginTop, pageSettings.marginRight, footerHeight+pageSettings.marginBottom ],
                    pageSize: printPageSize,
                    pageOrientation: pageSettings.pageOrientation,
                    styles: {
                        header: {
                            fontSize: 18,
                            bold: true,
                            margin: [20, 20, 20, 30]
                        },
                        titleHeader: {
                            fontSize: 18,
                            bold: true,
                            alignment: 'left',
                            margin: [40, 10, 10, 20],
                            paddingBottom: 200
                        },
                        footer: {
                            alignment: 'right',
                            margin: [20, 10, 40, 20],
                        },
                        subheader: {
                            fontSize: 16,
                            bold: true,
                            margin: [0, 10, 0, 5]
                        },
                        tableExample: {
                            margin: [0, 5, 0, 15]
                        },
                        tableHeader: {
                            bold: true,
                            fontSize: 10,
                            color: 'black'
                        },
                        contentText: {
                            fontSize: 10
                        }
                    },
                }
        pdfMake.createPdf(docdefination).open();
        },function(msg){
            alertify.log('Error in getting Summary Print settings');
        });        
    }
    //End of Summary Print  

    /*Get Print Settings*/
    this.getPrintSetting = function (param,callbackSuccess,callbackError) {
        var getPrintUrl = hcueServiceUrl.getPrintSettings;
        $http.post(getPrintUrl,param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    /*End of GetPrint Settings*/

    /*Add Update Print Settings*/
    this.addUpdatePrintSetting = function (param,callbackSuccess,callbackError) {
        var addUpdatePrintUrl = hcueServiceUrl.addUpdatePrintSettings;
        $http.post(addUpdatePrintUrl,param, header)
        .success(function (data) {
           callbackSuccess(data);
        })
        .error(function (data) {
            callbackError(data);
        });
    }
    /*End of Add Update Print Settings*/

    
    function getHeaderWithoutLogo(data,header,headerSetting){
        headerSetting.clinicName.alignment=headerSetting.clinicName.position.toLowerCase();
        headerSetting.clinicName.bold = headerSetting.clinicName.isBold == "Y" ? true : false;
        if (headerSetting.showClinicName != undefined && headerSetting.showClinicName == 'Y') {
            headerSetting.clinicName.text = doctorinfo.doctordata.doctorAddress[0].ClinicName;
        }
        header.stack.push(headerSetting.clinicName);
    }
    function getHeaderWithLogo(data,header,headerSetting){
        var logobase=data.ImageStrings.logoString;
        var logowidth=headerSetting.logo.dimension.width>75?75:headerSetting.logo.dimension.width;
        var logoheight=headerSetting.logo.dimension.height>75?75:headerSetting.logo.dimension.height;
        var marTxtTop = parseInt((logoheight-headerSetting.clinicName.fontSize)/2);
        if (headerSetting.showClinicName != undefined && headerSetting.showClinicName == 'Y') {
            headerSetting.clinicName.text = doctorinfo.doctordata.doctorAddress[0].ClinicName;
        }
        if(headerSetting.logo.position=="Center"){  //Header With Logo Center
            header.stack.push({
                image:"data:image/jpeg;base64,"+logobase,
                alignment:'center',
                margin:[0,5,0,5],
                fit:[logowidth,logoheight]
            })
            headerSetting.clinicName.alignment=headerSetting.clinicName.position.toLowerCase();
            headerSetting.clinicName.bold=headerSetting.clinicName.isBold=="Y"?true:false;
            header.stack.push(headerSetting.clinicName); //Clinic Name in New Line
        }
        else if(headerSetting.logo.position=="Left"){ //Header with Logo Left
            header.stack.push({columns:[{
                stack:[{
                        image:"data:image/jpeg;base64,"+logobase,
                        fit:[logowidth,logoheight]
                    }],
                width:'auto'
            },{
                    width:'*',
                    alignment:headerSetting.clinicName.position.toLowerCase(),
                    bold:headerSetting.clinicName.isBold=="Y"?true:false,
                    stack:[headerSetting.clinicName],
                    margin:[10,marTxtTop,0,0]
            }]})
            
        }
        else{  //Header with Logo Right
            header.stack.push({columns:[
            {
                    width:'*',
                    alignment:headerSetting.clinicName.position.toLowerCase(),
                    bold:headerSetting.clinicName.isBold=="Y"?true:false,
                    stack:[headerSetting.clinicName],
                    margin:[0,marTxtTop,10,0]
            },
            {
                stack:[{
                        image:"data:image/jpeg;base64,"+logobase,
                        fit:[logowidth,logoheight]
                        
                    }],
                width:'auto'
            }]})
        }
    }
    function getTemplateDataContents(data,loc,contentLines){
        var left=[],right=[],center=[];

        if(data.showAddress=="Y"){
            var addr1="",addr2="";
            var docAddr=doctorinfo.doctordata.doctorAddress[0];
            angular.forEach(doctorinfo.doctordata.doctorAddress,function(val,key){
                if(val.ExtDetails!=undefined && val.ExtDetails.HospitalID!=undefined && val.ExtDetails.HospitalID==scopeHospitalID){
                    docAddr=doctorinfo.doctordata.doctorAddress[key];
                }
            })
            if(doctorinfo.doctordata!=undefined && doctorinfo.doctordata.doctorAddress!=undefined)
            {   addr1={text:docAddr.Address1||"",bold:data.address.isBold=="Y",fontSize:data.address.fontSize};
                addr2={text:docAddr.Address2||"",bold:data.address.isBold=="Y",fontSize:data.address.fontSize};
            }
            if(data.address.position=="Left")
                left.push(addr1,addr2);
            else if(data.address.position=="Right")
                right.push(addr1,addr2);
            else
                center.push(addr1,addr2);
        }    
        contentLines.forEach(function(i){
            i.bold=i.isBold=="Y"?true:false;
            if(i.position=="Left")
                left.push(i);
            else if(i.position=="Right")
                right.push(i);
            else
                center.push(i);
        });
        if(center.length>0){
            center.forEach(function(i){
                i.alignment="center";
                i.bold=i.isBold=="Y"?true:false;
                loc.stack.push(i);
            });
        }
        if(left.length>0 || right.length>0){
            var count=(left>=right?left:right);
            var stackL=[],stackR=[];
            for(var i=0;i<count.length;i++){
                stackL.push(left[i]!=undefined?left[i]:{});
                stackR.push(right[i]!=undefined?right[i]:{});
            }
            loc.stack.push({columns:[{stack:stackL,width:'*'},{stack:stackR,width:'auto'}]});
        }
    }   
    function getHeader(data,marginRight){
        if(data==undefined){
            return {text:" ",margin:[0,100,0,0]}
        }
        //var imagebase=getImageAsBase();
        var header={};
        var headerSetting=data.JsonSetting.clinicSettings.headerSettings;
        var headerpagepageOrientation = data.JsonSetting.pageSettings.pageOrientation;
        //headerSetting.templateImg.data=="";
        if(headerSetting.showHeaderImage=="Y" && headerSetting.templateImg.data!=""){    //Header Banner
            var imagebase=data.ImageStrings.headerImgString;
            if(printPageSize=="A4" && headerSetting.dimension.width>540 && headerpagepageOrientation == 'Portrait'){
                headerSetting.dimension.width=540;
            }else if(printPageSize=="A4" && headerSetting.dimension.width>540 && headerpagepageOrientation == 'landscape'){
                headerSetting.dimension.width=820;
            }
            if(printPageSize=="A5" && headerSetting.dimension.width>360 && headerpagepageOrientation == 'Portrait'){
                headerSetting.dimension.width=360;
            } else if (printPageSize == "A5" && headerSetting.dimension.width > 360 && headerpagepageOrientation == 'landscape') {
                headerSetting.dimension.width=580;
            }
            header={
                image:"data:image/jpeg;base64,"+imagebase,
                height:headerSetting.dimension.height,
                width:headerSetting.dimension.width
                //fit:[headerSetting.dimension.width,headerSetting.dimension.height]
            }
        }else{ 
            header.stack=[];
            if(headerSetting.logo.data!=""){ //Header With Logo
                getHeaderWithLogo(data,header,headerSetting);
            }
            else{
                getHeaderWithoutLogo(data,header,headerSetting);
            }
            getTemplateDataContents(headerSetting,header,headerSetting.headerLines); //Header Lines   
			var newLine = (headerpagepageOrientation == 'landscape' && printPageSize == 'A4') ? 825-marginRight : 585-marginRight;			
            headerborder={
            "margin": [0, 0, 0, 10],
            "canvas": [{
                "type": "line",
                "x1": 0,
                "y1": 1,
                "x2": newLine,
                "y2": 1,
                "lineWidth": 0.5,
                "lineColor": "#BDBDBD"
            }]
            };
            header.stack.push(headerborder);
        }
        
        return header;
    }
    function getFooter(data,marginRight){
        if(data==undefined){
            return {text:""};
        }
        var footer={};
        var footerSetting=data.JsonSetting.clinicSettings.footerSettings;
        var headerpagepageOrientation = data.JsonSetting.pageSettings.pageOrientation;
        if(footerSetting.showFooterImage=="Y" && footerSetting.templateImg.data!="" ){    //Footer Banner
            var imagebase=data.ImageStrings.footerImgString;
             if(printPageSize=="A4" && footerSetting.dimension.width>540 && headerpagepageOrientation == 'Portrait'){
                footerSetting.dimension.width=540;
             } else if (printPageSize == "A4" && footerSetting.dimension.width > 540 && headerpagepageOrientation == 'landscape') {
                footerSetting.dimension.width=820;
            }
            if(printPageSize=="A5" && footerSetting.dimension.width>360 && headerpagepageOrientation == 'Portrait'){
                footerSetting.dimension.width=360;
            } else if (printPageSize == "A5" && footerSetting.dimension.width > 360 && headerpagepageOrientation == 'landscape') {
                footerSetting.dimension.width=580;
            }
            footer={
                image:"data:image/jpeg;base64,"+imagebase,
                height: footerSetting.dimension.height,
                width:footerSetting.dimension.width
            }
        }else{ 
            footer.stack=[];
			var newLine = (headerpagepageOrientation == 'landscape' && printPageSize == 'A4') ? 825-marginRight : 585-marginRight;
            footerborder={
            "margin": [0, 0, 0, 10],
            "canvas": [{
                "type": "line",
                "x1": 0,
                "y1": 1,
                "x2": newLine,
                "y2": 1,
                "lineWidth": 0.5,
                "lineColor": "#BDBDBD"
            }]
            };
            footer.stack.push(footerborder);
            getTemplateDataContents(footerSetting,footer,footerSetting.footerLines); //Footer Lines           
        }
        return footer;
    }
    /*End of General Print Functions*/

    this.uploadSettingImages=function(param,callbackSuccess,callbackError){
        var uploadImgUrl = hcueServiceUrl.uploadSettingImages;
        $http.post(uploadImgUrl,param, header)
        .success(function (data) {
           callbackSuccess(data);
        })
        .error(function (data) {
            callbackError(data);
        });
    }


    //summary and pastrecord print in Ophthalmology 

    function GetOphTablePDF(ophthal) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.opthalConditions){
            cBold=contentSetting.opthalConditions.isBold;
            cFont=contentSetting.opthalConditions.fontSize;
        }
        if (ophthal !== "" && ophthal !== undefined && ophthal.length!=0) {
           
            var content = [];
            var bodyItems = [
              [{ text: '',fillColor: '#eaeaea', bold: true  },
               { text: 'RIGHT EYE'.toUpperCase(), fontSize: cFont, colSpan: 4, alignment: 'center', fillColor: '#eaeaea', bold: true }, {}, {}, {},
               { text: 'LEFT EYE'.toUpperCase(), fontSize: cFont, colSpan: 4, alignment: 'center', fillColor: '#eaeaea', bold: true }, {}, {}, {}],
              [{ text: '', fillColor: '#eaeaea', bold: true },
                { text: 'SPH'.toUpperCase(), fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', bold: true },
                { text: 'CYL'.toUpperCase(), fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', bold: true },
                { text: 'AXIS'.toUpperCase(), fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', bold: true },
                { text: 'VIS'.toUpperCase(), fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', bold: true },
                { text: 'SPH'.toUpperCase(), fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', bold: true },
                { text: 'CYL'.toUpperCase(), fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', bold: true },
                { text: 'AXIS'.toUpperCase(), fontSize: cFont, alignment: 'center', fillColor: '#eaeaea', bold: true },
                { text: 'VIS'.toUpperCase(), fontSize: cFont, alignment: 'center', fillColor:'#eaeaea',bold:true}]
            ];
            var distant = []; near = [];
            distant.push({ text: 'DV'.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor:'#eaeaea',bold:true  });
            near.push({ text: 'NV '.toUpperCase(), fontSize: defaultFontSize, alignment: 'center', fillColor:'#eaeaea',bold:true  });
            for (var i = 0; i < 2; i++) {

                for (var j = 0; j < 8; j++) {
                    var index = (i * 8) + j;
                    if (j < 4) {
                        distant.push({ text: ophthal[index], fontSize: cFont,bold:cBold, alignment: 'center' });
                    }
                    else {
                        near.push({ text: ophthal[index], fontSize: cFont,bold:cBold, alignment: 'center' });
                    }
                }

            }
            bodyItems.push(distant);
            bodyItems.push(near);
            var printPageWidth=['*', 50, 50, 50, 50,50, 50, 50, 50];
              if(printPageSize=="A5"){
                printPageWidth=['*', 30, 30, 30, 30,30, 30, 30, 30];
              }
            var ophNotes = {
                style: 'tableExample',
                table: {
                    widths: printPageWidth,
                    body: bodyItems
                },
                layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
                 
            };
            content.push(ophNotes);
            return content;
        }
        return null;
    }
    function getOphthalLensPDF(opthalLens) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.opthalConditions){
            cBold=contentSetting.opthalConditions.isBold;
            cFont=contentSetting.opthalConditions.fontSize;
        }
        if (opthalLens && opthalLens != "") {
            return [{ text: opthalLens,bold:cBold,fontSize:cFont }, { text: " " }];
        }
        else {
            return null;
        }
    }
    function GetTreatmentTable(scopeObj) {
        var cBold=false;
        var cFont=defaultFontSize;
        if(contentSetting.patientTreatment){
            cBold=contentSetting.patientTreatment.isBold;
            cFont=contentSetting.patientTreatment.fontSize;
        }
        if (scopeObj.consultinfo.CaseTreatment.length > 0 && scopeObj.consultinfo.CaseTreatment[0].TreatmentDesctription == '')
            return;
        var bodyItem = [[{ text: 'Treatment Name'.toUpperCase(), fontSize: cFont, alignment: 'center', fillColor: '#ccc', bold: true },
                          { text: 'Treatment Notes'.toUpperCase(), fontSize: cFont, alignment: 'center', fillColor: '#ccc', bold: true }]];
        angular.forEach(scopeObj.consultinfo.CaseTreatment, function (item) {
            var CaseTreatmentItem = [];
            if (item.TreatmentDesctription != '') {
                CaseTreatmentItem.push({ text: (item.TreatmentDesctription != undefined) ? item.TreatmentDesctription.toUpperCase() : "", color: 'black', fontSize: cFont, bold:cBold });
                CaseTreatmentItem.push({ text: (item.TreatmentNotes != undefined) ? item.TreatmentNotes.toUpperCase() : "", color: 'black', fontSize: cFont, bold:cBold });
                bodyItem.push(CaseTreatmentItem);
            }
        });
        if (bodyItem.length == 1)
            return null;
        var patientTreatTestTable = {
            style: 'tableExample',
            table: {
                widths: '*',
                body: bodyItem
            },
            layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 0.5:0.5;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
        };
        return patientTreatTestTable;
    }
    function GetSpecialityById(splCode) {

        var spl = "";

        angular.forEach(doctorinfo.specialitylist, function (item) {
            //display the key and value pair
            if (item.DoctorSpecialityID == splCode) {
                spl = item.DoctorSpecialityDesc;
                return spl;
            }

        });
        return spl;
    }

    function convertcase(str) {
        var lower = str.toLowerCase();
        return lower.replace(/(^| )(\w)/g, function (x) {
            return x.toUpperCase();
        });
    }
}]);
