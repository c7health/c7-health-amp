﻿hCueDoctorWebApp.lazyController('hCueDashboardController', ['$scope','$window','$filter', 'hcueLoginStatusService', 'hcueDoctorLoginService', 'datacookieFactory', '$location','$anchorScroll', 'hCueDashboardReportsServices', '$timeout','alertify','hcueMISReportService','$rootScope','hCueReferralServices',function($scope,$window, $filter, hcueLoginStatusService, hcueDoctorLoginService, datacookieFactory, $location,$anchorScroll, hCueDashboardReportsServices, $timeout,alertify,hcueMISReportService,$rootScope,hCueReferralServices) {

    /* Show header */

    userLoginDetails=datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === "true") {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }

    /* end of show header */
    /* Config */

    $scope.branchfilt = false;
    var doctorinfo=hcueDoctorLoginService.getDoctorInfo();
    var parentHospitalId;
    var hospitalCode;
    $scope.HospitalNewCD = undefined;
    if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined){
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        $scope.HospitalNewCD = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
    }
    var d=new Date();
    d.setMonth(d.getMonth() - 1);
    $scope.dashboard={
        'startDate':$filter('date')(new Date(d), "dd-MMMM-yyyy"),
        'endDate':$filter('date')(new Date(), "dd-MMMM-yyyy"),
        'hospitalList':'',
        'doctorList':''
    }
    $scope.ReferralData = {
        'ReferralList' : [],
        'SubSource' : "",
        'Source' : "",
        'subSourceList' : []
    }
    $scope.detailedPage = {
        'startDate':$filter('date')(new Date(d), "dd-MMMM-yyyy"),
        'endDate':$filter('date')(new Date(), "dd-MMMM-yyyy"),
        'hospitalList':'',
        'doctorList':'',
        'paymentType':'',
        'AppointmentVisit':'N'
    }
    $scope.campaignvalue ={
        'AppointmentVisit':'N'
    }
    $scope.outstanding = {
        'Filter' : 'branch'
    }
    $scope.targetData = {
        'RevenueTarget':'',
        'TargetType':'MONTHLY'
    }
    $scope.showGraph = true;
    $scope.showData = false;
    var initialLoad = true;
    $scope.activeMenu =  {
        "value":"Snapshot"
    }
    $scope.readAdminAccess = false;
    var single_doctorListing = [];

    /* Set the currency */
    if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes !== undefined){
        if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.currencyCode !== undefined){
            $scope.currencyType = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.currencyCode;
        }
        else{
            $scope.currencyType = "INR";
        }
    }
    else {
        $scope.currencyType = "INR";
    }
     /* End of set the currency */
     var allDoctorAllowed = false;
     var allReportaccess = false;
     if (doctorinfo.isAccessRightsApplicable) {
         if (doctorinfo.AccountAccessID.indexOf('APPLSTVIEW') !== -1) {
             allDoctorAllowed = true;
         }
     }
     if (doctorinfo.isAccessRightsApplicable) {
         if (doctorinfo.AccountAccessID.indexOf('ALLRPTACCESS') !== -1) {
             allReportaccess = true;
         }
     }
     $scope.PayoutPagesaccess = false;
     if(hospitalCode == "GOCLINIC")  {
        $scope.PayoutPagesaccess = true;
     }
    /* End of config */

    /* On load access id */
    /* check for access rights */
    $scope.consultantView = false;
    $scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
    if (doctorinfo.isAccessRightsApplicable) {
        if (doctorinfo.AccountAccessID.indexOf('CONSLTANTS') !== -1) {
            $scope.consultantView = true;
        } 
    }
    $scope.doctordiaryView = false;
    if (doctorinfo.isAccessRightsApplicable) {
        if (doctorinfo.AccountAccessID.indexOf('DOTORDAIRY') !== -1) {
            $scope.doctordiaryView = true;
        } 
    }
    $scope.referralView = false;
    if (doctorinfo.isAccessRightsApplicable) {
        if (doctorinfo.AccountAccessID.indexOf('TOPREFERRA') !== -1) {
            $scope.referralView = true;
        } 
    }
    $scope.payoutsView = false;
    if (doctorinfo.isAccessRightsApplicable) {
        if (doctorinfo.AccountAccessID.indexOf('PAYOUT') !== -1) {
            $scope.payoutsView = true;
        } 
    }
    $scope.ratedView = false;
    if (doctorinfo.isAccessRightsApplicable) {
        if (doctorinfo.AccountAccessID.indexOf('TOPRATEDOC') !== -1) {
            $scope.ratedView = true;
        } 
    }
    $scope.enablemisreport = false;
    if (doctorinfo.isAccessRightsApplicable) {
        if (doctorinfo.AccountAccessID.indexOf('MISREPORTS') !== -1) {
            $scope.enablemisreport = true;
        }
    }
    /* End of check access rights */ 

    function branchDoctorListing(pageSize = 100) {
        var get_branches={
            "pageNumber": 1,
            "HospitalID": parseInt(parentHospitalId),
            "pageSize": parseInt(pageSize),
            "IncludeDoctors": "Y",
            "IncludeBranch": "Y",
            "ActiveHospital":"Y",
            "ActiveDoctor":"Y"
        }; 

        hCueDashboardReportsServices.getHospitalDetails(get_branches,onSuccessBranches,onFailedBranches)
    } 
    var totalBranchValue;
    function onSuccessBranches(data) {
        $scope.totalBranchDataLength = data.count;
        totalBranchValue=data.rows;

        var loopStatus=true;
        var innerLoop=true;
        var ownerid=0;
        //Single Doctor -> Created Doctor
        angular.forEach(totalBranchValue,function(e){
            angular.forEach(e.DoctorDetails,function(f){
                if(f.DoctorID==doctorinfo.doctorid){
                    ownerid=f.DoctorOtherDtls.OwnerID;
                }
            })
        })
        if ((innerLoop && ownerid != 0) ||  doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID=="ADMIN" || allReportaccess) {
            $scope.readAdminAccess = true;
            $scope.branchDoctorDetails = totalBranchValue;
            if(totalBranchValue.length > 0 && allDoctorAllowed || totalBranchValue.length > 0 && allReportaccess) {
                var allHospitalList = {"HospitalInfo":{"HospitalDetails":{"HospitalID":parentHospitalId,"HospitalName":"All"}},"DoctorDetails":[{"DoctorID":0,"FullName":"All"}]};
                $scope.branchDoctorDetails.unshift(allHospitalList); 
            }
            $scope.dashboard.hospitalList=$scope.branchDoctorDetails[0];
            $scope.detailedPage.hospitalList = $scope.branchDoctorDetails[0];
            $scope.getDoctor($scope.dashboard.hospitalList);
            innerLoop=false;
        }
        else if(ownerid == 0) {
            angular.forEach(doctorinfo.doctordata.doctorAddress, function(item) {
                if(item.Active == "Y"){
                    single_doctorListing.push(
                        {"HospitalInfo":
                            {"HospitalDetails":{"HospitalID":item.ExtDetails.HospitalID,"HospitalName":item.ClinicName}},"DoctorDetails":[{"DoctorID":doctorinfo.doctorid,"FullName":doctorinfo.doctordata.doctor[0].FullName}]}
                    );
                }
            });
            $scope.branchDoctorDetails = single_doctorListing;
            $scope.dashboard.hospitalList = $scope.branchDoctorDetails[0];
            $scope.doctorList = single_doctorListing[0].DoctorDetails;
            $scope.dashboard.doctorList = single_doctorListing[0].DoctorDetails[0];
            $scope.detailedPage.hospitalList = $scope.branchDoctorDetails[0];
            $scope.getDoctor($scope.dashboard.hospitalList);
        }
    } 

    function onFailedBranches(data) {
        console.log("Branches list error " + JSON.stringify(data));
    }
    branchDoctorListing();

    var dateStatus = false;
    $scope.getDoctor=function(val,viewType = '',dateFilter=false) {
        if(val.HospitalInfo.HospitalDetails.HospitalName == "All"){
            $scope.branchfilt = false;
        }
        else{
            $scope.branchfilt = true;
        }
        if(!dateFilter) {
            $scope.doctorList = [];
            
            angular.forEach($scope.branchDoctorDetails,function(item) {
                if(val.HospitalInfo.HospitalDetails.HospitalName == "All") {
                    angular.forEach(item.DoctorDetails,function(doc) {
                        $scope.doctorList.push(doc);
                    });
                }
                else if((item.HospitalInfo.HospitalDetails.HospitalID == val.HospitalInfo.HospitalDetails.HospitalID) && val.HospitalInfo.HospitalDetails.HospitalName != "All") {
                    angular.forEach(item.DoctorDetails,function(doc) {
                        $scope.doctorList.push(doc);
                    });
                }
            });
            if(allDoctorAllowed && single_doctorListing.length==0 || allReportaccess && single_doctorListing.length==0) {

                var allDoctorList = {'DoctorID':0,"FullName":'All'};
                $scope.doctorList.unshift(allDoctorList);
            }
            $scope.dashboard.doctorList = $scope.doctorList[0];
            $scope.detailedPage.doctorList = $scope.doctorList[0];
        }
        if(viewType != '') {
            $scope.showPage(viewType,true)
        }
        if(viewType == '') {
            serviceCall();
        }
        dateStatus = true;
    }

    $scope.changeDoctorDetails = function(type) {
        $scope.showPage(type,true)
    }

    $scope.hospitalFilter = function(val) {
        if(val.DoctorDetails.length > 0) {
            return val;
        }
    }

    $scope.changeDoctors = function() {
        serviceCall();
    }


    /* Set target */
    $scope.setTarget = function(targetForm) {

        if(!targetForm.$valid) {
            return;
        }

        var params = {
            "USRType": "ADMIN",
            "USRId": doctorinfo.doctorid,
            "HospitalAddress": {}, //Send as Empty as it is mandatory parameter. 
            "HospitalDetails": {
                "HospitalID": $scope.dashboard.hospitalList.HospitalInfo.HospitalDetails.HospitalID
            },
            "Preference" :
            {
                "RevenueTarget" :$scope.targetData.RevenueTarget, 
                "TargetType" : $scope.targetData.TargetType
            }
        }
        hCueDashboardReportsServices.addUpdateBranch(params,onTargetSuccess,onTargetFailed);

        function onTargetSuccess(data) {
            if(data) {
                alertify.success("Target set successfully");
                serviceCall();
                $scope.setTargetPopup = false;
                $scope.targetData.RevenueTarget = '';
            }
        }

        function onTargetFailed(data) {
            console.log("Set target error " + JSON.stringify(data));
        }
    }
    /* End of set target */



    //Get the dashboard data 
    $scope.getData = function(type) {

        var cash_fromDate = $filter('date')(new Date($scope.dashboard.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd");
        var cash_toDate = $filter('date')(new Date($scope.dashboard.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd");
        if(dateStatus) {

            if ((new Date(cash_toDate)).getTime() < (new Date(cash_fromDate)).getTime()) {
                alertify.error('"From" date cannot be greater than "To" date');
                return;
            } 
        }
        var includeBranch = "N";
        if($scope.dashboard.hospitalList.HospitalInfo.HospitalDetails.HospitalName == "All" && $scope.dashboard.doctorList.FullName == 'All') {
            includeBranch = "Y";
        }
        var param = {
            "hospitalId" : (type == "TARGET")?parentHospitalId:$scope.dashboard.hospitalList.HospitalInfo.HospitalDetails.HospitalID,
            "doctorid": $scope.dashboard.doctorList.DoctorID,
            "addressid": 0,
            "startDate":$filter('date')(new Date($scope.dashboard.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
            "endDate": $filter('date')(new Date($scope.dashboard.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
            "includeBranch": includeBranch,
            "dashBoardType" : type,
            "loaderStatus":false,
            "InventoryType":(type == "INVENTORY")?"DRG":undefined,
            "AppointmentVisit": (type == "CAMPAIGN")? $scope.detailedPage.AppointmentVisit: undefined
        }

        hCueDashboardReportsServices.getDashboard(param,onSuccess,onFailed);

        function onSuccess(data) {
            dateStatus = false;
            if(data.DashboardListing.totalAmountObj !== undefined) {
                $scope.totalOutstanding = parseInt(data.DashboardListing.totalAmountObj.totalOutstanding);
                $scope.totalCashReceived = parseInt(data.DashboardListing.totalAmountObj.totalCashReceived);
                $scope.totalExpenses = data.DashboardListing.totalAmountObj.totalExpenses;
                loaderStat('TotalRevenue',true);
                loaderStat('TotalOutstanding',true);
            }
            if(data.DashboardListing.revenueRecievedDetails !== undefined) {
                $scope.getRevenueDetails = data.DashboardListing.revenueRecievedDetails;
                loaderStat('Revenue',true);
            }
            if(data.DashboardListing.patientVisits !== undefined) {
                patientgraph(data.DashboardListing.patientVisits);
                $scope.patientCount = data.DashboardListing.patientVisits.NewPatient + data.DashboardListing.patientVisits.OldPatient;
                loaderStat('PatientCount',true);
            }
            if(data.DashboardListing.doctorAppointmentDetails !== undefined) {
                getAppointmentGraph(data.DashboardListing.doctorAppointmentDetails);
                $scope.totalappointment = data.DashboardListing.doctorAppointmentDetails.totalappointment;
                loaderStat('TotalAppointment',true);
            }
            if(data.DashboardListing.revenueRecievedDetails !== undefined) {
                $scope.tempReceivedVal = data.DashboardListing.revenueRecievedDetails;
                cashReceivedGraph(data.DashboardListing.revenueRecievedDetails);
                loaderStat('Revenue',true);
            }
            if(data.DashboardListing.outstandingDetails !== undefined) {
                $scope.tempOutstandingval = data.DashboardListing.outstandingDetails;
                outStandingGraph(data.DashboardListing.outstandingDetails);
                loaderStat('Outstanding',true);
            }
            if(data.DashboardListing.topTrenDoctors !== undefined) {
                loaderStat('TrendingDoctors',true);
                loaderStat('TrendingTreatments',true);
                loaderStat('Trendingbranches',true);
				$scope.topTrendDoctors = [];
                $scope.topTrendBranches = [];
                $scope.topTrendTreatments = [];
				angular.forEach(data.DashboardListing.topTrenDoctors, function(i){
					if(i.value > 0){
						$scope.topTrendDoctors.push(i);
					}
				});
				angular.forEach(data.DashboardListing.topTrendBranches, function(i){
					if(i.value > 0){
						$scope.topTrendBranches.push(i);
					}
				});
				angular.forEach(data.DashboardListing.topTrendTreatments, function(i){
					if(i.value > 0){
						$scope.topTrendTreatments.push(i);
					}
				});
                
                if($scope.topTrendTreatments.length == 0) {

                    $scope.treatmentView = false;
                    $scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
                    if (doctorinfo.isAccessRightsApplicable) {
                        if (doctorinfo.AccountAccessID.indexOf('TREATMENTS') !== -1) {
                            $scope.treatmentView = true;
                        } 
                    }
                    $scope.treatmentResultView = false;
                }
                else {
					$scope.treatmentView = false;
					$scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
					if (doctorinfo.isAccessRightsApplicable) {
						if (doctorinfo.AccountAccessID.indexOf('TREATMENTS') !== -1) {
							$scope.treatmentView = true;
						} 
					}
					$scope.treatmentResultView = true;
                    generateZenotiTreatmentReports($scope.topTrendTreatments);
                }
                if($scope.topTrendBranches.length == 0) {

                    $scope.branchView = false;
                    $scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
                    if (doctorinfo.isAccessRightsApplicable) {
                        if (doctorinfo.AccountAccessID.indexOf('BRANCHES') !== -1) {
                            $scope.branchView = true;
                        } 
                    }
                    $scope.branchResultView = false;
                }
                else {
					$scope.branchView = false;
                    $scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
                    if (doctorinfo.isAccessRightsApplicable) {
                        if (doctorinfo.AccountAccessID.indexOf('BRANCHES') !== -1) {
                            $scope.branchView = true;
                        } 
                    }
                    $scope.branchResultView = true;
                    generateZenotiBranchReports($scope.topTrendBranches);
                }
                if($scope.topTrendDoctors.length == 0) {
					$scope.AccessDutyDoc = false;
					$scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
					if (doctorinfo.isAccessRightsApplicable) {
						if (doctorinfo.AccountAccessID.indexOf('DUTYDOCTOR') !== -1) {
							$scope.AccessDutyDoc = true;
						} 
					}
                    $scope.dutyView = false;
                    
                }
                else {
					$scope.AccessDutyDoc = false;
					$scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
					if (doctorinfo.isAccessRightsApplicable) {
						if (doctorinfo.AccountAccessID.indexOf('DUTYDOCTOR') !== -1) {
							$scope.AccessDutyDoc = true;
						} 
					}
					$scope.dutyView = true;
                    generateZenotiDoctorReports($scope.topTrendDoctors);
                }
                
            }
            if(data.DashboardListing.feedbackDetails !== undefined) {		
                feedbackGraph(data.DashboardListing.feedbackDetails);		
                loaderStat('Feedback',true);		
            }
            if(data.DashboardListing.totalTreatmentCount !== undefined) {
                loaderStat('TotalTreatment',true);
                $scope.treatmentCountSnap = data.DashboardListing.totalTreatmentCount;
            }
            if(data.MedicineListing !== undefined) {
                loaderStat('Inventory',true);
                inventoryGraph(data.MedicineListing.data);
            }

            if(data.DashboardListing.smsDetails !== undefined) {
                loaderStat('Communication',true);
                getCommunicationGraph(data.DashboardListing.smsDetails);
            }

            if(data.DashboardListing.pharmaLeadsCount !== undefined) {
                loaderStat('PharmaLeads',true);
                getPharmaGraph(data.DashboardListing.pharmaLeadsCount);
            }
            if(data.DashboardListing.revenueTarget !== undefined) {
                loaderStat('RevenueTarget',true);
                $scope.getTargetValue = data.DashboardListing.revenueTarget;
                generateTargetGraph(data.DashboardListing.revenueTarget);
            }
            if(data.DashboardListing.campaignDtls !== undefined) {
                loaderStat('Campaign',true);
				$scope.CampaingnResponseArrObj = angular.copy(data.DashboardListing.campaignDtls.CampaingnResponseArrObj);
				data.DashboardListing.campaignDtls.CampaingnResponseArrObj.length = 0;
				angular.forEach($scope.CampaingnResponseArrObj, function(key){
					if(key.TotalRevenue > 0){
						data.DashboardListing.campaignDtls.CampaingnResponseArrObj.push(key);
					}
				});
                $scope.getCampainReports = data.DashboardListing.campaignDtls.CampaingnResponseArrObj;
				if($scope.getCampainReports.length == 0){
					$scope.campaignStatus = false;
				}else{
					$scope.campaignStatus = true;
                generateZenotiCampaignReports($scope.getCampainReports);
				}
            }
            if(data.DashboardListing.expensesCost !== undefined) {
                loaderStat('Expenses',true);
                generateExpensesReports(data.DashboardListing.expensesCost);
            }
        }

        function onFailed(data) {
            console.log("Error in loading the dashboard data" + JSON.stringify(data));
        }
    }


    /* Patient view graph */

     function patientgraph(data) { 

        /* check for access rights */
        $scope.patientView = true;
        $scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
        if (doctorinfo.isAccessRightsApplicable) {
            if (doctorinfo.AccountAccessID.indexOf('RPTPATIENT') !== -1) {
                $scope.patientView = true;
            } 
        }
        /* End of check access rights */  

                
        AmCharts.addInitHandler(function(chart) {

          // init holder for nested charts
          if (AmCharts.nestedChartHolder === undefined)
            AmCharts.nestedChartHolder = {};

          if (chart.bringToFront === true) {
            chart.addListener("init", function(event) {
              // chart inited
              var chart = event.chart;
              var div = chart.div;
              var parent = div.parentNode;

              // add to holder
              if (AmCharts.nestedChartHolder[parent] === undefined)
                AmCharts.nestedChartHolder[parent] = [];
              AmCharts.nestedChartHolder[parent].push(chart);

              // add mouse mouve event
              chart.div.addEventListener('mousemove', function() {

                // calculate current radius
                var x = Math.abs(chart.mouseX - (chart.realWidth / 2));
                var y = Math.abs(chart.mouseY - (chart.realHeight / 2));
                var r = Math.sqrt(x * x + y * y);

                // check which chart smallest chart still matches this radius
                var smallChart;
                var smallRadius;
                for (var i = 0; i < AmCharts.nestedChartHolder[parent].length; i++) {
                  var checkChart = AmCharts.nestedChartHolder[parent][i];

                  if ((checkChart.radiusReal < r) || (smallRadius < checkChart.radiusReal)) {
                    checkChart.div.style.zIndex = 1;
                  } else {
                    if (smallChart !== undefined)
                      smallChart.div.style.zIndex = 1;
                    checkChart.div.style.zIndex = 2;
                    smallChart = checkChart;
                    smallRadius = checkChart.radiusReal;
                  }

                }
              }, false);
            });
          }

        }, ["pie"]);
        $scope.patientVisitStatus = true;
        $scope.patientGenderStatus = true;
        $scope.patientAgeStatus = true;
        var patientTypeData = [{
            "key": "New Patient",
            "val": data.NewPatient,
            "color": "#32DFCC"
        }, {
            "key": "Old Patients",
            "val": data.OldPatient,
            "color": "#D5D1EA"
        }]; 
        if(data.NewPatient == 0 && data.OldPatient == 0) {
            $scope.patientVisitStatus = false;
        }
        var chart = AmCharts.makeChart( "piePatientType", {
            "type": "pie",
            "theme": "light",
            "bringToFront": true,
            "dataProvider": patientTypeData,
            "responsive": {
                "enabled": false
            },
            "startDuration": 0,
            "pullOutRadius": 0,
            "color": "#000",
            "fontSize": 14,
            "valueField": "val",
            "titleField": "key",
            "colorField": "color",
            "labelRadius": -25,
            "labelColor": "#000",
            "radius": 95,
            "innerRadius": 70,
            "labelText": "[[key]]",
            "balloonText": "[[key]]: [[val]]",
            "outlineAlpha": 1,
            "outlineThickness": 0,
            "labelsEnabled":false
        });
       // chart.animateAgain();



        var patientGenderData = [{
            "key": "Male",
            "val": data.Male,
            "color": "#D5D1EA"
        }, {
            "key": "Female",
            "val": data.Female,
            "color": "#64CEFF"
        }];

        if(data.Male == 0 && data.Female == 0) {
            $scope.patientGenderStatus = false;
        }

        var chart = AmCharts.makeChart( "piePatientGender", {
            "type": "pie",
            "theme": "light",
            "bringToFront": true,
            "dataProvider":patientGenderData,
            "responsive": {
                "enabled": false
            },
            "startDuration": 0,
            "pullOutRadius": 0,
            "color": "#000",
            "fontSize": 14,
            "valueField": "val",
            "titleField": "key",
            "colorField": "color",
            "labelRadius": -25,
            "labelColor": "#000",
            "radius": 120,
            "innerRadius": 100,
            "labelText": "[[key]]",
            "balloonText": "[[key]]: [[val]]",
            "outlineAlpha": 0,
            "outlineThickness": 0,
            "labelsEnabled":false
        });

        var patientAgeData = [{
            "key": "Age 0-10",
            "val": data.Age.Age_0_10,
            "color": "#FF6600"
        },{
            "key": "Age 10-25",
            "val": data.Age.Age_10_25,
            "color": "#64CEFF"
        },{
            "key": "Age 25-50",
            "val": data.Age.Age_25_50,
            "color": "#FED3E2"
        },{
            "key": "Age 50-75",
            "val": data.Age.Age_50_75,
            "color": "#D5D1EA"
        },{
            "key": "Age 75-100",
            "val": data.Age.Age_75_100,
            "color": "#D9EFD2"
        }];

        if(data.Age.Age_0_10 == 0 && data.Age.Age_10_25 == 0 && data.Age.Age_25_50 == 0 && data.Age.Age_50_75 == 0 && data.Age.Age_75_100 == 0) {
            $scope.patientAgeStatus = false;
        }
        var chart = AmCharts.makeChart( "piePatientAge", {
            "type": "pie",
            "theme": "light",
            "bringToFront": true,
            "dataProvider":patientAgeData,
            "responsive": {
                "enabled": true
            },
            "export": {
                "enabled": false
            },
            "startDuration": 0,
            "pullOutRadius": 0,
            "color": "#000",
            "fontSize": 14,
            "valueField": "val",
            "titleField": "key",
            "colorField": "color",
            "labelRadius": -25,
            "labelColor": "#000",
            "radius": 150,
            "innerRadius": 125,
            "labelText": "[[key]]",
            "balloonText": "[[key]]: [[val]]",
            "outlineAlpha": 1,
            "outlineThickness": 0,
            "labelsEnabled":false
        });



    }


    function getAppointmentGraph(data) {

        /* check for access rights */
        $scope.appointmentView = true;
        $scope.appointmentStatusview = true;
        $scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
        if (doctorinfo.isAccessRightsApplicable) {
            if (doctorinfo.AccountAccessID.indexOf('APPSTATS') !== -1) {
                $scope.appointmentView = true;
            } 
        }
        /* End of check access rights */  
        var appointmentDetails = [{
            'key':"Booked",
            'val':data.booked,
            "color": "#32DFCC"
        },{
            'key':"Completed",
            'val':data.completed,
            "color": "#64CEFF"
        },{
            'key':"Cancelled",
            'val':data.cancelled,
            "color": "#FED3E2"
        }]
        if(data.booked == 0 && data.completed == 0 && data.cancelled == 0) {
            $scope.appointmentStatusview = false;
        }
        var chart = AmCharts.makeChart( "pieAppointmentDts", {
            "type": "pie",
            "theme": "light",
            "dataProvider":appointmentDetails,
            "valueField": "val",
            "titleField": "key",
            "responsive": {
                "enabled": true
            },
            "export": {
                "enabled": true
            },
            "startEffect": "elastic",
            "colorField": "color",
            "startDuration": 2,
            "pullOutRadius": 5,
            "labelRadius": 15,
            "depth3D": 0,
            "innerRadius": "50%",
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 0,
            "balloon":{
                "fixedPosition":true
            },
            "labelsEnabled":false
        });
    }

    $scope.changeOutFilter = function(val) {
        outStandingGraph($scope.tempOutstandingval,val)
    }

    $scope.changeCashFilter = function(val) {
        cashReceivedGraph($scope.tempReceivedVal,val)
    }

    function outStandingGraph(data,val="treatment") {

        /* check for access rights */
        $scope.outstandingView = false;
        $scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
        if (doctorinfo.isAccessRightsApplicable) {
            if (doctorinfo.AccountAccessID.indexOf('OTSTANDING') !== -1) {
                $scope.outstandingView = true;
            } 
        }
        /* End of check access rights */  

        $scope.outstandingStatus = true;
        var outstandingCollection;
        var totalOutValue = [];
        var array_list = ['#32DFCC', '#64CEFF', '#FED3E2' ,'#D5D1EA','#D9EFD2','#FFBE99','#FFF89F','#B5FFE2','#FFD991','#BBF5BC' ];
        /* Branch wise reports */
        $scope.activeTabValueOut = val;
        if(val == "branch") {
            angular.forEach(data.branchWise,function(item) {
                item.color = array_list[Math.floor(Math.random() * array_list.length)];
            })
            outstandingCollection = data.branchWise;
        }
        /* Doctor wise reports */
        else if(val == "doctor") {
            angular.forEach(data.doctorWise,function(item) {
                item.color = array_list[Math.floor(Math.random() * array_list.length)];
            })
            outstandingCollection = data.doctorWise;
        }
        /* Treatment wise reports */
        else {
            var count_array = 0;
            angular.forEach(data.treatmentWise,function(item) {
                item.color = array_list[count_array];
                count_array++;
            })
            outstandingCollection = data.treatmentWise
        }

        if(outstandingCollection.length == 0) {
            $scope.outstandingStatus = false;
        }
        else {
            angular.forEach(outstandingCollection,function(dataItem) {
                totalOutValue.push(dataItem);
            });
            var getOutLength = outstandingCollection.length;
            var getRemainingVal = 10-getOutLength;
            for(i=0;i<getRemainingVal;i++) {
                totalOutValue.push({
                    "key":"",
                    "value":0
                })
            }
            outstandingCollection = totalOutValue;
        }
        var chart = AmCharts.makeChart("baroutStanding", {
            "theme": "light",
            "type": "serial",
            "dataProvider": outstandingCollection,
            "valueAxes": [{
                "stackType": "3d",
                "unit": "",
                "position": "left",
                "title": "Amount ("+$scope.currencyType+")",
                "gridAlpha": 0
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[key]]: <b>[[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "key",
                "type": "column",
                "valueField": "value",
                "fixedColumnWidth": 60  
            }],
            "export": {
                "enabled": true
            },
            "plotAreaFillAlphas": 0.1,
            "depth3D": 0,
            "angle": 10,
            "categoryField": "key",
            "categoryAxis": {
                "gridPosition": "start",
                "labelRotation": 45,
                "gridThickness": 0,
                "labelFunction": function(valueText, serialDataItem, categoryAxis) {
                                      if (valueText.length > 15)
                                        return valueText.substring(0, 15) + '...';
                                      else
                                        return valueText;
                                    }
            },
            "responsive": {
                "enabled": true
            },
            "autoGridCount": false,
        });
        jQuery('.chart-input').off().on('input change',function() {
            var property    = jQuery(this).data('property');
            var target      = chart;
            chart.startDuration = 0;
            if ( property == 'topRadius') {
                target = chart.graphs[0];
                if ( this.value == 0 ) {
                  this.value = undefined;
                }
            }
            target[property] = this.value;
            chart.validateNow();
        });
    }

    // Cash received

    function cashReceivedGraph(data,val = "treatment") {
        /* check for access rights */
        $scope.cashreceivedView = false;
        $scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
        if (doctorinfo.isAccessRightsApplicable) {
            if (doctorinfo.AccountAccessID.indexOf('CASHRECIVD') !== -1) {
                $scope.cashreceivedView = true;
            } 
        }
        /* End of check access rights */  
        $scope.cashReceivedStatus = true;
        var cashReceivedCollection;
        var cashTotalValue = [];
        var array_list = ['#32DFCC', '#64CEFF', '#FED3E2' ,'#D5D1EA','#D9EFD2','#FFBE99','#FFF89F','#B5FFE2','#FFD991','#BBF5BC' ];
        /* Branch wise reports */
        $scope.activeTabValue = val;
        if(val == "branch") {
            angular.forEach(data.branchWise,function(item) {
                item.color = array_list[Math.floor(Math.random() * array_list.length)];
            })
            cashReceivedCollection = data.branchWise;
        }
        /* Doctor wise reports */
        else if(val == "doctor") {
            angular.forEach(data.doctorWise,function(item) {
                item.color = array_list[Math.floor(Math.random() * array_list.length)];
            })
            cashReceivedCollection = data.doctorWise;
        }
        /* Treatment wise reports */
        else {
            var array_count = 0;
            angular.forEach(data.treatmentWise,function(item) {
                item.color = array_list[array_count];
                array_count++;
            })
            cashReceivedCollection = data.treatmentWise
        }

        if(cashReceivedCollection.length == 0) {
            $scope.cashReceivedStatus = false;
        }
        else {
            angular.forEach(cashReceivedCollection,function(dataItem) {
                cashTotalValue.push(dataItem);
            });
            var getOutLength = cashReceivedCollection.length;
            var getRemainingVal = 10-getOutLength;
            for(i=0;i<getRemainingVal;i++) {
                cashTotalValue.push({
                    "key":"",
                    "value":0
                })
            }
            cashReceivedCollection = cashTotalValue;
        }

        var chart = AmCharts.makeChart("cashReceivedData", {
            "theme": "light",
            "type": "serial",
            "dataProvider": cashReceivedCollection,
            "valueAxes": [{
                "stackType": "3d",
                "unit": "",
                "position": "left",
                "title": "Amount ("+$scope.currencyType+")",
                "gridAlpha": 0
            }],
            "export": {
                "enabled": true
            },
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[key]]: <b>[[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "key",
                "type": "column",
                "valueField": "value"   
            }],
            "plotAreaFillAlphas": 0.1,
            "depth3D": 0,
            "angle": 10,
            "categoryField": "key",
            "categoryAxis": {
                "gridPosition": "start",
                "labelRotation": 45,
                 "gridThickness": 0,
                "labelFunction": function(valueText, serialDataItem, categoryAxis) {
                                      if (valueText.length > 15)
                                        return valueText.substring(0, 15) + '...';
                                      else
                                        return valueText;
                                    }
            },
            "responsive": {
                "enabled": true
            }
        });
        jQuery('.chart-input').off().on('input change',function() {
            var property    = jQuery(this).data('property');
            var target      = chart;
            chart.startDuration = 0;
            if ( property == 'topRadius') {
                target = chart.graphs[0];
                if ( this.value == 0 ) {
                  this.value = undefined;
                }
            }
            target[property] = this.value;
            chart.validateNow();
        });
    }

    function inventoryGraph(data) {
        /* check for access rights */
        $scope.inventoryView = false;
        $scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
        if (doctorinfo.isAccessRightsApplicable) {
            if (doctorinfo.AccountAccessID.indexOf('CASHRECIVD') !== -1) {
                $scope.inventoryView = true;
            } 
        }
        /* End of check access rights */  
        $scope.inventoryStatus = true;
        var array_list = ['#32DFCC', '#64CEFF', '#FED3E2' ,'#D5D1EA','#D9EFD2','#FFBE99','#FFF89F','#B5FFE2','#FFD991','#BBF5BC' ];
        var drug_collection = [];
        var durgTotalValue = [];
        var array_count = 0;
        angular.forEach(data,function(item) {
            drug_collection.push({
                'key':item.Medicine,
                'value':item.AvailableStock,
                'color':array_list[array_count]
            })
            array_count++;
        });

        if(drug_collection.length == 0) {
            $scope.inventoryStatus = false;
        }
        else {
            angular.forEach(drug_collection,function(dataItem) {
                durgTotalValue.push(dataItem);
            });
            var getOutLength = drug_collection.length;
            var getRemainingVal = 10-getOutLength;
            for(i=0;i<getRemainingVal;i++) {
                durgTotalValue.push({
                    "key":"",
                    "value":0
                })
            }
            drug_collection = durgTotalValue;
        }

        //Generate chart
        var chart = AmCharts.makeChart("inventoryData", {
            "theme": "light",
            "type": "serial",
            "dataProvider": drug_collection,
            "valueAxes": [{
                "stackType": "3d",
                "unit": "",
                "position": "left",
                "title": "Available Stock",
                "gridAlpha": 0,
            }],
            "export": {
                "enabled": true
            },
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[key]]: <b>[[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "key",
                "type": "column",
                "valueField": "value"
            }],
            "plotAreaFillAlphas": 0.1,
            "depth3D": 0,
            "angle": 10,
            "categoryField": "key",
            "categoryAxis": {
                "gridPosition": "start",
                "gridThickness": 0,
                "labelRotation": 45,
                "labelFunction": function(valueText, serialDataItem, categoryAxis) {
                                      if (valueText.length > 15)
                                        return valueText.substring(0, 15) + '...';
                                      else
                                        return valueText;
                                    }
            },
            "responsive": {
                "enabled": true
            }
        });
        jQuery('.chart-input').off().on('input change',function() {
            var property    = jQuery(this).data('property');
            var target      = chart;
            chart.startDuration = 0;
            if ( property == 'topRadius') {
                target = chart.graphs[0];
                if ( this.value == 0 ) {
                  this.value = undefined;
                }
            }
            target[property] = this.value;
            chart.validateNow();
        });
    }

    function getCommunicationGraph(data) {
        /* check for access rights */
        $scope.communicationView = true;
        $scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
        if (doctorinfo.isAccessRightsApplicable) {
            if (doctorinfo.AccountAccessID.indexOf('SMSREPT') !== -1) {
                $scope.communicationView = true;
            } 
        }
        /* End of check access rights */  
        $scope.communicationStatus = true;
        var communicationDetails = [{
            'key':"Total Delivered",
            'val':data.Total_Delivered,
            "color":'#32DFCC'
        },{
            'key':"Total Failed",
            'val':data.Total_Failed,
            "color":'#64CEFF'
        },{
            'key':"Total Pending",
            'val':data.Total_Pending,
            "color":'#FED3E2'
        },{
            'key':"Total Sent",
            'val':data.Total_Submitted,
            "color":'#D5D1EA'
        }]

        if(data.Total_Delivered == 0 && data.Total_Failed == 0 && data.Total_Pending == 0 && data.Total_Submitted == 0) {
            $scope.communicationStatus = false;
        }

        var chart = AmCharts.makeChart( "pieCommunicationDetails", {
            "type": "pie",
            "theme": "light",
            "dataProvider":communicationDetails,
            "valueField": "val",
            "titleField": "key",
            "startEffect": "elastic",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "depth3D": 0,
            "pullOutRadius": 5,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
            "angle": 0,
            "balloon":{
                "fixedPosition":true
            },
            "export": {
                "enabled": true
            },
            "responsive": {
                "enabled": true
            },
            "colorField": "color",
            "labelsEnabled":false
        });
    }

    function getPharmaGraph(data) {
        /* check for access rights */
        $scope.pharmaView = false;
        $scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
        if (doctorinfo.isAccessRightsApplicable) {
            if (doctorinfo.AccountAccessID.indexOf('SMSREPT') !== -1) {
                $scope.pharmaView = true;
            } 
        }
        /* End of check access rights */  

        $scope.pharmaleadStatus = true;
        var array_list = ['#32DFCC', '#64CEFF', '#FED3E2' ,'#D5D1EA','#D9EFD2','#FFBE99','#FFF89F','#B5FFE2','#FFD991','#BBF5BC' ];
        var array_count = 0;
        angular.forEach(data,function(item) {
            item.color = array_list[array_count];
            array_count++;
        });

        if(data.length == 0) {
            $scope.pharmaleadStatus = false;
        }
        var chart = AmCharts.makeChart( "piePharmaDetails", {
            "type": "pie",
            "theme": "light",
            "dataProvider":data,
            "valueField": "value",
            "titleField": "key",
            "responsive": {
                "enabled": true
            },
            "startEffect": "elastic",
            "startDuration": 2,
            "pullOutRadius": 5,
            "labelRadius": 15,
            "depth3D": 0,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 0,
            "balloon":{
                "fixedPosition":true
            },
            "export": {
                "enabled": true
            },
            "colorField": "color",
            "labelsEnabled":false,
            "outlineAlpha": 1,
            "outlineThickness": 1
        });
    }
		
		function feedbackGraph(data) {
        /* check for access rights */
        $scope.feedbackView = false;
        $scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
        if (doctorinfo.isAccessRightsApplicable) {
            if (doctorinfo.AccountAccessID.indexOf('CASHRECIVD') !== -1) {
                $scope.feedbackView = true;
            } 
        }
        /* End of check access rights */  
        $scope.feedbackStatus = true;
        var array_list = ['#32DFCC', '#64CEFF', '#FED3E2' ,'#D5D1EA','#D9EFD2','#FFBE99','#FFF89F','#B5FFE2','#FFD991','#BBF5BC' ];
        var feedback_collection = [];
        var feedbackTotalValue = [];
        var array_count = 0;
        angular.forEach(data,function(item) {
            feedback_collection.push({
                'key':item.key,
                'value':item.value,
                'color':array_list[array_count]
            })
            array_count++;
        });

        if(feedback_collection.length == 0) {
            $scope.feedbackStatus = false;
        }
        else {
            angular.forEach(feedback_collection,function(dataItem) {
                feedbackTotalValue.push(dataItem);
            });
            var getOutLength = feedback_collection.length;
            var getRemainingVal = 10-getOutLength;
            for(i=0;i<getRemainingVal;i++) {
                feedbackTotalValue.push({
                    "key":"",
                    "value":0
                })
            }
            feedback_collection = feedbackTotalValue;
        }

        //Generate chart
       var chart = AmCharts.makeChart("feedbackData", {
            "theme": "light",
            "type": "serial",
            "dataProvider": feedback_collection,
            "valueAxes": [{
                "stackType": "3d",
                "unit": "",
                "position": "left",
                "title": "Ratings",
                "gridAlpha": 0,
				"autoGridCount" : false,
                "gridCount" : 5,
				"labelFrequency" : 1,
            }],
            "export": {
                "enabled": true
            },
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[key]]: <b>[[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "key",
                "type": "column",
                "valueField": "value"
            }],
            "plotAreaFillAlphas": 0.1,
            "depth3D": 0,
            "angle": 10,
            "categoryField": "key",
            "categoryAxis": {
                "gridPosition": "start",
                "gridThickness": 0,
                "labelRotation": 45,
                "labelFunction": function(valueText, serialDataItem, categoryAxis) {
                                      if (valueText.length > 15)
                                        return valueText.substring(0, 15) + '...';
                                      else
                                        return valueText;
                                    }
            },
            "responsive": {
                "enabled": true
            }
        });
        
        jQuery('.chart-input').off().on('input change',function() {
            var property    = jQuery(this).data('property');
            var target      = chart;
            chart.startDuration = 0;
            if ( property == 'topRadius') {
                target = chart.graphs[0];
                if ( this.value == 0 ) {
                  this.value = undefined;
                }
            }
            target[property] = this.value;
            chart.validateNow();
        });
    }

    function generateTargetGraph(data) {
        var targetData = [{
            "key":"Remaining",
            "value":100 - data.targetAchievedPercent,
            "color": "#CCC"
        },{
            "key":"Target",
            "value":data.targetAchievedPercent,
            "color": "#32DFCC"
        }]

        var startAngle = 180 - ((data.targetAchievedPercent / 2) * (360 / 100));
        var chart = AmCharts.makeChart( "pieTargetDetails", {
            "type": "pie",
            "dataProvider": targetData,
            "titleField": "key",
            "valueField": "value",
            "labelRadius": 0,
            "radius": "35%",
            "innerRadius": "60%",
            "export": {
                "enabled": true
            },
            "colorField": "color",
            "Alpha": 1,
            "borderColor" : "#000",
            "responsive": {
                "enabled": true
            },
            "fontSize":10,
            "startAngle": startAngle,
            "endAngle": 180,
            "labelsEnabled":false,
            "addLabel":"center",
            "balloonText": "[[key]]<br><span style='font-size:14px'><b>[[percents]]%</b></span>",
            
        });
    }

    function generateExpensesReports(dataval) {
        /* check for access rights */
        var expenses_collection = [];
        $scope.expensesView = false;
        $scope.isAccessRightsApplicable = doctorinfo.isAccessRightsApplicable;
        if (doctorinfo.isAccessRightsApplicable) {
            if (doctorinfo.AccountAccessID.indexOf('TOTALEXPEN') !== -1) {
                $scope.expensesView = true;
            } 
        }
        /* End of check access rights */  
        $scope.expensesStatus = true;
        var array_list = ['#32DFCC', '#64CEFF', '#FED3E2' ,'#D5D1EA','#D9EFD2','#FFBE99','#FFF89F','#B5FFE2','#FFD991','#BBF5BC' ];
        var array_count = 0;
        var expensesTotalValue = [];

        angular.forEach(dataval,function(item) {
            expenses_collection.push({
                'key':item.key,
                'value':item.value,
                'color':array_list[array_count]
            })
            array_count++;
        });
        
        if(dataval.length == 0) {
            $scope.expensesStatus = false;
        }
        else {

            angular.forEach(dataval,function(dataItem) {
                expensesTotalValue.push(dataItem);
            });
            var getOutLength = dataval.length;
            var getRemainingVal = 10-getOutLength;
            for(i=0;i<getRemainingVal;i++) {
                expensesTotalValue.push({
                    "key":"",
                    "value":0
                })
            }
            expenses_collection = expensesTotalValue;
        }
        //Generate chart
        var chart = AmCharts.makeChart("expensesData", {
            "theme": "light",
            "type": "serial",
            "dataProvider": expenses_collection,
            "valueAxes": [{
                "stackType": "3d",
                "unit": "",
                "position": "left",
                "title": "Amount ("+$scope.currencyType+")",
                "gridAlpha": 0,
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[key]]: <b>[[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "key",
                "type": "column",
                "valueField": "value",
            }],
            "plotAreaFillAlphas": 0.1,
            "depth3D": 0,
            "angle": 10,
            "categoryField": "key",
            "categoryAxis": {
                "gridPosition": "start",
                "gridThickness": 0,
                "labelRotation": 45,
                "labelFunction": function(valueText, serialDataItem, categoryAxis) {
                                      if (valueText.length > 15)
                                        return valueText.substring(0, 15) + '...';
                                      else
                                        return valueText;
                                    }
            },
            "export": {
                "enabled": true
            },
            "export": {
                "enabled": true
            },
            "responsive": {
                "enabled": true
            }
        });
        jQuery('.chart-input').off().on('input change',function() {
            var property    = jQuery(this).data('property');
            var target      = chart;
            chart.startDuration = 0;
            if ( property == 'topRadius') {
                target = chart.graphs[0];
                if ( this.value == 0 ) {
                  this.value = undefined;
                }
            }
            target[property] = this.value;
            chart.validateNow();
        });
    }


    var loaderStat=function(screen,bool){
        switch(screen){
            case "TotalRevenue":$scope.showTotalRevenue=bool;break;
            case "TotalOutstanding":$scope.showTotalOutstanding=bool;break;
            case "Revenue":$scope.showRevenue=bool;break;
            case "PatientCount":$scope.showPatientCount=bool;break;
            case "TotalAppointment":$scope.showTotalAppointment=bool;break;
            case "Outstanding":$scope.showOutstanding=bool;break;
            case "TrendingDoctors":$scope.showTrendingDoctors=bool;break;
            case "TotalTreatment":$scope.showTotalTreatment=bool;break;
            case "Inventory":$scope.showInventory=bool;break;
            case "Communication":$scope.showCommunication=bool;break;
            case "PharmaLeads":$scope.showPharmaLeads=bool;break;
			case "Feedback":$scope.showFeedback =bool;break;
            case "RevenueTarget":$scope.showRevenueTarget=bool;break;
            case "Campaign":$scope.showCampaign=bool;break;
            case "Expenses":$scope.showExpenses=bool;break;
            case "TrendingTreatments":$scope.showTrendingTreatments=bool;break;
            case "Trendingbranches":$scope.showTrendingbranches=bool;break;

        }
    }

     /* Am charts */
     /* End of am charts */
    var serviceCall = function() {
        $scope.getData("PATIENTDTLS");
        $scope.getData("TREATMENTCNT");
        $scope.getData("TRENDING");
        $scope.getData("REVENUERECIEVED");
        $scope.getData("APPOINTMENTDTLS");
        $scope.getData("OUTSTANDING");
        $scope.getData("INVENTORY");
        $scope.getData("PHARMALEADS");
		$scope.getData("FEEDBACK");
        $scope.getData("TARGET");
        $scope.getData("CAMPAIGN");
        $scope.getData("EXPENSES");
        loaderStat('TotalRevenue',false);
        loaderStat('TotalOutstanding',false);
        loaderStat('Revenue',false);
        loaderStat('PatientCount',false);
        loaderStat('TotalAppointment',false);
        loaderStat('Outstanding',false);
        loaderStat('TrendingDoctors',false);
        loaderStat('TotalTreatment',false);
        loaderStat('Inventory',false);
        loaderStat('Communication',false);
        loaderStat('PharmaLeads',false);
        loaderStat('RevenueTarget',false);
        loaderStat('Campaign',false);
        loaderStat('Expenses',false);
        loaderStat('TrendingTreatments',false);
        loaderStat('Trendingbranches',false);
		loaderStat('Feedback',false);
        initialLoad = false;
    }
    if(single_doctorListing.length > 0) {
        serviceCall();
    }

    $scope.ShowRprt =function(value){
        var hospitaldtl = $scope.detailedPage.hospitalList.HospitalInfo.HospitalDetails.HospitalID;
        var param; 
            
        if($scope.branchfilt){
            param = {
                "pageNumber":1,
                "hospitalId":hospitaldtl,
                "hospitalcd":hospitalCode,
                "startDate":$filter('date')(new Date($scope.detailedPage.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                "endDate":$filter('date')(new Date($scope.detailedPage.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                "pageSize":1000
            }
        }
        else{
            param = {
                "pageNumber":1,
                "hospitalId":0,
                "hospitalcd":hospitalCode,
                "startDate":$filter('date')(new Date($scope.detailedPage.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                "endDate":$filter('date')(new Date($scope.detailedPage.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                "pageSize":1000
            }
        }
       
        if(value == 'cashrpt'){
            hCueDashboardReportsServices.getTotalCashByPaymentMode(param, onDetailexpenserptSuccess, onDetailsRprtFailed);
        }else{
            hCueDashboardReportsServices.getExpenseReport(param, onDetailsRprtSuccess, onDetailsRprtFailed);
        }
        function onDetailsRprtSuccess(data){

            generateTable(data,'ExpenseRprt');

        }

        function onDetailexpenserptSuccess(data){
            generateTable(data,'cashrpt');
        }

        function onDetailsRprtFailed(){

        }
    }


    $scope.showPage = function(value,detailType=false,excelExport="",pageSize = 10) {
        //Assign the dropdown value
        if(value == "graphView") {
            $scope.showGraph = true;
            $scope.showData = false;
            $scope.activeMenu = {
                "value":"Snapshot"
            };
            $scope.detailedPage.AppointmentVisit = 'N';
            $scope.AppointmentVisit('REG',$scope.detailedPage.AppointmentVisit)
            
        }
        else {
                
            var cash_fromDate = $filter('date')(new Date($scope.detailedPage.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd");
            var cash_toDate = $filter('date')(new Date($scope.detailedPage.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd");
            
            if ((new Date(cash_toDate)).getTime() < (new Date(cash_fromDate)).getTime()) {
                alertify.error('"From" date cannot be greater than "To" date');
                return;
            } 
            var hospitalIdVal;
            var doctorIdVal;

            if(detailType) {
                hospitalIdVal = $scope.detailedPage.hospitalList.HospitalInfo.HospitalDetails.HospitalID;
                doctorIdVal = $scope.detailedPage.doctorList.DoctorID;
            }
            else {
                hospitalIdVal = $scope.dashboard.hospitalList.HospitalInfo.HospitalDetails.HospitalID;
                doctorIdVal = $scope.dashboard.doctorList.DoctorID;
                $scope.detailedPage.hospitalList = $scope.dashboard.hospitalList;
                $scope.detailedPage.doctorList = $scope.dashboard.doctorList;
                $scope.detailedPage.startDate = $scope.dashboard.startDate;
                $scope.detailedPage.endDate = $scope.dashboard.endDate;
            }
            var includeBranch = "N";
            if($scope.detailedPage.hospitalList.HospitalInfo.HospitalDetails.HospitalName == "All" && $scope.detailedPage.doctorList.FullName == 'All') {
                includeBranch = "Y";
            }
            $scope.viewtype = value;
            $scope.showGraph = false;
            $scope.showData = true;
            $scope.activeMenu = value;
            var params;
            if(value == "expenses") {
                params = {
                    //"hospitalId": hospitalIdVal,
					"HospitalCD":(includeBranch == "Y")?hospitalCode:undefined,
					"hospitalId": (includeBranch == "N")?hospitalIdVal:undefined,
                    //"ParentHospitalID":parentHospitalId,
                    "ValueStartDate":$filter('date')(new Date($scope.detailedPage.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                    "ValueEndDate": $filter('date')(new Date($scope.detailedPage.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                    "PageNumber": 1,
                    "PageSize": pageSize,
                    "SyncStatus":["O","F","S"],
                    "TxnType":"D"
                }
            }
            else if(value == "campaign") {
                if(!detailType){
                    getSourceListing(doctorIdVal)
                }
                params = {
                    "pageNumber": 0,
                    "hospitalId": hospitalIdVal,
                    "doctorId": doctorIdVal,
                    "startDate":$filter('date')(new Date($scope.detailedPage.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                    "endDate": $filter('date')(new Date($scope.detailedPage.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                    "includeBranch": includeBranch,
                    "addressID": 0,
                    "count": 0,
                    "pageSize": pageSize,
                    "ReferralID": ($scope.ReferralData.Source == '')? 0: parseInt($scope.ReferralData.Source),
                    "SubReferralID": ($scope.ReferralData.SubSource == '')? 0: parseInt($scope.ReferralData.SubSource),
                    'AppointmentVisit': $scope.detailedPage.AppointmentVisit

                }
            }
        else if(value == "SmsRprt") {
                params = {
                    "pageNumber": 0,
                    "hospitalId": hospitalIdVal,
                    "doctorId": 0,
                    "startDate":$filter('date')(new Date($scope.detailedPage.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                    "endDate": $filter('date')(new Date($scope.detailedPage.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                    "includeBranch": includeBranch,
                    "addressID": 0,
                    "count": 0,
                    "pageSize": pageSize,
                    "PaymentType":(value == "cash")?$scope.detailedPage.paymentType:undefined
                }
        }else{
            params = {
                "pageNumber": 0,
                "hospitalId": hospitalIdVal,
                "doctorId": doctorIdVal,
                "startDate":$filter('date')(new Date($scope.detailedPage.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                "endDate": $filter('date')(new Date($scope.detailedPage.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                "includeBranch": includeBranch,
                "addressID": 0,
                "count": 0,
                "pageSize": pageSize,
                "PaymentType":(value == "cash")?$scope.detailedPage.paymentType:undefined
            }
        }



            if(excelExport != "") {
                params.count = pageSize;
            }



            if(value == "patient") {
                hCueDashboardReportsServices.patientInfoList(params, onDetailsSuccess, onDetailsFailed);
            }
            else if(value  == "appointment") {
                hCueDashboardReportsServices.getAppointmentReports(params, onDetailsSuccess, onDetailsFailed);
            }
            else if(value == "cash") {
                hCueDashboardReportsServices.getTotalCashReport(params, onDetailsSuccess, onDetailsFailed);
            }
            else if(value == "outstanding") {
                hCueDashboardReportsServices.getOutstandingReports(params, onDetailsSuccess, onDetailsFailed);
            }
            else if(value == "treatment") {
                hCueDashboardReportsServices.getTopTreatments(params, onDetailsSuccess, onDetailsFailed);
            }
            else if(value == "branch") {
                hCueDashboardReportsServices.getBranchesTopReports(params, onDetailsSuccess, onDetailsFailed);
            }
            else if(value == "doctor") {
                hCueDashboardReportsServices.getDutyDoclist(params, onDetailsSuccess, onDetailsFailed);
            }
            else if(value == "payouts") {
                hCueDashboardReportsServices.getPayoutReports(params, onDetailsSuccess, onDetailsFailed);
            }
            else if(value == "expenses") {
                params.HospitalID = hospitalIdVal;
                hCueDashboardReportsServices.getTotalExpensesReport(params, onDetailsSuccess, onDetailsFailed);
            }
            else if(value == "referrals") {
                hCueDashboardReportsServices.getReferralReports(params, onDetailsSuccess, onDetailsFailed);
            }
            else if(value == "inventory") {
                var inventoryParams = {"DoctorID":doctorIdVal,"HospitalID":hospitalIdVal,"HospitalCD":hospitalCode,"PageNumber":1,"PageSize":pageSize,"USRId":76,"USRType":"DOCTOR","ItemType":"DRG"}
                hCueDashboardReportsServices.getInventoryReports(inventoryParams, onDetailsSuccess, onDetailsFailed);
            }
            else if(value == "campaign") {
                hCueDashboardReportsServices.getCampainReports(params,onDetailsSuccess,onDetailsFailed)
            }
            else if(value == "pharmalead") {
                hCueDashboardReportsServices.getPharmaLead(params,onDetailsSuccess,onDetailsFailed)
            }
            else if(value == "consultant") {
               params.includeBranch = "Y";
               params.doctorId = 0;
               hCueDashboardReportsServices.getTrendingConsultReports(params,onDetailsSuccess,onDetailsFailed)
             }
            else if(value == "rated") {
                params.includeBranch="Y";
               hCueDashboardReportsServices.getTopRatedDoctorsReports(params,onDetailsSuccess,onDetailsFailed)
            }
			else if (value == "feedback"){		
                 hCueDashboardReportsServices.getFeedbackReportInfo(params,onDetailsSuccess,onDetailsFailed)		
                         }
			else if (value == "cancelbill"){		
                 hCueDashboardReportsServices.getCancelBillingReportInfo(params,onDetailsSuccess,onDetailsFailed)		
                         }
            else if (value == "MIS"){
                $scope.showmisreports = true;
                $scope.data_show = true;
                $scope.listingMISReport($scope.currentPage,$scope.maxSize);
             
            }

            else if(value == "patientTrmt") {
                hCueDashboardReportsServices.getTopPatientTreatment(params, onDetailsSuccess, onDetailsFailed);
            }else if(value == "LabRprt"){
                hCueDashboardReportsServices.getLabOrdersInfo(params, onDetailsSuccess, onDetailsFailed);
            }else if(value == "SmsRprt"){
                hCueDashboardReportsServices.getSmsReportinfo(params, onDetailsSuccess, onDetailsFailed);
            }

            function onDetailsSuccess(data) {
                if(excelExport != "") {
                    generateTable(data,value);
                }else{
                    $scope.detailedData = data;
                    $scope.totalItems = data.Count;
                    $scope.totalLoadCount = data.Count;
                    if(value == "inventory") {
                        $scope.inventoryPageCount = data.rows;
                        $scope.totalItems = data.rows;
                        $scope.totalLoadCount = data.rows;
                    }
                    if(value=="pharmalead"){
                        $scope.totalItems = data.count;
                        $scope.totalLoadCount = data.count;
                    }
                    if(value == "patientTrmt") {
                        $scope.totalItems = data.count;
                        $scope.totalLoadCount = data.count;
                    }
                    if(value == "referrals"){
                        $scope.totalItems = data.count;
                        $scope.totalLoadCount = data.count;
                    }
					 if(value == "LabRprt"){
                        $scope.totalItems = data.Count;
                        $scope.totalLoadCount = data.Count;
                    }
                    
                    if(value == "cash") {
                        $scope.pagewisetotal = 0;
                        for(var i = 0; i < $scope.detailedData.TotalCashReceivedObj.length; i++){
                            var product = $scope.detailedData.TotalCashReceivedObj[i];
                            $scope.pagewisetotal += (product.AmountReceived);
                        }
                        return $scope.pagewisetotal;
                    }
                }
            }

            function onDetailsFailed(data) {
                console.log("Fetching the details error " + JSON.stringify(data));
            }


        }
    }

    $scope.navigateTo = function(val) {
        $window.scrollTo(0, angular.element(document.getElementById(val)).offsetTop);
    }


    function generateZenotiTreatmentReports(val) {
        
        var myEl = angular.element( document.querySelector( '#TreatmentTree' ) );
        myEl.empty();  //clears contents
        var treatmentName = [];
       
        var bind_val;
        var i = 1;
		var newFont = '10px';
        if(val.length > 8){
            var j = 100;
        }else if(val.length > 5){
            var j = 80;
        }else{
            var j = 40;
        }
        angular.forEach(val,function(item) {
            if(item.value != 0 || item.value > 0) {
                bind_val = {
					'name': item.key +' ('+ $scope.currencyType +' '+ item.value +')',
                    'value': j,
                    'colorValue': i,
					dataLabels: {
						style: {
						    fontSize: newFont,
							 fontFamily: 'serif'
						}
					}
                };
                bind_val.dataLabels.style.fontSize = (bind_val.name.length > 20 && i > 4 || i > 6) ? '7px':newFont;
                treatmentName.push(bind_val);
                i = i+1;
                j = j-9;
                
            }
        });
        Highcharts.chart('TreatmentTree', {
            chart: {
                plotBorderWidth: 1,
            },
            colorAxis: {
               minColor: '#eff6fd',
                maxColor: '#7cb5ec'

            },
			legend: {
				enabled: false
			},
			credits: {
				style: {
					fontSize: '7px'
				}
			},
            tooltip: {
               
                formatter: function () {
                    return this.point.name;
                }
            },
            
            series: [{
                type: 'treemap',
                layoutAlgorithm: 'squarified',
                data:treatmentName
            }],
            title: {
                text: ''
            }
        });

    }

    function generateZenotiBranchReports(val) {
       
        var myEl = angular.element( document.querySelector( '#BranchTree' ) );
        myEl.empty();  //clears contents
        var branchName = [];
       
        var bind_val;
        var i = 1;
		var newFont = '10px';
        if(val.length > 8){
            var j = 100;
			
        }else if(val.length > 5){
            var j = 80;
        }else{
            var j = 40;
        }
        angular.forEach(val,function(item) {
            if(item.value != 0 && item.value > 0) {
                bind_val = {'name': item.key +' ('+ $scope.currencyType +' '+ item.value +')',
                    'value': j,
                    'colorValue': i,
					dataLabels: {
						style: {
							fontSize: newFont,
							 fontFamily: 'serif'
						}
					}
                };
                bind_val.dataLabels.style.fontSize = (bind_val.name.length > 20 && i > 4 || i > 6) ? '7px':newFont;
                branchName.push(bind_val);
                i = i+1;
                j = j-9;
            }
        });
        Highcharts.chart('BranchTree', {
            chart: {
                plotBorderWidth: 1,
            },
            colorAxis: {
                minColor: '#dfffc9',
				maxColor: '#81c868'

            },
			legend: {
				enabled: false
			},
			credits: {
				style: {
					fontSize: '7px'
				}
			},
            tooltip: {
               
                formatter: function () {
                    return this.point.name;
                }
            },
            
            series: [{
                type: 'treemap',
                layoutAlgorithm: 'squarified',
                data:branchName
            }],
            title: {
                text: ''
            }
        });
       
    }

    function generateZenotiDoctorReports(val) {
        /* check for access rights */
         
        var myEl = angular.element( document.querySelector( '#DoctorTree' ) );
        myEl.empty();  //clears contents
        var doctorName = [];
        
        var bind_val;
        var i = 1;
		var newFont = '10px';
        if(val.length > 8){
            var j = 100;
			
        }else if(val.length > 5){
            var j = 80;
			
        }else{
            var j = 40;
			
        }
        angular.forEach(val,function(item) {
            if(item.value != 0 || item.value > 0) {
                bind_val = {'name': item.key +' ('+ $scope.currencyType +' '+ item.value +')',
                    'value': j,
                    'colorValue': i,
					dataLabels: {
						style: {
							fontSize: newFont,
							 fontFamily: 'serif'
						}
					}
                };
                bind_val.dataLabels.style.fontSize = (bind_val.name.length > 20 && i > 4 || i > 6) ? '7px':newFont;
                doctorName.push(bind_val);
                i = i+1;
                j = j-9;
            }
        });
        Highcharts.chart('DoctorTree', {
            chart: {
                plotBorderWidth: 1,
            },
            colorAxis: {
               minColor: '#feebeb',
				maxColor: '#f57070'

            },
			legend: {
				enabled: false
			},
			credits: {
				style: {
					fontSize: '7px'
				}
			},
            tooltip: {
               
                formatter: function () {
                    return this.point.name;
                }
            },
            
            series: [{
                type: 'treemap',
                layoutAlgorithm: 'squarified',
                data:doctorName
            }],
            title: {
                text: ''
            }
        });
        
    }


    function generateZenotiCampaignReports(val) {
        
        var myEl = angular.element( document.querySelector( '#CampaignTree' ) );
        myEl.empty();  //clears contents
        var campaignName = [];
        var bind_val;
        var i = 1;
		var newFont = '10px';
        if(val.length > 8){
            var j = 100;
			
        }else if(val.length > 5){
            var j = 80;
			
        }else{
            var j = 40;
			
        }
        angular.forEach(val,function(item) {
            if(item.value != 0 || item.value > 0) {
                bind_val = {'name': item.CampaignName +' ('+ $scope.currencyType +' '+ item.TotalRevenue +')',
                    'value': j,
                    'colorValue': i,
					dataLabels: {
						style: {
							fontSize: newFont,
							 fontFamily: 'serif'
						}
					}
                };
                bind_val.dataLabels.style.fontSize = (bind_val.name.length > 20 && i > 4 || i > 6) ? '7px':newFont;
                campaignName.push(bind_val);
                i = i+1;
                j = j-9;
            }
        });
        Highcharts.chart('CampaignTree', {
            chart: {
                plotBorderWidth: 1,
            },
            colorAxis: {
                minColor: '#f9d8a7',
                maxColor: '#ffc268'
            },
			legend: {
				enabled: false
			},
			credits: {
				style: {
					fontSize: '7px'
				}
			},
            tooltip: {
               
                formatter: function () {
                    return this.point.name;
                }
            },
            
            series: [{
                type: 'treemap',
                layoutAlgorithm: 'squarified',
                data:campaignName
            }],
            title: {
                text: ''
            }
        });
    }


     /* Pagination Block */
     $scope.maxSize = 10;

     function isInteger(x) {
         return x % 1 === 0;
     }
     $scope.currentPage = 1;
     $scope.setPage = function(pageNo) {
         $scope.currentPage = pageNo;
     };


    $scope.pageChanged = function(pageNo,value) {
        /* Pagination params */
        var hospitalIdVal = $scope.detailedPage.hospitalList.HospitalInfo.HospitalDetails.HospitalID;
        var doctorIdVal = $scope.detailedPage.doctorList.DoctorID;

        var includeBranch = "N";
        if($scope.detailedPage.hospitalList.HospitalInfo.HospitalDetails.HospitalName == "All" && $scope.detailedPage.doctorList.FullName == 'All') {
            includeBranch = "Y";
        }
		if(value == "expenses") {
			var params = {
                "HospitalID": hospitalIdVal,
                "HospitalCD": hospitalCode,
                "ParentHospitalID":parentHospitalId,
                "ValueStartDate":$filter('date')(new Date($scope.detailedPage.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                "ValueEndDate": $filter('date')(new Date($scope.detailedPage.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
                "PageNumber": pageNo,
                "PageSize": 10,
                "SyncStatus":["O","F","S"],
                "TxnType":"D"
            }
		}
		else {
			var params = {
				"pageNumber": pageNo - 1 ,
				"hospitalId": hospitalIdVal,
				"doctorId": doctorIdVal,
				"startDate":$filter('date')(new Date($scope.detailedPage.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
				"endDate": $filter('date')(new Date($scope.detailedPage.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
				"includeBranch": includeBranch,
				"addressID": 0,
				"count": $scope.totalLoadCount,
				"pageSize": 10,
				"paymentType":(value == "cash")?$scope.detailedPage.paymentType:undefined
			}

		}
		if(value == "referrals") {
		    hCueDashboardReportsServices.getReferralReports(params, onPageSuccess, onPageFailed);
		}
        if(value == "patient") {
            hCueDashboardReportsServices.patientInfoList(params, onPageSuccess, onPageFailed);
        }
        else if(value  == "appointment") {
            hCueDashboardReportsServices.getAppointmentReports(params, onPageSuccess, onPageFailed);
        }
        else if(value == "cash") {
            hCueDashboardReportsServices.getTotalCashReport(params, onPageSuccess, onPageFailed);
        }
        else if(value == "outstanding") {
            hCueDashboardReportsServices.getOutstandingReports(params, onPageSuccess, onPageFailed);
        }
        else if(value == "treatment") {
            hCueDashboardReportsServices.getTopTreatments(params, onPageSuccess, onPageFailed);
        }
        else if(value == "branch") {
            hCueDashboardReportsServices.getBranchesTopReports(params, onPageSuccess, onPageFailed);
        }
        else if(value == "doctor") {
            hCueDashboardReportsServices.getDutyDoclist(params, onPageSuccess, onPageFailed);
        }
        else if(value == "payouts") {
            hCueDashboardReportsServices.getPayoutReports(params, onPageSuccess, onPageFailed);
        }
        else if(value == "campaign") {
            hCueDashboardReportsServices.getCampainReports(params,onPageSuccess,onPageFailed)
        }
        else if(value == "pharmalead") {
            hCueDashboardReportsServices.getPharmaLead(params,onPageSuccess,onPageFailed)
        }
        else if(value == "expenses") {
            hCueDashboardReportsServices.getTotalExpensesReport(params,onPageSuccess,onPageFailed)
        }
        else if(value == "consultant") {
           hCueDashboardReportsServices.getTrendingConsultReports(params,onPageSuccess,onPageFailed)
         }
        else if(value == "rated") {
           hCueDashboardReportsServices.getTopRatedDoctorsReports(params,onPageSuccess,onPageFailed)
        }
		else if (value == "cancelbill"){		
                 hCueDashboardReportsServices.getCancelBillingReportInfo(params,onPageSuccess,onPageFailed)		
        }
        else if(value == "patientTrmt") {
            hCueDashboardReportsServices.getTopPatientTreatment(params, onPageSuccess, onPageFailed);
        }
        else if(value == "inventory") {
            var inventoryParams = {"DoctorID":doctorIdVal,"HospitalID":hospitalIdVal,"HospitalCD":hospitalCode,"PageNumber":pageNo,"PageSize":10,"USRId":76,"USRType":"DOCTOR","ItemType":"DRG"}
            hCueDashboardReportsServices.getInventoryReports(inventoryParams, onPageSuccess, onPageFailed);
        }
        else if(value=="MIS"){
            $scope.listingMISReport(pageNo,$scope.maxSize);
        }else if (value == "feedback"){		
            hCueDashboardReportsServices.getFeedbackReportInfo(params,onPageSuccess,onPageFailed)		
        }
		else if(value == "LabRprt"){
                hCueDashboardReportsServices.getLabOrdersInfo(params, onPageSuccess, onPageFailed);
            }
			
			
        function onPageSuccess(data) {
            $scope.detailedData = data;
            //$scope.totalItems = data.Count;
			if(value == "expenses") {
                if(data.Count == 0) {
                    $scope.totalItems = $scope.totalItems;
                }
            }
            else if(value=="pharmalead"){
                $scope.totalItems = data.count;
            }
            else {
                $scope.totalItems = data.Count;
            }
            if(value == "inventory") {

            $scope.totalItems = data.rows;
            }
            if(value == "referrals") {

                $scope.totalItems = data.count;
            }
            if(value == "patientTrmt") {

                $scope.totalItems = data.count;
            }
			if(value == "LabRprt"){
              $scope.totalItems = data.Count;
            }
            if(value == "cash") {
                $scope.pagewisetotal = 0;
                for(var i = 0; i < $scope.detailedData.TotalCashReceivedObj.length; i++){
                    var product = $scope.detailedData.TotalCashReceivedObj[i];
                    $scope.pagewisetotal += (product.AmountReceived);
                }
                return $scope.pagewisetotal;
            }
        }

        function onPageFailed(data) {
            console.log("Pagination Error in "+ type + "reports.Error details " + JSON.stringify(data));
        }
    };

     /* End of pagination Block */


     /* Reports exports to excel */
    $scope.exportDatatoexcel = function(pageCount) {
        exportToStatus = true;
        $scope.searchData(pageCount,pageCount);
    }


    /* Sms Reports to excel */
    var reports_smsexportData = [];
    $scope.excelSMSReports = function (detailType = false) {
        /* Pagination params */
        var hospitalIdVal;
        var doctorIdVal;
        if(detailType) {
            hospitalIdVal = $scope.detailedPage.hospitalList.HospitalInfo.HospitalDetails.HospitalID;
            doctorIdVal = $scope.detailedPage.doctorList.DoctorID;
        }
        else {
            hospitalIdVal = $scope.dashboard.hospitalList.HospitalInfo.HospitalDetails.HospitalID;
            doctorIdVal = $scope.dashboard.doctorList.DoctorID;
            $scope.detailedPage.hospitalList = $scope.dashboard.hospitalList;
            $scope.detailedPage.doctorList = $scope.dashboard.doctorList;
            $scope.detailedPage.startDate = $scope.dashboard.startDate;
            $scope.detailedPage.endDate = $scope.dashboard.endDate;
        }

        var includeBranch = "N";
        if($scope.detailedPage.hospitalList.HospitalInfo.HospitalDetails.HospitalName == "All" && $scope.detailedPage.doctorList.FullName == 'All') {
            includeBranch = "Y";
        }
        var params = {
            "MessageType": "SMS",
            "DoctorID": doctorIdVal,
            "HospitalID": hospitalIdVal,
            "HospitalCD":hospitalCode,
            "StartDate":$filter('date')(new Date($scope.detailedPage.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
            "EndDate": $filter('date')(new Date($scope.detailedPage.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
            "PageNumber": 1 ,
            "PageSize": 1000,
            "USRId": parseInt(doctorIdVal),
            "addressID": 0,
            "USRType":"DOCTOR"
        }
        hCueDashboardReportsServices.getSMSListDetails(params, onSmsSuccess, onSmsFailed);


        function onSmsSuccess(data) {
            if(data.Message == "SUCCESS")
            {
                $scope.Smslistdetails = data.Details;
                angular.forEach( $scope.Smslistdetails,function(item) {
                    reports_smsexportData.push({
                    'PatientName':item.PatientName,
                    'PhoneNumber' : item.PhoneNumber,
                    'MessageBody':item.MessageBody,
                    'Status':item.Status,

                    });
                });
                jsonData = reports_smsexportData;

                var col = [];
                for (var i = 0; i < jsonData.length; i++) {
                    for (var key in jsonData[i]) {
                        if (col.indexOf(key) === -1) {
                            col.push(key);
                        }
                    }
                }

                // CREATE DYNAMIC TABLE.
                var table = document.createElement("table");
                table.style="border: 1px solid #000;";

                // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

                var tr = table.insertRow(-1);               // TABLE ROW.

                for (var i = 0; i < col.length; i++) {
                    var th = document.createElement("th");      // TABLE HEADER.
                    th.innerHTML = col[i];
                    tr.appendChild(th); 
                    tr.style="background:#f00;color:#fff;width:100px"; 
                }

                // ADD JSON DATA TO THE TABLE AS ROWS.
                for (var i = 0; i < jsonData.length; i++) {

                    tr = table.insertRow(-1);
                    tr.style="border: 1px solid #000;"; 

                    for (var j = 0; j < col.length; j++) {
                        var tabCell = tr.insertCell(-1);
                        tabCell.innerHTML = jsonData[i][col[j]];
                    }
                }
                download(table, 'Communication');
            }
        }

        function onSmsFailed(data) {
            console.log("SMS download failed" + JSON.stringify(data));
        }
     }

    

    /* Generate the table structure to download in excel format */
    reports_exportData = [];
    function generateTable(jsonData,typeValue) {
        $scope.xlName = 'New';
        if(typeValue == "cash") {
            reports_exportData.length = 0;
            var pay_type;
            angular.forEach(jsonData.TotalCashReceivedObj,function(itemExports) {
                if(itemExports.PaymentType == "CASH"){
                    pay_type = "Cash";
                }
                else if(itemExports.PaymentType == "CREDITCARD"){
                    pay_type = "Credit Card";
                    
                }
                else if(itemExports.PaymentType == "DEBITCARD"){ 
                    pay_type = "Debit Card";
                
                }
                else if(itemExports.PaymentType == "CHEQUE"){
                    pay_type = "Cheque";
                    
                }
                else if(itemExports.PaymentType == "EMI"){
                    pay_type = "EMI";
                    
                }
                else if(itemExports.PaymentType == "OTHERS"){
                    pay_type = "Others";
                    
                }
                else if(itemExports.PaymentType == null){
                    pay_type = "Others";
                    
                }
                else{
                    pay_type = "N/A";
                    
                }
                reports_exportData.push({
                    'Date':moment((itemExports.Date).substring(0,10)).format("DD-MMMM-YYYY"),
                    'Amount Received' : itemExports.AmountReceived,
                    'Branch':itemExports.Branch,
                    'Payment Type':pay_type,
                    'Doctor Name' : itemExports.DoctorName
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Cash Received Stats";
        }else if(typeValue == "cashrpt"){
            reports_exportData.length = 0;
            var pay_type;
            angular.forEach(jsonData,function(itemExports) {
                reports_exportData.push({
                    "Hospital Name": itemExports.hospitalName,
                    "State Name": itemExports.stateName,
                    "Cash": itemExports.cash,
                    "Card": itemExports.card,
                    "Cheque": itemExports.cheque,
                    "Insurance": itemExports.insurance,
                    "OnLine Payment": itemExports.onlinePayment,
                    "Bajaj Receipt": itemExports.bajai,
                    "PayTM": itemExports.payTm,
                    "Free Charge": itemExports.freeCharge,
                    "ECHS": itemExports.echs,
                    "EHS": itemExports.ehs,
                    "Arogya Bhadratha": itemExports.arogyabhadratha,
                    "Arogya Sahayatha(Exise Dep)": itemExports.arogyasahayatha,
                    "Reliance Ins": itemExports.reliance,
                    "SBI Buddy": itemExports.sbibuddy,
                    "Others": itemExports.others,
                    "Total": itemExports.cash + itemExports.card + itemExports.cheque + itemExports.insurance + itemExports.onlinePayment + itemExports.bajai + itemExports.payTm + itemExports.freeCharge + itemExports.echs + itemExports.ehs + itemExports.arogyabhadratha + itemExports.arogyasahayatha + itemExports.reliance + itemExports.sbibuddy + itemExports.others
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Detailed Cash Received";
        }
        else if(typeValue == "patient") {
            reports_exportData.length = 0;
            angular.forEach(jsonData.TotalNewPatientVisitedObj,function(itemExports) {
                reports_exportData.push({
                    'Consultation Date':$filter('date')(new Date(itemExports.ConsultationDt), "dd-MMMM-yyyy"),
                    'Patient ID': itemExports.PatientDisPlayID,
                    'Patient Name' : itemExports.PatientName,
                    'Gender': (itemExports.AdditionalReportDtls.PatientGender == 'F')?'Female':'Male',
                    'DOB':$filter('date')(new Date(itemExports.AdditionalReportDtls.PatientDOB), "dd-MMMM-yyyy"),
                    'Contact Number': itemExports.AdditionalReportDtls.contactnumber,
                    'Referral Source':(itemExports.AdditionalReportDtls.ReferralSource == undefined)?'Others':itemExports.AdditionalReportDtls.ReferralSource,
                    'Sub Referral Source':(itemExports.AdditionalReportDtls.SubReferralSource == undefined)?'Others':itemExports.AdditionalReportDtls.SubReferralSource,
                    'Patient Type' : (itemExports.PatientType == 'Y')?'New Visit':'Follow Up',
                    'Treatment':itemExports.Treatment,
                    'Doctor Name' :itemExports.DoctorName,
                    'Branch':itemExports.Branch,
					'Amount Received':itemExports.BilledCost
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Patient Stats";
        }
		else if(typeValue == "patientvisit") {
            reports_exportData.length = 0;
            angular.forEach(jsonData.TotalNewPatientVisitedObj,function(itemExports) {
                reports_exportData.push({
				    'Consultation Date':$filter('date')(new Date(itemExports.ConsultationDt), "dd-MMMM-yyyy"),
					'Patient ID': itemExports.PatientDisPlayID,
                    'Patient Name' : itemExports.PatientName,
                    'Gender': (itemExports.AdditionalReportDtls.PatientGender == 'F')?'Female':'Male',
                    'DOB':$filter('date')(new Date(itemExports.AdditionalReportDtls.PatientDOB), "dd-MMMM-yyyy"),
                    'ContactNo': itemExports.AdditionalReportDtls.contactnumber,
                    'EmailID':itemExports.AdditionalReportDtls.emailid,
                    'Referral Source':(itemExports.AdditionalReportDtls.ReferralSource == undefined)?'Others':itemExports.AdditionalReportDtls.ReferralSource,
                    'Sub Referral Source':(itemExports.AdditionalReportDtls.SubReferralSource == undefined)?'Others':itemExports.AdditionalReportDtls.SubReferralSource,
                    'Patient Type' : (itemExports.PatientType == 'Y')?'New Visit':'Follow Up',
                    'Billing Treatment':itemExports.Treatment,
                    'Doctor Name' :itemExports.DoctorName,
                    'Branch':itemExports.Branch,
					'Visit Reason': itemExports.AdditionalReportDtls.visitreason,
                    'Total Dental Treatment':itemExports.AdditionalReportDtls.totaltrtments,
                    'Completed Dental Treatment':itemExports.AdditionalReportDtls.completedtrtments,
                    'Advised Treatment':itemExports.AdditionalReportDtls.advicedtrtments,
				    'Estimation Cost' : itemExports.AdditionalReportDtls.estimationcost,
                	'Feedback': itemExports.AdditionalReportDtls.patientfeedback
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Detailed Patient Visit Report";
        }
        else if(typeValue == "appointment") {
            reports_exportData.length = 0;
            angular.forEach(jsonData.TotalCancelledPatientObj,function(itemExports) {
                if(itemExports.AppointmentType == "FOLLOWUP") {
                    typeApp = "Follow Up";
                }
                else if (itemExports.AppointmentType == "NEWPATIENT") {
                    typeApp = "New Patient";
                }
                else if(itemExports.AppointmentType == "OLDPATIENT") {
                    typeApp = "Existing Patient";
                }
                else if(itemExports.AppointmentType == "NURSING") {
                    typeApp = "Nursing";
                }
                else if(itemExports.AppointmentType == "URGENT") {
                    typeApp = "Urgent Visit";
                }
                else {
                    typeApp = "Others";
                }
                reports_exportData.push({
                    'Appointment Date':$filter('date')(new Date(itemExports.AppointmentDate), "dd-MMMM-yyyy"),
                    'Appointment Time' : itemExports.AppointmentTime,
                    'Doctor Name':itemExports.DoctorName,
                    // 'Patient ID': itemExports.PatientDisplayID,
                    'Patient Name': itemExports.PatientName,
                    // 'Contact Number': itemExports.PhoneNumber,
                    'Branch Name' :itemExports.BranchName,
                    'Appointment Type' : typeApp,
                    'Status' : itemExports.Status,
                    'Reason':itemExports.Reason
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Appointment Stats";
        }
        else if(typeValue == "outstanding") {
            reports_exportData.length = 0;
            angular.forEach(jsonData.TotalOutStandingsObj,function(itemExports) {
                reports_exportData.push({
                    'Date':moment((itemExports.Date).substring(0,10)).format("DD-MMMM-YYYY"),
					'Patient ID': itemExports.PatientDisPlayID,
                    'Patient Name' : itemExports.PatientName,
					'Contact Number': itemExports.PhoneNumber,
                    'Outstanding Amount' : itemExports.OutStandingAmount,
                    'Branch':itemExports.Branch,
                    'Treatment' : itemExports.Treatment,
                    'Doctor Name' : itemExports.DoctorName
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Outstanding Stats";
        }
        else if(typeValue == "treatment") {
            reports_exportData.length = 0;
            angular.forEach(jsonData.TopTrendingTretmentsObj,function(itemExports) {
                reports_exportData.push({
                    'S no':itemExports.Slno,
                    'Treatment':itemExports.Treatment,
                    'Branch':itemExports.Branch,
                    'No of Patient':itemExports.NoOfPatientsTreated,
                    'No of Treatment' : itemExports.NoOfTreatmentsDone,
                    'Cash Received' : itemExports.TotalCashCollected
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Treatment Stats";
        }
        else if(typeValue == "branch") {
            reports_exportData.length = 0;
            angular.forEach(jsonData.TopTrendingBranchesObj,function(itemExports) {
                reports_exportData.push({
                    'Branch':itemExports.Branch,
                    'No of Patient':itemExports.NoOfPatientsTreated,
                    'No of Treatment':itemExports.NoOfTretamentsDone,
                    'Location' : itemExports.PatientType,
                    'Cash Received' : itemExports.TotalCashCollected
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Branch Stats";
        }
        else if(typeValue == "doctor") {

            reports_exportData.length = 0;
            angular.forEach(jsonData.TopTrendingDutyDocObj,function(itemExports) {
                reports_exportData.push({
                    'S no':itemExports.Slno,
                    'Doctor Name' : itemExports.DoctorName,
                    'Branch':itemExports.Branch,
                    'Speciality' : itemExports.Speciality,
                    'No of Treatment':itemExports.NoOfTreatmentsDone,
                    'Cash Received' : itemExports.TotalCashCollected
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Doctor Stats";
        }
        else if(typeValue == "expenses") {
            reports_exportData.length = 0;
            angular.forEach(jsonData.ArrExpDep,function(itemExports) {
                reports_exportData.push({
                    'Date':moment(itemExports.ValueDate).format("DD-MMMM-YYYY"),
                    'Expense Category' : itemExports.AccountHeadDesc,
                    'Description' : itemExports.Description,
                    'Amount':itemExports.Amount,
                    'Entered By' : itemExports.EnteredUser
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Expense stats";
        }
        else if(typeValue == "ExpenseRprt") {
            reports_exportData.length = 0;
            angular.forEach(jsonData,function(itemExports) {
                reports_exportData.push({
                    'Hospital Name':itemExports.hospitalName,
                    'State Name' : itemExports.stateName,
                    'Dental Material' : itemExports.DentalMaterial,
                    'Consultation Charges' : itemExports.ConsultationCharges,
                    'Lab Charges':itemExports.LabCharges,
                    'Conveyance Expense' : itemExports.ConveyanceExp,
                    'Electricity Charges': itemExports.ElectricityCharges,
                    'Hospital Maintenance' : itemExports.HospitalMaintenance,
                    'Internet Expense' : itemExports.InternetExp,
                    'MISC Expense' : itemExports.MISCExpenses,
                    'Postage And Courier Expense' : itemExports.PostageAndCourierExp,
                    'Printing And Stationery Expense' : itemExports.PrintingAnStationeryExp,
                    'Vehicle Maintenance' : itemExports.VehicleMaintenance,
                    'Water Expense' : itemExports.WaterExp,
                    'Staff Welfare Expense' : itemExports.StaffWelfareExp,
                    'Repairs And Maintenance Expense' : itemExports.RepairsAndMaintenanceExp,
                    'Telephone Expense' : itemExports.TelephoneExp,
                    'Travelling Expense' : itemExports.TravellingExp,
                    'Credit Default' : itemExports.CreditDefault,
                    'Salary' : itemExports.otherDetails.Salary,
                    'Rent' : itemExports.otherDetails.Rent,
                    'Total' : itemExports.DentalMaterial + itemExports.ConsultationCharges + itemExports.LabCharges + itemExports.ConveyanceExp + itemExports.ElectricityCharges + itemExports.HospitalMaintenance + itemExports.InternetExp + itemExports.MISCExpenses + itemExports.PostageAndCourierExp + itemExports.PrintingAnStationeryExp + itemExports.VehicleMaintenance + itemExports.WaterExp + itemExports.StaffWelfareExp + itemExports.RepairsAndMaintenanceExp + itemExports.TelephoneExp + itemExports.TravellingExp + itemExports.CreditDefault + itemExports.otherDetails.Salary + itemExports.otherDetails.Rent
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Detailed Expense Report";
        }
        else if(typeValue == "consultant") {
            reports_exportData.length = 0;
            angular.forEach(jsonData.TopTrendingConsultantsObj,function(itemExports) {
                reports_exportData.push({
                    'S no':itemExports.SNO,
                    'Consultant Name':itemExports.ConsultantName,
                    'Branch':itemExports.Branch,
                    'Speciality':itemExports.Speciality,
                    'No of Treatment' : itemExports.NoOfTretamentsDone,
                    'Cash Received' : itemExports.TotalCashCollected
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Consultant Stats";
        }
        else if(typeValue == "rated") {
            reports_exportData.length = 0;
            angular.forEach(jsonData.TopRatedDoctorsObj,function(itemExports) {
                reports_exportData.push({
                    'Doctor Name':itemExports.DoctorName,
                    'Branch':itemExports.Branch,
                    //'No of Treatment':itemExports.NoOfTretamentsDone,
                    'Speciality':itemExports.Speciality,
                    'Rating' : itemExports.Rating
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Top Rated Doctor Stats";
        }
        else if(typeValue == "payouts") {
            reports_exportData.length = 0;
            angular.forEach(jsonData.BillingSplitObj,function(itemExports) {
                reports_exportData.push({
                    'Date':moment((itemExports.Date).substring(0,10)).format("DD-MMMM-YYYY"),
                    'Patient Name' : itemExports.PatientName,
                    'Branch':itemExports.Branch,
                    'Doctor Name':itemExports.DoctorName,
                    'Doctor Share' : itemExports.DoctorShare,
                    'Clinic Share' : itemExports.ClinicShare,
                    'Membership Amount' : itemExports.MembershipAmt,
                    'Treatment' : itemExports.TreatmentName,
                    'Total Amount' : itemExports.TotalAmount
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Payouts Stats";
        }
        else if(typeValue == "referrals") {
            reports_exportData.length = 0;
            angular.forEach(jsonData.rows,function(itemExports) {
                reports_exportData.push({
                    'Date':moment(itemExports.Date).format("DD-MMMM-YYYY"),
                    'Patient ID' : itemExports.PatientDisplayID,
                    'Patient Name' : itemExports.PatientName,
                    'Contact Number' : itemExports.PhoneNumber,
                    'Referrer By' : 'Dr.'+itemExports.RefferedByDoctor,
                    'Referred To Doctor':'Dr.'+itemExports.RefferedToDoctor,
                    'Referred to Doctor speciality':itemExports.RefferedToDoctorSpeciality,
                    'Referral Notes' : itemExports.ReferrelNotes,
                    'Referral Reason' : itemExports.VisitReason
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Referrals Stats";
        }else if(typeValue == "patientTrmt"){
            reports_exportData.length = 0;
            angular.forEach(jsonData.toptrendingtretmentsobj,function(itemExports) {
                reports_exportData.push({
                    'S no': itemExports.slno,
                    'Patient ID': itemExports.patientdisplayid,
                    'Patient Name' : itemExports.patientname,
                    'Gender': (itemExports.gender == 'F')?'Female':'Male',
                    'Contact Number': itemExports.PhoneNumber,
                    'Procedure Name' : itemExports.procedurename,
                    'Branch Name' : itemExports.clinicname,
                    'Procedure Cost' : itemExports.procedurecost,
                    'Received Cost' : itemExports.billedcost,
                    'Discount' : itemExports.discountcost,
                    'Balance' : itemExports.balance
                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Trending Patient Stats";
        }else if(typeValue == "LabRprt"){
            reports_exportData.length = 0;
            angular.forEach(jsonData.LaporderObj,function(itemExports) {
                reports_exportData.push({
                    'Date': moment(itemExports.Date).format("DD-MMMM-YYYY"),
                    'Patient ID' : itemExports.PatientID,
                    'Patient Name' : itemExports.PatientName,
                    'Contact Number': itemExports.PhoneNumber,
                    'Doctor Name': itemExports.DoctorName,
                    'Branch Name' : itemExports.BranchName,
                    'Type' : itemExports.CrownType,
                    'Variety': itemExports.Variety,
                    'Shade' : itemExports.Shade,
                    'Teeth No' : itemExports.ToothNo,
                    'No Of Units' : itemExports.NoOfUntis,
					'Status' : itemExports.Status,
                    'Amount' : itemExports.Amount,
                    'Lab Name' : itemExports.LabName

                });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Lab Order Report";
       	}
		  else if(typeValue == "feedback") {		
            reports_exportData.length = 0;		           
            angular.forEach(jsonData.FeedbackReportObj,function(itemExports) {
                var Question = [];
                angular.forEach(itemExports.ToImproved, function(i,index){
                    var Number = index + 1
                    if(itemExports.FeedbackMode == 'PATIENTAPP'){
                    
                        Question.push(i.question);
                    }else{
                        var obj = 'Q'+Number+':'+ i.question
                        Question.push(obj);
                    }
                    
                })
                reports_exportData.push({	
                    'Date':moment(itemExports.RatingDate).format("DD-MMMM-YYYY"),
					'Patient Name' : itemExports.PatientName,
					'Patient ID ' : itemExports.PatientId,
					'Phone':itemExports.MobileNumber,
					'Age':itemExports.Age,		                   
					'Gender': (itemExports.Gender == 'F')?'Female':'Male',
					'Doctor':itemExports.DoctorName,
					'Branch':itemExports.Branch,
					'Feedback': itemExports.Feedback,
					'Question':Question,
                    'Ratings':itemExports.Ratings/2,		                    
          			'Comments':itemExports.Comments,		                    
					  });
            });
            jsonData = reports_exportData;
            $scope.xlName = "Patient Feedback";
       	}
		 else if(typeValue == "cancelbill") {		
            reports_exportData.length = 0;		           
            angular.forEach(jsonData.TotalCancelCashReceivedObj,function(itemExports) {		         
                reports_exportData.push({		              
					'Date' : itemExports.Date,
					'Branch ' : itemExports.Branch,
					'Doctor Name':itemExports.DoctorName,	                   
					'Cancel Amount': itemExports.CancelledAmount	                   
						                    
					  });
            });
            jsonData = reports_exportData;
            $scope.xlName = 'Billing Cancellation Report';
       	}
        var col = [];
        for (var i = 0; i < jsonData.length; i++) {
            for (var key in jsonData[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }

        // CREATE DYNAMIC TABLE.
        var table = document.createElement("table");
        table.style="border: 1px solid #000;";

        // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

        var tr = table.insertRow(-1);               // TABLE ROW.

        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");      // TABLE HEADER.
            th.innerHTML = col[i];
            tr.appendChild(th); 
            tr.style="background:#f00;color:#fff;width:100px"; 
        }

        // ADD JSON DATA TO THE TABLE AS ROWS.
        for (var i = 0; i < jsonData.length; i++) {

            tr = table.insertRow(-1);
            tr.style="border: 1px solid #000;"; 

            for (var j = 0; j < col.length; j++) {
                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = jsonData[i][col[j]];
            }
        }
        download(table, $scope.xlName);
    }
    /* Generate the table structure to download in excel format */

    /* Get the data and convert to excel format */
    var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,',
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }, format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {
                worksheet: name || 'Worksheet',
                table: table.innerHTML
                }

            var link = document.createElement("a");    
            link.href = uri + base64(format(template, ctx));
            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = filename +''+'.xls';
            
            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);


        }
    })();
    function download(table, typeValue){
        tableToExcel(table, 'Sheet 1', typeValue)
    }
    /* End of the data and convert to excel format */

    $scope.positionZenotiStyle = {
        "position" : "absolute",
        "top" : "0"
    }

    $scope.activeMenuVal = function(val) {
        var id = $location.hash();
        $location.hash(val);
        $anchorScroll();
        $location.hash(id);
        $scope.activeMenu = {
            "value":val
        };
    }

    $scope.navigatePage = function () {
        var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
        var url = "/settings?kwid=" + (guid || "")+"&screen=Inventory";
        $location.url(url);
    }

   

        /***MIS Report ***/
    var reportType = "MIS";
    var reportStatus = "INIT";var now = new Date();
    var todaydate = $filter('date')(now, "yyyy-MM-dd");
    $scope.startDate = todaydate;
    $scope.endDate = todaydate;
    $scope.radioVal = "today";
    $scope.reportname = "";
    $scope.showmisreports = false;
    $scope.data_show = false;
    $scope.rptspecdate = $filter('date')(new Date(), "dd-MMMM-yyyy");
    $scope.rptstartdate = $filter('date')(new Date(), "dd-MMMM-yyyy");
    var cur = new Date();
    var before30days = cur.setDate(cur.getDate() - 30);
    $scope.rptenddate = $filter('date')(before30days,"dd-MMMM-yyyy");
    $scope.showPagination = true;

    $scope.setDate = function(){
        if ($scope.radioVal=="today") {
            $scope.endDate = $filter('date')(todaydate, "yyyy-MM-dd");;
            $scope.startDate = $filter('date')(todaydate, "yyyy-MM-dd");
        } else if ($scope.radioVal=="lastsevenday") {
            var lastseven = new Date();
            $scope.endDate = $filter('date')(lastseven, "yyyy-MM-dd");;
            $scope.startDate = $filter('date')(lastseven.setDate(lastseven.getDate()-7), "yyyy-MM-dd");
        } else if ($scope.radioVal=="lastfifteenday") {
            var lastfifteen = new Date();
            $scope.endDate = $filter('date')(lastfifteen, "yyyy-MM-dd");
            $scope.startDate = $filter('date')(lastfifteen.setDate(lastfifteen.getDate()-15), "yyyy-MM-dd");
        } else if ($scope.radioVal=="lastthirtyday"){
            var lastthirty = new Date();
            $scope.endDate = $filter('date')(lastthirty, "yyyy-MM-dd");
            $scope.startDate = $filter('date')(lastthirty.setDate(lastthirty.getDate()-30), "yyyy-MM-dd");
        } else if($scope.radioVal=="specificdate") {
            $scope.endDate = $filter('date')(new Date($scope.rptspecdate), "yyyy-MM-dd");
            $scope.startDate = $filter('date')(new Date($scope.rptspecdate), "yyyy-MM-dd");
        } else if($scope.radioVal=="daterange") {
            $scope.endDate = $filter('date')(new Date($scope.rptstartdate), "yyyy-MM-dd");
            $scope.startDate = $filter('date')(new Date($scope.rptenddate), "yyyy-MM-dd");
        }
    };



    $scope.addmisreportrequest = function(reportrequestform) {

        if(!reportrequestform.$valid) {
            return;
        }

        $scope.setDate();

        var addreqparams = {
            "ReportType": reportType,
            "ReportDescription": $scope.reportname,
            "StartDate": $scope.startDate,
            "EndDate": $scope.endDate,
            "Status": reportStatus,
            "HospitalCode" : (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo == undefined) ? 0 : doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode,
            "HospitalID":(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo == undefined) ? 0 : doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID,
            "ParentHospitalID": (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo == undefined) ? 0 : doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID,
            "USRId": doctorinfo.doctordata.doctor[0].DoctorID,
            "USRIdType": "DOCTOR"
        }

        hcueMISReportService.addReportRequest(addreqparams, onSuccess, OnError);

        function onSuccess(data) {
            if (data == "success") {
                alertify.success('Request Added Successfully');
                resetreportrequest(reportrequestform);
                $scope.pageChanged($scope.currentPage,$scope.viewtype);
            } else {
                alertify.error("Error Occured, Check Internet connectivity");
                $scope.showloading = false;
            }
        }

        function OnError(data) {
            alertify.error("Error Occured, Check Internet connectivity");
            $scope.showloading = false;
        }

    };

    $scope.listingMISReport = function(pageNumber,pageSize) {
        var listDate = new Date();
        var listingParams = {
            "EndDate":$filter('date')(listDate, "yyyy-MM-dd"),
            "StartDate": $filter('date')(listDate.setDate(listDate.getDate()-365), "yyyy-MM-dd"),
            "HospitalID" : (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo == undefined) ? 0 : doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID,
            "PageNumber":pageNumber,
            "PageSize":pageSize
        }

        hcueMISReportService.listMISReports(listingParams, onSuccessList, OnListError);

        function onSuccessList(data) {
            $scope.requestlist = data.ArrayReportObj;
            $scope.totalItems = data.Count;
        }

        function OnListError(data) {
            console.log("Listing MIS Report error" + JSON.stringify(data));
            alertify.error("Listing MIS Report error");
        }

    };

		$scope.ShowDetailRprt =function(){
         
        var hospitalIdVal = $scope.detailedPage.hospitalList.HospitalInfo.HospitalDetails.HospitalID;
        var doctorIdVal = $scope.detailedPage.doctorList.DoctorID;
        var includeBranch = "N";
        if($scope.detailedPage.hospitalList.HospitalInfo.HospitalDetails.HospitalName == "All" && $scope.detailedPage.doctorList.FullName == 'All') {
            includeBranch = "Y";
        }
            
        var  param = {
				"pageNumber":  0,
				"hospitalId": hospitalIdVal,
				"doctorId": doctorIdVal,
				"startDate":$filter('date')(new Date($scope.detailedPage.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
				"endDate": $filter('date')(new Date($scope.detailedPage.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
				"includeBranch": includeBranch,
				"addressID": 0,
				"count": $scope.totalLoadCount,
				"pageSize": 0,
				
			}
        

            hCueDashboardReportsServices.patientInfoList(param, onDetailsRprtSuccess, onDetailsRprtFailed);

        function onDetailsRprtSuccess(data){

            generateTable(data,'patientvisit');

        }

        function onDetailsRprtFailed(){

        }
    }
    $scope.download = function(data){
        //window.open(data);
        window.location.href = data;
    };

    var resetreportrequest = function(reportrequestform) {
        $scope.reportname = "";
        $scope.startDate = todaydate;
        $scope.endDate = todaydate;
        $scope.radioVal = "today"
        $scope.currentPage = 1;
        $scope.showPagination = true;
        if(reportrequestform!=undefined){
            reportrequestform.$setPristine();
            reportrequestform.submitted = false;
            reportrequestform.$setUntouched();
        }
    };

    $rootScope.$on("refreshTab", function() {
        $scope.data_show = true;
        $scope.show_input = true;
        $scope.showmisreports = true;
    });
        /***End of MIS Report ***/
    
    //$scope.ShowLabRprt = function(){
    //    var param = {
    //        'hospitalId': parentHospitalId, //parent hospitalId
    //        'doctorId': 0, 
    //        'startDate':$filter('date')(new Date($scope.detailedPage.startDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"),
    //        'endDate': $filter('date')(new Date($scope.detailedPage.endDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), "yyyy-MM-dd"), 
    //        'includeBranch': 'Y', 
    //        'addressID': 0, 
    //        'hospitalcd':hospitalCode,
    //        'pageNumber': 0,
    //        'pageSize': 0, 
    //        'count': 0
    //    }
    //    hCueDashboardReportsServices.getLabOrdersInfo(param, onLabRprtSuccess, onLabRprtFailed);

    //    function onLabRprtSuccess (data){
    //        generateTable(data,'LabRprt');
    //    }
    //    function onLabRprtFailed (data){

    //    }
    //}
    

    function getSourceListing(doctorIdVal) {
        var getParams = { "DoctorID": doctorIdVal, "USRType": "DOCTOR", "PageSize": 1000, "PageNumber": 1, "USRId": doctorIdVal}
        hCueReferralServices.getReferralServicesList(getParams,hospitalCode, onSourceSuccess, onSourceFailed);

        function onSourceSuccess(data) {
            if (data.rows.length > 0) {
                $scope.ReferralData.ReferralList = data.rows;
                if ($scope.ReferralData.Source == '' || $scope.ReferralData.Source == undefined) {
                    $scope.ReferralData.SubSource = "";
                } else {
                    var params = {
                        "DoctorID": doctorIdVal,
                        "HospitalCD": hospitalCode,
                        "USRType": "DOCTOR",
                        "CatagoryID": parseInt($scope.ReferralData.Source),
                        "PageSize": 100,
                        "PageNumber": 1,
                        "USRId": doctorIdVal
                    }

                    hCueReferralServices.listSubVisitReferral(params, listSubSouSuccess, listSubSoError);

                    function listSubSouSuccess(data) {
                        if (data.status !== "failure") {
                            var tmp = [];
                            for (var i in data.response.rows) {
                                if (data.response.rows[i].ActiveIND == "Y") {
                                    tmp.push(data.response.rows[i]);
                                }
                            }
                            $scope.ReferralData.subSourceList = tmp;
                        } else {
                            $scope.ReferralData.SubSource = "";
                            $scope.ReferralData.subSourceList = "";
                        }
                    }
                    function listSubSoError(item) {

                    }
                }
            }
        }
        function onSourceFailed(data) {

        }
    }

        //change source
    $scope.SourceChange = function (sourceid) {

        if (sourceid == '' || sourceid == undefined) {
            $scope.ReferralData.SubSource = "";
            $scope.ReferralData.subSourceList = "";
            $scope.showPage('campaign',true)
            return;
        }
        var params = {
            "DoctorID": doctorinfo.doctorid,
            "HospitalCD": $scope.HospitalCD,
            "USRType": "DOCTOR",
            "CatagoryID": parseInt(sourceid),
            "PageSize": 100,
            "PageNumber": 1,
            "USRId": doctorinfo.doctorid
        }

        hCueReferralServices.listSubVisitReferral(params, listSubSourceSuccess, listSubSourceError);

        function listSubSourceSuccess(data) {
            if (data.status !== "failure") {
                var tmp=[];
                for(var i in data.response.rows){
                    if(data.response.rows[i].ActiveIND=="Y"){
                        tmp.push(data.response.rows[i]);
                    }
                }
                $scope.ReferralData.subSourceList = tmp;
                $scope.ReferralData.SubSource = "";
                $scope.showPage('campaign',true)
            } else {
                $scope.ReferralData.SubSource = "";
                $scope.ReferralData.subSourceList = "";
                $scope.showPage('campaign',true)
            }
        }
        function listSubSourceError(data) {

        }
    }
    $scope.subSourceChange = function(){
        $scope.showPage('campaign',true)
    }

    $scope.AppointmentVisit = function(data,value){
        
        if(data == 'APP'){
			$scope.campaignvalue.AppointmentVisit = value;
			$scope.detailedPage.AppointmentVisit = value;
            $scope.showPage('campaign',true)
            
        }else{
			$scope.detailedPage.AppointmentVisit = value;
            $scope.campaignvalue.AppointmentVisit = value;
            $scope.getData("CAMPAIGN");
            loaderStat('Campaign',false);
           
        }
    }

 }]);
