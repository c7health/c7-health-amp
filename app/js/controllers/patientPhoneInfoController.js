﻿hCueDoctorWebApp.lazyController('hCuePatientPhoneInfo', ['$scope', '$location', 'datapersistanceFactory', 'hcueDoctorLoginService', 'alertify', 'hcueConsultationInfo','hcueLoginStatusService','datacookieFactory', function ($scope, $location, datapersistanceFactory, hcueDoctorLoginService, alertify, hcueConsultationInfo, hcueLoginStatusService, datacookieFactory) {



//login check
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === "true") {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    }
    else {
        $location.path('/login');
    }

    //end of login check

    var patientinfo = hcueDoctorLoginService.getNewPatientInfo();
    patientinfo.doctorid = hcueDoctorLoginService.getLoginId();
    patientinfo.title = 'Mr';
    patientinfo.firstname = "";
    patientinfo.lastname = "";
    patientinfo.CurrentAge = "";
    patientinfo.emailid = "";
    patientinfo.address1 = "";
    patientinfo.address2 = "";
    patientinfo.Landmark = "";
    patientinfo.pincode = '';
    patientinfo.citytown = "";
    patientinfo.phonenumber = "";
    patientinfo.familyid = "";
    patientinfo.gender = 'M'
    hcueDoctorLoginService.addNewPatientInfo(patientinfo);
    //
    var emergencycontactinfo = hcueDoctorLoginService.getEmergencycontactinfo();
    emergencycontactinfo.name = '';
    emergencycontactinfo.title = 'Mr';
    emergencycontactinfo.relationship = '';
    emergencycontactinfo.mobilenumber = '';
    emergencycontactinfo.alternatenumber = '';
    hcueDoctorLoginService.addEmergencycontactinfo(emergencycontactinfo);
    //
    var patientotherdetails = hcueDoctorLoginService.getPatientotherdetails();
    patientotherdetails.referralsource = {};
    patientotherdetails.maritalstatus = 'M';
    patientotherdetails.education = '';
    patientotherdetails.occupation = '';
    patientotherdetails.paymentmode = 'OTHERS';
    //patientotherdetails.paymentmode = "Select Mode of Payment";
    patientotherdetails.referralsource = { 'OTHERS': 'Others' };
    //patientotherdetails.referralsource =  'OTHERS'; //"'OTHERS':'Others'";
    hcueDoctorLoginService.addPatientotherdetails(patientotherdetails);
    var patientinfo = hcueDoctorLoginService.getNewPatientInfo();
     $scope.error = false;
     $scope.mobilenumber = "";
     $scope.telephonenumber = "";
     $scope.familyid = "";
     $scope.stdcode = "";
     var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    /* Set the country code */
     if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes !== undefined) {
         if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.countryCode !== undefined) {
             $scope.cntryCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.countryCode;
         }
         else {
             $scope.cntryCode = "91";
         }
     }
     else {
         $scope.cntryCode = "91";
     }
    /* End of set the country code */

     $scope.Sarayucode1 = false;

     if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined) {
         if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined &&
             doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined &&
             doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined &&
             doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined) {
             $scope.Sarayu = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;

             if ($scope.Sarayu == "PRIMEINDIA") {
                 $scope.Sarayucode1 = true;
                 console.log($scope.Sarayucode1);
             }
             else {
                 $scope.Sarayucode1 = false;

             }
         }

     }
     else {
         $scope.Sarayucode1 = false;
     }

	 if(doctorinfo.DoctorSetting !== undefined)
	{
		if(doctorinfo.DoctorSetting.CountryCode == '+971')
		{
			$scope.StdCode = "+971";
			
		}
		else{
			$scope.StdCode = "+91";
		}
	}


     $scope.AddPatient = function() {
         patientinfo.hasphone = false;
         patientinfo.patType = true;
         patientinfo.doctorid = hcueDoctorLoginService.getLoginId();
         hcueDoctorLoginService.Addpatientvalconsult('Start Consultation');
         hcueDoctorLoginService.addNewPatientInfo(patientinfo);
         hcueDoctorLoginService.addAddPatientwithoutConsult(false);
         var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
         // var url = "/registerPatient?kwid=" + (guid || "");
         var url = "/addUpdatePatient?kwid=" + (guid || "");
         $location.url(url);

     }

     $scope.GetPatientCancel = function(){
        var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
        var url = "/appointment?kwid=" + (guid || "");
        $location.url(url);
     }
     $scope.errorvalue = false;
     $scope.mobilevalue = function (val) {
         if (val.length > 0) {
             $scope.errorvalue = false;
         }
     }
     $scope.landvalue = function (val) {
         if (val.length > 0) {
             $scope.errorvalue = false;
         }
     }
     $scope.StdcodeChange = function (val) {
         if (val.length >= 1) {
             $scope.Stdchck = false;
         }
     }

     $scope.GetPatient = function (phonenoform) {
         $scope.Stdchck = false;
         $scope.lndnumChck = false;
         var phonenumber = $scope.mobilenumber;
         var familyid = $scope.familyid;
          
         var stdcode = $scope.stdcode;
         var telnumber = $scope.telephonenumber;
         if (phonenumber.length == 0) {
           if (telnumber.length >= 6 && stdcode.length <= 1) {
             $scope.Stdchck = true;
             return;
           }
           if (stdcode.length >= 1 && telnumber.length == 0) {
               $scope.lndnumChck = true;
               return;
           }
         }
         if (phonenumber.length == 0 && telnumber.length == 0) {
             $scope.errorvalue = true;
             return;
         }
       

         //if ($scope.Sarayucode1 == false) {
         //    $scope.error = phonenoform.telephonenumber.$viewValue == '' && phonenoform.mobilenumber.$viewValue == '';

         //    if (!phonenoform.$valid || $scope.error) {
         //        return;
         //    }
         //} else {
         //    if (familyid == '' && telnumber == '') {

         //        alertify.error("Enter a valid Search or telephone number");
         //        return;
         //    }
         //}
         patientinfo.stdNo = stdcode;
         patientinfo.landlineNumber = telnumber;
         patientinfo.phonenumber = phonenumber;
         patientinfo.familyid = $scope.familyid;
         hcueDoctorLoginService.addNewPatientInfo(patientinfo);

         patientinfo.patType = true;
         var consultinfo = hcueConsultationInfo.getConsultationInfo();
         if(consultinfo.treatmentCollection !== undefined)
		   {
			   if(consultinfo.treatmentCollection.VisitRsn.length > 0)
			   {
					consultinfo.treatmentCollection.VisitRsn = [];
					hcueConsultationInfo.addConsultationInfo(consultinfo);
			   }
         }
         hcueConsultationInfo.addaddeditpatientvitals('');
         var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
         var url = "/patientlist?kwid=" + (guid || "");
         $location.url(url);
         // $location.path('/patientlist');

     }
     if (!JSON.parse(localStorage.getItem("isAppOnline"))) {
         var tmpappointmentinfo = {
             AppointmentID: 0,
             caseid: 0,
             PatientID: 0,
             PatientDetails: { Patient: [], PatientPhone: [] }
         };
         hcueDoctorLoginService.addAppointmentInfo(tmpappointmentinfo);
		 var consultinfo = hcueConsultationInfo.getConsultationInfo();
		 consultinfo.BillingInfo = [];
		 consultinfo.patientid = 0;
		 
         hcueConsultationInfo.addConsultationInfo(consultinfo);
         hcueDoctorLoginService.addBillingInfo('');
     }
 }])
