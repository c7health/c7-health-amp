﻿hCueDoctorWebControllers.controller('hCueParentController', ['$rootScope', '$scope', '$route', '$cookies', '$location', 'hcueDoctorLoginService', 'hcueLoginStatusService', 'hcueDoctorDataService', 'datacookieFactory', 'hcueAuthenticationService', 'datapersistanceFactory', '$anchorScroll', 'alertify', '$timeout', 'hCueMainAdminServices', 'AuthToken', function ($rootScope, $scope, $route, $cookies, $location, hcueDoctorLoginService, hcueLoginStatusService, hcueDoctorDataService, datacookieFactory, hcueAuthenticationService, datapersistanceFactory, $anchorScroll, alertify, $timeout, hCueMainAdminServices, AuthToken) {
    $scope.$route = $route;
    $scope.showList = false;
    $scope.showtab = false;
    $scope.setFalse = false;
    var ChkClinic = hcueDoctorLoginService.getClinicpopup();
    var doctorInfo = hcueDoctorLoginService.getDoctorInfo();
    $scope.urlpath = false;
    var documentURL = window.location.origin;

    if (documentURL.indexOf('justdental.in'.toLowerCase()) >= 0) {
        $scope.urlpath = true;
    }

    if ($scope.urlpath) {
        document.querySelector("link[rel*='icon']").href = "img/Justdental/jd.ico";
        document.querySelector("title").innerHTML = 'Just Dental Web';
    } else {
        document.querySelector("title").innerHTML = 'C7 Health Amp';
    }

    $scope.drnameshow = false;

    if (ChkClinic == 'open') {
        $scope.setFalse = true;
    }

    $scope.SignOut = function () {
        $scope.check_location = $location.url();
        if ($scope.check_location.substring(0, 5) == '/main') {
            $rootScope.$broadcast('consultationMainpopup');
        }
        $scope.openPopupCheck = "";
        hcueDoctorLoginService.setClinicpopup($scope.openPopupCheck);
        if (!JSON.parse(localStorage.getItem("isAppOnline"))) {

            window.location.reload();
        }
        if (JSON.parse(localStorage.getItem("isAppOnline"))) {
            var Param = {
                "USRType": "DOCTOR",
                "DoctorID": doctorInfo.doctorid,
                "USRId": doctorInfo.doctorid
            }

            hcueAuthenticationService.lastSignoutTime(Param, signoutcallbackSuccess, signoutcallbackError);

            function signoutcallbackSuccess(data) {
                // hcueAuthenticationService.RemoveStoredCredential();
                $scope.setHeaderShouldShow(false);
                // $location.$$url = "";
                // $location.$$path = "";
                // $location.$$search = "";
                // $location.$$absUrl = $location.$$absUrl.substring(0, $location.$$absUrl.indexOf('#'));
                // $location.path('/login');
                // AuthToken.setToken();
                // window.location.reload(true);
                 
                var c = document.cookie.split("; ");
                for (i in c)
                    document.cookie = /^[^=]+/.exec(c[i])[0] + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
                localStorage.clear();

                location.replace('index.html');

            }

            function signoutcallbackError(data) {

            }
        } else {
            hcueAuthenticationService.RemoveStoredCredential();
            $location.path('/login');
        }

    };

    $scope.timeStamp = Date.now();
    $scope.profile = function () {
        $scope.setHeaderShouldShow(true);
        var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
        var url = "/profile?kwid=" + (guid || "");
        $location.url(url);

    };
    $scope.doctorreferral = function () {
        $scope.isAccessRightsApplicable = doctorInfo.isAccessRightsApplicable;
        if ($scope.isAccessRightsApplicable) {
            if (doctorInfo.AccountAccessID.indexOf('REGION') !== -1 || doctorInfo.AccountAccessID.indexOf('MNGCCG') !== -1 || doctorInfo.AccountAccessID.indexOf('MNGPRACTICE') !== -1 || doctorInfo.AccountAccessID.indexOf('MNGREFDOCTOR') !== -1) {
                $scope.setHeaderShouldShow(true);
                var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
                var url = "/referralteam?kwid=" + (guid || "");
                $location.url(url);
            } else {
                $scope.setHeaderShouldShow(true);
                $location.path('/unauthorized');
            }
        } else {
            $scope.setHeaderShouldShow(true);
            $location.path('/unauthorized');
        }

    };

    $scope.searchpatient = function () {
        $scope.isAccessRightsApplicable = doctorInfo.isAccessRightsApplicable;

        if ($scope.isAccessRightsApplicable) {
            if (doctorInfo.AccountAccessID.indexOf('VIEWCASE') !== -1) {
                $scope.setHeaderShouldShow(true);
                var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
                var url = "/searchpatient?kwid=" + (guid || "");
                $location.url(url);
            } else {
                $scope.setHeaderShouldShow(true);
                $location.path('/unauthorized');
            }
        } else {
            $scope.setHeaderShouldShow(true);
            $location.path('/unauthorized');
        }
    };
    $scope.appointment = function () {
        $scope.isAccessRightsApplicable = doctorInfo.isAccessRightsApplicable;

        if ($scope.isAccessRightsApplicable) {
            if (doctorInfo.AccountAccessID.indexOf('APPLSTVIEW') !== -1 || doctorInfo.AccountAccessID.indexOf('CALENDER') !== -1 || doctorInfo.AccountAccessID.indexOf('MNGENQUIRY') !== -1 || doctorInfo.AccountAccessID.indexOf('MNREFERRALS') !== -1 || doctorInfo.AccountAccessID.indexOf('LISTVIEW') !== -1) {

                $scope.setHeaderShouldShow(true);
                var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
                var url = "/appointment?kwid=" + (guid || "");
                $location.url(url);
            } else {
                $scope.setHeaderShouldShow(true);
                $location.path('/unauthorized');
            }
        } else {
            $scope.setHeaderShouldShow(true);
            $location.path('/unauthorized');
        }

    };
    $scope.dashboard = function () {
        $scope.isAccessRightsApplicable = doctorInfo.isAccessRightsApplicable;

        if ($scope.isAccessRightsApplicable) {
            if (doctorInfo.AccountAccessID.indexOf('AGENTRPT') !== -1 || doctorInfo.AccountAccessID.indexOf('APPPRARPT') !== -1 || doctorInfo.AccountAccessID.indexOf('CLINICAUDIT') !== -1 || doctorInfo.AccountAccessID.indexOf('COSTANALYTIC') !== -1 || doctorInfo.AccountAccessID.indexOf('COSTRPT') !== -1 || doctorInfo.AccountAccessID.indexOf('DAILYRPT') !== -1 || doctorInfo.AccountAccessID.indexOf('LSTDASHBRD') !== -1 || doctorInfo.AccountAccessID.indexOf('GPPRACIND') !== -1 || doctorInfo.AccountAccessID.indexOf('GENERALRPT') !== -1 || doctorInfo.AccountAccessID.indexOf('PATIENTRPT') !== -1 || doctorInfo.AccountAccessID.indexOf('REFDATAWOIND') !== -1 || doctorInfo.AccountAccessID.indexOf('REFERRALRPT') !== -1 || doctorInfo.AccountAccessID.indexOf('EXCEPTNOTES') !== -1) {
                $scope.setHeaderShouldShow(true);
                var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
                var url = "/dashboard?kwid=" + (guid || "");
                $location.url(url);
            } else {
                $scope.setHeaderShouldShow(true);
                $location.path('/unauthorized');
            }
        } else {
            $scope.setHeaderShouldShow(true);
            $location.path('/unauthorized');
        }

    };

    $scope.publish = function () {
        $scope.isAccessRightsApplicable = doctorInfo.isAccessRightsApplicable;

        if ($scope.isAccessRightsApplicable) {
            if (doctorInfo.AccountAccessID.indexOf('PUBLISH') !== -1) {
                $scope.setHeaderShouldShow(true);
                var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
                var url = "/publish?kwid=" + (guid || "");
                $location.url(url);
            } else {
                $scope.setHeaderShouldShow(true);
                $location.path('/unauthorized');
            }
        } else {
            $scope.setHeaderShouldShow(true);
            $location.path('/unauthorized');
        }

    };

    $scope.reminder = function () {
        $scope.isAccessRightsApplicable = doctorInfo.isAccessRightsApplicable;

        if ($scope.isAccessRightsApplicable) {
            if (doctorInfo.AccountAccessID.indexOf('REMINDER') !== -1) {
                $scope.setHeaderShouldShow(true);
                var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
                var url = "/reminder?kwid=" + (guid || "");
                $location.url(url);
            } else {
                $scope.setHeaderShouldShow(true);
                $location.path('/unauthorized');
            }
        } else {
            $scope.setHeaderShouldShow(true);
            $location.path('/unauthorized');
        }

    };

    $scope.showDoctorAddress = function () {
        if ($scope.showList) {
            $scope.showList = false;
        } else {
            $scope.showList = true;
        }
    }
    $scope.showMoreTab = function () {
        if ($scope.showtab) {
            $scope.showtab = false;
        } else {
            $scope.showtab = true;
        }
    }
    $scope.mouseLeaveshowDoctorAddress = function () {
        $scope.showList = false;
    }

    //Change the array position
    function changeArrayPosition(array, value, positionChange) {
        var swap_pos1 = array[0];
        var swap_pos2 = array[value];

        array[0] = swap_pos2;
        array[value] = swap_pos1;
        return array;
    }
    $scope.OpenClinicPopup = false;
    $scope.changeDoctorAddress = function (indexValue) {

        $scope.setFalse = true;
        $scope.valueIndex = parseInt(indexValue);
        var hash = window.location.hash;
        var URL = hash.substring(0, 6).toLowerCase() == "#/main";
        if (URL) {
            $scope.OpenClinicPopup = true;
        } else {
            var array_val = changeArrayPosition(doctorInfo.doctordata.doctorAddress, indexValue, -indexValue);
            doctorInfo.doctordata.doctorAddress = array_val;
            hcueDoctorLoginService.setDoctorInfo(doctorInfo);
            $scope.LoadDoctorName();
            window.location.reload();

        }


    }

    function closeMainPopup() {
        $('#hamburger-menu').animate({
            'right': '-320px'
        });
    }
    $scope.closePopupConsultation = function () {
        $scope.OpenClinicPopup = false;
        closeMainPopup();
        $scope.changeGlobalClinic = "0";
    }
    $scope.changeGlobalClinic = "0";
    $scope.ChangeClinicfromConsultion = function () {
        var array_val = changeArrayPosition(doctorInfo.doctordata.doctorAddress, $scope.valueIndex, -$scope.valueIndex);
        doctorInfo.doctordata.doctorAddress = array_val;
        hcueDoctorLoginService.setDoctorInfo(doctorInfo);
        $scope.LoadDoctorName();
        $scope.changeGlobalClinic = "0";
        closeMainPopup();
        var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
        var url = "/appointment?kwid=" + (guid || "");
        $location.url(url);
        $scope.OpenClinicPopup = false;
    }


    $timeout(function () {
        $scope.setFalse = true;
        $scope.openPopupCheck = "open";
        hcueDoctorLoginService.setClinicpopup($scope.openPopupCheck);
    }, 20000);

    $scope.closeMessage = function () {
        $scope.openPopupCheck = "open";
        $scope.setFalse = true;
        hcueDoctorLoginService.setClinicpopup($scope.openPopupCheck);
    }
    $rootScope.$on('CallDropdownForClinicChange', function () {
        $scope.allDoctorAddressCollection = doctorInfo.doctordata.doctorAddress;
    });

    $scope.LoadDoctorName = function () {

        var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
        userLoginDetails = datacookieFactory.getItem('loginData');
        var doctorimage = datacookieFactory.getItem('ProfileImage');
        if (userLoginDetails != null) {
            $scope.allDoctorAddressCollection = doctorInfo.doctordata.doctorAddress;
            if (userLoginDetails.RoleID !== undefined && userLoginDetails.RoleID !== "") {
                var parentHospitalId;
                if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
                    parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
                }
                var rolesParams = { "ParentHospitalID": parentHospitalId, "USRId": doctorInfo.doctorid, "USRType": "ADMIN", "ActiveIND": "Y" }
                hCueMainAdminServices.getRolesGroup(rolesParams, OnGetlistHcueRoleSuccess, OnError);

                function OnGetlistHcueRoleSuccess(data) {
                    $scope.hcuerole = data;
                    angular.forEach($scope.hcuerole, function (item) {
                        if (item.RoleID == userLoginDetails.RoleID) {
                            $scope.LoggedRoleValue = item.RoleDesc;
                            if ($scope.allDoctorAddressCollection != null) {
                                angular.element('body').on('click', function () {
                                    if ((!$scope.setFalse) && ($scope.allDoctorAddressCollection != null)) {
                                        $scope.setFalse = true;
                                        $scope.openPopupCheck = "open";
                                        hcueDoctorLoginService.setClinicpopup($scope.openPopupCheck);
                                    }
                                });
                            }
                        }
                    });
                }

                function OnError(data) {

                }
            } else {
                $scope.LoggedRoleValue = "Doctor";
                if ($scope.allDoctorAddressCollection != null) {
                    //$scope.checkAddressId = $scope.allDoctorAddressCollection[0].AddressID;
                    console.log(JSON.stringify($scope.checkAddressId));
                    angular.element('body').on('click', function () {
                        if ((!$scope.setFalse) && ($scope.allDoctorAddressCollection != null)) {
                            $scope.setFalse = true;
                            $scope.openPopupCheck = "open";
                            hcueDoctorLoginService.setClinicpopup($scope.openPopupCheck);

                        }
                    });

                }
            }
            $scope.accessRights = doctorInfo.AccountAccessID;

            $scope.Name = userLoginDetails.displayName;
            $scope.sname = userLoginDetails.displayName;
            $scope.SpecialityName = userLoginDetails.specialisation;
            $scope.Speciality = userLoginDetails.specialisation;
            $scope.groupID = userLoginDetails.GroupID;
            $scope.RoleId = userLoginDetails.RoleID;
            $scope.Speciality = userLoginDetails.specialisation;
            var speciality = datacookieFactory.set('specialisation');

            $scope.drnameshow = false;


            if (datapersistanceFactory.getItem('doctorProfileImage') != "") {
                $scope.ProfileImage = "data:image/png;base64," + GetImage(datapersistanceFactory.getItem('doctorProfileImage'));
            } else {
                $scope.ProfileImage = "data:image/png;base64," + (datapersistanceFactory.getItem('doctorProfileImage') == "") ? "" : GetImage(datapersistanceFactory.getItem('doctorProfileImage'));
            }

        }
    }
    $scope.setHeaderShouldShow = function (val) {
        $scope.loggedinStatus = val;
    };

    $scope.getHeaderShouldShow = function () {
        return $scope.loggedinStatus;
    };

    $scope.chkAdmin = function () {
        $scope.isAccessRightsApplicable = doctorInfo.isAccessRightsApplicable;

        if ($scope.isAccessRightsApplicable) {
            if (doctorInfo.AccountAccessID.indexOf('MANBRNS') !== -1 || doctorInfo.AccountAccessID.indexOf('MANROLES') !== -1 || doctorInfo.AccountAccessID.indexOf('MANUSRS') !== -1 || doctorInfo.AccountAccessID.indexOf('MNGSCHEDULE') !== -1 || doctorInfo.AccountAccessID.indexOf('MNGCALENDER') !== -1 || doctorInfo.AccountAccessID.indexOf('SPECIALITY') !== -1) {

                var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
                var url = "/admin?kwid=" + (guid || "");
                $location.url(url);
            } else {
                $scope.setHeaderShouldShow(true);
                $location.path('/unauthorized');
            }
        } else {
            $scope.setHeaderShouldShow(true);
            $location.path('/unauthorized');
        }
    }

    $scope.LoadDoctorName();

    var DEBUG_MODE = true; // Set this value to false for production

    if (typeof (console) === 'undefined') {
        console = {}
    }

    if (!DEBUG_MODE || typeof (console.log) === 'undefined') {
        // FYI: Firebug might get cranky...
        console.log = console.error = console.info = console.debug = console.warn = console.trace = console.dir = console.dirxml = console.group = console.groupEnd = console.time = console.timeEnd = console.assert = console.profile = function () { };
    }


    $scope.chkOffline = JSON.parse(localStorage.getItem("isAppOnline"));

    $rootScope.$broadcast('chkOfflineMode');

    var cleanCallChkOfflineMode = $rootScope.$on('chkOfflineMode', function (event) {

        $scope.chkOffline = JSON.parse(localStorage.getItem("isAppOnline"));
    });

    $scope.$on('$destroy', function () {
        cleanCallChkOfflineMode();
    });

    $rootScope.$on('doctorProfileImageChange', function () {
        if (datapersistanceFactory.getItem('doctorProfileImage') != "") {
            $scope.ProfileImage = "data:image/png;base64," + GetImage(datapersistanceFactory.getItem('doctorProfileImage'));
        } else {
            $scope.ProfileImage = "data:image/png;base64," + (datapersistanceFactory.getItem('doctorProfileImage') == "") ? "" : GetImage(datapersistanceFactory.getItem('doctorProfileImage'));
        }
    });

}])