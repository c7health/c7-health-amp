﻿hCueDoctorWebControllers.controller('hCueConsultInfoController', ['$scope', '$location', '$routeParams', 'hcueDoctorSignUp', 'hcueDoctorLoginService', 'hcueRegisterDoctorService', 'hcueDoctorProfileService', 'hcueLoginStatusService', 'datacookieFactory', 'NgMap', 'alertify', '$rootScope', function ($scope, $location, $routeParams, hcueDoctorSignUp, hcueDoctorLoginService, hcueRegisterDoctorService, hcueDoctorProfileService, hcueLoginStatusService, datacookieFactory, NgMap, alertify, $rootScope) {
   //login check
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
    userLoginDetails = datacookieFactory.getItem('loginData');
    if(userLoginDetails.loggedinStatus === "true")
    {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    }
    else
    {
        $location.path('/login');
    }
    $scope.clinicImagepopup = false;
    //end of login check
    $scope.loadingPage = function() {
        $scope.showPage = false;
        setTimeout(function() {
            $scope.showPage = true;
        }, 200);
    }
    $scope.loadingPage();

    var addressid = $routeParams.addressid; 
    $scope.addressidparams = $routeParams.addressid;
    
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    var logininfo = hcueDoctorSignUp.getProfileInfo();
    var consultinghours = [];
    $scope.editmode = true;
    $scope.hospitaladdress = {};
    $scope.doctorinfo = doctorinfo;
    logininfo.addressid = addressid;
    logininfo.CurrencyCode = doctorinfo.DoctorSetting.CurrencyCode;
    logininfo.AddressCommunication  = [];
    if (addressid > 0) {
    
        GetExistingConsultInfo(addressid, doctorinfo, $scope.hospitaladdress, consultinghours);
        logininfo.doctorConsultation = doctorinfo.doctordata.doctorConsultation;
        AssignHospitalInfo(logininfo, $scope.hospitaladdress, addressid);

    }
    else {
        logininfo.HospitalId = 0; //new hospital
        logininfo.ClinicName = "";
        logininfo.ContactNumber = "";
        logininfo.Country = "";
        logininfo.State = "";
        logininfo.Address = "";
        logininfo.Address1 = "";
        logininfo.Location = "";
        logininfo.CityTown = "";
        logininfo.Fees = '';
        logininfo.MinPerCase = "15";
        logininfo.Active = "Y";
        logininfo.CurrencyCode = "";
        logininfo.CountryCode = "";
        logininfo.businesshoursUI.length=0;
        angular.forEach(logininfo.businesshoursUI, function (item) {
            item.AddressConsultID = 0;
        });
    }
    $scope.logininfo = logininfo;
    
    $scope.disableTap = function () {
        DisableTap('Autocomplete');
    };
    if(addressid == 0) {
        $scope.view_hospitalDetails = false;
        $scope.edit_hospitalDetails = true;
    }
    else {
        $scope.view_hospitalDetails = true;
        $scope.edit_hospitalDetails = false;
    }

    $scope.edit = function() {
        $scope.view_hospitalDetails = false;
        $scope.edit_hospitalDetails = true;
        $scope.clinicNameTemp = angular.copy(logininfo.ClinicName);
        $scope.clinicAddressTemp = angular.copy(logininfo.Address);
        $scope.clinicAddress1Temp = angular.copy(logininfo.Address1);
        $scope.clinicCityTemp = angular.copy(logininfo.CityTown);
        $scope.clinicLocationTemp = angular.copy(logininfo.Location);
        $scope.clinicContactTemp = angular.copy(logininfo.AddressCommunication);
        $scope.clinicFeesTemp = angular.copy(logininfo.Fees);
        $scope.clinicminPerCaseTemp = angular.copy(logininfo.MinPerCase);
        $scope.activeStatusTemp = angular.copy(logininfo.Active);
        $scope.clinicEditStatus = true;
    }
    $scope.close_edit = function() {
        $scope.view_hospitalDetails = true;
        $scope.edit_hospitalDetails = false;
        if($scope.clinicEditStatus) {
            logininfo.ClinicName = $scope.clinicNameTemp;
            logininfo.Address = $scope.clinicAddressTemp;
            logininfo.Address1 = $scope.clinicAddress1Temp;
            logininfo.CityTown = $scope.clinicCityTemp;
            logininfo.Location =$scope.clinicLocationTemp;
            logininfo.AddressCommunication = $scope.clinicContactTemp;
            logininfo.Fees = $scope.clinicFeesTemp;
            logininfo.MinPerCase =$scope.clinicminPerCaseTemp;
            logininfo.Active = $scope.activeStatusTemp;
        }
    } 
    if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID == "ADMIN" || doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalID == undefined) 
    {
        $scope.disable_name = false;
    }
    else {
        $scope.disable_name = true;
        
    }

    if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID == "ADMIN" || doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalID == undefined) 
    {
        $scope.blnInactiveDisable = false;
    }
    else 
    {
        $scope.blnInactiveDisable = true;
    }

    var countryCodeValue = []; 
    var getplacesParams = {"DoctorID": doctorinfo.doctordata.doctor[0].DoctorID,"ActiveIND": "Y"};
    /* Get Country */
    hcueRegisterDoctorService.getCountry(getplacesParams,onsuccessCountry,onerrorCountry);
    function onsuccessCountry(data) {
        $scope.countryValues = data;
        countryCodeValue.push(data);
        angular.forEach($scope.countryValues , function(item) {
           if(item.CountryID == $scope.logininfo.Country) {
               $scope.countryCodeValue = item.CountryCode;
               $scope.countryCodeSetValue = item.CountryCode;
           }
       });
    }
    function onerrorCountry(data) {
        
    }

    /* Get States */
    hcueRegisterDoctorService.getState(getplacesParams,onsuccessState,onerrorState);
    function onsuccessState(data) {
        $scope.stateDatavalue = [];
        $scope.stateValues = data;
        angular.forEach($scope.stateValues,function(item) {
            if(item.CountryID == $scope.logininfo.Country) {
                $scope.stateDatavalue.push(item);
            }
        });
    }
    function onerrorState(data) {
        
    }
    
    //get the country code
    var currencyCodeValue = [];
    hcueRegisterDoctorService.getreadlookup(onsuccesslookup,onerrorlookup);
    function onsuccesslookup(data) {
        currencyCodeValue.push(data.currencyType);
        $scope.CurrencyType = data.currencyType;

        /*angular.forEach(countryCodeValue , function(item) {
            if(item.CountryID == $scope.logininfo.Country) {
                $scope.countryCodeValue = item.CountryCode;
            }
            angular.forEach(item,function(getCode) {
                if(getCode.CountryID == $scope.logininfo.Country) {
                    $scope.countryCodeSetValue = getCode.CountryCode;
                }
            });
        });*/
    }

    function onerrorlookup(data) {
        console.log(JSON.stringify(data));

    }
    
    /* Change Country */
    $scope.changecountry = function() {
        $scope.stateDatavalue = [];
        logininfo.State = "";
        logininfo.CityTown = "";
        logininfo.Location = "";
        angular.forEach($scope.stateValues,function(item) {
            if(item.CountryID == $scope.logininfo.Country) {
                $scope.stateDatavalue.push(item);
            }
        });
        angular.forEach(countryCodeValue , function(item) {
            angular.forEach(item,function(getCode) {
                if(getCode.CountryID == $scope.logininfo.Country) {
                    $scope.countryCodeSetValue = getCode.CountryCode;
                }
            });
        });


        /* Get Currency */
        angular.forEach(currencyCodeValue,function(currencyItem) {
            angular.forEach(currencyItem,function(currencyItemValue) {
                if(currencyItemValue.ActiveIND == "Y") {
                    if(currencyItemValue.CountryID == $scope.logininfo.Country) {
                        logininfo.CurrencyCode = currencyItemValue.CurrencyCode;
                    }
                }
            });
        });

        setTimeout(function() {
            $scope.changecitytown();
        }, 300);
    }
    
    /* Location & City */
    hcueRegisterDoctorService.getCity(getplacesParams,onsuccesscity,onerrorcity);
    function onsuccesscity(data) {
        $scope.cityValues = data;
        $scope.cityDatavalue = [];
        angular.forEach($scope.cityValues,function(item) {
            if(item.StateID == $scope.logininfo.State) {
                $scope.cityDatavalue.push(item);
            }
        })
    }
    function onerrorcity(data) {
        
    }
    
    hcueRegisterDoctorService.getLocations(getplacesParams,onsuccess,onerror);
    function onsuccess(data) {
        $scope.locationValues = data;        
        angular.forEach($scope.locationValues,function(item) {
            if(item.CityID == $scope.logininfo.CityTown) {
                $scope.locationsetValues.push({item});
            }
        });
    }
    function onerror(data) {
        
    }

    $scope.changeState = function() {
        logininfo.CityTown = "";
        logininfo.Location = "";
        $scope.cityDatavalue = [];
         angular.forEach($scope.cityValues,function(item) {
            if(item.StateID == $scope.logininfo.State) {
                $scope.cityDatavalue.push(item);
            }
        });
        
    }
    
    /* Location & City */
    $scope.changecitytown = function() {
        logininfo.Location = "";
        $scope.locationsetValues = []
        angular.forEach($scope.locationValues,function(item) {
            if(item.CityID == $scope.logininfo.CityTown) {
                $scope.locationsetValues.push({item});
            }
        });
    }
    $scope.locationsetValues = [];
    angular.forEach($scope.locationValues,function(item) {
        if(item.CityID == logininfo.CityTown) {
            $scope.locationsetValues.push({item});
        }
    });

    //google map api
  $scope.types = "['establishment']";
  $scope.placeChanged = function() {
    $scope.latlngStatus = true;
    $scope.place = this.getPlace();
    //lAT & lONG
    var lat_val = $scope.place.geometry.location.lat();
    var lng_val = $scope.place.geometry.location.lng();
    logininfo.Longitude = lng_val;
    logininfo.Latitude = lat_val;
    // END OF LAT & LONG
    angular.forEach($scope.place.address_components,function(place) {
        angular.forEach(place.types,function(item) {
            if(item == "country") {
                logininfo.Country = place.short_name;
            }
            $scope.changecountry();

            setTimeout(function() {
                if(item == "administrative_area_level_1") {
                    logininfo.State = place.short_name;
                }
                $scope.changeState();
            }, 50);

            //$scope.changecitytown();
            setTimeout(function() {
                if(item == "administrative_area_level_2") {
                    angular.forEach($scope.cityValues,function(item) {
                        if(item.CityIDDesc == place.short_name) {
                            window.dispatchEvent(new Event('resize'));  
                            logininfo.CityTown = item.CityID;
                        }
                    });
                }
                $scope.changecitytown();
            }, 100);


            setTimeout(function() {
                if(item == "sublocality_level_1") {
                    angular.forEach($scope.locationsetValues,function(locationitem) {
                        if(locationitem.item.LocationIDDesc == place.short_name) {
                            window.dispatchEvent(new Event('resize'));  
                            logininfo.Location = locationitem.item.LocationID;
                        }
                    });
                }
            }, 200);


            
        })
    });
    //$scope.map.setCenter($scope.place.geometry.location);
  }
  NgMap.getMap().then(function(map) {
    $scope.map = map;
  });
  $scope.getpos = function(event){
     $scope.latlng = [event.latLng.lat(), event.latLng.lng()];
     $scope.marked_lat = event.latLng.lat();
     $scope.marked_lng = event.latLng.lng();

     
  };


    $scope.closeMapmodal = function() {
        $scope.showMapBlock = false;
        $scope.latlngStatus = false;
    }
    // end of google map api

    $scope.Update = function (loginform, logininfo,val,option) {

        if(option) {
            if (!loginform.$valid) {
                return;
            }
        }

        if(!$scope.latlngStatus) {
            if($scope.clinicAddressTemp != logininfo.Address) {
                logininfo.Longitude = "0";
                logininfo.Latitude = "0";
                window.dispatchEvent(new Event('resize'));  
                $scope.showMapBlock = true;
                NgMap.initMap('mapID');
                //Populate the location
                angular.forEach($scope.locationsetValues,function(locationitem) {
                    if(locationitem.item.LocationID == logininfo.Location) {
                        $scope.address_custom = locationitem.item.LocationIDDesc;

                    }
                });
                return;
            }
        }
        $scope.error_location_mapping = false;
		/*
        if(logininfo.Address1 !== "" || logininfo.Address1 !== undefined) {
           //logininfo.Address = logininfo.Address1 + ',' + logininfo.Address;
            if($scope.clinicAddress1Temp != logininfo.Address1) {
                logininfo.Address = logininfo.Address1 + ',' + logininfo.Address;
            }
        }*/
        angular.forEach(countryCodeValue , function(item) {
            if(item.CountryID == $scope.logininfo.Country) {
                $scope.countryCodeValue = item.CountryCode;
            }
        });


        if(logininfo.Fees == undefined || logininfo.Fees == "") {
            logininfo.Fees = 0;
        }

        if (angular.isUndefined(logininfo.Longitude)) {
            logininfo.Longitude = "0";
        }
        if (angular.isUndefined(logininfo.Latitude)) {
            logininfo.Latitude = "0";
        }
        if (!angular.isUndefined($scope.logininfo.latlng)) {
            var lat = $scope.logininfo.latlng.geometry.location.lat();  //Variable refered in ngAutoComplete to avoid another service call
            var lng = $scope.logininfo.latlng.geometry.location.lng();
            $scope.logininfo.Latitude = lat;
            $scope.logininfo.Longitude = lng;
            logininfo.Latitude = lat;
            logininfo.Longitude = lng;
        }
      
        $scope.showloading = true;
        hcueDoctorProfileService.SaveBusinessHours(doctorinfo, logininfo,$scope.countryCodeSetValue, OnProfileInfoSaved, OnError)

    }
	$scope.mapReloadcontainer = function() {
		window.dispatchEvent(new Event('resize')); 
		NgMap.initMap('mapID');
	}
    $scope.mapUpdate = function (loginform, logininfo,val,option) {
        if($scope.marked_lat == undefined) {
            $scope.error_location_mapping = true;
            return;
        }
        logininfo.Latitude = $scope.marked_lat;
        logininfo.Longitude = $scope.marked_lng;
        $scope.latlngStatus = true;
        alertify.confirm('Can we mark this place as your exact location?', function(){ 
            $scope.Update(loginform, logininfo,val,option);
            $scope.showMapBlock = false;
         }, function(){ });
    }
    
    $scope.AddBusinessHours = function () {
        var newitem = { ApplyTo: false, AddressConsultID: 0, DayCode: 'Sunday', DayCD: 'SUN', Active: 'N', StartTimeHH: '00', StartTimeMM: '00', StartTime: '00:00', EndTimeHH: '00', EndTimeMM: '00', EndTime: '00:00' };
         logininfo.businesshoursUI.push(newitem);
        
    }
    if(logininfo.businesshoursUI.length == 0) {
        $scope.AddBusinessHours();
    }
    var temp_MinPerCase;
    temp_MinPerCase = $scope.logininfo.MinPerCase;
    $scope.changemintues = function() {
        for(var i=0;i<$scope.logininfo.businesshoursUI.length;i++) {
            $scope.logininfo.businesshoursUI[i].EndTimeMM = "00";
            $scope.logininfo.businesshoursUI[i].StartTimeMM = "00";
            $scope.logininfo.businesshoursUI[i].StartTime = $scope.logininfo.businesshoursUI[i].StartTimeHH+':'+"00";
            $scope.logininfo.businesshoursUI[i].EndTime = $scope.logininfo.businesshoursUI[i].EndTimeHH+':'+"00";
        }
        if($scope.logininfo.MinPerCase == 5) {
                $scope.consultationminituesvalue = [{"mins":"00"},{"mins":"05"},{"mins":"10"},  {"mins":"15"},{"mins":"20"},{"mins":"25"},{"mins":"30"},{"mins":"35"},{"mins":"40"},{"mins":"45"},{"mins":"50"},{"mins":"55"}];
        }
        else if($scope.logininfo.MinPerCase == 10) {
            $scope.consultationminituesvalue = [{'mins':'00'},{'mins':'10'},{'mins':'20'},{'mins':'30'},{'mins':'40'},{'mins':'50'}]
        }
        else if($scope.logininfo.MinPerCase == 15) {
            $scope.consultationminituesvalue = [{'mins':'00'},{'mins':'15'},{'mins':'30'},{'mins':'45'}]
        }
        else if($scope.logininfo.MinPerCase == 20) {
           $scope.consultationminituesvalue = [{'mins':'00'},{'mins':'20'},{'mins':'40'}]
        }
        else if($scope.logininfo.MinPerCase == 30) {
            $scope.consultationminituesvalue = [{'mins':'00'},{'mins':'30'}]
        }
        else if($scope.logininfo.MinPerCase == 60) {
            $scope.consultationminituesvalue = [{'mins':'00'}];
        }
        ////console.log(JSON.stringify($scope.consultationminituesvalue));
    }
    
    $scope.RemoveBusinessHours = function (consulthritem) {
        var index = $scope.logininfo.businesshoursUI.indexOf(consulthritem);
        $scope.logininfo.businesshoursUI.splice(index, 1);
    }
    $scope.OnScheduleEdit = function (item) {
        item.DayCD = item.DayCode.substring(0, 3).toUpperCase();
        item.StartTime = item.StartTimeHH + ':' + item.StartTimeMM;
        item.EndTime = item.EndTimeHH + ':' + item.EndTimeMM;
        item.ApplyTo = true;

    }
    $scope.ApplyToAll = function (item) {
        angular.forEach($scope.logininfo.businesshoursUI, function (scheduleItem) {
            if (false == scheduleItem.ApplyTo) {
                scheduleItem.StartTimeHH = item.StartTimeHH;
                scheduleItem.StartTimeMM = item.StartTimeMM;
                scheduleItem.StartTime = item.StartTimeHH + ':' + item.StartTimeMM;
                scheduleItem.EndTimeHH = item.EndTimeHH;
                scheduleItem.EndTimeMM = item.EndTimeMM;
                scheduleItem.EndTime = item.EndTimeHH + ':' + item.EndTimeMM;
                scheduleItem.Working = item.Working;
            }
        });
    }
    
    if(logininfo.MinPerCase == 5) {
        $scope.consultationminituesvalue = [{"mins":"00"},{"mins":"05"},{"mins":"10"},{"mins":"15"},{"mins":"20"},{"mins":"25"},{"mins":"30"},{"mins":"35"},{"mins":"40"},{"mins":"45"},{"mins":"50"},{"mins":"55"}];
    }
    else if(logininfo.MinPerCase == 10) {
        $scope.consultationminituesvalue = [{'mins':'00'},{'mins':'10'},{'mins':'20'},{'mins':'30'},{'mins':'40'},{'mins':'50'}];
    }
    else if(logininfo.MinPerCase == 15) {
        $scope.consultationminituesvalue = [{'mins':'00'},{'mins':'15'},{'mins':'30'},{'mins':'45'}];
    }
    else if(logininfo.MinPerCase == 20) {
       $scope.consultationminituesvalue = [{'mins':'00'},{'mins':'20'},{'mins':'40'}];
    }
    else if(logininfo.MinPerCase == 30) {
        $scope.consultationminituesvalue = [{'mins':'00'},{'mins':'30'}];
    }
    else if(logininfo.MinPerCase == 60) {
        $scope.consultationminituesvalue = [{'mins':'00'}];
    }
    
    if( logininfo.AddressCommunication == undefined || logininfo.AddressCommunication.length == 0) {
        logininfo.AddressCommunication.push({"PhCntryCD": '',"PhStateCD": '',"PhAreaCD": '',"PhNumber": '',"PhType": "M","RowID": 0,"PrimaryIND": "Y","PublicDisplay": "Y"});
    }
    function AssignHospitalInfo(logininfo, hospitaladdress, addressid) {
        var businesshours = [];
        logininfo.HospitalId = hospitaladdress.AddressID;
        logininfo.ClinicName = hospitaladdress.ClinicName;
        logininfo.Country = hospitaladdress.Country;
        logininfo.State = hospitaladdress.State;
        logininfo.CityTown = hospitaladdress.CityTown;
        logininfo.Location = hospitaladdress.Location;
        logininfo.Address1 = hospitaladdress.Address1;
        logininfo.Address = hospitaladdress.Address2;
        logininfo.Active = hospitaladdress.Active;
        if (hospitaladdress.ExtDetails != null) {
            ////console.log(JSON.stringify(hospitaladdress.ExtDetails.AddressCommunication));
            logininfo.Latitude = hospitaladdress.ExtDetails.Latitude;
            logininfo.Longitude = hospitaladdress.ExtDetails.Longitude;
            logininfo.AddressCommunication = hospitaladdress.ExtDetails.AddressCommunication;
            if(hospitaladdress.ExtDetails.ClinicImages !== undefined && hospitaladdress.ExtDetails.ClinicImages !== null) {
                logininfo.ClinicImages = GetDatas(hospitaladdress.ExtDetails.ClinicImages.Images);
            }
        }
        
        
               
        
        angular.forEach(consultinghours, function (consulthr) {
            if (consulthr.AddressID == addressid) {
                logininfo.Fees = consulthr.Fees;
                if(consulthr.MinPerCase !== undefined) {
		   logininfo.MinPerCase = consulthr.MinPerCase.toString();
		}
                var businesshrs = {
                    AddressConsultID: consulthr.AddressConsultID,
                    DayCode: GetDay(consulthr.DayCD), DayCD: consulthr.DayCD,
                    StartTimeHH: GetHours(consulthr.StartTime), StartTimeMM: GetMins(consulthr.StartTime), StartTime: consulthr.StartTime,
                    Active:consulthr.Active,
                    EndTimeHH: GetHours(consulthr.EndTime), EndTimeMM: GetMins(consulthr.EndTime), EndTime: consulthr.EndTime,
                    Fees: consulthr.Fees,
                    MinPerCase: consulthr.MinPerCase
                };
                businesshours.push(businesshrs);
            }
        })
        logininfo.businesshoursUI = businesshours;
        $scope.businesshoursTemp = angular.copy(logininfo.businesshoursUI);
    }
	function getConsultationTimings(hours) {
        var businesshours = [];
        angular.forEach(hours, function (consulthr) {
            if (consulthr.AddressID == addressid) {
                logininfo.Fees = consulthr.Fees;
                logininfo.MinPerCase = consulthr.MinPerCase.toString();
                var businesshrs = {
                    AddressConsultID: consulthr.AddressConsultID,
                    DayCode: GetDay(consulthr.DayCD), DayCD: consulthr.DayCD,
                    StartTimeHH: GetHours(consulthr.StartTime), StartTimeMM: GetMins(consulthr.StartTime), StartTime: consulthr.StartTime,
                    Active:consulthr.Active,
                    EndTimeHH: GetHours(consulthr.EndTime), EndTimeMM: GetMins(consulthr.EndTime), EndTime: consulthr.EndTime,
                    Fees: consulthr.Fees,
                    MinPerCase: consulthr.MinPerCase
                };
                businesshours.push(businesshrs);
            }
        })
        logininfo.businesshoursUI = businesshours;
        $scope.businesshoursTemp = angular.copy(logininfo.businesshoursUI);
    }

    function GetExistingConsultInfo(addressid, doctorinfo ,hospitaladdress, consultinghours) {
        angular.forEach(doctorinfo.doctordata.doctorAddress, function (item) {
            if (addressid == item.AddressID) {
                hospitaladdress = item;
                $scope.hospitaladdress = hospitaladdress;
            }
        });
        angular.forEach(doctorinfo.doctordata.doctorConsultation, function (itemhr) {
            if (addressid == itemhr.AddressID) {
                var consulthour = itemhr.MinPerCase;
                consultinghours.push(itemhr);
            }
        });
    }
     
    function OnProfileInfoSaved(data) {
        if (data == "Invalid EmailId"   ) {
            alert('Invalid Email id');
        }
        else if(data.doctorAddress == undefined) {
            alertify.error(JSON.stringify(data));
            logininfo.businesshoursUI = $scope.businesshoursTemp;
            $scope.update_btn_status = false;
            $scope.selectedAll = false;
        }
        else {
            doctorinfo.doctordata.doctorConsultation = data.doctorConsultation;
            doctorinfo.doctordata.doctorAddress = data.doctorAddress;
			doctorinfo.doctordata.DoctorSetting = data.DoctorSetting;
            hcueDoctorLoginService.setDoctorInfo(doctorinfo);
            $scope.showloading = false;
            $scope.clinicEditStatus = false;
            $scope.close_edit();
            if (addressid == 0) {
                var addressLength = data.doctorAddress.length;
                var getAdressVal = data.doctorAddress[addressLength - 1];
                $location.path('/editconsultinfo/' + getAdressVal.AddressID);
            }
            alertify.success('Successfully updated');
            $scope.latlngStatus= false;
            $scope.selection.length = 0;
            $scope.update_btn_status = false;
			$scope.selectedAll = false;
			getConsultationTimings(data.doctorConsultation);
            //setTimeout(function () {
               //window.location.reload();
            //}, 250);
			 $rootScope.$broadcast('CallDropdownForClinicChange');
        }
    }
    function OnError(data) {
        $scope.showloading = false;
    }
    $scope.selection=[];
    $scope.checkAll = function(selectedAll) {
        if(selectedAll) {
             $scope.selection = ['SUN','MON','TUE','WED','THU','FRI','SAT'];
        }
        else {
             $scope.selection = [];
        }
    }
     $scope.toggleSelection = function toggleSelection(val) {
         var idx = $scope.selection.indexOf(val);
         if (idx > -1) {
           $scope.selection.splice(idx, 1);
         }
         else {
           $scope.selection.push(val);
         }
         if($scope.selection.length == 0 || $scope.selection.length < 7) {
            $scope.selectedAll = false;
         }
         if($scope.selection.length == 7) {
            $scope.selectedAll = true;
         }
     };
    $scope.daycdList = [{"id":"Sunday","value":"SUN",},{"id":"Monday","value":"MON",},{"id":"Tuesday","value":"TUE"},{"id":"Wednesday","value":"WED"},{"id":"Thursday","value":"THU"},{"id":"Friday","value":"FRI"},{"id":"Saturday","value":"SAT"}];
    $scope.goToprofile = function() {
        $location.path("/profile");
    }
    $scope.timestatuslog = function(item) {
        ////console.log(item);
        $scope.selection.length = 0;
        $scope.selection.push(item.DayCD);
        $scope.startTimeHH = item.StartTimeHH
        $scope.startTimeMM = item.StartTimeMM;
        $scope.endTimeHH = item.EndTimeHH;
        $scope.endTimeMM = item.EndTimeMM;
        $scope.addressconsultid = item.AddressConsultID;
        $scope.update_btn_status = true;
        if(item.Active == "Y" || item.Active == undefined) {
            $scope.active = "Y";
        }
        else {
            $scope.active = "N";
        }
        $('html,body').animate({scrollTop:200 }, "slow");
    }

    $scope.startTimeHH = "00";
    $scope.endTimeHH = "00";
    $scope.startTimeMM = "00";
    $scope.endTimeMM = "00";

    $scope.addcousultationTiming = function(loginform,logininfo,val) {
        if($scope.selection.length == 0) {
            $scope.daySelectionError = true;
            return;
        }
        else if($scope.startTimeHH > $scope.endTimeHH) {
			$scope.daySelectionError = false;
			$scope.validStartTimeEndTime = true;
            //alert('Please select valid start time and end time');
            return;
        }
        else if($scope.startTimeHH >= $scope.endTimeHH) {
			$scope.daySelectionError = false;
            if($scope.startTimeMM > $scope.endTimeMM) {
				$scope.validStartTimeEndTime = true;
                //alert('Please select valid start time and end time');
                return;
            }
        }
        else if($scope.startTimeHH == "00") {
			$scope.daySelectionError = false;
			$scope.validStartTimeEndTime = true;
            //alert('Please select valid start time');
                return;
        }
        else if($scope.endTimeHH == "00") {
			$scope.daySelectionError = false;
			$scope.validStartTimeEndTime = true;
            //alert('Please select valid end time');
                return;
        }
        else {
            $('html,body').animate({scrollTop:790 }, "slow");
        }

        if($scope.startTimeHH == $scope.endTimeHH) {
            if($scope.startTimeMM == $scope.endTimeMM) {
                $scope.validStartTimeEndTime = true;
                //alert('Please select valid start time and end time');
                return;
            }
        }
		$scope.validStartTimeEndTime = false;
		$scope.daySelectionError = false;
        angular.forEach($scope.selection,function(selecteditemset) {
             if($scope.addressconsultid == 0 || $scope.addressconsultid == undefined) {
                logininfo.businesshoursUI.push({
                    AddressConsultID: 0,
                    DayCD:selecteditemset,
                    Active:'Y' ,
                    EndTime: $scope.endTimeHH +':'+ $scope.endTimeMM,
                    StartTime: $scope.startTimeHH +':'+ $scope.startTimeMM,
                    Fees: logininfo.Fees,
                    MinPerCase: logininfo.MinPerCase
                });
             }
			 else {
				angular.forEach(logininfo.businesshoursUI, function (scheduleItem) {
					if(scheduleItem.AddressConsultID == $scope.addressconsultid) {
						scheduleItem.StartTime = $scope.startTimeHH +':'+ $scope.startTimeMM;
						scheduleItem.EndTime = $scope.endTimeHH +':'+ $scope.endTimeMM;
						scheduleItem.DayCD = selecteditemset;
						scheduleItem.Active = 'Y';
						$scope.addressconsultid = 0;
					}
				 });
			 }
        });
        $scope.latlngStatus = true;
        $scope.Update(loginform,logininfo,val,false);
        
        $scope.startTimeHH = "00";
        $scope.endTimeHH = "00";
        $scope.startTimeMM = "00";
        $scope.endTimeMM = "00";
        $scope.selection.length = 0;
    }   


    $scope.inactive_timings = function(item,loginform,logininfo,val) {
        $scope.latlngStatus = true;
        if(item.AddressConsultID == 0) {
            alert("Kindly refresh this page to delete the time slot");
            return;
        }
        if(item.Active == "Y") {
            //var inactive_confirm = confirm("Are you sure you want to deactivate?");
            //if(inactive_confirm) {
                angular.forEach(logininfo.businesshoursUI, function (scheduleItem) {
                    if(scheduleItem.AddressConsultID == item.AddressConsultID) {
                        if(item.Active == "Y") {
                            scheduleItem.Active = "N";
                        }
                        else {
                            scheduleItem.Active = "Y";
                        }
                        
                    }
                });  
            //}
        }
        else {
            angular.forEach(logininfo.businesshoursUI, function (scheduleItem) {
                if(scheduleItem.AddressConsultID == item.AddressConsultID) {
                    if(item.Active == "Y") {
                        scheduleItem.Active = "N";
                    }
                    else {
                        scheduleItem.Active = "Y";
                    }
                    
                }
            });  
        }
        $scope.Update(loginform,logininfo,val,false);   
    }
    $scope.slotStatusbtn = function() {
        $scope.showAddslotInfo = false;
    }
    if($scope.addressidparams > 0) {
        $scope.showAddslotInfo = true;
        var slotinfotemp;
        angular.forEach(logininfo.businesshoursUI,function(SlotBussinessTime) { 
            if(SlotBussinessTime.StartTimeHH !== "00") {
                $scope.showAddslotInfo = false;
                return;
            }

        });
    }

    $scope.AddPhoneNumber = function () {
        var newitem = { PhNumber: '',PhType: 'M',RowID:'',PhAreaCD:'' };
        $scope.logininfo.AddressCommunication.push(newitem);
    }
    $scope.RemovePhoneNumber = function(item) {
        var index = $scope.logininfo.AddressCommunication.indexOf(item);
        $scope.logininfo.AddressCommunication.splice(index, 1);
    }

    $scope.OpenUploadLogoImage = function (files,key) {

        var type = files[0].type;
        for(file of files) {
            if (file.name.toLowerCase().indexOf(".stv") >= 0 || file.name.toLowerCase().indexOf(".rvg") >= 0 || file.name.toLowerCase().indexOf(".jpeg") >= 0 ||
            file.name.toLowerCase().indexOf(".pano") >= 0 || file.name.toLowerCase().indexOf(".png") >= 0 || file.name.toLowerCase().indexOf(".jpg") >= 0) {
                var formData = new FormData();
                formData.append("fileUpload", file);
                formData.append("hospitalID ", $scope.HospitalID);
                formData.append("isLogoImage ", key == 'Y' ? 'Y':'N');
                formData.append("fileExtn  ", type);
               
                hcueDoctorProfileService.uploadHospitalClinicImage(formData, onLogosuccess, onLogoerror);
            }
            else {
                alertify.error("Upload Images with format .jpg, .jpeg, .png");

            }
        }
    }
    function onLogosuccess(data) {
        $scope.clinicLogoImgList();
    }
    function onLogoerror(data) {

    }

    $scope.clinicLogoImgList = function () {
        var params = { "DoctorID": hcueDoctorLoginService.getLoginId(), "USRType": "ADMIN", "USRId": hcueDoctorLoginService.getLoginId(), "profileImage": true };
        hcueDoctorProfileService.getDocProfImgList(params, onClinicLogoSuccess, onClinicLogoError);
    }
    function onClinicLogoSuccess(data) {
        var HospitalDocuments = [];
        var HospitalImage = [];
        angular.forEach(data.doctorAddress, function (k) {
            if (k.ExtDetails && k.ExtDetails.HospitalInfo && k.ExtDetails.HospitalInfo.HospitalDocuments && k.ExtDetails.HospitalID == $scope.HospitalID) {
                angular.forEach(k.ExtDetails.HospitalInfo.HospitalDocuments, function (j) {
                    HospitalDocuments.push(j);
                    if (j.FileName != 'Logo') {
                        HospitalImage.push(j);
                    }
                })
            }
        });

        angular.forEach(doctorinfo.doctordata.doctorAddress, function (k) {
            if (k.ExtDetails && k.ExtDetails.HospitalInfo && k.ExtDetails.HospitalID == $scope.HospitalID) {
                k.ExtDetails.HospitalInfo.HospitalDocuments = HospitalDocuments;
            }
        });
        hcueDoctorLoginService.setDoctorInfo(doctorinfo);
        if (HospitalImage.length == 0) {
            $scope.clinicImagepopup = false;
        } else {
            $scope.imgURL = HospitalImage[0].FileURL;
            $scope.clinicImgLength = HospitalImage.length;
        }
        $scope.currentPage = 0;
        $scope.pageSize = 6;
		$rootScope.$broadcast('CallDropdownForClinicChange');

    }
    function onClinicLogoError(data) {

    }
    // $scope.openClinicImage = function (img,type,lng) {
        // $scope.clinicImagepopup = true;
        // $scope.imgType = type;
        // $scope.imgURL = img;
        // if (lng != undefined) {
            // $scope.clinicImgLength = lng;
            // $scope.currentPage = 0;
            // $scope.pageSize = 6;
        // }
    // }
    // $scope.closeclinicImagePopup = function () {
        // $scope.clinicImagepopup = false;
    // }

		      $scope.showimages = false;
     $scope.openClinicImage = function(profileimage) {
		 var newImages = profileimage;
         $scope.showimages = true;
         var images = [];
         // angular.forEach($scope.ImageDocumentArray, function (value) {           
             images.push(newImages);
         // });

         
         var curImageIdx = 1,
             total = images.length;
         var wrapper = $('#image-gallerys'),
             curSpan = wrapper.find('.currents');
         var viewer = ImageViewer(wrapper.find('.image-containers'));

         //display total count
         wrapper.find('.totals').html(total);

         function showImage() {
             var imgObj = images[curImageIdx - 1];
             viewer.load(imgObj, imgObj);
             curSpan.html(curImageIdx);
         }

         wrapper.find('.nexts').click(function () {
             curImageIdx++;
             if (curImageIdx > total)
                 curImageIdx = 1;
             showImage();
         });

         wrapper.find('.prevs').click(function () {
             curImageIdx--;
             if (curImageIdx < 1)
                 curImageIdx = total;
             showImage();
         });

         //initially show image
         setTimeout(function () {
             showImage();
         }, 2);
         
     };

     $scope.closeimages = function () {
         $scope.showimages = false;
     }
	  $scope.showimagess = false;
     $scope.openClinicImages = function(index,imageLength) {
         		 
         $scope.showimagess = true;
         var images = [];
         angular.forEach(imageLength, function (value) {     
				 
             images.push(value.FileURL);
         });

         index++
         var curImageIdx = index,
             total = images.length;
         var wrapper = $('#image-galleryss'),
             curSpan = wrapper.find('.currentss');
         var viewer = ImageViewer(wrapper.find('.image-containerss'));

         //display total count
         wrapper.find('.totalss').html(total);

         function showImage() {
             var imgObj = images[curImageIdx - 1];
             viewer.load(imgObj, imgObj);
             curSpan.html(curImageIdx);
         }

         wrapper.find('.nextss').click(function () {
             curImageIdx++;
             if (curImageIdx > total)
                 curImageIdx = 1;
             showImage();
         });

         wrapper.find('.prevss').click(function () {
             curImageIdx--;
             if (curImageIdx < 1)
                 curImageIdx = total;
             showImage();
         });

         //initially show image
         setTimeout(function () {
             showImage();
         }, 2);
         
     };

     $scope.closeimagess = function () {
         $scope.showimagess = false;
     }
	
    $scope.deleteClinicLogoImage = function (value) {
        alertify.confirm("<b class='txt-centre'>Confirm !</b><h4 class='txt-body-centre'>Are you sure want to delete</h4> ", function () {
            var params = { "HospitalID": $scope.HospitalID, "ImageName": value.DocumentNumber };
            hcueDoctorProfileService.deleteHospitalImage(params, onLogoDeleteSuccess, onLogoDeleteError);
        },
	    	function () {

	    	});
        
    }

    function onLogoDeleteSuccess(data) {
        $scope.clinicLogoImgList();
    }
    function onLogoDeleteError(data) {

    }

    // $scope.ViewallImage = function (imageLength) {
        // $scope.clinicImagepopup = true;
		// $scope.imgType = 'Clinic';
        // if (imageLength != undefined) {
            // $scope.imgURL = imageLength[0].FileURL;
            // $scope.clinicImgLength = imageLength.length;
            // $scope.currentPage = 0;
            // $scope.pageSize = 6;
        // }
    // }

    angular.forEach(doctorinfo.doctordata.doctorAddress, function (i) {
        if (i.AddressID == $scope.addressidparams) {
            $scope.HospitalID = i.ExtDetails.HospitalID;
        }
    })

    $scope.currentPage = 0;
    $scope.pageSize = 6;
}]);