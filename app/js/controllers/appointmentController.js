﻿hCueDoctorWebApp.lazyController('hcueAppointmentController', function ($scope, $rootScope, $location,
    $uibModal, $filter, $timeout, hCueAppointmentService, hcueDoctorLoginService,
    hcueAuthenticationService, hcueLoginStatusService, datacookieFactory, $window, hcueConsultationInfo,
    datapersistanceFactory, hcuePatientCaseService, hcueDoctorAppointmentService, alertify,
    uiCalendarConfig, c7AppointmentListServices, c7AdminServices, c7EnquiriesServices) {
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    var access = doctorinfo.isAccessRightsApplicable
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    $scope.showEnquiryAccess = !access;
    $scope.showApplstAccess = !access;
    $scope.showCalenderAccess = !access;
    $scope.showReferralAccess = !access;
    $scope.showListviewAccess = !access;

    if (access) {
        if (doctorinfo.AccountAccessID.indexOf('MNREFERRALS') !== -1) {
            $scope.showReferralAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('MNGENQUIRY') !== -1) {
            $scope.showEnquiryAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('APPLSTVIEW') !== -1) {
            $scope.showApplstAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('CALENDER') !== -1) {
            $scope.showCalenderAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('LISTVIEW') !== -1) {
            $scope.showListviewAccess = true;
        }

    }
    $scope.activeEvent = 'Service';

    if ($scope.showEnquiryAccess == true) {
        $scope.activeEvent = 'Service';
    } else if ($scope.showReferralAccess == true) {
        $scope.activeEvent = 'Referrals';
    } else if ($scope.showApplstAccess == true) {
        $scope.activeEvent = 'list';
    } else if ($scope.showCalenderAccess == true) {
        $scope.activeEvent = 'day';
        setTimeout(function () {
            $scope.loadCalenderEvent('day');
        }, 500);
    } else if ($scope.showListviewAccess == true) {
        $scope.activeEvent = 'listview';
    }

    $scope.showbook = true;
    var that = this;
    var calCurrTime = '';
    var docMinPerCase = 15;

    $scope.onlineval = false;
    $scope.referredappointmentlist = [];
    $scope.query = { "value": "", "value1": "" };
    $scope.pastivfhistory = [];
    hcueDoctorLoginService.addivfpatienthistory($scope.pastivfhistory);
    hcueDoctorLoginService.Addwalkinviewcase(true);

    $scope.addresIDs = [];
    $scope.patDetails = [];
    $scope.paramValueColl = [];
    $scope.eventsList = [];
    $scope.selectedDoctor = {};
    $scope.ageSplits = { year: 0, months: '0' };
    $scope.appCount = {
        bookedCount: 0,
        checkedInCount: 0,
        completedCount: 0,
        startedCount: 0,
        eventsCount: 0
    };
    $scope.emergencyslotstarttime = null;
    $scope.emergencyslotendtime = null;
    docname = "";
    docSpecialization = "";
    $scope.appointment_type = "OTHERS";
    $scope.showEditAppointmentID = true;
    $scope.loginUserType = '';
    $scope.disableClinic = false;
    $scope.appointmentDocName = '';
    $scope.showicon = false;
    var admin_status = false;
    var qtipDown = false;
    var clinicAddress = [];
    $scope.clearScanCenter = true;

    $scope.adminHospitalList = [];
    var Selectedfrpastdate = $filter('date')(new Date(), "dd-MM-yyyy"); // changing the format for pastappointment date
    hcueDoctorLoginService.addSelectedPastAppDate(Selectedfrpastdate);
    var PastHealthHistoryRecord = [];
    hcueDoctorLoginService.addPreMedicalHistory(PastHealthHistoryRecord);

    var consultinfo = angular.copy(hcueConsultationInfo.getNewConsultInfoStructure());
    consultinfo.vitalnoteinfo.vitals = { TMP: '', HGT: '', WGT: '', SPP: '', SFT: '', BPL: '', BPH: '', BLG: '' };

    var doctorInfo = hcueDoctorLoginService.getDoctorInfo();
    var parentHospitalId;
    if (doctorInfo.doctordata.doctorAddress !== undefined && doctorInfo.doctordata.doctorAddress[0].ExtDetails !== undefined) {
        $scope.Role = doctorInfo.doctordata.doctorAddress[0].ExtDetails.RoleID !== undefined ? doctorInfo.doctordata.doctorAddress[0].ExtDetails.RoleID : undefined;
        $scope.newHospitalID = doctorInfo.doctordata.doctorAddress[0].ExtDetails.HospitalID != undefined ? doctorInfo.doctordata.doctorAddress[0].ExtDetails.HospitalID : undefined;
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
    }

    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
    hcueDoctorLoginService.addBillingInfo('');
    // var chk = [];
    // chk = userLoginDetails;
    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === "true") {
        $scope.setHeaderShouldShow(true);
    } else {
        $location.path('/login');
    }
    var onchageselectappoint = 0;
    setCalenderViewVisibility(false);
    $scope.selectedDocCSSid = 0; // to know selected doctor.
    $scope.allDoctorAllowed = true;
    //load doctor list
    var doctorInfo = hcueDoctorLoginService.getDoctorInfo();
    $scope.isAccessRightsApplicable = doctorInfo.isAccessRightsApplicable;
    if (doctorInfo.isAccessRightsApplicable) {
        if (doctorInfo.AccountAccessID.indexOf('APPLSTVIEW') !== -1) {
            $scope.allDoctorAllowed = true;
        }
    }

    $scope.hospitalcd = "";
    $scope.hospid = "";
    if (doctorInfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined &&
        doctorInfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
        $scope.hospitalcd = doctorInfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        $scope.hospid = doctorInfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
    }

    $scope.admintype = doctorInfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    $scope.clinicDoctorList = [];
    $scope.clinicDoctorCalendar = [];

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var sliceTime = "15";
    $scope.eventSource = {};
    $scope.eventsList = [];
    calCurrTime = "15:00:00";

    $scope.pickerShow = false;
    $scope.calendarDate = new Date();
    $scope.minPerCase = 15;
    $scope.calendarView = 'day';
    $scope.redirectToPatientadd = false;
    $scope.isCellOpen = true;
    var jsonUnavailableTime = [];
    $scope.fullCollectionBillin  = [];
    $scope.HospitalChange = false;
    $scope.appScreen = "list";
    $scope.refappwithcolors = {};
    $scope.refsuccessvalchange = {};
    $scope.collectionsuccessclr = [];
    $scope.collectionrefclr = [];
    $scope.offlineID = 0;
    $scope.items = [];
    $scope.animationsEnabled = true;
    var sdate = "";
    $scope.disabled = true;

    $scope.appointaddressId = false;
    $scope.appointmentlistID = true;
    $scope.showDayID = false;
    $scope.showWeekID = false;
    $scope.showCalendarID = true;
    $scope.items = [];
    $scope.animationsEnabled = true;
    var addressconsultids = GetAllAddressConsultIDByDayCode(currentdaycode);
    if (JSON.parse(localStorage.getItem("isAppOnline"))) {
        $scope.appMode = "Go Offline";
    } else {
        $scope.appMode = "Go Online";
    }

    $scope.Regpatient = function (data) {
        datacookieFactory.remove('editPatientId');
        datacookieFactory.remove('editPatientEnquiryId');
        var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
        var url = "/addUpdatePatient?kwid=" + (guid || "");
        $location.url(url);
    }
    $scope.itemCountTimer = undefined;
    $scope.itemCount = 0;
    $scope.itemCountTimer = setInterval(function () {
        getCount();
    }, 300000);

    function getCount() {
        var url = $location.absUrl();
        if (url.indexOf('appointment') !== -1) {
            var countparams = {
                HospitalCode: $scope.hospitalcd,
                pagenumber: 0,
                pagesize: 0
            }
            c7EnquiriesServices.Referralcount(countparams, countSuccess, error);

            function countSuccess(data) {
                $scope.itemCount = data[0].referalcount;
            }

            function error() {
                alertify.error('Referral count error');
            }
        }
    }
    getCount();
    //end of login condition check

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.eventsList.length; i++) {
                //var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    //return $scope.events[i].status;
                }
            }
        }

        return '';
    }

    function getTimeSplit(time) {
        var t = time.split(':');
        dur = {};
        dur['hr'] = parseInt(t[0]);
        dur['min'] = parseInt(t[1].substr(0, 2));
        dur['ampm'] = t[1].substr(2);
        return dur;
    }

    function getFormattedDate(currdt) {

        var month = new Array(12);
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        return currdt.getDate() < 10 ? "0" + currdt.getDate() : currdt.getDate() + "-" + month[currdt.getMonth()] + "-" + currdt.getFullYear();
    }

    function getSpecialityDesc(splCode) {

        var spl = "";
        angular.forEach(doctorinfo.specialitylist, function (item) {
            if (item.DoctorSpecialityID == splCode) {
                spl = item.DoctorSpecialityDesc;
                return;
            }
        });
        return spl;
    }


    function setCalenderViewVisibility(val) {
        if (val)
            document.getElementById('divCalenderView').setAttribute('style', 'visibility:visible;display:block;opacity:1;');
        else
            document.getElementById('divCalenderView').setAttribute('style', 'visibility:hidden;display:none;opacity:0;');
    }
    $scope.set_color = function () {
        var value = '#' + Math.floor(Math.random() * 16777215).toString(16);
        return {
            background: value
        }
    }

    $scope.today = function () {
        $scope.referredappointmentdate = $filter('date')(new Date(), "dd-MM-yyyy");
        $scope.viewDate = moment();
        $scope.calendarView = "day";
        $scope.changeView('agendaDay');
        $scope.activeEvent = "day";
        $scope.getListingDetailsonDateChange($scope.referredappointmentdate, 'calendar');
    };


    $scope.greaterThan = function (prop, val) {
        return function (item) {
            if (val == 0) {
                return item[prop];

            } else {
                return item[prop] > val;
            }

        }
    }

    $scope.loadDayView = function (dateValue, docObject) {
        $scope.appointmentCountDisplay = 0;
        var DoctorID = 0;
        var formatteddateValue;

        if($scope.clearScanCenter){
            localStorage.removeItem('selectedscancenter');
        }

        if (typeof dateValue == 'object') {
            formatteddateValue = moment(dateValue).format('YYYY-MM-DD');
        }
        if (typeof dateValue == 'string') {
            formatteddateValue = moment(dateValue, 'DD-MM-YYYY').format('YYYY-MM-DD');
            dateValue = moment(dateValue, 'DD-MM-YYYY');
        }
        if (uiCalendarConfig.calendars["calControl"])
            uiCalendarConfig.calendars["calControl"].fullCalendar('gotoDate', moment(dateValue));

        $scope.noData = "";
        // if (JSON.stringify($scope.showDayID) === "true") {
        //     $scope.dayConfig.visible = true;
        // }
        // if ($scope.selectedDocCSSid == 0) {
        //     DoctorID = hcueDoctorLoginService.getLoginId();
        // } else {
        //     DoctorID = $scope.selectedDocCSSid
        // }

        if (docObject !== undefined && $scope.isAccessRightsApplicable && $scope.allDoctorAllowed) {
            DoctorID = docObject.DoctorID;
            // $scope.selectedDocCSSid = DoctorID;
            onchageselectappoint = docObject.AddressID;
        }
        // if($scope.allDoctorAllowed && $scope.selectedDoctor.AddressID==0){
        //     DoctorID=0;
        // }

        if (dateValue == "") {
            var datefinalValue = $scope.getCurrentDate();
        } else {
            var datefinalValue = formatteddateValue
        }

        hcueDoctorLoginService.addSelectedAppointDt(datefinalValue);
        var data_param = {
            'doctorID': DoctorID,
            'usrId': hcueDoctorLoginService.getLoginId(),
            'usrType': "DOCTOR",
            'startDate': datefinalValue, //datefinalValue,
            'endDate': datefinalValue,
            'addressID': onchageselectappoint, //88
            'hospitalID': that.AppListselectedclinic.ExtDetails.HospitalID
        };
        $scope.getDcotorAppointmentDtlsDt = datefinalValue;
        $scope.patDetails.length = 0;

        c7AppointmentListServices.getDoctorAppointments(data_param, OnGetAppointmentSuccess, OnGetAppointmentError);
        // function OnGetAppointmentSuccess(data) {
        //    if(data.status == "failure"){
        //     alertify.error('No Schedule for this Location');
        //    }
        //    else{
        //     $scope.addressconsultid = data.calendarList[0].addressconsultid;
        //     window.localStorage.setItem('addressGetData',  $scope.addressconsultid);
        //    }
        
        // }

        // function OnGetAppointmentError() {
        //     //alertify.error('Referral count error');
        // }

        // var param = {
        //     "AddressID": onchageselectappoint,
        //     "HospitalID":that.AppListselectedclinic.ExtDetails.HospitalID,
        //     "ScheduleDate": datefinalValue
        // }
        // c7AppointmentListServices.listHospitalAppointments(param, OnGetAppointmentSuccess, OnGetAppointmentError);


    }


    function getDoctorName(screen) {
        var value = "";
        if ($scope.isAccessRightsApplicable && $scope.allDoctorAllowed && $scope.selectedDoctor != undefined && $scope.selectedDoctor.DoctorID != 0) {
            value = $scope.selectedDoctor.FullName;
        } else {
            value = doctorInfo.doctordata.doctor[0].FullName;
        }
        return value;
    }

    function getDoctorSpecialities(screen) {
        var value = "";
        if ($scope.isAccessRightsApplicable && $scope.allDoctorAllowed) {
            var speciality = {};
            // if(screen=="calendar"){
            //     speciality=$scope.selectedDoctorCalendar.SpecialityCD;
            // }else{
            speciality = $scope.selectedDoctor.SpecialityCD;
            //}
            for (var i in speciality) {
                value += ", " + getSpecialityDesc(speciality[i]);
            }
        } else {
            var speciality = doctorinfo.doctordata.SpecilityDetails;
            for (var i in speciality) {
                value += ", " + speciality[i].SepcialityDesc;
            }
        }
        if (value.charAt(0) == ",") {
            value = value.slice(2);
        }
        return angular.copy(value);
    }

    $scope.loadCalenderEvent = function (evt) {
        $scope.SearchBy = "";
        if (evt == 'week' || evt == 'month') {
            $scope.onlineval = $scope.checkOnline();
            if ($scope.onlineval == true) {
                return;
            }
        }
        // datacookieFactory.remove('selectedscancenter');
        window.localStorage.removeItem('selectedscancenter');

        $scope.hidescreen = false;
        $scope.hideregister = false;
        $scope.activeTab = evt;
        $scope.activeEvent = evt;
        $scope.calendarView = evt;
        $scope.renderCalendar('calControl');
        // if(evt=="day"||evt=="week"||evt=="month"){
        //     //that.AppListselectedclinic=$scope.AppListdoctoraddress[0];
        //     setFirstDoctor('calendar');
        // }else{
        //     //that.AppListselectedclinic=$scope.AppListdoctoraddress[0];
        //     setFirstDoctor('list');
        // }
        if (evt === 'day') {
            setCalenderViewVisibility(true);
            $scope.calendarView = evt;
            $scope.changeView('agendaDay');
            $scope.appScreen = "calendar";
            //$scope.selectedDoctorCalendar=selectedDr;
            //$scope.events.length = 0;
            //if($scope.loginUserType == 'Receptionist' || $scope.loginUserType == 'Administrator'){
            $scope.loadDayView($scope.viewDate, $scope.selectedDoctor);
            $scope.initialiseClinicDoctor();
            $scope.today();

        } else if (evt === 'week') {
            $scope.onlineval = $scope.checkOnline();
            $scope.appScreen = "calendar";
            //$scope.selectedDoctorCalendar=selectedDr;
            if ($scope.onlineval == true) {
                return;
            } else {
                $scope.changeView('agendaWeek');
                setCalenderViewVisibility(true);
                $scope.calendarView = evt;
                loadWeekView();
            }

        } else if (evt === 'month') {
            $scope.onlineval = $scope.checkOnline();
            $scope.appScreen = "calendar";
            //$scope.selectedDoctorCalendar=selectedDr;
            if ($scope.onlineval == true) {
                return;
            } else {
                $scope.changeView('month');
                setCalenderViewVisibility(true);
                $scope.calendarView = evt;
                loadMonthView()
            }

        } else if (evt == 'offlineBilling') {
            //$scope.selectedDoctor=$scope.selectedDoctorCalendar;
            $scope.hidescreen = true;
            $scope.appScreen = "list";
            // $scope.showOfflineBiling();
        } else if (evt == 'manage') {
            //$scope.selectedDoctor=$scope.selectedDoctorCalendar;
            $scope.hidescreen = true;
            $scope.appScreen = "list";
            $scope.showManageList();
        } else if (evt == 'Service') {
            //$scope.selectedDoctor=$scope.selectedDoctorCalendar;
            $scope.$broadcast("EnquiryTabClick", 'enquiry');
            $scope.hidescreen = true;
            $scope.hideregister = true;

        } else if (evt == 'listview') {
            //$scope.selectedDoctor=$scope.selectedDoctorCalendar;
            $scope.$broadcast("EnquiryTabClick", 'listview');
            $scope.hidescreen = true;
            $scope.hideregister = true;

        } else if (evt == 'Referrals') {
            $scope.$broadcast("EnquiryTabClick", 'Referrals');
            $scope.hidescreen = true;
            $scope.hideregister = true;
        } else {
            //$scope.selectedDoctor=$scope.selectedDoctorCalendar;
            $scope.appScreen = "list";
            $scope.$broadcast("EnquiryTabClick", 'appointment');
            $scope.showAppointmentList();
        }

    };



    $scope.timespanClicked = function (date) {
        //check the slot
        date.action = "book";
        date.doctorName = getDoctorName('calendar');
        date.doctorSpecialities = getDoctorSpecialities('calendar');
        var doctorinfoaccess = hcueDoctorLoginService.getDoctorInfo();
        date.doctorImage = "";
        // var access = doctorinfoaccess.isAccessRightsApplicable;
        // if (access) {
        //     if (doctorinfoaccess.AccountAccessID.indexOf('BKAPPTMENT') == -1) {
        //         alertify.log("You do not have access to view this feature, Kindly contact your Administrator");
        //         return;
        //     }
        // }

        var formatteddateValue = moment(date.date).format('HH:mm');
        var stMinutes = moment(date.minuteStart, 'HH:mm:ss');
        var che_time = moment(date.date)
            .clone()
            .hours(stMinutes.hours())
            .minutes(stMinutes.minutes())
            .seconds(stMinutes.seconds());
        //var che_time = moment(date.date).format('YYYY-MM-DDTHH:MM:SS');
        $scope.dateStart = moment(date.date).format('YYYY-MM-DD');

        var newEvent = {};
        eventPopUp(date);
        //}
        //end of check the slot

        // }
    };
    $scope.eventClicked = function (event, action = "") {
        var doctorinfoaccess = hcueDoctorLoginService.getDoctorInfo();
        var access = doctorinfoaccess.isAccessRightsApplicable;
        if (access) {
            if (doctorinfoaccess.AccountAccessID.indexOf('BKAPPTMENT') == -1) {
                alertify.log("You do not have access to view this feature, Kindly contact your Administrator");
                return;
            }
        }
        var tmpEvent = event;
        switch (tmpEvent.rowData.PatientInfo.appointmentStatus) {
            case "B":
                EditeventPopUp(event, action);
                break;
            case "EVENT":
                EditeventPopUp(event, action);
                break;
            case "IMPEVENT":
                EditeventPopUp(event, action);
                break;
            case "S":
                EditeventPopUp(event, action);
                //rebookEvent(event,{},{});
                break;
            case "I":
                if (action == "cancel") { EditeventPopUp(event, action); } else { rebookEvent(event, {}, {}); }
                break;
            case "E":
                rebookEvent(event, {}, {});
                break;
            default:
                alertify.log("Sorry, this Event cannot be edited");
                break;
        }

        //eventPopUp(event);
    };
    $scope.viewChangeClicked = function (date, nextView) {
        $scope.calendarView = nextView;
        $scope.activeEvent = nextView;

        $scope.viewDate = date;
        $scope.referredappointmentdate = date;
    };

    function eventPopUp(event) {
        $scope.sendItem = event;
        if($scope.showbook  == false){
            return;
        }
        //$scope.redirectToPatientadd = false;
        //hcueDoctorLoginService.addredirectToPatientaddPage = false;
        // event.date = moment(event.date).toString();
        hcueDoctorLoginService.addAddPatientwithoutConsult(false);
        hcueDoctorLoginService.addcalenderInfo(event);
        $scope.mInstanceAdd = $uibModal.open({
            templateUrl: 'modalContent.html',
            controller: function ($scope, $uibModal) {
                $scope.ok = function () {
                    $scope.mInstanceAdd.dismiss('cancel');
                };

                $scope.cancel = function () {
                    $scope.mInstanceAdd.dismiss('cancel');
                };
            } //,
            //controllerAs: 'hcueAppointmentController'
        });

        var cleanCallAppointmentCloseMdl = $rootScope.$on('CallAppointmentCloseMdl', function (event) {
            $scope.mInstanceAdd.dismiss('cancel');
            //$scope.loadDayView($scope.viewDate);
            //initialiseClinicList();
            if ($scope.isAccessRightsApplicable && $scope.allDoctorAllowed) {
                if ($scope.calendarView == "day") {
                    $scope.loadDayView($scope.viewDate, $scope.SelectedAppointmentDr);
                } else if ($scope.calendarView == "week") {
                    loadWeekView();
                } else if ($scope.calendarView == "month") {
                    loadMonthView();
                }

            } else {
                if ($scope.calendarView == "day") {
                    $scope.loadDayView($scope.viewDate);
                } else if ($scope.calendarView == "week") {
                    loadWeekView();
                } else if ($scope.calendarView == "month") {
                    loadMonthView();
                }
                // $scope.loadDayView($scope.viewDate);
            }
            // $scope.GetReferredAppointmentByDoctorID($scope.SelectedAppointmentDr);

            if (hcueDoctorLoginService.getAddPatientwithoutConsult()) {

                var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
                // var url = "/registerPatient?kwid=" + (guid || "");
                var url = "/addUpdatePatient?kwid=" + (guid || "");
                $location.url(url);

            }

        });

        $scope.$on('$destroy', function () {
            cleanCallAppointmentCloseMdl();
        });

    }


    function EditeventPopUp(event, action = "") {
        $scope.sendItem = event;
        event.source = {
            action: action
        };
        event.doctorName = event.rowData.doctorName;
        event.doctorSpecialities = getDoctorSpecialities('calendar');
        event.doctorImage = "";
        event.clinicDoctorList = $scope.clinicDoctorList;
        if (event.allDay && event.startDate != event.endDate && event.end != null) {
            event.endDate = moment(event.endDate).subtract(1, 'days');
            event.end = moment(event.end).subtract(1, 'days');
        }
        hcueDoctorLoginService.addcalenderInfo(event);
        $scope.mInstanceEdit = $uibModal.open({
            templateUrl: 'modalEdit.html',
            controller: function ($scope, $uibModal) {
                $scope.ok = function () {
                    $scope.mInstanceEdit.dismiss('cancel');
                };

                $scope.cancel = function () {
                    $scope.mInstanceEdit.dismiss('cancel');
                };

            }
        });

        $scope.mInstanceEdit.result.then(function (selectedItem) {

        }, function () {
            if ($scope.calendarView == 'day') {
                $scope.loadDayView($scope.viewDate, $scope.selectedDoctor);
            } else if ($scope.calendarView == 'week') {
                loadWeekView();
            } else if ($scope.calendarView == 'month') {
                loadMonthView();
            }
        });
    };



    var cleanCallEditAppointmentCloseMdl = $rootScope.$on('CallEditAppointmentCloseMdl', function (event) {
        //initialiseClinicDoctor();
        $scope.mInstanceEdit.dismiss('cancel');
    });

    $scope.$on('$destroy', function () {
        cleanCallEditAppointmentCloseMdl();
    });



    $scope.toggleCalender = function ($event, field) {
        $event.preventDefault();
        $event.stopPropagation();
    };

    $scope.getCurrentDate = function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        return today = yyyy + '-' + mm + '-' + dd;
    }

    function allDoctorsData(val) {
        $scope.clinicDoctorList = []
        angular.forEach(val, function (i) {
            angular.forEach(i.Doctors, function (j) {
                $scope.clinicDoctorList.push(j);
            })
        })

    }

    $scope.initialiseClinicDoctor = function () {
        $scope.AppListdoctoraddress = [];
        onchageselectappoint = 0;
        $scope.selectedDocCSSid = 0;
        var item = doctorInfo.doctordata.doctorAddress[0];

        if (item.ExtDetails.HospitalID !== undefined) {
            //ADMIN
            $scope.AppListdoctoraddress = clinicAddress;
            that.AppListselectedclinic = clinicAddress[0];
            if ($scope.AppListdoctoraddress[0].ClinicName == 'All') {
                allDoctorsData(angular.copy(clinicAddress))
            } else {
                $scope.clinicDoctorList = angular.copy(clinicAddress[0].Doctors);
            }
            if ($scope.allDoctorAllowed) {
                // var docAll = { DoctorID: 0, FullName: 'All', RoleID: 'SONOGRAPHER', AddressID: 0, MinPerCase:15 };
                // $scope.clinicDoctorList.unshift(docAll);
                $scope.selectedDoctor = $scope.clinicDoctorList[0];
            }
            onchageselectappoint = parseInt($scope.selectedDoctor.AddressID);
            $scope.selectedDocCSSid = $scope.selectedDoctor.DoctorID;
        }

        setCalendarSlot();
        listSelectedClinic = that.AppListselectedclinic;
        calSelectedClinic = that.AppListselectedclinic;
        // setDefaultClinic();
        if ($scope.appScreen == "list") {
            $scope.loadAppointments($scope.selectedDoctor);
        } else {
            if ($scope.calendarView == "day") {
                $scope.loadDayView($scope.viewDate, $scope.selectedDoctor);
            } else if ($scope.calendarView == "week") {
                loadWeekView();
            } else if ($scope.calendarView == "month") {
                loadMonthView();
            }
        }
    }

    $scope.loadDoctor = function (val) {
        // datacookieFactory.set('selectedscancenter', val);
        window.localStorage.setItem('selectedscancenter', JSON.stringify(val));
        $scope.clearScanCenter = false;
        that.AppListselectedclinic = val;
        if (val.ClinicName == 'All') {
            allDoctorsData(angular.copy($scope.AppListdoctoraddress))
        } else {
            $scope.clinicDoctorList = angular.copy(val.Doctors);
        }
        // var docAll = { DoctorID: 0, FullName: 'All', AddressID: 0,MinPerCase:15 };
        // $scope.clinicDoctorList.unshift(docAll);
        $scope.selectedDoctor = $scope.clinicDoctorList[0];
        $scope.loadAppointments($scope.selectedDoctor);
    }

    function GetAllAddressConsultIDByDayCode(dayCode) {
        var addressconsultids = [];
        if (doctorinfo != null && doctorinfo.doctordata.doctorConsultation != null && $scope.selectedclinic != null) {
            angular.forEach(doctorinfo.doctordata.doctorConsultation, function (consDetail) {
                if (dayCode == consDetail.DayCD && $scope.selectedclinic.AddressID == consDetail.AddressID) {
                    addressconsultids.push(consDetail.AddressConsultID);
                }
            });
        }
        return addressconsultids;
    }


    // load doctor appointments based on addressID and DoctorID
    $scope.loadAppointments = function (selectedDr) {
        if ($scope.appScreen == "calendar") {
            $scope.selectedDoctor = selectedDr;
            if (selectedDr != null || selectedDr != undefined) {
                $scope.selectedDocCSSid = selectedDr.DoctorID;
                $scope.selectedDocApp = onchageselectappoint;
                $scope.appointmentDocName = selectedDr.FullName;
                $scope.SelectedAppointmentDr = selectedDr;
                docMinPerCase = selectedDr.MinPerCase;
                $scope.appointmentCountDisplay = selectedDr.AppointmentCount;
                onchageselectappoint = $scope.selectedDoctor.AddressID;
            } else {
                onchageselectappoint = that.AppListselectedclinic.AddressID;
                docMinPerCase = that.AppListselectedclinic.ExtDetails.MinPerCase || 15;

            }
            if ($scope.calendarView == 'day') {
                $scope.loadDayView($scope.viewDate, selectedDr);
            } else if ($scope.calendarView == 'week') {
                loadWeekView();
            } else if ($scope.calendarView == 'month') {
                loadMonthView();
            }
            $scope.selectedDoctorView = getDoctorName('calendar');
            $scope.showDoctorListing = false;
        }
    }

    $scope.getRandomColor = function () {

        var array_list = ['#af77c6', '#e4c120', '#69b3e7'];
        return 'background:' + array_list[Math.floor(Math.random() * array_list.length)];
    };


    $scope.currentdate = moment().format("YYYY-MM-DD");
    //End AppointmentList

    $scope.getListingDetailsonDateChange = function (refDate, screen) {
        $scope.referredappointmentdate = refDate;
        $scope.appScreen = screen;
        $scope.viewDate = refDate;
        hcueDoctorLoginService.addSelectedPastAppDate(refDate);

        $scope.loadAppointments($scope.selectedDoctor);


    }

    $scope.showManageList = function () {
        setCalenderViewVisibility(false);

    }
    $scope.showAppointmentList = function () {
        setCalenderViewVisibility(false);
        $scope.noData = "";
        hcueDoctorLoginService.addSelectedAppointDt($scope.getCurrentDate());
        $scope.appointmentlistID = false;
        $scope.appointaddressId = false;
        $scope.appointmentlistID = true;
        $scope.showEditAppointmentID = true;
        $scope.vitalsID = false;
        $scope.showDayID = false;
        $scope.showWeekID = false;
        $scope.loadAppointments($scope.selectedDoctor);
        //GetDoctorAppointmentListByDate(daycode, addressconsultids, futuredate);
        //$rootScope.$broadcast('CallAppointmentList');
    }


    var cleanCallAppointmentList = $rootScope.$on('CallAppointmentList', function (event) {
        if ($scope.isAccessRightsApplicable && $scope.allDoctorAllowed && $scope.allDoctors == null) {
            //$scope.GetReferredAppointmentByDoctorID();
            $scope.loadAppointments($scope.selectedDoctor);

        } else {
            $scope.GetReferredAppointmentByConsultIds($scope.allDoctors);
        }
    });

    $scope.$on('$destroy', function () {
        cleanCallAppointmentList();
    });




    $scope.Endtimechange = function () {
        if ($scope.patinet_appStarttime != null) {
            var app_startingTime = $scope.patinet_appStarttime.startTime;
            $scope.dffTime = hcueDoctorLoginService.getDocMinPerCaseInfo();
            var eee = new Date(1980, 1, 1, 0, 0, 0);
            eee.setHours(app_startingTime.substring(0, 2), app_startingTime.substring(3, 5), 0);
            eee.setMinutes(eee.getMinutes() + $scope.dffTime);

            var lowm = eee.getMinutes();
            if (lowm < 10) lowm = "0" + lowm;

            sdate = eee.getHours() + ":" + lowm;

            $scope.EndtimeVAlue = sdate;
        }
    }


    $scope.tmpTodayValue = $filter('date')(new Date(), "yyyy-MM-dd");
    $scope.chkTodayDate = function (d1) {
        var tmpd1 = $filter('date')(new Date(d1), "yyyy-MM-dd");
        if (tmpd1 === $scope.tmpTodayValue) {
            return true;
        } else {
            return false;
        }
    };
    $scope.chkFutureDate = function (d1) {
        var tmpd1 = $filter('date')(new Date(d1), "yyyy-MM-dd");
        if (tmpd1 < $scope.tmpTodayValue || tmpd1 === $scope.tmpTodayValue) {
            return true;
        } else {
            return false;
        }
    };

    $scope.showDoctorList = function () {
        $scope.showDoctorListing = true;
    }



    //**New calendar config  functions**//

    /* alert on eventClick */
    $scope.alertOnEventClick = function (date, jsEvent, view, action = "reschedule") {
        api.destroy();
        if (angular.element(jsEvent.target).hasClass('app-cancel')) {
            action = "cancel";
        } else if (angular.element(jsEvent.target).hasClass('app-reschedule')) {
            action = "reschedule";
        }
        $scope.eventClicked(date, action);
    };
    /* Choose Event Action */
    var chooseAction = function (event, delta, revertFunc, jsEvent, ui, view) {
        var tmpEvent = event;
        switch (tmpEvent.rowData.PatientInfo.appointmentStatus) {
            case "B":
                $scope.alertOnEventClick(event, jsEvent, view, "reschedule");
                break;
            case "EVENT":
                $scope.alertOnEventClick(event, jsEvent, view, "reschedule");
                break;
            case "IMPEVENT":
                $scope.alertOnEventClick(event, jsEvent, view, "reschedule");
                break;
            case "S":
                $scope.alertOnEventClick(event, jsEvent, view, "reschedule");
                //rebookEvent(event,jsEvent,view);
                //revertFunc();
                //alertify.log("Sorry, Started Appointment cannot be edited");
                break;
            case "I":
                rebookEvent(event, jsEvent, view);
                revertFunc();
                //alertify.log("Sorry, Checked-In Appointment cannot be edited");
                break;
            case "E":
                rebookEvent(event, jsEvent, view);
                revertFunc();
                //alertify.log("Sorry, Completed Appointment cannot be edited");
                break;
            default:
                revertFunc();
                alertify.log("Sorry, this Event cannot be edited");
                break;
        }
    }
    /* alert on Drop */
    $scope.alertOnDrop = function (event, delta, revertFunc, jsEvent, ui, view) {
        api.destroy();
        chooseAction(event, delta, revertFunc, jsEvent, ui, view);
        //$scope.alertMessage = ('Event Dropped to make dayDelta ' + delta);
        //revertFunc();
    };
    /*alert on Day Click*/
    $scope.alertOnDayClick = function (date, event, view) {
        var isAllDay = false;
        var docinfo = hcueDoctorLoginService.getDoctorInfo();
        if (!date.hasTime() && $scope.calendarView != "month") {
            //All Day
            isAllDay = true;
        }
        var endDate = date.clone();
        var DoctorID = 0;
        var AddressID = onchageselectappoint;
        if ($scope.selectedDocCSSid == 0 && $scope.isAccessRightsApplicable && $scope.allDoctorAllowed && $scope.clinicDoctorList.length > 0) {
            DoctorID = $scope.clinicDoctorList[1].DoctorID;
            AddressID = $scope.clinicDoctorList[1].AddressID;
        } else if ($scope.selectedDocCSSid == 0) {
            DoctorID = hcueDoctorLoginService.getLoginId();
        } else {
            DoctorID = $scope.selectedDocCSSid
        }
        if (AddressID == 0 && docinfo.doctordata.doctorAddress != undefined) {
            AddressID = docinfo.doctordata.doctorAddress[0].AddressID;
        }
        var obj = {
            "label": date.format('hh:mmA'),
            "date": date,
            "minuteStart": date.format('HH:mm:ss'),
            //"clinicName": "Hcue Admin Clinic",
            //"addressConsultId": 23788,
            "addressId": AddressID,
            "doctorId": DoctorID,
            "consultationDate": date.valueOf(),
            "DayCD": "FRI",
            "sliceTime": docMinPerCase,
            "TimeSlotDetail": {
                "StartTime": date.format('HH:mm'),
                "EndTime": endDate.add(docMinPerCase, 'minutes').format('HH:mm'),
                "Available": "Y",
                "TokenNumber": 0,
                "PatientInfo": {},
                "isAllDay": isAllDay
            },
            "hospitalId": that.AppListselectedclinic.ExtDetails.HospitalID,
            "clinicDoctorList": $scope.clinicDoctorList,
            "showEventDoctors": $scope.allDoctorAllowed && $scope.isAccessRightsApplicable,
            "showAppointmentDoctors": true,
            "emergencyslotstarttime" : $scope.emergencyslotstarttime ,
            "emergencyslotendtime" : $scope.emergencyslotendtime,
            "minpercaseslot": $scope.minpercaseslot
        }
        $scope.timespanClicked(obj);
    }
    /* alert on Resize */
    $scope.alertOnResize = function (event, delta, revertFunc, jsEvent, ui, view) {
        //$scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
        api.destroy();
        chooseAction(event, delta, revertFunc, jsEvent, ui, view);

    };
    /* add and removes an event source of choice */
    $scope.addRemoveEventSource = function (sources, source) {
        var canAdd = 0;
        angular.forEach(sources, function (value, key) {
            if (sources[key] === source) {
                sources.splice(key, 1);
                canAdd = 1;
            }
        });
        if (canAdd === 0) {
            sources.push(source);
        }
    };

    /* Change View */
    $scope.changeView = function (view) {
        uiCalendarConfig.calendars["calControl"].fullCalendar('changeView', view);
    };
    /* Change View */
    $scope.renderCalendar = function (calendar) {
        $timeout(function () {
            if (uiCalendarConfig.calendars[calendar]) {
                uiCalendarConfig.calendars[calendar].fullCalendar('render');
            }
        });
    };
    /* Render Tooltip */

    $scope.eventRender = function (event, element, view) {
        var patInfo = event.rowData.PatientInfo;
        var profileImg = (patInfo.profileImage == undefined || patInfo.profileImage == "") ? "images/default.png" : patInfo.profileImage;
        var patientName = ((patInfo.title == undefined || patInfo.title == "") ? "" : (patInfo.title + ". ")) + patInfo.fullName;
        var patientAge = patInfo.currentAge != undefined ? (patInfo.currentAge.year + "y " + (patInfo.currentAge.month != "0" ? (patInfo.currentAge.month + "m") : "")) : "";
        var patientMobile = (patInfo.mobileNumber == 0) ? "" : patInfo.mobileNumber;
        var patientSex = (patInfo.sex == "M" ? "Male" : "Female");
        var patientID = (doctorInfo.doctordata.doctorAddress[0].ExtDetails != undefined && doctorInfo.doctordata.doctorAddress[0].ExtDetails.HospitalID == undefined) ? "" : patInfo.patDisplayID;
        var patID = "";
        if (patientID == "") { patID = "hide"; }
        var consultDate = $filter('date')(new Date(event.rowData.consultationDate), "dd-MM-yyyy");
        var start = $filter('date')(new Date(event.startTime), "HH:mm");
        var end = $filter('date')(new Date(event.endTime), "HH:mm");
        var token = event.token;
        if ($scope.allDoctorAllowed && $scope.isAccessRightsApplicable) {
            if ($scope.drnameshow) {
                var doctorName = (event.rowData.doctorName) || "";
            } else {
                var doctorName = ("Dr. " + event.rowData.doctorName) || "";
            }
        } else {
            var doctorName = "";
        }
        var doctorSpecialities = event.rowData.doctorSpecialities || "";
        if (end == "23:59") { end = "24:00"; }
        event.source = "";
        //var popover = '<div class="appointment-details" style="width:430px;height:130px;position:static"><div class="clearfix"><div class="app-pat-info"><img src="' + profileImg + '"><div class="pat-other-info"><h5>' + patientName + '</h5><p>' + patientAge + ' / ' + patientSex + '</p><span>' + patientMobile + '</span><div class="pat-id">Patient ID: ' + patientID + '</div></div></div><div class="pat-date-time"><h6>' + consultDate + '</h6><span>' + start + ' to ' + end + ' Hrs</span></div></div><div class="app-pat-action"><div style="display:none" id="eventData">' + JSON.stringify(event) + '</div><button class="btn-cancel-app app-cancel">CANCEL APPOINTMENT</button><button class="btn-cancel-app app-reschedule">RESCHEDULE APPOINTMENT</button></div></div>';
        var popover = "";
        if (patInfo.appointmentStatus == "EVENT" || patInfo.appointmentStatus == "IMPEVENT") {
            //popover='<div class="appointment-popover-section"> <div class="row appointment-popover-header"> <div class="col-md-7"> <div class="popover-image"> <img src="' + profileImg + '" alt=""> </div> <div class="popover-details"> <h3>'+patientName+'</h3> <p>'+(patInfo.doctorSpecialities||"")+' </p> <p>'+event.rowData.doctorSpecialities+'</p> </div> </div> <div class="col-md-5"> <div class="popover-timming"> <p>'+consultDate+'</p> <p>'+start+' Hrs to '+end+' Hrs</p> </div> </div> <div style="display:none" id="eventData">' + JSON.stringify(event) + '</div> </div> <div class="row popover-button"> <div class="col-md-6 padding-right-0"> <button class="btn-cancel-app app-cancel">Cancel Event</button> </div> <div class="col-md-6 reschedule-button"> <button class="btn-cancel-app app-reschedule">Reschedule Event</button> </div> </div> </div>';
            if (patientName == "Dr. ") { patientName = "All Doctors" }
            popover = '<div class="appointment-popover-section"> <div class="row appointment-popover-header"> <div class="col-md-7"> <div class="popover-image"> <img src="' + profileImg + '" alt=""> </div> <div class="popover-details"> <h3 style="margin-top:20px">' + patientName + '</h3> </div> </div> <div class="col-md-5"> <div class="popover-timming"> <p>' + consultDate + '</p> <p>' + start + ' Hrs to ' + end + ' Hrs</p> </div> </div> <div style="display:none" id="eventData">' + JSON.stringify(event) + '</div> </div> <div class="row popover-button"> <div class="col-md-6 padding-right-0"> <button class="btn-cancel-app app-cancel">Cancel Event</button> </div> <div class="col-md-6 reschedule-button"> <button class="btn-cancel-app app-reschedule">Reschedule Event</button> </div> </div> </div>';
        } else if (patInfo.appointmentStatus == "S") {
            //popover='<div class="appointment-popover-section"> <div class="row appointment-popover-header"> <div class="col-md-7"> <div class="popover-image"> <img src="' + profileImg + '" alt=""> </div> <div class="popover-details"> <h3>'+patientName+'</h3> <p>'+patientSex+' - '+patientAge+'</p> <p>'+ patientMobile+'</p> <p class='+patID+'>Patient ID: '+patientID+'</p><p>Token: '+token+'</p> </div> </div> <div class="col-md-5"> <div class="popover-timming"><p>'+doctorName+'</p> <p>'+consultDate+'</p> <p>'+start+' Hrs to '+end+' Hrs</p><p class="app-status-started">Started</p> </div> </div> <div style="display:none" id="eventData">' + JSON.stringify(event) + '</div> </div> <div class="row popover-button"> <div class="col-md-6 padding-right-0"> <button class="btn-cancel-app app-cancel no-action">Cancel Appointment</button> </div> <div class="col-md-6 reschedule-button"> <button class="btn-cancel-app app-reschedule">Rebook Appointment</button> </div> </div> </div>';
            popover = '<div class="appointment-popover-section"> <div class="row appointment-popover-header"> <div class="col-md-7"> <div class="popover-image"> <img src="' + profileImg + '" alt=""> </div> <div class="popover-details"> <h3>' + patientName + '</h3> <p>' + patientSex + ' - ' + patientAge + '</p> <p>' + patientMobile + '</p> <p class=' + patID + '>Patient ID: ' + patientID + '</p> </div> </div> <div class="col-md-5"> <div class="popover-timming"><p>' + doctorName + '</p> <p>' + consultDate + '</p> <p>' + start + ' Hrs to ' + end + ' Hrs</p><p class="app-status-started">Started</p> </div> </div> <div style="display:none" id="eventData">' + JSON.stringify(event) + '</div> </div> </div>';
            //  <div class="row popover-button"> <div class="col-md-6 padding-right-0"> <button class="btn-cancel-app app-cancel no-action">Cancel Appointment</button> </div> <div class="col-md-6 reschedule-button"> <button class="btn-cancel-app app-reschedule">Reschedule Appointment</button> </div> </div>
        } else if (patInfo.appointmentStatus == "E") {
            popover = '<div class="appointment-popover-section"> <div class="row appointment-popover-header"> <div class="col-md-7"> <div class="popover-image"> <img src="' + profileImg + '" alt=""> </div> <div class="popover-details"> <h3>' + patientName + '</h3> <p>' + patientSex + ' - ' + patientAge + '</p> <p>' + patientMobile + '</p> <p class=' + patID + '>Patient ID: ' + patientID + '</p> </div> </div> <div class="col-md-5"> <div class="popover-timming"><p>' + doctorName + '</p> <p>' + consultDate + '</p> <p>' + start + ' Hrs to ' + end + ' Hrs</p><p class="app-status-completed">Completed</p> </div> </div> <div style="display:none" id="eventData">' + JSON.stringify(event) + '</div> </div> </div>';
            //  <div class="row popover-button"> <div class="col-md-6 padding-right-0"> <button class="btn-cancel-app app-cancel no-action">Cancel Appointment</button> </div> <div class="col-md-6 reschedule-button"> <button class="btn-cancel-app app-reschedule">Rebook Appointment</button> </div> </div>
        } else if (patInfo.appointmentStatus == "I") {
            popover = '<div class="appointment-popover-section"> <div class="row appointment-popover-header"> <div class="col-md-7"> <div class="popover-image"> <img src="' + profileImg + '" alt=""> </div> <div class="popover-details"> <h3>' + patientName + '</h3> <p>' + patientSex + ' - ' + patientAge + '</p> <p>' + patientMobile + '</p> <p class=' + patID + '>Patient ID: ' + patientID + '</p> </div> </div> <div class="col-md-5"> <div class="popover-timming"><p>' + doctorName + '</p> <p>' + consultDate + '</p> <p>' + start + ' Hrs to ' + end + ' Hrs</p><p class="app-status-checkin">Checked-In</p> </div> </div> <div style="display:none" id="eventData">' + JSON.stringify(event) + '</div> </div> </div>';
            //  <div class="row popover-button"> <div class="col-md-6 padding-right-0"> <button class="btn-cancel-app app-cancel">Cancel Appointment</button> </div> <div class="col-md-6 reschedule-button"> <button class="btn-cancel-app app-reschedule">Rebook Appointment</button> </div> </div>
        } else {
            popover = '<div class="appointment-popover-section"> <div class="row appointment-popover-header"> <div class="col-md-7"> <div class="popover-image"> <img src="' + profileImg + '" alt=""> </div> <div class="popover-details"> <h3>' + patientName + '</h3> <p>' + patientSex + ' - ' + patientAge + '</p> <p>' + patientMobile + '</p> <p class=' + patID + '>Patient ID: ' + patientID + '</p> </div> </div> <div class="col-md-5"> <div class="popover-timming"><p>' + doctorName + '</p> <p>' + consultDate + '</p> <p>' + start + ' Hrs to ' + end + ' Hrs</p><p class="app-status-booked">Booked</p> </div> </div> <div style="display:none" id="eventData">' + JSON.stringify(event) + '</div> </div> </div>';
            //  <div class="row popover-button"> <div class="col-md-6 padding-right-0"> <button class="btn-cancel-app app-cancel">Cancel Appointment</button> </div> <div class="col-md-6 reschedule-button"> <button class="btn-cancel-app app-reschedule">Reschedule Appointment</button> </div> </div>
        }
        var tooltip = element.qtip({
            content: {
                text: popover
            },
            show: {
                solo: true,
                event: 'mouseenter'
            },
            hide: {
                fixed: true,
                delay: 50,
                event: 'click mousedown mouseleave',
            },
            adjust: {
                scroll: true
            },
            position: {
                my: 'top left',
                at: 'left bottom',
                viewport: $('#calControl'),

            },
            events: {
                mousedown: function (event, api) {
                    qtipDown = true;
                },
                move: function (event, api) {
                    if (qtipDown)
                        event.preventDefault();
                },
                mouseleave: function (event, api) {
                    qtipDown = false;
                },
                render: function (event, api) {
                    angular.element(this).on('click', '.app-cancel', function () {
                        var ele = angular.element(this).closest('.appointment-popover-section').find('#eventData');
                        var obj = JSON.parse(ele.html());
                        if (obj.rowData.PatientInfo.appointmentStatus != "S" && obj.rowData.PatientInfo.appointmentStatus != "E") {
                            $scope.eventClicked(obj, "cancel");
                        } else {
                            alertify.log('Sorry you cannot CANCEL this appointment');
                        }
                    });
                    angular.element(this).on('click', '.app-reschedule', function () {
                        var ele = angular.element(this).closest('.appointment-popover-section').find('#eventData');
                        var obj = JSON.parse(ele.html());
                        if (obj.rowData.PatientInfo.appointmentStatus != "E") {
                            $scope.eventClicked(obj, "reschedule");
                        } else {
                            $scope.eventClicked(obj, "cancel");
                        }
                    });

                }
            },
            style: {
                width: 415,
                tip: false,
                // padding: 5,
                // color: 'black',
                // textAlign: 'left',
                // border: {
                //     width: 1,
                //     radius: 3
                // },
                classes: 'qtip-light qtip-shadow'
            }
        });
        api = tooltip.qtip('api');
    };
    $scope.cancelqtip = function () { }

    $scope.changeLang = function () {
        if ($scope.changeTo === 'Hungarian') {
            $scope.uiConfig.calendar.dayNames = ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"];
            $scope.uiConfig.calendar.dayNamesShort = ["Vas", "Hét", "Kedd", "Sze", "Csüt", "Pén", "Szo"];
            $scope.changeTo = 'English';
        } else {
            $scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            $scope.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
            $scope.changeTo = 'Hungarian';
        }
    };
    /*Set MinPerCase Calendar*/
    function setCalendarSlot() {
        if (that.AppListselectedclinic.ExtDetails != undefined && that.AppListselectedclinic.ExtDetails.MinPerCase != undefined) {
            sliceTime = that.AppListselectedclinic.ExtDetails.MinPerCase;
            docMinPerCase = sliceTime;
            angular.element('#calControl').fullCalendar('option', {
                defaultTimedEventDuration: '00:' + docMinPerCase + ':00',
                slotDuration: '00:' + docMinPerCase + ':00',
                //scrollTime:calCurrTime
            })
            setTimeout(function () { // Focus event
                angular.element('.fc-scroller').scrollTop(0);
                if ($('.fc-now-indicator-line').offset()) {
                    angular.element('.fc-scroller').scrollTop($('.fc-now-indicator-line').offset().top - 300);
                }
            }, 1000);
        }
    }

    //Click to book an appointment text on hover
    angular.element('body').on('mouseenter', '#calControl .fc-agendaDay-view .fc-slats .fc-widget-content', function (event) {
        if (!angular.element(this).hasClass('fc-axis') && ($scope.showbook)) {
            angular.element(this).addClass('fc-time-hover');
        }
    });
    angular.element('body').on('mouseleave', '#calControl .fc-agendaDay-view .fc-slats .fc-widget-content', function (event) {
        angular.element(this).removeClass('fc-time-hover');
    });

    var loadEvents = function (booked, data) {
        for (var j = 0; j < booked.calendarList.length; j++) {
            for (var k = 0; k < booked.calendarList[j].slotList.length; k++) {
                var sTime = booked.calendarList[j].slotList[k];
                if (sTime.appointmentStatus == "EVENT" || sTime.appointmentStatus == "IMPEVENT") {
                    sTime.patientInfo = {};
                }
                if (sTime.patientInfo !== undefined && sTime.patientInfo.appointmentStatus !== 'C') {
                    sTime.patientInfo.appointmentID = sTime.appointmentID;
                    sTime.patientInfo.appointmentStatus = sTime.appointmentStatus;
                    var datefinalValue = $filter('date')(data.calendarList[j].consultationDt, "yyyy-MM-dd");
                    if (sTime.startDate == undefined)
                        var datefinalValue = $filter('date')(data.calendarList[j].consultationDt, "yyyy-MM-dd");
                    else
                        var datefinalValue = $filter('date')(sTime.startDate, "yyyy-MM-dd");
                    if (sTime.endDate == undefined)
                        var dateendval = $filter('date')(data.calendarList[j].consultationDt, "yyyy-MM-dd");
                    else
                        var dateendval = $filter('date')(sTime.endDate, "yyyy-MM-dd");
                    var dayViewStart = moment(sTime.startTime, 'HH:mm:ss');
                    var dayViewEnd = moment(sTime.endTime, 'HH:mm:ss');
                    if (sTime.endTime == "23:59" && sTime.allDayEvents != undefined && datefinalValue != dateendval) {
                        dateendval = moment(sTime.endDate).add('days', 1).format('YYYY-MM-DD');
                    }
                    var appBgColor = "",
                        appFontColor = "";
                    var dayCounter = moment(datefinalValue)
                        .clone()
                        .hours(dayViewStart.hours())
                        .minutes(dayViewStart.minutes())
                        .seconds(dayViewStart.seconds());

                    if (sTime.endTime == "24:00") {
                        datefinalValue = $filter('date')(data.calendarList[j].consultationDt + 86400000, "yyyy-MM-dd")
                    }
                    var dayend = moment(dateendval)
                        .clone()
                        .hours(dayViewEnd.hours())
                        .minutes(dayViewEnd.minutes())
                        .seconds(dayViewEnd.seconds());
                    if (sTime.appointmentStatus == "B") {
                        var appStatus = "Booked";
                        appBgColor = "#757ef8";
                        appFontColor = "#FFF";
                    } else if (sTime.appointmentStatus == "S") {
                        var appStatus = "Started";
                        appBgColor = "#77cb52";
                        appFontColor = "#FFF";
                    } else if (sTime.appointmentStatus == "I") {
                        var appStatus = "Checked-In";
                        appFontColor = "#FFF";
                        appBgColor = "#70d7cc";
                    } else if (sTime.appointmentStatus == "EVENT" || sTime.appointmentStatus == "IMPEVENT") {
                        var appStatus = "Events";
                        appBgColor = "#8d67b1";
                        appFontColor = "#FFF";
                    } else {
                        var appStatus = "Completed";
                        appBgColor = "#ff9024";
                        appFontColor = "#FFF";
                    }
                    var doctorSpecialities = "";
                    for (var i in sTime.doctorInfo.specialityCd) {
                        doctorSpecialities += ", " + getSpecialityDesc(sTime.doctorInfo.specialityCd[i]);
                    }
                    if (doctorSpecialities.charAt(0) == ",") {
                        doctorSpecialities = doctorSpecialities.slice(2);
                    }
                    if (sTime.appointmentStatus == "EVENT" || sTime.appointmentStatus == "IMPEVENT") {
                        sTime.patientInfo.title = "Dr";
                        sTime.patientInfo.doctorID = sTime.doctorID;
                        sTime.patientInfo.fullName = sTime.doctorInfo.doctorName;
                        sTime.patientInfo.doctorSpecialities = "";
                        title = sTime.otherVisitRsn || "Events";
                        eventInfo = ((sTime.patientInfo.title === undefined) ? "" : sTime.patientInfo.title + '.') + sTime.patientInfo.fullName + " | Token : 0 | - | /Male | " + sTime.startTime + " - " + sTime.endTime + " | " + appStatus;
                    } else {
                        title = ((sTime.patientInfo.title === undefined || sTime.patientInfo.title === "") ? "" : sTime.patientInfo.title + '. ') + sTime.patientInfo.fullName;
                        eventInfo = ((sTime.patientInfo.title === undefined || sTime.patientInfo.title === "") ? "" : sTime.patientInfo.title + '. ') + sTime.patientInfo.fullName + " | Token : " + (sTime.tokenNumber == '0' ? "Walk-In" : sTime.tokenNumber) + " | " + ((sTime.patientInfo.mobileNumber == undefined || sTime.patientInfo.mobileNumber.toString().length < 6) ? "-" : sTime.patientInfo.mobileNumber) + " | " + ((sTime.patientInfo.currentAge === undefined) ? ((sTime.patientInfo.age == undefined) ? '' : (sTime.patientInfo.age + "y")) : (((sTime.patientInfo.currentAge.year > 0) ? sTime.patientInfo.currentAge.year + "y" : '') + ' ' + ((sTime.patientInfo.currentAge.month > 0) ? sTime.patientInfo.currentAge.month + "m" : ''))) + "/" + ((sTime.patientInfo.sex == 'M') ? 'Male' : 'Female') + " | " + sTime.startTime + " - " + sTime.endTime + " | " + appStatus;
                    }
                    var newEvent = {
                        title: title, // + " | Token : " + (sTime.tokenNumber == '0' ? "Walk-In" : sTime.tokenNumber) + " | " + ((sTime.patientInfo.mobileNumber == undefined || sTime.patientInfo.mobileNumber.toString().length < 6) ? "-" : sTime.patientInfo.mobileNumber) + " | " + ((sTime.patientInfo.currentAge === undefined) ? ((sTime.patientInfo.age == undefined) ? '' : (sTime.patientInfo.age)) : (((sTime.patientInfo.currentAge.year > 0) ? sTime.patientInfo.currentAge.year + "y" : '') + ' ' + ((sTime.patientInfo.currentAge.month > 0) ? sTime.patientInfo.currentAge.month + "m" : ''))) + "/" + ((sTime.patientInfo.sex == 'M') ? 'Male' : 'Female') + " | " + sTime.startTime + " - " + sTime.endTime + " | " + appStatus,
                        eventInfo: eventInfo,
                        start: dayCounter.clone(),
                        startDate: dayCounter.clone(),
                        startTime: dayCounter.clone(),
                        endDate: dayend.clone(),
                        endTime: dayend.clone(),
                        end: dayend.clone(),
                        color: appBgColor,
                        textColor: appFontColor,
                        allDay: sTime.allDayEvents != undefined,
                        token: sTime.tokenNumber == "0" ? "Walk-In" : sTime.tokenNumber,
                        eventTitle: sTime.otherVisitRsn || "Events",
                        eventDesc: sTime.eventDescription || "",
                        eventVenue: sTime.eventLocation || "",
                        sliceTime: docMinPerCase,
                        showDoctors: $scope.allDoctorAllowed && $scope.isAccessRightsApplicable,
                        rowData: {
                            addressId: booked.calendarList[j].slotList[k].addressID,
                            doctorId: booked.calendarList[j].slotList[k].doctorID,
                            hospitalId: booked.calendarList[j].slotList[k].hospitalID,
                            doctorName: sTime.doctorInfo.doctorName,
                            doctorSpecialities: doctorSpecialities,
                            doctorImage: sTime.doctorInfo.doctorImage,
                            consultationDate: booked.calendarList[j].slotList[k].consultationDt,
                            DayCD: booked.calendarList[j].slotList[k].dayCD,
                            PatientInfo: sTime.patientInfo,
                            visitReason: sTime.otherVisitRsn || "",
                            doctorVisitRsnId: sTime.doctorVisitRsnId
                        }
                    };
                    $scope.eventsList.push(newEvent);
                }

            } //for k
        } // for j
    }

    var rebookEvent = function (event, jsEvent, view) {
        var coeff = 1000 * 60 * docMinPerCase;
        var startdt = moment(event.start);
        var newenddt = moment(event.end);
        var date = moment(new Date(Math.floor(startdt.valueOf() / coeff) * coeff));
        var enddate = event.end != null ? moment(new Date(Math.floor(newenddt.valueOf() / coeff) * coeff)) : null;
        var obj = {
            "action": "rebook",
            "label": date.format('hh:mmA'),
            "date": event.start,
            "minuteStart": date.format('HH:mm:ss'),
            "DoctorStTime": "07:00",
            "DoctorEdTime": "20:00",
            "addressId": event.rowData.addressId,
            "doctorId": event.rowData.doctorId,
            "consultationDate": date.valueOf(),
            "DayCD": "FRI",
            "doctorName": event.rowData.doctorName,
            "doctorSpecialities": event.rowData.doctorSpecialities,
            "sliceTime": docMinPerCase,
            "TimeSlotDetail": {
                "StartTime": date.format('HH:mm'),
                "EndTime": enddate != null ? enddate.format('HH:mm') : date.add(docMinPerCase, 'minutes').format('HH:mm'),
                "Available": "Y",
                "TokenNumber": 31,
                "PatientInfo": angular.copy(event.rowData.PatientInfo)
            }
        }
        var doctorinfoaccess = hcueDoctorLoginService.getDoctorInfo();
        var access = doctorinfoaccess.isAccessRightsApplicable;
        if (access) {
            if (doctorinfoaccess.AccountAccessID.indexOf('BKAPPTMENT') == -1) {
                alertify.log("You do not have access to view this feature, Kindly contact your Administrator");
                return;
            }
        }
        $scope.dateStart = moment(date).format('YYYY-MM-DD');

        var newEvent = {};
        eventPopUp(obj);

    }
    //**End of New calendar config functions**//

    var hoslistparam = {
        "pageNumber": 1,
        "HospitalID": parentHospitalId,
        "pageSize": 1000,
        "IncludeDoctors": "Y",
        "IncludeBranch": "Y",
        "ActiveHospital": "Y",
        "ActiveDoctor": "Y"

    }
    $scope.attachstatustype = '';
    c7AdminServices.getHospitals(hoslistparam, OnGetDoctorSuccess, OnGetDoctorError);

    function OnGetDoctorSuccess(data) {
        if (!data.error) {
            clinicAddress = [];
            for (dt of data.rows) {
                if (dt.HospitalInfo.HospitalDetails.ActiveIND == "Y" && dt.DoctorDetails.length > 0) {
                    var doctorNewCollection = [];
                    angular.forEach(dt.DoctorDetails, function (j) {
                        if (j.RoleID == 'SONOGRAPHER')
                            doctorNewCollection.push(j);
                    });
                    if (doctorNewCollection.length != 0) {
                        var item = {
                            "ClinicName": dt.HospitalInfo.HospitalDetails.HospitalName,
                            "Address1": dt.HospitalInfo.HospitalAddress.Address1,
                            "Address2": dt.HospitalInfo.HospitalAddress.Address2,
                            "Location": dt.HospitalInfo.HospitalAddress.Location,
                            "CityTown": dt.HospitalInfo.HospitalAddress.CityTown,
                            "State": dt.HospitalInfo.HospitalAddress.State,
                            "PinCode": dt.HospitalInfo.HospitalAddress.PinCode,
                            "Country": dt.HospitalInfo.HospitalAddress.Country,
                            "Active": dt.HospitalInfo.HospitalDetails.ActiveIND,
                            "Doctors": doctorNewCollection,
                            "ExtDetails": {
                                "HospitalID": dt.HospitalInfo.HospitalDetails.HospitalID,
                                "PrimaryIND": "Y",
                                "AddressCommunication": [],
                                "HospitalInfo": {
                                    "HospitalID": dt.HospitalInfo.HospitalDetails.HospitalID,
                                    "ParentHospitalID": dt.HospitalInfo.HospitalDetails.ParentHospitalID,
                                    "HospitalCode": doctorInfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode,
                                    "BranchCode": "",
                                    "Preference": {}
                                }
                            },
                        };
                        clinicAddress.push(item);
                    }
                }
            }
            $scope.initialiseClinicDoctor();
        }
    }

    function OnGetDoctorError(msg) {
        alertify.error('Error in Appointment Booking');
    }

    function OnGetAppointmentSuccess(data) {

        $scope.eventsList = [];
        $scope.appCount = data.appointmentCounts;
        var booked = data;

        $scope.patDetails.length = 0;
        var jsonAvailConsultTime = [];
        jsonUnavailableTime.length = 0;
        $scope.minPerCase = 0;

        if(data.status == "failure"){
            $scope.showbook = false;
            alertify.error('No Schedule for this Location, Please choose another Location');
           }
           else{
            $scope.addressconsultid = data.calendarList[0].addressconsultid;
            $scope.showbook = true;
            window.localStorage.setItem('addressGetData',  $scope.addressconsultid);
           }
        if (booked.calendarList != 'undefined' && booked.calendarList != null && booked.calendarList.length > 0 && booked.calendarList[0].slotList.length > 0) {

            $scope.eventsList = [];
            loadEvents(booked, data);
            hcueDoctorLoginService.addDocAvailConsultTimeInfo(jsonAvailConsultTime);

        } else {
            if (JSON.stringify($scope.showDayID) === "true" && JSON.stringify($scope.appointmentlistID) === "false") {
                $scope.dayConfig.visible = false;
                $scope.noData = "Time slot is not available for the selected day...";
            }
            hcueDoctorLoginService.addDocAvailConsultTimeInfo("");
            $scope.setValue = [];
            $scope.eventsList = [];
            $scope.beginBHr = [];
            $scope.beginEHr = [];

        }

        if (booked.calendarList != 'undefined' && booked.calendarList != null && booked.calendarList.length > 0){
            $scope.emergencyslotstarttime = booked.calendarList[0].emergencyslotstarttime;
            $scope.emergencyslotendtime = booked.calendarList[0].emergencyslotendtime;
            $scope.minpercaseslot = booked.calendarList[0].minpercaseslot;
        }

        //calCurrTime="15:00:00";//moment().format('HH:mm:ss');
        uiCalendarConfig.calendars["calControl"].fullCalendar('removeEvents');
        uiCalendarConfig.calendars["calControl"].fullCalendar('addEventSource', $scope.eventsList);
        uiCalendarConfig.calendars["calControl"].fullCalendar('rerenderEvents');
        angular.element('#calControl').fullCalendar('option', {
            defaultTimedEventDuration: '00:' + docMinPerCase + ':00',
            slotDuration: '00:' + docMinPerCase + ':00',
            //scrollTime:calCurrTime
        })
        setTimeout(function () { // Focus event
            angular.element('.fc-scroller').scrollTop(0);
            if ($('.fc-now-indicator-line').offset()) {
                angular.element('.fc-scroller').scrollTop($('.fc-now-indicator-line').offset().top - 300);
            }
        }, 1000);
        hcueDoctorLoginService.addDocMinPerCaseInfo($scope.minPerCase);
        /**Start Time**/

        if (getFormattedDate(new Date($scope.formatteddateValue)) == getFormattedDate(new Date())) {
            var dt = new Date()
            dt.getHours() >= 12 ? (function () {
                hr = dt.getHours() % 12 == 0 ? 12 : dt.getHours() % 12;
                ampm = "PM"
            })() : (function () {
                hr = dt.getHours();
                ampm = "AM"
            })();
            var min = dt.getMinutes() < 10 ? "0" + dt.getMinutes() : dt.getMinutes();
            var time = hr + ":" + min + "" + ampm;

            setTimeout(function () {
                var match = "";
                $('.cal-day-hour-part .span1').each(function (i, e) {
                    var ele = $(e).text();

                    if ($(e).text() == time) {
                        match = e;
                        return false;
                    } else {
                        var curr = getTimeSplit(time);
                        var pre = getTimeSplit(ele);
                        if (curr['hr'] == pre['hr'] && curr['ampm'] == pre['ampm']) {
                            if (curr['min'] < pre['min']) {
                                match = $('.cal-day-hour-part .span1')[i];
                                return false;
                            }
                        }
                    }
                });

                var scrollVal = $('.cal-day-hour-part').index($(match).parent()) * 30;
                $('.cal-day-panel').scrollTop(scrollVal);
            }, 1000);
        }
        /**End Time**/
        $scope.showCalendar = true;


    }

    function OnGetAppointmentError(data) {
        hcueDoctorLoginService.addDocAvailConsultTimeInfo("");
        $scope.eventsList = [];
        $scope.setValue = [];
        $scope.beginBHr = [];
        $scope.beginEHr = [];
        $scope.showCalendar = false;
        //setCalenderViewVisibility(false);
        alertify.log("Error in listing Appointments,  Please check internet connectivity");
    }

    //**End of Callback Functions**//

    $scope.checkOnline = function () {
        if (!JSON.parse(localStorage.getItem("isAppOnline"))) {
            alertify.log("This is for Online users");
            return true;

        }

    }

    //**Initializing Function Calls**//
    //ResetClinicDoctorList();
    //List Appointment
    //$scope.today();
    $scope.referredappointmentdate = $filter('date')(new Date(), "dd-MM-yyyy");
    $scope.viewDate = new Date();
    $scope.showCalendar = true;
    $scope.referredappointmentdate = $filter('date')(new Date($scope.getCurrentDate()), "dd-MM-yyyy")
    var currentdaycode = $filter('date')($scope.getCurrentDate(), "EEE").toUpperCase();
    hcueDoctorLoginService.addSelectedAppointDt($scope.getCurrentDate());



    /* config object */
    $scope.uiConfig = {
        calendar: {
            height: 450,
            editable: true,
            navLinks: true,
            header: {
                left: '',
                center: '',
                right: ''
            },
            views: {
                day: {
                    columnFormat: 'DD-MM-YYYY'
                },
                week: {
                    columnFormat: 'ddd Do MMM\'YY'
                },
                month: {
                    columnFormat: 'ddd'
                }
            },
            allDaySlot: false,
            defaultTimedEventDuration: '00:05:00',
            displayEventTime: false,
            timezone: "local",
            duration: '00:05:00',
            slotDuration: '00:' + sliceTime + ':00',
            slotLabelInterval: '00:30:00',
            slotLabelFormat: 'h:mm a',
            defaultView: 'agendaDay',
            eventClick: undefined,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender,
            navLinkDayClick: function (date, jsEvent) {
                $scope.referredappointmentdate = $filter('date')(new Date(date), "dd-MM-yyyy");
                $scope.viewDate = new Date(date);
                $scope.loadCalenderEvent('day');

            },
            editable: false,
            disableDragging: true,
            dayClick: $scope.alertOnDayClick,
            eventMouseover: $scope.eventMouseover,
            eventMouseout: $scope.eventMouseout,
            eventLimit: true,
            nowIndicator: true,
            scrollTime: calCurrTime,
            eventDragStart: function (event, jsEvent, ui, view) {
                event.className += ' eventDrag';
            },
            eventDragStop: function (event, jsEvent, ui, view) {
                event.className = "";
            }
        }
    };
    /*End of config object*/


    //**End of Initializing Function Calls**//


    $scope.cleardoctorall = function () {
        $scope.query.value1 = "";
    }

});