﻿hCueDoctorWebApp.lazyController("hcueFeedBackController", ["$scope", "$location", "hcueDoctorLoginService", "hcueLoginStatusService", "hCueAdminServices", "datacookieFactory", "$filter","alertify", function ($scope, $location, hcueDoctorLoginService, hcueLoginStatusService, hCueAdminServices, datacookieFactory, $filter, alertify) {
     //Sabarish Code Start
     var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
     var HospitalId = doctorinfo.hospitalID;
     var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
     userLoginDetails = datacookieFactory.getItem('loginData');
     if (userLoginDetails.loggedinStatus === "true") {
         $scope.setHeaderShouldShow(true);
     } else {
         $location.path('/login');
     }
     $scope.addNew = true;
     $scope.updateNew = false;
     getUsers(userLoginDetails.HospitalID);

     $scope.eng_lang = 'Y';
     $scope.tam_lang = 'Y';
     $scope.hin_lang = 'Y';
     function getUsers(id) {
         var data = {
             "pageNumber": 1,
             "HospitalID": id,
             "pageSize": 10,
             "IncludeDoctors": "Y",
             "IncludeBranch": "Y"
         };
         hCueAdminServices.GetUserList(data, callbackSuccessUserList, callbackErrorUserList);
         function callbackSuccessUserList(data) {
             $scope.overrideDataUn = data.rows;
         }
         function callbackErrorUserList(error) {
             console.log(JSON.stringify(error));
         }
     }
     $scope.managebranch = function () {
         if (doctorinfo.isAccessRightsApplicable) {
             if (doctorinfo.AccountAccessID.indexOf('MANBRNS') == -1) {
                 alertify.log("To enable this feature, Please contact hCue @ 9543279999 or email your requirements to support@hcue.co");
                 return;
             } else {

                 $scope.setHeaderShouldShow(true);
                 var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
                 var url = "/managebranch?kwid=" + (guid || "");
                 $location.url(url);

             }
         }
     }
     $scope.manageusers = function () {
         if (doctorinfo.isAccessRightsApplicable) {
             if (doctorinfo.AccountAccessID.indexOf('MANUSRS') == -1) {
                 alertify.log("To enable this feature, Please contact hCue @ 9543279999 or email your requirements to support@hcue.co");
                 return;
             } else {

                 $scope.setHeaderShouldShow(true);
                 var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
                 var url = "/manageusers?kwid=" + (guid || "");
                 $location.url(url);
             }
         }
     }
     $scope.getSelectedRegBranch = function (branchid) {
         if (branchid == null) {
             $scope.RegFields = JSON.parse(localStorage.getItem("regfields"));
         } else {
             GetRegistrationFields(branchid);
         }
     };
     localStorage.removeItem("regfields");
      $scope.RegFields = [
         {
             "rowid": 1,
             "groupid": 1,
             "groupedes": "BasicDetails",
             "fieldname": "Name",
             "fieldtype": "TEXT_BOX",
             "mandatory": "Y",
             "active": "Y"
      },
         {
             "rowid": 2,
             "groupid": 1,
             "groupedes": "BasicDetails",
             "fieldname": "Age",
             "fieldtype": "TEXT_BOX",
             "mandatory": "Y",
             "active": "Y"

      },
         {
             "rowid": 3,
             "groupid": 1,
             "groupedes": "BasicDetails",
             "fieldname": "Gender",
             "fieldtype": "TEXT_BOX",
             "mandatory": "Y",
             "active": "Y"

      },
         {
             "rowid": 4,
             "groupid": 1,
             "groupedes": "BasicDetails",
             "fieldname": "Mobile Number",
             "fieldtype": "TEXT_BOX",
             "mandatory": "N",
             "active": "Y"

      },
         {
             "rowid": 5,
             "groupid": 1,
             "groupedes": "BasicDetails",
             "fieldname": "Email Id",
             "fieldtype": "TEXT_BOX",
             "mandatory": "N",
             "active": "Y"

      },
         {
             "rowid": 6,
             "groupid": 1,
             "groupedes": "BasicDetails",
             "fieldname": "Address",
             "fieldtype": "TEXT_BOX",
             "mandatory": "N",
             "active": "Y"

      },
         {
             "rowid": 7,
             "groupid": 2,
             "groupedes": "EmergencyContact",
             "fieldname": "Emergency Contact Name",
             "fieldtype": "TEXT_BOX",
             "mandatory": "N",
             "active": "Y",

      },
         {
             "rowid": 8,
             "groupid": 2,
             "groupedes": "EmergencyContact",
             "fieldname": "Emergency Contact Relationship",
             "fieldtype": "TEXT_BOX",
             "mandatory": "N",
             "active": "Y"

      },
         {
             "rowid": 9,
             "groupid": 2,
             "groupedes": "EmergencyContact",
             "fieldname": "Emergency Contact Mobile Number",
             "fieldtype": "TEXT_BOX",
             "mandatory": "N",
             "active": "Y"

      },
         {
             "rowid": 10,
             "groupid": 2,
             "groupedes": "EmergencyContact",
             "fieldname": "Emergency Contact Alternative Contact Number",
             "fieldtype": "TEXT_BOX",
             "mandatory": "N",
             "active": "Y"

      },
         //{
         //    "rowid": 11,
         //    "groupid": 2,
         //    "groupedes": "EmergencyContact",
         //    "fieldname": "Would you recommend this App to your friends?",
         //    "fieldtype": "TEXT_BOX",
         //    "mandatory": "N",
         //    "active": "Y"

         //},
         {
             "rowid": 11,
             "groupid": 3,
             "groupedes": "OtherDetails",
             "fieldname": "Marital Status",
             "fieldtype": "TEXT_BOX",
             "mandatory": "N",
             "active": "Y"

      },
         {
             "rowid": 12,
             "groupid": 3,
             "groupedes": "OtherDetails",
             "fieldname": "Education",
             "fieldtype": "TEXT_BOX",
             "mandatory": "N",
             "active": "Y"

      },
         {
             "rowid": 13,
             "groupid": 3,
             "groupedes": "OtherDetails",
             "fieldname": "Occupation",
             "fieldtype": "TEXT_BOX",
             "mandatory": "N",
             "active": "Y"

      },
         {
             "rowid": 14,
             "groupid": 3,
             "groupedes": "OtherDetails",
             "fieldname": "Payment Mode",
             "fieldtype": "TEXT_BOX",
             "mandatory": "N",
             "active": "Y"

      },
         {
             "rowid": 15,
             "groupid": 3,
             "groupedes": "OtherDetails",
             "fieldname": "Referral Source",
             "fieldtype": "TEXT_BOX",
             "mandatory": "N",
             "active": "Y"

      },
      
 ];
     localStorage.setItem('regfields', JSON.stringify($scope.RegFields));
     function GetRegistrationFields(hospitalid) {
         hCueAdminServices.listRegistration(hospitalid, OnSuccess, Onerror);
         function OnSuccess(data) {

             console.log(JSON.stringify(data));
             if (data == "No Data Available") {
                 $scope.RegFields = [];
                 $scope.RegFields = JSON.parse(localStorage.getItem("regfields"));
             } else {
                 $scope.RegFields = [];
                 $scope.RegFields = data;
             }
         }
         function Onerror(error) {
             console.log(JSON.stringify(error));
         }
     }
     $scope.changeStatus = function (event, index, groupdesc) {
      
        var id = event.target.id;
        var value = event.target.value;
        if (angular.element("#" + id).is(":checked") == true) {
         $scope.RegFields[index].mandatory = "Y";
        } else {
         $scope.RegFields[index].mandatory = "N";
        }
         
     };

     $scope.FeedbackchangeStatus = function (index, rowID) {
         AddupdateFeedBack($scope.getAddBranchDetails, rowID, 0);
     };


     $scope.SaveRegFileds = function () {
         $scope.regfieldarray = [];
         var doctorid = doctorinfo.doctorid;
         var hospitalid = doctorinfo.hospitalID;
         if ($scope.PatientRegBranch == undefined) {
             alert("Select Any Branch");
         } else {
              for (var i = 0; i < $scope.RegFields.length; i++) {
                     if ($scope.RegFields[i].fieldname == "Name") {
                         $scope.RegFields[i].mandatory = "Y";
                         }
                     if ($scope.RegFields[i].fieldname == "Age") {
                         $scope.RegFields[i].mandatory = "Y";
                     }
                     if($scope.RegFields[i].fieldname == "Gender") {
                         $scope.RegFields[i].mandatory = "Y";
                         }
                 }

            
             hCueAdminServices.addPatientRegistration($scope.RegFields, doctorid, $scope.PatientRegBranch, OnSuccess, Onerror);
             function OnSuccess(data) {
                 $scope.RegFields = [];
                 $scope.RegFields = data;
                 alertify.success("Saved successfully");
             }
             function Onerror(error) {
                 console.log(JSON.stringify(error));
             }
         }
     };
     //Sabarish Code End

     //DurgaReddy Code Start
     $scope.buttonName = "Add";
     $scope.updateBranch = function (feedbackAdd) {

         $scope.question_array = [];
         if($scope.eng_lang == 'Y' && $scope.eng_lanText !== undefined) {
            $scope.question_array.push({
                'lang':'eng',
                'question':$scope.eng_lanText
            })
         }

         if($scope.tam_lang == 'Y' && $scope.tamil_lanText !== undefined) {
            $scope.question_array.push({
                'lang':'tam',
                'question':$scope.tamil_lanText
            })
         }

         if($scope.hin_lang == 'Y' && $scope.hin_lanText !== undefined) {
            $scope.question_array.push({
                'lang':'hin',
                'question':$scope.hin_lanText
            })
         }

         
         if($scope.question_array.length == 0) {
            alert('Add aleast one question');
            return;
         }

         AddupdateFeedBack($scope.getAddBranchDetails, $scope.question_array, 1);
     }
     var question_rowsCollection = [{"rowid":0,"notes":"","rankvalue":0,"activeIND":"Y","questions":[]}]
     function AddupdateFeedBack(obj, questionValue, type) {

         if(obj == undefined) {
            alertify.error("Select the clinic");
            return;
         }
         var lan_list = [];
         var lang_collection = [];
         var existing_collection;
         $scope.getAddBranchDetails = obj;
         var index = 0;
         var question_collection = [];

         if ($scope.getAddBranchDetails !== undefined) {
             if ($scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion !== undefined && $scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion.length > 0) {
                 $scope.tmpQuestion = angular.copy($scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion);
             }
             if($scope.updateRowId == undefined) {

                 if($scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion !== undefined && $scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion.questions !== undefined) {
                    var length;
                    var row_id;
                    if($scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion.questions !== undefined && $scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion.questions.length > 0) {
                        length = $scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion.questions.length;
                        row_id = $scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion.questions[length-1].rowid;
                        row_id = row_id+1;
                    }
                    existing_collection = $scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion.questions
                        
                    if(existing_collection.length >= 6) {
                        alert('You cannot add more than 6 questions');
                        return;
                    }
                    if(type == 1 ) {

                        $scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion.questions.push({
                            "rowid":row_id,
                            "notes":"",
                            "rankvalue":0,
                            "activeIND":"Y",
                            "questions":questionValue
                        })

                    }
                    angular.forEach(questionValue,function(item) {
                        lan_list.push(item.lang);
                     });
                    var lan_params = {
                        "langCount": 3,
                        "lang":['eng','tam','hin'],
                        "questions":existing_collection
                     }
                 }
                 else {

                    angular.forEach(questionValue,function(item) {
                        lan_list.push(item.lang);
                     });
                    //question_rowsCollection.questions.push(questionValue);

                     var lan_params = {
                        "langCount": 3,
                        "lang":['eng','tam','hin'],
                        "questions":[{"rowid":0,"notes":"","rankvalue":0,"activeIND":"Y","questions":questionValue}]
                     }
                 }
             }
             else {
                angular.forEach($scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion.questions,function(updateItem) {
                    if(updateItem.rowid == $scope.updateRowId) {
                        updateItem.questions = questionValue;
                    }
                })
             }

             var params = {
                 "USRType": "ADMIN",
                 "Feedbackquestions": $scope.tmpQuestion,
                 "USRId": parseInt(doctorinfo.doctordata.doctor[0].DoctorID),
                 "HospitalAddress": {
                     "State": "TN",
                     "Country": "IND",
                     "Address1": $scope.getAddBranchDetails.HospitalAddress.Address1,
                     "Address2": $scope.getAddBranchDetails.HospitalAddress.Address1,
                     "CityTown": $scope.getAddBranchDetails.HospitalAddress.CityTown,
                     "DistrictRegion": $scope.getAddBranchDetails.HospitalAddress.DistrictRegion,
                     "PinCode": parseInt($scope.getAddBranchDetails.HospitalAddress.PinCode),
                     "Location": $scope.getAddBranchDetails.HospitalAddress.Location
                 },
                 "HospitalDetails": {
                     "RegsistrationDate": Date.parse($scope.getAddBranchDetails.HospitalDetails.RegsistrationDate),
                     "Account": {
                         "1": "PLUS"
                     },
                     "AboutHospital": $scope.getAddBranchDetails.HospitalDetails.HospitalName,
                     "LicenseNumber": $scope.getAddBranchDetails.HospitalDetails.LicenseNumber,
                     "HospitalID": $scope.getAddBranchDetails.HospitalDetails.HospitalID,
                     "ParentHospitalID": userLoginDetails.HospitalID,
                     "TINNumber": $scope.getAddBranchDetails.HospitalDetails.TINNumber,
                     "EmailAddress": $scope.getAddBranchDetails.HospitalDetails.EmailAddress,
                     "MobileNumber": parseInt($scope.getAddBranchDetails.HospitalDetails.MobileNumber),
                     "TermsAccepted": "Y",
                     "WebSite": "pearldental.com",
                     "ActiveIND": $scope.getAddBranchDetails.HospitalDetails.ActiveIND,
                     "HospitalName": $scope.getAddBranchDetails.HospitalDetails.HospitalName,
                     "ContactName": $scope.getAddBranchDetails.HospitalDetails.ContactName
                 }
             };

             if(type == 1 && $scope.updateRowId == undefined) {
                params.Feedbackquestions = lan_params;
             }
             else if(type == 1 && $scope.updateRowId !== undefined) {
                params.Feedbackquestions = $scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion;
             }
             if(type == 0) {
                params.Feedbackquestions = $scope.getAddBranchDetails.HospitalDetails.FeedBackQuestion;
             }

             if (type == 1) {
                 hCueAdminServices.UpdateBranch(params, updateBranchSuccess, updateBranchError);
             }
             if (type == 0) {
                 hCueAdminServices.UpdateBranch(params, updateActiveInactiveSuccess, updateBranchError);
             }

         }

     }
     function updateBranchSuccess(data) {

       
         $scope.getAddBranchDetails = [];

         $scope.getAddBranchDetails = data;

         if ($scope.buttonName == "Add") {
             alertify.success("Feedback question added successfully")
         } else {
             alertify.success("Feedback updated successfully")
         }
         $scope.questionValue = "";
         $scope.buttonName = "Add";
         $scope.updateRowId = undefined;
         $scope.eng_lanText = undefined;
         $scope.tamil_lanText = undefined;
         $scope.hin_lanText = undefined;
         $scope.addNew = true;
         $scope.updateNew = false;
         $scope.eng_lang == 'N';
         $scope.tam_lang == 'N';
         $scope.hin_lang == 'N';
     }

     function updateActiveInactiveSuccess(data) {
         alertify.success("Changed Status Successfully");
     }
     function updateBranchError(data) {
         alertify.error("Internal Error, Branch Not Updated, Please try again");
     }
     $scope.SelectedFeedbackObj = "";
     $scope.resetForm = function() {
        $scope.eng_lanText = undefined;
        $scope.tamil_lanText = undefined;
        $scope.hin_lanText = undefined;
         $scope.addNew = true;
         $scope.updateNew = false;
         $scope.updateRowId = undefined;
         $scope.eng_lang == 'Y';
         $scope.tam_lang == 'Y';
         $scope.hin_lang == 'Y';
     }
     $scope.EditFeedbackQuestion = function (obj,index) {
         $scope.SelectedFeedbackObj = obj;
         $scope.questionValue = obj.questions;
         $scope.buttonName = "Update";
         $scope.addNew = false;
         $scope.updateNew = true;
         $scope.updateRowId = obj.rowid;
         
            $scope.eng_lanText = undefined;
            $scope.tamil_lanText = undefined;
            $scope.hin_lanText = undefined;
        angular.forEach($scope.questionValue,function(item) {
            if(item.lang == "eng") {
                $scope.eng_lang = "Y";
                $scope.eng_lanText = item.question;
             }

             if(item.lang == "tam") {

                $scope.tam_lang = "Y";
                $scope.tamil_lanText = item.question;
             }

             if(item.lang == "hin") {

                $scope.hin_lang = "Y";
                $scope.hin_lanText = item.question;
             }

        })

     }

     $scope.getBranch = function (branch) {
        $scope.resetForm();
         if (branch !== null) {
             hCueAdminServices.GetBranch(branch, ongetBranchScuccessRoles, ongetBranchErrorRoles);
             function ongetBranchScuccessRoles(data) {
                 console.log(JSON.stringify(data));
                 $scope.getAddBranchDetails = [];
                 $scope.getAddBranchDetails = data;

             }

             function ongetBranchErrorRoles(data) {
                 console.log('Error...' + JSON.stringify(data));

             }
         }
         else {
             $scope.getAddBranchDetails = [];
         }
     }
     // DurgaReddy Code End
 }]);
