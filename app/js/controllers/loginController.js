﻿hCueDoctorWebApp.lazyController('hCueLoginController', [
  '$scope',
  '$filter',
  '$location',
  '$route',
  'datapersistanceFactory',
  'hcueAuthenticationService',
  'hcueDoctorDataService',
  'hcueDoctorLoginService',
  'hcuePatientCaseService',
  'hcueConsultationInfo',
  'hCueAppointmentService',
  'datacookieFactory',
  'pouchdbMaintenanceService',
  'cfpLoadingBar',
  'alertify',
  '$timeout',
  'AuthToken',
  'hCueforgotpasswordServices',
  function (
    $scope,
    $filter,
    $location,
    $route,
    datapersistanceFactory,
    hcueAuthenticationService,
    hcueDoctorDataService,
    hcueDoctorLoginService,
    hcuePatientCaseService,
    hcueConsultationInfo,
    hCueAppointmentService,
    datacookieFactory,
    pouchdbMaintenanceService,
    cfpLoadingBar,
    alertify,
    $timeout,
    AuthToken,
    hCueforgotpasswordServices
  ) {
    $scope.error = false;
    $scope.setHeaderShouldShow(false);
    $scope.logininfo = { username: '', password: '', rememberme: false };
    $scope.emailpattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;;
    datapersistanceFactory.remove('medicineData');
    datapersistanceFactory.remove('docAddressConsultID');
    datacookieFactory.remove('labtests');
    datacookieFactory.remove('vitalnoteinfo');
    datacookieFactory.remove('prescriptionnotessymptom');
    if (localStorage.getItem('isAppOnline') == null) {
      localStorage.setItem('isAppOnline', true);
    }
    if (localStorage.getItem('offlineURL') == null) {
      //Offline URL Initialization
      localStorage.setItem('offlineURL', 'https://localhost:8012');
    }
    $scope.showloading = true;
    var storedUserName = datacookieFactory.getItem('rememberUser');
    var remembercheck = datacookieFactory.getItem('rememberme');

    if (storedUserName !== undefined) {
      $scope.logininfo.username = storedUserName;
    }
    if (remembercheck !== undefined) {
      $scope.logininfo.rememberme = remembercheck;
    }
    //clear cache

    $scope.LoginClick = function (loginform) {
      $scope.error = false;
      cfpLoadingBar.start();
      if (!loginform.$valid) {
        cfpLoadingBar.complete();
        return;
      }
      $scope.showloading = true;
      $scope.rememberme = true;

      hcueAuthenticationService.AuthenticateDoctor(
        $scope.logininfo.username,
        $scope.logininfo.password,
        $scope.logininfo.rememberme,
        OnLoginSuccess,
        OnLoginError
      );
    };

    function OnLoginSuccess(data) {
      if (data.Identity) {
        AuthToken.setToken(data.Identity);
      } else {
        alertify.error('No token Found');
        return false;
      }
      if (data.doctorAddress[0].ExtDetails.HospitalInfo !== undefined) {
        hcueDoctorLoginService.addsettingpreference(
          data.doctorAddress[0].ExtDetails.HospitalInfo
        );
      }

      hcueDoctorLoginService.addprintblankline(data.DoctorSetting);
      hcueDoctorLoginService.addprintsettingsheader(data.DoctorSetting);

      $scope.loggedinStatus = true;
      $scope.showloading = false;
      cfpLoadingBar.complete();
      var successflg = hcueDoctorDataService.PopulateDoctorInfo(data);
      if (successflg) {
        var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

        if (
          doctorinfo.DoctorSetting !== undefined &&
          doctorinfo.DoctorSetting.isOfflineEnabled !== undefined
        ) {
          //$scope.OfflineEnabled = doctorinfo.DoctorSetting.isOfflineEnabled;
          $scope.OfflineEnabled = false;
        }

        $scope.LoadDoctorName();

        if (
          doctorinfo.doctordata.doctorAddress[0].ExtDetails !== undefined &&
          doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !==
          undefined &&
          doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo
            .BranchCode !== undefined &&
          doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo
            .BranchCode == 'PRIMEINDIA'
        ) {
          localStorage.setItem('offlineURL', 'https://192.168.0:100');
        } else {
          localStorage.setItem('offlineURL', 'https://localhost:8012');
        }

        $scope.closeurlPopup = function () {
          $scope.urlPopup = false;
        };

        if (doctorinfo.AccountAccessID.indexOf('APPLSTVIEW') !== -1 || doctorinfo.AccountAccessID.indexOf('CALENDER') !== -1 || doctorinfo.AccountAccessID.indexOf('MNGENQUIRY') !== -1 || doctorinfo.AccountAccessID.indexOf('MNREFERRALS') !== -1 || doctorinfo.AccountAccessID.indexOf('LISTVIEW') !== -1) {

          $scope.setHeaderShouldShow(true);
          var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
          var url = "/appointment?kwid=" + (guid || "");
          $location.url(url);
        } else {
          $scope.setHeaderShouldShow(true);
          $location.path('/unauthorized');
        }
        // var guid = (getKWID() + getKWID() + '-' + getKWID() + '-4' + getKWID().substr(0, 3) + '-' + getKWID() + '-' + getKWID() + getKWID() + getKWID()).toLowerCase();
        // var url = '/appointment?kwid=' + (guid || '');
        // $location.url(url);
      } else {
        cfpLoadingBar.complete();
        $scope.error = true;
        $scope.errordesc = 'Invalid Email Id / Password';
      }
    }

    function OnLoginError(data, no) {
      if (data == 'RESET YOUR PASSWORD') {
        $scope.showloading = false;
        cfpLoadingBar.complete();
        alertify.error(
          'Your Password has been expired. Please choose forgot password option to change the password.'
        );
      } else if (data != 'ACCOUNT INACTIVATED') {
        $scope.showloading = false;
        cfpLoadingBar.complete();
        if (no == 0) {
          return;
        } else if (no == 1) {
          $scope.error = true;
          $scope.errordesc = 'Invalid Email Id / Password';
        } else {
          alertify.log('Please check the internet connectivity!!!');
        }
      } else {
        $scope.showloading = false;
        cfpLoadingBar.complete();
        alertify.error(
          'Your user role has been removed. Kindly contact your administrator.'
        );
      }
    }
    $scope.forgotpwd = function () {
      $location.url('/forgotpassword');
    };

    $scope.LoadAllDocList = function () {
      hcueAuthenticationService.LoadAllDocIntoDB(
        OnLoginSuccess,
        OnLoadAllDocIntoDBError
      );
    };

    function OnLoadAllDocIntoDBError() { }

    $scope.clearemail = function (form) {
      let controlNames = Object.keys(form).filter(key => key.indexOf('$') !== 0);

      for (let name of controlNames) {
        let control = form[name];
        control.$setViewValue(undefined);
      }
      $scope.forgotemail = '';

      form.$setPristine();
      form.$setUntouched();
    }
    $scope.forgotemail = '';
    $scope.submitforgot = function () {
      if ($scope.forgotemail == '') {
        alertify.error('Please enter your email');
        return false;
      }
      console.log($scope.forgotemail);
      var data = {
        DoctorLoginID: $scope.forgotemail
      };
      hCueforgotpasswordServices.validateEmail(
        data,
        forgotsuccess,
        forgoterror
      );
    };
    function forgotsuccess(data) {
      if (data == 'Success') {
        alertify.success('Mail sent successfully');
        $('#forgotpwd').modal('hide');
      }
    }
    function forgoterror(data) {
      console.log(data);
    }
  }
]);
