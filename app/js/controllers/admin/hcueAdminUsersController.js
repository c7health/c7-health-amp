hCueDoctorWebControllers.controller('hCueAdminUsersController', ['$scope', 'hcueDoctorLoginService', 'hcueDoctorDataService', 'c7AdminServices', '$filter', 'NgMap', 'alertify', '$window', 'referraldoctor', 'datacookieFactory', function($scope, hcueDoctorLoginService, hcueDoctorDataService, c7AdminServices, $filter, NgMap, alertify, $window, referraldoctor, datacookieFactory) {
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    var parentHospitalId;
    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
    }

    $scope.newDoctorId = doctorinfo.doctorid;

    $scope.New = {
        "selectBranch": ''
    };
    $scope.Main = { searchbytext: '' };
    $scope.ListingDisplay = [];
    $scope.accessLisingCollection = [];
    $scope.email_pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    // $scope.password_pattern = /^[0-9a-zA-Z]{6,}$/;
    $scope.password_pattern = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\S+$).{8,}$/;
    $scope.enableSearch = true;

    function branchListing(val1) {
        $scope.Main.searchbytext = '';
        var get_branches = {
            "pageNumber": 1,
            "HospitalID": parseInt(parentHospitalId),
            "pageSize": parseInt(200),
            "IncludeDoctors": "Y",
            "IncludeBranch": "Y",
            "ActiveHospital": "Y",
            "ActiveDoctor": "Y"
        };

        c7AdminServices.getHospitals(get_branches, onSuccessBranches, onAdminUserError)


        function onSuccessBranches(data) {
            if (val1 == undefined) {
                $scope.hospitalDoctorsList = data.rows;
                $scope.New.selectBranch = $scope.hospitalDoctorsList[0];
                $scope.addHospital = {
                    'clinic': data.rows[0]
                }
                $scope.changeClinic($scope.New.selectBranch, '1');
            } else {
                $scope.hospitalDoctorsList = data.rows;
                var newIndex = $scope.hospitalDoctorsList.findIndex(value => value.HospitalInfo.HospitalDetails.HospitalID == val1)
                $scope.New.selectBranch = $scope.hospitalDoctorsList[newIndex];
                $scope.addHospital = {
                    'clinic': data.rows[newIndex]
                }
                $scope.changeClinic($scope.New.selectBranch, '1');
            }
        }
    }
    $scope.changeClinic = function(clinic, sltBranch) {
        if (clinic != null) {
            if (sltBranch == '2') {
                $scope.New.selectBranch = clinic;
                $scope.addHospital = {
                    'clinic': clinic
                }
            }
            angular.forEach($scope.hospitalDoctorsList, function(item) {
                if (item.HospitalInfo.HospitalDetails.HospitalID == clinic.HospitalInfo.HospitalDetails.HospitalID) {

                    $scope.doctorListing = item.DoctorDetails;
                    var selDoc = datacookieFactory.getItem('rememberDoctorSelection');
                    (selDoc.length == 0) ? selDoc = 0: selDoc = selDoc;
                    if (selDoc > $scope.doctorListing.length) {
                        $scope.getDoctorDetails($scope.doctorListing[0]);
                        datacookieFactory.remove('rememberDoctorSelection');
                    } else {
                        $scope.getDoctorDetails($scope.doctorListing[selDoc], selDoc);
                    }

                }
            });
        }
    }
    $scope.getDoctorDetails = function(doctordetails, index) {
        $scope.showaddclinic = false;
        datacookieFactory.set('rememberDoctorSelection', index);
        if (doctordetails !== undefined) {
            var params = {
                "DoctorID": doctordetails.DoctorID,
                "USRType": "ADMIN",
                "USRId": doctordetails.DoctorID
            };
            c7AdminServices.getDoctor(params, onDoctorDetailsSuccess, onAdminUserError);
        } else {
            $scope.selectedDoctor = [];
        }


    }

    function onDoctorDetailsSuccess(data) {
        $scope.selectedDoctor = data;
        $scope.selectedDocID = data.doctor[0].DoctorID;
        // console.log(data);
    }


    function onAdminUserError(data) {
        console.log(JSON.stringify(data));
    }

    function reset() {
        // add user
        $scope.userCollection = {
            "USRType": "ADMIN",
            "USRId": doctorinfo.doctorid,
            "TermsAccepted": "Y",
            "Searchable": "Y",
            "doctorDetails": {
                "Title": "",
                "FirstName": "",
                "LastName": "",
                "Exp": 0,
                "TermsAccepted": "1",
                "FullName": "",
                "DoctorLoginID": "",
                "DoctorPassword": "",
                "Prospect": "N",
                "Gender": "M",
                "DOB": null,
                "RegistrationNo": ""
            },
            "DoctorPhone": [{
                "PhNumber": "",
                "PhType": "D",
                "PrimaryIND": "Y"
            }, {
                "PhNumber": "",
                "PhType": "E",
                "PrimaryIND": "Y"
            }],
            "DoctorAddress": [{
                "Active": "Y",
                "ClinicName": "",
                "ExtDetails": {
                    "MinPerCase": "",
                    "HospitalID": "",
                    "PrimaryIND": "Y",
                    "RoleID": "",
                    "GroupID": "",
                    "Latitude": "",
                    "Longitude": "",
                    "HospitalInfo": {
                        "HospitalID": "",
                        "HospitalCode": "",
                        "BranchCode": ""
                    },
                    "RateperHour": "",
                    "RateBelowTwenty": "",
                    "RateAboveTwenty": ""
                },
                "Country": "",
                "Address1": "",
                "Address2": "",
                "Street": "",
                "Address4": "",
                "AddressType": "B",
                "State": "",
                "CityTown": "",
                "Location": "",
                "PostCode": ""
            }],
            "doctorOtherDetails": {
                "notes": "",
                "isDeleted": "N"
            },
            "SpecialityDetails": []
        }

        $scope.hCueConfimrPassword = {
            'hCueConfimrPassword': ''
        }
        $scope.specialitydisplayCollection = [];

    }

    function loadRoles() {
        var rolesParams = {
            "ParentHospitalID": parentHospitalId,
            "USRId": doctorinfo.doctorid,
            "USRType": "ADMIN",
            "ActiveIND": "Y"
        }
        c7AdminServices.listHospitalRole(rolesParams, onLoadRoleSuccess, onAddUpdateError);
    }

    function onLoadRoleSuccess(data) {
        $scope.rolesList = data;
    }

    function loadRolesList() {
        c7AdminServices.listProfile(parentHospitalId, onListLoadRoleSuccess, onAddUpdateError);
    }

    function onListLoadRoleSuccess(data) {
        $scope.rolesListing = data.Roles;
    }

    function loadRights() {
        var params = {
            "USRType": "ADMIN",
            "USRId": doctorinfo.doctorid,
            "hospitalId": parentHospitalId
        };
        c7AdminServices.listHcueAccessRights(params, onLoadSuccess, onAddUpdateError);
    }

    function onLoadSuccess(data) {
        $scope.rightsList = data;
        $scope.accessLisingCollection.length = 0;
        angular.forEach($scope.rightsList, function(row) {
            angular.forEach(row.AccessRights, function(item) {
                $scope.accessLisingCollection.push(item);
            });
        });
    }

    function specialitylisting() {
        c7AdminServices.SpecialityList(parentHospitalId, specialitylistSuccess, onAddUpdateError)

        function specialitylistSuccess(data) {
            // $scope.specialityCollection=data;
            // angular.forEach($scope.specialityCollection, function(k){
            // 	k.isActive = false;
            // })

            $scope.specialityCollection = data;
            $scope.statusSuccess.speciality += 1;
            if ($scope.statusSuccess.speciality == 2) {
                loadspeciality();
            }
        }
    }

    function onAddUpdateError(data) {
        console.log(JSON.stringify(data));
    }


    var DPhone = [{
        "PhNumber": "",
        "PhType": "D",
        "PrimaryIND": "Y"
    }, {
        "PhNumber": "",
        "PhType": "E",
        "PrimaryIND": "Y"
    }];
    $scope.pageChange = function(val, val1) {
        $scope.rateType = undefined;
        $scope.changeValue = val;
        if (val == "addDoctor") {
            $scope.showaddclinic = false;
            $scope.listingUsers = false;
            $scope.step1 = true;
            $scope.editStep1 = false;
            $scope.enableSearch = false;
            $scope.subSpecialityCollection = [];
            $scope.subSpecialityCollection.selected = [];
            reset()
        } else if (val == "Listing") {
            $scope.showaddclinic = false;
            $scope.listingUsers = true;
            $scope.step1 = false;
            $scope.editStep1 = false;
            $scope.deleteStatus = false;
            $scope.enableSearch = true;
            if($scope.specialityCollection != undefined && $scope.specialityCollection.selected.length > 0){
                $scope.specialityCollection.selected = [];                
            }
            
            reset()
            loadRights()
            loadRoles()
            loadRolesList()
            specialitylisting()
            branchListing(val1)
        } else if (val == 'editUser') {
            $scope.showaddclinic = false;
            $scope.listingUsers = false;
            $scope.step1 = true;
            $scope.editStep1 = true;
            $scope.enableSearch = false;
            reset()
            $scope.userCollection.doctorDetails = $scope.selectedDoctor.doctor[0];
            $scope.userCollection.doctorDetails.FirstName = $scope.selectedDoctor.doctorOtherDetails.FirstName;
            $scope.userCollection.doctorDetails.LastName = $scope.selectedDoctor.doctorOtherDetails.LastName;
            $scope.userCollection.doctorDetails.DOB = $scope.userCollection.doctorDetails.DOB != undefined ? moment($scope.userCollection.doctorDetails.DOB).format('DD-MM-YYYY') : null;
            $scope.userCollection.DoctorPhone = ($scope.selectedDoctor.doctorPhone == undefined) ? DPhone : $scope.selectedDoctor.doctorPhone;
            $scope.userCollection.DoctorAddress = $scope.selectedDoctor.doctorAddress;
            $scope.userCollection.doctorOtherDetails.notes = $scope.selectedDoctor.doctorOtherDetails.notes;
            $scope.userCollection.doctorOtherDetails.isDeleted = "N";
            $scope.userCollection.DoctorAddress[0].ExtDetails.MinPerCase = ($scope.selectedDoctor.doctorAddress[0].ExtDetails.MinPerCase == undefined) ? '' : $scope.selectedDoctor.doctorAddress[0].ExtDetails.MinPerCase.toString();
            // angular.forEach($scope.selectedDoctor.SpecilityDetails, function(i){
            // 	angular.forEach($scope.specialityCollection, function(j){
            // 		if(i.specialityID == j.specialityID){
            // 			j.isActive = true;
            // 			var arr = Object.values(i.SubSpecialityID);
            // 			angular.forEach(j.subspeciality, function(p) { p.isActive = false });
            // 			angular.forEach(arr, function(p){
            // 				j.subspeciality.filter(data => data.DoctorSpecialityID == p).forEach(q => q.isActive = true);
            // 			});
            // 			$scope.specialitydisplayCollection.push(j);
            // 		}
            // 	})
            // })
            // angular.forEach($scope.specialitydisplayCollection, function(i){
            // });

            $scope.statusSuccess.speciality += 1;
            if ($scope.statusSuccess.speciality == 2) {
                loadspeciality();
            }
            if ($scope.userCollection.DoctorAddress[0].ExtDetails.RateBelowTwenty || $scope.userCollection.DoctorAddress[0].ExtDetails.RateAboveTwenty) {
                $scope.rateType = 'Procedure';
            } else if ($scope.userCollection.DoctorAddress[0].ExtDetails.RatePerHour) {
                $scope.userCollection.DoctorAddress[0].ExtDetails.RateperHour = $scope.userCollection.DoctorAddress[0].ExtDetails.RatePerHour;
                $scope.rateType = 'Per Hour';
            }
        } else if (val == "addClinic") {
            reset()
            $scope.showaddclinic = true;
            $scope.newAddHospitalList = [];
            angular.forEach($scope.hospitalDoctorsList, function(hospitalDetials) {
                var isset = false;
                var hosaddress = $scope.selectedDoctor.doctorAddress.filter(val => val.Active == 'Y');
                angular.forEach(hosaddress, function(doctorItem) {
                    if (hospitalDetials.HospitalInfo.HospitalDetails.HospitalID == doctorItem.ExtDetails.HospitalID) {
                        isset = true;
                    }
                });
                if (!isset) {
                    $scope.newAddHospitalList.push(hospitalDetials);
                }
            })
            $scope.addHospital = {
                "newclinic": ''
            };

            if ($scope.newAddHospitalList.length == 0) {
                alertify.error("You have already added to all Locations");
                $scope.showaddclinic = false;
                return;
            } else {
                $scope.userCollection.doctorDetails = $scope.selectedDoctor.doctor[0];
                $scope.userCollection.doctorDetails.DOB = $scope.userCollection.doctorDetails.DOB != undefined ? moment($scope.userCollection.doctorDetails.DOB).format("YYYY-MM-DD") : null;
                $scope.userCollection.DoctorPhone = $scope.selectedDoctor.doctorPhone;
                $scope.userCollection.DoctorAddress = $scope.selectedDoctor.doctorAddress;
                $scope.userCollection.doctorOtherDetails = $scope.selectedDoctor.doctorOtherDetails;
                $scope.userCollection.SpecialityDetails = $scope.selectedDoctor.SpecilityDetails;
                $scope.userCollection.doctorDetails.FirstName = $scope.selectedDoctor.doctorOtherDetails.FirstName;
                $scope.userCollection.doctorDetails.LastName = $scope.selectedDoctor.doctorOtherDetails.LastName;
            }
        } else if (val == 'DeleteClinic') {
            reset()
            $scope.showaddclinic = false;
            $scope.userCollection.doctorDetails = $scope.selectedDoctor.doctor[0];
            $scope.userCollection.doctorDetails.FirstName = $scope.selectedDoctor.doctorOtherDetails.FirstName;
            $scope.userCollection.doctorDetails.LastName = $scope.selectedDoctor.doctorOtherDetails.LastName;
            $scope.userCollection.doctorDetails.DOB = $scope.userCollection.doctorDetails.DOB != undefined ? moment($scope.userCollection.doctorDetails.DOB).format("YYYY-MM-DD") : null;
            $scope.userCollection.DoctorPhone = $scope.selectedDoctor.doctorPhone;
            $scope.userCollection.SpecialityDetails = $scope.selectedDoctor.SpecilityDetails;
            $scope.userCollection.DoctorAddress = $scope.selectedDoctor.doctorAddress;
            $scope.userCollection.doctorOtherDetails = $scope.selectedDoctor.doctorOtherDetails;
            $scope.userCollection.doctorOtherDetails = { isDeleted: 'N' };
            angular.forEach($scope.userCollection.DoctorAddress, function(item) {
                if (item.ExtDetails.HospitalID == $scope.newHospitalID)
                    item.Active = "N";
            })
            $scope.addUpdateDoctor()
        } else {
            reset()
            $scope.showaddclinic = false;
            $scope.userCollection.doctorDetails = $scope.selectedDoctor.doctor[0];
            $scope.userCollection.doctorDetails.FirstName = $scope.selectedDoctor.doctorOtherDetails.FirstName;
            $scope.userCollection.doctorDetails.LastName = $scope.selectedDoctor.doctorOtherDetails.LastName;
            $scope.userCollection.doctorDetails.DOB = $scope.userCollection.doctorDetails.DOB != undefined ? moment($scope.userCollection.doctorDetails.DOB).format("YYYY-MM-DD") : null;
            $scope.userCollection.DoctorPhone = $scope.selectedDoctor.doctorPhone;
            $scope.userCollection.SpecialityDetails = $scope.selectedDoctor.SpecilityDetails;
            $scope.userCollection.DoctorAddress = $scope.selectedDoctor.doctorAddress;
            $scope.userCollection.doctorOtherDetails = $scope.selectedDoctor.doctorOtherDetails;
            $scope.userCollection.doctorOtherDetails = { isDeleted: 'Y' };
            angular.forEach($scope.userCollection.DoctorAddress, function(item) {
                item.Active = "N";
            })
            $scope.userCollection.doctorDetails.DoctorLoginID = undefined;
            $scope.addUpdateDoctor()
        }
    }

    $scope.pageChange('Listing');
    $scope.getDeleteDoctorDetails = function(doctordetails) {
        alertify.confirm("<b class='txt-centre'>Confirm !</b><h4 class='txt-body-centre'>Are you sure want to delete</h4> ", function() {

                if (doctordetails !== undefined) {
                    var params = { "DoctorID": doctordetails.DoctorID, "USRType": "ADMIN", "USRId": doctordetails.DoctorID };
                    c7AdminServices.getDoctor(params, onDoctorDeleteDetailsSuccess, onAddUpdateError);

                    $scope.deleteStatus = true;

                }
            },
            function() {
                $scope.deleteStatus = false;
            });
    }

    function onDoctorDeleteDetailsSuccess(data) {
        $scope.selectedDoctor = data;
        $scope.selectedDocID = data.doctor[0].DoctorID;
        $scope.pageChange('delete')
    }
    $scope.roleFilter = function(val) {
        if (val.RoleID == $scope.userCollection.DoctorAddress[0].ExtDetails.RoleID) {
            return val;
        }
    }
    $scope.changeRole = function() {
        $scope.userCollection.DoctorAddress[0].ExtDetails.GroupID = "";
        $scope.ListingDisplay.length = 0;
    }
    $scope.changeGroup = function(val) {
        $scope.groupRights = [];
        angular.forEach($scope.rolesListing, function(item) {
            if (item.GroupName == val) {
                $scope.groupRights.push(item.AccountAccessID);
            }
        });
        var rightsListVal = getJsonVal($scope.groupRights);
        $scope.ListingDisplay.length = 0;
        angular.forEach(rightsListVal, function(item) {
            angular.forEach($scope.accessLisingCollection, function(access) {
                if (item == access.AccessID) {
                    $scope.ListingDisplay.push(access);
                }
            })
        })

    }

    $scope.subSpecialityCollection = [];
    $scope.subSpecialityCollection.selected = [];
    $scope.statusSuccess = {
        speciality: 0
    }
    $scope.selectspeciality = function() {
        // $scope.specialityCollection.selected.isActive = true;
        // angular.forEach($scope.specialityCollection.selected.subspeciality, function(i){
        // 	i.isActive = true;
        // });
        // var index = $scope.specialitydisplayCollection.findIndex(data => data.specialityID == $scope.specialityCollection.selected.specialityID);
        // if (index == -1) {
        //  $scope.specialitydisplayCollection.push($scope.specialityCollection.selected);
        // } else {
        // 	$scope.specialitydisplayCollection[index].isActive = true;
        // }
        // $scope.specialityCollection.selected = {};

        angular.forEach($scope.specialityCollection.selected, function(i) {
            angular.forEach(i.subspeciality, function(j) {
                j.spelid = i.specialityID;
                j.speldesc = i.specialitydesc;
                if ($scope.subSpecialityCollection.findIndex(d => d.DoctorSpecialityID == j.DoctorSpecialityID) == -1) {
                    $scope.subSpecialityCollection.push(j);
                    // $scope.subSpecialityCollection.selected.push(j);
                }
            })
        });
        for (var i = 0; i < $scope.subSpecialityCollection.length; i++) {
            var index = $scope.specialityCollection.selected.findIndex(d => d.specialityID == $scope.subSpecialityCollection[i].spelid);
            if (index == -1) {
                $scope.subSpecialityCollection.splice(i, 1);
                i--;
            }
        }
        if ($scope.subSpecialityCollection.selected != undefined) {
            for (var i = 0; i < $scope.subSpecialityCollection.selected.length; i++) {
                var selindex = $scope.specialityCollection.selected.findIndex(d => d.specialityID == $scope.subSpecialityCollection.selected[i].spelid);
                if (selindex == -1) {
                    $scope.subSpecialityCollection.selected.splice(i, 1);
                    i--;
                }
            }
        }
    }


    function loadspeciality() {

        $scope.statusSuccess.speciality = 0;
        if ($scope.specialityCollection != undefined) {

            $scope.subSpecialityCollection = [];
            $scope.specialityCollection.selected = [];
            $scope.subSpecialityCollection.selected = [];
            var curSpel = angular.copy($scope.selectedDoctor.SpecilityDetails);
            angular.forEach(curSpel, function(i) {
                var data = $scope.specialityCollection.filter(data => data.specialityID == i.specialityID);
                $scope.specialityCollection.selected = $scope.specialityCollection.selected.concat(data);
                angular.forEach($scope.specialityCollection.selected, function(x) {
                    angular.forEach(x.subspeciality, function(y) {
                        y.spelid = x.specialityID;
                        if ($scope.subSpecialityCollection.findIndex(d => d.DoctorSpecialityID == y.DoctorSpecialityID) == -1) {
                            $scope.subSpecialityCollection.push(y);
                        }
                    })
                })
                var arr = Object.values(i.SubSpecialityID);
                angular.forEach(arr, function(p) {
                    var subdata = $scope.subSpecialityCollection.filter(data => data.DoctorSpecialityID == p);
                    angular.forEach(subdata, function(x) {
                        if ($scope.subSpecialityCollection.selected.findIndex(d => d.DoctorSpecialityID == x.DoctorSpecialityID) == -1) {
                            $scope.subSpecialityCollection.selected.push(x);
                        }
                    })
                });

            });
        }
    }

    function fillspeciality() {
        $scope.newSpecialityCollection = [];
        angular.forEach($scope.specialityCollection.selected, function(i) {
            var params = {
                specialityDesc: i.specialitydesc,
                specialityID: i.specialityID,
                ActiveIND: 'Y',
                SubSpecialityID: {}
            }
            if (i.id != undefined) {
                params.id = i.id;
            }
            angular.forEach(i.subspeciality, function(k, key) {
                if ($scope.subSpecialityCollection.selected.findIndex(p => p.DoctorSpecialityID == k.DoctorSpecialityID) > -1) {
                    params.SubSpecialityID[key] = k.DoctorSpecialityID;
                }
            })
            $scope.newSpecialityCollection.push(params);
        })
    }



    // $scope.specialityReq = {
    // 	speciality: true,
    // 	subspeciality: true
    // }
    //  $scope.specialityCheck = function() {
    //  	if ($scope.specialitydisplayCollection.filter(data => data.isActive == true).length == 0) {
    // $scope.specialityReq.speciality = true;
    //  	} else {
    // $scope.specialityReq.speciality = false;
    //  	}
    //  	var skip = true;
    //  	angular.forEach($scope.specialitydisplayCollection, function(i){
    // if (skip && i.isActive) {
    //  			if(i.subspeciality.filter(data => data.isActive == true).length == 0){
    //  				$scope.specialityReq.subspeciality = true;
    //  			} else {
    //  				$scope.specialityReq.subspeciality = false;
    //  				skip = false;  
    //  			}
    //  		}
    //  	})
    //  }


    function getJsonVal(accessList) {
        var data = [];
        for (var property in accessList[0]) {
            data.push(accessList[0][property]);
        }
        return data;
    }

    $scope.addUpdateDoctor = function() {

        // $scope.specialityCheck();
        if ($scope.step1 && !$scope.editStep1) {
            // if($scope.userCollection.DoctorAddress[0].ExtDetails.RoleID == 'SONOGRAPHER' && $scope.specialityReq.speciality){
            // 	alertify.error('Choose Speciality');
            // 	return false;
            // }
            // if($scope.userCollection.DoctorAddress[0].ExtDetails.RoleID == 'SONOGRAPHER' && $scope.specialityReq.subspeciality){
            // 	alertify.error('Choose Sub Speciality');
            // 	return false;
            // }
            fillspeciality()
            $scope.userCollection.DoctorAddress[0].ClinicName = $scope.addHospital.clinic.HospitalInfo.HospitalDetails.HospitalName;
            $scope.userCollection.DoctorAddress[0].Address1 = $scope.addHospital.clinic.HospitalInfo.HospitalAddress.Address1;
            $scope.userCollection.DoctorAddress[0].Address2 = $scope.addHospital.clinic.HospitalInfo.HospitalAddress.Address2;
            $scope.userCollection.DoctorAddress[0].Street = $scope.addHospital.clinic.HospitalInfo.HospitalAddress.Address3;
            $scope.userCollection.DoctorAddress[0].Address4 = $scope.addHospital.clinic.HospitalInfo.HospitalAddress.Address4;
            $scope.userCollection.DoctorAddress[0].State = $scope.addHospital.clinic.HospitalInfo.HospitalAddress.State;
            $scope.userCollection.DoctorAddress[0].CityTown = $scope.addHospital.clinic.HospitalInfo.HospitalAddress.CityTown;
            $scope.userCollection.DoctorAddress[0].Country = $scope.addHospital.clinic.HospitalInfo.HospitalAddress.Country;
            $scope.userCollection.DoctorAddress[0].PostCode = $scope.addHospital.clinic.HospitalInfo.HospitalAddress.PostCode;
            $scope.userCollection.DoctorAddress[0].ExtDetails.HospitalID = $scope.addHospital.clinic.HospitalInfo.HospitalDetails.HospitalID;
            $scope.userCollection.DoctorAddress[0].ExtDetails.Longitude = $scope.addHospital.clinic.HospitalInfo.HospitalAddress.Longitude;
            $scope.userCollection.DoctorAddress[0].ExtDetails.Latitude = $scope.addHospital.clinic.HospitalInfo.HospitalAddress.Latitude;
            $scope.userCollection.DoctorAddress[0].ExtDetails.HospitalInfo.HospitalID = $scope.addHospital.clinic.HospitalInfo.HospitalDetails.HospitalID;
            $scope.userCollection.DoctorAddress[0].ExtDetails.HospitalInfo.HospitalCode = $scope.addHospital.clinic.HospitalInfo.HospitalDetails.HospitalCode;
            $scope.userCollection.DoctorAddress[0].ExtDetails.HospitalInfo.BranchCode = $scope.addHospital.clinic.HospitalInfo.HospitalDetails.BranchCode;
            $scope.userCollection.DoctorAddress[0].ExtDetails.MinPerCase = parseFloat($scope.userCollection.DoctorAddress[0].ExtDetails.MinPerCase);
            $scope.userCollection.doctorDetails.DOB = $scope.userCollection.doctorDetails.DOB != null ? moment($scope.userCollection.doctorDetails.DOB, 'DD-MM-YYYY').format("YYYY-MM-DD") : null;
            $scope.userCollection.DoctorPhone[0].PhNumber = $scope.userCollection.DoctorPhone[0].PhNumber;
            $scope.userCollection.DoctorPhone[1].PhNumber = $scope.userCollection.DoctorPhone[1].PhNumber;
            $scope.userCollection.SpecialityDetails = $scope.newSpecialityCollection;
            $scope.userCollection.DoctorAddress[0].ExtDetails.RateBelowTwenty = parseFloat($scope.userCollection.DoctorAddress[0].ExtDetails.RateBelowTwenty);
            $scope.userCollection.DoctorAddress[0].ExtDetails.RateAboveTwenty = parseFloat($scope.userCollection.DoctorAddress[0].ExtDetails.RateAboveTwenty);
            $scope.userCollection.DoctorAddress[0].ExtDetails.RateperHour = parseFloat($scope.userCollection.DoctorAddress[0].ExtDetails.RateperHour);
        } else if ($scope.showaddclinic) {
            $scope.AddClinicDoc()
            $scope.userCollection.doctorDetails.DoctorLoginID = undefined;
            $scope.userCollection.DoctorAddress = $scope.newDoctorAddress;
        } else if ($scope.editStep1 && $scope.step1) {
            // if($scope.userCollection.DoctorAddress[0].ExtDetails.RoleID == 'SONOGRAPHER' && $scope.specialityReq.speciality){
            // 	alertify.error('Choose Speciality');
            // 	return false;
            // }
            // if($scope.userCollection.DoctorAddress[0].ExtDetails.RoleID == 'SONOGRAPHER' && $scope.specialityReq.subspeciality){
            // 	alertify.error('Choose Sub Speciality');
            // 	return false;
            // }
            angular.forEach($scope.userCollection.DoctorAddress, function(k) {
                k.ExtDetails.RoleID = $scope.userCollection.DoctorAddress[0].ExtDetails.RoleID;
                k.ExtDetails.GroupID = $scope.userCollection.DoctorAddress[0].ExtDetails.GroupID;
            })
            fillspeciality()
            $scope.userCollection.doctorDetails.DoctorLoginID = undefined;
            $scope.userCollection.doctorDetails.DOB = $scope.userCollection.doctorDetails.DOB != null ? moment($scope.userCollection.doctorDetails.DOB, 'DD-MM-YYYY').format("YYYY-MM-DD") : null;
            $scope.userCollection.DoctorPhone[0].PhNumber = $scope.userCollection.DoctorPhone[0].PhNumber;
            $scope.userCollection.DoctorPhone[1].PhNumber = $scope.userCollection.DoctorPhone[1].PhNumber;
            $scope.userCollection.SpecialityDetails = $scope.newSpecialityCollection;
            $scope.userCollection.DoctorAddress[0].ExtDetails.MinPerCase = parseFloat($scope.userCollection.DoctorAddress[0].ExtDetails.MinPerCase);
            $scope.userCollection.DoctorAddress[0].ExtDetails.RateBelowTwenty = parseFloat($scope.userCollection.DoctorAddress[0].ExtDetails.RateBelowTwenty);
            $scope.userCollection.DoctorAddress[0].ExtDetails.RateAboveTwenty = parseFloat($scope.userCollection.DoctorAddress[0].ExtDetails.RateAboveTwenty);
            $scope.userCollection.DoctorAddress[0].ExtDetails.RateperHour = parseFloat($scope.userCollection.DoctorAddress[0].ExtDetails.RateperHour);
        }

        if($scope.userCollection.DoctorAddress[0].ExtDetails.RoleID == 'SONOGRAPHER' && $scope.userCollection.SpecialityDetails.length <= 0){
            alertify.error("Please select speciality");
            return;
        }

        if ($scope.step1 && !$scope.editStep1) {
            referraldoctor.newadddoctor($scope.userCollection, onAddUpdateSuccess, onAddUpdateError);
        } else {
            referraldoctor.newupdateDoctor($scope.userCollection, onAddUpdateSuccess, onAddUpdateError);
        }

    }

    function onAddUpdateSuccess(data) {
        if (data == "ALREADY_LOGINID_EXISTS") {
            alertify.error("User id already exist create with some other id.");
            return;
        } else if (data == "INVALID_EMAIL_ID") {
            alertify.error("Invalid email Id");
            return;
        } else if ($scope.step1 && !$scope.editStep1) {
            alertify.success('User added successfully')
        } else if ($scope.editStep1 && $scope.step1) {
            alertify.success('User Updated successfully')
        } else if ($scope.showaddclinic) {
            alertify.success('Locations assigned successfully')
        } else if ($scope.changeValue == 'DeleteClinic') {
            alertify.success('Locations Deleted successfully')
        } else {
            alertify.success('User deleted successfully')
        }
        $scope.pageChange('Listing', $scope.New.selectBranch.HospitalInfo.HospitalDetails.HospitalID);
    }

    // function fillspeciality(){
    // 	$scope.newSpecialityCollection = [];
    // 	angular.forEach($scope.specialitydisplayCollection, function(i){
    // 		if(i.isActive == true){
    // 			var params = {
    // 				"specialityID": i.specialityID,
    // 				"specialityDesc": i.specialitydesc,
    // 				"ActiveIND": i.activeIND,
    // 				"SubSpecialityID": {}
    // 			}
    // 			angular.forEach(i.subspeciality, function (k, key) {
    // 				if (k.isActive == true) {
    // 					params.SubSpecialityID[key] = k.DoctorSpecialityID;
    // 				}
    // 			})
    // 			$scope.newSpecialityCollection.push(params);
    // 		}
    // 	})
    // }
    /* Assign hospital to the doctor */
    $scope.AddClinicDoc = function() {
        var newCollection = angular.copy($scope.userCollection.DoctorAddress);
        if ($scope.addHospital.newclinic != '') {
            var tempaddHospital = JSON.parse($scope.addHospital.newclinic);
            var params = {
                "Active": "Y",
                "ClinicName": tempaddHospital.HospitalInfo.HospitalDetails.HospitalName,
                "ExtDetails": {
                    "MinPerCase": parseFloat(newCollection[0].ExtDetails.MinPerCase),
                    "HospitalID": tempaddHospital.HospitalInfo.HospitalDetails.HospitalID,
                    "PrimaryIND": "Y",
                    "RoleID": newCollection[0].ExtDetails.RoleID,
                    "GroupID": newCollection[0].ExtDetails.GroupID,
                    "Latitude": tempaddHospital.HospitalInfo.HospitalAddress.Latitude,
                    "Longitude": tempaddHospital.HospitalInfo.HospitalAddress.Longitude,
                    "HospitalInfo": {
                        "HospitalID": tempaddHospital.HospitalInfo.HospitalDetails.HospitalID,
                        "HospitalCode": tempaddHospital.HospitalInfo.HospitalDetails.HospitalCode,
                        "BranchCode": tempaddHospital.HospitalInfo.HospitalDetails.BranchCode
                    },
                    "RateBelowTwenty": parseFloat(newCollection[0].ExtDetails.RateBelowTwenty),
                    "RateAboveTwenty": parseFloat(newCollection[0].ExtDetails.RateAboveTwenty),
                    "RateperHour": parseFloat(newCollection[0].ExtDetails.RateperHour),
                },
                "Country": tempaddHospital.HospitalInfo.HospitalAddress.Country,
                "Address1": tempaddHospital.HospitalInfo.HospitalAddress.Address1,
                "Address2": tempaddHospital.HospitalInfo.HospitalAddress.Address2,
                "Street": tempaddHospital.HospitalInfo.HospitalAddress.Address3,
                "Address4": tempaddHospital.HospitalInfo.HospitalAddress.Address4,
                "AddressType": "B",
                "State": tempaddHospital.HospitalInfo.HospitalAddress.State,
                "CityTown": tempaddHospital.HospitalInfo.HospitalAddress.CityTown,
                "Location": "",
                "PostCode": tempaddHospital.HospitalInfo.HospitalAddress.PostCode
            }
            $scope.newDoctorAddress = [];
            $scope.newDoctorAddress.push(params);
        } else {
            $scope.newDoctorAddress = [];
            angular.forEach($scope.newAddHospitalList, function(tempaddHospital) {
                var params = {
                    "Active": "Y",
                    "ClinicName": tempaddHospital.HospitalInfo.HospitalDetails.HospitalName,
                    "ExtDetails": {
                        "MinPerCase": parseFloat(newCollection[0].ExtDetails.MinPerCase),
                        "HospitalID": tempaddHospital.HospitalInfo.HospitalDetails.HospitalID,
                        "PrimaryIND": "Y",
                        "RoleID": newCollection[0].ExtDetails.RoleID,
                        "GroupID": newCollection[0].ExtDetails.GroupID,
                        "Latitude": tempaddHospital.HospitalInfo.HospitalAddress.Latitude,
                        "Longitude": tempaddHospital.HospitalInfo.HospitalAddress.Longitude,
                        "HospitalInfo": {
                            "HospitalID": tempaddHospital.HospitalInfo.HospitalDetails.HospitalID,
                            "HospitalCode": tempaddHospital.HospitalInfo.HospitalDetails.HospitalCode,
                            "BranchCode": tempaddHospital.HospitalInfo.HospitalDetails.BranchCode
                        },
                        "RateBelowTwenty": parseFloat(newCollection[0].ExtDetails.RateBelowTwenty),
                        "RateAboveTwenty": parseFloat(newCollection[0].ExtDetails.RateAboveTwenty),
                        "RateperHour": parseFloat(newCollection[0].ExtDetails.RateperHour),
                    },
                    "Country": tempaddHospital.HospitalInfo.HospitalAddress.Country,
                    "Address1": tempaddHospital.HospitalInfo.HospitalAddress.Address1,
                    "Address2": tempaddHospital.HospitalInfo.HospitalAddress.Address2,
                    "Street": tempaddHospital.HospitalInfo.HospitalAddress.Address3,
                    "Address4": tempaddHospital.HospitalInfo.HospitalAddress.Address4,
                    "AddressType": "B",
                    "State": tempaddHospital.HospitalInfo.HospitalAddress.State,
                    "CityTown": tempaddHospital.HospitalInfo.HospitalAddress.CityTown,
                    "Location": "",
                    "PostCode": tempaddHospital.HospitalInfo.HospitalAddress.PostCode
                }
                $scope.newDoctorAddress.push(params);
            })
        }
    }

    $scope.rateintype = function() {
        $scope.userCollection.DoctorAddress[0].ExtDetails.RateBelowTwenty = '';
        $scope.userCollection.DoctorAddress[0].ExtDetails.RateAboveTwenty = '';
        $scope.userCollection.DoctorAddress[0].ExtDetails.RateperHour = '';
    }

    $scope.getDeleteClinicDetails = function(doctordetails) {
        $scope.newHospitalID = doctordetails;
        alertify.confirm("<b class='txt-centre'>Confirm !</b><h4 class='txt-body-centre'>Are you sure want to delete</h4> ", function() {
                $scope.pageChange('DeleteClinic')
            },
            function() {

            });
    }
    $scope.deleteUsersClinic = function(addressid, hospitalis, roleid) {

        var scDeleteParam = {
            Active: "N",
            AddressID: addressid,
            HospitalID: hospitalis,
            DoctorID: $scope.selectedDoctor.doctor[0].DoctorID,
            RoleID: roleid
        }

        alertify.confirm("<b class='txt-centre'>Confirm !</b><h4 class='txt-body-centre'>Are you sure want to delete</h4> ", function() {
                c7AdminServices.deactiveDoctor(scDeleteParam, scDeleteSuccess, scDeleteError);
            },
            function() {

            });

        function scDeleteSuccess(data) {
            if (!data.error) {
                if (data == "Feature Schedule available for this Sonographer") {
                    alertify.error("Feature Schedule available for this Sonographer");
                } else if (data.Identity != undefined) {
                    $scope.selectedDoctor = data;
                }
            }
        }

        function scDeleteError(data) {
            alertify.error(data);
        }
    }
}]);