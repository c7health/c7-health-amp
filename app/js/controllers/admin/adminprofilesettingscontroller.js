hCueDoctorWebControllers.controller('hCueAdminProfileSettingsController', ['$scope','$window','hcueDoctorLoginService','c7AdminServices','$filter','NgMap','alertify',function($scope,$window,hcueDoctorLoginService,c7AdminServices,$filter,NgMap,alertify) {

	//Config variables
	var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
	var parentHospitalId;
	// var hospitalCode;
	var hospitalId;
	$scope.newRoleId = 'DOCTOR';
	if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined){
		parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
		hospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
		// hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
		$scope.newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
	}
	$scope.rolesCollection = {};
	$scope.AllCheck = {};
	$scope.ReadOnly = "N";
	$scope.groupName = {
		"group":""
	} 
	$scope.roleName = {
		"role":""
	};
	$scope.newDeleteRolepupop = false;
	$scope.edit_status = false;
	$scope.delete_status = false;
	$scope.selectAllStatus = true;
	$scope.deselectAllStatus = false;
	$scope.selectGroupCode = '';
	function loadRights() {
		var params = {
		    "USRType": $scope.newRoleId,
			"USRId":doctorinfo.doctorid,
			"hospitalId" :parentHospitalId
		};
		c7AdminServices.listHcueAccessRights(params,onLoadSuccess,onProfileSettingError);
	}

	function onLoadSuccess(data) {
		$scope.rightsList = data;
	}


	function loadRoles() {
		var rolesParams = {"ParentHospitalID":parentHospitalId,"USRId":doctorinfo.doctorid,"USRType":"ADMIN","ActiveIND":"Y"}
		c7AdminServices.listHospitalRole(rolesParams,onLoadRoleSuccess,onProfileSettingError);
	}

	function onLoadRoleSuccess(data) {
		$scope.rolesList = data;
	}

	function loadRolesList() {
		c7AdminServices.listProfile(parentHospitalId,onListLoadRoleSuccess,onProfileSettingError);
	}

	function onListLoadRoleSuccess(data) {
		$scope.rolesListing = data.Roles;
	}

	function onListLoadRoleError(data) {
		console.log("Listing the roles error " + JSON.stringify(data));
	}


	loadRights();

 	loadRoles();

 	loadRolesList();

 	$scope.selectAll = function() {
 		$scope.selectAllStatus = false;
 		$scope.deselectAllStatus = true;
 		angular.forEach($scope.rightsList,function(values) {
 			$scope.AllCheck[values.AccessCategoryDesc] = "Y";
 			angular.forEach(values.AccessRights,function(items) {
 				$scope.rolesCollection[items.AccessID] = "Y";
 			})
 		})
 	}


 	$scope.deselectAll = function() {
 		$scope.selectAllStatus = true;
 		$scope.deselectAllStatus = false;
 		angular.forEach($scope.rightsList,function(values) {
 			$scope.AllCheck[values.AccessCategoryDesc] = "N";
 			angular.forEach(values.AccessRights,function(items) {
 				$scope.rolesCollection[items.AccessID] = "N";
 			})
 		})
 	}

 	$scope.CheckAll = function (val, check) {
 	    $scope.selectAllStatus = true;
		angular.forEach($scope.rightsList,function(values) {
			if(values.AccessCategoryDesc == check) {
	 			angular.forEach(values.AccessRights,function(items) {
	 				angular.forEach($scope.AllCheck,function(key,innerval) {
	 					if(innerval == check) {
	 						$scope.rolesCollection[items.AccessID] = key;
	 					}
	 				})
	 			})
			}
		})
 	} 
 	$scope.editRoles = function (values) {
 	    if (values.RoleID == 'ADMIN') {
 	        return;
 	    }
 	    $scope.selectAllStatus = true;
 	    $scope.deselectAllStatus = false;
 	    
 	    angular.forEach($scope.rightsList, function (i) {
 	        var newadmincount = 0;
 	        var admincount = 0;
 	        $scope.AllCheck[i.AccessCategoryDesc] = "N";
 	            newadmincount = i.AccessRights.length;
 	            angular.forEach(i.AccessRights, function (items) {
 	                $scope.rolesCollection[items.AccessID] = "N";
 	                angular.forEach(values.AccountAccessID, function (data) {
 	                    if (items.AccessID == data) {
 	                        admincount = admincount + 1;
 	                    }
 	                })
 	            })
 	        if (newadmincount == admincount) {
 	            $scope.AllCheck[i.AccessCategoryDesc] = "Y";
 	        }
 	    })
 		$scope.edit_status = true;
 		$scope.rolesCollection = {};
 		$scope.groupName.group = values.GroupName;
 		$scope.groupCodeVal = values.GroupCode;
 		$scope.selectGroupCode = $scope.groupCodeVal;
 		$scope.roleName.role = values.RoleID;
 		angular.forEach(values.AccountAccessID,function(item) {
 			$scope.rolesCollection[item] = "Y";
 		})
 		//$scope.rolesCollection
 	} 

 	$scope.deleteRole = function (values) {
 	    $window.scrollTo(0, angular.element(document.getElementById('chekin')).offsetTop);
 	    $scope.newDeleteRolepupop = true;
 	        $scope.newValue = values;
 	        
 	    
 	}
 	$scope.newDeletepupopOk = function () {
 	    $scope.delete_status = true;
 	    $scope.edit_status = false;
 	    $scope.newDeleteRolepupop = false;
 	    $scope.addUpdateRole($scope.newValue);
 	    
 	}
 	$scope.newDeletepupopCancel = function () {
 	    $scope.newDeleteRolepupop = false;
 	}
	//Submit Add / Update Role

	$scope.addUpdateRole = function(values) {
		if(!$scope.delete_status) {
				
			if($scope.roleName.role == "") {
				alertify.error("Role name cannot be empty");
				return;
			}

			if($scope.groupName.group == "") {
				alertify.error("Group name cannot be empty");
				return;
			}

			if(Object.keys($scope.rolesCollection).length == 0) {
				alertify.error("Please set the role to the group");
				return;
			}
		}


		if(values == undefined) {
			var addUpdateParams = {
				"GroupName": $scope.groupName.group,
			    "USRType": "DOCTOR",
			    "Active": "Y",
			    "HospitalID": hospitalId,
			    "GroupCode": 0,
			    "USRId": doctorinfo.doctorid,
			    "RoleID": $scope.roleName.role,
			    "ReadOnly": "N"
			}

			if($scope.edit_status) {
				addUpdateParams.GroupCode = $scope.groupCodeVal;
			}
			addUpdateParams.AccountAccessID = getKeyPairValue($scope.rolesCollection);
		}
		else {
			var addUpdateParams = values;
			addUpdateParams.Active = "N";
			addUpdateParams.HospitalID = parentHospitalId;
			addUpdateParams.USRType = "ADMIN";
			addUpdateParams.USRId = doctorinfo.doctorid;
		}
		c7AdminServices.addUpdateProfile(addUpdateParams, onAddUpdateSuccess, onProfileSettingError);
	}

	function onAddUpdateSuccess(data) {
		if($scope.edit_status) {
			alertify.success("Profile updated successfully");
		}
		else if($scope.delete_status) {
			alertify.success("Profile deleted successfully");
		}
		else {
			alertify.success("Profile added successfully");
		}
 		loadRolesList();
		$scope.resetForm();
		$scope.delete_status = false;
	}


	$scope.resetForm = function() {
		$scope.rolesCollection = {};
		$scope.edit_status = false;
		$scope.delete_status = false;
		$scope.groupCodeVal = "";
 		$scope.groupName = {
			"group":""
		} 
		$scope.roleName = {
			"role":""
		};
		$scope.AllCheck = {};
 		$scope.selectAllStatus = true;
 		$scope.deselectAllStatus = false;
 		$scope.selectGroupCode = '';
	}

	function getKeyPairValue(val) {
		var items = {};
	    var rowno = 1;
	    angular.forEach(val, function (value, key) {
	    	if(value == "Y") {
		        items[rowno] = key;
		        rowno++;
	    	}
	    });
	    return items;
	}

	var checkAllStatus = [];
	var checkLengthAccess = [];

	angular.forEach($scope.rightsList,function(status) {
		angular.forEach(status.AccessRights,function(itemStatus) {
			checkLengthAccess.push(itemStatus);
		});
	});
	$scope.accessCheck = function(item,order) {
		//Get access list by id
		var accessListArray = [];
		var temp_arrayVal = [];
		var getParentVal;
		$scope.selectAllStatus = true;
		accessListArray.length = 0;
		temp_arrayVal.length = 0;
		angular.forEach($scope.rightsList,function(list) {
			if(list.AccessCategoryID == order.AccessCategoryID) {
				getParentVal = list.AccessCategoryDesc;
			}
			angular.forEach(list.AccessRights,function(accessList) {
				if(order.AccessCategoryID == accessList.AccessCategoryID) {
					accessListArray.push(accessList)
				}
			});
		});
		var listLength = accessListArray.length;

		angular.forEach(accessListArray,function(manageList) {
			angular.forEach($scope.rolesCollection,function(keyItem,valItem) {
				if(manageList.AccessID == valItem) {
					if(keyItem == "Y") {
						temp_arrayVal.push(valItem);
					}
				}
			})
		})

		if(temp_arrayVal.length == listLength) {
			$scope.AllCheck[getParentVal] = "Y";
		}
		else {
			$scope.AllCheck[getParentVal] = "N";
		}

	}

	$scope.popupClose = function() {
		$scope.showPopup = false;
	}



	$scope.showInfo = function(val) {
		$window.scrollTo(0, angular.element(document.getElementById('searchbyId')).offsetTop);
		$scope.showPopup = true;
		switch(val) {
			case 'Admin':
				$scope.info_title = "Admin",
				$scope.info_details = [{
					'Title':'Manage Branch',
					'Description1':'Create new clinic Branches (anywhere in India)',
					'Description2':'Edit Branches',
					'Description3':'Activate / Inactivate Branches'
				},{
					'Title':'Manage Users',
					'Description1':'Edit Users',
					'Description2':'Activate / Inactivate Users'
				}]
			break;
			case 'Appointment':
				$scope.info_title = "Appointment",
				$scope.info_details = [{
					'Title':'All Doctor List',
					'Description1':'View all doctor Appointments in all Clinics',
					'Description2':'This along with Book appointment below will give the user book appointments with all doctors.',
					'Description3':'Note: Specifically for Admins & Receptionists'
				},{
					'Title':'Book Appointment',
					'Description1':'Cancel Booked Appointments',
					'Description2':'Reschedule Appointments',
					'Description3':'Note: Specifically for Admins & Receptionists'
				},{
					'Title':'Billing - Appointment View',
					'Description1':'Add Billing',
					'Description2':'Print Invoice',
					'Description3':'Update Bill',
					'Description4':'Note: Specifically for Admins & Receptionists'
				},{
					'Title':'Capture Vitals - Appointment View',
					'Description1':'Add Vitals',
					'Description2':'Edit Vitals',
					'Description3':'Note: Specifically for Receptionists'
				},{
					'Title':'Update Patient Details',
					'Description1':'Update Existing Patient Details',
					'Description2':'Note: Specifically for Admins, Receptionists & Doctors',
					'Description3':''
				}]
			break;
			case 'Consultation':
				$scope.info_title = "Consultation",
				$scope.info_details = [{
					'Title':'Billing',
					'Description1':'Bill & Print receipt for the patient during consultation'
				},{
					'Title':'Start Consultation',
					'Description1':'Submit / Save Records for the Patient'
				}]
			break;
			case 'My Patients':
				$scope.info_title = "My Patients",
				$scope.info_details = [{
					'Title':'Register Patient',
					'Description1':'Register a new Patient in the system'
				},{
					'Title':'View Case History',
					'Description1':'View entire Case History of the Patient',
					'Description2':'Print Case History'
				}]
			break;
			case 'Settings':
				$scope.info_title = "Settings",
				$scope.info_details = [{
					'Title':'Vital Settings',
					'Description1':'Select / Unselect preferred Vitals to be displayed during consultation'
				},{
					'Title':'Medical History Settings',
					'Description1':'Add New Medical History which will be displayed during consultation',
					'Description2':'Edit / Remove currently added Medical History'
				},{
					'Title':'Visit Reason Settings',
					'Description1':'Add New Visit Reason (For ex. Fever, Knee Pain etc) which will be displayed during consultation',
					'Description2':'Edit / Remove currently added Visit Reason'
				},{
					'Title':'Diagnosis Settings',
					'Description1':'Add New Diagnosis Notes(For ex. Viral Infection, Malaria etc) which will be displayed during consultation',
					'Description2':'Edit / Remove currently added Diagnosis Notes'
				},{
					'Title':'Referral Source Settings',
					'Description1':'Add Patient Referral Source during registration (For Ex. Newspaper, Google, Flyers etc)',
					'Description2':'Edit / Remove currently added Referral Source'
				},{
					'Title':'Inventory Settings',
					'Description1':'Link a Pharmacy Inventory during Consultation',
					'Description2':'Kindly contact hCue Support @ 9543279999 or email us at support@hcue.co to know more about this	'
				}]
			break;
			case 'Others':
				$scope.info_title = "Others",
				$scope.info_details = [{
					'Title':'Edit Profile',
					'Description1':'The doctors to edit their current profile'
				},{
					'Title':'Communications',
					'Description1':'Send single / bulk emails to his patients',
					'Description2':'Send single / bulk sms\'s to his patients'
				},{
					'Title':'Care Team',
					'Description1':'Add doctors to your Clinic network',
					'Description2':'Add Pharmacies to your network',
					'Description3':'Add Labs to your netowrk'
        }]
			break;
			case 'Stats':
				$scope.info_title = "Stats",
				$scope.info_details = [{
					'Title':'Reports & Dashboard',
					'Description1':'View his Performance Reports'
				}]
			break;

		}
	}

	function onProfileSettingError(data) {
		console.log("Right Load error " + JSON.stringify(data));
	}
	
}]);