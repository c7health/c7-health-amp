hCueDoctorWebControllers.controller('hCueAdminMainController', ['$scope', 'datacookieFactory', 'hcueDoctorLoginService', 'hCueMainAdminServices', 'alertify', function($scope, datacookieFactory, hcueDoctorLoginService, hCueMainAdminServices, alertify) {

    //Show Header 
    var parent = this;
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === "true") {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    /* Access Settings */

    var access = doctorinfo.isAccessRightsApplicable
    $scope.showBranchAccess = !access;
    $scope.showProfileSettingsAccess = !access;
    $scope.showManageUserAccess = !access;
    $scope.showManageSchedulingAccess = !access;
    $scope.showManageCalendarAccess = !access;
    $scope.showManageSpecialitiesAccess = !access;



    if (access) {
        if (doctorinfo.AccountAccessID.indexOf('MANBRNS') !== -1) {
            $scope.showBranchAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('MANROLES') !== -1) {
            $scope.showProfileSettingsAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('MANUSRS') !== -1) {
            $scope.showManageUserAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('MNGSCHEDULE') !== -1) {
            $scope.showManageSchedulingAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('MNGCALENDER') !== -1) {
            $scope.showManageCalendarAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('SPECIALITY') !== -1) {
            $scope.showManageSpecialitiesAccess = true;
        }

    }

    /* End of access settings */
    $scope.activeMenu = "dashboard";
    $scope.tabActive = "dashboard";
    $scope.parentScope = {};
    //End of show header 

    $scope.activeTab = function(val) {
        datacookieFactory.remove('rememberDoctorSelection');
        $scope.activeMenu = val;
        $scope.tabActive = val;
    }

    parent.showPage = function(val) {
        $scope.activeMenu = val;
        $scope.tabActive = val;
    }
}]);