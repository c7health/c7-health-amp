hCueDoctorWebControllers.controller('hCueAdminFeedbackController', ['$scope','hcueDoctorLoginService','hCueMainAdminServices','$filter','NgMap','alertify',function($scope,hcueDoctorLoginService,hCueMainAdminServices,$filter,NgMap,alertify) {

	//Config variables
	var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
	var parentHospitalId;
	var hospitalCode;
	var hospitalId;
	if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined){
		parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
		hospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
		hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
	}
	$scope.currencyType = "INR";
	if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes !== undefined) {
	    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.currencyCode !== undefined) {
	        $scope.currencyType = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.currencyCode;
	    }
	}
	$scope.eng_lang = {
		'eng':'Y'
	};
	$scope.tam_lang = {
		'tam':'N'
	};
	$scope.hin_lang = {
		'hin':'N'
	};
	$scope.addNew = true;
	$scope.updateNew = false;
	$scope.lan = {
		'eng_lanText':'',
		'hin_lanText':'',
		'tam_lanText':''
	}
	$scope.languageSelectionCollection = [];
	var questionsValues = [];
	$scope.question_array = [];
	var existing_collection;
	var length;
    var row_id;
    var type;

	/* Load hospital & doctors */
    function branchListing(pageSize = 100) {
        var get_branches={
            "pageNumber": 1,
            "HospitalID": parseInt(parentHospitalId),
            "pageSize": parseInt(pageSize),
            "IncludeDoctors": "N",
            "IncludeBranch": "Y",
            "ActiveHospital":"Y",
            "ActiveDoctor":"Y"
        }; 

        hCueMainAdminServices.getHospitalDetails(get_branches,onSuccessBranches,onFailedBranches)
    }

    function onSuccessBranches(data) {
        $scope.hospitalList=data.rows;
        $scope.branchDetails = {
        	'branch':$scope.hospitalList[0]
        }
    }

    function onFailedBranches(data) {
        console.log("Branches list error " + JSON.stringify(data));
    }

    branchListing()
    /* End of load hospital & doctors */

    /* Edit and assing the values */

    $scope.editFeedbackQuestion = function (obj,index) {
		$scope.SelectedFeedbackObj = obj;
		$scope.questionValue = obj.questions;
		$scope.buttonName = "Update";
		$scope.addNew = false;
		$scope.updateNew = true;
		$scope.updateRowId = obj.rowid;
		angular.forEach($scope.questionValue,function(item) {
			if(item.lang == "eng") {
				$scope.eng_lang.eng = "Y";
				$scope.lan.eng_lanText = item.question;
			}

			if(item.lang == "tam") {
				$scope.tam_lang.tam = "Y";
				$scope.lan.tam_lanText = item.question;
			}

			if(item.lang == "hin") {
				$scope.hin_lang.hin = "Y";
				$scope.lan.hin_lanText = item.question;
			}

		})

    }

    $scope.deleteFeedbackQuestion = function(val) {
    	alertify.confirm("<b class='txt-centre'>Confirm !</b><h4 class='txt-body-centre'>Are you sure want to delete </h4>", function(){ 
	    	$scope.deleteStatus = true;
	    	$scope.deleteIndexId = val.rowid;
	    	$scope.deleteValue = val;
	    	$scope.addUpdate(1)
    	}, 
    	function(){ 

    	});
    }

    /* End of edit and assing the values */

    /* Update the feedback question */
    // type 0 -  Add/Update | type 1 - Delete
    $scope.addUpdate = function(deletetype = 0) {
    	if(!$scope.deleteStatus) {

	    	$scope.question_array.length = 0;
			if($scope.eng_lang.eng == 'Y' && $scope.lan.eng_lanText !== '') {
				$scope.question_array.push({
				    'lang':'eng',
				    'question':$scope.lan.eng_lanText
				})
			}

			if($scope.tam_lang.tam == 'Y' && $scope.lan.tam_lanText !== '') {
				$scope.question_array.push({
				    'lang':'tam',
				    'question':$scope.lan.tam_lanText
				})
			}

			if($scope.hin_lang.hin == 'Y' && $scope.lan.hin_lanText !== '') {
				$scope.question_array.push({
				    'lang':'hin',
				    'question':$scope.lan.hin_lanText
				})
			}

			if($scope.question_array.length == 0) {
				alertify.error('Add aleast one question');
				return;
			}

			if(!$scope.updateNew) {

				if($scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion == undefined) {
					//Add totally new feedback
					questionsValues.push({"rowid":0,"notes":"","rankvalue":0,"activeIND":"Y","questions":$scope.question_array})
					type = "new";
				}
				else if($scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion !== undefined && $scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion.questions.length == 0) {
					questionsValues.push({"rowid":0,"notes":"","rankvalue":0,"activeIND":"Y","questions":$scope.question_array})
					type = "new";
				}
				else {
					//Add new feedback to the existing collection
					length = $scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion.questions.length;
		            row_id = $scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion.questions[length-1].rowid;
		            row_id = row_id+1;
		            existing_collection = $scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion.questions

		            if(existing_collection.length >= 6) {
		                alertify.error('You cannot add more than 6 questions');
		                return;
		            }
		            $scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion.questions.push({
		                "rowid":row_id,
		                "notes":"",
		                "rankvalue":0,
		                "activeIND":"Y",
		                "questions":$scope.question_array
		            })
		            type = "exisitng";
				}
			}
			else {
				type = "exisitngUpdate";
				angular.forEach($scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion.questions,function(updateItem) {
	                if(updateItem.rowid == $scope.updateRowId) {
	                    updateItem.questions = $scope.question_array;
	                }
	            })
			}
    	}
		// Params
		var params = {
             "USRType": "ADMIN",
             "USRId": parseInt(doctorinfo.doctordata.doctor[0].DoctorID),
             "HospitalAddress": {
                 "State": "TN",
                 "Country": "IND",
                 "Address1": $scope.branchDetails.branch.HospitalInfo.HospitalAddress.Address1,
                 "Address2": $scope.branchDetails.branch.HospitalInfo.HospitalAddress.Address1,
                 "CityTown": $scope.branchDetails.branch.HospitalInfo.HospitalAddress.CityTown,
                 "DistrictRegion": $scope.branchDetails.branch.HospitalInfo.HospitalAddress.DistrictRegion,
                 "PinCode": parseInt($scope.branchDetails.branch.HospitalInfo.HospitalAddress.PinCode),
                 "Location": $scope.branchDetails.branch.HospitalInfo.HospitalAddress.Location
             },
             "HospitalDetails": {
                 "RegsistrationDate": Date.parse($scope.branchDetails.branch.HospitalInfo.HospitalDetails.RegsistrationDate),
                 "Account": {
                     "1": "PLUS"
                 },
                 "AboutHospital": $scope.branchDetails.branch.HospitalInfo.HospitalDetails.HospitalName,
                 "LicenseNumber": $scope.branchDetails.branch.HospitalInfo.HospitalDetails.LicenseNumber,
                 "HospitalID": $scope.branchDetails.branch.HospitalInfo.HospitalDetails.HospitalID,
                 "ParentHospitalID": parentHospitalId,
                 "TINNumber": $scope.branchDetails.branch.HospitalInfo.HospitalDetails.TINNumber,
                 "EmailAddress": $scope.branchDetails.branch.HospitalInfo.HospitalDetails.EmailAddress,
                 "MobileNumber": parseInt($scope.branchDetails.branch.HospitalInfo.HospitalDetails.MobileNumber),
                 "TermsAccepted": "Y",
                 "WebSite": "pearldental.com",
                 "ActiveIND": $scope.branchDetails.branch.HospitalInfo.HospitalDetails.ActiveIND,
                 "HospitalName": $scope.branchDetails.branch.HospitalInfo.HospitalDetails.HospitalName,
                 "ContactName": $scope.branchDetails.branch.HospitalInfo.HospitalDetails.ContactName,
                 "FeedBackQuestion":{}
             }
         };

		if(!$scope.updateNew && type == "exisitng") {
			params.FeedBackQuestion = $scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion;
		}
		else if(!$scope.updateNew && type == "new") {
			var lan_params = {
                "langCount": 3,
                "lang":['eng','tam','hin'],
                "questions":questionsValues
            }
			params.FeedBackQuestion = lan_params;
		}
		else if(type == "exisitngUpdate"){
			params.FeedBackQuestion = $scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion;
		}

		if(deletetype == 1 && $scope.deleteStatus != undefined) {
			/*angular.forEach($scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion.questions,function(deleteItem) {
                if(deleteItem.rowid == $scope.deleteIndexId) {
                    deleteItem.activeIND = 'N';
                }
            })	
            params.FeedBackQuestion = $scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion;*/
			
			var index_val = $scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion.questions.indexOf($scope.deleteValue);
			var delete_paramval = $scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion.questions.splice(index_val,1);
			//params.FeedBackQuestion={};
			params.FeedBackQuestion = $scope.branchDetails.branch.HospitalInfo.HospitalDetails.FeedBackQuestion;
		}

        hCueMainAdminServices.UpdateBranch(params, updateBranchSuccess, updateBranchError);

    }

    function updateBranchSuccess(data) {
		if (!$scope.updateNew && !$scope.deleteStatus) {
			alertify.success("Feedback question added successfully");
		} 
		else if($scope.deleteStatus) {
			alertify.success("Feedback question deleted successfully");
		}
		else {
			alertify.success("Feedback question updated successfully");
		}
		$scope.addNew = true;
		$scope.updateNew = false;
		$scope.deleteStatus = false;
    	$scope.deleteIndexId = undefined;
		$scope.lan = {
			'eng_lanText':'',
			'hin_lanText':'',
			'tam_lanText':''
		}
    	branchListing();
    	questionsValues.length = 0;
		$scope.eng_lang.eng='Y';
		$scope.tam_lang.tam='N';
		$scope.hin_lang.hin='N';
		$scope.question_array.length = 0;
    }

    function updateBranchError(data) {
    	console.log("Update feedback error " + JSON.stringify(data));
    }


    /* End of update the feedback questions */
    $scope.changeBranch = function() {
    	$scope.lan = {
			'eng_lanText':'',
			'hin_lanText':'',
			'tam_lanText':''
		}
    }

    $scope.resetForm = function() {

		$scope.addNew = true;
		$scope.updateNew = false;
		$scope.deleteStatus = false;
    	$scope.deleteIndexId = undefined;
		$scope.lan = {
			'eng_lanText':'',
			'hin_lanText':'',
			'tam_lanText':''
		}
		$scope.eng_lang.eng='Y';
		$scope.tam_lang.tam='N';
		$scope.hin_lang.hin='N';
    }

    $scope.languageSelection = function(lan,val) {
    	
    }


}]); 