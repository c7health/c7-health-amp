hCueDoctorWebControllers.controller('adminSchedulingController', [
  '$scope',
  '$filter',
  'NgMap',
  'alertify',
  'c7AdminServices',
  'hcueDoctorLoginService',
  'referralregion',
  function(
    $scope,
    $filter,
    NgMap,
    alertify,
    c7AdminServices,
    hcueDoctorLoginService,
    referralregion
  ) {
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (
      doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !==
        undefined &&
      doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo
        .ParentHospitalID !== undefined
    ) {
      parentHospitalId =
        doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo
          .ParentHospitalID;
      hospitalCode =
        doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo
          .HospitalCode;
      newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    var userParam = {
      HospitalID: parentHospitalId,
      USRId: doctorinfo.doctorid,
      USRType: 'ADMIN'
    };
    $scope.pageNumber = 1;
    $scope.Main = {
      currentPage: 1
    };
    // var parentHospitalId;
    $scope.applyNextSevenDays = false;
    $scope.ScheduleDate = moment().format('DD-MM-YYYY');

    $scope.changeDate = function(count) {
      if (count == 1) {
        $scope.ScheduleDate = moment($scope.ScheduleDate, 'DD-MM-YYYY')
          .add(1, 'day')
          .format('DD-MM-YYYY');
      } else if (count == -1) {
        $scope.ScheduleDate = moment($scope.ScheduleDate, 'DD-MM-YYYY')
          .subtract(1, 'day')
          .format('DD-MM-YYYY');
      }
      listing();
    };

    $scope.declareVariables = function() {
      $scope.selectBreak = 0;
      $scope.showfirstBreak = 1;
      $scope.selectClinic = 0;
      $scope.selectBreakNew = 0;
      $scope.showfirstBreakNew = 1;
      $scope.showNewColl = false;
      $scope.addEditSchedule = false;
    };
    $scope.setClinic = function(data) {
      $scope.selectClinic = data;
    };
    $scope.showBreak = function(data, index) {
      $scope.selectBreak = data + '' + index;
    };
    $scope.showBreakNew = function(data) {
      $scope.selectBreakNew = data;
    };

    // var regionParam = {
    //     "usrid": doctorinfo.doctorid,
    //     "usrtype": "ADMIN"
    // };
    var regionParam = {
      pageNumber: 0,
      pageSize: 1000,
      usrid: doctorinfo.doctorid,
      usrtype: 'ADMIN',
      hospitalcd: hospitalCode
    };
    referralregion.alllistRegion(
      regionParam,
      regionlistsuccess,
      schedulingError
    );

    function regionlistsuccess(data) {
      if (!data.error) {
        $scope.regionList = data.filter(val => val.active == true);
        listing();
      } else {
        alertify.error('Region List error');
      }
    }

    //listing scheduling
    function listing() {
      (userParam.ScheduleDate = moment(
        $scope.ScheduleDate,
        'DD-MM-YYYY'
      ).format('YYYY-MM-DD')),
        c7AdminServices.listAllScheduleInfo(
          userParam,
          schedulinglistSuccess,
          schedulingError
        );

      function schedulinglistSuccess(data) {
        $scope.selectedHos = undefined;
        $scope.hospitalCollection = data;
        var test = angular.copy($scope.hospitalCollection);

        for (var i = 0; i < test.length; i++) {         

          var selectedregion = $scope.regionList.filter(
            data => data.regionid == test[i].districtregion
          );          
          if (selectedregion.length > 0) {
            test[i].regionName = selectedregion[0].regionname;
          } else {
            test[i].regionName = '';
          }
          test[i].sort = test[i].scheduledetail.Active != undefined ? 'Y' : 'N';
          if (
            test[i - 1] != undefined &&
            test[i].hospitalname == test[i - 1].hospitalname
          ) {
            test[i].addnew = true;
            test[i - 1].addnew = false;
            test[i].applyNextSevenDays = false;
          } else if (
            test[i + 1] != undefined &&
            test[i].hospitalname == test[i + 1].hospitalname
          ) {
            test[i].addnew = false;
            test[i + 1].addnew = true;
            test[i].applyNextSevenDays = false;
          } else {
            test[i].addnew = true;
            test[i].applyNextSevenDays = false;
          }
        }
        $scope.hospitalCollection = angular.copy(test);
        $scope.selectedHos = $scope.hospitalCollection[0];
        $scope.declareVariables();
        $scope.hospitalCollectionFilter = angular.copy(
          $scope.hospitalCollection
        );
      }
    }
    $scope.setMinpercase = function(id) {
      var minpercase = $scope.selectedHos.doctordetails.filter(
        data => data.doctorid == id
      );
      $scope.selectedHos.scheduledetail.MinPerCase = minpercase[0].minpercase;
    };

    $scope.showCollection = function(data, index) {
      $scope.hospitalNew = angular.copy(data);
      if ($scope.hospitalNew.scheduledetail.Active) {
        $scope.showNewColl = true;
        delete $scope.hospitalNew.scheduledetail['AddressConsultID'];
        $scope.hospitalNew.scheduledetail.Slot == 'Start'
          ? ($scope.hospitalNew.scheduledetail.Slot = 'Slat1')
          : ($scope.hospitalNew.scheduledetail.Slot = 'Slat2');
        // $scope.hospitalNew.scheduledetail.Slot = 'Slat2'
      } else {
        alertify.error('Please fill primary slots and try it.');
      }
    };

    function schedulingError(data) {
      alertify.error('Error');
    }

    $scope.getDeleteScheduleDetails = function(hospitaldetails) {
      alertify.confirm("<b class='txt-centre'>Confirm !</b><h4 class='txt-body-centre'>Are you sure want to delete</h4> ", function() {

              if (hospitaldetails.scheduledetail !== undefined && hospitaldetails.scheduledetail !== null && Object.keys(hospitaldetails.scheduledetail).length > 0) {
                  var params = { "AddressConsultID": hospitaldetails.scheduledetail.AddressConsultID, "Active": hospitaldetails.scheduledetail.Active };
                  c7AdminServices.hospitalScheduleDelete(params, onScheduleDeleteSuccess, onScheduleDeleteError);                 

              } else{
                alertify.error('Not able to delete');
              }
          },
          function() {
              
          });
  }

  function onScheduleDeleteSuccess(data) {
    alertify.success('Schedule Deleted Successfully');
    listing();
  }

  function onScheduleDeleteError(data){
    alertify.error('Error, ' + data);
  }

    $scope.fillClinicData = function(data) {
      $scope.addEditSchedule = false;
      $('#loaderDiv').show();
      $scope.selectedHos = undefined;
      $scope.selectedHos = data;
      $scope.selectedHos.RoomCostStatus = 'F';
      setTimeout(function() {
        $('#loaderDiv').hide();
      }, 500);
      $scope.addEditSchedule = true;
    };
    $scope.addSchedule = function(data, update) {
      if (data.sonographerid == 0 || isNaN(data.sonographerid)) {
        alertify.error('Choose Sonographer');
        return;
      } else if (data.supportworkerid == 0 || isNaN(data.supportworkerid)) {
        alertify.error('Choose Support Worker');
        return;
      }
     
      var reg = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/;
      if(!reg.test(data.scheduledetail.FromTime1) || !reg.test(data.scheduledetail.ToTime1) || !reg.test(data.scheduledetail.FromTime2) || 
      !reg.test(data.scheduledetail.ToTime2)){
         alertify.error('Please check time format');
         return;
      }
      
      if(data.scheduledetail.FromTime1 > data.scheduledetail.ToTime1){
        alertify.error('Enter valid working hours');
        return false;
      }

      if(data.scheduledetail.FromTime2 > data.scheduledetail.ToTime2){
        alertify.error('Enter valid urgent time');
        return false;
      }

      if(data.scheduledetail.FromBreakTime1 > data.scheduledetail.ToBreakTime1){
        alertify.error('Enter valid break time1');
        return false;
      }

      if(data.scheduledetail.FromBreakTime2 > data.scheduledetail.ToBreakTime2){
        alertify.error('Enter valid break time2');
        return false;
      }

      if(data.scheduledetail.FromBreakTime3 > data.scheduledetail.ToBreakTime3){
        alertify.error('Enter valid break time3');
        return false;
      }

      if(data.scheduledetail.FromTime2 < data.scheduledetail.FromTime1 || data.scheduledetail.FromTime2 > data.scheduledetail.ToTime1 || 
        data.scheduledetail.ToTime2 < data.scheduledetail.FromTime1 || data.scheduledetail.ToTime2 > data.scheduledetail.ToTime1 || 
        data.scheduledetail.ToTime2 < data.scheduledetail.FromTime2){
          alertify.error('Enter Emergency time between working hours');
          return false;
        }

      // if (
      //   !(
      //     data.scheduledetail.FromTime2 < data.scheduledetail.ToTime1 &&
      //     data.scheduledetail.ToTime2 <= data.scheduledetail.ToTime1
      //   ) &&
      //   !(
      //     data.scheduledetail.FromTime2 >= data.scheduledetail.FromTime1 &&
      //     data.scheduledetail.ToTime2 <= data.scheduledetail.ToTime1
      //   )
      // ) {
      //   alertify.error('Enter Emergency time between working hours');
      //   return false;
      // }
      if (data.sonographerid != undefined) {
        for (var i = 0; i < data.doctordetails.length; i++) {
          if (data.doctordetails[i].doctorid == data.sonographerid) {
            data.scheduledetail.SonographerAddressID =
              data.doctordetails[i].addressid;
            data.scheduledetail.SonographerDoctorID = data.sonographerid;
          }
        }
        if (data.sonographerid == 0) {
          data.scheduledetail.SonographerAddressID = 0;
          data.scheduledetail.SonographerDoctorID = 0;
        }
      }
      if (data.supportworkerid != undefined) {
        for (var i = 0; i < data.doctordetails.length; i++) {
          if (data.doctordetails[i].doctorid == data.supportworkerid) {
            data.scheduledetail.SupportworkerAddressID =
              data.doctordetails[i].addressid;
            data.scheduledetail.SupportworkerDoctorID = data.supportworkerid;
          }
        }
        if (data.supportworkerid == 0) {
          data.scheduledetail.SupportworkerAddressID = 0;
          data.scheduledetail.SupportworkerDoctorID = 0;
        }
      }
      var params = {
        HospitalID: data.hospitalid,
        scheduleRecord: [
          {
            SonographerDoctorID: data.sonographerid,
            SupportworkerDoctorID: data.supportworkerid,
            SonographerAddressID: data.scheduledetail.SonographerAddressID,
            SupportworkerAddressID: data.scheduledetail.SupportworkerAddressID,
            DayCD: moment($scope.ScheduleDate, 'DD-MM-YYYY')
              .format('ddd')
              .toUpperCase(),
            ScheduleDate: moment($scope.ScheduleDate, 'DD-MM-YYYY').format(
              'YYYY-MM-DD'
            ),
            MinPerCase: data.scheduledetail.MinPerCase,
            FromTime1: data.scheduledetail.FromTime1,
            ToTime1: data.scheduledetail.ToTime1,
            FromTime2: data.scheduledetail.FromTime2,
            ToTime2: data.scheduledetail.ToTime2,
            FromTime3: '00:00',
            ToTime3: '00:00',
            FromBreakTime1:
              data.scheduledetail.FromBreakTime1 === null
                ? '00:00'
                : data.scheduledetail.FromBreakTime1,
            ToBreakTime1:
              data.scheduledetail.ToBreakTime1 === null
                ? '00:00'
                : data.scheduledetail.ToBreakTime1,
            FromBreakTime2:
              data.scheduledetail.FromBreakTime2 === null
                ? '00:00'
                : data.scheduledetail.FromBreakTime2,
            ToBreakTime2:
              data.scheduledetail.ToBreakTime2 === null
                ? '00:00'
                : data.scheduledetail.ToBreakTime2,
            FromBreakTime3:
              data.scheduledetail.FromBreakTime3 === null
                ? '00:00'
                : data.scheduledetail.FromBreakTime3,
            ToBreakTime3:
              data.scheduledetail.ToBreakTime3 === null
                ? '00:00'
                : data.scheduledetail.ToBreakTime3,
            otherdetails: {
              Slot: 'Slat1',
              BookFlag: 'Y',
              Active: 'Y',
              RoomCostStatus: data.RoomCostStatus != undefined ? data.RoomCostStatus : 'F'
            }
          }
        ],
        USRType: 'ADMIN',
        USRId: doctorinfo.doctorid
      };
      if (data.applyNextSevenDays) {
        params.Scheduledateflag = 'Y';
        params.Scheduledays = 7;
      } else {
        params.Scheduledateflag = 'N';
        params.Scheduledays = 1;
      }
      if (update) {
        params.scheduleRecord[0].AddressConsultID =
          data.scheduledetail.AddressConsultID;
        c7AdminServices.hospitalscheduleupdate(
          params,
          addHospitalsSuccess,
          schedulingError
        );
      } else {
        c7AdminServices.hospitalscheduleadd(
          params,
          addHospitalsSuccess,
          schedulingError
        );
      }

      // console.log(params);
    };

    function addHospitalsSuccess(data) {
      if (data == 'Success') {
        $scope.selectedHos = undefined;

        $scope.addEditSchedule = false;
        listing();
      } else {
        alertify.error(data);
      }
    }   

    // $("#workFromTime").keyup(function (e) {
    //   if ($(this).val().length == 2) {
    //       if (!e.ctrlKey && !e.metaKey)
    //           $(this).val($(this).val() + ":");
    //   }
    // });

    $scope.onTimeSet = function(ne, old, id) {
      $('#' + id).dropdown('toggle');
    };

    $scope.beforeRender = function(
      $leftDate,
      $rightDate,
      $upDate,
      $view,
      $dates,
      minutestep,
      minMinutes
    ) {
      $leftDate.selectable = false;
      $rightDate.selectable = false;
      if ($view == 'minute') {
        $upDate.display = 'Go Back';
        $upDate.selectable = true;
      } else {
        $upDate.selectable = false;
      }
      if ($view == 'minute' && minutestep != undefined) {
        // console.log();
        var len = Math.floor((24 * 60) / minutestep) + 1;
        var timeColl = [];
        var timeDate = [];
        if ($scope.selectedHos.scheduledetail.FromTime1 != undefined) {
          var starttime = moment(
            $scope.selectedHos.scheduledetail.FromTime1,
            'HH:mm'
          ).valueOf();
          for (var i = 1; i <= len; i++) {
            timeColl.push(
              moment.utc(starttime + i * minutestep * 60 * 1000).valueOf()
            );
            var loctime = moment(starttime + i * minutestep * 60 * 1000);
            if (
              !moment(loctime).isSameOrAfter(
                moment()
                  .startOf('day')
                  .add(1, 'day')
              )
            ) {
              timeDate.push(loctime.format('HH:mm'));
            }
          }
          $dates
            .filter(function(date) {
              return timeDate.indexOf(date.display) < 0;
            })
            .forEach(function(date) {
              date.selectable = false;
            });
        } else {
          for (var i = 1; i <= len; i++) {
            timeColl.push(
              moment
                .utc()
                .startOf('day')
                .valueOf() +
                i * minutestep * 60 * 1000
            );
          }
          $dates
            .filter(function(date) {
              return timeColl.indexOf(date.utcDateValue) < 0;
            })
            .forEach(function(date) {
              date.selectable = false;
            });
        }
      }
      if ($view == 'minute' && minMinutes != undefined) {
        $dates
          .filter(function(date) {
            return date.display <= minMinutes;
          })
          .forEach(function(date) {
            date.selectable = false;
          });
      }
    };
    $scope.filterbyregion = function() {
      // $('#loaderDiv').show();
      // var data = angular.copy($scope.hospitalCollectionFilter);
      // data = data.filter(data => data.districtregion == parseInt($scope.selectedRegion));
      // $scope.hospitalCollection = angular.copy($scope.hospitalCollectionFilter);
      // $('#loaderDiv').hide();
    };
  }
]);
