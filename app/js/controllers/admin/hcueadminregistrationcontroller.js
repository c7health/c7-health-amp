hCueDoctorWebControllers.controller('hCueAdminRegistrationController', ['$scope','hcueDoctorLoginService','hCueMainAdminServices','$filter','NgMap','alertify',function($scope,hcueDoctorLoginService,hCueMainAdminServices,$filter,NgMap,alertify) {

	//Config variables
	var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
	var parentHospitalId;
	var hospitalCode;
	var hospitalId;
	if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined){
		parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
		hospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
		hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
	}
	var hos_id;
	
	$scope.showVetentry = false;
	if (doctorinfo.AccountAccessID.indexOf('VETENTRY') !== -1){
        $scope.showVetentry = true;
    }
	
	//Veterinary

	/* Registartion collections */
	function resetForm() {
		$scope.RegFields = [{"rowid":1,"groupid":1,"groupedes":"BasicDetails","fieldname":"Name","fieldtype":"TEXT_BOX","mandatory":"Y","active":"Y"},
		{"rowid":2,"groupid":1,"groupedes":"BasicDetails","fieldname":"Age","fieldtype":"TEXT_BOX","mandatory":"Y","active":"Y"},
		{"rowid":3,"groupid":1,"groupedes":"BasicDetails","fieldname":"Gender","fieldtype":"TEXT_BOX","mandatory":"Y","active":"Y"},
		{"rowid":4,"groupid":1,"groupedes":"BasicDetails","fieldname":"Mobile Number","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":5,"groupid":1,"groupedes":"BasicDetails","fieldname":"Email Id","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":6,"groupid":1,"groupedes":"BasicDetails","fieldname":"Address","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":7,"groupid":2,"groupedes":"EmergencyContact","fieldname":"Name","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":8,"groupid":2,"groupedes":"EmergencyContact","fieldname":"Relationship","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":9,"groupid":2,"groupedes":"EmergencyContact","fieldname":"Contact Number","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":10,"groupid":2,"groupedes":"EmergencyContact","fieldname":"Emergency Contact Alternative Contact Number","fieldtype":"TEXT_BOX","mandatory":"N","active":"N"},
		{"rowid":11,"groupid":3,"groupedes":"OtherDetails","fieldname":"Marital Status","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":12,"groupid":3,"groupedes":"OtherDetails","fieldname":"Education","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":13,"groupid":3,"groupedes":"OtherDetails","fieldname":"Occupation","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":14,"groupid":3,"groupedes":"OtherDetails","fieldname":"Payment Mode","fieldtype":"TEXT_BOX","mandatory":"N","active":"N"},
		{"rowid":15,"groupid":3,"groupedes":"OtherDetails","fieldname":"Referral Source","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":16,"groupid":1,"groupedes":"BasicDetails","fieldname":"Blood Group","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":17,"groupid":4,"groupedes":"VetneryDetails","fieldname":"Colour","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":18,"groupid":4,"groupedes":"VetneryDetails","fieldname":"Species","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":19,"groupid":4,"groupedes":"VetneryDetails","fieldname":"Breed","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"},
		{"rowid":20,"groupid":4,"groupedes":"VetneryDetails","fieldname":"Owner Name","fieldtype":"TEXT_BOX","mandatory":"N","active":"Y"}];
	}
	
	resetForm();
	/* End of registration collections */


	function branchListing(pageSize = 100) {
		var get_branches={
			"pageNumber": 1,
			"HospitalID": parseInt(parentHospitalId),
			"pageSize": parseInt(pageSize),
			"IncludeDoctors": "N",
			"IncludeBranch": "Y",
			"ActiveHospital":"Y",
    		"ActiveDoctor":"Y"
		}; 

		hCueMainAdminServices.getHospitalDetails(get_branches,onSuccessBranches,onFailedBranches)
	}

	function onSuccessBranches(data) {
		$scope.hospitalList=data.rows;
		$scope.selectBranch = $scope.hospitalList[0];
		$scope.changeHospital($scope.selectBranch);
		$scope.count=data.count;
		$scope.totalItems=data.count;
	}

	function onFailedBranches(data) {
		console.log("Branches list error " + JSON.stringify(data));
	}

	branchListing();

	$scope.changeHospital = function(hospitalDetails) {
		hos_id = hospitalDetails.HospitalInfo.HospitalDetails.HospitalID;
		hCueMainAdminServices.listRegistration(hos_id, OnSuccess, onError);

		function OnSuccess(data) {
			if (data == "No Data Available") {
				 resetForm()
			} else {
				 $scope.RegFields = [];
				 $scope.RegFields = data;
			}
		}

		function onError(data) {
			console.log("Get fields error in getList" + JSON.stringify(data))
		}
	}

	$scope.basicFilter = function(val) {
		if(val.groupedes == "BasicDetails" && val.active == "Y") {
			return val;
		}
	}

	$scope.emergencyFilter = function(val) {
		if(val.groupedes == "EmergencyContact" && val.active == "Y") {
			return val;
		}
	}

	$scope.otherFilter = function(val) {
		if(val.groupedes == "OtherDetails" && val.active == "Y") {
			return val;
		}
	}
	
	$scope.vetneryFilter = function(val) {
		if(val.groupedes == "VetneryDetails" && val.active == "Y") {
			return val;
		}
	}
	
	$scope.addUpdate = function() {

		var params = {
			"USRType": "PATIENT",
		    "Type": "Patient",
		    "ScreenType": "DOCTOR",
		    "RegSettings": $scope.RegFields,
		    "ID": hos_id,
		    "USRId": doctorinfo.doctorid,
		    "AppType": "Web"
		}


		 hCueMainAdminServices.mandatorRegistrationFields(params, OnRegistrationSuccess, OnRegistrationerror);


		 function OnRegistrationSuccess(data) {
		 	alertify.success("Data updated successfully");
		 	$scope.selectBranch = $scope.hospitalList[0];
		 	$scope.changeHospital($scope.selectBranch);

		 }

		 function OnRegistrationerror(data) {
		 	console.log("Add update error " + JSON.stringify(data));
		 }
	}


	$scope.reset = function() {
		resetForm();
		$scope.changeHospital($scope.selectBranch)
	}
}]);