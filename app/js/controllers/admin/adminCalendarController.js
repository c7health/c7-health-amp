hCueDoctorWebControllers.controller('adminCalendarController', ['$scope', '$filter','NgMap','alertify','c7AdminServices','hcueDoctorLoginService', function($scope,$filter,NgMap,alertify, c7AdminServices,hcueDoctorLoginService) {
    
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined){
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    // $scope.selectBreak = 0;
    // $scope.showfirstBreak = 1;
    // $scope.selectClinic = 0;
    // $scope.setClinic = function (data) {
    //     $scope.selectClinic = data;
    // }
    // $scope.showBreak = function (data, index) {
    //     $scope.selectBreak = data +''+ index;
    // }
    $scope.filtype = 'sc';
    c7AdminServices.listdoctorhospital(parentHospitalId, sonolistSuccess, calendarError);
    function sonolistSuccess(data) {
        $scope.doctorList = data;
        $scope.doctorList.disabled = true;
    }
    function branchListing(){

        var param ={
            "pageNumber": $scope.pageNumber,
            "HospitalID": parentHospitalId,
            "pageSize": 100,
            "IncludeDoctors": "N",
            "IncludeBranch": "Y",
            "ActiveHospital": "Y",
            "ActiveDoctor": "Y"
            
        }
        
        c7AdminServices.getHospitals(param,onGetHospitalsSuccess,calendarError);
    
        function onGetHospitalsSuccess(data) {
            $scope.hospitalCollection = data.rows;
            // console.log(data.rows);
        }
    }
    branchListing();
    $scope.hospitalSelected = function () {
        console.log($scope.hospitalCollection.selected);
    }

    $scope.applyFilter = function () {
        // var dateRange = moment(moment($scope.toDate, 'DD-MM-YYYY')).diff(moment($scope.fromDate, 'DD-MM-YYYY'), 'days');
        var splitRange = {
            from:$scope.fromDate.split(' - ')[0],
            to:$scope.fromDate.split(' - ')[1]
        }

        $scope.filterparam = {
            "FromDate": moment(splitRange.from, 'DD-MM-YYYY').format('YYYY-MM-DD'),
            "ToDate": moment(splitRange.to, 'DD-MM-YYYY').format('YYYY-MM-DD'),
            "USRId":doctorinfo.doctorid,
            "USRType":"ADMIN"
        }
        if($scope.doctorList.selected != undefined){
            $scope.filterparam.SonographersID = $scope.doctorList.selected.doctorid;
        } else {
            $scope.filterparam.SonographersID = 0;
        }
        if($scope.hospitalCollection.selected != undefined){
            $scope.filterparam.HospitalID = $scope.hospitalCollection.selected.HospitalInfo.HospitalDetails.HospitalID;
        } else {
            $scope.filterparam.HospitalID = 0;
        }

        listing()
    
    }
    //listing calendar
    function listing(){
        c7AdminServices.listAllCalendarInfo($scope.filterparam, calendarlistSuccess, calendarError);

        function calendarlistSuccess(data){
            // console.log(data);
            if (data.length != 0) {
                $scope.calendarCollection = angular.copy(data);
                angular.forEach($scope.calendarCollection, function (i) {
                    i.date = moment(i.ScheduledDate).format('DD-MM-YYYY(dddd)');
                    i.timestamp = moment(i.ScheduledDate).valueOf();
                    var slots = [];
                    //  || 'emer': 
                    angular.forEach(i.time1, function (j) {
                        var time = j.split('~')[0];
                        if (j.split('~')[1] == 'Y') {
                            slots.push({
                                time: time,
                                booked: true,
                                break: false,
                                emer: false,
                                eqid: undefined
                            });
                        } else if (j.split('~')[1] == 'EY') {
                            slots.push({
                                time: time,
                                booked: true,
                                break: false,
                                emer: true,
                                eqid: undefined
                            });
                        } else if (j.split('~')[1] == 'E') {
                            slots.push({
                                time: time,
                                booked: false,
                                emer: true,
                                break: false,
                                eqid: undefined
                            });
                        } else {
                            slots.push({
                                time: time,
                                booked: false,
                                break: false,
                                emer: false,
                                eqid: undefined
                            });
                        }
                    });
                    angular.forEach(i.BreakTime1, function (j) {
                        slots.filter(data => data.time == j).forEach(data => data.break = true);
                    });
                    angular.forEach(i.BreakTime2, function (j) {
                        slots.filter(data => data.time == j).forEach(data => data.break = true);
                    });
                    angular.forEach(i.BreakTime3, function (j) {
                        slots.filter(data => data.time == j).forEach(data => data.break = true);
                    });
                    i.slotscoll = slots;
                    i.totalslot = i.totalslot - i.BreakTime1.length - i.BreakTime2.length - i.BreakTime3.length;
                });
                console.log($scope.calendarCollection);
                if($scope.swiper != undefined){
                    $scope.swiper.destroy();
                }
                setTimeout(function () {
                    $scope.swiper = new Swiper('.swiper-calendar', {
                        initialSlide: 0,
                        slidesPerView: 4,
                        spaceBetween: 10,
                        navigation: {
                            nextEl: '.swiper-button-next',
                            prevEl: '.swiper-button-prev',
                        }
                    });
                    $('[data-toggle="popover"]').popover();
                }, 100);
            } else {
                $scope.calendarCollection = [];
                alertify.error('No record found');
            }
        }
    }
    $scope.getBookedDetails = function (data) {
        console.log(data);
    }


    // function getTimeSlots(start, end, interval){
    //     var startTime = moment(start, 'HH:mm');
    //     var endTime = moment(end, 'HH:mm');

    //     if (endTime.isBefore(startTime)) {
    //         endTime.add(1, 'day');
    //     }

    //     var timeStops = [];

    //     while (startTime <= endTime) {
    //         timeStops.push(new moment(startTime).format('HH:mm'));
    //         startTime.add(interval, 'minutes');
    //     }
    //     return timeStops;
    // }

    // $scope.timeSlots = getTimeSlots('09:00', '23:59', 5);
    
    function calendarError(err){
        console.log(err);
    }

}]); 