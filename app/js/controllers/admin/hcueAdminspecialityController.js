hCueDoctorWebControllers.controller('hcueAdminspecialityController', ['$scope', '$filter','NgMap','alertify','c7AdminServices','hcueDoctorLoginService', function($scope,$filter,NgMap,alertify, c7AdminServices,hcueDoctorLoginService) {
    
var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
var ParentHospitalID
if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined){
    ParentHospitalID = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
}

//add category


// crate category


//add speciality

$scope.main = {
    createSpeciality : '',
    editSpeciality : '',
    editSpecialityid : '',
    editSubSpeciality : '',
    editSubSpecialityid: '',
    dicomcode: '',
    specialityId: '',
    usernotes: '',
    editusernotes: '',
    categoryID:''
};
$scope.specialityCollection = [];
$scope.categoryCollection = [];

$scope.selectspeciality = function(data){
$scope.speciality=$filter('filter')($scope.specialityCollection,{CategoryID:data})[0]
}

// create speciality
$scope.addSpeciality = function(){
    if($scope.main.createSpeciality == ''){
        alertify.error('Enter speciality name')
        return false;
    }

    var params = {
        "updtusr": doctorinfo.doctorid,
        "updtusrtype": "ADMIN",
        "CategoryID": Number ($scope.main.specialityId), 
        "speciality":[{
            "specialitydesc": $scope.main.createSpeciality,
            "activeIND": "Y",
            "dicomcode": "",
            "usernotes": $scope.main.usernotes,
            "hospitalId": ParentHospitalID,
        }]

    }
    fillinfo()

    angular.forEach($scope.multiSubSpeciality, function(i) {
        params.speciality.push(i);
    });

    c7AdminServices.addSpecialitylkup(params, addspecialitySuccess, specialityError)
    $('#addSpeciality').modal('hide');

    // $scope.multiSubSpeciality = [];
    // $scope.subSpecialityAddCollection = [];
    
}

function addspecialitySuccess(data){

    // alertify.error(JSON.stringify(data));
    if(data.error) {
        alertify.error('Some thing went wrong');
    }
    else if(data == "Success"){
        $scope.main.createSpeciality = '';
        $scope.main.usernotes = '';
        $scope.specialityCollection = [];
        
        $scope.multiSubSpeciality = [];
        $scope.subSpecialityAddCollection = [];
        $('#addSubSpeciality').modal('hide');
        listing()
    }else{
        alertify.error("Name already exists");
    }
}

//listing speciality

    function listing(){

        c7AdminServices.SpecialityListNew(ParentHospitalID,specialitylistSuccess, specialityError)

        function specialitylistSuccess(data){
            $scope.specialityCollection=data;
            angular.forEach($scope.specialityCollection, function(j){
                j.isEdit = false;
                angular.forEach(j.subspeciality, function(k){
                    k.isEdit = false;
                })
    
            })
            $('#loaderDiv').hide();

        }

    }
    $('#loaderDiv').show();
    listing()

    //edit speciality

    $scope.onEditSpeciality = function(list){

        // alertify.error(JSON.stringify($scope.specialityCollection));

        $scope.main.editSpeciality = '';
        $scope.main.editSpecialityid = '';
        angular.forEach($scope.specialityCollection, function(j){
            for(x=0;x<j.speciality.length;x++){
            if(j.speciality[x].DoctorSpecialityID == list.DoctorSpecialityID){
                j.speciality[x].isEdit = true;
                $scope.main.editSpeciality = list.DoctorSpecialityDesc;
                $scope.main.editSpecialityid = list.DoctorSpecialityID;
                $scope.main.editusernotes = list.usernotes;
            }else{
                j.speciality[x].isEdit = false;
            }
            angular.forEach(j.subspeciality, function(k){
                k.speciality[x].isEdit = false;
            })
        }
    })
    
    }

    $scope.updateSpeciality = function(){
        if($scope.main.editSpeciality == ''){
            alertify.error('Enter speciality name')
            return false;
        }
        var params = {
            "specialityID": $scope.main.editSpecialityid,
            "updtusr": doctorinfo.doctorid,
            "updtusrtype": "ADMIN",
            "CategoryID": Number ($scope.main.specialityId), 
            "speciality":[{
                "specialitydesc":$scope.main.editSpeciality,
                "activeIND":"Y",
                "dicomcode":"",
                "usernotes": $scope.main.editusernotes,
                "hospitalId": ParentHospitalID
            }]
        }
        c7AdminServices.updateSpecialitylkup(params, addspecialitySuccess, specialityError)
    
    }

    $scope.deleteSpeciality = function(data){
        var params = {
            "specialityID": data.DoctorSpecialityID,
            "updtusr": doctorinfo.doctorid,
            "updtusrtype": "ADMIN",
            "CategoryID": Number ($scope.main.specialityId),            
            "speciality":[{
                "specialitydesc":data.DoctorSpecialityDesc,
                "activeIND":"N",
                "dicomcode":"",
                "usernotes": data.usernotes,
                "hospitalId": ParentHospitalID
            }]
        }

        c7AdminServices.updateSpecialitylkup(params, addspecialitySuccess, specialityError)
    }

    
// subspeciality create
   $scope.clearAddSubSpeciality = function(){
    $scope.main.specialityId = '';
        $scope.subSpecialityAddCollection = [{
            "specialitydesc": '',
            "dicomcode": '',
            "activeIND": 'Y',
            "usernotes": "",
            "hospitalId": ParentHospitalID
        }]
    }

    $scope.clearAddSubSpeciality()
    $scope.addExtrabox = function(){
        var newvalue = {
            "specialitydesc": '',
            "dicomcode": '',
            "activeIND": 'Y',
            "usernotes": "",
            "hospitalId": ParentHospitalID
        }

        $scope.subSpecialityAddCollection.push(newvalue)
    }

    $scope.addCategory = function(){
        if($scope.main.createCategory == ''){
            alertify.error('Select category name')
            return false;
        }
        var param = {
            "CategoryName": $scope.main.createCategory,
            "Description":$scope.main.usercategorynotes,
            "HospitalID":ParentHospitalID,
            "CrtUSR":doctorinfo.doctorid,
            "CrtUSRType":"ADMIN"
        }
        c7AdminServices.Specialitycategory(param, addCategorySuccess, specialityError)
       }

       function addCategorySuccess(data){
        if(data.error) {
            alertify.error('Some thing went wrong');
        }
        else if(data == "Category added succesfully"){
            $scope.main.createCategory = '';
            $scope.main.usercategorynotes = '';
             $scope.categoryCollection = [];
            $('#addCategory').modal('hide');
            listing();
            categorylisting();

        }else{
            alertify.error("Name already exists");
        }
    }

    function categorylisting(){

        c7AdminServices.CategoryList(ParentHospitalID,categorylistSuccess, specialityError)

        function categorylistSuccess(data){
            $scope.categoryCollection=data;

        }
    }
    categorylisting();

    $scope.collapsescroll = function(divCatID){

        var y = event.clientY;
        var elmnt = document.getElementById(divCatID);
//        alertify.error(elmnt.offsetHeight);
        setTimeout(function(){
            if(y >=550)
            {
                document.getElementById("page-scrollnew").scrollTop+=elmnt.offsetHeight;
    
            }
    
        }, 200);
 
        // alertify.error(document.getElementById("page-scrollnew").scrollTop);
    }
    
    $scope.onCreateSubSpeciality = function(){
        if($scope.main.specialityId == ''){
            alertify.error('Select speciality name')
            return false;
        }
        fillinfo()
        if($scope.multiSubSpeciality.length == 0){
            alertify.error('Enter sub-speciality name')
            return false;
        }
        var param = {
            "updtusr":doctorinfo.doctorid,
            "updtusrtype":"ADMIN",
            "specialityID":$scope.main.specialityId,
            "CategoryID":Number ($scope.main.categoryID),
            "speciality":$scope.multiSubSpeciality
        }
        c7AdminServices.addSpecialitylkup(param, addspecialitySuccess, specialityError)

        // $scope.multiSubSpeciality = [];
        // $scope.subSpecialityAddCollection = [];
            
    }

    function fillinfo(){
        $scope.multiSubSpeciality = [];

        angular.forEach($scope.subSpecialityAddCollection, function(k){
            if(k.specialitydesc != ''){
                $scope.multiSubSpeciality.push(k);
            }
        })
    }


//subspeciality edit
    $scope.onEditSubSpeciality = function(data, data1){
        $scope.main.editSpecialityid = data1.DoctorSpecialityID;

        angular.forEach($scope.specialityCollection, function(i){

            angular.forEach(i.speciality, function(j){

                j.isEdit = false;
                angular.forEach(j.subspeciality, function(k){
                    
                    if(k.DoctorSpecialityID == data.DoctorSpecialityID){
                        k.isEdit = true;
                        $scope.main.editSubSpeciality = data.DoctorSpecialityDesc;
                        $scope.main.dicomcode = data.dicomcode;
                        $scope.main.editSubSpecialityid = data.DoctorSpecialityID;
                    }else{
                        k.isEdit =false;
                    }
                })
            })
            // alertify.error(JSON.stringify(data));
        })

        
    }

    $scope.updateSubSpeciality = function(){
        if($scope.main.editSubSpeciality == ''){
            alertify.error('Enter sub-speciality name')
            return false;
        }
        var params = {
            "specialityID": $scope.main.editSpecialityid,
            "updtusr": doctorinfo.doctorid,
            "updtusrtype": "ADMIN",
            "speciality":[{
                "subSpecialityID":$scope.main.editSubSpecialityid,
                "specialitydesc":$scope.main.editSubSpeciality,
                "activeIND":"Y",
                "dicomcode":$scope.main.dicomcode,
                "usernotes": "",
                "hospitalId": ParentHospitalID
            }]
        }
        c7AdminServices.updateSpecialitylkup(params, addspecialitySuccess, specialityError)
    }

    $scope.deleteSubSpeciality = function(data, data1, data2){

        var params = {
            "specialityID": data1.DoctorSpecialityID,
            "updtusr": doctorinfo.doctorid,
            "updtusrtype": "ADMIN",
            "CategoryID":data2,

            "speciality":[{
                "subSpecialityID":data.DoctorSpecialityID,
                "specialitydesc":data.DoctorSpecialityDesc,
                "activeIND":"N",
                "dicomcode":data.dicomcode,
                "usernotes": "", 
                "hospitalId": ParentHospitalID
            }]
        }
        // alertify.error(JSON.stringify(data));
        c7AdminServices.updateSpecialitylkup(params, addspecialitySuccess, specialityError)
    }

    function specialityError(data){

    }

    $scope.removesub = function(ind){
        $scope.subSpecialityAddCollection.splice(ind, 1);

        if($scope.subSpecialityAddCollection.length == 0){
            $scope.addExtrabox();
        }
    }
}]); 