hCueDoctorWebControllers.controller('hCueAdminDashbroadController', ['$scope','hcueDoctorLoginService','c7AdminServices','$filter','NgMap','alertify',function($scope,hcueDoctorLoginService,c7AdminServices,$filter,NgMap,alertify) {

	//Config variables
	var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
	var parentHospitalId;
	if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined){
		parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
	}

	function loadData() {
		var params = {"HospitalID":parentHospitalId,"USRId":parentHospitalId,"USRType":"ADMIN" }
		c7AdminServices.listDashBoardInfo(params,onDashboardSuccess,onDashboardError);

		function onDashboardSuccess(data) {
			$scope.dashboardData = data;
		}

		function onDashboardError(data) {
			console.log("Dashboard load data error " + JSON.stringify(data));
		}
	}

	loadData();

}]);