hCueDoctorWebControllers.controller('hCueAdminManageBranchController', ['$scope', '$window', 'hcueDoctorLoginService', 'c7AdminServices', '$filter', 'NgMap', 'alertify', '$timeout', 'c7PostCodeServices', 'referralregion', function ($scope, $window, hcueDoctorLoginService, c7AdminServices, $filter, NgMap, alertify, $timeout, c7PostCodeServices, referralregion) {

    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    $scope.searchText = {
        search: ''
    }
    $scope.Main = {
        "currentPage": 1,
        "enab": true
    }
    var parentHospitalId;
    var hospitalcd;
    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;

    }

    function resetCollection() {
        $scope.hospitalParams = {
            "USRType": "ADMIN",
            "USRId": doctorinfo.doctorid,
            "HospitalAddress": {
                "State": "",
                "Country": "UK",
                "Address1": "",
                "Address2": "",
                "Address3": "",
                "Address4": "",
                "CityTown": "",
                "DistrictRegion": undefined,
                "PostCode": '',
                "Location": "",
                "Latitude": null,
                "Longitude": null
            },
            "HospitalDetails": {
                "RegsistrationDate": moment().format('YYYY-MM-DD'),
                "AboutHospital": "",
                "HospitalID": 0,
                "ParentHospitalID": parentHospitalId,
                "EmailAddress": "",
                "AppListEmail": "",
                "TermsAccepted": "Y",
                "ActiveIND": "Y",
                "HospitalName": "",
                "Description": "",
                "Clinictype": "",
                "RoomCost": '',
                "HalfDayRoomCost": '',
                "Consumables": $scope.consumables,
                "ExpPerDay": '',
                "isParentConsumables": 'N',
                "TriageCost": ''
            },
            "HospitalPhone": [{
                "HospitalID": 0,
                "PhCntryCD": 1111,
                "PhStateCD": 11,
                "PhAreaCD": 1111,
                "PhNumber": '',
                "FullNumber": '',
                "PhType": "D",
                "PrimaryIND": "Y",
                "PublicDisplay": "Y",
                "RowID": 0
            }, {
                "HospitalID": 0,
                "PhCntryCD": 1111,
                "PhStateCD": 11,
                "PhAreaCD": 1111,
                "PhNumber": '',
                "FullNumber": '',
                "PhType": "F",
                "PrimaryIND": "Y",
                "PublicDisplay": "Y",
                "RowID": 1
            }]
        };

    }

    function branchListing(pageSize = 1) {
        $scope.newPage = pageSize;
        var param = {
            "pageNumber": $scope.newPage,
            "HospitalID": parentHospitalId,
            "pageSize": 10,
            "IncludeDoctors": "N",
            "IncludeBranch": "Y",
            "ActiveHospital": "Y",
            "ActiveDoctor": "Y"

        }

        c7AdminServices.getHospitals(param, onGetHospitalsSuccess, onGetHospitalsError);

        function onGetHospitalsSuccess(data) {
            $scope.branchDetails = data.rows;
            $scope.consumables = data.consumables;
            $scope.Main.enab = true;
            $scope.totalItems = data.count;
            $scope.setPage($scope.newPage);
            $scope.noResultfound = false;
            $scope.pageDisplay = true;
            $scope.searchText.search = "";

        }
    }

    /* Pagination */
    $scope.maxSize = 10;

    function isInteger(x) {
        return x % 1 === 0;
    }

    $scope.setPage = function (pageNo) {
        $scope.Main.currentPage = pageNo;
    };
    $scope.pageChanged = function (pageNo) {
        branchListing(pageNo);
    }

    /* End of pagination */

    //add Locations

    $scope.pageShow = function (val) {
        if (val == 'add') {
            $scope.edit_status = false;
            resetCollection();
            $timeout(function () {
                window.dispatchEvent(new Event('resize'));
            }, 500);
            $scope.branchList = false;
            $scope.addUpdateBranch = true;
            // $scope.showEditBranch = true;
        } else if (val == "edit") {
            $scope.branchList = false;
            $scope.addUpdateBranch = true;
            // $scope.showEditBranch = true;
            $scope.edit_status = true;
            $timeout(function () {
                window.dispatchEvent(new Event('resize'));
            }, 500);

        } else {

            branchListing();
            $scope.branchList = true;
            $scope.addUpdateBranch = false;
            $scope.edit_status = false;
        }
    }
    $scope.pageShow('list');
    $scope.branch_details = function (val, array, index) {
        if (val == "delete") {
            $scope.delete_status = true;
            $scope.hospitalParams = array.HospitalInfo;
            $scope.hospitalParams.HospitalDetails.ActiveIND = "N";
            $scope.hospitalParams.HospitalDetails.HospitalID = array.HospitalInfo.HospitalDetails.HospitalID;
            $scope.hospitalParams.USRType = "ADMIN";
            $scope.hospitalParams.USRId = doctorinfo.doctorid;
            $scope.hospitalParams.HospitalAddress.DistrictRegion = $scope.hospitalParams.HospitalAddress.DistrictRegion != undefined ? $scope.hospitalParams.HospitalAddress.DistrictRegion.toString() : '';
            $scope.submit(undefined, 'delete');
        } else {
            $scope.edit_status = true;
            $scope.hospitalParams = array.HospitalInfo;
            $scope.hospitalParams.HospitalDetails.RegsistrationDate = moment($scope.hospitalParams.HospitalDetails.RegsistrationDate).format('YYYY-MM-DD');
            $scope.hospitalParams.HospitalDetails.AboutHospital = $scope.hospitalParams.HospitalDetails.About;
            $scope.hospitalParams.USRType = "ADMIN";
            $scope.hospitalParams.USRId = doctorinfo.doctorid;
            $scope.hospitalParams.HospitalAddress.DistrictRegion = $scope.hospitalParams.HospitalAddress.DistrictRegion != undefined ? $scope.hospitalParams.HospitalAddress.DistrictRegion.toString() : '';
            $scope.hospitalParams.HospitalDetails.isParentConsumables = 'N';
            if ($scope.hospitalParams.HospitalPhone[1] == undefined) {
                var param = {
                    "HospitalID": $scope.hospitalParams.HospitalPhone[0].HospitalID,
                    "PhCntryCD": 1111,
                    "PhStateCD": 11,
                    "PhAreaCD": 1111,
                    "PhNumber": '',
                    "FullNumber": '',
                    "PhType": "F",
                    "PrimaryIND": "Y",
                    "PublicDisplay": "Y",
                    "RowID": 1
                }
                $scope.hospitalParams.HospitalPhone.push(param)
            }
            $scope.pageShow('edit');
        }
    }

    // Post code
    $scope.getloockup = function () {
        $scope.addressCollection = [];
        $scope.addressData = null;
        if ($scope.hospitalParams.HospitalAddress.PostCode == '') {
            return false;
        }
        c7PostCodeServices.listpostcodeinfo($scope.hospitalParams.HospitalAddress.PostCode, postcodeSuccess, onGetHospitalsError)
    }

    function postcodeSuccess(data) {
        $scope.addressCollection = data.addresses;
        $scope.addressData = data;
    }

    $scope.selectpostcode = function (list) {
        // latlong(list)

        $scope.addressSplit = list.split(',');
        $scope.hospitalParams.HospitalAddress.Address1 = $scope.addressSplit[0] == ' ' ? '' : $scope.addressSplit[0].trim();
        $scope.hospitalParams.HospitalAddress.Address2 = $scope.addressSplit[1] == ' ' ? '' : $scope.addressSplit[1].trim();
        $scope.hospitalParams.HospitalAddress.Address3 = $scope.addressSplit[2] == ' ' ? '' : $scope.addressSplit[2].trim();
        $scope.hospitalParams.HospitalAddress.Address4 = $scope.addressSplit[3] == ' ' ? '' : $scope.addressSplit[3].trim();
        $scope.hospitalParams.HospitalAddress.CityTown = $scope.addressSplit[5] == ' ' ? '' : $scope.addressSplit[5].trim();
        $scope.hospitalParams.HospitalAddress.State = $scope.addressSplit[6] == ' ' ? '' : $scope.addressSplit[6].trim();

        if ($scope.addressData.latitude != null && $scope.addressData.longitude != null) {
            $scope.hospitalParams.HospitalAddress.Latitude = $scope.addressData.latitude.toString();
            $scope.hospitalParams.HospitalAddress.Longitude = $scope.addressData.longitude.toString();
            $scope.refreshMap()
            $scope.$apply()
        } else{
            latlong(list);
        }

    }

    function latlong(val) {
        var geocoder = new google.maps.Geocoder();
        var address = val;

        geocoder.geocode({ 'address': address }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                $scope.hospitalParams.HospitalAddress.Latitude = results[0].geometry.location.lat().toString();
                $scope.hospitalParams.HospitalAddress.Longitude = results[0].geometry.location.lng().toString();
                $scope.refreshMap()
                $scope.$apply()
            }
        });
    }

    $scope.getRegionList = function () {
        $scope.allRegion = [];
        var userParam = {
            "pageNumber": 0,
            "pageSize": 1000,
            "usrid": doctorinfo.doctorid,
            "usrtype": "ADMIN",
            "hospitalcd": hospitalcd
        };
        referralregion.alllistRegion(userParam, regionlistsuccess, onGetHospitalsError);
    }

    function regionlistsuccess(data) {
        $scope.allRegion = data.filter(val => val.active == true);
    }

    $scope.getRegionList()

    $scope.submit = function (form, val) {
        $scope.addUpdateBranchForm = form;
        if (val == undefined) {
            $scope.hospitalParams.HospitalPhone[0].PhNumber = parseFloat($scope.hospitalParams.HospitalPhone[0].FullNumber);
            $scope.hospitalParams.HospitalPhone[1].PhNumber = $scope.hospitalParams.HospitalPhone[1].FullNumber != '' ? parseFloat($scope.hospitalParams.HospitalPhone[1].FullNumber) : undefined;
            if ($scope.hospitalParams.HospitalPhone[1].PhNumber == undefined) {
                $scope.hospitalParams.HospitalPhone.splice(1, 1);
            }
        }
        $scope.hospitalParams.HospitalAddress.PostCode = $scope.hospitalParams.HospitalAddress.PostCode.toUpperCase();
        $scope.hospitalParams.HospitalAddress.DistrictRegion = parseInt($scope.hospitalParams.HospitalAddress.DistrictRegion);
        $scope.hospitalParams.HospitalDetails.Consumables = parseFloat($scope.hospitalParams.HospitalDetails.Consumables);
        $scope.hospitalParams.HospitalDetails.RoomCost = parseFloat($scope.hospitalParams.HospitalDetails.RoomCost);
        $scope.hospitalParams.HospitalDetails.HalfDayRoomCost = parseFloat($scope.hospitalParams.HospitalDetails.HalfDayRoomCost);
        $scope.hospitalParams.HospitalDetails.ExpPerDay = parseFloat($scope.hospitalParams.HospitalDetails.ExpPerDay);
        $scope.hospitalParams.HospitalDetails.TriageCost = parseFloat($scope.hospitalParams.HospitalDetails.TriageCost);
        c7AdminServices.addUpdateHospital($scope.hospitalParams, onAddHospitalsSuccess, onGetHospitalsError);

    }

    function onAddHospitalsSuccess(data) {
        if (data.error) {
            alertify.error(data.error.message);
        } else if ($scope.edit_status) {
            alertify.success("Location updated successfully");
        } else if ($scope.delete_status) {
            alertify.success("Location deleted successfully");
        } else {
            alertify.success("Location added successfully");
        }
        $scope.edit_status = false;
        $scope.delete_status = false;
        $scope.pageShow("listview");
        resetCollection();

        if ($scope.addUpdateBranchForm != undefined) {
            $timeout(function () {
                $scope.addUpdateBranchForm.$setPristine();
                $scope.addUpdateBranchForm.$setUntouched();
                $scope.addUpdateBranchForm.$submitted = false;
            });
        }
    }

    function onGetHospitalsError(data) {
        console.log("Get Palce error " + JSON.stringify(data));
    }

    $scope.refreshMap = function () {
        window.dispatchEvent(new Event('resize'));
        NgMap.initMap('mapID');
    }

    $scope.clearSearchVal = function () {
        $scope.searchText.search = "";
        branchListing();
    }

    /* Search branch */
    $scope.searchBranch = function (val) {
        if (val != "") {

            var searchParams = {
                "pageNumber": 0,
                "pageSize": 10,
                "HospitalCD": hospitalcd,
                "SearchText": val,
                "loaderStatus": false
            }
            c7AdminServices.searchBranch(searchParams, onSearchSuccess, onSearchFailed);

            function onSearchSuccess(data) {
                $scope.branchDetails = data.rows;
                if ($scope.branchDetails.length == 0) {
                    $scope.noResultfound = true;
                }
                $scope.pageDisplay = false;
            }

            function onSearchFailed(data) {

            }
        } else {
            branchListing();
        }

    }

    /* End of search branch */


}]);