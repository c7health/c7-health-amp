﻿hCueDoctorWebControllers.controller('hCueDocAppointmentList', ['$scope', '$routeParams', '$rootScope', 'hcueServiceUrl', '$filter', 'hcueLoginStatusService', 'hcueConsultationInfo', 'hcueDoctorLoginService', 'hcuePatientCaseService', 'datacookieFactory', '$window', '$timeout', 'alertify', 'hCueReferralServices', 'hCueAppointmentService', 'hcueDoctorAppointmentService', 'hCueVisitReasonServices', 'c7AdminServices', 'c7SearchServices', 'c7AppointmentListServices', function($scope, $routeParams, $rootScope, hcueServiceUrl, $filter, hcueLoginStatusService, hcueConsultationInfo, hcueDoctorLoginService, hcuePatientCaseService, datacookieFactory, $window, $timeout, alertify, hCueReferralServices, hCueAppointmentService, hcueDoctorAppointmentService, hCueVisitReasonServices, c7AdminServices, c7SearchServices, c7AppointmentListServices) {

    var inputData = hcueDoctorLoginService.getcalenderInfo();
    $scope.offlineAddedPatient = [];
    $scope.docTimeStatus = inputData.TimeSlotDetail.Available;
    $scope.urlpath = false;
    var documentURL = window.location.origin;

    if (documentURL.indexOf('justdental.in'.toLowerCase()) >= 0) {
        $scope.urlpath = true;
    }

    var sdate = "";
    var onchageselectappoint = [];
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === "true") {
        //$scope.setHeaderShouldShow(false);
    } else {
        $location.path('/login');
    }
    $scope.searchBy = "";
    $scope.selectedPatInfo = {};
    var prevpagno, pageno;
    var param = hcueDoctorLoginService.getPatientSearchInfo();
    InitialiseSearchParam();
    $scope.addnewpat = false;
    var i = 0;
    $scope.appointment_type = "";
    $scope.ApointmentAdded = false;
    $scope.showTab = 1;
    $scope.SearchByPatients = 'Y';
    $scope.notifySMS = true;
    $scope.notifyMAIL = false;
    $scope.doctorSMS = false;
    $scope.doctorEmail = false;
    $scope.patientSMS = true;
    $scope.patientEmail = false;
    $scope.drshow = false;
    $scope.referralCreated = '';
    $scope.referralReceived = '';
    $scope.referralProcessed = moment().format('DD-MM-YYYY');
    $scope.presentComplaint = '';
    $scope.provisionDiagnosis = '';
    $scope.appointmenttypeid = '';
    $scope.enquirysourceid = '';
    //var printlines = hcueDoctorLoginService.getprintblankline();
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    $scope.drnameshow = false;

    $scope.HospitalCD = undefined;
    var access = doctorinfo.isAccessRightsApplicable;
    if (access) {
        if (doctorinfo.AccountAccessID.indexOf('ONLYDOCPAT') == -1) {
            $scope.drshow = false;
        } else {
            $scope.drshow = true;
        }
    }
    if (doctorinfo.doctordata.doctorAddress !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined &&
        doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
        $scope.HospitalCD = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
    }
    $scope.hourList = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"];
    $scope.minList = ["00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55"];
    
//     var minpercaseslot = inputData.minpercaseslot;

//     console.log(minpercaseslot +"-------------------------------------");
    
//     var minpercaseslot = parseInt(minpercaseslot);
   
      
//     $scope.minList = ["00"];    
//    var minslot = minpercaseslot;    
//     while(minslot < 60) {
//         $scope.minList.push(minslot.toString());
//         minslot =  minslot + minpercaseslot;
//     }
//     if(!minslot >= 60) {
//         $scope.minList.push(minslot.toString());
//     }
   
    var sliceTime = inputData.sliceTime;
    $scope.HospitalCD = undefined;
    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined &&
        doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
        $scope.HospitalCD = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        var parenthospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
    }
    getSourceListing();

    $scope.showEventDoctors = inputData.showEventDoctors;
    $scope.showAppointmentDoctors = inputData.showAppointmentDoctors;

    $scope.doctorSpecialities = inputData.doctorSpecialities;
    $scope.doctorImage = "";
    $scope.showmandatory = "N";
    if (doctorinfo.doctordata.DoctorSetting !== undefined) {
        if (doctorinfo.doctordata.DoctorSetting.AppmntTypeMandatory !== undefined) {
            $scope.showmandatory = doctorinfo.doctordata.DoctorSetting.AppmntTypeMandatory;

        }

    } else {
        $scope.showmandatory = "N";
    }
    $scope.showRegisterPatient = false;
    $scope.isPatientSelected = false;
    $scope.selectedPatientTitle = "";
    $scope.selectedPatientName = "";
    $scope.selectedPatientAge = 0;
    $scope.selectedPatientGender = "";
    $scope.selectedPatientNumber = "";
    $scope.selectedPatientImage = "";
    var patientinfo = hcueDoctorLoginService.getNewPatientInfo();
    $scope.repeatTypeChange = function(val) {
        switch (val) {
            case "DAY":
                $scope.repeatInterval = 1;
                break;
            case "ALTERDAYS":
                $scope.repeatInterval = 1;
                break;
            case "WEEK":
                $scope.repeatInterval = 1;
                break;
            case "MONTH":
                $scope.repeatInterval = 1;
                break;
        }
    }
    $scope.searchToggle = function() {
        if (!JSON.parse(localStorage.getItem("isAppOnline"))) {
            alertify.log('This is for online users');
            return;
        }

        $scope.SearchBy = "";
        $scope.showRegisterPatient = false;
        $scope.patients.length = 0;
        //$scope.selectedPatInfo={};

        //if($scope.Search.SearchByPatients == "Y")
        //{
        //    $scope.Search.SearchByPatients = "Y";
        //}
        //else
        //{s

        //}
    }





    function OnGetPatientSuccess(tmpData) {

        var data = tmpData;
        var patients = [];
        var patientsdata = data;
        //$scope.setvalue = false;
        //angular.forEach(patientsdata, function (item) {
        //    angular.forEach(item.PatientPhone, function (Value) {
        //        if (Value.PhType == 'M') {
        //            $scope.phoneno = Value.PhNumber;
        //            $scope.setvalue = true;
        //        }
        //        else if (Value.PhType == 'L' && $scope.setvalue == false ) {
        //            $scope.phoneno = (Value.PhStdCD +' '+ Value.PhNumber);
        //        }
        //    });
        //});
        var boolval = !isNaN($scope.SearchBy);
        $scope.showRegisterPatient = false;
        if (patientsdata.length == 0 && boolval == true && $scope.SearchBy.length == 10) {
            $scope.showRegisterPatient = true;
        } else if (patientsdata.length == 0 && $scope.SearchBy.length >= 3) {
            $scope.showRegisterPatient = true;
        }
        $scope.close = function() {
            $scope.addnewpat = false;
        }
        $scope.noresult = false;
        $scope.showloading = false;

        if (patientsdata.length == 0 && $scope.patients.length == 0) {
            $scope.noresult = true;
        }
        $scope.pageno += 1;
        $scope.patients = [];
        $scope.patients = patientsdata;
        $scope.totalpatvalue = {};
        $scope.totalpatvalue = $scope.patients;

    }

    $scope.DisplayAge = function(birthday) {
        return GetAge(birthday);
    }

    $scope.SearchPatient = function() {}
    $scope.onItemSelected = function(visit) {

        if (visit) {
            $scope.appointment_reason = visit.VisitRsnTitle;
        }
    }
    $scope.visitReasonOnChange = function() {

        if ($scope.appointment_reason.length > 3) {
            var getParams = {
                "DoctorID": doctorinfo.doctorid,
                "USRType": "DOCTOR",
                "PageSize": 100,
                "PageNumber": 1,
                "ActiveIND": "Y",
                "USRId": doctorinfo.doctorid,
                "StartsWith": $scope.appointment_reason,
                "loaderStatus": false
            }
            hCueVisitReasonServices.getVisitReasonServicesList(getParams, $scope.HospitalCD, onreadSuccess, onreadFailed);

            function onreadSuccess(data) {
                $scope.visitReasonList = data.rows;

            }

            function onreadFailed(data) {

            }
        }
    }
    $scope.AppointmentCancel = function() {
        $scope.notifySMS = false;
        $scope.notifyMAIL = false;
        $scope.closeBookAppPopup();
        var param = hcueDoctorLoginService.getPatientSearchInfo();

        param.DoctorID = hcueDoctorLoginService.getLoginId();
        // param.PhoneNumber = "";
        // param.FirstName = "";
        param.PageSize = 20;
        param.PageNumber = 1;
        param.Sort = "asc";
        param.SearchText = "";
        param.hasparam = true;
        param.loaderStatus = false;
        hcueDoctorLoginService.addPatientSearchInfo(param);

    }


    $scope.SearchByText = function() {
        var name = $scope.SearchBy;
        if (name == "" || name.length < 3) {
            $scope.patients = [];
            return;
        }
        if ($scope.SearchBy.length >= 3) {
            var searchParm = {
                searchtext: $scope.SearchBy,
                searchtype: 'TRI'
            }
            c7SearchServices.searchPatients(searchParm, OnGetPatientSuccess, OnGetError);

            function OnGetPatientSuccess(data) {
                $scope.patients = [];
                angular.forEach(data, function(i) {
                    var array_list = ['#af77c6', '#e4c120', '#69b3e7', '#ed9945', '#90c752'];
                    var val = array_list[Math.floor(Math.random() * array_list.length)];
                    i.colors = val;
                    $scope.patients.push(i)
                })
            }

            function OnGetError() {

            }

            //var param = hcueDoctorLoginService.getPatientSearchInfo();
            // var param = {
            //     "PageSize": 30,
            //     "DoctorID": "",
            //     "SearchText": $scope.SearchBy,
            //     "PageNumber": 1,
            //     "IncludeCommDetails": "Y",
            //     "SearchDetails": {
            //         "listdocpatients": "Y"
            //     }
            // }
            // param.DoctorID = hcueDoctorLoginService.getLoginId();
            // var doctorinfoaccess = hcueDoctorLoginService.getDoctorInfo();
            // var access = doctorinfoaccess.isAccessRightsApplicable;
            // if (access) {
            //     if (doctorinfoaccess.AccountAccessID.indexOf('ONLYDOCPAT') == -1) {
            //         delete param.SearchDetails;
            //     }
            // }

            // hcueDoctorLoginService.addPatientSearchInfo(param);


            // InitialiseSearchParam();
            // $scope.morepatientdata = false;

            // hcuePatientCaseService.SearchPatient(param, OnGetSearchPatientSuccess, OnGetPatientError);

        }

    }

    // function OnGetSearchPatientSuccess(data)
    // {
    //     var chkSearchParam = hcueDoctorLoginService.getPatientSearchInfo();
    //      $scope.showRegisterPatient=false;

    //      $scope.patients = data.rows;
    //         if(data.rows.length == 0 && $scope.SearchBy.length >=3){
    //             $scope.showRegisterPatient=true;
    //         }    
    //     var boolval = !isNaN($scope.SearchBy);
    //     if (data.message == undefined && data.rows.length == 0 && boolval == true && $scope.SearchBy.length == 10) {
    //         $timeout(function () {
    //             if (data.rows.length == 0) {
    //                 $scope.addnewpat = true;
    //             }

    //         }, 100);
    //     }
    // }
    $scope.close = function() {
            $scope.addnewpat = false;
        }
        // function OnGetPatientError(data)
        // {

    // }


    //
    //
    $scope.SearchByDocAddressID = function() {
        onchageselectappoint = parseInt($scope.LoadAddressConsultID);
    }

    $scope.$on('$locationChangeSuccess', function() {
        if (!$scope.morepatientdata)
            $scope.SearchPatient();
    });

    $scope.toggleGroup = function(item) {
        if ($scope.isGroupShown(item)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = item;
        }
    };

    $scope.isGroupShown = function(item) {
        return $scope.shownGroup == item;
    };

    $scope.closeBookAppPopup = function() {
        $rootScope.$broadcast('CallAppointmentCloseMdl');
    }

    $scope.StartAppointment = function(item) {
        item.Patient = [];
        item.PatientAddress = [];
        item.PatientPhone = [];
        item.Patient.push({ PatientID: item.PatientID, FullName: item.FullName, DOB: item.DOB, Gender: item.Gender });
        item.PatientPhone.push({ PhNumber: item.PhoneNumber });

        UpdateConsultInfo(item, datapersistanceFactory,
            hcueConsultationInfo, hcueDoctorLoginService);
        var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
        var url = "/main?kwid=" + (guid || "");
        $location.url(url);
        //$location.path('/main');
    }

    $scope.ScheduleFollowup = function(item) {}

    $scope.ViewHistory = function(item) {
        item.Patient = [];
        item.PatientAddress = [];
        item.PatientPhone = [];
        item.Patient.push({
            PatientID: item.PatientID,
            FullName: item.FullName,
            DOB: item.DOB,
            Gender: item.Gender
        });
        item.PatientPhone.push({ PhNumber: item.PhoneNumber });
        UpdateConsultInfo(item, datapersistanceFactory,
            hcueConsultationInfo, hcueDoctorLoginService);
        var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
        var url = "/casehistory?kwid=" + (guid || "");
        $location.url(url);
        //$location.path('/casehistory');
    }
    $scope.CustomMessage = function(item) {}

    function InitialiseSearchParam() {
        $scope.patients = [];
        $scope.morepatientdata = true;
        prevpagno = 0;
        pageno = 0;
        $scope.pageno = 1;
    }

    function OnGetPatientError(data) {
        //$ionicLoading.hide();
        alertify.log("Check internet connectivity");
    }

    $scope.getLastVistedDate = function(Visitedate) {
        var thedate = $filter('date')(new Date(Visitedate), "yyyy-MM-dd");
        var currdate = $filter('date')(new Date(), "yyyy-MM-dd");

        var date1 = new Date(thedate);
        var date2 = new Date(currdate);
        var m = moment(date2);
        var years = m.diff(date1, 'years');
        m.add(-years, 'years');
        var months = m.diff(date1, 'months');
        m.add(-months, 'months');
        var days = m.diff(date1, 'days');
        var daysago;
        if (months <= 0) {
            daysago = days.toString() + " Days ago ";
        } else {
            if (months > 1) {
                daysago = months.toString() + " Months " + days.toString() + " Days " + " Ago ";
            } else {
                daysago = months.toString() + " Month " + days.toString() + " Days" + " Ago ";
            }
        }
        return daysago;
    }
    $scope.loadAllPatients = function() {
        $scope.SearchBy = "";
        $scope.selectedPatInfo = {};
        var param = hcueDoctorLoginService.getPatientSearchInfo();
        param.DoctorID = hcueDoctorLoginService.getLoginId();
        // param.PhoneNumber = "";
        //param.FirstName = "";
        param.PageSize = 20;
        param.PageNumber = 1;
        param.SearchText = "";
        param.Count = $scope.count;
        if ($scope.doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && $scope.doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            param.HospitalID = 0;
        }

        param.loaderStatus = true;
        hcueDoctorLoginService.addPatientSearchInfo(param);
        hcuePatientCaseService.SearchPatient(param, onLoadSuccess, onLoadError);
    }

    $scope.$on('$locationChangeSuccess', function() {
        if (!$scope.morepatientdata)
            $scope.SearchPatient();
    });

    //load the time dynamically
    var json = hcueDoctorLoginService.getDocAvailConsultTimeInfo();
    $scope.setValue = json;
    //end of time load dynamically
 
    //Slot time change dynamically
    $scope.slotTimeChange = function() {
       // To change the slotTime 
    var startHour= $scope.StartTimeHour;
    var startMin=$scope.StartTimeMin;

    var endHour = $scope.EndTimeHour;
    var endMin = $scope.EndTimeMin;

    var startHourMin = startHour+":"+startMin;
    var endHourMin = endHour+":"+endMin;

    var startTime = moment(startHourMin, "HH:mm");
    var endTime = moment(endHourMin, "HH:mm");

    var duration = moment.duration(endTime-startTime);

    var hours = parseInt(duration.asHours());
    var minutes = parseInt(duration.asMinutes())-hours*60;
    $scope.slotTimeMins = minutes;
       
    }

    $scope.Endtimechange = function() {
        var app_startingTime = $scope.StartTime;
        $scope.dffTime = parseInt(sliceTime); //hcueDoctorLoginService.getDocMinPerCaseInfo();
        var eee = new Date(1980, 1, 1, 0, 0, 0);
        eee.setHours(app_startingTime.substring(0, 2), app_startingTime.substring(3, 5), 0);
        eee.setMinutes(eee.getMinutes() + $scope.dffTime);
        sdate = eee.getHours() + ":" + eee.getMinutes();
        if (inputData.action == "rebook") {
            $scope.EndtimeVAlue = moment(inputData.TimeSlotDetail.EndTime, "HH:mm"); //sdate;
            $scope.EndtimeVAlue = moment($scope.EndtimeVAlue).format("HH:mm");
        } else {
            $scope.EndtimeVAlue = moment(sdate, "HH:mm"); //sdate;
            $scope.EndtimeVAlue = moment($scope.EndtimeVAlue).format("HH:mm");
        }

    }

    //end of search data

    /*var getset = hcueDoctorLoginService.getSelectedAppointDt();
    if(getset == "") {
       var currdatetime = $routeParams.startDate;
    }
    else
    {
       var currdatetime = hcueDoctorLoginService.getSelectedAppointDt();
    }*/

    var currdatetime = $filter('date')(new Date(inputData.date), "dd-MM-yyyy"); //hcueDoctorLoginService.getSelectedAppointDt();
    var currformatdate = currdatetime //.substring(0, 10); //$filter('date')(new Date(currdatetime), "yyyy-MM-dd");
    $scope.startDate = currformatdate;
    $scope.appDate = currformatdate;
    $scope.appStartDate = $filter('date')(new Date(inputData.date), "dd-MM-yyyy");
    $scope.StartTime = inputData.minuteStart.substring(0, 5); //hcueDoctorLoginService.getSelectedAppointStartTime();
    $scope.StartTimeHour = inputData.minuteStart.substring(0, 2);
    $scope.StartTimeMin = inputData.minuteStart.substring(3, 5);
    $scope.Endtimechange();
    $scope.EndTime = $scope.EndtimeVAlue; //hcueDoctorLoginService.getSelectedAppointEndTime();
    $scope.EndTimeHour = $scope.EndtimeVAlue.substring(0, 2);
    $scope.EndTimeMin = $scope.EndtimeVAlue.substring(3, 5);
    $scope.slotTime =  inputData.minpercaseslot;
    $scope.patient_appDate = currformatdate;
    $scope.patient_problems = "";
    $scope.dtEdit = $filter('date')(new Date(inputData.date), "dd-mm-yy");
    $scope.dtEnd = $filter('date')(new Date(inputData.date), "dd-mm-yy");
    $scope.eventStartDate = $filter('date')(new Date(inputData.date), "MMM d, yyyy");
    $scope.eventEndDate = $filter('date')(new Date(inputData.date), "MMM d, yyyy");
    $scope.event_Startdate = $filter('date')(new Date(inputData.date), "dd-mm-yy");
    $scope.event_Enddate = currformatdate;
    $scope.eventStartHr = inputData.minuteStart.substring(0, 2);
    $scope.eventStartMin = inputData.minuteStart.substring(3, 5);
    $scope.eventEndHr = $scope.EndtimeVAlue.substring(0, 2);
    $scope.eventEndMin = $scope.EndtimeVAlue.substring(3, 5);
    $scope.blnChkPatient = false;
    $scope.apptypeval = "";
    $scope.apptypechange = function(appointment_type) {
        $scope.apptypeval = appointment_type;
    }
    $scope.showAppNotification = true;
    if (new Date($scope.dtEdit).valueOf() < new Date().setHours(0, 0, 0, 0)) {
        $scope.showAppNotification = false;
    }
    //Popup Events
    $scope.appointmentTitle = "Book Appointment";

    /**Repeat Appointment**/
    $scope.repeatEndDate = currformatdate;
    $scope.isRepeat = "N";
    $scope.repeatType = "DAY";
    $scope.repeatInterval = 1;
    /**End of Repeat Appointment**/
    // $scope.openEvent=function(){
    //     $scope.showEventContainer=true;
    //     $scope.showAppointmentContainer=false;
    //     $scope.appointmentTitle="Book Event";
    // }
    //End of Popup Events
    $scope.convertDateymd = function(date) {
        return moment(moment(date, 'DD-MM-YYYY')).format('YYYY-MM-DD')
    }
    $scope.getAddress = window.localStorage.getItem('addressGetData');
    console.log( $scope.getAddress);
    $scope.addAppointmentFormDataSubmit = function() {
        if ($scope.scanCentreDetails == undefined) {
            $scope.scanCentreDetails = $scope.scanCentreValue;
        }

        if ($scope.selectedPatInfo.patientid !== 0 && $scope.selectedPatInfo.patientid !== undefined) {

            if ($scope.selectedPatInfo != undefined && $scope.SearchBy != '') {
                // var EnquiryDetails = {
                //       referalcreated: $scope.convertDateymd($scope.referralCreated),
                //       referalreceived: $scope.convertDateymd($scope.referralReceived),
                //       referalprocessed: $scope.convertDateymd($scope.referralProcessed),
                //       clinicalnotes: $scope.presentComplaint,
                //       clinicaldetails: $scope.provisionDiagnosis,
                //       speciality: $scope.selectedSpeciality,
                //       subspeciality: $scope.selectedSubSpeciality,
                //       appointmenttypeid: $scope.appointmenttypeid,
                //       enquirysourceid: $scope.enquirysourceid,
                //       patientenquiryid: $scope.patientenquiryid,
                //       enquirystatusid: 'TRIAGE',
                //       crtusr: 1,
                //       crtusrtype: 'ADMIN',
                //       updtusr: 1,
                //       updtusrtype: 'ADMIN'
                //   }
                $scope.blnChkPatient = false;
                var isEmergencySlot = false;
                var VisitRsnType = $scope.apptypeval;
                if ($scope.showmandatory == "Y" && $scope.apptypeval == "" || $scope.apptypeval == undefined) {
                    alertify.error('Please select appointment type');
                    return false;
                } else {
                    $scope.appointment_type = $scope.apptypeval;
                    var VisitRsnType = ($scope.appointment_type == "") ? 'OTHERS' : $scope.appointment_type;
                }

                for (var i = 0; i < $scope.setValue.length; i++) {
                    if ($scope.setValue[i].startTime == hcueDoctorLoginService.getSelectedAppointStartTime()) {
                        var consultIDAlert = $scope.setValue[i].consultID;
                    }
                }
                var AddressConsultID = parseInt($scope.getAddress);
                var DoctorID = ($scope.showAppointmentDoctors ? $scope.selectedAppointmentDoctor.DoctorID : inputData.doctorId); //hcueDoctorLoginService.getLoginId();
                
                // var DoctorID =  $scope.doctorinfo.doctorid;

                // console.log($scope.doctorinfo.doctorid);
                // console.log($scope.showEventDoctors ? $scope.selectedDoctor.DoctorID : inputData.doctorId);
                
                var daycd = $filter('date')(moment($scope.appDate, "DD-MM-YYYY").valueOf(), "EEE").toUpperCase();
                var patient_appStartTime = $scope.StartTimeHour + ":" + $scope.StartTimeMin //$scope.StartTime; //$scope.patinet_appStarttime;
                var patient_appEndTime = $scope.EndTimeHour + ":" + $scope.EndTimeMin //($scope.EndTime === "00:00") ? "23:59" : $scope.EndTime; //$scope.EndTime;
                if (patient_appEndTime == "00:00") { patient_appEndTime = "24:00" }
                if (new Date('1-1-2016 ' + patient_appStartTime).getTime() >= new Date('1-1-2016 ' + patient_appEndTime).getTime()) {

                    alertify.error("End Time cannot be equal or lesser than Start Time");
                    return false;
                }
                $scope.slotTimeChange();



                // if ($scope.slotTimeMins != inputData.minpercaseslot) {

                //     alertify.error("Difference between Start Time and End Time should be equal Slot Time");
                //     return false;
                // }

                if (!Number.isInteger($scope.slotTimeMins / inputData.minpercaseslot)) {

                    alertify.error(" Slot Time is invalid");
                    return false;
                }

                if(inputData.emergencyslotstarttime != null && inputData.emergencyslotendtime != null){

                    var emergencystarttime = parseInt(inputData.emergencyslotstarttime.replace(':', ''));
                    var emergencyendtime = parseInt(inputData.emergencyslotendtime.replace(':', ''));
                    var appointmentstarttime = parseInt( $scope.StartTimeHour.toString() + $scope.StartTimeMin.toString());
                    var appointmentendtime = $scope.EndTimeHour.toString() + $scope.EndTimeMin.toString();

                    if(appointmentendtime == "0000"){
                        appointmentendtime = 2400;
                    } else{
                        appointmentendtime = parseInt(appointmentendtime);
                    }

                    if((appointmentstarttime < emergencystarttime) && (appointmentendtime > emergencyendtime)){
                        alertify.error("Emergency time slot is covered completely. Emergency slot is " + inputData.emergencyslotstarttime + " to " +  inputData.emergencyslotendtime);
                        return false;
                    }

                    if(((appointmentstarttime >= emergencystarttime) && (appointmentstarttime <= emergencyendtime)) || 
                    ((appointmentendtime >= emergencystarttime) && (appointmentendtime <= emergencyendtime))){
                        isEmergencySlot = true;
                    }

                }

                var recurringAppointment = "";
                if ($scope.isRepeat == "Y") {
                    if (new Date($scope.appDate).getTime() > new Date($scope.repeatEndDate).getTime()) {
                        alertify.error("Repeat End Date cannot be lesser than Start Date");
                        return false;
                    }
                    var recurringAppointment = {
                        "RecurringAppointment": {
                            "Mode": $scope.repeatType,
                            "Interval": $scope.repeatInterval,
                            "StartDate": $filter('date')(new Date($scope.appDate), "yyyy-MM-dd"),
                            "EndDate": $filter('date')(new Date($scope.repeatEndDate), "yyyy-MM-dd")
                           
                        }
                    };
                }

               
                var patient_appDate = moment($scope.appDate, 'DD-MM-YYYY').format('YYYY-MM-DD');
                // var patient_appDate = $filter('date')(new Date($scope.appDate), "yyyy-MM-dd");
                var patient_id = ($scope.selectedPatInfo.Patient !== undefined) ? $scope.selectedPatInfo.Patient[0].PatientID : $scope.selectedPatInfo.patientid;
                var Final_StartTime = patient_appStartTime; // $scope.StartTime;
                var Final_AddressId = ($scope.showAppointmentDoctors ? $scope.selectedAppointmentDoctor.AddressID : inputData.addressId);
                var visitreason = $scope.appointment_reason;
                //var visitreason = $scope.patient_problems;
                var sendSms = ($scope.showAppNotification && $scope.notifySMS) ? "Y" : "N";
                var sendMail = ($scope.showAppNotification && $scope.notifyMAIL) ? "Y" : "N";
                var date = $filter('date')(new Date(), "yyyy-MM-dd");
                var time = $filter('date')(new Date(), 'HH:mm');
                hcueDoctorLoginService.addDocPatientID(($scope.selectedPatInfo.Patient !== undefined) ? $scope.selectedPatInfo.Patient[0].PatientID : $scope.selectedPatInfo.PatientID);

                // if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
                //     var hospcode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
                //     var branchcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode;
                //     var hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
                //     var parenthospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
                //     c7AppointmentListServices.AddApointmentHospcode($scope.Source, $scope.SubSource, hospcode, branchcd, hospid, parenthospid, Final_StartTime, patient_appEndTime, DoctorID, daycd, patient_appDate, patient_id, Final_AddressId, visitreason, VisitRsnType, sendSms,sendMail,recurringAppointment, OnAddApointmentSuccess, OnCaseSaveError);
                // } else {
                //     var hospcode = undefined;
                //     var branchcd = undefined;
                //     var hospid = undefined;
                //     var parenthospid = undefined;
                //     c7AppointmentListServices.AddApointmentHospcode($scope.Source, $scope.SubSource, hospcode, branchcd, hospid, parenthospid, Final_StartTime, patient_appEndTime, DoctorID, daycd, patient_appDate, patient_id, Final_AddressId, visitreason, VisitRsnType, sendSms,sendMail,recurringAppointment, OnAddApointmentSuccess, OnCaseSaveError);
                // }


                var hospcode = $scope.scanCentreDetails.ExtDetails.HospitalInfo.HospitalCode;
                var branchcd = $scope.scanCentreDetails.ExtDetails.HospitalInfo.BranchCode;
                var hospid = $scope.scanCentreDetails.ExtDetails.HospitalInfo.HospitalID;
                var parenthospid = $scope.scanCentreDetails.ExtDetails.HospitalInfo.ParentHospitalID;
                var userid = hcueDoctorLoginService.getLoginId();
                c7AppointmentListServices.AddApointmentHospcode(hospcode, branchcd, hospid, parenthospid, Final_StartTime, patient_appEndTime, $scope.selectedAppointmentDoctor.DoctorID, userid, AddressConsultID, daycd, patient_appDate, patient_id, Final_AddressId, $scope.patientenquiryid, isEmergencySlot, OnAddApointmentSuccess, OnCaseSaveError);


                function OnAddApointmentSuccess(data) {
                    if (data != "NO_TIME_SLOT_FOR_THE_DOCTOR" && data !="Time Slot Already mapped") {
                        $scope.ApointmentAdded = false;
                        $scope.patients = [];
                        $scope.notifySMS = false;
                        $scope.notifyMAIL = false;
                        alertify.success('Appointment Created Successfully');
                        $scope.closeBookAppPopup();
                    } 
                    // else {
                    //     alertify.error("No timeslot for this Sonographer");
                    // }
                    if(data =="Time Slot Already mapped" ){
                        alertify.error('Time Slot Already mapped')
                    }
                }

                function OnCaseSaveError(data) {
                    alertify.error('Error in Creating Appointment');
                }
            } else {
                $scope.blnChkPatient = true;
                alertify.error("Search for a patient to book an appointment");
            }
        } else {

            alertify.error("Search for a patient to book an appointment");
        }

    }

    function addAppointmentInfoFrmPatient() {

        var DoctorID = inputData.doctorId;
        var daycd = $filter('date')(new Date(currformatdate), "EEE").toUpperCase();
        var patient_appStartTime = $scope.StartTime;
        var patient_appEndTime = ($scope.EndTime === "00:00") ? "23:59" : $scope.EndTime;

        var patient_appDate = $filter('date')(new Date(currformatdate), "yyyy-MM-dd");
        var patient_id = 0;
        var Final_StartTime = $scope.StartTime;
        var Final_ConsultId = inputData.addressConsultId || 0;
        var visitreason = $scope.patient_problems || "";
        $scope.appointment_type = $scope.apptypeval;
        var Final_AddressId = inputData.addressId;
        var VisitRsnType = ($scope.appointment_type == "") ? 'OTHERS' : $scope.appointment_type;
        var sendemail = ($scope.notifyMAIL == true) ? 'Y' : 'N';
        //var VisitRsnType = $scope.apptypeval;
        if ($scope.showAppointmentDoctors) {
            DoctorID = $scope.selectedAppointmentDoctor.DoctorID;
            Final_AddressId = $scope.selectedAppointmentDoctor.AddressID;
        }
        var appointmentDtls = [{ startTime: Final_StartTime, endTime: patient_appEndTime, drID: DoctorID, dayCD: daycd, appointmentDt: patient_appDate, patID: 0, consultID: Final_ConsultId, visitReason: visitreason, visitRsnType: VisitRsnType, addressId: Final_AddressId, SendEmail: sendemail }];

        hcueDoctorLoginService.addAppointmentInfoFrmPatient(appointmentDtls);

    }
    $scope.selectedPatient = function(patientitem) {
        $scope.selectedPatInfo = patientitem;
        $scope.patients = [];
        $scope.isPatientSelected = true;
        $scope.SearchBy = patientitem.patientname;
        $scope.selectedPatientTitle = patientitem.Title == undefined ? "" : (patientitem.Title.length > 0 ? (patientitem.Title + ". ") : "");
        $scope.selectedPatientName = patientitem.patientname;
        $scope.selectedPatientAge = patientitem.age + "y ";
        $scope.selectedPatientGender = patientitem.gender == "M" ? "Male" : "Female";
        // $scope.selectedPatientNumber = patientitem.PhoneNumber!=0?patientitem.PhoneNumber:"";
        $scope.selectedPatientImage = patientitem.imageurl || "";
        $scope.patientenquiryid = patientitem.patientenquiryid;
    }
    $scope.AddPatient = function() {
        addAppointmentInfoFrmPatient();
        //$scope.redirectToPatientadd = true;
        //hcueDoctorLoginService.addredirectToPatientaddPage(true);
        patientinfo.hasphone = false;
        patientinfo.patType = true;
        patientinfo.phonenumber = $scope.SearchBy;
        if (isNaN($scope.SearchBy)) {
            patientinfo.phonenumber = "";
        }
        patientinfo.doctorid = hcueDoctorLoginService.getLoginId();
        hcueDoctorLoginService.addNewPatientInfo(patientinfo);
        hcueDoctorLoginService.addAddPatientwithoutConsult(true);

        $scope.closeBookAppPopup();
        //window.location.path('/addpatient/' + 'true');

        //window.parent.location.href = "#/addpatient/"; 
        /*var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0,3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
        var url = "/addpatient?kwid=" +( guid || "");
        $location.url(url);*/
    }


    //referral subreferral
    $scope.SourceChange = function(sourceid) {

        if (sourceid == '' || sourceid == undefined) {
            $scope.SubSource = "";
            $scope.subSourceList = "";
            return;
        }
        var params = {
            "DoctorID": doctorinfo.doctorid,
            "HospitalCD": $scope.HospitalCD,
            "USRType": "DOCTOR",
            "CatagoryID": parseInt(sourceid),
            "PageSize": 100,
            "PageNumber": 1,
            "USRId": doctorinfo.doctorid
        }

        hCueReferralServices.listSubVisitReferral(params, listSubSourceSuccess, listSubSourceError);

        function listSubSourceSuccess(data) {
            if (data.status !== "failure") {
                var tmp = [];
                for (var i in data.response.rows) {
                    if (data.response.rows[i].ActiveIND == "Y") {
                        tmp.push(data.response.rows[i]);
                    }
                }
                $scope.subSourceList = tmp;
                $scope.SubSource = "";

            } else {
                $scope.SubSource = "";
                $scope.subSourceList = "";
                //alertify.error("No SubSource Available");
            }
        }

        function listSubSourceError(data) {

        }

    }

    function getSourceListing() {
        // var getParams = { "DoctorID": doctorinfo.doctorid, "USRType": "DOCTOR", "PageSize": 1000, "PageNumber": 1, "USRId": doctorinfo.doctorid }
        // hCueReferralServices.getReferralServicesList(getParams,$scope.HospitalCD, onSourceSuccess, onSourceFailed);

        // function onSourceSuccess(data) {
        //     if (data.rows.length > 0) {
        //         $scope.ReferralList = data.rows;

        //     }
        //     else {

        //     }


        // }
        // function onSourceFailed(data) {

        // }

        var hoslistparam = {
            "pageNumber": 1,
            "HospitalID": parenthospid,
            "pageSize": 1000,
            "IncludeDoctors": "Y",
            "IncludeBranch": "Y",
            "ActiveHospital": "Y",
            "ActiveDoctor": "Y"

        }
        $scope.attachstatustype = '';
        c7AdminServices.getHospitals(hoslistparam, OnGetDoctorSuccess, OnGetDoctorError);

        function OnGetDoctorSuccess(data) {
            if (!data.error) {
                clinicAddress = [];
                for (dt of data.rows) {
                    if (dt.HospitalInfo.HospitalDetails.ActiveIND == "Y" && dt.DoctorDetails.length > 0) {
                        var doctorNewCollection = [];
                        angular.forEach(dt.DoctorDetails, function(j) {
                            if (j.RoleID == 'SONOGRAPHER')
                                doctorNewCollection.push(j);
                        });
                        var item = {
                            "ClinicName": dt.HospitalInfo.HospitalDetails.HospitalName,
                            "Address1": dt.HospitalInfo.HospitalAddress.Address1,
                            "Address2": dt.HospitalInfo.HospitalAddress.Address2,
                            "Location": dt.HospitalInfo.HospitalAddress.Location,
                            "CityTown": dt.HospitalInfo.HospitalAddress.CityTown,
                            "State": dt.HospitalInfo.HospitalAddress.State,
                            "PinCode": dt.HospitalInfo.HospitalAddress.PinCode,
                            "Country": dt.HospitalInfo.HospitalAddress.Country,
                            "Active": dt.HospitalInfo.HospitalDetails.ActiveIND,
                            "Doctors": doctorNewCollection,
                            "ExtDetails": {
                                "HospitalID": dt.HospitalInfo.HospitalDetails.HospitalID,
                                "PrimaryIND": "Y",
                                "AddressCommunication": [],
                                "HospitalInfo": {
                                    "HospitalID": dt.HospitalInfo.HospitalDetails.HospitalID,
                                    "ParentHospitalID": dt.HospitalInfo.HospitalDetails.ParentHospitalID,
                                    "HospitalCode": doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode,
                                    "BranchCode": "",
                                    "Preference": {}
                                }
                            },
                        };
                        clinicAddress.push(item);
                    }
                }
                $scope.AppListdoctoraddress = clinicAddress;
               // var selclinic = datacookieFactory.getItem('selectedscancenter');
                var selclinic = JSON.parse(window.localStorage.getItem('selectedscancenter'));
              if (selclinic == null) {
                    $scope.scanCentreValue = $scope.AppListdoctoraddress[0];
              } else {
                    $scope.scanCentreValue = $scope.AppListdoctoraddress.filter(data => data.ClinicName == selclinic.ClinicName)[0];
              }
                // datacookieFactory.remove('selectedscancenter');
                //  $scope.initialiseClinicDoctor();
            }
        }

        function OnGetDoctorError(msg) {
            alertify.error('Error in Appointment Booking');
        }



    }

    //Create Event    
    $scope.rescheduleAppointment = false;
    $scope.isCreateEvent = false;
    //$scope.isAllDay=false;
    $scope.isAllDay = inputData.TimeSlotDetail.isAllDay;
    $scope.isCreateEvent = inputData.TimeSlotDetail.isAllDay;
    $scope.isImportant = false;
    $scope.eventDesc = "";
    $scope.eventTitle = "";
    $scope.eventVenue = "";
    $scope.cancelReason = "";
    if ($scope.showEventDoctors) {
        $scope.clinicDoctorList = inputData.clinicDoctorList;
        $scope.selectedDoctor = $scope.clinicDoctorList[0];
    }
    if ($scope.showAppointmentDoctors) {
        $scope.clinicAppDoctorList = angular.copy(inputData.clinicDoctorList);
        $scope.clinicAppDoctorList.shift();
        angular.forEach($scope.clinicAppDoctorList, function(item) {
            if (item.DoctorID == doctorinfo.doctorid) {
                $scope.selectedAppointmentDoctor = item;
            }
        });
        if ($scope.selectedAppointmentDoctor != undefined) {
            $scope.doctorName = $scope.selectedAppointmentDoctor.FullName;
        } else {
            $scope.clinicAppDoctorList = angular.copy(inputData.clinicDoctorList);
            // $scope.clinicAppDoctorList.shift();
            angular.forEach($scope.clinicAppDoctorList, function(item) {
                if (item.DoctorID == inputData.doctorId) {
                    $scope.selectedAppointmentDoctor = item;
                }
            });
            $scope.doctorName = $scope.selectedAppointmentDoctor.FullName;
        }
    } else {
        $scope.doctorName = inputData.doctorName;
    }
    $scope.clinicAddress = [];
    $scope.doctorList = [];
    $scope.doctorSMS = false;
    $scope.doctorEmail = false;
    $scope.patientSMS = true;
    $scope.patientEmail = false;
    $scope.bookType = "Booking";
    $scope.bookTypeBtn = "BOOK";
    $scope.appStatus = "";
    if (inputData.action == "rebook") {
        $scope.bookType = "Rebooking";
        $scope.bookTypeBtn = "REBOOK";
        $scope.isPatientSelected = true;
        $scope.selectedPatInfo = { Patient: [{ PatientID: 0 }] };
        var patInfo = inputData.TimeSlotDetail.PatientInfo;
        switch (patInfo.appointmentStatus) {
            case "E":
                $scope.appStatus = "Completed";
                break;
            case "S":
                $scope.appStatus = "Started";
                break;
            case "I":
                $scope.appStatus = "Checked-In";
                break;
        }
        $scope.selectedPatInfo.PatientID = patInfo.patientID;
        $scope.selectedPatInfo.Patient[0].PatientID = patInfo.patientID;
        $scope.SearchBy = patInfo.fullName;
        $scope.selectedPatientName = (patInfo.title != undefined && patInfo.title.length > 0 ? (patInfo.title + ". ") : "") + patInfo.fullName;
        $scope.selectedPatientAge = patInfo.currentAge != undefined ? (patInfo.currentAge.year + "y " + (patInfo.currentAge.month != "0" ? (patInfo.currentAge.month + "m") : "")) : "";
        $scope.selectedPatientGender = patInfo.sex == "M" ? "Male" : "Female";
        $scope.selectedPatientNumber = patInfo.mobileNumber == "0" ? "" : patInfo.mobileNumber;
        $scope.selectedPatientImage = patInfo.profileImage;
    }

    $scope.createEvent = function() {
        $scope.isCreateEvent = true;
        $scope.doctorName = $scope.selectedDoctor.FullName;
    }
    $scope.createAppointment = function() {
        $scope.isCreateEvent = false;
        if ($scope.showAppointmentDoctors) {
            $scope.doctorName = $scope.selectedAppointmentDoctor.FullName;
        } else {
            $scope.doctorName = inputData.doctorName;
        }
    }
    $scope.changeDoctor = function() {
        $scope.doctorName = $scope.selectedDoctor.FullName;
    }
    $scope.changeAppointmentDoctor = function() {
        $scope.doctorName = $scope.selectedAppointmentDoctor.FullName;
    }
    $scope.changeScanCentre = function(value) {
        $scope.scanCentreDetails = value;
    }
    $scope.chgAppDt = function(dt) {
        $scope.appStartDate = $filter('date')(new Date(dt), "dd-MM-yyyy");
        if (new Date(dt).valueOf() < new Date().setHours(0, 0, 0, 0)) {
            $scope.showAppNotification = false;
            $scope.notifySMS = false;
            $scope.notifyMAIL = false;
        } else {
            $scope.showAppNotification = true;
            $scope.notifySMS = true;
            $scope.notifyMAIL = false;
        }
    }
    $scope.EndDatechange = function(enddate) {
        $scope.event_Enddate = $filter('date')(new Date(enddate), "dd-mm-yy")
    }
    $scope.toggleAllDay = function() {
        $scope.event_Startdate = $filter('date')(new Date(inputData.date), "dd-mm-yy");
        $scope.event_Enddate = $filter('date')(new Date(inputData.date), "dd-mm-yy");
        $scope.dtEdit = $filter('date')(new Date(inputData.date), "dd-mm-yy");
        $scope.dtEnd = $filter('date')(new Date(inputData.date), "dd-mm-yy");
        if (!$scope.isAllDay) {
            $scope.eventStartHr = inputData.minuteStart.substring(0, 2);
            $scope.eventStartMin = inputData.minuteStart.substring(3, 5);
            $scope.eventEndHr = $scope.EndtimeVAlue.substring(0, 2);
            if ($scope.eventEndHr == "00") {
                $scope.eventEndMin = "00";
            } else {
                $scope.eventEndMin = $scope.EndtimeVAlue.substring(3, 5);
            }
        } else {
            $scope.eventStartHr = "00";
            $scope.eventStartMin = "00";
            $scope.eventEndHr = "00";
            $scope.eventEndMin = "00";
        }
        $scope.eventStartDate = $filter('date')(new Date(inputData.date), "MMM d, yyyy");
        $scope.eventEndDate = $filter('date')(new Date(inputData.date), "MMM d, yyyy");
        $scope.toggleImportant();
    }
    $scope.toggleImportant = function() {
        if ($scope.isImportant) {
            $scope.showReschedule = true;
            $scope.rescheduleAppointment = false;
            $scope.showPatNotification = true;
            $scope.doctorSMS = false;
            $scope.doctorEmail = false;
            $scope.patientSMS = true;
            $scope.patientEmail = false;
            if (new Date($scope.dtEnd).valueOf() < new Date().setHours(0, 0, 0, 0)) {
                $scope.showPatNotification = false;
            }
            if (new Date($scope.dtEdit).valueOf() != new Date($scope.dtEnd).valueOf()) {
                $scope.showReschedule = false;
            }
        }
    }
    
    $scope.chgDate = function(startdate) {
        $scope.dtEdit = $filter('date')(new Date(startdate), "dd-mm-yy");
        $scope.dtEnd = $filter('date')(new Date(startdate), "dd-mm-yy");
        $scope.eventStartDate = $filter('date')(new Date(startdate), "MMM d, yyyy");
        $scope.eventEndDate = $filter('date')(new Date(startdate), "MMM d, yyyy");
        $scope.toggleImportant();
    }
    $scope.chgendDate = function(dtEnd) {
        $scope.dtEnd = $filter('date')(new Date(dtEnd), "dd-mm-yy");
        $scope.eventEndDate = $filter('date')(new Date(dtEnd), "MMM d, yyyy");
        $scope.toggleImportant();
    }
    $scope.bookEvent = function() {
        var params = {
            "AppointmentStatus": "EVENT",
            "StartTime": $scope.eventStartHr + ":" + $scope.eventStartMin,
            "EndTime": $scope.eventEndHr + ":" + $scope.eventEndMin,
            "DoctorID": ($scope.showEventDoctors ? $scope.selectedDoctor.DoctorID : inputData.doctorId),
            "HospitalID": inputData.hospitalId,
            "DayCD": $filter('date')(new Date($scope.dtEdit), "EEE").toUpperCase(),
            "StartDate": $filter('date')(new Date($scope.dtEdit), "yyyy-MM-dd"),
            "EndDate": $filter('date')(new Date($scope.dtEnd), "yyyy-MM-dd"),
            "AddressID": ($scope.showEventDoctors ? $scope.selectedDoctor.AddressID : inputData.addressId),
            "USRId": hcueDoctorLoginService.getLoginId(),
            "USRType": "DOCTOR",
            "RescheduleAppointment": $scope.rescheduleAppointment ? "Y" : "N",
            "EventName": $scope.eventTitle == "" ? "Events" : $scope.eventTitle,
            "EventDescription": $scope.eventDesc,
            "EventLocation": $scope.eventVenue,
            "Reason": ($scope.isImportant && ($scope.patientSMS || $scope.patientEmail)) ? $scope.cancelReason : "",
            "SendNotification": {
                "sendPatientSMS": "N",
                "sendPatientEmail": "N",
                "sendDoctorSMS": $scope.doctorSMS ? "Y" : "N",
                "sendDoctorEmail": $scope.doctorEmail ? "Y" : "N"
            }
        }
        if ($scope.isAllDay) {
            params["StartTime"] = "00:00";
            params["EndTime"] = "23:59";
        }
        if ($scope.isImportant) {
            params["AppointmentStatus"] = "IMPEVENT";
            params["SendNotification"]["sendPatientSMS"] = $scope.patientSMS ? "Y" : "N";
            params["SendNotification"]["sendPatientEmail"] = $scope.patientEmail ? "Y" : "N";
        }
        if (params.EndDate > params.StartDate) {
            hcueDoctorAppointmentService.AddUpdateEvent(params, onSourceSuccess, onSourceFailed);
        } else if (params.EndDate == params.StartDate && new Date('1-1-2016 ' + params.StartTime).getTime() < new Date('1-1-2016 ' + params.EndTime).getTime()) {
            hcueDoctorAppointmentService.AddUpdateEvent(params, onSourceSuccess, onSourceFailed);
        } else {
            alertify.error("End Time cannot be equal or lesser than Start Time");
        }

        function onSourceSuccess(data) {
            $scope.closeBookAppPopup();
            if ($scope.isImportant && $scope.rescheduleAppointment) {
                alertify.success('Event Created Successfully. Appointments are rescheduled at background, kindly refresh after sometime.');
            } else if ($scope.isImportant && !$scope.rescheduleAppointment) {
                alertify.success('Event Created Successfully. Appointments are cancelled at background, kindly refresh after sometime.');
            } else {
                alertify.success('Event Created Successfully');
            }
            //alertify.success('Event Created Successfully');

        }

        function onSourceFailed(data) {
            $scope.closeBookAppPopup();
            alertify.error('Error in Creating Event');
        }
    }
    if ($scope.isAllDay) {
        $scope.toggleAllDay();
        if ($scope.showEventDoctors) { $scope.doctorName = $scope.selectedDoctor.FullName; }
    }

    // Speciality
    // c7AdminServices.SpecialityList(parenthospid, specialitylistSuccess, specialitylistError)

    // function specialitylistSuccess(data){
    //     $scope.specialityCollection=data;
    //     $scope.statusSuccess.speciality += 1;
    //     if ($scope.statusSuccess.speciality == 2) {
    //         loadspeciality();
    //     }
    // }

    // function specialitylistError (data){
    //     alertify.error('Error loading speciality');
    // }
    // $scope.selectspeciality = function () {

    //     $scope.selectedSpeciality = $scope.specialityCollection.selected.specialitydesc;
    //     if ($scope.specialityCollection.selected.subspeciality.length > 0) {
    //         $scope.subSpecialityCollection = $scope.specialityCollection.selected.subspeciality;
    //     }
    //   //  validateVal();
    // }

    // $scope.selectsubspeciality = function () {
    //  $scope.selectedSubSpeciality = $scope.subSpecialityCollection.selected.DoctorSpecialityDesc;
    // }

    // function loadspeciality() {

    //     if ($scope.specialityCollection != undefined) {

    //         $scope.specialityCollection.selected = $scope.specialityCollection.filter(data => data.specialitydesc == $scope.addUpdatePat.patientDetails.EnquiryDetails.speciality)[0];
    //         if ($scope.specialityCollection.selected != undefined && $scope.specialityCollection.selected.subspeciality.length > 0) {
    //             $scope.subSpecialityCollection = $scope.specialityCollection.selected.subspeciality;
    //             $scope.subSpecialityCollection.selected = $scope.subSpecialityCollection.filter(data => data.DoctorSpecialityDesc == $scope.addUpdatePat.patientDetails.EnquiryDetails.subspeciality)[0];
    //         }
    //     }

    //     $scope.statusSuccess.speciality = 0;
    // }

}]);