﻿hCueDoctorWebApp.lazyController('hCueReportsController', ['$scope', '$filter', '$rootScope', 'hcueLoginStatusService', 'hcueDoctorLoginService', 'datacookieFactory', '$location', 'hCueReportCashServices', '$timeout', function ($scope, $filter, $rootScope, hcueLoginStatusService, hcueDoctorLoginService, datacookieFactory, $location, hCueReportCashServices, $timeout) {

    /***Speciality Checking***/
    $scope.ChckSpeciality = false;
    if (doctorinfo.specialitycode) {
        for (var specIndex in doctorinfo.specialitycode) {
            if (doctorinfo.specialitycode[specIndex] == "DNT") {
                $scope.ChckSpeciality = true;
            }
        }
    }
    /***End of Speciality Checking***/

    /*** Hospital level Checking ***/
    $scope.hospcode = false;
    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
        $scope.hospcode = true;
    }
    /*** End of Hospital level Checking ***/

     $scope.tabRefresh = function() {
         $rootScope.$emit("refreshTab", {});
     }
	 	    $scope.topReferral = false;
	 if (doctorinfo.AccountAccessID.indexOf('TOPREFERRA') !== -1) {
		    $scope.topReferral = true;
	 }


  $scope.enablemisreport = false;
  if (doctorinfo.AccountAccessID.indexOf('MISREPORTS') !== -1) {
       $scope.enablemisreport = true;
  }


 }]);