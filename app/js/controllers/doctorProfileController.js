﻿hCueDoctorWebApp.lazyController('hCueDoctorProfileController', ['$scope', '$location', '$filter', '$window', 'datapersistanceFactory', 'hcueDoctorSignUp', 'hcueDoctorLoginService', 'hcueDoctorDataService', 'hcueAuthenticationService', 'hcueLoginStatusService', 'datacookieFactory', 'hcueDoctorProfileService', 'Lightbox', 'alertify', '$rootScope', function ($scope, $location, $filter, $window, datapersistanceFactory, hcueDoctorSignUp, hcueDoctorLoginService, hcueDoctorDataService, hcueAuthenticationService, hcueLoginStatusService, datacookieFactory, hcueDoctorProfileService, Lightbox, alertify, $rootScope) {
    /* Login details */
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === "true") {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
        $scope.LoadDoctorName();
    }
    else {
        $location.path('/login');
    }
    $scope.showSettingList = false;
    $scope.profileImgPupop = false;
    // $scope.clinicDoctorImagepopup = false;
    $scope.ShowTab = function (no) {
        ////console.log(no);
        $scope.tabno = no;
    };
      $scope.loadingPage = function() {
        $scope.showPage = false;
        setTimeout(function() {
            $scope.showPage = true;
        }, 100);
    }

    $scope.loadingPage();

    $scope.ShowTab(1);
    $scope.movetoabout = function() {
        $location.path('/profile');
    }

    $scope.openLightboxModal = function (index) {
        Lightbox.openModal($scope.images, index);
    };
    /* End of login details */
    var specialitylist = hcueDoctorDataService.GetLookUpData();
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    $scope.drnameshow = false;
    

    /* Set the country code */
    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes !== undefined) {
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.countryCode !== undefined) {
            $scope.responsecntryCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.CountryCodes.countryCode;
            $scope.splitcd = $scope.responsecntryCode.split('+');
            if ($scope.splitcd.length == 1) {
                $scope.cntryCode = $scope.splitcd[0];
            }
            else {
                $scope.cntryCode = $scope.splitcd[1];
            }
        }
        else {
            $scope.cntryCode = "91";
        }
    }
    else {
        $scope.cntryCode = "91";
    }
  
    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    $scope.editmode = true;
    $scope.emailpattern = emailfilter;
    var speciality_default_push = {"DoctorSpecialitySEODesc":"","DoctorSpecialityID":"","DoctorSpecialityDesc":"Select the speciality","SpecialityDesc":"","ActiveIND":"Y"};
    doctorinfo.specialitylist.unshift(speciality_default_push);
    $scope.specialitylist = doctorinfo.specialitylist;
    //$scope.tabno = 1;
    $scope.doctorinfo = doctorinfo.doctordata;
    $scope.referraldoctors = doctorinfo.referraldoctors;
    if (datapersistanceFactory.getItem('doctorProfileImage') != "") {
        $scope.doctorinfo.profileimage = GetImage(datapersistanceFactory.getItem('doctorProfileImage'));
	}
	function profImg(){
	    if ($scope.doctorinfo.profileimage == null || $scope.doctorinfo.profileimage.length == 0) {
	        $scope.profilepopup = 'Upload a Profile Picture';
	        $scope.profilepopupshow = false;
	    } else {
	        $scope.profilepopup = 'Upload / Remove Profile Picture';
	        $scope.profilepopupshow = true;
	    }
	}
   profImg()

    var logininfo = hcueDoctorSignUp.getProfileInfo();
    var currentdaycode = new Date();
    $scope.currentdaycode = $filter('date')(currentdaycode, "EEE").toUpperCase();


    if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID == undefined || doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalID == undefined) 
    {
        $scope.disableAddhospital = false;
    }
    else {
        $scope.disableAddhospital = true;
        
    }
    var spcialityList_data = [];

    angular.forEach(doctorinfo.specialitylist, function (item) {
        angular.forEach(doctorinfo.doctordata.SpecilityDetails, function (spcialityList) {
            if (item.DoctorSpecialityID == spcialityList.SepcialityCode) {
                spcialityList_data.push(spcialityList);
            }
        });
    });
    logininfo.specialization = spcialityList_data;
    function getDoctorInfoData() {
        if (doctorinfo.doctorid > 0) {
            var data = doctorinfo.doctordata.doctor[0];
            logininfo.Name = data.FullName;
			console.log(JSON.stringify(doctorinfo.doctordata));
            // logininfo.specialization = GetDataspeciality(data.SpecialityCD);
			if(doctorinfo.doctordata.doctorPhone !== undefined)
			{
    			if(doctorinfo.doctordata.doctorPhone.length !== 0) {
    			    angular.forEach(doctorinfo.doctordata.doctorPhone, function (val) {
    			        if (val.PhType == "M") {
    			            logininfo.MobileNumber = val.PhNumber;
    			            logininfo.PhCntryCD = $scope.cntryCode;
    			        }
    			    });                    
    			}
			}
			if(doctorinfo.doctordata.doctorEmail !== undefined)
			{
    			if(doctorinfo.doctordata.doctorEmail.length !== 0) {
    				logininfo.Email = doctorinfo.doctordata.doctorEmail[0].EmailID;
    			}
                else {
                    logininfo.Email = data.DoctorLoginID;
                }
			} else {
			    logininfo.Email = data.DoctorLoginID;
			}
            if(data.MemberID !== undefined) {
                logininfo.MedicalRegNumber = data.MemberID[1];
            }

            logininfo.Experience = data.Exp;
            logininfo.About = GetDictionaryValues(data.About);
            logininfo.Gender = data.Gender;
            if(data.DOB !== undefined && data.DOB !== "Invalid date") {
                logininfo.DOB = $filter('date')(data.DOB, "yyyy-MM-dd");
            }
            logininfo.Qualification = GetDatas(data.Qualification);
            logininfo.Services = GetDatas(data.Services);
            logininfo.Memberships = GetDatas(data.Membership);
            logininfo.Awards = GetDatas(data.Awards);
            logininfo.facebook = GetUrl('facebook' + doctorinfo.doctorid);
            logininfo.linkedin = GetUrl('linkedin' + doctorinfo.doctorid);
            logininfo.twitter = GetUrl('twitter' + doctorinfo.doctorid);
            logininfo.googleplus = GetUrl('googleplus' + doctorinfo.doctorid);
            //   logininfo.MedicalRegNumber = GetDatas(data.MemberID);

        }
    }
    getDoctorInfoData();

    $scope.showSingleTime = true;
    $scope.showMultipleTime = false;
    $scope.showTimingsset = function(status,val) {
        if(status) {
            $scope.showSingleTime = false;
            $scope.showMultipleTime = true;
        }
        else {
            $scope.showSingleTime = true;
            $scope.showMultipleTime = false;
        }
    }

    //logininfo.Qualification = GetDatas(doctorinfo.doctordata.doctor[0].Qualification);
    var edu_collection = [];
    //logininfo.doctorEducation = doctorinfo.doctordata.doctorEducation;
    angular.forEach(doctorinfo.doctordata.doctorEducation,function(edu_item){
        ////console.log(edu_item);
        var split_year = edu_item.FullName;
        if(split_year.split("-") == undefined) {
            ////console.log('123');
        }
        else {
            var year = split_year.split("-");
        }
        edu_collection.push({
            'RowID':edu_item.RowID,
            'EducationName':edu_item.EducationName,
            'InstituteName':edu_item.InstituteName,
            'edu_startyear':year[0],
            'edu_endyear':year[1]
        });
    });
    logininfo.doctorEducation = edu_collection;
    logininfo.Services = GetDatas(doctorinfo.doctordata.doctor[0].Services);
    logininfo.Memberships = GetDatas(doctorinfo.doctordata.doctor[0].Membership);
    logininfo.Awards =  GetDatas(doctorinfo.doctordata.doctor[0].Awards);
    $scope.editmode = true;
    $scope.logininfo = logininfo;
    //logininfo.DoctorPublishing = doctorinfo.doctordata.DoctorPublishing;
    //logininfo.DoctorAchievements = doctorinfo.doctordata.DoctorAchievements;
	if(doctorinfo.doctordata.DoctorPublishing !== undefined) {

        logininfo.DoctorPublishing = doctorinfo.doctordata.DoctorPublishing;
    }
	else {
		logininfo.DoctorPublishing.length = 0;
	}

    if(doctorinfo.doctordata.DoctorAchievements !== undefined) {

        logininfo.DoctorAchievements = doctorinfo.doctordata.DoctorAchievements;
    }
	else{
		logininfo.DoctorAchievements.length = 0;
	}

    $scope.edit_contact_view = false;
    $scope.contact_view_con = true;

    //cancel

    $scope.contact_cancel = function() {
        $scope.edit_contact_view = false;
        $scope.contact_view_con = true;
        if($scope.edit_contactTemp) {
            logininfo.Email = $scope.email_temp;
            logininfo.MobileNumber = $scope.phone_temp;
            logininfo.PhCntryCD = parseInt($scope.phone_CntryCd);
        }
    }


    
    $scope.about_cancel = function() {
        $scope.edit_info = false;
        $scope.about_view_container = true;
        if($scope.aboutedit_status) {
            logininfo.About = $scope.aboutTemp;
            logininfo.DOB = $scope.dobTemp;
            logininfo.Gender = $scope.genderTemp;
        }

        /*
        $scope.edit_aboutForm.$setPristine(true);
        $scope.edit_aboutForm.submited = false;
        $scope.edit_aboutForm.$setUntouched();*/
    }
    
    
    $scope.profile_top_cancel = function() {
        $scope.exp_edit_view = false;
        $scope.exp_view_con = true;
        $scope.medical_edit_view = false;
        $scope.medical_view_con = true;
        if($scope.profileToeedit_status) {
            logininfo.Experience = $scope.expTemp;
            logininfo.MedicalRegNumber = $scope.medicalTemp;
            $scope.experience_valid = false;
        }
    }

    
    $scope.education_cancel = function() {
        $scope.eudcation_edit_view = false;
        $scope.education_view_container = true;
        $scope.education_add_container = false;
        if($scope.edutempStatus) {
            logininfo.doctorEducation = $scope.educationTemp;
        }
        $scope.edu_name = undefined;
        $scope.edu_year = undefined;
        $scope.edu_institute = undefined;
        
        /*$scope.edit_educationForm.$setPristine(true);
        $scope.edit_educationForm.submited = false;
        $scope.edit_educationForm.$setUntouched();
        
        $scope.add_educationForm.$setPristine(true);
        $scope.add_educationForm.submited = false;
        $scope.add_educationForm.$setUntouched();*/
        
    }

    
    $scope.services_cancel = function() {
        $scope.edit_view_services = false;
        $scope.services_view_container = true;
        $scope.add_service_container = false;
        if($scope.servicesTempStatus) {
            logininfo.Services = $scope.servicesTemp;
        }
        $scope.services_addset = undefined;
        
        /*$scope.services_editForm.$setPristine(true);
        $scope.services_editForm.submited = false;
        $scope.services_editForm.$setUntouched();
        
        $scope.services_addForm.$setPristine(true);
        $scope.services_addForm.submited = false;
        $scope.services_addForm.$setUntouched();*/
    }
    
    
    $scope.achievments_cancel = function() {
        $scope.edit_view_achievments = false;
        $scope.achievments_view_container = true;
        $scope.add_achievements_container = false;
        if($scope.AchievmentsTempStatus) {
            logininfo.DoctorAchievements = $scope.achievementsTemp;
        }
        $scope.add_achievmentsset = undefined;
        
        /*$scope.edit_achievmentsForm.$setPristine(true);
        $scope.edit_achievmentsForm.submited = false;
        $scope.edit_achievmentsForm.$setUntouched();
        
        $scope.add_achievmentsForm.$setPristine(true);
        $scope.add_achievmentsForm.submited = false;
        $scope.add_achievmentsForm.$setUntouched();*/
    }
    
    $scope.publication_cancel = function() {
        $scope.edit_view_publication = false;
        $scope.publication_view_container = true;
        $scope.publication_add_container = false;
        if($scope.publicationTempStatus) {
            logininfo.DoctorPublishing = $scope.publicationTemp;
        }
        $scope.addPublication = undefined;
        
        /*$scope.publication_editForm.$setPristine();
        $scope.publication_editForm.submited = false;
        $scope.publication_editForm.$setUntouched();
        
        $scope.publication_addForm.$setPristine();
        $scope.publication_addForm.submited = false;
        $scope.publication_addForm.$setUntouched();*/
    }
    
    
    $scope.membership_cancel = function() {
        $scope.edit_view_membership = false;
        $scope.membership_view_container = true;
        $scope.add_view_membership = false;
        $scope.membership_addset = undefined;

        if($scope.membershiptEditStatus) {
            logininfo.Memberships = $scope.membershipTempData;
        }

        /*$scope.edit_membershipForm.$setPristine(true);
        $scope.edit_membershipForm.submited = false;
        $scope.edit_membershipForm.$setUntouched();
        
        $scope.add_membershipForm.$setPristine(true);
        $scope.add_membershipForm.submited = false;
        $scope.add_membershipForm.$setUntouched();*/
    }

    
    
    $scope.awards_cancel = function() {
        $scope.edit_view_awards = false;
        $scope.awards_view_container = true;
        $scope.awards_add_container = false;
        $scope.awards_addset = undefined;

        if($scope.awardsEditStatus) {
            logininfo.Awards = $scope.awardsTempData;
        }

        /*$scope.awards_editform.$setPristine(true);
        $scope.awards_editform.submited = false;
        $scope.awards_editform.$setUntouched();
        
        $scope.awards_addForm.$setPristine(true);
        $scope.awards_addForm.submited = false;
        $scope.awards_addForm.$setUntouched();*/
    }


    $scope.speciality_cancel = function() {
        $scope.speciality_edit_view = false;
        $scope.speciality_view_container = true;
        $scope.speciality_add_container = false;
        $scope.speciality_add = undefined;
        if($scope.specialityTempstatus) {
            logininfo.specialization = $scope.specialityTemp;
        }
        /*
        $scope.edit_specialityForm.$setPristine(true);
        $scope.edit_specialityForm.submited = false;
        $scope.edit_specialityForm.$setUntouched();

        $scope.add_specialityForm.$setPristine(true);
        $scope.add_specialityForm.submited = false;
        $scope.add_specialityForm.$setUntouched();*/
    }
    //end of cancel

   
    //Speciality
    $scope.speciality_view_container = true;
    $scope.speciality_edit = function() {
        $scope.speciality_edit_view = true;
        $scope.speciality_view_container = false;
        $scope.speciality_add_container = false;
        $scope.specialityTempstatus = true;
        $scope.speciality_add = undefined;
        $scope.specialityTemp = angular.copy(logininfo.specialization);
        $scope.education_cancel();
        $scope.services_cancel();
        $scope.achievments_cancel();
        $scope.membership_cancel();
        $scope.awards_cancel();
        $scope.publication_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }
    $scope.add_speciality = function() {
        $scope.speciality_edit_view = false;
        $scope.speciality_view_container = true;
        $scope.speciality_add_container = true;
        $scope.speciality_add = "";
        $scope.education_cancel();
        $scope.services_cancel();
        $scope.achievments_cancel();
        $scope.membership_cancel();
        $scope.awards_cancel();
        $scope.publication_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }
    $scope.edit_contact = function() {
        $scope.edit_contact_view = true;
        $scope.contact_view_con = false;
        $scope.edit_contactTemp = true;
        $scope.email_temp = angular.copy(logininfo.Email);
        $scope.phone_temp = angular.copy(logininfo.MobileNumber);
        $scope.phone_CntryCd = angular.copy(logininfo.PhCntryCD);
        $scope.education_cancel();
        $scope.services_cancel();
        $scope.achievments_cancel();
        $scope.membership_cancel();
        $scope.awards_cancel();
        $scope.publication_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.speciality_cancel();
    }

    $scope.about_view_container = true;
    $scope.edit_about = function() {
        $scope.edit_info = true;
        $scope.about_view_container = false;
        $scope.aboutedit_status = true;
        $scope.aboutTemp= angular.copy(logininfo.About);
        $scope.dobTemp = angular.copy(logininfo.DOB);
        $scope.genderTemp = angular.copy(logininfo.Gender);
		getDoctorInfoData();
    }

    $scope.add_about = function() {
        $scope.edit_info = true;
        $scope.about_view_container = false;
    }

    //Top profile 
    $scope.exp_edit_view = false;
    $scope.exp_view_con = true;
    $scope.medical_edit_view = false;
    $scope.medical_view_con = true;
    $scope.profile_top_edit = function() {
        $scope.exp_edit_view = true;
        $scope.exp_view_con = false;
        $scope.medical_edit_view = true;
        $scope.medical_view_con = false;
        $scope.profileToeedit_status = true;
        $scope.expTemp = angular.copy(logininfo.Experience);
        $scope.medicalTemp = angular.copy(logininfo.MedicalRegNumber);
        $scope.education_cancel();
        $scope.services_cancel();
        $scope.achievments_cancel();
        $scope.membership_cancel();
        $scope.awards_cancel();
        $scope.publication_cancel();
        $scope.speciality_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }

    // end of top profile
    //Education    
    $scope.education_view_container = true;
    $scope.education_edit = function() {
        $scope.eudcation_edit_view = true;
        $scope.education_view_container = false;
        $scope.education_add_container = false;
        $scope.edutempStatus = true;
        $scope.educationTemp = angular.copy(logininfo.doctorEducation);
        $scope.edu_name = undefined;
        $scope.edu_year = undefined;
        $scope.edu_institute = undefined;
        $scope.services_cancel();
        $scope.achievments_cancel();
        $scope.membership_cancel();
        $scope.awards_cancel();
        $scope.publication_cancel();
        $scope.speciality_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();

    }

    $scope.add_education = function() {
        $scope.eudcation_edit_view = false;
        $scope.education_view_container = true;
        $scope.education_add_container = true;
        $scope.edu_name = "";
        $scope.start_year = "";
        $scope.end_year = "";
        $scope.awards_addset = $scope.addPublication = $scope.membership_addset = $scope.add_achievmentsset = $scope.services_addset = $scope.speciality_add = $scope.speciality_addCustomSpeciality = undefined;
        $scope.services_cancel();
        $scope.achievments_cancel();
        $scope.membership_cancel();
        $scope.awards_cancel();
        $scope.publication_cancel();
        $scope.speciality_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }
    //Services
    $scope.services_view_container = true;
    $scope.services_edit = function() {
        $scope.edit_view_services = true;
        $scope.services_view_container = false;
        $scope.servicesTempStatus = true;
        $scope.servicesTemp = angular.copy(logininfo.Services);
        $scope.services_addset = undefined;
        $scope.add_service_container = false;
        $scope.education_cancel();
        $scope.achievments_cancel();
        $scope.membership_cancel();
        $scope.awards_cancel();
        $scope.publication_cancel();
        $scope.speciality_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }

    $scope.add_services = function() {
        $scope.edit_view_services = false;
        $scope.services_view_container = true;
        $scope.add_service_container = true;
        $scope.services_addset = "";
        $scope.education_cancel();
        $scope.achievments_cancel();
        $scope.membership_cancel();
        $scope.awards_cancel();
        $scope.publication_cancel();
        $scope.speciality_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }

    //Achievments
    $scope.achievments_view_container = true;
    $scope.achievments_edit = function() {
        $scope.edit_view_achievments = true;
        $scope.achievments_view_container = false;
        $scope.add_achievements_container = false;
        $scope.AchievmentsTempStatus = true;
        $scope.add_achievmentsset = undefined;
        $scope.achievementsTemp = angular.copy(logininfo.DoctorAchievements);
        $scope.services_addset = undefined;
        $scope.education_cancel();
        $scope.services_cancel();
        $scope.membership_cancel();
        $scope.awards_cancel();
        $scope.publication_cancel();
        $scope.speciality_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }
    $scope.add_achievments = function() {
        $scope.edit_view_achievments = false;
        $scope.achievments_view_container = true;
        $scope.add_achievements_container = true;
        $scope.add_achievmentsset = "";
        $scope.services_addset = undefined;
        $scope.education_cancel();
        $scope.services_cancel();
        $scope.membership_cancel();
        $scope.awards_cancel();
        $scope.publication_cancel();
        $scope.speciality_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }

    //Publication 
    $scope.publication_view_container = true;
    $scope.publication_edit = function() {
        $scope.edit_view_publication = true;
        $scope.publication_view_container = false;
        $scope.publication_add_container = false;
        $scope.publicationTempStatus = true;
        $scope.publicationTemp = angular.copy(logininfo.DoctorPublishing);
        $scope.addPublication = undefined;
        $scope.education_cancel();
        $scope.services_cancel();
        $scope.achievments_cancel();
        $scope.membership_cancel();
        $scope.awards_cancel();
        $scope.speciality_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }

    $scope.add_publication = function() {
        $scope.edit_view_publication = false;
        $scope.publication_view_container = true;
        $scope.publication_add_container = true;
        $scope.addPublication = "";
        $scope.education_cancel();
        $scope.services_cancel();
        $scope.achievments_cancel();
        $scope.membership_cancel();
        $scope.awards_cancel();
        $scope.speciality_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }

    //Membership 
    $scope.membership_view_container = true;
    $scope.membership_edit = function() {
        $scope.edit_view_membership = true;
        $scope.membership_view_container = false;
        $scope.add_view_membership = false;
        $scope.membership_addset = undefined;
        $scope.membershiptEditStatus = true;
        $scope.membershipTempData = angular.copy(logininfo.Memberships);
        $scope.education_cancel();
        $scope.services_cancel();
        $scope.achievments_cancel();
        $scope.awards_cancel();
        $scope.publication_cancel();
        $scope.speciality_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }

    $scope.membership_add = function() {
        $scope.add_view_membership = true;
        $scope.edit_view_membership = false;
        $scope.membership_view_container = true;
        $scope.membership_addset = "";
        $scope.education_cancel();
        $scope.services_cancel();
        $scope.achievments_cancel();
        $scope.awards_cancel();
        $scope.publication_cancel();
        $scope.speciality_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }

    //Awards
    $scope.edit_view_awards = false;
    $scope.awards_view_container = true;
    $scope.awards_edit = function() {
        $scope.edit_view_awards = true;
        $scope.awards_view_container = false;
        $scope.awards_add_container = false;
        $scope.awards_addset = undefined;
        $scope.awardsEditStatus = true;
        $scope.awardsTempData = angular.copy(logininfo.Awards);
        $scope.education_cancel();
        $scope.services_cancel();
        $scope.achievments_cancel();
        $scope.membership_cancel();
        $scope.publication_cancel();
        $scope.speciality_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }


    $scope.awards_add = function() {
        $scope.edit_view_awards = false;
        $scope.awards_view_container = true;
        $scope.awards_add_container = true;
        $scope.awards_addset = "";
        $scope.education_cancel();
        $scope.services_cancel();
        $scope.achievments_cancel();
        $scope.membership_cancel();
        $scope.publication_cancel();
        $scope.speciality_cancel();
        $scope.profile_top_cancel();
        $scope.about_cancel();
        $scope.contact_cancel();
    }


    //professional controller
    $scope.editmode = true;
    $scope.cntrychange = function (val) {
        if (val.length >= 1) {
            $scope.cntryError = false;
        }
    }

    $scope.Update = function (loginform, logininfo,pageno) {
        $scope.cntryError = false;
        if(!loginform.$valid) {
            return;
        }

        if(logininfo.MobileNumber == undefined || logininfo.MobileNumber.length < 10) {
            $scope.phone_valid = true;
            return;
        }
        else {
            $scope.phone_valid = false;
        }
        if(!emailfilter.test(logininfo.Email)) {
            $scope.email_valid = true;
            return;
        }
        else {
            $scope.email_valid = false;
        }

        //experience validation
        if(logininfo.Experience == undefined || logininfo.Experience == null) {
            $scope.experience_valid = true;
            return;
        }
        else {
            $scope.experience_valid = false;
        }

        if (logininfo.MobileNumber.toString().length >= 7 && logininfo.PhCntryCD.length < 1) {
            $scope.cntryError = true;
            return;
        }
        //end of dob
        $scope.cntryCode = logininfo.PhCntryCD;
        $scope.phone_valid = false;
        $scope.email_valid = false;
        if($scope.edu_name !== undefined) {
            if($scope.edu_name == "") {
                alert("Degree cannot be empty");
                return;
            }
            logininfo.doctorEducation.push({
                'EducationName':$scope.edu_name,
                'InstituteName':($scope.edu_institute == undefined)?'':$scope.edu_institute,
                'edu_startyear':($scope.start_year == undefined)?'':$scope.start_year,
                'edu_endyear':($scope.end_year == undefined)?'':$scope.end_year
            });
        }
        $scope.education_name_valid = false;
        if($scope.speciality_add !== undefined || $scope.speciality_add == "SELECTCODE") {
            if($scope.speciality_add == "" || $scope.speciality_add == "SELECTCODE") {
                alert("Speciality cannot be empty");
                return;
            }
            logininfo.specialization.push({
                "SepcialityCode": $scope.speciality_add,
                "CustomSpecialityDesc": $scope.speciality_addCustomSpeciality == undefined?'':$scope.speciality_addCustomSpeciality
            });
        }

        if($scope.services_addset !== undefined) {
            if($scope.services_addset == "") {
                alert("Services cannot be empty");
                return;
            }
            logininfo.Services.push({
                'name':$scope.services_addset
            });
        }

        if($scope.add_achievmentsset !== undefined) {
            if($scope.add_achievmentsset == "") {
                alert("Achievement cannot be empty");
                return;
            }
            var length;
            var row_id;
            if(logininfo.DoctorAchievements.length > 0) {
                length = logininfo.DoctorAchievements.length;
                row_id = logininfo.DoctorAchievements[length-1].RowID;
                row_id = row_id+1;
            }
            else {
                row_id =0;
            }
            logininfo.DoctorAchievements.push({
                'RowID':row_id,
                'Name':$scope.add_achievmentsset,
                'Year':'',
                'Indicator': "A"
            });
        }

        if($scope.membership_addset !== undefined) {
            if($scope.membership_addset == "") {
                alert("Membership cannot be empty");
                return;
            }
            logininfo.Memberships.push({
                'name':$scope.membership_addset
            });
        }

        if($scope.addPublication !== undefined) {
            if($scope.addPublication == "") {
                alert("Publication cannot be empty");
                return;
            }
            var pub_length;
            var pub_row_id;
            ////console.log(JSON.stringify(logininfo.DoctorPublishing));
            if(logininfo.DoctorPublishing.length > 0) {
                pub_length = logininfo.DoctorPublishing.length;
                pub_row_id = logininfo.DoctorPublishing[pub_length-1].RowID;
                pub_row_id = pub_row_id+1;
            }
            else {
                pub_row_id =0;
            }
            logininfo.DoctorPublishing.push({
                'RowID':pub_row_id,
                'Name':$scope.addPublication,
                'Year':'',
                'Indicator': "P"
            });
        }

        if($scope.awards_addset !== undefined) {
            if($scope.awards_addset == "") {
                alert("Awards cannot be empty");
                return;
            }
            logininfo.Awards.push({
                'name':$scope.awards_addset
            });
        }
        if(logininfo.DOB !== null) {
            var momentObj = moment(logininfo.DOB);
            var momment_currentdate = momentObj.format("YYYY-MM-DD");
            logininfo.DOB = momment_currentdate;
        }

        $scope.logininfo.doctorid = doctorinfo.doctorid;
        hcueDoctorSignUp.addProfileInfo($scope.logininfo);
        $scope.showloading = true;
        hcueDoctorProfileService.SaveProfile($scope.logininfo, pageno, OnProfileInfoSaved, OnError);
        
    }


    function OnProfileInfoSaved(data) {
        if (data == "Invalid EmailId") {
            alert('Invalid Email id');
        }
        else {
            doctorinfo.doctordata = data;
            hcueDoctorLoginService.setDoctorInfo(doctorinfo);
            $scope.showloading = false;
            $scope.about_view_container = true;
            $scope.edit_info = false;
            //$scope.AchievmentsTempStatus = $scope.edutempStatus = $scope.servicesTempStatus = $scope.publicationTempStatus = $scope.specialityTempstatus = false;
            $scope.AchievmentsTempStatus = $scope.membershiptEditStatus = $scope.awardsEditStatus = $scope.edutempStatus = $scope.servicesTempStatus = $scope.publicationTempStatus = $scope.specialityTempstatus = $scope.edit_contactTemp = $scope.aboutedit_status = $scope.profileToeedit_status = false;
            $scope.education_cancel();
            $scope.services_cancel();
            $scope.achievments_cancel();
            $scope.membership_cancel();
            $scope.awards_cancel();
            $scope.publication_cancel();
            $scope.speciality_cancel();
            $scope.profile_top_cancel();
            $scope.about_cancel();
            $scope.contact_cancel();
            $location.path('/profile');
            $scope.awards_addset = $scope.addPublication = $scope.membership_addset = $scope.add_achievmentsset = $scope.services_addset = $scope.speciality_add = $scope.edu_name = $scope.speciality_addCustomSpeciality = undefined;
            //getDoctorInfoData();
            alertify.success('Profile Information updated successfully')
			
        }
    }

    function OnError(data ) {
        $scope.showloading = false;
    }




    //Add and remove functions 



    // Speciality details
    $scope.AddSpeciality = function () {
        var newitem = {DoctorSpecialityID: ''};
        $scope.logininfo.specialization.push(newitem);
        $scope.speciality_edit_view = true;
        $scope.speciality_view_container = false;
    }
    $scope.RemoveSpeciality = function (item) {
        var index = $scope.logininfo.specialization.indexOf(item);
        $scope.logininfo.specialization.splice(index, 1);
    }
    // End of Speciality details


    $scope.AddEducation = function () {
        var newitem = { EducationName: '',InstituteName: '',FullName: '',Active: 'Y' };
        $scope.logininfo.doctorEducation.push(newitem);
        $scope.add_education();
    }
    $scope.RemoveEducation = function (item) {
        var index = $scope.logininfo.doctorEducation.indexOf(item);
        $scope.logininfo.doctorEducation.splice(index, 1);
    }
    $scope.AddService = function () {
        var newitem = { name: '' };
        $scope.logininfo.Services.push(newitem);
        $scope.edit_view_services = true;
        $scope.services_view_container = false;
    }

    $scope.RemoveService = function (item) {
        var index = $scope.logininfo.Services.indexOf(item);
        $scope.logininfo.Services.splice(index, 1);
    }
    $scope.AddMembership = function () {
        var newitem = { name: '' };
        $scope.logininfo.Memberships.push(newitem);
        $scope.edit_view_membership = true;
        $scope.membership_view_container = false;
    }

    $scope.RemoveMembership = function (item) {
        var index = $scope.logininfo.Memberships.indexOf(item);
        $scope.logininfo.Memberships.splice(index, 1);
    }
    $scope.AddAward = function () {
        var newitem = { name: '' };
        $scope.logininfo.Awards.push(newitem);
        $scope.edit_view_awards = true;
        $scope.awards_view_container = false;
    }
    $scope.RemoveAward = function (item) {
        var index = $scope.logininfo.Awards.indexOf(item);
        $scope.logininfo.Awards.splice(index, 1);
    }
    //Publications
     $scope.AddPublications = function () {
        var newitem = {"publishedBy": '',"publishedBooks": '',"Year": '',"Name": '',"Active":'Y' };
        $scope.logininfo.DoctorPublishing.push(newitem);
        $scope.edit_view_publication = true;
        $scope.publication_view_container = false;
    }
    $scope.RemovePublications = function (item) {
        var index = $scope.logininfo.DoctorPublishing.indexOf(item);
        $scope.logininfo.DoctorPublishing.splice(index, 1);
    }
    
    //Achieviments
    $scope.AddAchievements = function () {
        var newitem = {Name:'',Year: '',"Active":'Y'};
        $scope.logininfo.DoctorAchievements.push(newitem);
        $scope.edit_view_achievments = true;
        $scope.achievments_view_container = false;
    }
    $scope.RemoveAchievements = function (item) {
        var index = $scope.logininfo.DoctorAchievements.indexOf(item);
        $scope.logininfo.DoctorAchievements.splice(index, 1);
    }




    //other functions
    $scope.GetImageFromString = function (imagestr) {
        return GetImage(imagestr)
    }
    $scope.GetFee = function (addressid) {
        var fee = "";
        angular.forEach($scope.doctorinfo.doctorConsultation, function (item) {
            if (item.AddressID == addressid) {
                fee = item.Fees;
                return fee;
            }
        });
        return fee;
    }
    $scope.GetQualification = function (qualification) {
        return GetQualification(qualification);
    }
    $scope.GetMemberID = function (MemberID) {
        return GetMemberID(MemberID);
    }
    $scope.GetSpeciality = function (code) {
        return GetSpeciality(doctorinfo.specialitylist, code);
    }
    $scope.GetSpecialityById = function (splCode) {
        var spl = "";
        angular.forEach(doctorinfo.specialitylist, function (item) {
            if (item.DoctorSpecialityID == splCode) {
                spl = item.DoctorSpecialityDesc;
                return spl;
            }
        });
        return spl;
    }
    $scope.GetValues = function (val) {
        return GetDictionaryValues(val);
    }

    $scope.EditPersonalInfo = function () {

        $location.path('/personalinfo');
    }
    $scope.EditAbout = function () {

        $location.path('/professionalinfo');
    }
    $scope.EditPractise = function () {

        $location.path('/practiseinfo');
    }
    $scope.EditConsultTime = function(addressid) {
       
         var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
         var url = "/editconsultinfo/" + addressid.toString() + "?kwid=" + (guid || "");
         $location.url(url);
     }
    //Refered as two seperate function in different controoler, search by same method name
    function GetUrl(key) {
        var url = "";
        var val = datapersistanceFactory.getItemString(key);
        if (val == null || val.length == 0)
            url = "";
        else {
            url = val.replace('"', '').replace('"', '');
        }
        return url;
    }

     $scope.openLightboxModal = function (index) {
        Lightbox.openModal($scope.ImageDocumentArray, index);
     };


    $scope.getkeyvallength = function(val) {
        if(val !== undefined) {
            return Object.keys(val).length;
        }
        
    }
     if($location.search().screen!=undefined && $location.search().screen=="Practice"){
			$scope.tabno=2;
     }


    //vp added new profile clinic img
    
     $scope.profileupload = function () {
         $('#myModal').modal('show');
         $scope.imgTabno = 1;
         $scope.photoTaken = false;
         $scope.myImage = '';
         $scope.browseOpenSet = true;
         $scope.openset = false;
     }
     $scope.clearImg = function () {
         $scope.myImage = '';
         $scope.browseOpenSet = true;
     }
     var _video = null,
         patData = null;

     $scope.patOpts = { x: 0, y: 0, w: 25, h: 25 };

     $scope.channel = {};

     $scope.webcamError = false;
     $scope.onError = function (err) {
         $scope.$apply(
             function () {
                 $scope.webcamError = err;
             }
         );
     };

     $scope.onSuccess = function () {

         _video = $scope.channel.video;
         $scope.$apply(function () {
             $scope.patOpts.w = _video.width;
             $scope.patOpts.h = _video.height;

         });
     };


     $scope.onStream = function (stream) {

     };
     $scope.openset = false;
     $scope.makeSnapshot = function () {
         $scope.openset = true;
         $scope.photoTaken = true;

         $scope.selectingTxtBox(true);
         if (_video) {
             var patCanvas = document.querySelector('#snapshot');
             if (!patCanvas) return;

             patCanvas.width = _video.width;
             patCanvas.height = _video.height;
             var ctxPat = patCanvas.getContext('2d');

             var idata = getVideoData($scope.patOpts.x, $scope.patOpts.y, $scope.patOpts.w, $scope.patOpts.h);
             ctxPat.putImageData(idata, 0, 0);

             sendSnapshotToServer(patCanvas.toDataURL());

             patData = idata;
         }
     };

     $scope.closeSet = function () {
         $scope.snapshotData = '//:0';
         $scope.openset = false;
     }
     $scope.downloadSnapshot = function downloadSnapshot(dataURL) {
         window.location.href = dataURL;
     };

     var getVideoData = function getVideoData(x, y, w, h) {
         var hiddenCanvas = document.createElement('canvas');
         hiddenCanvas.width = _video.width;
         hiddenCanvas.height = _video.height;
         var ctx = hiddenCanvas.getContext('2d');
         ctx.drawImage(_video, 0, 0, _video.width, _video.height);
         return ctx.getImageData(x, y, w, h);
     };


     var sendSnapshotToServer = function sendSnapshotToServer(imgBase64) {
         $scope.snapshotData = imgBase64;
     };
     

    //Upload Profile Image
     $scope.onFileSelectConsent = function ($files) {
         $scope.imgTabno = 2;
         var type = $files[0].type;
         Upload.base64DataUrl($files).then(function (urls) {
             $scope.snapshotData = urls.toString();
         });
     }
     $scope.myImage = '';
     $scope.myCroppedImage = '';

     $scope.handleFileSelect = function ($files) {
         
         var file = $files[0];
         var reader = new FileReader();
         reader.onload = function ($files) {
             $scope.$apply(function ($scope) {
                 $scope.myImage = $files.target.result;
                 $scope.browseOpenSet = false;
             });
         };
         reader.readAsDataURL(file);
     };

     $scope.upload = function () {
         $scope.imgTabno = 2;
         $scope.selectingTxtBox(false);


     }
     
   

     $scope.selectingTxtBox = function (val) {
         $scope.blnCamera = val;

     }

     $scope.snap = function (val) {

         $scope.base64Data = val.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
         $scope.profileImages = $scope.base64Data;
         hcueDoctorProfileService.UpdateProfileImage(hcueDoctorLoginService.getLoginId(), $scope.profileImages, onUpdateProfileImageSuccess, onUpdateProfileImageError);
     }

     function onUpdateProfileImageSuccess(data) {
         $scope.docProfileImgList();
     }
     function onUpdateProfileImageError(data) {
        
     }

    //delete profile Image
     $scope.deleteProfile = function () {
         alertify.confirm("<b class='txt-centre'>Confirm !</b><h4 class='txt-body-centre'>Are you sure want to delete</h4> ", function () {
             hcueDoctorProfileService.deleteProfileImage(hcueDoctorLoginService.getLoginId(), onDeleteProfileImageSuccess, onDeleteProfileImageError);
         },
           function () {
               $scope.showSettingList = false;
           });
     }
     function onDeleteProfileImageSuccess(data) {
         $scope.showSettingList = false;
         doctorinfo.doctordata.doctor[0].ProfileImage = undefined;
         doctorinfo.doctordata.profileimage = undefined;
         hcueDoctorLoginService.setDoctorInfo(doctorinfo);
         datapersistanceFactory.set('doctorProfileImage', doctorinfo.doctordata.doctor[0].ProfileImage);
         $scope.doctorinfo.profileimage = datapersistanceFactory.getItem('doctorProfileImage');
         $rootScope.$broadcast('doctorProfileImageChange');
         profImg()
     }
     function onDeleteProfileImageError(data) {

     }


     $scope.closeProfile = function () {
         $scope.imgTabno = '';
     }


    //listing doc profile
     $scope.docProfileImgList = function() {
         var params = { "DoctorID": hcueDoctorLoginService.getLoginId(), "USRType": "ADMIN", "USRId": hcueDoctorLoginService.getLoginId(),"profileImage": true };
         hcueDoctorProfileService.getDocProfImgList(params, onDoctorDetailsSuccess, onDoctorDetailsError);
     }
     function onDoctorDetailsSuccess(data) {
         doctorinfo.doctordata.doctor[0].ProfileImage = data.doctor[0].ProfileImage;
         doctorinfo.doctordata.profileimage = data.doctor[0].ProfileImage;
         hcueDoctorLoginService.setDoctorInfo(doctorinfo);
         datapersistanceFactory.set('doctorProfileImage', doctorinfo.doctordata.doctor[0].ProfileImage);
         $scope.doctorinfo.profileimage = GetImage(datapersistanceFactory.getItem('doctorProfileImage'));
         $rootScope.$broadcast('doctorProfileImageChange');
         profImg()
     }
     function onDoctorDetailsError(data) {

     }


     $scope.settingList = function () {
         if ($scope.showSettingList) {
             $scope.showSettingList = false;
         }
         else {
             $scope.showSettingList = true;
         }
     }

     // $scope.clinicDoctorpopup = function(){
         // $scope.clinicDoctorImagepopup = true;
     // }

     // $scope.closeDoctorclinicImagePopup = function () {
         // $scope.clinicDoctorImagepopup = false;
     // }
	 
	 
	      $scope.showimage = false;
     $scope.clinicDoctorpopup = function(profileimage) {
         
		 var newImage = "data:image/png;base64" +','+profileimage
         $scope.showimage = true;
         var images = [];
         // angular.forEach($scope.ImageDocumentArray, function (value) {           
             images.push(newImage);
         // });

         
         var curImageIdx = 1,
             total = images.length;
         var wrapper = $('#image-gallery'),
             curSpan = wrapper.find('.current');
         var viewer = ImageViewer(wrapper.find('.image-container'));

         //display total count
         wrapper.find('.total').html(total);

         function showImage() {
             var imgObj = images[curImageIdx - 1];
             viewer.load(imgObj, imgObj);
             curSpan.html(curImageIdx);
         }

         wrapper.find('.next').click(function () {
             curImageIdx++;
             if (curImageIdx > total)
                 curImageIdx = 1;
             showImage();
         });

         wrapper.find('.prev').click(function () {
             curImageIdx--;
             if (curImageIdx < 1)
                 curImageIdx = total;
             showImage();
         });

         //initially show image
         setTimeout(function () {
             showImage();
         }, 2);
         
     };

     $scope.closeimage = function () {
         $scope.showimage = false;
     }
	 

}]);
