﻿hCueDoctorWebApp.lazyController('hCuevitals', ['$rootScope', '$scope', '$location', 'hcueNotesDrawingService', 'hcuePatientCaseService', 'hcueConsultationInfo', 'hcueDoctorLoginService', 'datacookieFactory', function($rootScope, $scope, $location, hcueNotesDrawingService, hcuePatientCaseService, hcueConsultationInfo, hcueDoctorLoginService, datacookieFactory) {

     var Temp = '0';
     var bp = '0';
     var sugar = '0';
     var bloodgroup = 'A+';
     var height = '0';
     var weight = '0';
     var bplow, bphigh, sugarbp, sugarfast;
     var doctorid = hcueDoctorLoginService.getLoginId();

     $scope.SearchByText = function() {
         Temp = $scope.temperature;
         var bp = $scope.bp;
         //window.alert(bp.length);
         if (bp.length == 3) {
             $scope.bp = bp + "/";

         }

         var sugar = $scope.sugar;
         if (sugar.length == 3) {
             $scope.sugar = sugar + "/";

         }
         bloodgroup = $scope.bloodgroup;
         weight = $scope.weight;

         height = $scope.height;
         bplow = bp.substring(0, 3);
         bphigh = bp.substring(4, 8);

         sugarbp = sugar.substring(0, 3);
         sugarfast = sugar.substring(4, 8);

     }

     $scope.Update = function() {
         $scope.showloading = true;
         hcuePatientCaseService.savevitals(bplow, bphigh, sugarbp, sugarfast, Temp, height, weight, bloodgroup, doctorid, OnCaseSaveSuccess, OnCaseSaveError);
     }

     function OnCaseSaveSuccess() {
         alertify.success('success');
         // var modal = new DayPilot.Modal({
         // onClosed: function(args) {
         // if (args.result) {  // args.result is empty when modal is closed without submitting
         // loadEvents();
         // }
         // }
         // });
         // DayPilot.Modal.close();
     }

     function OnCaseSaveError() {
         alertify.error('error');
     }


 }])