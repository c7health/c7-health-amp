﻿hCueDoctorWebApp.lazyController('hCueforgotPasswordController', ['$scope', 'alertify', '$routeParams', 'hCueforgotpasswordServices', '$location', function ($scope, alertify, $routeParams, hCueforgotpasswordServices, $location) {
  //Email Patter
  $scope.email_pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  $scope.password_pattern = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\S+$).{8,}$/;

  $scope.email = '';
  $scope.password = '';
  $scope.confirmpassword = '';
  $scope.success = false;

  $scope.updatepassword = function () {
    var data = {
      doctorid: JSON.parse($routeParams.id),
      doctorloginid: $scope.email,
      password: $scope.password,
      newpassword: $scope.confirmpassword,
      active: true
    }
    console.log(data);
    hCueforgotpasswordServices.updatePassword(data, updatesuccess, updateerror);
    function updatesuccess(data) {
      alertify.success('Password updated successfully');
      $scope.success = true;
    }
    function updateerror(data) {
      alertify.error('Password update error');
    }
  }



  $scope.gotologin = function () {
    $location.path("/login");
  }








  // $scope.step2 = false;
  // $scope.step3 = false;
  // $scope.step4 = false;
  // $scope.reset_optionmodel = 'M';
  // $scope.resetOptionValue = "M";
  // $scope.random_number = "";
  // //validateEmail
  // $scope.validateEmail = function (forgotpasswordform) {
  //     if (!forgotpasswordform.$valid) {
  //         return;
  //     }
  //     //Params
  //     var params = {
  //         'DoctorLoginID': $scope.emailid
  //     };
  //     hCueforgotpasswordServices.validateEmail(params, onValidateSuccess, onValidateError);
  //     function onValidateSuccess(data) {
  //         if (data.Message == "Success") {
  //             $scope.userSuccessData = data;
  //             //Email
  //             var email_show = data.forgotPasswordResponse.EmailID.split('@');
  //             $scope.first_email_character = email_show[0];
  //             $scope.last_email_character = email_show[1];
  //             //Phone
  //             var phone_show = data.forgotPasswordResponse.MobileNumber.toString();
  //             $scope.last_phone_digits = phone_show.substring(8, 10)
  //             $scope.step1 = false;
  //             $scope.step2 = true;
  //         }
  //         else {
  //             alertify.success("Invalid login");
  //         }
  //     }

  //     function onValidateError(data) {

  //     }
  // }
  // $scope.getResetval = function (val) {
  //     $scope.resetOptionValue = val;
  // }
  // var randomFixedInteger = function (length) {
  //     return Math.floor(Math.pow(10, length - 1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length - 1) - 1));
  // }

  // $scope.random_number = randomFixedInteger(4);
  // //Send OTP
  // $scope.send_otp = function (resetOptionForm) {

  //     if ($scope.resetOptionValue == "M") {
  //         $scope.showblock = true;
  //         var sendOTPparams = {
  //             "to": "91" + $scope.userSuccessData.forgotPasswordResponse.MobileNumber,
  //             "text": "Your OTP is " + $scope.random_number
  //         };
  //         hCueforgotpasswordServices.sendOTPSMS(sendOTPparams, onSMSSuccess, onSMSError);

  //         function onSMSSuccess(data) {
  //             var otp_params = {
  //                 "USRType": "DOCTOR",
  //                 "TermsAccepted": "N",
  //                 "PinPassCode": $scope.random_number.toString(),
  //                 "DoctorLoginID": $scope.userSuccessData.forgotPasswordResponse.DoctorLoginID,
  //                 "USRId": $scope.userSuccessData.forgotPasswordResponse.DoctorID
  //             }
  //             hCueforgotpasswordServices.updatePinPass(otp_params, onUpdateSuccess, onUpdateError);
  //         }

  //         function onSMSError(data) {

  //         }

  //         function onUpdateSuccess(data) {
  //             if (data == "Success") {
  //                 $scope.step2 = false;
  //                 $scope.step3 = true;
  //             } else {

  //             }
  //         }

  //         function onUpdateError(data) {

  //         }
  //     }
  //     else {
  //         $scope.showblock = false;
  //         var sendEmailOTP = {
  //             "USRType": "DOCTOR",
  //             "USRId": $scope.userSuccessData.forgotPasswordResponse.DoctorID,
  //             "ToAddress": $scope.userSuccessData.forgotPasswordResponse.EmailID,
  //             "Subject": "One Time Password",
  //             "MailContent": $scope.random_number.toString(),
  //             "HospitalCD":"SAULOYNYBZ"
  //         }
  //         if ($scope.urlpath == false) {
  //             delete sendEmailOTP.HospitalCD;
  //         }
  //         hCueforgotpasswordServices.sendOTPEmail(sendEmailOTP, onEmailOTPSuccess, onEmailOTPError);

  //         function onEmailOTPSuccess(data) {
  //             if (data == "Success") {
  //                 var emailotp_params = {
  //                     "USRType": "DOCTOR",
  //                     "TermsAccepted": "N",
  //                     "PinPassCode": $scope.random_number.toString(),
  //                     "DoctorLoginID": $scope.userSuccessData.forgotPasswordResponse.DoctorLoginID,
  //                     "USRId": $scope.userSuccessData.forgotPasswordResponse.DoctorID
  //                 }
  //                 hCueforgotpasswordServices.updatePinPass(emailotp_params, onEmailUpdateSuccess, onEmailUpdateError);
  //             }
  //         }

  //         function onEmailOTPError(data) {

  //         }
  //     }

  //     function onEmailUpdateSuccess(data) {
  //         if (data == "Success") {
  //             $scope.step2 = false;
  //             $scope.step3 = true;
  //         }
  //         else {

  //         }

  //     }

  //     function onEmailUpdateError() {

  //     }
  // }

  // $scope.otpSubmit = function (otpForm) {
  //     if (!otpForm.$valid) {
  //         return;
  //     }

  //     var opt_sendParams = {
  //         'PinPassCode': $scope.otp_val.toString(),
  //         'DoctorLoginID': $scope.userSuccessData.forgotPasswordResponse.DoctorLoginID
  //     }
  //     hCueforgotpasswordServices.validatePinPass(opt_sendParams, onValidateSuccess, onValidateError);

  //     function onValidateSuccess(data) {
  //         if (data == "Success") {
  //             $scope.step3 = false;
  //             $scope.step4 = true;
  //         } else {
  //             alertify.error("Invalid OTP");
  //         }
  //     }

  //     function onValidateError(data) {

  //     }
  // }

  // $scope.changePassword = function (changePasswordForm) {
  //     if (changePasswordForm.$invalid) {
  //         return;
  //     }

  //     var changepassword_params = {
  //         "DoctorCurrentPassword": $scope.random_number.toString(),
  //         "PinPassCode": $scope.random_number.toString(),
  //         "USRType": "DOCTOR",
  //         "DoctorNewPassword": $scope.password,
  //         "DoctorLoginID": $scope.userSuccessData.forgotPasswordResponse.DoctorLoginID,
  //         "USRId": $scope.userSuccessData.forgotPasswordResponse.DoctorID
  //     }

  //     hCueforgotpasswordServices.updatePassword(changepassword_params, onChangeSuccess, onChangeError);

  //     function onChangeSuccess(data) {
  //         if (data == "Password_Updated") {
  //             $scope.step4 = false;
  //             $scope.step5 = true;
  //         } else {
  //             alertify.error("Invalid OTP");
  //         }
  //     }

  //     function onChangeError(data) {

  //     }
  // }

}
]);
