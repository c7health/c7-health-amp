'use strict';

/* Filters */

angular.module('hCueDoctorWebFilters', []).filter('checkmark', function() {
  return function(input) {
    return input ? '\u2713' : '\u2718';
  };
});
