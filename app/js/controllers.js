﻿ //'use strict';

 /* Controllers */

 var hCueDoctorWebControllers = angular.module('hCueDoctorWebControllers', ['angucomplete-alt', 
 'mwl.calendar', 'ui.bootstrap', 'ui.bootstrap.datepicker', 'ngTouch', 'ngAnimate', 'ngAutocomplete', 'oc.lazyLoad']);






 function getKWID() {
     return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
 }
function getGUID() {
    return  (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
}

 hCueDoctorWebControllers.controller('hCueDoctorProfile', ['$scope', '$location', '$window', 'datapersistanceFactory', 
 'hcueDoctorLoginService', 'hcueAuthenticationService', 'hcueLoginStatusService', 'datacookieFactory', 'Lightbox', 
 function($scope, $location, $window, datapersistanceFactory, hcueDoctorLoginService, hcueAuthenticationService,
  hcueLoginStatusService, datacookieFactory, Lightbox) {
     /* Login details */
     var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
     userLoginDetails = datacookieFactory.getItem('loginData');
     if (userLoginDetails.loggedinStatus === "true") {
         $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
         $scope.LoadDoctorName();
     } else {
         $location.path('/login');
     }




     $scope.openLightboxModal = function(index) {
         Lightbox.openModal($scope.images, index);
     };


     /* End of Gallery code */
     /* End of login details */
     var doctorinfo = hcueDoctorLoginService.getDoctorInfo();


     $scope.tabno = 1;
     $scope.doctorinfo = doctorinfo.doctordata;
     $scope.referraldoctors = doctorinfo.referraldoctors;
     //window.alert(doctorinfo.doctordata.doctor[0].ProfileImage);
     $scope.doctorinfo.profileimage = GetImage(doctorinfo.doctordata.doctor[0].ProfileImage);

     $scope.facebook = GetUrl('facebook' + doctorinfo.doctorid);
     $scope.linkedin = GetUrl('linkedin' + doctorinfo.doctorid);
     $scope.twitter = GetUrl('twitter' + doctorinfo.doctorid);
     $scope.gplus = GetUrl('googleplus' + doctorinfo.doctorid);

     $scope.GetImageFromString = function(imagestr) {
         return GetImage(imagestr)
     }
     $scope.GetFee = function(addressid) {
         var fee = "";
         angular.forEach($scope.doctorinfo.doctorConsultation, function(item) {
             //display the key and value pair
             if (item.AddressID == addressid) {
                 fee = item.Fees;
                 return fee;
             }

         });
         return fee;
     }


     $scope.GetQualification = function(qualification) {

         return GetQualification(qualification);
     }
     $scope.GetMemberID = function(MemberID) {

         return GetMemberID(MemberID);
     }
     $scope.GetSpeciality = function(code) {
         return GetSpeciality(doctorinfo.specialitylist, code);
     }

     $scope.GetSpecialityById = function(splCode) {
         var spl = "";

         angular.forEach(doctorinfo.specialitylist, function(item) {
             //display the key and value pair
             if (item.DoctorSpecialityID == splCode) {
                 spl = item.DoctorSpecialityDesc;
                 return spl;
             }

         });
         return spl;
     }

     $scope.GetValues = function(val) {
         return GetDictionaryValues(val);
     }

     $scope.MovetoUpload = function(imagefor, id) {
         var fileid = imagefor + "-" + doctorinfo.doctorid; //"doctorprofile-" + hcueDoctorLoginService.getLoginId();

         $location.path('/profileupload', { fileid: fileid });
     }
     $scope.MoveToUrl = function(key) {
         var value = GetUrl(key + doctorinfo.doctorid);

         if (value.length == 0)
             return;
         $window.open('//' + value);
         // $window.location.href = value;
     }
     $scope.EditPersonalInfo = function() {

         var guid = getGUID();
         var url = "/personalinfo?kwid=" + (guid || "");
         $location.url(url);
         // $location.path('/personalinfo');
     }
     $scope.EditAbout = function() {
         var guid = getGUID();
         var url = "/professionalinfo?kwid=" + (guid || "");
         $location.url(url);
         //$location.path('/professionalinfo');
     }
     $scope.EditPractise = function() {
         var guid = getGUID();
         var url = "/practiseinfo?kwid=" + (guid || "");
         $location.url(url);
         //$location.path('/practiseinfo');
     }
     $scope.EditConsultTime = function(addressid) {
         //$location.path('/editconsultinfo/', { addressid: addressid });
         //window.alert('/editconsultinfo/' + addressid.toString());
         var guid = getGUID();
         var url = "/editconsultinfo/" + addressid.toString() + "?kwid=" + (guid || "");
         $location.url(url);
         //$location.path('/editconsultinfo/' + addressid.toString());
     }
     $scope.EditSocial = function() {
             var guid = getGUID();
             var url = "/profileblog?kwid=" + (guid || "");
             $location.url(url);

             //$location.path('/profileblog');
         }
         //Refered as two seperate function in different controoler, search by same method name
     function GetUrl(key) {
         var url = "";

         var val = datapersistanceFactory.getItemString(key);
         if (val == null || val.length == 0)
             url = "";
         else {
             url = val.replace('"', '').replace('"', '');
         }

         return url;

     }

 }])

 hCueDoctorWebControllers.controller('tryderectiveController', ['$scope', '$filter', '$location', 
 function ($scope, $filter, $location ) {
$scope.conditions = {
            "ConditionID": 110,
			"ConditionDesc": "-0.2",
			"ConditionNotes": "Special Notes"
		}

$scope.chkconditions =[ {

            "ConditionID": 110,
			"ConditionDesc": "",
            "ConditionDesc1": "D Shape Lens"
}]

$scope.bulred = function(item){
    console.log('blured');
    console.log(JSON.stringify(item));
}

$scope.chkboxChange = function(item){
  console.log('checked');
    console.log(JSON.stringify(item));

}

}])

 hCueDoctorWebControllers.controller('hCueDoctorImageUpload', ['$scope', '$location', 
 '$locationParams', '$cordovaCamera', '$cordovaFile', 'hcueDoctorLoginService', 'hcueDoctorProfileService',
 function($scope, $location, $locationParams, $cordovaCamera, $cordovaFile, hcueDoctorLoginService, hcueDoctorProfileService) {

         var filename = $locationParams.fileid;
         var id = 0;
         var imagetype = "";
         GetInfo(filename);

         function GetInfo(filename) {
             var imagedtl = filename.split("-");
             imagetype = imagedtl[0];
             id = imagedtl[1];
         }

         function GetCameraOption(method) {
             var options = {
                 destinationType: Camera.DestinationType.DATA_URL, //Camera.DestinationType.FILE_URI  //---Return image , Dataurl return base64
                 sourceType: Camera.PictureSourceType.CAMERA,
                 //allowEdit: false,
                 encodingType: Camera.EncodingType.JPEG,
                 popoverOptions: CameraPopoverOptions,
                 targetWidth: 200,
                 targetHeight: 200,
                 quality: 100,
                 correctOrientation: true
             };
             options.sourceType = (method == 'choose') ? Camera.PictureSourceType.PHOTOLIBRARY : Camera.PictureSourceType.CAMERA;

             return options;
         }
         $scope.AddImage = function(method) {

             var options = GetCameraOption(method); // Decides source image type.
             // 3

             $cordovaCamera.getPicture(options).then(function(imageData) {
                 $scope.lastselectedimage = "";
                 // 4
                 onImageSuccess(imageData);

                 function onImageSuccess(fileURI) {
                     $scope.lastselectedimage = fileURI; //used for upload
                 }


             }, function(err) {
                 //  ////console.log(err);
             });
         }
         $scope.UploadImage = function() {
             var key = filename;
             var data = $scope.lastselectedimage;
             if (data.length > 0) {
                 $scope.imagedata = $scope.lastselectedimage;
             } else {
                 // OnSuccess($scope.lastselectedimage);
                 var guid = getGUID();
                 var url = "/profile?kwid=" + (guid || "");
                 $location.url(url);
                 // $location.path('/profile');
             }

             hcueDoctorProfileService.UpdateProfileImage(id, data, OnSuccess, OnError);

         }

         function OnSuccess(data) {
             var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
             $scope.doctorinfo = doctorinfo;
             doctorinfo.doctordata.doctor[0].ProfileImage = "Some(" + data + ")";
             doctorinfo.profileimage = data;
             hcueDoctorLoginService.setDoctorInfo(doctorinfo);
             $scope.doctorinfo = doctorinfo;
             var guid = getGUID();
             var url = "/profile?kwid=" + (guid || "");
             $location.url(url);
             // $location.path('/profile');
         }

         function OnError(data) {}
     }])
     //Arun code 
 
 //end of arun code




 hCueDoctorWebControllers.controller('InvoicePopup', function ($scope, $uibModalInstance, Upload, hcuePatientCaseService, hcueConsultationInfo, consultinfo, $timeout) {
     $scope.CloseInvoicepopUp = function () {
         $uibModalInstance.close();
     };
     $scope.Invoicefiles = [];
     $scope.filename = "";
     $scope.onFileSelectInvoice = function ($files) {
         Upload.base64DataUrl($files)
             .then(function (urls) {
                 url = urls;
                 var params = {
                     "UpLoadType": "LabInvoice",
                     "PatientID": consultinfo.patientid,
                     "ImageStr": urls.toString()
                 };
                 hcuePatientCaseService.updateaudionotes(params, onupdatesuccess, onupdateerror);
             });
         function onupdatesuccess(response) {
             var documentid = response.data;
             var object = {
                 "name": $files[0].name,
                 "documentid": documentid,
                 "thumbUrl": url.toString()
             }
             $uibModalInstance.close(object);
         }
         function onupdateerror(data) {
             alert('file size should not be grater then 5 mb.');
         }
     };
     //$scope.uploadinvoicefiles = function () {
     //    var file = $scope.Invoicefiles;
     //    Upload.base64DataUrl(file)
     //        .then(function (urls) {
     //            var params = {
     //                "UpLoadType": "LabInvoice",
     //                "PatientID": consultinfo.patientid,
     //                "ImageStr": urls.toString()
     //            };
     //            hcuePatientCaseService.updateaudionotes(params, onupdatesuccess, onupdateerror);
     //        });
     //    function onupdatesuccess(response) {
     //        var documentid = response.data;
     //        var object = {
     //            "name": $scope.filename,
     //            "documentid": documentid
     //        }
     //        $uibModalInstance.close(object);
     //    }
     //    function onupdateerror(data) {
     //        alert('file size should not be grater then 5 mb.');
     //    }
     //};
     $scope.removeImgInvoice = function () {
         $scope.Invoicefiles = [];
     };
 });



hCueDoctorWebControllers.controller('EstimationPopup', function ($scope, $uibModalInstance, Upload, hcuePatientCaseService, $compile, planNumber, consultinfo, hcueConsultationInfo, EstiImgdocArray, EstiPdfdocArray) {

    $scope.CloseEstimationpopUp = function () {
        $uibModalInstance.close();
    };
    $scope.files = [];
    $scope.onFileSelect = function ($files) {
        var url = "";
        Upload.base64DataUrl($files).then(function (urls) {
            url = urls;
            var params = {
                "UpLoadType": "Estimation",
                "PatientID": consultinfo.patientid,
                "ImageStr": urls.toString()
            };

         hcuePatientCaseService.updateaudionotes(params, onupdatesuccess, onupdateerror);
        });
        function onupdatesuccess(response) {

            var documentid = response.data;
            var object = {
                "name": $files[0].name,
                "documentid": documentid
            };


            if ($files[0].type != "application/pdf") {
                EstiImgdocArray.push({
                    "url": url.toString(),
                    "documentid": documentid,
                    "Name": $files[0].name,
                    "planNumber": planNumber,
                    "thumbUrl": url.toString()
                });
                consultinfo.EstiImageDocumentArray = [];
                consultinfo.EstiImageDocumentArray = EstiImgdocArray;
            } else {
                EstiPdfdocArray.push({
                    "documentid": documentid,
                    "Name": $files[0].name,
                    "planNumber": planNumber,
                    "url": url.toString(),
                });
                consultinfo.EstiPdfdocArray = [];
                consultinfo.EstiPdfdocArray = EstiPdfdocArray;
            }



            hcueConsultationInfo.addConsultationInfo(consultinfo);

            $uibModalInstance.close(object);
        }
        function onupdateerror(data) {
            console.log(JSON.stringify(data));
            alert('file size should not be grater than 5 mb.');
        }

    };
    $scope.removeImgEst = function () {
        $scope.files = [];
    };
});

hCueDoctorWebControllers.controller('Trt_DeleteAudioConfirm', function ($scope, $uibModalInstance, datacookieFactory, datapersistanceFactory, $uibModal) {
    $scope.Trt_CloseDeleteAudioConfirm = function () {
        //$('#' +id).remove();
        $uibModalInstance.close();
    };
});
hCueDoctorWebControllers.controller("Trt_DeleteEstiImage", ["$uibModalInstance", "$uibModal", "hcuePatientCaseService", "hcueConsultationInfo", "datacookieFactory", "datapersistanceFactory", "$rootScope", "$scope", "DocumentId", "PatientId", "EstiImageDocumentArray", "consultinfo", function ($uibModalInstance, $uibModal, hcuePatientCaseService, hcueConsultationInfo, datacookieFactory, datapersistanceFactory, $rootScope, $scope, DocumentId, PatientId, EstiImageDocumentArray, consultinfo) {
    $scope.Trt_CloseEstiDelImage = function () {
        $uibModalInstance.close();
    };
    $scope.Trt_CloseDelEstiImage = function (id, patiendid, docterid) {
        $uibModalInstance.close();
        var name = angular.element("#btnDeleteEstiImage").attr("data-name");
        var params = {
            "USRType": "DOCTOR",
            "UpLoadType": "CaseDocument",
            "PatientID": patiendid,
            "USRId": docterid,
            "DocumentNumber": "" + id
        };
        hcuePatientCaseService.deleteaudionotes(params, onupdateDeletesuccess, onupdateDelerror);
        function onupdateDeletesuccess(response) {
            if (response.data == 'Success') {
                $('#Img_' + id).remove();
                for (var k = 0; k < EstiImageDocumentArray.length; k++) {
                    if (EstiImageDocumentArray[k].documentid == id) {
                        EstiImageDocumentArray.splice(k, 1);
                    }
                }
                var str = "";
                str += " <div class='modal-body condt-abcess'>";
                str += " <div class='row'>";
                str += "<div class='col-lg-12 '>";
                str += " <div class='cond-close'>";
                str += "  <label data-ng-click='DeleteEstiImageConfirm()' class='pull-right'>x</label>";
                str += " </div>";
                str += " <div class='condt-abcess'>";
                str += "<p style='text-align:center; margin: 0;'>";
                str += "<label>" + name + "";
                str += "</label>";
                str += " </p>";
                str += "<p>has been successfully deleted.</p>";
                str += "</div></div></div>";
                str += "</div>";
                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    template: str,
                    controller: 'DeleteEstiImage'
                });
                consultinfo.EstiImageDocumentArray = [];
                console.log(JSON.stringify(EstiImageDocumentArray));
                consultinfo.EstiImageDocumentArray = EstiImageDocumentArray;
                hcueConsultationInfo.addConsultationInfo(consultinfo);
            }
        }
        function onupdateDelerror(errordata) {
            console.log(JSON.stringify(errordata));
        }
    };
}]);
hCueDoctorWebControllers.controller("DeleteEstiImage", ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
    $scope.DeleteEstiImageConfirm = function () {
        $uibModalInstance.close();
    };
}]);
hCueDoctorWebControllers.controller('Trt_DeleteEstiPdf', function ($rootScope, $scope, $uibModalInstance, $uibModal, hcuePatientCaseService, hcueConsultationInfo, datacookieFactory, datapersistanceFactory, DocumentId, PatientId, EstiPdfdocArray, consultinfo) {
    $scope.Trt_CloseEstiDelPdf = function () {
        $uibModalInstance.close();
    };
    $scope.Trt_CloseEstiDelPdf = function (id, patiendid, docterid) {
        $uibModalInstance.close();
        var name = angular.element("#btnDeleteEstiPdf").attr("data-name");
        var params = {
            "USRType": "DOCTOR",
            "UpLoadType": "CaseDocument",
            "PatientID": patiendid,
            "USRId": docterid,
            "DocumentNumber": "" + id
        };
        hcuePatientCaseService.deleteaudionotes(params, onupdateDeletesuccess, onupdateDelerror);
        function onupdateDeletesuccess(response) {
            console.log(JSON.stringify(response));
            if (response.data == 'Success') {
                console.log($('#Pdf_' + id));
                $('#Pdf_' + id).remove();
                for (var k = 0; k < EstiPdfdocArray.length; k++) {
                    if (EstiPdfdocArray[k].documentid == id) {
                        EstiPdfdocArray.splice(k, 1);
                    }
                }
                var str = "";
                str += " <div class='modal-body condt-abcess'>";
                str += " <div class='row'>";
                str += "<div class='col-lg-12 '>";
                str += " <div class='cond-close'>";
                str += "  <label data-ng-click='DeleteEstiPdfConfirm()' class='pull-right'>x</label>";
                str += " </div>";
                str += " <div class='condt-abcess'>";
                str += "<p style='text-align:center; margin: 0;'>";
                str += "<label>" + name + "";
                str += "</label>";
                str += " </p>";
                str += "<p>has been successfully deleted.</p>";
                str += "</div></div></div>";
                str += "</div>";
                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    template: str,
                    controller: 'DeleteEstiPdf'
                });
                consultinfo.EstiPdfdocArray = [];
                console.log(JSON.stringify(EstiPdfdocArray));
                consultinfo.EstiPdfdocArray = EstiPdfdocArray;
                hcueConsultationInfo.addConsultationInfo(consultinfo);
            }
        }
        function onupdateDelerror(errordata) {
        }
    };
});
hCueDoctorWebControllers.controller("DeleteEstiPdf", ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
    $scope.DeleteEstiPdfConfirm = function () {
        $uibModalInstance.close();
    };
}]);

 
 hCueDoctorWebControllers.controller("NotesImage", ["$scope", "$uibModalInstance", "consultinfo", "hcuePatientCaseService",
  "$timeout", "Upload", "ImageDocumentArray","PdfDocumentArray",
  function($scope, $uibModalInstance, consultinfo, hcuePatientCaseService, $timeout, Upload, ImageDocumentArray,PdfDocumentArray) {
     $scope.ImageNotes = [];
     $scope.CloseImageNotesPopup = function() {
         $uibModalInstance.close();
     };
     $scope.removeImageNotes = function() {
         $scope.ImageNotes = [];
     };

     $scope.onFileSelectImageNotes = function($files) {

         ////console.log($files);

         var url = "";
         var imgname = $files[0].name;
         var filename = imgname.split(".");
         var fileURL = filename[0];
         var type = $files[0].type;
         console.log(type);
         var Type = type.toString().split('/');
         //$scope.name = Type[0].toString();
         $scope.type = Type[1].toString();

         Upload.base64DataUrl($files)
             .then(function(urls) {
                 url = urls;
                 // ////console.log(JSON.stringify(urls));
                 var params = {
                     "UpLoadType": "CaseDocument",
                     "PatientID": consultinfo.patientid,
                     "ImageStr": urls.toString(),
                     //"ImageStr": urls.toString().split("data:" + type + ";base64,")[1],
                     "FileExtn": "." + $scope.type,
                     "DocumentNumber": fileURL
                 };

                 hcuePatientCaseService.updateaudionotes(params, onupdatesuccess, onupdateerror);
             });

         function onupdatesuccess(response) {
             $uibModalInstance.close();
             $scope.imageTitle = $files[0].name;
             var count = ImageDocumentArray.length + 1;
             var documentid = response.data;
             var name = $scope.imageTitle;
             if (type != "application/pdf") {
                 ImageDocumentArray.push({
                     "url": url.toString(),
                     "documentid": documentid,
                     "name": $scope.imageTitle,
                     "thumbUrl": url.toString()
                 });
             } else {
                 PdfDocumentArray.push({
                     "url": url.toString(),
                     "documentid": documentid,
                     "name": name
                 });
             }
         }

         function onupdateerror(data) {
             alertify.log("Please Check Internet Connectivity");
         }
     };

  }]);


 hCueDoctorWebControllers.controller('Trt_DeleteAudioNotes', function($rootScope, $scope, $uibModalInstance, 
 $uibModal, hcuePatientCaseService, hcueConsultationInfo, datacookieFactory, 
 datapersistanceFactory, DocumentId, PatientId, voiceDocumentArray, consultinfo, Index) {
     $scope.Trt_CloseDelAudio = function(id) {
         //$('#' + documentid).remove();
         $uibModalInstance.close();
     };
     $scope.Trt_CloseDelAudioNote = function(Id, pId) {
         var params = {
             "USRType": "DOCTOR",
             "UpLoadType": "VoiceDocument",
             "PatientID": pId,
             "USRId": doctorinfo.doctorid,
             "DocumentNumber": "" + Id
         };

hcuePatientCaseService.deleteaudionotes(params, onupdateDeletesuccess, onupdateDelerror);

         function onupdateDeletesuccess(response) {
             
             if (response.data == 'Success') {
               
                 var consultinfo = hcueConsultationInfo.getConsultationInfo();
                 
                 voiceDocumentArray.splice(Index, 1);
                 
                 consultinfo.voiceDocuments.voiceDocumentArray = voiceDocumentArray;
                 hcueConsultationInfo.addConsultationInfo(consultinfo);
             }
         }

         function onupdateDelerror(errordata) {}
         $uibModalInstance.close();
         var str = "";
         str += " <div class='modal-body condt-abcess'>";
         str += " <div class='row'>";
         str += "<div class='col-lg-12 '>";
         str += " <div class='cond-close'>";
         str += "  <label ng-click='Trt_CloseDeleteAudioConfirm()' class='pull-right'>x</label>";
         str += " </div>";
         str += " <div class='condt-abcess'>";
         str += "<p style='text-align:center; margin: 0;'>";
         str += "<label>Audio";
         str += "</label>";
         str += " </p>";
         str += "<p>has been successfully deleted.</p>";
         str += "</div></div></div>";
         str += "</div>";
         var modalInstance = $uibModal.open({
             animation: $scope.animationsEnabled,
             template: str,
             controller: 'Trt_DeleteAudioConfirm',
             resolve: {
                 items: function() {
                     return $scope.items;
                 }
             }
         });
     };
 });

 hCueDoctorWebControllers.controller('Trt_DeleteAudioConfirm', function($scope, $uibModalInstance, datacookieFactory, datapersistanceFactory, $uibModal) {
     $scope.Trt_CloseDeleteAudioConfirm = function() {
         //$('#' +id).remove();
         $uibModalInstance.close();
     };
 });

 
 function GetSpeciality(doctorspeciality, specialitycode) {
     var spl = "";

     angular.forEach(doctorspeciality, function(item) {
         angular.forEach(specialitycode, function(k, v) {
             //display the key and value pair
             if (item.DoctorSpecialityID == k) {
                 spl += item.DoctorSpecialityDesc + " ";
             }
         });
     });
     return spl;
 }

 function GetQualification(qualifications) {
     var education = "";
     angular.forEach(qualifications, function(k, v) {
         education += k + " ";
     });
     return education;
 }

 function Capitalize(string) {
     var word = "";
     for (var z = 0; z < string.length; z++) {
         if (z == 0) {
             word += string[z].toUpperCase();
         } else {
             word += string[z];
         }
     }
     //console.log(word);
     return word;
 }

 function GetMemberID(memberid) {
     var regid = "";
     angular.forEach(memberid, function(k, v) {
         regid += k + " ";
     });
     //////console.log(regid);
     return regid;
 }

 function GetClinicImages(images) {
     var image = "";
     angular.forEach(images, function(k, v) {
         image += k + " ";
     });
     //////console.log(image);
     return image;
 }

 function GetDictionaryValues(qualifications) {
     var education = "";
     angular.forEach(qualifications, function(k, v) {
         education += k + " ";
     });
     return education;
 }

 function GetDatas(lists) {
     var items = [];
     var data = lists; // $scope.doctorinfo.doctor[0].Qualification;
     angular.forEach(data, function(item) {
         var dataitem = {};
         dataitem.name = item;
         items.push(dataitem);
     });

     return items;
 }

 function GetDataspeciality(lists) {
     var items = [];
     var data = lists; // $scope.doctorinfo.doctor[0].Qualification;
     angular.forEach(data, function(item) {
         var dataitem = {};
         dataitem.DoctorSpecialityID = item;
         items.push(dataitem);
     });

     return items;
 }

 function GetHours(time) {
     var hh = "00";
     if (time.indexOf(":") != -1) {
         var hrmm = time.split(":");
         hh = hrmm[0];
     } else {
         hh = time.substring(0, 2);

     }
     return hh;
 }

 function GetMins(time) {
     var mm = "00";
     if (time.indexOf(":") != -1) {
         var hrmm = time.split(":");
         mm = hrmm[1];
     } else {
         mm = time.substring(2, 4);

     }
     return mm;
 }

 function GetDay(daycd) {
     switch (daycd) {
         case "MON":
             return "Monday";
         case "TUE":
             return "Tuesday";
         case "WED":
             return "Wednesday";
         case "THU":
             return "Thursday";
         case "FRI":
             return "Friday";
         case "SAT":
             return "Saturday";
         case "SUN":
             return "Sunday";
     }
 }

 function GetAge(dob) {
     var ageDifMs = Date.now() - new Date(dob).getTime();
     var ageDate = new Date(ageDifMs); // miliseconds from epoch
     var age = Math.abs(ageDate.getUTCFullYear() - 1970);
     if (isNaN(age))
         age = 0;

     return age;
 }

 function DisableTap(id) {
     var container = document.getElementsByClassName('pac-container');
     // disable ionic data tab
     angular.element(container).attr('data-tap-disabled', 'true');
     // leave input field if google-address-entry is selected
     angular.element(container).on("click", function() {
         document.getElementById(id).blur();
     });
 }

 function InitialiseConsultInfo(hcueConsultationInfo, datapersistanceFactory) {
    /* var consultinfo = {
         prescription: { pharmaid: 0, medicines: [] },
         labtests: [],
         CaseTreatment: [],
         scantests: [],
         referrals: [],
         documents: {},
         vitalnoteinfo: { visitreason: '', vitals: {}, notes: { symptom: '', general: '', followup: '', doctornotes: '' } },
         caseid: 0,
         appointmentid: 0,
         patientid: 0,
         doctorid: 0,
         fees: 0.00,
         BillingInfo: [],
         ImageNotesData: [],
         voiceDocuments: { voiceDocumentArray: [] },
         treatmentCollection: {
             VisitRsn: [],
             Diagnostic: [],
             Investigation: [],
             PastMedicalHistory: []
         },
         DentalConditionsArray: { DentalHistoryArray: [], MedicalHistoryArray: [] },CasePrescription:[{"RowID": 1,"PatientCaseConditions":[],"USRId": 0}]

     } */

    var consultinfo = angular.copy(hcueConsultationInfo.getNewConsultInfoStructure());
     console.log(JSON.stringify(consultinfo));

     consultinfo.vitalnoteinfo.vitals = { TMP: '', HGT: '', WGT: '', SPP: '', SFT: '', BPL: '', BPH: '', BLG: '' };

     hcueConsultationInfo.addConsultationInfo(consultinfo);

     hcueConsultationInfo.getConsultationInfo();

     datapersistanceFactory.remove("notes-drawing");

 }

 function UpdateConsultInfo(patientinfo, datapersistanceFactory, hcueConsultationInfo, hcueDoctorLoginService) {
     
     if (!JSON.parse(localStorage.getItem("isAppOnline"))) {
         var consultinfo = hcueConsultationInfo.getConsultationInfo();
         if(patientinfo.Patient!=undefined){
             consultinfo.patientid=patientinfo.Patient[0].PatientID;
         }
         consultinfo.doctorid = hcueDoctorLoginService.getDoctorId();
     }
     else
     {
         InitialiseConsultInfo(hcueConsultationInfo, datapersistanceFactory); //Clear old value reference
         //window.alert(JSON.stringify(patientinfo));
         var appointmentinfo = hcueDoctorLoginService.getAppointmentInfo();
         appointmentinfo.PatientID = patientinfo.Patient[0].PatientID;
         appointmentinfo.PatientDetails.Patient = patientinfo.Patient;
         appointmentinfo.PatientDetails.PatientAddress = patientinfo.PatientAddress;
         appointmentinfo.PatientDetails.PatientPhone = patientinfo.PatientPhone;
         hcueDoctorLoginService.addAppointmentInfo(appointmentinfo);


         var consultinfo = hcueConsultationInfo.getConsultationInfo();
         //var cserv = hcueConsultationInfo.getConsoltationService();

         consultinfo.patientid = patientinfo.Patient[0].PatientID;
         consultinfo.doctorid = hcueDoctorLoginService.getDoctorId();
     }

     hcueConsultationInfo.addConsultationInfo(consultinfo);

 }

 function UpdateConsultInfoEntryState(caseid, patientinfo, datapersistanceFactory, hcueConsultationInfo, hcueDoctorLoginService) {
     InitialiseConsultInfo(hcueConsultationInfo, datapersistanceFactory); //Clear old value reference
     //window.alert(JSON.stringify(patientinfo));
     var appointmentinfo = hcueDoctorLoginService.getAppointmentInfo();
     //window.alert('cum1');
     //window.alert(JSON.stringify(patientinfo));
     appointmentinfo.PatientID = patientinfo.Patient[0].PatientID;
     appointmentinfo.PatientDetails.Patient = patientinfo.Patient;
     appointmentinfo.PatientDetails.PatientAddress = patientinfo.PatientAddress;
     appointmentinfo.PatientDetails.PatientPhone = patientinfo.PatientPhone;
     hcueDoctorLoginService.addAppointmentInfo(appointmentinfo);


     var consultinfo = hcueConsultationInfo.getConsultationInfo();
     //var cserv = hcueConsultationInfo.getConsoltationService();

     consultinfo.patientid = patientinfo.Patient[0].PatientID;
     consultinfo.doctorid = hcueDoctorLoginService.getDoctorId();
     consultinfo.caseid = caseid;
     hcueConsultationInfo.addConsultationInfo(consultinfo);

 }

 function GetImage(serverImage) {

     if (serverImage != null && serverImage.substring(0, 5).toLowerCase() == "some(") {
         serverImage = serverImage.substring(5);
         serverImage = serverImage.substring(0, serverImage.length - 1)
         return serverImage;
     }
     return "";
 }

 function ShowLoading($ionicLoading) {

     $ionicLoading.show({
         noBackdrop: true,
         template: '<p class="item-icon-left">Loading...<ion-spinner icon="lines"/></p>' // '<ion-spinner icon="crescent" class="spinner spinner-crescent" style="background-color:white"><svg viewBox="0 0 64 64"><g><circle stroke-width="4" stroke-dasharray="128" stroke-dashoffset="82" r="26" cx="32" cy="32" fill="none" transform="rotate(243.761 32 32)"><animateTransform values="0,32,32;360,32,32" attributeName="transform" type="rotate" repeatCount="indefinite" dur="750ms"></animateTransform></circle></g></svg></ion-spinner>' // '<p class="item-icon-left">Loading stuff...<ion-spinner icon="lines"/></p>' 

     });
 }
 //arun code
 function validate(key) {

     //getting key code of pressed key
     var keycode = (key.which) ? key.which : key.keyCode;

     //comparing pressed keycodes
     if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57)) {
         return false;
     } else {
         //Condition to check textbox contains ten numbers or not

     }
 }

 
  // allow only numbers 
 function isNumber(evt) {
     evt = (evt) ? evt : window.event;
     var charCode = (evt.which) ? evt.which : evt.keyCode;
     if (charCode > 31 && (charCode < 48 || charCode > 57)) {
         return false;
     }
     return true;
 }

 function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
