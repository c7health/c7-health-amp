//'use strict';
/* Services */

var hCueDoctorWebServices = angular.module('hCueDoctorWebServices', ['ngResource', 'ngCookies', 'LocalStorageModule']);
var header = { headers: { "Content-Type": "application/json", "Accept-Encoding": "gzip" } };
var imageheader = { headers: { "Content-Type": "text/json", "Accept-Encoding": "gzip" } };
var fileheader = { headers: { "Content-Type": undefined } };
var doctorinfo = "";

hCueDoctorWebServices.service('hcueDoctorDataService', ['$http', 'datapersistanceFactory', 'hcueServiceUrl', 'hcueDoctorSignUp', 'hcueDoctorLoginService', 'hcueOfflineServiceUrl', function ($http, datapersistanceFactory, hcueServiceUrl, hcueDoctorSignUp, hcueDoctorLoginService, hcueOfflineServiceUrl) {

    doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    this.PopulateDoctorInfo = function (data) {

        return PopulateDoctorData(data, true);
    }
    this.PopulateDoctorProfileInfo = function (data) {

        return PopulateDoctorData(data, false);
    }


    this.ListDepartments = function (params, callbackSuccess, callbackError) {
        var deptUrl = hcueServiceUrl.listDoctorDepartment;
        $http.post(deptUrl, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }

    // Loads the look up values - specialit type, reason
    this.PopulateLookupInfoOnLoad = function () {

        this.GetLookUpData();


    }
    this.GetLookUpData = function () {
        var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

        if (datapersistanceFactory.getItem("lookup").length == 0) {
            var param = { "ActiveIND": "Y", "SearchText": "" };
            $http.post(hcueServiceUrl.readLookup, param, header)
                .success(function (data) {

                    datapersistanceFactory.set("lookup", data);

                    doctorinfo.specialitylist = data.doctorSpecialityType;
                    doctorinfo.visitreasonlist = data.doctorVisitRsnType;
                    hcueDoctorLoginService.setDoctorInfo(doctorinfo);
                })
                .error(function (data) { });
        } else {
            var data = datapersistanceFactory.getItem("lookup");
            doctorinfo.specialitylist = data.doctorSpecialityType;
            doctorinfo.visitreasonlist = data.doctorVisitRsnType;
            hcueDoctorLoginService.setDoctorInfo(doctorinfo);
        }

    }


    this.AddDoctorMedicineTemplate = function (templatename, templateitems, doctorid, templateid, callbackSuccess) {
        var doctorrequest = { "TemplateName": templatename, "TemplateDetails": templateitems, "DoctorID": doctorid, "USRType": "DOCTOR", "USRId": doctorid, "ActiveIND": 'Y' };
        var url = hcueServiceUrl.addMedicineTemplate;
        $http.post(url, doctorrequest, header)
            .success(function (data) {
                callbackSuccess(data);
            });
    };
    this.AddDoctorMedicineTemplate1 = function (key, templatename, templateitems, doctorid, templateid, callbackSuccess) {
        var doctorrequest = { "TemplateName": templatename, "TemplateDetails": templateitems, "DoctorID": doctorid, "USRType": "DOCTOR", "USRId": doctorid, "TemplateID": templateid, "ActiveIND": 'Y' };
        url = hcueServiceUrl.updateMedicineTemplate;
        $http.post(url, doctorrequest, header)
            .success(function (data) {
                callbackSuccess(data, key);
            });
    };
    this.DeleteDoctorMedicineTemplate = function (doctorrequest, callbackSuccess) {
        url = hcueServiceUrl.updateMedicineTemplate;
        $http.post(url, doctorrequest, header)
            .success(function (data) {
                callbackSuccess(data);
            });
    };
    this.GetDoctorMedicineTemplate = function (callbackSuccess) {
        var doctorId = hcueDoctorLoginService.getLoginId();
        $http.get(hcueServiceUrl.getTemplatebyDoctorID + '/' + doctorId, header)
            .success(function (data) {
                datapersistanceFactory.set("medicinetemplates", data);
                callbackSuccess(data);
            })
            .error(function (data) { });
    }
    this.GetDoctorRef = function (doctorid, callbackSuccess, callbackError) {
        //console.log(JSON.stringify(doctorid));
        var hospid = undefined;
        var hospcd = undefined;
        var parenthospid = undefined;
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
            hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
            hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            parenthospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        }
        var listDoctorLabs = {
            USRType: "DOCTOR",
            USRId: doctorid,
            DoctorID: doctorid,
            HospitalCD: hospcd,
            HospitalID: hospid,
            ParentHospitalID: parenthospid
        };
        if (hospid == undefined && hospcd == undefined && parenthospid == undefined) {
            delete listDoctorLabs.HospitalCD;
            delete listDoctorLabs.HospitalID;
            delete listDoctorLabs.ParentHospitalID;
        }
        var params2 = {};
        //var referralurl = hcueServiceUrl.getDoctorRefList + "/" + doctorid.toString() + "/list";
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getDoctorRefList, listDoctorLabs, params2, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorid + '/' + hcueOfflineServiceUrl.getDoctorRefList, params2, header);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.GetLabRef = function (labid, callbackSuccess, callbackError) {
        //console.log(JSON.stringify(labid));
        var hospid = undefined;
        var hospcd = undefined;
        var parenthospid = undefined;
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
            hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
            hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            parenthospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        }
        var labparams1 = {};
        var labParams = {
            USRType: "DOCTOR",
            USRId: labid,
            DoctorID: labid,
            HospitalDetails: {
                HospitalCD: hospcd,
                HospitalID: hospid,
                ParentHospitalID: parenthospid
            }
        };
        if (hospid == undefined && hospcd == undefined && parenthospid == undefined) {
            delete labParams.HospitalDetails;
        }
        var labparams1 = {};
        //var labreferurl = hcueServiceUrl.getLabListByDoctorId + "/" + labid.toString() + "/list";
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getLabListByDoctorId, labParams, labparams1, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getLabListByDoctorId, labparams1, header);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.GetDocPharma = function (pharmaid, callbackSuccess, callbackError) {
        //console.log(JSON.stringify(labid));
        var hospid = undefined;
        var hospcd = undefined;
        var parenthospid = undefined;
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
            hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
            hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            parenthospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        }
        if (doctorinfo.doctordata.doctorAddress[0].AddressID !== undefined) {
            addressID = doctorinfo.doctordata.doctorAddress[0].AddressID;
        }
        var labparams1 = {};
        var pharmareferurl = {
            USRType: "DOCTOR",
            USRId: pharmaid,
            AddressID: addressID,
            DoctorID: pharmaid,
            HospitalDetails: {
                HospitalCD: hospcd,
                HospitalID: hospid,
                ParentHospitalID: parenthospid
            }
        };
        if (hospid == undefined && hospcd == undefined && parenthospid == undefined) {
            delete pharmareferurl.HospitalDetails;
        }
        // var pharmareferurl = hcueServiceUrl.getPharmaListByDoctorId + "/" + pharmaid.toString() + "/list";
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getPharmaListByDoctorId, pharmareferurl, labparams1, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.pharmaurl, labparams1, header);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });
    }


    this.GetDoctorReferral = function (doctorid, callbackSuccess, callbackError) {
        //GetDoctorReferralListByDoctorId
        var doctorparams = {};
        var referurl = hcueServiceUrl.getDoctorRefList + "/" + doctorid.toString() + "/list";
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.get(referurl, doctorparams, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorid + '/' + hcueOfflineServiceUrl.getDoctorRefList, doctorparams, header);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });
    }

    function GetDoctorMedicineTemplate(doctorId) {
        $http.get(hcueServiceUrl.getTemplatebyDoctorID + '/' + doctorId, header)
            .success(function (data) {
                datapersistanceFactory.set("medicinetemplates", data);
            })
            .error(function (data) { });

    }

    function GetPharmaListByDoctorId(doctorinfo, data) {
        var pharmaparams = {};
        var userId = doctorinfo.doctorid;
        var newdoctorinfo = data.doctorAddress;
        var hospid = undefined;
        var hospcd = undefined;
        var parenthospid = undefined;
        if (newdoctorinfo[0].ExtDetails.HospitalInfo !== undefined && newdoctorinfo[0].ExtDetails.HospitalInfo.HospitalID !== undefined && newdoctorinfo[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && newdoctorinfo[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {


            hospid = newdoctorinfo[0].ExtDetails.HospitalInfo.HospitalID;
            hospcd = newdoctorinfo[0].ExtDetails.HospitalInfo.HospitalCode;
            parenthospid = newdoctorinfo[0].ExtDetails.HospitalInfo.ParentHospitalID;
        }
        if (newdoctorinfo[0].AddressID !== undefined) {
            addressID = newdoctorinfo[0].AddressID;
        }
        var GetPharmaListLogin = {
            USRType: "DOCTOR",
            USRId: userId,
            DoctorID: userId,
            AddressID: addressID,

            HospitalDetails: {
                HospitalCD: hospcd,
                HospitalID: hospid,
                ParentHospitalID: parenthospid
            }

        };
        if (hospid == undefined && hospcd == undefined && parenthospid == undefined) {

            delete GetPharmaListLogin.HospitalDetails;

        }

        $http.post(hcueServiceUrl.getPharmaListByDoctorId, GetPharmaListLogin, pharmaparams, header)

            //var pharmaurl = hcueServiceUrl.getPharmaListByDoctorId + "/" + doctorinfo.doctorid.toString() + "/list";
            //$http.get(pharmaurl, pharmaparams, header)
            .success(function (data) {
                doctorinfo.referralpharmacies = data.rows;
                hcueDoctorLoginService.setDoctorInfo(doctorinfo);
            })
            .error(function (data) { });
    }

    function GetLabListByDoctorId(doctorinfo, data) {
        var labparams = {};
        var userId = doctorinfo.doctorid;
        var newdoctorinfo = data.doctorAddress;
        var hospid = undefined;
        var hospcd = undefined;
        var parenthospid = undefined;
        if (newdoctorinfo[0].ExtDetails.HospitalInfo !== undefined && newdoctorinfo[0].ExtDetails.HospitalInfo.HospitalID !== undefined && newdoctorinfo[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && newdoctorinfo[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {


            hospid = newdoctorinfo[0].ExtDetails.HospitalInfo.HospitalID;
            hospcd = newdoctorinfo[0].ExtDetails.HospitalInfo.HospitalCode;
            parenthospid = newdoctorinfo[0].ExtDetails.HospitalInfo.ParentHospitalID;
        }
        var GetLabListLogin = {
            USRType: "DOCTOR",
            USRId: userId,
            DoctorID: userId,

            HospitalDetails: {
                HospitalCD: hospcd,
                HospitalID: hospid,
                ParentHospitalID: parenthospid
            }

        };
        if (hospid == undefined && hospcd == undefined && parenthospid == undefined) {

            delete GetLabListLogin.HospitalDetails;

        }

        $http.post(hcueServiceUrl.getLabListByDoctorId, GetLabListLogin, labparams, header)

            //var laburl = hcueServiceUrl.getLabListByDoctorId + "/" + doctorinfo.doctorid.toString() + "/list";
            // $http.get(laburl, labparams, header)
            .success(function (data) {
                doctorinfo.referrallabs = data.rows;
                hcueDoctorLoginService.setDoctorInfo(doctorinfo);
            })
            .error(function (data) { });
    }

    function GetDoctorReferralListByDoctorId(doctorinfo, data) {
        var doctorparams = {};
        var userId = doctorinfo.doctorid;
        var newdoctorinfo = data.doctorAddress;
        var hospid = undefined;
        var hospcd = undefined;
        var parenthospid = undefined;
        if (newdoctorinfo[0].ExtDetails.HospitalInfo !== undefined && newdoctorinfo[0].ExtDetails.HospitalInfo.HospitalID !== undefined && newdoctorinfo[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && newdoctorinfo[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {


            hospid = newdoctorinfo[0].ExtDetails.HospitalInfo.HospitalID;
            hospcd = newdoctorinfo[0].ExtDetails.HospitalInfo.HospitalCode;
            parenthospid = newdoctorinfo[0].ExtDetails.HospitalInfo.ParentHospitalID;
        }
        var GetDoctorReferralListByDoctorId = {
            USRType: "DOCTOR",
            USRId: userId,
            DoctorID: userId,


            HospitalCD: hospcd,
            HospitalID: hospid,
            ParentHospitalID: parenthospid


        };
        console.log(JSON.stringify(data));
        if (hospid == undefined && hospcd == undefined && parenthospid == undefined) {

            delete GetDoctorReferralListByDoctorId.HospitalCD;
            delete GetDoctorReferralListByDoctorId.HospitalID;
            delete GetDoctorReferralListByDoctorId.ParentHospitalID;
        }

        //var referurl = hcueServiceUrl.getDoctorRefList + "/" + doctorinfo.doctorid.toString() + "/list";
        $http.post(hcueServiceUrl.getDoctorRefList, GetDoctorReferralListByDoctorId, doctorparams, header)

            //$http.get(referurl, doctorparams, header)
            .success(function (data) {
                doctorinfo.referraldoctors = data.rows;
                hcueDoctorLoginService.setDoctorInfo(doctorinfo);
            })
            .error(function (data) { });
    }

    function GetAllMappedInfoByDoctorId(doctorid, data) {
        var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

        doctorinfo.doctorid = doctorid;

        // GetPharmaListByDoctorId(doctorinfo, data);
        // GetLabListByDoctorId(doctorinfo, data);
        // GetDoctorReferralListByDoctorId(doctorinfo, data);
        // GetDoctorMedicineTemplate(doctorid);
        // RegisterDevice(doctorid);
    }
    //Sarayu offline code
    function SetMembershipDiscountData(doctorinfo) {
        var valdata = {};
        hcueDoctorLoginService.addMembershipDiscountData(valdata);
        //if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && 
        //doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined ) {

        //    var hospcode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;

        //    var hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;

        //    var param = {
        //        "HospitalCD":hospcode,
        //        "HospitalID":hospid,
        //        "USRId":doctorinfo.doctorid,
        //        "USRType":"DOCTOR",
        //        "PageNumber":1,
        //        "PageSize":100
        //    }

        //    $http.post( hcueServiceUrl.listMembershipDetails, param, header)
        //        .success(function (data) {

        //            hcueDoctorLoginService.addMembershipDiscountData(data);
        //        })
        //        .error(function (data) { 
        //            var valdata = {};
        //            hcueDoctorLoginService.addMembershipDiscountData(valdata);

        //        });
        //}

    }

    //Arun code
    function PopulateDoctorData(data, loadallmappedinfo) {

        var docArrObjj = data.doctor;



        if (docArrObjj != null) {
            var docObjj = docArrObjj[0];
            var doctorid = docObjj.DoctorID;
            var mailArrobj = data.doctorEmail;
            var emailid = (mailArrobj != null || mailArrobj != undefined) ? mailArrobj[0].EmailID : "";
            if (loadallmappedinfo) {
                GetAllMappedInfoByDoctorId(doctorid, data);
            }

            var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
            //alert(JSON.stringify(doctorinfo));
            doctorinfo.doctorid = doctorid;
            doctorinfo.profileimage = GetImage(docObjj.ProfileImage);
            doctorinfo.doctorloginid = docObjj.DoctorLoginID;
            doctorinfo.specialitycode = docObjj.SpecialityCD;
            if (data.DoctorSetting) {
                doctorinfo.DoctorSetting = data.DoctorSetting;
            }
            if (data.doctorAddress) {
                doctorinfo.groupID = (data.doctorAddress[0].ExtDetails.GroupID !== undefined) ? data.doctorAddress[0].ExtDetails.GroupID : '';
                doctorinfo.hospitalID = data.doctorAddress[0].ExtDetails.HospitalID;
                doctorinfo.RoleID = (data.doctorAddress[0].ExtDetails.RoleID !== undefined) ? data.doctorAddress[0].ExtDetails.RoleID : '';
                if (doctorinfo.hospitalID) {
                    doctorinfo.isAccessRightsApplicable = true;
                }
                if (data.doctorAddress[0].ExtDetails.AccountAccessID !== undefined) {
                    doctorinfo.isAccessRightsApplicable = true;
                    angular.forEach(data.doctorAddress[0].ExtDetails.AccountAccessID, function (value, key) {
                        this.push(value);
                    }, doctorinfo.AccountAccessID);
                }

            }
            doctorinfo.doctordata = data;
            hcueDoctorLoginService.setDoctorInfo(doctorinfo);
            hcueDoctorLoginService.addLoginId(doctorid);

            var logininfo = hcueDoctorSignUp.getProfileInfo(); //Profile pages
            logininfo.doctorid = doctorid;
            logininfo.memberid = docObjj.MemberID;
            logininfo.specialitycode = docObjj.SpecialityCD;
            hcueDoctorSignUp.addProfileInfo(logininfo);
            console.log(doctorinfo.hospitalID);
            //Sarayu offline code
            if (doctorinfo.hospitalID) {
                SetMembershipDiscountData(doctorinfo);
            }
            return true;
        } else {
            return false;
        }

    }

    this.DocProfileList = function (docID, callbackSuccess, callbackError) {
        var offlinePost = $http.get(localStorage.getItem("offlineURL") + '/configure/' + docID + '/' + hcueOfflineServiceUrl.offlineDoctorData);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {

                callbackError(data);
            })
    }

    //End of arun code

    function RegisterDevice(doctorid) {
        /* var hasdeviceid = datapersistanceFactory.getItem("Pushwoosh");
         var pushwooshid = localStorage.getItem("pushwooshid");
         var deviceid = localStorage.getItem("token");
         
         if (deviceid !=null && deviceid.length > 0) {
             var ids = "[" + doctorid.toString() + "]";
             var param = {
                 USRType: "DOCTOR", ClientType: "DOCTOR", DeviceID: pushwooshid, UUID: deviceid, IDs: ids, "USRId": doctorid
             }
             $http.post(hcueServiceUrl.registerDevice, param,header)
             .success(function (data) {
                // alert("device registered successfully");
             })
         }*/

    }
}]);

/*IoT Services*/
//hCueDoctorWebServices.service('hcueExternalSolutions', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

//this.listDentalImages = function (params, callbackSuccess, callbackError) {
//    $http.post(hcueServiceUrl.listDentalImages, params, header)
//      .success(function (data) {
//          callbackSuccess(data);
//      })
//      .error(function (data) {
//          callbackError(data);
//      });
//};
//this.deleteDentalImages = function (params, callbackSuccess, callbackError) {
//    $http.post(hcueServiceUrl.deleteDentalImages, params, header)
//      .success(function (data) {
//          callbackSuccess(data);
//      })
//      .error(function (data) {
//          callbackError(data);
//      });
//};
//this.uploadDentalImages = function (params, callbackSuccess, callbackError) {
//    var request=new XMLHttpRequest();
//    request.open("POST",hcueServiceUrl.uploadDentalImages);
//    request.send(params);
//    request.onload=function(){
//        if(request.status==200){
//            $('#loaderDiv').hide()
//            callbackSuccess(request.responseText);
//        }
//        else{
//            $('#loaderDiv').hide()
//            callbackError(request.responseText);
//        }
//    }

//};
//this.postTallySoftware = function (params, callbackSuccess, callbackError) {
//    $http.post(hcueServiceUrl.postTallySoftware, params, header)
//      .success(function (data) {
//          callbackSuccess(data);
//      })
//      .error(function (data) {
//          callbackError(data);
//      });
//};
//this.stopDentalSoftware= function(params,callbackSuccess,callbackError){
//    $http.post(hcueServiceUrl.stopDentalSoftware,params,header)
//    .success(function (data) {
//        callbackSuccess(data);
//    })
//    .error(function (data) {
//        callbackError(data);
//    });
//}

//this.startDentalSoftware= function(params,callbackSuccess,callbackError){
//    $http.post(hcueServiceUrl.startDentalSoftware,params,header)
//    .success(function (data) {
//        callbackSuccess(data);
//    })
//    .error(function (data) {
//        callbackError(data);
//    });
//}

// this.deleteDentalImage=function(path,doctorid,name,ext,callbackSuccess,callbackerror){
//     $http.get(hcueServiceUrl.deleteDentalImage + '/' + path +'/'+doctorid+ '/' + name+'/'+ext)
//     .success(function (data) {
//         callbackSuccess(data);
//     })
//     .error(function (data) {
//         callbackError(data);
//     });
// }
//}]);
/*End of IoT Services*/
hCueDoctorWebServices.service('hcuePartnerService', ['$http', 'hcueServiceUrl', 'hcueOfflineServiceUrl', 'hcueDoctorLoginService', function ($http, hcueServiceUrl, hcueOfflineServiceUrl, hcueDoctorLoginService) {
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    this.GetPharmaSearchList = function (param, callbackSuccess, callbackError) {

        //var pharmaurl = hcueServiceUrl.pharmaSearch;
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.pharmaSearch, param, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.pharmaSearch, param);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.SaveLabProspect = function (param, callbackSuccess, callbackError) {

        var Laburl = hcueServiceUrl.AddLabProspect;


        $http.post(Laburl, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {

                //console.log(JSON.stringify(data));
                callbackError(data);
            });
    }
    this.SavePharmaProspect = function (param, callbackSuccess, callbackError) {

        var Pharmaurl = hcueServiceUrl.AddPharmaProspect;


        $http.post(Pharmaurl, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {

                //console.log(JSON.stringify(data));
                callbackError(data);
            });
    }
    this.GetLabSearchList = function (param, callbackSuccess, callbackError) {

        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.labSearch, param, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.labSearch, param);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.AddPharmacyToDoctor = function (doctorid, pharmacyitem, callbackSuccess, callbackError) {
        //console.log(JSON.stringify(pharmacyitem));
        $http.post(hcueServiceUrl.addDoctorPharma, pharmacyitem, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }
    this.deletePharmacyToDoctor = function (doctorid, pharmacyitem, callbackSuccess, callbackError) {
        console.log(JSON.stringify(pharmacyitem));
        $http.post(hcueServiceUrl.addDoctorPharma, pharmacyitem, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.LabReferralDelete = function (doctorid, labitem, callbackSuccess, callbackError) {
        console.log(JSON.stringify(labitem));
        $http.post(hcueServiceUrl.addDoctorLab, labitem, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.AddLabToDoctor = function (doctorid, labitem, callbackSuccess, callbackError) {

        //console.log(JSON.stringify(labitem));
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addDoctorLab, labitem, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addDoctorLab, labitem);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });

    }
    this.AddDoctorReferral = function (param, callbackSuccess, callbackError) {
        var areacode = 1;
        var statecode = 1;
        var hasPhone = false;
        var hospid = undefined;
        var hospcd = undefined;
        var parenthospid = undefined;
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {


            hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
            hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            parenthospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        }
        if (param.phonenumber.toString().length > 9) {
            statecode = parseFloat(param.phonenumber.toString().substring(0, 4));
            areacode = parseFloat(param.phonenumber.toString().substring(4, 10));
            hasPhone = true;
        }
        var params = {
            "USRType": "DOCTOR",
            USRId: param.doctorid,
            "doctorDetails": {
                "FirstName": param.name,
                "FullName": param.name,
                "LastName": "",
                "Gender": param.gender,
                "TermsAccepted": "N",
                "ReferredBy": param.doctorid,
                "Prospect": "Y"
            }
        };
        if (param.speciality != null) {
            params.doctorDetails.SpecialityCD = {
                1: param.speciality
            };
        }
        params.HospitalDetails = {};

        params.HospitalDetails.HospitalID = hospid;
        params.HospitalDetails.HospitalCD = hospcd;
        params.HospitalDetails.ParentHospitalID = parenthospid;

        if (hasPhone) {
            params.DoctorPhone = [{
                "PhAreaCD": areacode,
                "FullNumber": param.phonenumber,
                "PhCntryCD": 91,
                "PhNumber": param.phonenumber,
                "PhType": "M",
                "PrimaryIND": "Y",
                "PhStateCD": statecode
            }];
        }
        if (param.email != "") {
            params.DoctorEmail = [{
                "EmailID": param.email,
                "EmailIDType": 'P',
                "PrimaryIND": "Y"
            }];
        }
        if (hospid == undefined) {
            delete params.HospitalDetails.HospitalID;

        }
        if (hospcd == undefined) {
            delete params.HospitalDetails.HospitalCD;

        }
        if (parenthospid == undefined) {
            delete params.HospitalDetails.ParentHospitalID;

        }
        if (hospid == undefined && hospcd == undefined && parenthospid == undefined) {
            delete params.HospitalDetails;
        }
        $http.post(hcueServiceUrl.addDoctor, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.addConsultDoctor = function (param, callbackSuccess, callbackError) {
        //console.log(JSON.stringify(param));
        $http.post(hcueServiceUrl.addConsultDoctor, param, header)
            .success(function (data) {
                //console.log(JSON.stringify(data));
                //console.log('asdfasd');
                callbackSuccess(data);
            })
            .error(function (data) {
                //console.log('asdf');
                callbackError(data);
            });

    }
    this.updateConsultDoctor = function (param, callbackSuccess, callbackError) {
        //console.log(JSON.stringify(param));
        $http.post(hcueServiceUrl.updateDoctor, param, header)
            .success(function (data) {
                //console.log(JSON.stringify(data));
                //console.log('asdfasd');
                callbackSuccess(data);
            })
            .error(function (data) {
                //console.log('asdf');
                callbackError(data);
            });

    }
    this.AddhcueDoctorReferral = function (param, SelectedDoctoritem, callbackSuccess, callbackError) {
        console.log(JSON.stringify(SelectedDoctoritem));
        console.log(JSON.stringify(doctorinfo));
        var hospid = undefined;
        var hospcd = undefined;
        var parenthospid = undefined;
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {


            hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
            hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            parenthospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        }


        var paramhcue = {
            DoctorID: param,
            USRType: "DOCTOR",
            USRId: param,
            hospitalRecord: { HospitalID: hospid, HospitalCD: hospcd, ParentHospitalID: parenthospid, MyNetworkDoctorID: SelectedDoctoritem.DoctorID, PrimaryIND: "Y", ActiveIND: "Y" }
        };

        if (hospid == undefined) {
            delete paramhcue.hospitalRecord.HospitalID;
        }
        if (hospcd == undefined) {
            delete paramhcue.hospitalRecord.HospitalCD;
        }
        if (parenthospid == undefined) {
            delete paramhcue.hospitalRecord.ParentHospitalID;
        }
        //console.log(hcueServiceUrl.hcueDoctor);
        //console.log(JSON.stringify(paramhcue));


        $http.post(hcueServiceUrl.hcueDoctor, paramhcue, header)

            .success(function (data) {
                console.log(JSON.stringify(data));
                callbackSuccess(data);
            })
            .error(function (data) {
                console.log(JSON.stringify(data));
                callbackError(data);
            });
    };
    this.InactDoctorReferral = function (param, SelectedDoctoritem, ActiveDoc, callbackSuccess, callbackError) {
        console.log(JSON.stringify(SelectedDoctoritem));
        var hospid = undefined;
        var hospcd = undefined;
        var parenthospid = undefined;
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {


            hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
            hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            parenthospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        }
        var activeflag = ActiveDoc;
        if (activeflag != "N") {
            activeflag = "Y";
        }
        var paramhcue = {
            DoctorID: param,
            USRType: "DOCTOR",
            USRId: param,
            hospitalRecord: { HospitalID: hospid, HospitalCD: hospcd, ParentHospitalID: parenthospid, MyNetworkDoctorID: SelectedDoctoritem.DoctorID, PrimaryIND: "Y", ActiveIND: activeflag }
        };
        //console.log(hcueServiceUrl.hcueDoctor);
        //console.log(JSON.stringify(paramhcue));


        if (hospid == undefined) {
            delete paramhcue.hospitalRecord.HospitalID;
        }
        if (hospcd == undefined) {
            delete paramhcue.hospitalRecord.HospitalCD;
        }
        if (parenthospid == undefined) {
            delete paramhcue.hospitalRecord.ParentHospitalID;
        }
        console.log(JSON.stringify(paramhcue));
        $http.post(hcueServiceUrl.hcueDoctor, paramhcue, header)

            .success(function (data) {
                console.log(JSON.stringify(data));
                callbackSuccess(data);
            })
            .error(function (data) {
                console.log(JSON.stringify(data));
                callbackError(data);
            });
    };
    this.doctorReferralDelete = function (param, SelectedDoctoritem, ActiveDoc, callbackSuccess, callbackError) {
        console.log(JSON.stringify(SelectedDoctoritem));
        var hospid = undefined;
        var hospcd = undefined;
        var parenthospid = undefined;
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {


            hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
            hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            parenthospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        }
        var activeflag = ActiveDoc;

        var paramhcue = {
            DoctorID: param,
            USRType: "DOCTOR",
            USRId: param,
            hospitalRecord: { HospitalID: hospid, HospitalCD: hospcd, ParentHospitalID: parenthospid, MyNetworkDoctorID: SelectedDoctoritem.DoctorID, PrimaryIND: "Y", ActiveIND: activeflag }
        };
        //console.log(hcueServiceUrl.hcueDoctor);
        //console.log(JSON.stringify(paramhcue));


        if (hospid == undefined) {
            delete paramhcue.hospitalRecord.HospitalID;
        }
        if (hospcd == undefined) {
            delete paramhcue.hospitalRecord.HospitalCD;
        }
        if (parenthospid == undefined) {
            delete paramhcue.hospitalRecord.ParentHospitalID;
        }
        console.log(JSON.stringify(paramhcue));
        $http.post(hcueServiceUrl.hcueDoctor, paramhcue, header)

            .success(function (data) {
                console.log(JSON.stringify(data));
                callbackSuccess(data);
            })
            .error(function (data) {
                console.log(JSON.stringify(data));
                callbackError(data);
            });
    };
    this.AddParnter = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.addpartners, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    //PatientIdGeneration
    this.PatientIdGenerationValue = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.patientIDGenaration, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.PatientGenerationIds = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.patientIDGenaration, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    //Sarayu offline code
    this.InvoiceGenerationIds = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.patientIDGenaration, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.BillGenerationIds = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.patientIDGenaration, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.PatientIdlookup = function (hospitalId, callbackSuccess, callbackdbError) {
        var url = hcueServiceUrl.patientIdgenlookup + "/" + hospitalId + "/lookup"
        $http.get(url, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.familyIDGenaration = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.FamilyIDGenaration, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
}])

/* Loyalty Offers - Start */

hCueDoctorWebServices.service('hCueLoyaltyOfferServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    this.addOffer = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addLoyaltyOffer, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.UpdateOffer = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.updateLoyaltyOffers, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

}]);

hCueDoctorWebServices.service('hCueLoyaltyMembershipServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    this.addMembership = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addLoyaltyMembership, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.UpdateMembership = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.updateLoyaltyMembership, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

}]);


hCueDoctorWebServices.service('hcueBillingService', ['$http', 'hcueServiceUrl', 'hcueOfflineServiceUrl', function ($http, hcueServiceUrl, hcueOfflineServiceUrl) {

    this.SaveDoctorProcedure = function (doctorID, procedureID, procedureCost, procedureName, notes, callbackSuccess, callbackError) {

        var request = {
            DoctorID: doctorID,
            USRType: "DOCTOR",
            USRId: doctorID,
            ProcedureCost: procedureCost,
            ProcedureName: procedureName,
            DefaultNotes: notes,
            ActiveIND: "Y",
            "USRId": doctorID,
            "ParentProcedureID": procedureID

        };
        //console.log("Add"+JSON.stringify(request))

        $http.post(hcueServiceUrl.addupdateBillingProcedure, request, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });


    }

    this.getOfflineConsutationInfo = function (offlineDBID, callbackSuccess, callbackError) {
        $http.get(localStorage.getItem("offlineURL") + '/data/' + offlineDBID + '/' + hcueOfflineServiceUrl.docAddAppointment + '?includeid=1')
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    // This Block is used to get the Avaliable Offers -- PK
    this.BillingOffer = function (hospitalID, parentHospitalID, hospitalCD, doctorID, callbackSuccess, callbackError) {

        var request = {
            HospitalID: hospitalID,
            ParentHospitalID: parentHospitalID,
            HospitalCD: hospitalCD,
            USRType: "DOCTOR",
            USRId: doctorID
        };

        $http.post(hcueServiceUrl.listBillingOffers, request, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });


    }

    // This Block is used to get the Avaliable Membership 
    this.BillingMembership = function (hospitalID, parentHospitalID, hospitalCD, doctorID, callbackSuccess, callbackError) {

        var request = {
            HospitalID: hospitalID,
            ParentHospitalID: parentHospitalID,
            HospitalCD: hospitalCD,
            USRType: "DOCTOR",
            USRId: doctorID
        };

        $http.post(hcueServiceUrl.listLoyaltyMembership, request, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });


    }

    this.saveOfflinePostBillingInfo = function (DbId, consultData, callbackSuccess, callbackError) {

        $http.post(localStorage.getItem("offlineURL") + '/data/' + DbId, consultData, header);
    }

    this.UpdateDoctorProcedure = function (doctorID, procedureID, procedureCost, procedureName, notes, callbackSuccess, callbackError) {

        var request = {
            DoctorID: doctorID,
            USRType: "DOCTOR",
            ProcedureID: procedureID,
            USRId: doctorID,
            ProcedureCost: procedureCost,
            ProcedureName: procedureName,
            DefaultNotes: notes,
            ActiveIND: "Y",
            "USRId": doctorID

        };
        //console.log(JSON.stringify(request))

        $http.post(hcueServiceUrl.addupdateBillingProcedure, request, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });


    }

    this.GetDoctorProcedure = function (doctorID, hospid, callbackSuccess, callbackError) {


        var params = {
            "DoctorID": doctorID,
            "HospitalCD": hospid,
            "USRType": "DOCTOR",
            "PageSize": 500,
            "PageNumber": 1,
            "USRId": doctorID,
            "StartsWith": "",
            "ProcedureType": "ALL",
            "Active": 'Y'
        }
        var rtnData = [];
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ?
            $http.post(hcueServiceUrl.listDoctorProcedure, params, header) :
            $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorID + '/' + hcueOfflineServiceUrl.listDoctorProcedure, params);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            })
        return rtnData;


    }

    this.InactivateActivateDoctorProcedure = function (doctorID, procedureID, procedureCost, procedureName, notes, activeflag, callbackSuccess, callbackError) {

        var request = {
            DoctorID: doctorID,
            USRType: "DOCTOR",
            USRId: doctorID,
            ProcedureID: procedureID,
            ProcedureCost: procedureCost,
            ProcedureName: procedureName,
            DefaultNotes: notes,
            ActiveIND: activeflag,
            "USRId": doctorID,
            "ParentProcedureID": procedureID

        };

        $http.post(hcueServiceUrl.addupdateBillingProcedure, request, header)
            .success(function (data) {
                callbackSuccess(data);
                //console.log("--------------Inactivate success ----------");
                //console.log(JSON.stringify(data));
            })
            .error(function (data) {
                callbackError(data);
                //console.log("--------------Inactivate failed ----------");
                //console.log(JSON.stringify(data));
            });
    }


    this.getDoctorProcedureListing = function (params, hospid, callbackSuccess, callbackError) {

        if (hospid !== undefined) {

            params.HospitalCD = hospid;
            console.log(JSON.stringify(params));
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listDoctorProcedure, params, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.listDoctorProcedure, params);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        } else {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listDoctorProcedure, params, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.listDoctorProcedure, params);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        };
    }


    //new search billing
    this.getDoctorSearchProcedure = function (params, callbackSuccess, callbackError) {



        console.log(JSON.stringify(params));
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listDoctorProcedure, params, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.listDoctorProcedure, params);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });


    }



    this.addBillingProcedure = function (params, hospid, callbackSuccess, callbackError) {
        // var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
        if (hospid !== undefined) {
            params.HospitalCD = hospid;
            //console.log("21312");
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addupdateBillingProcedure, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addupdateBillingProcedure, params);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        } else {
            //console.log("1212");
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addupdateBillingProcedure, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addupdateBillingProcedure, params);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        }
    }


    /* Service Methods for Doctor Medicine Settings */

    this.listDoctorMedicineSetting = function (params, callbackSuccess, callbackError) {


        $http.post(hcueServiceUrl.listDoctorMedicineSetting, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }

    this.addUpdateDoctorMedicineSetting = function (params, callbackSuccess, callbackError) {


        $http.post(hcueServiceUrl.addUpdateDoctorMedicine, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }
    this.addStockItems = function (params, callbackSuccess, callbackError) {

        console.log(JSON.stringify(params));
        $http.post(hcueServiceUrl.addStockItems, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }
    //Sarayu offline code
    // membership details listMembershipDetails

    this.listMembershipDetails = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listMembershipDetails, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.addMembershipDetails = function (params, callbackSuccess, callbackError) {

        console.log(JSON.stringify(params));
        $http.post(hcueServiceUrl.addMembershipDetails, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }
    this.addupdateTimeslots = function (params, callbackSuccess, callbackError) {


        $http.post(hcueServiceUrl.addupdateTimeslots, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }
    //listAddedTimeSlots
    this.listAddedTimeSlots = function (params, callbackSuccess, callbackError) {


        $http.post(hcueServiceUrl.listAddedTimeSlots, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }
    //addUpdateTimeSheets
    this.addUpdateTimeSheets = function (params, callbackSuccess, callbackError) {


        $http.post(hcueServiceUrl.addUpdateTimeSheets, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }
    this.listAddedTimeSheets = function (params, callbackSuccess, callbackError) {


        if (params.FromDate == "" || params.FromDate == null && params.ToDate == "" || params.ToDate == null) {
            delete params.FromDate;
            delete params.ToDate;
        }
        console.log(JSON.stringify(params));
        $http.post(hcueServiceUrl.listAddedTimeSheets, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }

    this.getPatienBillingHistory = function (params, callbackSuccess, callbackError) {


        $http.post(hcueServiceUrl.getPatienBillingHistory, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }
    this.getListBillingHistory = function (params, packageListSuccess, packageListError) {


        $http.post(hcueServiceUrl.getListBillingHistory, params, header)
            .success(function (data) {
                packageListSuccess(data);
            })
            .error(function (data) {
                packageListError(data);
            });

    }
    this.addBillingTemplate = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addBillingTemplate, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }
    this.updateBillingTemplate = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.UpdateBillingTemplate, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }
    this.listBillingTemplate = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getListBillingHistory, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }

    this.listDoctorProcedureSubCatagory = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listDoctorProcedureSubCatagory, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.addUpdateProcedureSubCatagory = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addUpdateProcedureSubCatagory, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
}]);

hCueDoctorWebServices.service('hcueVitalsForDoctor', ['$http', 'hcueServiceUrl', 'hcueOfflineServiceUrl',
    function ($http, hcueServiceUrl, hcueOfflineServiceUrl) {

        var doctorid = doctorinfo.doctorid;

        this.getDentalHistoryDetailsForDoctor = function (docId, hospitalId) {


            var dentalParams = {
                "DoctorID": doctorid,
                "StartsWith": "",
                "USRType": "DOCTOR",
                "PageSize": 75,
                "PageNumber": 1,
                "USRId": doctorid,
                "SpecialityCD": "DNT",
                "HospitalCD": hospitalId
            };
            return $http.post(hcueServiceUrl.readDoctorHistorySetting, dentalParams, header).success(function (data) { });
        }

        this.getMedicalHistoryDetailsForDoctor = function (docId, hospitalId) {

            //  var hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;

            var medicalParams = {
                "DoctorID": docId,
                "StartsWith": "",
                "USRType": "DOCTOR",
                "PageSize": 100,
                "PageNumber": 1,
                "USRId": doctorid,
                "SpecialityCD": "ALL",
                "HospitalCD": hospitalId
            };
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readDoctorHistorySetting, medicalParams, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readDoctorHistorySetting, medicalParams, header);

            return offlinePost.success(function (data) { });


            /* else
             {
                 var medicalParams = {
                         "DoctorID": doctorid,
                         "StartsWith": "",
                         "USRType": "DOCTOR",
                         "PageSize": 100,
                         "PageNumber": 1,
                         "USRId": doctorid,
                         "SpecialityCD": "ALL"
                 };
                 var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readDoctorHistorySetting, medicalParams, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readDoctorHistorySetting, medicalParams, header);

                 return offlinePost.success(function (data) {
                 });

              }*/ //commented by Senthil.S
        };

        this.getPatientVitalsByDoctorId = function (callback, callbackError) {
            var hospid = undefined;
            if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode) {
                hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            }
            var vitalParams = {
                "DoctorID": doctorinfo.doctorid,
                "USRType": "DOCTOR",
                "PageSize": 100,
                "PageNumber": 1,
                "HospitalCD": hospid,
                "USRId": doctorinfo.doctorid
            };
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readVitals, vitalParams, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readVitals, vitalParams);
            return offlinePost.success(function (data) {
                callback(data);
            }).error(function (data) { callbackError(data); });


            /*  else
              {
                      var vitalParams = {"DoctorID": doctorinfo.doctorid,
                      "USRType": "DOCTOR",
                      "PageSize": 100,
                      "PageNumber": 1,
                      "USRId": doctorinfo.doctorid};
                      var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readVitals, vitalParams, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readVitals, vitalParams);
                      return offlinePost
                           .success(function (data) {
                               callback(data);
                           }).error(function (data) { callbackError(data); });
              }*/ //commented by Senthil.S
        };




    }
]);

hCueDoctorWebServices.service('hcueVitalService', ['$http', 'hcueServiceUrl', 'hcueDoctorLoginService', 'hcueOfflineServiceUrl', function ($http, hcueServiceUrl, hcueDoctorLoginService, hcueOfflineServiceUrl) {

    var doctorinfo1 = hcueDoctorLoginService.getDoctorInfo();

    var doctorid = doctorinfo1.doctorid;
    //console.log(doctorid);

    this.readAvailableTreatmentsByDoctorId = function () {

        return $http.get(hcueServiceUrl.getTreatmentsByDoctorId + '/' + doctorid);

    };


    this.updateTreatments = function (values) {
        var request = {
            DoctorID: doctorid,
            USRType: "DOCTOR",
            USRId: doctorid,
            ProcedureID: values.ProcedureID,
            ProcedureCost: values.ProcedureCost,
            ProcedureName: values.ProcedureName,
            DefaultNotes: values.DefaultNotes,
            ActiveIND: values.ActiveIND,
            TreamentName: values.ProcedureName,
            TreamentID: values.ProcedureID
        };
        return $http.post(hcueServiceUrl.updateTreatmentByDoctorId, request, header).success(function (data) {
            //  //console.log(JSON.stringify(data));
        });
    };

    this.updateDoctorVitalsSettings = function (values) {
        var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
        var index = 0;
        var arrayEachValue = [];
        if (values !== null) {
            angular.forEach(values, function (value1, key) {
                index++;
                arrayEachValue.push({ "DefaultOrder": index, "VitalID": value1.vitId, "ActiveIND": value1.vtlActive });
            });
        } else {
            alert("Null");
        }

        //var vitalValuesDoctor = {"DoctorID":doctorid,"USRType":"DOCTOR","USRId":doctorid,"VitalInfo":arrayEachValue};


        if (doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            var hospid = doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;

            var vitalValuesDoctor = { "DoctorID": doctorid, "USRType": "DOCTOR", "USRId": doctorid, "HospitalCD": hospid, "VitalInfo": arrayEachValue };
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addUpdateDoctorVitalSetting, vitalValuesDoctor, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addUpdateDoctorVitalSetting, vitalValuesDoctor, header);

            return offlinePost.success(function (data) {

            });
        } else {
            var vitalValuesDoctor = { "DoctorID": doctorid, "USRType": "DOCTOR", "USRId": doctorid, "VitalInfo": arrayEachValue };
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addUpdateDoctorVitalSetting, vitalValuesDoctor, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addUpdateDoctorVitalSetting, vitalValuesDoctor, header);

            return offlinePost.success(function (data) {

            });
        }
    };

    this.getPatientVitals = function () {

        if (doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            var hospid = doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            var vitalParams = { "DoctorID": doctorid, "USRType": "DOCTOR", "PageSize": 100, "PageNumber": 1, "HospitalCD": hospid, "USRId": doctorid };
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readVitals, vitalParams, hospid, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readVitals, vitalParams, hospid);
            return offlinePost.success(function (data) {
                //  //console.log(JSON.stringify(data.rows));
            });

        } else {
            var vitalParams = { "DoctorID": doctorid, "USRType": "DOCTOR", "PageSize": 100, "PageNumber": 1, "USRId": doctorid };
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readVitals, vitalParams, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readVitals, vitalParams);
            return offlinePost.success(function (data) {

            });
        }
    };
    this.getPatientVitalsByDoctorId = function (id) {
        if (doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            var hospid = doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            var vitalParams = { "DoctorID": doctorid, "USRType": "DOCTOR", "PageSize": 100, "PageNumber": 1, "HospitalCD": hospid, "USRId": doctorid };
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readVitals, vitalParams, hospid, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readVitals, vitalParams, hospid);
            return offlinePost.success(function (data) {
                //  //console.log(JSON.stringify(data.rows));
            });

        } else {
            var vitalParams = { "DoctorID": doctorid, "USRType": "DOCTOR", "PageSize": 100, "PageNumber": 1, "USRId": doctorid };
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readVitals, vitalParams, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readVitals, vitalParams);
            return offlinePost.success(function (data) {

            });
        }
        //return $http.get(hcueServiceUrl.readVitalsByDoctorId+'/'+doctorid);
    };


    this.getMedicalHistoryDetails = function () {
        if (doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            var hospid = doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            var trtmntParams = { "DoctorID": doctorid, "StartsWith": "", "HospitalCD": hospid, "USRType": "DOCTOR", "PageSize": 100, "PageNumber": 1, "USRId": doctorid, "SpecialityCD": "ALL" };
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readDentalDetailsByDoctorId, trtmntParams, hospid, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readDentalDetailsByDoctorId, trtmntParams, hospid);
            return offlinePost.success(function (data) {
                //  //console.log(JSON.stringify(data.rows));
            });

        } else {
            var trtmntParams = { "DoctorID": doctorid, "StartsWith": "", "USRType": "DOCTOR", "PageSize": 100, "PageNumber": 1, "USRId": doctorid, "SpecialityCD": "ALL" };
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readDentalDetailsByDoctorId, trtmntParams, hospid, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readDentalDetailsByDoctorId, trtmntParams, hospid);
            return offlinePost.success(function (data) {

            });


        }
        //return $http.get(hcueServiceUrl.readDentalDetailsByDoctorId+'/'+doctorid+'/'+'ALL');
    };

    /*this.readDoctorHistorySettingMedicalByDoctorId=function(){
         return $http.get(hcueServiceUrl.readDoctorHistorySetting+'/'+doctorid+'/'+'ALL');
    }*/
    this.readDoctorHistorySettingMedicalByDoctorId = function () {
        //   return $http.get(hcueServiceUrl.readDoctorHistorySetting+'/'+doctorid+'/'+'ALL');


        if (doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            var hospid = doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            var trtmntParams = { "DoctorID": doctorid, "StartsWith": "", "HospitalCD": hospid, "USRType": "DOCTOR", "PageSize": 100, "PageNumber": 1, "USRId": doctorid, "SpecialityCD": "ALL" };
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readDoctorHistorySetting, trtmntParams, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readDoctorHistorySetting, trtmntParams, header);

            return offlinePost.success(function (data) {
                //  //console.log(JSON.stringify(data.rows));
            });

        } else {
            var trtmntParams = { "DoctorID": doctorid, "StartsWith": "", "USRType": "DOCTOR", "PageSize": 100, "PageNumber": 1, "USRId": doctorid, "SpecialityCD": "ALL" };
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readDoctorHistorySetting, trtmntParams, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readDoctorHistorySetting, trtmntParams, header);

            return offlinePost.success(function (data) {

            });

        }
    };

    this.readDoctorHistorySettingDentalByDoctorId = function (pagecount, pagesize) {
        if (doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            var hospcd = doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            var trtmntParams = {
                "DoctorID": doctorid,
                "StartsWith": "",
                "HospitalCD": hospcd,
                "USRType": "DOCTOR",
                "PageSize": pagesize,
                "PageNumber": pagecount,
                "USRId": doctorid,
                "SpecialityCD": "DNT"
            };
            return $http.post(hcueServiceUrl.readDoctorHistorySetting, trtmntParams, header).success(function (data) { });
        } else {
            var trtmntParams = {
                "DoctorID": doctorid,
                "StartsWith": "",
                "USRType": "DOCTOR",
                "PageSize": pagesize,
                "PageNumber": pagecount,
                "USRId": doctorid,
                "SpecialityCD": "DNT"
            };
            return $http.post(hcueServiceUrl.readDoctorHistorySetting, trtmntParams, header).success(function (data) { });
        }
        //   return $http.get(hcueServiceUrl.readDoctorHistorySetting+'/'+doctorid+'/'+'DNT');
    };

    //Dental Chart & treatements

    this.readAvailableTreatmentsByDoctorIdInConsultation = function () {
        var hospcd = undefined;
        if (doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo &&
            doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode) {
            var hospcd = doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        }
        var trtmntParams = {
            "DoctorID": doctorid,
            "USRType": "DOCTOR",
            "PageSize": 500,
            "PageNumber": 1,
            "USRId": doctorid,
            "HospitalCD": hospcd,
            "StartsWith": "",
            "ProcedureType": "TREATMENT"
        };
        return $http.post(hcueServiceUrl.getTreatmentsByDoctorId, trtmntParams, header).success(function (data) {

        });


    };

    this.readAvailableConditionsByDoctorId = function (pageno, startwith, TreatmentpageSize, val) {

        var comepleteList = "N";
        if (val == 0) {
            comepleteList = "Y";
        }

        var trtmntParams = {
            "DoctorID": doctorid,
            "USRType": "DOCTOR",
            "PageSize": TreatmentpageSize,
            "PageNumber": pageno,
            "USRId": doctorid,
            "StartsWith": startwith,
            "SpecialityCD": "DNT",
            "CompleteLst": comepleteList,
            "Active": 'Y'
        };

        if (doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined &&
            doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            var hospcd = doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            trtmntParams.HospitalCD = hospcd;

        }

        return $http.post(hcueServiceUrl.getConditionsByDoctorId, trtmntParams, header).success(function (data) { });


    };

    this.updateDentalHistoryDetails = function (values, pagesize, pagecount) {

        var arrayEachValue = [];
        if (values !== null) {
            angular.forEach(values, function (value1, key) {
                arrayEachValue.push({
                    "Description": value1.Description,
                    "HistoryID": value1.HistoryID,
                    "ActiveIND": value1.ActiveIND,
                    "DefaultOrder": 1,
                    "IsPrivate": "Y"
                });
            });
        } else {
            alert("Null");
        }
        if (doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            var hospcd = doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            var dentalSettingHistory = {
                "DoctorID": doctorid,
                "HospitalCD": hospcd,
                "USRType": "DOCTOR",
                "PageSize": pagesize,
                "PageNumber": pagecount,
                "USRId": doctorid,
                "HistoryInfo": arrayEachValue,
                "StartsWith": "",
                "SpecialityCD": "DNT"
            };
            return $http.post(hcueServiceUrl.addDoctorHistorySetting, dentalSettingHistory, header).success(function (data) { });
        } else {
            var dentalSettingHistory = {
                "DoctorID": doctorid,
                "USRType": "DOCTOR",
                "PageSize": pagesize,
                "PageNumber": pagecount,
                "USRId": doctorid,
                "HistoryInfo": arrayEachValue,
                "StartsWith": "",
                "SpecialityCD": "DNT"
            };
            return $http.post(hcueServiceUrl.addDoctorHistorySetting, dentalSettingHistory, header).success(function (data) { });
        }
    };
    this.readAvailableConditionsByDoctorId = function (pageno, startwith, TreatmentpageSize, val) {
        var comepleteList = "";
        if (val == 0) {
            comepleteList = "Y";
        } else {
            comepleteList = "N";
        }
        //return $http.post(hcueServiceUrl.getConditionsByDoctorId+'/'+doctorid+'/'+'DNT');
        if (doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            var hospcd = doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            var trtmntParams = {
                "DoctorID": doctorid,
                "USRType": "DOCTOR",
                "HospitalCD": hospcd,
                "PageSize": TreatmentpageSize,
                "PageNumber": pageno,
                "USRId": doctorid,
                "StartsWith": startwith,
                "SpecialityCD": "DNT",
                "CompleteLst": comepleteList,
                "Active": 'Y'
            };
            console.log(JSON.stringify(trtmntParams));
            return $http.post(hcueServiceUrl.getConditionsByDoctorId, trtmntParams, header).success(function (data) { });
        } else {
            var trtmntParams = {
                "DoctorID": doctorid,
                "USRType": "DOCTOR",
                "PageSize": TreatmentpageSize,
                "PageNumber": pageno,
                "USRId": doctorid,
                "StartsWith": startwith,
                "SpecialityCD": "DNT",
                "CompleteLst": comepleteList,
                "Active": 'Y'
            };
            //console.log(JSON.stringify(trtmntParams));
            return $http.post(hcueServiceUrl.getConditionsByDoctorId, trtmntParams, header).success(function (data) { });
        }
    };

    this.updateConditionDetailsByDoctorId = function (values, pagenumber, startswith, conditionsPageSize) {

        if (doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            var hospcd = doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            var conditionDetails = {
                "DoctorID": doctorid,
                "HospitalCD": hospcd,
                "Condition": values,
                "USRType": "DOCTOR",
                "PageSize": conditionsPageSize,
                "PageNumber": pagenumber,
                "USRId": doctorid,
                "StartsWith": startswith,
                "SpecialityCD": "DNT",
                "Active": 'Y'
            };
            return $http.post(hcueServiceUrl.addUpdateCondition, conditionDetails, header).success(function (data) {

            });
        } else {
            var conditionDetails = {
                "DoctorID": doctorid,
                "Condition": values,
                "USRType": "DOCTOR",
                "PageSize": conditionsPageSize,
                "PageNumber": pagenumber,
                "USRId": doctorid,
                "StartsWith": startswith,
                "SpecialityCD": "DNT",
                "Active": 'Y'
            };
            return $http.post(hcueServiceUrl.addUpdateCondition, conditionDetails, header).success(function (data) {

            });
        }
    };
    this.updateMedicalHistoryDetails = function (values) {

        var index = 0;
        var arrayEachValue = [];
        if (values !== null) {
            angular.forEach(values, function (value1, key) {
                index++;

                arrayEachValue.push({
                    "Description": value1.Description,
                    "DefaultOrder": index,
                    "HistoryID": value1.HistoryID,
                    "ActiveIND": value1.ActiveIND,
                    "IsPrivate": "Y"
                });
            });
        } else {
            alert("Null");
        }


        if (doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            var hospid = doctorinfo1.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            var medicalSettingHistory = { "DoctorID": doctorid, "HospitalCD": hospid, "USRType": "DOCTOR", "USRId": doctorid, "HistoryInfo": arrayEachValue, "SpecialityCD": "ALL", "PageNumber": 1, "PageSize": 100 };

            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addDoctorHistorySetting, medicalSettingHistory, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addDoctorHistorySetting, medicalSettingHistory);
            return offlinePost.success(function (data) {

            });
        } else {
            var medicalSettingHistory = { "DoctorID": doctorid, "USRType": "DOCTOR", "USRId": doctorid, "HistoryInfo": arrayEachValue, "SpecialityCD": "ALL", "PageNumber": 1, "PageSize": 100 };


            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addDoctorHistorySetting, medicalSettingHistory, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addDoctorHistorySetting, medicalSettingHistory);
            return offlinePost.success(function (data) {

            });
        }
    };

    /*** Consent Form ***/
    this.consentFormImageUploadInSettings = function (params, hospcd, onupdatesuccess, onupdateerror) {
        if (hospcd !== undefined) {
            params.HospitalCD = hospcd;
            $http.post(hcueServiceUrl.consentFormUploadInSettings, params, imageheader).then(function successCallback(response) {

                onupdatesuccess(response);
            }, function errorCallback(response) {
                onupdateerror(response);
            });
        } else {
            $http.post(hcueServiceUrl.consentFormUploadInSettings, params, imageheader).then(function successCallback(response) {

                onupdatesuccess(response);
            }, function errorCallback(response) {
                onupdateerror(response);
            });
        }

    };

    this.addDoctorConsentFormInsetting = function (params, onupdatesuccess, onupdateerror) {

        $http.post(hcueServiceUrl.addDoctorConsentForm, params, imageheader).then(function successCallback(response) {

            onupdatesuccess(response);
        }, function errorCallback(response) {
            onupdateerror(response);
        });

        //console.log(JSON.stringify(params));
    };
    this.getDoctorsConsentForm = function (params, hospcd, onupdatesuccess, onupdateerror) {
        if (hospcd !== undefined) {
            params.HospitalCD = hospcd;
            $http.post(hcueServiceUrl.getDoctorsConsentFormList, params, header).then(function successCallback(response) {
                onupdatesuccess(response);
            }, function errorCallback(response) {
                onupdateerror(response);
            });
        } else {
            $http.post(hcueServiceUrl.getDoctorsConsentFormList, params, header).then(function successCallback(response) {
                onupdatesuccess(response);
            }, function errorCallback(response) {
                onupdateerror(response);
            });
        }
    };
    this.GetDoctorConsentFormDownload = function (params, hospcd, onupdatesuccess, onupdateerror) {
        if (hospcd !== undefined) {
            params.HospitalCD = hospcd;

            console.log(JSON.stringify(params));
            $http.post(hcueServiceUrl.GetDoctorConsentFormDownload, params, header).then(function successCallback(response) {
                onupdatesuccess(response);
            }, function errorCallback(response) {
                onupdateerror(response);
            });
        } else {
            console.log(JSON.stringify(params));
            $http.post(hcueServiceUrl.GetDoctorConsentFormDownload, params, header).then(function successCallback(response) {
                onupdatesuccess(response);
            }, function errorCallback(response) {
                onupdateerror(response);
            });
        }
    };

    this.GetPatientConsentFormDownload = function (params, onupdatesuccess, onupdateerror) {
        $http.post(hcueServiceUrl.GetPatientConsentFormDownload, params, header).then(function successCallback(response) {
            onupdatesuccess(response);
        }, function errorCallback(response) {
            onupdateerror(response);
        });
    };
    this.UpdatePatientSignedConsentFroms = function (params, onupdatesuccess, onupdateerror) {
        $http.post(hcueServiceUrl.UpdatePatientSignedConsentFroms, params, header).then(function successCallback(response) {
            onupdatesuccess(response);
        }, function errorCallback(response) {
            onupdateerror(response);
        });
    };
    /*** End Consent Form ***/


}]);



hCueDoctorWebServices.service('hcueAuthenticationService', ['$rootScope', '$http', 'datapersistanceFactory', 'hcueServiceUrl', 'hcueDoctorDataService', 'hcueDoctorLoginService', 'hcueLoginStatusService', 'datacookieFactory', 'hcueOfflineServiceUrl', function ($rootScope, $http, datapersistanceFactory, hcueServiceUrl, hcueDoctorDataService, hcueDoctorLoginService, hcueLoginStatusService, datacookieFactory, hcueOfflineServiceUrl) {


    this.AuthenticateWithStoredCredential = function (callbackPopulateUser, callbackSuccess, callbackError) {
        var userid = "",
            password = "",
            rememberme = false;

        if (datapersistanceFactory.getItem("rememberme") == true) {

            var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
            userLoginDetails = datacookieFactory.getItem('loginData');
            if (userLoginDetails.length > 0) {
                userid = userLoginDetails.userId;
                password = userLoginDetails.password;
                rememberme = userLoginDetails.rememberme;

                callbackPopulateUser(userid, password);
                AuthenticateUser(userid, password, rememberme, callbackSuccess, callbackError);
            } else {
                callbackError("", 0);
            }
        } else {
            callbackError("", 0);
        }
    }

    this.RemoveStoredCredential = function () {
        ClearStoredCredential();
    }
    this.RemoveProcessingCookies = function () {
        datapersistanceFactory.remove('medicineData');
        datacookieFactory.remove('labtests');
        datacookieFactory.remove('vitalnoteinfo');
        datacookieFactory.remove('whichTabIamin');
        datacookieFactory.remove('billingInfo');
        datacookieFactory.remove('labprepration');
        datacookieFactory.remove('prescriptionVisitRsn');
        datacookieFactory.remove('prescriptionnotessymptom');
        datacookieFactory.remove('prescriptionnotesInvestgation');
        datacookieFactory.remove('prescriptionnotesdisgnosis');
        datacookieFactory.remove('NotesPrint');
        datacookieFactory.remove('prescriptiondoctornotes');
        datacookieFactory.remove('OpthalTextBoxes');
        datacookieFactory.remove('OpthalchkBoxes');
        datacookieFactory.remove('PreMedicalHistory');
        // datacookieFactory.remove('PreviousBillingData');

    }
    this.RemovePreviousBillingandAppointmentInfo = function () {

        datacookieFactory.remove('PreviousBillingData');
        datacookieFactory.remove('StartAppointmentInfo');
        datacookieFactory.remove('PreMedicalHistory');
        var data = [];
        hcueDoctorLoginService.addBillingInfo(data);

    }

    function ClearStoredCredential() {

        datapersistanceFactory.remove('patientsearchparam');
        datapersistanceFactory.remove('register');
        datapersistanceFactory.remove('doctorinfo');
        datapersistanceFactory.remove('patientinfo');
        datapersistanceFactory.remove('appointmentinfo');
        datapersistanceFactory.remove('medicinetemplates');

        datapersistanceFactory.remove('labtemplates');
        datapersistanceFactory.remove("lookup");
        datapersistanceFactory.remove("doctorProfileImage");
        datapersistanceFactory.remove("consultation");
        datapersistanceFactory.remove('medicineData');
        //Sarayu offline code
        datapersistanceFactory.remove('MembershipDiscount');


        datacookieFactory.remove('loginData');
        datacookieFactory.remove('medicineData');
        datacookieFactory.remove('labtests');
        datacookieFactory.remove('vitalnoteinfo');
        datacookieFactory.remove('whichTabIamin');
        datacookieFactory.remove('billingInfo');
        datacookieFactory.remove('labprepration');
        datacookieFactory.remove('prescriptionVisitRsn');
        datacookieFactory.remove('prescriptionnotessymptom');
        datacookieFactory.remove('prescriptionnotesInvestgation');
        datacookieFactory.remove('prescriptionnotesdisgnosis');
        datacookieFactory.remove('NotesPrint');
        datacookieFactory.remove('prescriptiondoctornotes');
        datacookieFactory.remove('OpthalTextBoxes');
        datacookieFactory.remove('OpthalchkBoxes');
        datacookieFactory.remove('offlineAppointmentDtls');
        datacookieFactory.remove('PreMedicalHistory');
    }

    this.gethcueDoctors = function (name, doctorid, onhcuedocsucess, onhcuedocerror) {
        doctorinfo = hcueDoctorLoginService.getDoctorInfo();
        var hospid = undefined;
        var hospcd = undefined;
        var parenthospid = undefined;
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {


            hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
            hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            parenthospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        }
        var Hospital_CD = hospcd;
        var Hospital_ID = hospid;
        var ParentHospital_ID = parenthospid;
        var request = { HospitalID: Hospital_ID, HospitalCD: Hospital_CD, ParentHospitalID: ParentHospital_ID, PageSize: 20, PageNumber: 1, FullName: name, MyCareID: doctorid };
        //console.log(JSON.stringify(request));
        if (hospid == undefined && hospcd == undefined && parenthospid == undefined) {

            delete request.HospitalID;
            delete request.HospitalCD;
            delete request.ParentHospitalID;
        }
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.gethcueDoctors, request, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.gethcueDoctors, request);
        offlinePost.success(function (data) {
            onhcuedocsucess(data);
        })
            .error(function (data) {
                alert("err");
                onhcuedocerror(data);
            });
    }
    //this.getapplistdoctors = function(name,doctorid,onhcuedocsucess,onhcuedocerror)
    //{
    //    var request = {PageSize:20,PageNumber:1,HospitalID:""};
    //    if()
    //    //console.log(JSON.stringify(request));
    //    var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.gethcueDoctors, request, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.gethcueDoctors, request);
    //    offlinePost.success(function (data) {
    //        onhcuedocsucess(data);
    //    })
    //   .error(function (data) {
    //       alert("err");
    //       onhcuedocerror(data);
    //   });
    //}

    this.AuthenticateDoctor = function (userid, password, rememberme, callbackSuccess, callbackError) {
        AuthenticateUser(userid, password, rememberme, callbackSuccess, callbackError);
    }

    this.SignupDoctor = function (params, callbackSuccess, callbackError) {
        //AuthenticateUser(userid, password, rememberme, callbackSuccess, callbackError);
        SignupNewUser(params, callbackSuccess, callbackError);
    }

    this.ChangeDoctorPassword = function (doctorId, doctoruserid, currentpassword, newpassword, callbackSuccess, callbackError) {
        var request = {
            USRType: "DOCTOR",
            USRId: doctorId,
            DoctorLoginID: doctoruserid,
            DoctorCurrentPassword: currentpassword,
            DoctorNewPassword: newpassword
        };

        $http.post(hcueServiceUrl.changeDoctorPassword, request, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }

    function AuthenticateUser(userid, password, rememberme, callbackSuccess, callbackError) {

        var getDoctorCollection = hcueDoctorLoginService.getDoctorInfo();
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.validateDoctorLogin, { "DoctorLoginID": userid, "DoctorPassword": password, "LoggedIn": "Y" }, header) : $http.post(localStorage.getItem("offlineURL") + '/' + hcueOfflineServiceUrl.validateDoctorLogin, { "LoginInfo": { "userid": userid, "password": password, "LoggedIn": "Y" }, "profile": getDoctorCollection.doctordata });
        offlinePost.success(function (data) {


            if (!JSON.parse(localStorage.getItem("isAppOnline"))) {
                if (data.status == true) {
                    var data = data.profile; //getDoctorCollection.doctordata; //tmpdata;
                    console.log(JSON.stringify(data));
                    //console.log(data.doctor)
                    if (data.doctor != null) {
                        if (rememberme) {

                            UpdateLocalStore(userid, password, rememberme);
                            datacookieFactory.set('rememberUser', userid);
                            datacookieFactory.set('rememberme', rememberme);

                        } else {
                            console.log("else");
                            // ClearStoredCredential();
                            UpdateLocalStore(userid, password, rememberme);
                            datacookieFactory.remove("rememberUser");
                            datacookieFactory.remove("rememberme");

                        }
                        hcueDoctorDataService.PopulateLookupInfoOnLoad();

                        var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
                        console.log("else 1");
                        userLoginDetails.HospitalID = "";
                        if (data.doctorAddress !== undefined) {
                            console.log("else 2");
                            angular.forEach(data.doctorAddress, function (item, key) {
                                if (item.ExtDetails.HospitalID && userLoginDetails.HospitalID == "") {
                                    userLoginDetails.HospitalID = item.ExtDetails.HospitalID;
                                    userLoginDetails.GroupID = item.ExtDetails.GroupID;
                                    userLoginDetails.PrimaryIND = item.ExtDetails.PrimaryIND;
                                    userLoginDetails.RoleID = item.ExtDetails.RoleID;
                                }
                            });
                        }
                        datacookieFactory.set('loginData', userLoginDetails);
                        UpdateLocalStore(userid, password, rememberme); // this is done anyway and will not remove if cookie of rememberme is true while log out.
                        callbackSuccess(data);
                    } else {
                        callbackError(data, 1);
                    }
                    //})
                }
            } else {
                if (data.doctor != null) {
                    if (rememberme) {

                        UpdateLocalStore(userid, password, rememberme);
                        datacookieFactory.set('rememberUser', userid);
                        datacookieFactory.set('rememberme', rememberme);

                    } else {
                        ////console.log("else");
                        // ClearStoredCredential();
                        UpdateLocalStore(userid, password, rememberme);
                        datacookieFactory.remove("rememberUser");
                        datacookieFactory.remove("rememberme");

                    }
                    hcueDoctorDataService.PopulateLookupInfoOnLoad();

                    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
                    userLoginDetails.HospitalID = "";
                    if (data.doctorAddress !== undefined) {
                        angular.forEach(data.doctorAddress, function (item, key) {
                            if (item.ExtDetails.HospitalID && userLoginDetails.HospitalID == "") {
                                userLoginDetails.HospitalID = item.ExtDetails.HospitalID;
                                userLoginDetails.GroupID = item.ExtDetails.GroupID;
                                userLoginDetails.PrimaryIND = item.ExtDetails.PrimaryIND;
                                userLoginDetails.RoleID = item.ExtDetails.RoleID;
                            }
                        });
                    }
                    datacookieFactory.set('loginData', userLoginDetails);
                    UpdateLocalStore(userid, password, rememberme); // this is done anyway and will not remove if cookie of rememberme is true while log out.

                    getDoctorCollection = data;

                    var isOfflineEnabled = "N";
                    if (getDoctorCollection.DoctorSetting !== undefined && getDoctorCollection.DoctorSetting.isOfflineEnabled !== undefined) {
                        isOfflineEnabled = getDoctorCollection.DoctorSetting.isOfflineEnabled;
                    }
                    console.log(JSON.stringify(data.DoctorSetting));
                    var Alldoctorlist = [];
                    if (JSON.parse(localStorage.getItem("isAppOnline"))) {
                        if (isOfflineEnabled == "Y") {
                            $http.post(localStorage.getItem("offlineURL") + '/configure/' + data.doctor[0].DoctorID + '/DOCTOR', { "logininfo": { "doctorloginid": userid, "doctorpassword": password }, "profile": data });
                            //$http.get(localStorage.getItem("offlineURL") + '/sync/DOWNSTREAM');
                            callbackSuccess(data);

                        } else {
                            callbackSuccess(data);

                        }
                    } else {
                        callbackError(data, 1);
                    }
                } else {
                    callbackError(data, 1);
                }
            }
        })
            .error(function (data) {
                if (!JSON.parse(localStorage.getItem("isAppOnline"))) {
                    callbackError(data, 1);
                } else {
                    callbackError(data, 2);
                }
            });
    }

    this.LoadAllDocIntoDB = function (callbackSuccess, callbackError) {
        var AdminHosptilId = 0;
        var AdminUsrId = 0;
        var isOfflineEnabled = "N";

        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined) {
            AdminHosptilId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
            AdminUsrId = doctorinfo.doctordata.doctor[0].DoctorID;

        }
        if (doctorinfo.DoctorSetting !== undefined && doctorinfo.DoctorSetting.isOfflineEnabled !== undefined) {

            isOfflineEnabled = doctorinfo.DoctorSetting.isOfflineEnabled;

        }
        var Alldoctorlist = [];
        var Alldocparams = {
            "HospitalID": AdminHosptilId,
            "USRId": AdminUsrId,
            "USRType": "ADMIN"
        }
        $http.post(hcueServiceUrl.getDoctorsOffline, Alldocparams, header)
            .success(function (data) {
                angular.forEach(data, function (alldoclist) {

                    Alldoctorlist.push(alldoclist);

                })
                console.log(JSON.stringify(Alldoctorlist));
                angular.forEach(Alldoctorlist, function (alldoclist) {

                    var setValKey;
                    if (alldoclist.doctor[0].Password !== undefined) {
                        setValKey = alldoclist.doctor[0].Password;
                    } else {
                        setValKey = '';
                    }
                    if (isOfflineEnabled == "Y") {
                        $http.post(localStorage.getItem("offlineURL") + '/configure/' + alldoclist.doctor[0].DoctorID + '/DOCTOR', { "logininfo": { "doctorloginid": alldoclist.doctor[0].DoctorLoginID, "doctorpassword": setValKey }, "profile": alldoclist });

                    }

                })
            })
            .error(function (data) {
                callbackError(data);
            });

        //var offlineAlldocPost =  $http.post(hcueServiceUrl.getDoctorsOffline,Alldocparams,header);
        //offlineAlldocPost.success(function (alldocdata) {
        //    //alldoctorSuccess(data);
        //    angular.forEach(alldocdata,function(alldoclist)
        //    {

        //        Alldoctorlist.push(alldoclist);

        //    })
        //    console.log(JSON.stringify(Alldoctorlist));   
        //    angular.forEach(Alldoctorlist,function(alldoclist)
        //    {

        //        console.log(JSON.stringify(alldoclist.doctor[0].DoctorID ));
        //        console.log(JSON.stringify(alldoclist.doctor[0].DoctorLoginID ));
        //        $http.post(localStorage.getItem("offlineURL") + '/configure/' + alldoclist.doctor[0].DoctorID + '/DOCTOR', { "logininfo": { "doctorloginid": alldoclist.doctor[0].DoctorLoginID, "doctorpassword": '' }, "profile": alldoclist });

        //    })
        // });
    }


    function SignupNewUser(params, callbackSuccess, callbackError) {
        //  alert(JSON.stringify(params));
        $http.post(hcueServiceUrl.addDoctor, params, header)
            .success(function (data) {
                if (data.doctor != null) {
                    callbackSuccess(data);

                } else {
                    callbackError(data, 1);
                }
            })
            .error(function (data) {
                callbackError(data, 2);
            });
    }

    function UpdateLocalStore(userid, password, rememberme) {
        // datapersistanceFactory.set("username", userid);
        //datapersistanceFactory.set("password", password);
        //datapersistanceFactory.set("rememberme", rememberme);

        var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
        userLoginDetails.userId = userid;
        userLoginDetails.password = password;
        userLoginDetails.rememberme = rememberme;
        userLoginDetails.setStatus = "true";
        userLoginDetails.loggedinStatus = "true";
        datacookieFactory.set('loginData', userLoginDetails);


    }
    this.getDoctorImageInfo = function (item, docid, docaddressid, callbackSuccess, callbackError) {

        var request = {
            DoctorID: docid,
            ImageName: item,
            AddressID: docaddressid

        };

        $http.post(hcueServiceUrl.GetClinicImage, request, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getDoctorcareerImageInfo = function (item, docid, callbackSuccess, callbackError) {
        var request = {
            DoctorID: docid,
            ImageName: item
        };

        $http.post(hcueServiceUrl.GetCareerImage, request, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }



    this.updateDoctorSettings = function (param, doctorid, callbackSuccess) {
        var url = hcueServiceUrl.updateDoctorSettings;

        var requestParam = {
            "USRType": "DOCTOR",
            "doctorDetails": {
                "DoctorID": 0,
                "TermsAccepted": "N"
            },
            "DoctorSettings": {},
            "USRId": 0
        };

        requestParam.doctorDetails.DoctorID = doctorid;
        requestParam.USRId = doctorid;
        requestParam.DoctorSettings = param;

        //console.log(JSON.stringify(requestParam));


        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.updateDoctor, requestParam, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorid + '/' + hcueOfflineServiceUrl.updateDoctor, requestParam);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        });
    }
    this.GetlistHcueRole = function (callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.getlistHcueRole, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    //LastLogintime
    this.lastSignoutTime = function (Param, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.lastLogin, Param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }
    this.updateOfflineLogin = function (params, callbacksuccess, callbackerror) {
        $http.post(localStorage.getItem("offlineURL") + '/' + hcueOfflineServiceUrl.loginStatus, params, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    }
}])

hCueDoctorWebServices.service('hcuePatientCaseService', ['$http', '$filter', 'hcueServiceUrl', 'hcueDoctorLoginService', 'hcueDoctorAppointmentService', 'hcueOfflineServiceUrl', function ($http, $filter, hcueServiceUrl, hcueDoctorLoginService, hcueDoctorAppointmentService, hcueOfflineServiceUrl) {

    //ellam arun code
    this.savevitals = function (bplow, bphigh, sugarpp, sugarfast, Temp, height, weight, bloodgroup, doctorid, callbackSuccess, callbackError) {

        var param = {
            USRType: "PATIENT",
            PatientID: parseInt(hcueDoctorLoginService.getDocPatientID()),
            USRId: doctorid,
            VitalInfo: { Vitals: { BPL: bplow, SPP: sugarpp, SFT: sugarfast, BPH: bphigh, TMP: Temp, HGT: height, WGT: weight, BLG: bloodgroup, SCT: SCT, LPP: LPP, SRM: SRM, HCF: hcf, REPRT: Resp, SPO2: Spo2, PEFR: Pefr } },
            USRId: doctorid,
            PatientID: parseInt(hcueDoctorLoginService.getDocPatientID()),
            USRType: "ADMIN"

        };

        $http.post(hcueServiceUrl.addPatientvitals, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }
    //end of ellam arun code

    //package//

    this.AddBillingTemplate = function (param, pakageSuccess, pakageError) {
        $http.post(hcueServiceUrl.addBillingTemplate, param, header)
            .success(function (data) {
                pakageSuccess(data);
                console.log(JSON.stringify(data));
            })
            .error(function (data) {
                pakageError(data);
            });

    }

    this.UpdateBillingTemplate = function (param, pakageSuccess, pakageError) {
        $http.post(hcueServiceUrl.UpdateBillingTemplate, param, header)
            .success(function (data) {
                pakageSuccess(data);
                console.log(JSON.stringify(data));
            })
            .error(function (data) {
                pakageError(data);
            });

    }

    this.getCity = function (callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.getcityvalues, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.AddReferredDocList = function (param, callbackSuccess, callbackError) {
        console.log("Pat1" + JSON.stringify(param));
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listPatientCase, param, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + param.PatientID + '/' + hcueOfflineServiceUrl.listPatientCase, param);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {

                callbackError(data);
            })
    }

    this.GetLabDocumentsDownload = function (params, onupdatesuccess, onupdateerror) {
        $http.post(hcueServiceUrl.GetLabDocumentsDownload, params, header).then(function successCallback(response) {
            onupdatesuccess(response);
        }, function errorCallback(response) {
            onupdateerror(response);
        });
    };

    this.GetPatientsbyId = function (patientid, callbackSuccess, callbackError) {
        var url = hcueServiceUrl.getPatientbyId + "/" + patientid + "/getPatient";

        var rtnData = [];
        if (patientid !== undefined) {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.get(url, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + patientid + '/' + hcueOfflineServiceUrl.getPatient);
        }


        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            })
        return rtnData;

    }
    this.getPatientCaseConsentForm = function (params, onupdatesuccess, onupdateerror) {
        $http.post(hcueServiceUrl.getPatientCaseConsentFormList, params, header).then(function successCallback(response) {
            onupdatesuccess(response);
        }, function errorCallback(response) {
            onupdateerror(response);
        });
    };

    var doctorid = doctorinfo.doctorid;
    this.getLabNamesForDoctor = function (doctid) {
        return $http.get(hcueServiceUrl.getLabnames + "/" + doctid.toString() + "/list");
    };

    this.getTypeVarietyShadeForLab = function (doctorid, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getTypeVarietyShadeForLab, { DoctorID: doctorid, "USRType": "DOCTOR", "USRId": doctorid }, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.updateaudionotes = function (params, onupdatesuccess, onupdateerror) {
        $http.post(hcueServiceUrl.addDrawing, params, imageheader)
            .then(function successCallback(response) {
                onupdatesuccess(response);
            }, function errorCallback(response) {
                onupdateerror(response);
            });
    };
    this.updateprogressaudionotes = function (params, param, onupdatesuccess, onupdateerror) {
        $http.post(hcueServiceUrl.addDrawing, params, imageheader)
            .then(function successCallback(response) {
                onupdatesuccess(response, param);
            }, function errorCallback(response) {
                onupdateerror(response);
            });
    };
    this.updateImgnotes = function (params, param, onupdatesuccess, onupdateerror) {
        $http.post(hcueServiceUrl.addDrawing, params, imageheader)
            .then(function successCallback(response) {
                onupdatesuccess(response, param);
            }, function errorCallback(response) {
                onupdateerror(response);
            });
    };
    this.deleteaudionotes = function (params, onupdatesuccess, onupdateerror) {
        $http.post(hcueServiceUrl.deleteAudioNotes, params, imageheader).then(function successCallback(response) {
            onupdatesuccess(response);
        }, function errorCallback(response) {
            onupdateerror(response);
        });
    };
    this.AddPatientAppointmentImmediate = function (starttime, endtime, doctorid, daycd, consultdate, patientid, addressconsultid, callbackSuccess, callbackError) {
        hcueDoctorAppointmentService.AddImmediateApointment(starttime, endtime, doctorid, daycd, consultdate, patientid, addressconsultid, callbackSuccess, callbackError);
    };
    this.AddPatientAppointment = function (starttime, endtime, doctorid, daycd, consultdate, patientid, addressconsultid, visitreason, VisitRsnType, Sendemail, callbackSuccess, callbackError) {
        hcueDoctorAppointmentService.AddApointment(starttime, endtime, doctorid, daycd, consultdate, patientid, addressconsultid, visitreason, VisitRsnType, Sendemail, callbackSuccess, callbackError);
    };
    this.UpdatePatientAppointment = function (starttime, endtime, doctorid, daycd, consultdate, patientid, addressid, appointmentid, visitreason, VisitRsnType, sendSms, sendMail, callbackSuccess, callbackError) {
        hcueDoctorAppointmentService.UpdateAppointment(starttime, endtime, doctorid, daycd, consultdate, patientid, addressid, appointmentid, visitreason, VisitRsnType, sendSms, sendMail, callbackSuccess, callbackError);
    };

    this.UpdateStartedPatientAppointment = function (starttime, endtime, doctorid, daycd, consultdate, patientid, addressid, appointmentid, visitreason, VisitRsnType, sendSms, sendMail, callbackSuccess, callbackError) {
        var param = {
            "AppointmentID": appointmentid,
            "AddressID": addressid,
            "DayCD": daycd,
            "ConsultationDt": consultdate,
            "StartTime": starttime,
            "EndTime": endtime,
            "PatientID": patientid,
            "OtherVisitRsn": visitreason,
            "AppointmentStatus": "S",
            "DoctorID": doctorid,
            "USRType": "PATIENT",
            "USRId": patientid,
            "DoctorVisitRsnID": VisitRsnType,
            "VisitUserTypeID": "OPATIENT",
            "SendNotification": {
                "sendPatientSMS": sendSms,
                "sendPatientEmail": sendMail,
                "sendDoctorReminderSms": "Y",
                "sendDoctorPushNotification": "Y"
            },

        };
        //$http.post(hcueServiceUrl.updtDoctorAppointments, param, header)
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addUpdateAppointment, param, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.updatePatientAppointments, param, header);
        offlinePost.success(function (data) {
            if (data != null) {
                callbackSuccess(data, status);
            }
        })
            .error(function (data) {
                callbackerror(data, status);
            });
    };

    this.UpdateAppointmentStauts = function (appointmentid, doctorid, callbackSuccess, callbackError) {

        var param = { AppointmentID: appointmentid, AppointmentStatus: "S", USRType: "DOCTOR", USRId: doctorid };
        //console.log("123");
        if (JSON.parse(localStorage.getItem("isAppOnline"))) {
            var offlinePost = $http.post(hcueServiceUrl.updtDoctorAppointments, param, header);
            offlinePost.success(function (data) {
                if (data != null) {
                    callbackSuccess(data, status);
                }
            })
                .error(function (data) {
                    callbackError(data, status);
                });
        }
    }

    this.getBillinginfoForAppointmentID = function (PatientID, docid, caseId, appointmentID, callbackSuccess, callbackError) {
        console.log(JSON.stringify(PatientID));
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getBillingInfo, { "PageSize": 20, "PageNumber": 1, "PatientID": PatientID, "DoctorID": docid, "AppointmentID": appointmentID }, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + PatientID + '/' + hcueOfflineServiceUrl.getBillingInfo);
        offlinePost.success(function (data) {

            callbackSuccess(data);
        })
            .error(function (data) {
                console.log(JSON.stringify(data));
                callbackError(data);
            })
    }


    this.getHospitalbillinginfo = function (PatientID, docid, HospitalCode, callbackSuccess, callbackError) {

        var param = {
            "PatientID": PatientID,
            "DoctorID": docid,
            "PageNumber": 1,
            "HospitalCD": HospitalCode,
            "getHospitalBilling": "Y",
            "PageSize": 20
        }

        $http.post(hcueServiceUrl.getBillingInfo, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    //this.getBillinginfoForAppointmentID = function (PatientID, callback, callbackError) {
    //    var urlget = hcueServiceUrl.getBillingInfoReceptionist +"/"+ PatientID ;
    //    ////console.log('header'+header);
    //    $http.get(urlget, header)
    //        .success(function (data) {
    //        callback(data);
    //    })
    //        .error(function (data) { callbackError(data); });
    //}



    this.getPatientAllBillingInfo = function (caseId, docid, patientcaseid, appointmentID, appointmentStatus, callbackSuccess, callbackError) {
        // console.log(JSON.stringify(docid));

        if (appointmentID == 0) {
            var param = { "PageSize": 20, "PageNumber": 1, "PatientID": caseId, "DoctorID": docid };
        } else {
            var param = { "PageSize": 20, "PageNumber": 1, "PatientID": caseId, "DoctorID": docid, "AppointmentID": appointmentID };
        }
        if (patientcaseid !== 0) {
            param.PatientCaseID = patientcaseid;
        }
        if (appointmentStatus == 'P') {
            param.PauseFlag = 'Y';
        }

        if (JSON.parse(localStorage.getItem("isAppOnline"))) {
            var onlineBillPost = $http.post(hcueServiceUrl.getBillingInfo, param, header);
            onlineBillPost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            if (caseId > 0) {
                var offlineBillPost = $http.get(localStorage.getItem("offlineURL") + '/data/' + caseId + '/' + hcueOfflineServiceUrl.getBillingInfo);
                offlineBillPost.success(function (data) {
                    callbackSuccess(data);
                })
                    .error(function (data) {
                        callbackError(data);
                    })
            } else {
                var data = { "message": "No data found!" };
                callbackSuccess(data);
            }

        }


    }

    this.UpdateNurseBillingProcedure = function (sendSms, billingInfo, PaymentInfo, appointmentID, doctorID, appointmentinfo, AppDtls, callbackSuccess, callbackError) {

        var param = {

            "ReceivedAmount": sendSms,

            "DoctorID": doctorID,

            "USRType": "PATIENT",

            "AppointmentID": appointmentID,

            "USRId": doctorID,

            "BillingInfo": [],

            PaymentInfo,
            "addAppointmentDtls": AppDtls.addAppointmentDtls,

        };


        if (billingInfo.length > 0) {
            param.BillingInfo = billingInfo;

        }



        //$http.post(hcueServiceUrl.updateBillingProcedure, param, header)

        if (!(JSON.parse(localStorage.getItem("isAppOnline")))) {
            var tmpDate = $filter('date')(new Date(), "yyyy-MM-dd");
            var tmpOfflineCaseDetails = appointmentinfo; //hcueDoctorLoginService.getAppointmentInfo();
            console.log(JSON.stringify(tmpOfflineCaseDetails));
            console.log(JSON.stringify(param));

            tmpOfflineCaseDetails.OfflineCaseDetail.AuditLog.CaseSummary.PaymentInfo = param.PaymentInfo;
            tmpOfflineCaseDetails.OfflineCaseDetail.AuditLog.CaseSummary.BillingInfo = param.BillingInfo;

            console.log(JSON.stringify(tmpOfflineCaseDetails));
            //var tmpAddedAppointment = hcueDoctorLoginService.getAppointmentInfo();
            var offlinePost = $http.post(hcueOfflineServiceUrl.offlineHostURL + '/data/' + appointmentinfo.offlineDBID, tmpOfflineCaseDetails.OfflineCaseDetail, header);


            //$http.post(hcueOfflineServiceUrl.offlineHostURL + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addAllPatientCase, tmpOfflineCaseDetails, header);
            offlinePost.success(function (data) {
                //console.log("*************Result Success*******");
                var clearOfflineCaseDetails = {
                    "VersionID": 1,
                    "DoctorID": 0,
                    "USRId": 0,
                    "USRType": "DOCTOR",
                    "AuditLog": {
                        "RequestDate": "",
                        "ConsultantDate": "",
                        "RequestBy": "",
                        "ParentHospitalID": 0,
                        "HospitalID": 0,
                        "AddAppointment": {},
                        "UpdateAppointment": {},
                        "AddPatient": {},
                        "UpdatePatient": {},
                        "CaseSummary": {}
                    }
                };


                hcueDoctorLoginService.setOfflineCaseDetails(clearOfflineCaseDetails);
                if (data == "Success") {
                    callbackSuccess(data);
                } else if (data.message !== undefined && data.message == "Save data successfull") {
                    callbackSuccess(data);
                }


            });

        } else {
            var offlinePost = $http.post(hcueServiceUrl.updateBillingProcedure, param, header);
            offlinePost.success(function (data) {

                if (data.ExceptionType == "PSQLException") {
                    callbackError(data);
                } else if (callbackSuccess != null)
                    callbackSuccess(data);
            })
                .error(function (data) {
                    if (callbackError != null)
                        callbackError(data);
                })
        }



    }



    this.UpdateBillingProcedure = function (sendSms, billingInfo, PaymentInfo, appointmentID, doctorID, appointmentinfo, callbackSuccess, callbackError) {

        var param = {

            "DoctorID": doctorID,

            "USRType": "PATIENT",

            "AppointmentID": appointmentID,


            "USRId": hcueDoctorLoginService.getLoginId(),
            "ReceivedAmount": sendSms,
            "BillingInfo": [],

            PaymentInfo

        };


        if (billingInfo.length > 0) {
            param.BillingInfo = billingInfo;

        }



        //$http.post(hcueServiceUrl.updateBillingProcedure, param, header)

        if (!(JSON.parse(localStorage.getItem("isAppOnline")))) {
            var tmpDate = $filter('date')(new Date(), "yyyy-MM-dd");
            var tmpOfflineCaseDetails = appointmentinfo; //hcueDoctorLoginService.getAppointmentInfo();
            console.log(JSON.stringify(tmpOfflineCaseDetails));
            console.log(JSON.stringify(param));
            if (tmpOfflineCaseDetails.OfflineCaseDetail !== undefined && tmpOfflineCaseDetails.OfflineCaseDetail.AuditLog !== undefined) {

                tmpOfflineCaseDetails.OfflineCaseDetail.AuditLog.CaseSummary.PaymentInfo = param.PaymentInfo;
                tmpOfflineCaseDetails.OfflineCaseDetail.AuditLog.CaseSummary.BillingInfo = param.BillingInfo;
            }

            console.log(JSON.stringify(tmpOfflineCaseDetails));
            //var tmpAddedAppointment = hcueDoctorLoginService.getAppointmentInfo();
            var offlinePost = $http.post(localStorage.getItem("offlineURL") + '/data/' + appointmentinfo.offlineDBID, tmpOfflineCaseDetails.OfflineCaseDetail, header);


            //$http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addAllPatientCase, tmpOfflineCaseDetails, header);
            offlinePost.success(function (data) {
                //console.log("*************Result Success*******");
                var clearOfflineCaseDetails = {
                    "VersionID": 1,
                    "DoctorID": 0,
                    "USRId": 0,
                    "USRType": "DOCTOR",
                    "AuditLog": {
                        "RequestDate": "",
                        "ConsultantDate": "",
                        "RequestBy": "",
                        "ParentHospitalID": 0,
                        "HospitalID": 0,
                        "AddAppointment": {},
                        "UpdateAppointment": {},
                        "AddPatient": {},
                        "UpdatePatient": {},
                        "CaseSummary": {}
                    }
                };


                hcueDoctorLoginService.setOfflineCaseDetails(clearOfflineCaseDetails);
                if (data == "Success") {
                    callbackSuccess(data);
                } else if (data.message !== undefined && data.message == "Save data successfull") {
                    callbackSuccess(data);
                }


            });

        } else {
            var offlinePost = $http.post(hcueServiceUrl.updateBillingProcedure, param, header);
            offlinePost.success(function (data) {

                if (data.ExceptionType == "PSQLException") {
                    callbackError(data);
                } else if (callbackSuccess != null)
                    callbackSuccess(data);
            })
                .error(function (data) {
                    if (callbackError != null)
                        callbackError(data);
                })
        }



    }

    this.getOfflineBillingData = function (params, callbackSuccess, callbackError) {
        //HardCoded date 
        $http.get(localStorage.getItem("offlineURL") + '/data/' + params + '/ADDPATIENTCASEDETAIL?created=2016-11-24', header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })

    }

    this.AddPatient = function (patientinfo, callbackSuccess, callbackError) {
        var areacode = 1,
            statecode = 1;

        if (patientinfo.stdCode !== undefined && patientinfo.stdCode != '') {
            areacode = parseFloat(patientinfo.stdCode);
        }

        if (patientinfo.phonenumber.toString().length > 9) {
            statecode = parseFloat(patientinfo.phonenumber.toString().substring(0, 4));
            areacode = parseFloat(patientinfo.phonenumber.toString().substring(4, 10));
        }
        var param = {
            USRType: "DOCTOR",
            USRId: patientinfo.doctorid,
            patientDetails: {
                Title: patientinfo.title,
                FirstName: patientinfo.firstname,
                "DOB": patientinfo.dob,
                Age: parseFloat(patientinfo.age),
                "TermsAccepted": "Y",
                FullName: patientinfo.firstname + " " + patientinfo.lastname,
                Gender: patientinfo.gender,
                FamilyHeadIndicator: "Y",
                MobileID: parseFloat(patientinfo.phonenumber)
            }
        };

        if (patientinfo.hasphone) {
            param.patientPhone = [{ "PhAreaCD": areacode, "PhCntryCD": 91, "PhNumber": parseFloat(patientinfo.phonenumber), "PhType": "M", "PrimaryIND": "Y", "PhStateCD": statecode }];
        }

        if (patientinfo.address1 != "" || patientinfo.address2 != "") {
            param.patientAddress = [{
                Address1: patientinfo.address1,
                Address2: patientinfo.address2,
                AddressType: "H",
                PrimaryIND: "Y",
                PinCode: patientinfo.pincode,
                Street: "",
                "CityTown": patientinfo.citytown,
                Latitude: patientinfo.lat.toString(),
                Longitude: patientinfo.lng.toString(),
                Country: patientinfo.country,
                DistrictRegion: "",
                Location: patientinfo.location,
                State: "TN"
            }];
        }

        if (patientinfo.emailid != "") {
            param.patientEmail = [{ EmailID: patientinfo.emailid, PrimaryIND: "Y", EmailIDType: "O" }];
        }

        //alert(JSON.stringify(param));

        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addPatient, param, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addPatient, param);
        offlinePost.success(function (data) {

            if (data.ExceptionType == "PSQLException") {
                //console.log("------Patient Add Error PSQL ----");
                //console.log(JSON.stringify(data));
                callbackError(data);
            } else if (callbackSuccess != null)
                //console.log("------Patient Add Success ----");
                //console.log(JSON.stringify(data));
                callbackSuccess(data);
        })
            .error(function (data) {
                //alert('up');
                if (callbackError != null)
                    //alert('middle');
                    //console.log("------Patient Add Error ----");
                    //console.log(JSON.stringify(data));
                    callbackError(data);
                //alert('down');
            })
    }

    this.GetPatientWithMobileNumber = function (param, callbackSuccess, callbackError) {
        //alert(JSON.stringify(param));
        GetPatientthroughmobilenumber(param, callbackSuccess, callbackError);
    }


    // Saryu offline code
    this.RegisterPatient = function (patientinfo, hosid, hospid, branchcd, country, state, city, locations, existfamilyid, patientid, AddPatinWalkin = "", callbackSuccess, callbackError) {


        var areacode = 1;
        var statecode = 1;
        if (patientinfo.BloodGroup !== '' && patientinfo.BloodGroup !== undefined) {
            var patientVitals = { "Vitals": { "BLG": patientinfo.BloodGroup } };
        } else {
            var patientVitals = {};
        }
        var param = {
            USRType: "DOCTOR",
            GenIDDetails: { BranchCD: "", HospitalCD: "", DoctorID: 0, HospitalID: "", VIPFlag: "N" },
            patientDetails: {
                EmergencyInfo: { EmailID: "", MobileNumber: "", Name: "", Relationship: "", CntyCD: "" },
                FirstName: "",
                DOB: "",
                TermsAccepted: "Y",
                FullName: "",
                Title: "",
                FamilyHdIND: "",
                FamilyHdID: 0,
                PatientOtherDetails: {
                    MaritalStatus: "",
                    Occupation: "",
                    Education: "",
                    Nationality: "",
                    Preference: {},
                    ReferralSource: {},
                    SpecialNotes: "",
                    IdentityDtls: [],
                    PatientMemberShip: [{ RowID: "", MembershipID: "", ValidityStart: "" }]
                },
                Gender: "M",
                Age: 0,
                MobileID: 0,
                AdditionalInfo: { colour: "", species: "", breed: "", ownername: "", ownernametitle: "" }
            },
            patientEmail: [],
            patientVitals,
            USRId: 0,
            patientAddress: [],
            patientPhone: []
        };


        // param.patientDetails.BloodGroup = patientinfo.BloodGroup;
        param.patientDetails.Gender = patientinfo.gender;
        if (patientinfo.profileImages != undefined && patientinfo.profileImages.length > 0) {
            param.profileImages = patientinfo.profileImages;
        }
        if (patientinfo.emailid !== '' && patientinfo.emailid !== undefined) {
            param.patientEmail.push({ EmailID: patientinfo.emailid, PrimaryIND: "Y", EmailIDType: "P" });
        }
        if (patientinfo.AdharNo !== '' && patientinfo.AdharNo !== undefined) {
            param.patientDetails.PatientOtherDetails.IdentityDtls.push({ ID: "AAD", Value: patientinfo.AdharNo });
        }
        if (patientinfo.PanNo !== '' && patientinfo.PanNo !== undefined) {
            param.patientDetails.PatientOtherDetails.IdentityDtls.push({ ID: "PAN", Value: patientinfo.PanNo });
        }
        if (patientinfo.PassportNo !== '' && patientinfo.PassportNo !== undefined) {
            param.patientDetails.PatientOtherDetails.IdentityDtls.push({ ID: "PPT", Value: patientinfo.PassportNo });
        }
        if (patientinfo.Visastatus !== '' && patientinfo.Visastatus !== undefined) {
            param.patientDetails.PatientOtherDetails.IdentityDtls.push({ ID: "VISA", Value: "0", Status: patientinfo.Visastatus });
        }
        if (patientinfo.familyRelation != '' && patientinfo.familyRelation != undefined) {
            param.patientDetails.FamilyRltnType = patientinfo.familyRelation;
        }
        if (patientinfo.Refvalue !== 0) {
            param.patientDetails.PatientOtherDetails.ReferralID = patientinfo.Refvalue;
        }
        if (patientinfo.SubValue !== 0) {
            param.patientDetails.PatientOtherDetails.SubReferralID = parseInt(patientinfo.SubValue);
        }
        param.USRId = patientinfo.doctorid;
        param.GenIDDetails.BranchCD = branchcd;
        param.GenIDDetails.HospitalCD = hospid;
        param.GenIDDetails.HospitalID = hosid;
        param.GenIDDetails.DoctorID = patientinfo.doctorid;
        param.GenIDDetails.VIPFlag = (patientinfo.vipFlag == true) ? 'Y' : 'N';

        //Vetnery Details included 
        param.patientDetails.AdditionalInfo.colour = patientinfo.colour;
        param.patientDetails.AdditionalInfo.species = patientinfo.species;
        param.patientDetails.AdditionalInfo.breed = patientinfo.breed;
        param.patientDetails.AdditionalInfo.ownername = patientinfo.ownername;
        param.patientDetails.AdditionalInfo.ownernametitle = patientinfo.ownernametitle;

        //param.patientEmail[0].EmailID = patientinfo.emailid;
        param.patientDetails.EmergencyInfo.CntyCD = patientinfo.emgyCntryCode;
        param.patientDetails.EmergencyInfo.MobileNumber = patientinfo.emgymobilenumber;
        param.patientDetails.EmergencyInfo.Relationship = patientinfo.emgyrelationship;
        param.patientDetails.EmergencyInfo.Name = patientinfo.emgyname;
        param.patientDetails.FirstName = patientinfo.firstname;
        param.patientDetails.FullName = patientinfo.firstname;
        param.patientDetails.EmergencyInfo.Title = patientinfo.emgytitle;
        param.patientDetails.DOB = $filter('date')(new Date(patientinfo.dob), "yyyy-MM-dd");
        param.patientDetails.Title = patientinfo.title;
        param.patientDetails.PatientOtherDetails.MaritalStatus = patientinfo.othDetailmaritalstatus;
        param.patientDetails.PatientOtherDetails.Occupation = patientinfo.othDetailoccupation;
        param.patientDetails.PatientOtherDetails.Education = patientinfo.othDetaileducation;
        param.patientDetails.PatientOtherDetails.ReferralSource = patientinfo.othDetailreferralsource;
        param.patientDetails.PatientOtherDetails.SpecialNotes = patientinfo.othrDetailNotes;
        param.patientDetails.PatientOtherDetails.Nationality = patientinfo.Nationality;
        param.patientDetails.PatientOtherDetails.PatientMemberShip = patientinfo.Membershipselected;
        param.patientDetails.Age = parseFloat(patientinfo.CurrentAge);
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined &&
            doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            if (existfamilyid == 'N') {
                param.patientDetails.FamilyHdID = patientid;
                param.patientDetails.FamilyHdIND = "N";

            } else {

                param.patientDetails.FamilyHdIND = "Y";
                param.patientDetails.FamilyHdID = 0;

            }
        }
        param.patientAddress.push({ Address2: patientinfo.Address2, Address1: patientinfo.Address1, LandMark: patientinfo.LandMark, Latitude: patientinfo.Latitude, DistrictRegion: patientinfo.DistrictRegion, Longitude: patientinfo.Longitude, PinCode: (patientinfo.PinCode == "") ? 0 : parseFloat(patientinfo.PinCode), State: (state != undefined) ? state : patientinfo.State, Street: "", Country: (country != undefined) ? country : patientinfo.Country, CityTown: (city != undefined) ? city : patientinfo.CityTown, PrimaryIND: "Y", AddressType: "H", Location: (locations != undefined) ? locations : patientinfo.Location });

        if (param.patientDetails.MobileID == 0) {

            if (patientinfo.patientPhone !== undefined && patientinfo.patientPhone.length > 0) {
                if (patientinfo.patientPhone[0].PhNumber != undefined) {

                    if (patientinfo.patientPhone.length == 1 && patientinfo.patientPhone[0].PhNumber == 0) {
                        param.patientDetails.MobileID = parseFloat("1" + patientinfo.doctorid);
                    } else {
                        param.patientDetails.MobileID = parseFloat(patientinfo.patientPhone[0].PhNumber);
                    }
                }
            } else {
                param.patientDetails.MobileID = parseFloat("1" + patientinfo.doctorid);
            }
        }
        angular.forEach(patientinfo.patientPhone, function (data) {
            if (data.PhNumber != undefined) {
                if (data.PhNumber.length > 5) {

                    if (data.PhNumber.toString().length > 9) {
                        statecode = data.PhNumber.toString().substring(0, 4);
                        areacode = parseFloat(data.PhNumber.toString().substring(4, 10));
                    }
                    if (data.PhType == 'L') {
                        statecode = (data.PhStateCD !== "") ? data.PhStateCD : 1;
                        areacode = (data.PhStateCD !== "") ? parseFloat(data.PhStateCD) : 1;
                    }


                    param.patientPhone.push({
                        PhCntryCD: parseFloat(data.PhCntryCD),
                        PhStateCD: parseFloat(statecode),
                        PhAreaCD: areacode,
                        PhStdCD: (data.PhType == 'M') ? '' : statecode,
                        PhNumber: parseFloat(data.PhNumber),
                        PhType: data.PhType,
                        PrimaryIND: "Y"
                    });
                }
            }
        });

        param.version = "V1";
        //var patWithAppointment =  hcueDoctorLoginService.getAppointmentInfoFrmPatient();
        //var chkPatWithAppointment = false;
        //var pfID = {};
        //if(JSON.parse(localStorage.getItem("isAppOnline")))
        //{
        $http.post(hcueServiceUrl.addPatient, param, header)
            .success(function (patData) {
                if (patData !== null && patData.ExceptionType !== "PSQLException") {
                    callbackSuccess(patData);
                }
            })
            .error(function (patData) {

                callbackError(patData);
            })
        //}
        //else if(!JSON.parse(localStorage.getItem("isAppOnline")) && doctorinfo.doctordata.doctorAddress!== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo!== undefined &&
        //   doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined)
        //{
        //    var offlinePatIDPost =  $http.get(localStorage.getItem("offlineURL") + '/configure/0/' + hcueOfflineServiceUrl.offlinePFID);
        //    offlinePatIDPost.success(function (patData) {



        //                if(patData.message == undefined)
        //                {
        //                    pfID = patData;
        //                    pfID.rows[0].PATID =  pfID.rows[0].PATID + 1;
        //                    param.GenIDDetails.PatientDisplayID = 'o_SAN-'+ pfID.rows[0].PATID;
        //                    if (existfamilyid == 'N') {
        //                        param.patientDetails.FamilyHdID = patientid;
        //                        param.patientDetails.FamilyHdIND = "N";

        //                    }
        //                    else {

        //                        pfID.rows[0].FAMID =  pfID.rows[0].FAMID + 1;
        //                        param.GenIDDetails.FamilyDisplayID ='o_FID-'+ pfID.rows[0].FAMID;

        //                    }
        //                    param.offlineGenIDDetails = pfID;
        //                }


        //            var offlineCaseDetails = hcueDoctorLoginService.getOfflineCaseDetails();
        //            offlineCaseDetails.AuditLog.AddPatient = param;
        //            hcueDoctorLoginService.setOfflineCaseDetails(offlineCaseDetails);
        //            if(patWithAppointment.length > 0 || offlineCaseDetails.AuditLog.AddAppointment.AddressConsultID != undefined)
        //            {
        //                chkPatWithAppointment = true;
        //            }

        //            if(AddPatinWalkin !== "Start Consultation" && !chkPatWithAppointment)
        //            {
        //                var offlinePost = $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addPatient, param);
        //                offlinePost.success(function (data) {

        //                    callbackSuccess(data);

        //                })
        //                .error(function (data) {
        //                    if (callbackError != null)
        //                        callbackError(data);

        //                })
        //            }
        //            else
        //            {
        //                //if (patData !== null && patData.ExceptionType !== "PSQLException") {
        //                //    callbackSuccess(patData);
        //                //}
        //                if(patWithAppointment.length > 0 && AddPatinWalkin !== "Start Consultation")
        //                {
        //                    callbackSuccess("Add Pat in Day");
        //                }
        //                else
        //                {
        //                    callbackSuccess("Offline Walkin");
        //                }
        //            }
        //    })
        //   .error(function (patData) {

        //       callbackError(patData);
        //   })
        //}
        //else if(!JSON.parse(localStorage.getItem("isAppOnline")))
        //{
        //    var offlineCaseDetails = hcueDoctorLoginService.getOfflineCaseDetails();
        //    offlineCaseDetails.AuditLog.AddPatient = param;
        //    param.offlineGenIDDetails = null;
        //    hcueDoctorLoginService.setOfflineCaseDetails(offlineCaseDetails);
        //    if(patWithAppointment.length > 0 || offlineCaseDetails.AuditLog.AddAppointment.AddressConsultID != undefined)
        //    {
        //        chkPatWithAppointment = true;
        //    }
        //    //var milliseconds = new Date().getTime();
        //    //if(AddPatinWalkin !== "Start Consultation" && offlineCaseDetails.AuditLog.AddAppointment.AddressConsultID == undefined)
        //    if(AddPatinWalkin !== "Start Consultation" && !chkPatWithAppointment )
        //    {
        //        var offlinePost = $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addPatient, param);
        //        offlinePost.success(function (data) {

        //            callbackSuccess(data);

        //        })
        //        .error(function (data) {
        //            if (callbackError != null)
        //                callbackError(data);

        //        })
        //    }
        //    else
        //    {
        //        if(patWithAppointment.length > 0 && AddPatinWalkin !== "Start Consultation")
        //        {
        //            callbackSuccess("Add Pat in Day");
        //        }
        //        else
        //        {
        //            callbackSuccess("Offline Walkin");
        //        }

        //    }


        //}
        ////var offlinePatIDPost =   (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addPatient, param, header) : $http.get(localStorage.getItem("offlineURL") + '/configure/0/' + hcueOfflineServiceUrl.offlinePFID);


    }


    this.AddNewPatient = function (patientinfo, hospid, branchcd, callbackSuccess, callbackError) {
        var areacode = 1,
            statecode = 1;


        if (patientinfo.phonenumber.toString().length > 9) {
            statecode = parseFloat(patientinfo.phonenumber.toString().substring(0, 4));
            areacode = parseFloat(patientinfo.phonenumber.toString().substring(4, 10));
        }


        if (patientinfo.phonenumber == "") {
            patientinfo.phonenumber = parseFloat("1" + patientinfo.doctorid);
        }

        var refsrc = {};
        refsrc = patientotherdetails.referralsource;
        ////console.log(JSON.stringify(patientinfo.phonenumber));
        if (hospid !== undefined && branchcd !== undefined) {
            var param = {
                USRType: "DOCTOR",
                USRId: patientinfo.doctorid,
                GenIDDetails: {
                    BranchCD: branchcd,
                    HospitalCD: hospid,
                    DoctorID: patientinfo.doctorid
                },
                patientDetails: {
                    EmergencyInfo: {
                        MobileNumber: emergencycontactinfo.mobilenumber.toString(),
                        Relationship: emergencycontactinfo.relationship,
                        AlternateNumber: emergencycontactinfo.alternatenumber.toString(),

                        Name: emergencycontactinfo.name,
                        Title: emergencycontactinfo.title
                    },
                    PatientOtherDetails: {
                        MaritalStatus: patientotherdetails.maritalstatus,
                        Occupation: patientotherdetails.occupation,
                        Education: patientotherdetails.education,
                        PaymentMode: patientotherdetails.paymentmode,
                        ReferralSource: patientotherdetails.referralsource

                    },


                    Title: patientinfo.title,
                    FirstName: patientinfo.firstname,
                    Age: parseFloat(patientinfo.CurrentAge),
                    TermsAccepted: "Y",
                    FullName: patientinfo.firstname,
                    Gender: patientinfo.gender,
                    FamilyHeadIndicator: "Y",
                    MobileID: parseInt(patientinfo.phonenumber),
                    Title: patientinfo.title
                }

            };

        } else {
            var param = {
                USRType: "DOCTOR",
                USRId: patientinfo.doctorid,
                GenIDDetails: {
                    DoctorID: patientinfo.doctorid
                },
                patientDetails: {
                    EmergencyInfo: {
                        MobileNumber: emergencycontactinfo.mobilenumber.toString(),
                        Relationship: emergencycontactinfo.relationship,
                        AlternateNumber: emergencycontactinfo.alternatenumber.toString(),

                        Name: emergencycontactinfo.name,
                        Title: emergencycontactinfo.title
                    },
                    PatientOtherDetails: {
                        MaritalStatus: patientotherdetails.maritalstatus,
                        Occupation: patientotherdetails.occupation,
                        Education: patientotherdetails.education,
                        PaymentMode: patientotherdetails.paymentmode,
                        ReferralSource: patientotherdetails.referralsource

                    },


                    Title: patientinfo.title,
                    FirstName: patientinfo.firstname,
                    Age: parseFloat(patientinfo.CurrentAge),
                    TermsAccepted: "Y",
                    FullName: patientinfo.firstname,
                    Gender: patientinfo.gender,
                    FamilyHeadIndicator: "Y",
                    MobileID: parseInt(patientinfo.phonenumber),
                    Title: patientinfo.title
                }

            };
        }
        //(patientinfo.phonenumber == null)?0: parseInt(patientinfo.phonenumber)
        if (patientinfo.hasphone && patientinfo.phonenumber != "") {

            param.patientPhone = [{ PhAreaCD: areacode, PhCntryCD: 91, PhNumber: (patientinfo.phonenumber == "") ? 0 : parseInt(patientinfo.phonenumber), PhType: "M", PrimaryIND: "Y", PhStateCD: statecode }];
        }

        if (patientinfo.address1 != "" || patientinfo.address2 != "") {
            param.patientAddress = [{
                Address1: patientinfo.address1,
                Address2: patientinfo.address2,
                AddressType: "H",
                PrimaryIND: "Y",
                PinCode: (patientinfo.pincode == "") ? 0 : parseInt(patientinfo.pincode),
                Street: "",
                CityTown: patientinfo.citytown,
                Latitude: patientinfo.lat,
                Longitude: patientinfo.lng,
                Country: patientinfo.country,
                DistrictRegion: "",
                Location: patientinfo.location,
                State: "TN"
            }];
        }


        if (patientinfo.emailid != "") {
            param.patientEmail = [{ EmailID: patientinfo.emailid, PrimaryIND: "Y", EmailIDType: "P" }];
        }

        //console.log(JSON.stringify(param));

        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addPatient, param, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addPatient, param);
        offlinePost.success(function (data) {

            if (data.ExceptionType == "PSQLException") {
                //console.log("------Patient Add Error PSQL ----");
                //console.log(JSON.stringify(data));
                callbackError(data);
            } else if (callbackSuccess != null)
                //console.log("------Patient Add Success ----");

                if (data.Patient[0].CurrentAge.month < 10) {
                    data.Patient[0].CurrentAge.month = "0" + data.Patient[0].CurrentAge.month;
                    //console.log(JSON.stringify(data));
                }

            callbackSuccess(data);
        })
            .error(function (data) {
                //alert('up');
                if (callbackError != null)
                    //alert('middle');
                    //console.log("------Patient Add Error ----");
                    //console.log(JSON.stringify(data));
                    callbackError(data);
                //alert('down');
            })
    }
    this.UpdatePatientNew = function (patientupdateinfo, state, country, city, locations, hosid, hospid, branchcd, callbackSuccess, callbackError) {



        var areacode = 1,
            statecode = 1;


        if (patientupdateinfo.stdCode !== undefined && patientupdateinfo.stdCode != '') {
            areacode = parseFloat(patientupdateinfo.stdCode);
        }

        if (patientupdateinfo.phonenumber.toString().length > 9) {
            statecode = parseFloat(patientupdateinfo.phonenumber.toString().substring(0, 4));
            areacode = parseFloat(patientupdateinfo.phonenumber.toString().substring(4, 10));
        }
        var doctorid = hcueDoctorLoginService.getLoginId();
        if (patientupdateinfo.bloodGroup !== '' && patientupdateinfo.bloodGroup !== undefined) {
            var patientVitals = { "Vitals": { "BLG": patientupdateinfo.bloodGroup } };
        } else {
            var patientVitals = {};
        }

        if (hospid !== undefined && branchcd !== undefined && hosid !== undefined) {
            var param = {
                USRType: "DOCTOR",
                GenIDDetails: {
                    BranchCD: branchcd,
                    HospitalCD: hospid,
                    DoctorID: doctorid,
                    HospitalID: hosid,
                    VIPFlag: patientupdateinfo.VIPFLAG
                },

                patientDetails: {
                    "Title": patientupdateinfo.title,
                    "FirstName": patientupdateinfo.fullname,
                    "Age": parseFloat(patientupdateinfo.Age),
                    "TermsAccepted": "Y",
                    "FullName": patientupdateinfo.fullname,
                    "PatientID": patientupdateinfo.patientid,
                    "Gender": patientupdateinfo.gender,
                    "FamilyHeadIndicator": "Y",
                    "PatientOtherDetails": {
                        MaritalStatus: patientupdateinfo.MaritalStatus,
                        Occupation: patientupdateinfo.Occupation,
                        Education: patientupdateinfo.Education,
                        Preference: {},
                        ReferralSource: patientupdateinfo.ReferralSource,
                        SpecialNotes: patientupdateinfo.SpecialNotes,
                        IdentityDtls: []
                    },
                    "AdditionalInfo": {
                        "colour": patientupdateinfo.AdditionalInfo.colour,
                        "species": patientupdateinfo.AdditionalInfo.species,
                        "breed": patientupdateinfo.AdditionalInfo.breed,
                        "ownername": patientupdateinfo.AdditionalInfo.ownername,
                        "ownernametitle": patientupdateinfo.AdditionalInfo.ownernametitle
                    },
                    //patientupdateinfo.AdditionalInfo,
                    EmergencyInfo: {
                        MobileNumber: patientupdateinfo.EmgyMobileNumber,
                        Name: patientupdateinfo.EmgyName,
                        Relationship: patientupdateinfo.EmgyRelationship,
                        Title: patientupdateinfo.EmgyTitle,
                        CntyCD: patientupdateinfo.EmgyCntryCd
                    }
                },
                "USRId": doctorid,
                patientAddress: [],
                patientEmail: [],
                patientVitals

            };
            if (patientupdateinfo.hasphone) {

                param = {
                    USRType: "DOCTOR",
                    GenIDDetails: {
                        BranchCD: branchcd,
                        HospitalCD: hospid,
                        DoctorID: doctorid,
                        HospitalID: hosid,
                        VIPFlag: patientupdateinfo.VIPFLAG
                    },
                    patientDetails: {
                        "Title": patientupdateinfo.title,
                        "FirstName": patientupdateinfo.fullname,
                        "Age": parseFloat(patientupdateinfo.Age),
                        "DOB": patientupdateinfo.dob,
                        "TermsAccepted": "Y",
                        "FullName": patientupdateinfo.fullname,
                        "PatientID": patientupdateinfo.patientid,
                        "Gender": patientupdateinfo.gender,
                        "FamilyHeadIndicator": "Y",
                        "PatientOtherDetails": { MaritalStatus: patientupdateinfo.MaritalStatus, Occupation: patientupdateinfo.Occupation, Education: patientupdateinfo.Education, Preference: {}, ReferralSource: patientupdateinfo.ReferralSource, SpecialNotes: patientupdateinfo.SpecialNotes, IdentityDtls: [] },
                        EmergencyInfo: { MobileNumber: patientupdateinfo.EmgyMobileNumber, Name: patientupdateinfo.EmgyName, Relationship: patientupdateinfo.EmgyRelationship, Title: patientupdateinfo.EmgyTitle, CntyCD: patientupdateinfo.EmgyCntryCd }
                    },
                    "USRId": doctorid,
                    patientAddress: [],
                    patientEmail: [],
                    patientVitals,
                    "patientPhone": patientupdateinfo.patientPhnumber
                };
            }
        } else {
            var param = {
                USRType: "DOCTOR",
                GenIDDetails: {
                    DoctorID: doctorid,
                    VIPFlag: patientupdateinfo.VIPFLAG
                },
                patientDetails: {
                    "Title": patientupdateinfo.title,
                    "FirstName": patientupdateinfo.fullname,
                    "Age": parseFloat(patientupdateinfo.Age),
                    "DOB": patientupdateinfo.dob,
                    "TermsAccepted": "Y",
                    "FullName": patientupdateinfo.fullname,
                    "PatientID": patientupdateinfo.patientid,
                    "Gender": patientupdateinfo.gender,
                    "FamilyHeadIndicator": "Y",
                    "PatientOtherDetails": { MaritalStatus: patientupdateinfo.MaritalStatus, Occupation: patientupdateinfo.Occupation, Education: patientupdateinfo.Education, Preference: {}, ReferralSource: patientupdateinfo.ReferralSource, SpecialNotes: patientupdateinfo.SpecialNotes, IdentityDtls: [] },
                    EmergencyInfo: { MobileNumber: patientupdateinfo.EmgyMobileNumber, Name: patientupdateinfo.EmgyName, Relationship: patientupdateinfo.EmgyRelationship, Title: patientupdateinfo.EmgyTitle, CntyCD: patientupdateinfo.EmgyCntryCd }
                },
                "USRId": doctorid,
                patientAddress: [],
                patientEmail: [],
                patientVitals

            };
            if (patientupdateinfo.hasphone) {
                param = {
                    USRType: "DOCTOR",
                    GenIDDetails: {
                        DoctorID: doctorid,
                        VIPFlag: patientupdateinfo.VIPFLAG
                    },
                    patientDetails: {
                        "Title": patientupdateinfo.title,
                        "FirstName": patientupdateinfo.fullname,
                        "Age": parseFloat(patientupdateinfo.Age),
                        "DOB": patientupdateinfo.dob,
                        "TermsAccepted": "Y",
                        "FullName": patientupdateinfo.fullname,
                        "PatientID": patientupdateinfo.patientid,
                        "Gender": patientupdateinfo.gender,
                        "FamilyHeadIndicator": "Y",
                        "PatientOtherDetails": { MaritalStatus: patientupdateinfo.MaritalStatus, Occupation: patientupdateinfo.Occupation, Education: patientupdateinfo.Education, Preference: {}, ReferralSource: patientupdateinfo.ReferralSource, SpecialNotes: patientupdateinfo.SpecialNotes, IdentityDtls: [] },
                        EmergencyInfo: { MobileNumber: patientupdateinfo.EmgyMobileNumber, Name: patientupdateinfo.EmgyName, Relationship: patientupdateinfo.EmgyRelationship, Title: patientupdateinfo.EmgyTitle, CntyCD: patientupdateinfo.EmgyCntryCd }
                    },
                    "USRId": doctorid,
                    patientAddress: [],
                    patientEmail: [],
                    patientVitals,
                    "patientPhone": patientupdateinfo.patientPhnumber
                }
            };
        }

        if (patientupdateinfo.MembershipDetails.length != 0) {
            param.patientDetails.PatientOtherDetails.PatientMemberShip = patientupdateinfo.MembershipDetails;
        }
        if (patientupdateinfo.EmailID !== '' && patientupdateinfo.EmailID !== undefined) {
            param.patientEmail.push({ EmailID: patientupdateinfo.EmailID, PrimaryIND: "Y", EmailIDType: "P" });
        }
        if (patientupdateinfo.Nationality !== '' && patientupdateinfo.Nationality !== undefined) {
            param.patientDetails.PatientOtherDetails.Nationality = patientupdateinfo.Nationality;
        }
        if (patientupdateinfo.AdharNo !== '' && patientupdateinfo.AdharNo !== undefined) {
            param.patientDetails.PatientOtherDetails.IdentityDtls.push({ ID: "AAD", Value: patientupdateinfo.AdharNo });
        }
        if (patientupdateinfo.PanNo !== '' && patientupdateinfo.PanNo !== undefined) {
            param.patientDetails.PatientOtherDetails.IdentityDtls.push({ ID: "PAN", Value: patientupdateinfo.PanNo });
        }
        if (patientupdateinfo.PassportNo !== '' && patientupdateinfo.PassportNo !== undefined) {
            param.patientDetails.PatientOtherDetails.IdentityDtls.push({ ID: "PPT", Value: patientupdateinfo.PassportNo });
        }
        if (patientupdateinfo.Visastatus !== '' && patientupdateinfo.Visastatus !== undefined) {
            param.patientDetails.PatientOtherDetails.IdentityDtls.push({ ID: "VISA", Value: "0", Status: patientupdateinfo.Visastatus });
        }
        if (patientupdateinfo.Refvalue !== 0) {
            param.patientDetails.PatientOtherDetails.ReferralID = patientupdateinfo.Refvalue;
        }
        if (patientupdateinfo.SubValue !== 0) {
            param.patientDetails.PatientOtherDetails.SubReferralID = parseInt(patientupdateinfo.SubValue);
        }
        if (patientupdateinfo.Address2 !== '' && patientupdateinfo.Address2 !== undefined || patientupdateinfo.Address1 !== '' && patientupdateinfo.Address1 !== undefined) {
            param.patientAddress.push({
                Address2: patientupdateinfo.Address2,
                Address1: patientupdateinfo.Address1,
                LandMark: patientupdateinfo.LandMark,
                Latitude: patientupdateinfo.Latitude,
                DistrictRegion: patientupdateinfo.DistrictRegion,
                Longitude: patientupdateinfo.Longitude,
                PinCode: (patientupdateinfo.PinCode == "") ? 0 : parseFloat(patientupdateinfo.PinCode),
                State: (patientupdateinfo.State == undefined) ? "" : patientupdateinfo.State,
                Street: "",
                Country: (patientupdateinfo.Country == undefined) ? "" : patientupdateinfo.Country,
                CityTown: (patientupdateinfo.CityTown == undefined) ? "" : patientupdateinfo.CityTown,
                PrimaryIND: "Y",
                AddressType: "H",
                Location: (patientupdateinfo.Location == undefined) ? "" : patientupdateinfo.Location
            });
        }

        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.updatePatient, param, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.updatePatient, param, header);
        offlinePost.success(function (data) {

            if (data.ExceptionType == "PSQLException") {
                callbackError(data);
            } else if (callbackSuccess != null)
                callbackSuccess(data);
        })
            .error(function (data) {

                if (callbackError != null)
                    callbackError(data);
            })
    }

    this.UpdatePatient = function (patientupdateinfo, hosid, hospid, branchcd, callbackSuccess, callbackError) {

        var areacode = 1,
            statecode = 1;
        //alert(JSON.stringify(patientupdateinfo));

        if (patientupdateinfo.stdCode !== undefined && patientupdateinfo.stdCode != '') {
            areacode = parseFloat(patientupdateinfo.stdCode);
        }

        if (patientupdateinfo.phonenumber.toString().length > 9) {
            statecode = parseFloat(patientupdateinfo.phonenumber.toString().substring(0, 4));
            areacode = parseFloat(patientupdateinfo.phonenumber.toString().substring(4, 10));
        }
        var doctorid = hcueDoctorLoginService.getLoginId();
        if (hospid !== undefined && branchcd !== undefined && hosid !== undefined) {
            var param = {
                USRType: "PATIENT",
                GenIDDetails: {
                    BranchCD: branchcd,
                    HospitalCD: hospid,
                    DoctorID: doctorid,
                    HospitalID: hosid
                },
                patientDetails: {
                    "Title": patientupdateinfo.title,
                    "FirstName": patientupdateinfo.fullname,
                    "Age": parseFloat(patientupdateinfo.Age),
                    "TermsAccepted": "Y",
                    "FullName": patientupdateinfo.fullname,
                    "PatientID": patientupdateinfo.patientid,
                    "Gender": patientupdateinfo.gender,
                    "FamilyHeadIndicator": "Y"
                },
                "USRId": patientupdateinfo.patientid

            };
            if (patientupdateinfo.hasphone) {

                param = {
                    USRType: "PATIENT",
                    GenIDDetails: {
                        BranchCD: branchcd,
                        HospitalCD: hospid,
                        DoctorID: doctorid,
                        HospitalID: hosid
                    },
                    patientDetails: {
                        "Title": patientupdateinfo.title,
                        "FirstName": patientupdateinfo.fullname,
                        "Age": parseFloat(patientupdateinfo.Age),
                        "TermsAccepted": "Y",
                        "FullName": patientupdateinfo.fullname,
                        "PatientID": patientupdateinfo.patientid,
                        "Gender": patientupdateinfo.gender,
                        "FamilyHeadIndicator": "Y"
                    },
                    "USRId": patientupdateinfo.patientid,
                    "patientPhone": [{ "PhAreaCD": areacode, "PhCntryCD": 91, "PhNumber": parseFloat(patientupdateinfo.phonenumber), "PhType": "M", "PrimaryIND": "Y", "PhStateCD": statecode }]
                }
            }
        } else {
            var param = {
                USRType: "PATIENT",
                GenIDDetails: {
                    DoctorID: doctorid
                },
                patientDetails: {
                    "Title": patientupdateinfo.title,
                    "FirstName": patientupdateinfo.fullname,
                    "Age": parseFloat(patientupdateinfo.Age),
                    "TermsAccepted": "Y",
                    "FullName": patientupdateinfo.fullname,
                    "PatientID": patientupdateinfo.patientid,
                    "Gender": patientupdateinfo.gender,
                    "FamilyHeadIndicator": "Y"
                },
                "USRId": patientupdateinfo.patientid

            };

            if (patientupdateinfo.hasphone) {

                param = {
                    USRType: "PATIENT",
                    GenIDDetails: {
                        DoctorID: doctorid
                    },
                    patientDetails: {
                        "Title": patientupdateinfo.title,
                        "FirstName": patientupdateinfo.fullname,
                        "Age": parseFloat(patientupdateinfo.Age),
                        "TermsAccepted": "Y",
                        "FullName": patientupdateinfo.fullname,
                        "PatientID": patientupdateinfo.patientid,
                        "Gender": patientupdateinfo.gender,
                        "FamilyHeadIndicator": "Y"
                    },
                    "USRId": patientupdateinfo.patientid,
                    "patientPhone": [{ "PhAreaCD": areacode, "PhCntryCD": 91, "PhNumber": parseFloat(patientupdateinfo.phonenumber), "PhType": "M", "PrimaryIND": "Y", "PhStateCD": statecode }]
                }
            }
        }

        $http.post(hcueServiceUrl.updatePatient, param, header)
            .success(function (data) {

                if (data.ExceptionType == "PSQLException") {
                    callbackError(data);
                } else if (callbackSuccess != null)

                    callbackSuccess(data);
            })
            .error(function (data) {
                //  alert(JSON.stringify(data));
                if (callbackError != null)
                    callbackError(data);
            })
    }

    this.GetPatientByPhoneNumber = function (patientinfo, callbackSuccess, callbackError) {
        var isAccessset = true;
        var param = {
            PageSize: 30,
            PageNumber: 1,
            AgeOffSet: 10,
            PhoneNumber: "",
            "Sort": "asc",
            LocalSearchFlag: "Y",
            SearchText: "",
            SearchDetails: {
                "listdocpatients": "Y"
            },
            DoctorID: hcueDoctorLoginService.getLoginId()
        }

        var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined &&
            doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            var hospcode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            param.HospitalCD = hospcode;
        }


        if (patientinfo.phonenumber == "" || patientinfo.phonenumber == undefined) {
            if (patientinfo.landlineNumber != '') {
                param.PhoneNumber = (patientinfo.stdNo.toString() + patientinfo.landlineNumber.toString());
                delete param.SearchText;
            } else if (!isNaN(patientinfo.familyid)) {
                param.PhoneNumber = patientinfo.familyid.toString();
                delete param.SearchText;
            } else {
                param.SearchText = patientinfo.familyid;
                delete param.PhoneNumber;
            }

            //param.SearchText = patientinfo.familyid;
        } else {
            console.log(isNaN(param.PhoneNumber));
            param.PhoneNumber = patientinfo.phonenumber.toString();
            delete param.SearchText;

        }

        var access = doctorinfo.isAccessRightsApplicable;
        if (access) {
            if (doctorinfo.AccountAccessID.indexOf('ONLYDOCPAT') == -1) {
                delete param.SearchDetails;
                isAccessset = false;
            }
        }

        if (isAccessset) {
            delete param.PhoneNumber;
            delete param.LocalSearchFlag;
            param.SearchText = patientinfo.phonenumber.toString();
        }
        GetPatientthroughmobilenumber(param, callbackSuccess, callbackError);
    }

    this.GetPatientByPatientID = function (phonenumber, callbackSuccess, callbackError) {

        var offlinePost = $http.get(localStorage.getItem("offlineURL") + '/search/0/' + hcueOfflineServiceUrl.getPatient + '?searchkey=PhNumber&search=' + phonenumber);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });

    }

    this.GetPatientRecentVital = function (patientid) {
        //json strucuture referred in controller InitialiseConsultInfo, anychanges update there also
        var vitals = { temperature: '', height: '', weight: '', sugar: '', sugarpp: '', sugarfast: '', bplow: '', bphigh: '', bloodgroup: '', bp: '', HCF: '' };

        if (patientid == null)
            return vitals;

        var url = hcueServiceUrl.getVitals + "/" + patientid.toString() + "/getPatientVitals"
        $http.get(url, header)
            .success(function (data) {
                if (data != null) {

                    vitals.temperature = (data.Temparature == null) ? "0" : data.Temparature;
                    vitals.height = (data.Height == null) ? "" : data.Height;
                    vitals.weight = (data.Weight == null) ? "" : data.Weight;
                    vitals.sugarpp = (data.SugarPP == null) ? "" : data.SugarPP;
                    vitals.sugarfast = (data.SugarFast == null) ? "" : data.SugarFast;
                    vitals.sugar = (data.SugarFast != "" && data.SugarPP != "") ? data.SugarFast + "/" + data.SugarPP : "";
                    vitals.bp = (data.BPLow != "" && data.BPHigh != "") ? data.BPLow + "/" + data.BPHigh : "";
                    vitals.bplow = (data.BPLow == null) ? "" : data.BPLow;
                    vitals.bphigh = (data.BPHigh == null) ? "" : data.BPHigh;
                    vitals.bloodgroup = data.BloodGroup;
                    vitals.HCF = data.HCF
                }
            })
            .error(function (data) {
                //callbackError(data);
            })
        return vitals;
    }

    this.SearchPatient = function (param, callbackSuccess, callbackError) {
        //alert(JSON.stringify(param));
        GetDoctorPatient(param, callbackSuccess, callbackError);
    }
    this.SearchPatient1 = function (param, callbackSuccess, callbackError, sendhospcode) {
        //alert(JSON.stringify(param));
        GetDoctorPatient1(param, callbackSuccess, callbackError, sendhospcode);
    }
    this.SearchOnlyPatient = function (param, callbackSuccess, callbackError) {
        GetPatient(param, callbackSuccess, callbackError);
    }
    this.SearchOnlyPatientVerson = function (param, callbackSuccess, callbackError) {
        GetPatientVerson(param, callbackSuccess, callbackError);
    }
    this.getTypeVarietyShadeForLab = function (doctorid, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getTypeVarietyShadeForLab, { DoctorID: doctorid, "USRType": "DOCTOR", "USRId": doctorid }, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.AddPatientCasewithMH = function (NewIVFDetail, ConsultationDate, consultationinfo, historyIds, historyNote, offlineID = 0, offlineDBID = 0, pauseVal = 0, callbackSuccess, callbackError) {

        var vitals = consultationinfo.vitalnoteinfo.vitals;
        var symptom = "";
        var diagnostics = "";
        var general = "";
        var doctornotes = "";
        var followup = "";
        var PaymentInfo = consultationinfo.PaymentInfo;
        var sendEmailSMS = consultationinfo.sendEmailSMS;

        if (consultationinfo.vitalnoteinfo.notes) {
            symptom = consultationinfo.vitalnoteinfo.notes.symptom;
            diagnostics = "";
            general = consultationinfo.vitalnoteinfo.notes.general;
            doctornotes = consultationinfo.vitalnoteinfo.notes.doctornotes;
            followup = consultationinfo.vitalnoteinfo.notes.followup;
        }

        //VOICE DOCUMENTS
        var VoiceNotesInSummary = consultationinfo.voiceDocuments.voiceDocumentArray;
        var voiceDcmnts = {};
        for (var i = 0; i < VoiceNotesInSummary.length; i++) {
            var documentidInSummary = VoiceNotesInSummary[i].voice.data;
            voiceDcmnts[i + 1] = documentidInSummary;
        }

        //Case Documents
        var caseDocumentsInSummary = consultationinfo.ImageNotesData.concat(consultationinfo.PdfNotesData);
        var caseDocuments = {};
        for (var i = 0; i < caseDocumentsInSummary.length; i++) {
            var casedocumentidInSummary = caseDocumentsInSummary[i].documentid;
            caseDocuments[i + 1] = casedocumentidInSummary;
        }

        //Estimate Document Upload
        var EstimateDocInSummary = consultationinfo.EstimateData;
        var estdata = consultationinfo.EstimationData;
        var estimateDocmts = {};
        for (var i = 0; i < EstimateDocInSummary.length; i++) {
            angular.forEach(estdata, function (val) {
                if (val.RowID == EstimateDocInSummary[i].planNumber) {
                    var EstInSummary = EstimateDocInSummary[i].documentid;
                    estimateDocmts[i + 1] = EstInSummary;
                }
            });

        }


        /****Consent Form****/
        var ConsentFormsArrary = consultationinfo.PatientConsertArray.concat(consultationinfo.PatientPdfConsertArray);

        angular.forEach(ConsentFormsArrary, function (data, index) {

            var date = data.signedDate;
            var newdate = $filter('date')(new Date(date), "yyyy-MM-dd");
            //var newdate = date[2] + "-" + date[1] + "-" + date[0];
            data.signedDate = newdate;
            delete data.Type;
            delete data.url;
        });

        /****IVF****/

        var IVFDetail = NewIVFDetail;

        angular.forEach(IVFDetail, function (value) {
            if (value.instructiondate == 'Invalid Date') {
                delete value.instructiondate;
            }
        });
        /****Lab Order ****/
        var labOrdersForsubmit = consultationinfo.LabTestObject.LabOrderArray;


        for (var i = 0; i < labOrdersForsubmit.length; i++) {

            var Tdate = labOrdersForsubmit[i].trialDate;
            var Fdate = labOrdersForsubmit[i].finalDelivaryDate;


            finalTdate = moment(Tdate, 'DD-MM-YYYY').format("YYYY-MM-DD");
            finalFdate = moment(Fdate, 'DD-MM-YYYY').format("YYYY-MM-DD");

            labOrdersForsubmit[i].trialDate = finalTdate;
            labOrdersForsubmit[i].finalDelivaryDate = finalFdate;

            if (labOrdersForsubmit[i].invoiceDate == undefined || labOrdersForsubmit[i].invoiceDate == "") {
                delete labOrdersForsubmit[i].invoiceDate;
            } else {
                var Idate = labOrdersForsubmit[i].invoiceDate;
                labOrdersForsubmit[i].invoiceDate = moment(Idate, 'DD-MM-YYYY').format("YYYY-MM-DD");
            }

            labOrdersForsubmit[i].LabID = parseInt(labOrdersForsubmit[i].LabID);
            labOrdersForsubmit[i].labOrderStatus = parseInt(labOrdersForsubmit[i].labOrderStatus);
            if (labOrdersForsubmit[i].cost == "") {
                delete labOrdersForsubmit[i].cost;
            } else {
                labOrdersForsubmit[i].cost = parseInt(labOrdersForsubmit[i].cost);
            }
        }

        //estimation

        var EstimationData = [];
        if (consultationinfo.EstimationData != undefined) {
            angular.forEach(consultationinfo.EstimationData, function (i) {
                angular.forEach(i.estimationBodyPart, function (j) {
                    var estimatevalue = [];
                    angular.forEach(j.estimationTreatment, function (k) {
                        if (k.treatmentDesc != '') {
                            estimatevalue.push(k);
                        }
                        j.estimationTreatment = estimatevalue;
                    });
                });
            });
            EstimationData = consultationinfo.EstimationData;
        }
        if (EstimationData.length != 0) {
            for (var i = 0; i < EstimationData.length; i++) {
                if (!EstimationData[i].ValidTillDate) {
                    delete EstimationData[i].ValidTillDate;
                } else {
                    var estiDate = EstimationData[i].ValidTillDate;
                    EstimationData[i].ValidTillDate = moment(estiDate, 'DD-MM-YYYY').format("YYYY-MM-DD");
                }
            }
        }
        for (var i = 0; i < EstimationData.length; i++) {
            for (var j = 0; j < EstimationData[i].estimationBodyPart.length; j++) {
                for (var k = 0; k < EstimationData[i].estimationBodyPart[j].estimationTreatment.length; k++) {
                    if (EstimationData[i].estimationBodyPart[j].estimationTreatment[k].multipleFactor != "") {
                        EstimationData[i].estimationBodyPart[j].estimationTreatment[k].multipleFactor = parseInt(EstimationData[i].estimationBodyPart[j].estimationTreatment[k].multipleFactor);
                    }
                }
            }
        }
        for (var i = 0; i < EstimationData.length; i++) {
            for (var j = 0; j < EstimationData[i].estimationBodyPart.length; j++) {
                for (var k = 0; k < EstimationData[i].estimationBodyPart[j].estimationTreatment.length; k++) {
                    // if (EstimationData[i].estimationBodyPart[j].estimationTreatment[k].discountDetails.ActualCost) {
                    EstimationData[i].estimationBodyPart[j].estimationTreatment[k].discountDetails.ActualCost = EstimationData[i].estimationBodyPart[j].estimationTreatment[k].discountDetails.ActualCost.toString();
                    //}                        
                }
            }
        }
        //CaseTreatment
        var CaseTreatmentInSummary = consultationinfo.CaseTreatment;
        var pastHistoryDetails = [];
        if (consultationinfo.treatmentCollection) {
            pastHistoryDetails = (consultationinfo.treatmentCollection.PastMedicalHistory) ? consultationinfo.treatmentCollection.PastMedicalHistory : [];
        }
        var appointmentType = (hcueDoctorLoginService.getappointmentType()) ? hcueDoctorLoginService.getappointmentType() : "OTHDNT";
        var request = {
            ReVisitPatientCaseID: parseFloat(consultationinfo.caseid),
            AppointMentInfoObj: { DoctorVisitRsnID: (appointmentType == "") ? undefined : appointmentType, OtherVisitRsn: consultationinfo.vitalnoteinfo.visitreason, ActualFees: consultationinfo.fees },
            //FollowUpDay: parseInt(consultationinfo.followup), 
            FollowUpNotes: followup,
            DoctorNotes: doctornotes,
            SymptomNotes: symptom,
            Diagnostic: diagnostics,
            TreatMent: general,
            PatientCaseVitals: {
                Vitals: {
                    TMP: vitals.TMP,
                    HGT: vitals.HGT,
                    WGT: vitals.WGT,
                    SPP: vitals.SPP,
                    BPL: vitals.BPL,
                    BLG: vitals.BLG,
                    SFT: vitals.SFT,
                    BPH: vitals.BPH,
                    SCT: vitals.SCT,
                    LPP: vitals.LPP,
                    SRM: vitals.SRM,
                    PLS: vitals.PLS,
                    HCF: vitals.HCF,
                    REPRT: vitals.REPRT,
                    SPO2: vitals.SPO2,
                    PEFR: vitals.PEFR
                },
                MedicalHistoryArray: consultationinfo.DentalConditionsArray.MedicalHistoryArray,
                History: historyIds,
                HistotyNotes: historyNote,
                PastHealthHistoryNotes: pastHistoryDetails
            },
            Notes: (consultationinfo.treatmentCollection) ? consultationinfo.treatmentCollection : {},

            AppointmentID: consultationinfo.appointmentid,
            PatientID: consultationinfo.patientid,
            // CaseDocuments : consultationinfo.documents,
            DoctorID: consultationinfo.doctorid,
            USRType: "DOCTOR",
            USRId: hcueDoctorLoginService.getLoginId(),
            CaseDocuments: caseDocuments,
            VoiceDocuments: voiceDcmnts,
            estimateDocuments: estimateDocmts
        };

        if (getLength(request.estimateDocuments) == 0) {
            delete request.estimateDocuments;

        }
        if (consultationinfo.followup >= 0 && consultationinfo.followup !== "") {
            request.FollowUpDay = parseInt(consultationinfo.followup);
        }
        if (doctorinfo.specialitycode) {
            for (var specIndex in doctorinfo.specialitycode) {
                if (doctorinfo.specialitycode[specIndex] == "OPT") {
                    request["SpecialityCD"] = "OPT";
                }
            }
        }

        if (!angular.equals(consultationinfo.documents, {})) {
            request.CaseDocuments = consultationinfo.documents;
        }

        //Treatment Image Collection
        var treatmentImgCollection = [];
        angular.forEach(consultationinfo.PatientCaseMediaFile.Condition, function (i) {
            treatmentImgCollection.push(i.mediafileid);
        });
        angular.forEach(consultationinfo.PatientCaseMediaFile.Treatment, function (i) {

            treatmentImgCollection.push(i.mediafileid);

        });

        var commCenterrequest = {
            "sendPrescriptionMsg": (hcueDoctorLoginService.getsendPrescriptionMsg() == false) ? "N" : "Y",
            "sendBillEmail": (sendEmailSMS) ? sendEmailSMS.EMAIL : "N",
            "ReceivedAmount": (sendEmailSMS) ? sendEmailSMS.SMS : "N"
        };
        var dentalSubmitiItems = getSubmitParams(consultationinfo);
        var dentalFiles = getSubmitParams(consultationinfo);

        var allrequest = {
            commCenter: commCenterrequest,
            USRType: "DOCTOR",
            USRId: hcueDoctorLoginService.getLoginId(),
            PatientCase: request,
            MedicalCasePrescription: { PharmaWrkFlowStatusID: 'FRDP', PharmaID: null, addUpdateRecord: [] },
            LabCasePrescription: [],
            signedConsentForms: {
                addedForms: ConsentFormsArrary
            },
            labOrders: labOrdersForsubmit,
            DentalCasePrescription: dentalSubmitiItems,
            CaseTreatment: CaseTreatmentInSummary,
            CasePrescription: [{ "RowID": 1, "PatientCaseConditions": [], "USRId": hcueDoctorLoginService.getLoginId() }],
            PaymentInfo,
            BillingInfo: [],
            estimations: EstimationData,
            patientcaseextn: {
                "tempuploaddocs": {
                    "treatmentimgs": treatmentImgCollection
                },
                getcaseinfo: "Y"
            },
            ivfdetail: IVFDetail
        };
        if (consultationinfo.EstimationData != undefined) {
            if (consultationinfo.EstimationData.length == 1) {
                if (consultationinfo.EstimationData[0].estimationBodyPart.length == 0) {
                    delete allrequest.estimations;
                }
            }
        }
        if (consultationinfo.CasePrescription !== undefined && consultationinfo.CasePrescription[0].PatientCaseConditions.length > 0) {
            allrequest.CasePrescription[0].PatientCaseConditions = consultationinfo.CasePrescription[0].PatientCaseConditions;
        }
        if (consultationinfo.prescription.medicines.length > 0) {
            allrequest.MedicalCasePrescription.PharmaID = (consultationinfo.prescription.pharmaid > 0) ? consultationinfo.prescription.pharmaid : null;

            allrequest.MedicalCasePrescription.addUpdateRecord = consultationinfo.prescription.medicines;
        }

        if (consultationinfo.labtests.length > 0) {
            /* var labrequest = { LabWrkFlowStatusID: 'FRDP', LabID: null, addUpdateRecord: [] };
             labrequest.LabID = (consultationinfo.labid > 0) ? consultationinfo.labid : null;
             labrequest.addUpdateRecord = consultationinfo.labtests;
             allrequest.LabCasePrescription.push(labrequest);*/
            allrequest.LabCasePrescription = consultationinfo.labtests;

        }

        if (consultationinfo.referrals.length > 0) {
            allrequest.ReferralInfo = consultationinfo.referrals;
        }
        var billingInfo = [];
        allrequest.BillingInfo = consultationinfo.BillingInfo;
        angular.forEach(allrequest.BillingInfo, function (value) {
            if (value.ProcedureID !== '') {
                value.InvoiceDate = $filter('date')(new Date(value.InvoiceDate), 'yyyy-MM-dd');
                value.BillDate = $filter('date')(new Date(ConsultationDate), 'yyyy-MM-dd');
                this.push(value);
            }
        }, billingInfo);
        allrequest.BillingInfo = billingInfo;


        function getSubmitParams(consultationinfo) {
            // Conditions & Treatments 
            //Teeth
            var teethTreatments = consultationinfo.TreatmentConditionsObject.conditionforpatient;
            console.log(JSON.stringify(teethTreatments));
            var dentalTMJConditions = [];
            var submitConditions = [];
            var dentalConditions = [];
            var showPrimary = "";
            for (var i = 0; i < teethTreatments.length; i++) {
                var bodyParts = {};
                if (teethTreatments[i].flag == 'teeth') {

                    var subTeethConditions = [];
                    bodyParts = {
                        "Desc": "Teeth",
                        "ID": (teethTreatments[i].toothid.toString())

                    }
                    for (var z = 0; z < teethTreatments[i].Treatment.length; z++) {
                        var ConditionTreatment = [];
                        if (teethTreatments[i].Treatment[z].Treatments.length != 0) {
                            for (var k = 0; k < teethTreatments[i].Treatment[z].Treatments.length; k++) {
                                ConditionTreatment.push({
                                    "TreatmentNotes": teethTreatments[i].Treatment[z].Treatments[k].NotesTreatments,
                                    "TreatmentID": parseInt(teethTreatments[i].Treatment[z].Treatments[k].treatmentID),
                                    "TreatmentIDDesc": teethTreatments[i].Treatment[z].Treatments[k].TreatmentDesc,
                                    "Treatmentcost": teethTreatments[i].Treatment[z].Treatments[k].treatmentcost,
                                    "TreatMentStatusCD": teethTreatments[i].Treatment[z].Treatments[k].statusid,
                                    "TreatmentSeqNumber": teethTreatments[i].Treatment[z].Treatments[k].TreatmentSeqNumber,
                                    "TreatmentStatusChange": teethTreatments[i].Treatment[z].Treatments[k].TreatmentStatusChange
                                });
                            }
                            subTeethConditions.push({
                                "ConditionTreatment": ConditionTreatment,
                                "ConditionID": parseInt(teethTreatments[i].Treatment[z].ProblemId),
                                "ConditionDesc": teethTreatments[i].Treatment[z].ProblemName,
                                "ConditionNotes": teethTreatments[i].Treatment[z].ConditionNotes
                            });
                        } else {
                            subTeethConditions.push({
                                "ConditionID": parseInt(teethTreatments[i].Treatment[z].ProblemId),
                                "ConditionDesc": teethTreatments[i].Treatment[z].ProblemName,
                                "ConditionNotes": teethTreatments[i].Treatment[z].ConditionNotes
                            });
                        }
                    }
                }
                if (teethTreatments[i].flag == 'Up') {
                    var subTeethConditions = [];
                    bodyParts = {
                        "Desc": "Gums Up",
                        "ID": (teethTreatments[i].toothid.toString())

                    };
                    for (var z = 0; z < teethTreatments[i].Treatment.length; z++) {
                        var ConditionTreatment = [];
                        if (teethTreatments[i].Treatment[z].Treatments.length != 0) {
                            for (var k = 0; k < teethTreatments[i].Treatment[z].Treatments.length; k++) {
                                ConditionTreatment.push({
                                    "TreatmentNotes": teethTreatments[i].Treatment[z].Treatments[k].NotesTreatments,
                                    "TreatmentID": parseInt(teethTreatments[i].Treatment[z].Treatments[k].treatmentID),
                                    "TreatmentIDDesc": teethTreatments[i].Treatment[z].Treatments[k].TreatmentDesc,
                                    "Treatmentcost": teethTreatments[i].Treatment[z].Treatments[k].treatmentcost,
                                    "TreatMentStatusCD": teethTreatments[i].Treatment[z].Treatments[k].statusid,
                                    "TreatmentSeqNumber": teethTreatments[i].Treatment[z].Treatments[k].TreatmentSeqNumber,
                                    "TreatmentStatusChange": teethTreatments[i].Treatment[z].Treatments[k].TreatmentStatusChange
                                });
                            }
                            subTeethConditions.push({
                                "ConditionTreatment": ConditionTreatment,
                                "ConditionID": parseInt(teethTreatments[i].Treatment[z].ProblemId),
                                "ConditionDesc": teethTreatments[i].Treatment[z].ProblemName,
                                "ConditionNotes": teethTreatments[i].Treatment[z].ConditionNotes
                            });
                        } else {
                            subTeethConditions.push({
                                "ConditionID": parseInt(teethTreatments[i].Treatment[z].ProblemId),
                                "ConditionDesc": teethTreatments[i].Treatment[z].ProblemName,
                                "ConditionNotes": teethTreatments[i].Treatment[z].ConditionNotes
                            });
                        }
                    }
                }
                if (teethTreatments[i].flag == 'down') {


                    var subTeethConditions = [];
                    bodyParts = {
                        "Desc": "Gums Down",
                        "ID": (teethTreatments[i].toothid.toString())

                    };
                    for (var z = 0; z < teethTreatments[i].Treatment.length; z++) {
                        var ConditionTreatment = [];
                        if (teethTreatments[i].Treatment[z].Treatments.length != 0) {
                            for (var k = 0; k < teethTreatments[i].Treatment[z].Treatments.length; k++) {
                                ConditionTreatment.push({
                                    "TreatmentNotes": teethTreatments[i].Treatment[z].Treatments[k].NotesTreatments,
                                    "TreatmentID": parseInt(teethTreatments[i].Treatment[z].Treatments[k].treatmentID),
                                    "TreatmentIDDesc": teethTreatments[i].Treatment[z].Treatments[k].TreatmentDesc,
                                    "Treatmentcost": teethTreatments[i].Treatment[z].Treatments[k].treatmentcost,
                                    "TreatMentStatusCD": teethTreatments[i].Treatment[z].Treatments[k].statusid,
                                    "TreatmentSeqNumber": teethTreatments[i].Treatment[z].Treatments[k].TreatmentSeqNumber,
                                    "TreatmentStatusChange": teethTreatments[i].Treatment[z].Treatments[k].TreatmentStatusChange
                                });
                            }
                            subTeethConditions.push({
                                "ConditionTreatment": ConditionTreatment,
                                "ConditionID": parseInt(teethTreatments[i].Treatment[z].ProblemId),
                                "ConditionDesc": teethTreatments[i].Treatment[z].ProblemName,
                                "ConditionNotes": teethTreatments[i].Treatment[z].ConditionNotes
                            });
                        } else {
                            subTeethConditions.push({
                                "ConditionID": parseInt(teethTreatments[i].Treatment[z].ProblemId),
                                "ConditionDesc": teethTreatments[i].Treatment[z].ProblemName,
                                "ConditionNotes": teethTreatments[i].Treatment[z].ConditionNotes
                            });
                        }
                    }
                }

                dentalConditions.push({
                    "RowID": dentalConditions.length + 1,
                    "BodyParts": bodyParts,
                    "PatientCaseConditions": subTeethConditions,
                    "ShowPrimary": consultationinfo.TeethConditionsObj.showPrimary
                });
            }
            //TMJ & MUCOSA Conditions & Treatments
            var tmjTreatments = consultationinfo.TreatmentConditionsObject.TreatmentTMJArray;
            var subTMJConditions = [];



            var submitTMJConditions = consultationinfo.TeethConditionsObj.tmjarray;

            var subTobject = {
                "ConditionID": 1,
                "ConditionDesc": "TMJ & Evaluations",
                "SubCondition": submitTMJConditions
            };
            var PatientCaseConditions = [];
            PatientCaseConditions.push(subTobject);
            if (tmjTreatments.length != 0) {
                dentalConditions.push({
                    "RowID": dentalConditions.length + 1,
                    "PatientCaseConditions": PatientCaseConditions,
                    "ShowPrimary": showPrimary
                });
            }
            //Mucosa
            var mucosaTreatments = consultationinfo.TreatmentConditionsObject.TreatmentMocusaArray;
            var subMucosaConditions = [];



            var submitMucosaConditions = consultationinfo.TreatmentConditionsObject.TreatmentMocusaArray;





            var subMobject = {
                "ConditionID": 2,
                "ConditionDesc": "Mucosa",
                "SubCondition": submitMucosaConditions
            };

            var PatientCaseConditions = [];
            PatientCaseConditions.push(subMobject);


            if (mucosaTreatments.length != 0) {
                dentalConditions.push({
                    "RowID": dentalConditions.length + 1,
                    "PatientCaseConditions": PatientCaseConditions,
                    "ShowPrimary": showPrimary
                });
            }

            if (consultationinfo.progressArray == undefined) {
                consultationinfo.progressArray = [];
            }

            if (dentalConditions.length == 0 && consultationinfo.progressArray.length == 0) {
                // No Treatments Selected
            } else if (consultationinfo.progressArray.length == 0 && dentalConditions.length != 0) {
                // No ProgressArray
            } else if (consultationinfo.progressArray.length != 0 && dentalConditions.length != 0) {
                //Both ProgressArray And DentalConditionsArray having parameters
                for (var a = 0; a < consultationinfo.progressArray.length; a++) {
                    //Checking Body Parts
                    if (consultationinfo.progressArray[a].BodyParts == undefined) {
                        // For Tmj And Mucosa
                        for (var b = 0; b < dentalConditions.length; b++) {
                            if (dentalConditions[b].BodyParts == undefined) {
                                if (consultationinfo.progressArray[a].PatientCaseConditions[0].ConditionID == dentalConditions[b].PatientCaseConditions[0].ConditionID) {
                                    //checking subconditionid
                                    for (var c = 0; c < consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition.length; c++) {
                                        for (var d = 0; d < dentalConditions[b].PatientCaseConditions[0].SubCondition.length; d++) {
                                            var subcondidlength = $filter("filter")(dentalConditions[b].PatientCaseConditions[0].SubCondition, { "SubConditionID": consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition[c].SubConditionID }).length;
                                            //No Subcondition id
                                            if (subcondidlength == 0) {
                                                dentalConditions[b].PatientCaseConditions[0].SubCondition.push(consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition[c]);
                                            } else {
                                                //Checking treatid
                                                if (consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition[c].SubConditionID == dentalConditions[b].PatientCaseConditions[0].SubCondition[d].SubConditionID) {
                                                    if (dentalConditions[b].PatientCaseConditions[0].SubCondition[d].SubConditionTreatment != undefined) {
                                                        for (var e = 0; e < consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition[c].SubConditionTreatment.length; e++) {
                                                            for (var f = 0; f < dentalConditions[b].PatientCaseConditions[0].SubCondition[d].SubConditionTreatment.length; f++) {
                                                                var treatidlength = $filter("filter")(dentalConditions[b].PatientCaseConditions[0].SubCondition[d].SubConditionTreatment, { "TreatmentID": consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition[c].SubConditionTreatment[e].TreatmentID }).length;
                                                                //No treatid
                                                                if (treatidlength == 0) {
                                                                    dentalConditions[b].PatientCaseConditions[0].SubCondition[d].SubConditionTreatment.push(consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition[c].SubConditionTreatment[e]);
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                var p = 0;
                                for (var h = 0; h < dentalConditions.length; h++) {
                                    if (dentalConditions[h].PatientCaseConditions[0].ConditionID == consultationinfo.progressArray[a].PatientCaseConditions[0].ConditionID) {
                                        p++;
                                    }
                                }
                                if (p == 0) {
                                    var rowid = dentalConditions.length;
                                    rowid++;
                                    consultationinfo.progressArray[a].RowID = rowid;
                                    dentalConditions.push(consultationinfo.progressArray[a]);
                                }
                            }
                        }
                    } else {
                        ////For Teeths and Gums
                        for (var i = 0; i < dentalConditions.length; i++) {
                            if (dentalConditions[i].BodyParts != undefined) {
                                //check bodyparts exist or not and push new bodyparts
                                if (dentalConditions[i].BodyParts.Desc == consultationinfo.progressArray[a].BodyParts.Desc && dentalConditions[i].BodyParts.ID == consultationinfo.progressArray[a].BodyParts.ID) {
                                    for (var Z = 0; Z < dentalConditions[i].PatientCaseConditions.length; Z++) {
                                        for (var D = 0; D < consultationinfo.progressArray[a].PatientCaseConditions.length; D++) {
                                            if (dentalConditions[i].PatientCaseConditions[Z].ConditionID == consultationinfo.progressArray[a].PatientCaseConditions[D].ConditionID && consultationinfo.progressArray[a].PatientCaseConditions[D].ConditionTreatment != undefined && consultationinfo.progressArray[a].PatientCaseConditions[D].ConditionTreatment.length != undefined) {
                                                for (var u = 0; u < consultationinfo.progressArray[a].PatientCaseConditions[D].ConditionTreatment.length; u++) {
                                                    var j = $filter("filter")(dentalConditions[i].PatientCaseConditions[Z].ConditionTreatment, { "TreatmentID": consultationinfo.progressArray[a].PatientCaseConditions[D].ConditionTreatment[u].TreatmentID }).length;
                                                    if (j == 0) {
                                                        dentalConditions[i].PatientCaseConditions[Z].ConditionTreatment.push(consultationinfo.progressArray[a].PatientCaseConditions[D].ConditionTreatment[u]);
                                                    }
                                                }
                                            } else {
                                                var g = $filter("filter")(dentalConditions[i].PatientCaseConditions, { "ConditionID": consultationinfo.progressArray[a].PatientCaseConditions[D].ConditionID }).length;
                                                if (g == 0) {
                                                    dentalConditions[i].PatientCaseConditions.push(consultationinfo.progressArray[a].PatientCaseConditions[D]);
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    var y = $filter("filter")(dentalConditions, { "BodyParts": { "Desc": consultationinfo.progressArray[a].BodyParts.Desc, "ID": consultationinfo.progressArray[a].BodyParts.ID } }).length;
                                    if (y == 0) {
                                        dentalConditions.push(consultationinfo.progressArray[a]);
                                    }
                                }
                            } else {
                                ////For Teeths and Gums

                                for (var i = 0; i < dentalConditions.length; i++) {

                                    if (dentalConditions[i].BodyParts != undefined) {
                                        //check bodyparts exist or not and push new bodyparts
                                        if (dentalConditions[i].BodyParts.Desc == consultationinfo.progressArray[a].BodyParts.Desc && dentalConditions[i].BodyParts.ID == consultationinfo.progressArray[a].BodyParts.ID) {
                                            for (var Z = 0; Z < dentalConditions[i].PatientCaseConditions.length; Z++) {
                                                for (var D = 0; D < consultationinfo.progressArray[a].PatientCaseConditions.length; D++) {
                                                    if (dentalConditions[i].PatientCaseConditions[Z].ConditionID == consultationinfo.progressArray[a].PatientCaseConditions[D].ConditionID) {
                                                        for (var u = 0; u < consultationinfo.progressArray[a].PatientCaseConditions[D].ConditionTreatment.length; u++) {
                                                            var j = $filter("filter")(dentalConditions[i].PatientCaseConditions[Z].ConditionTreatment, { "TreatmentID": consultationinfo.progressArray[a].PatientCaseConditions[D].ConditionTreatment[u].TreatmentID }).length;
                                                            if (j == 0) {
                                                                dentalConditions[i].PatientCaseConditions[Z].ConditionTreatment.push(consultationinfo.progressArray[a].PatientCaseConditions[D].ConditionTreatment[u]);
                                                            }
                                                        }
                                                    } else {
                                                        var g = $filter("filter")(dentalConditions[i].PatientCaseConditions, { "ConditionID": consultationinfo.progressArray[a].PatientCaseConditions[D].ConditionID }).length;
                                                        if (g == 0) {
                                                            dentalConditions[i].PatientCaseConditions.push(consultationinfo.progressArray[a].PatientCaseConditions[D]);
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            var y = $filter("filter")(dentalConditions, { "BodyParts": { "Desc": consultationinfo.progressArray[a].BodyParts.Desc, "ID": consultationinfo.progressArray[a].BodyParts.ID } }).length;
                                            if (y == 0) {
                                                dentalConditions.push(consultationinfo.progressArray[a]);
                                            }
                                        }
                                    } else {

                                        //check bodyparts exist or not and push new bodyparts
                                        if (consultationinfo.progressArray[a].PatientCaseConditions[0].ConditionID == dentalConditions[i].PatientCaseConditions[0].ConditionID) {
                                            //checking subconditionid
                                            for (var c = 0; c < consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition.length; c++) {
                                                for (var d = 0; d < dentalConditions[i].PatientCaseConditions[0].SubCondition.length; d++) {
                                                    var subcondidlength = $filter("filter")(dentalConditions[i].PatientCaseConditions[0].SubCondition, { "SubConditionID": consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition[c].SubConditionID }).length;
                                                    //No Subcondition id
                                                    if (subcondidlength == 0) {
                                                        dentalConditions[i].PatientCaseConditions[0].SubCondition.push(consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition[c]);
                                                    } else {
                                                        //Checking treatid
                                                        if (consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition[c].SubConditionID == dentalConditions[b].PatientCaseConditions[0].SubCondition[d].SubConditionID) {
                                                            if (dentalConditions[i].PatientCaseConditions[0].SubCondition[d].SubConditionTreatment != undefined) {
                                                                for (var e = 0; e < consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition[c].SubConditionTreatment.length; e++) {
                                                                    for (var f = 0; f < dentalConditions[i].PatientCaseConditions[0].SubCondition[d].SubConditionTreatment.length; f++) {
                                                                        var treatidlength = $filter("filter")(dentalConditions[i].PatientCaseConditions[0].SubCondition[d].SubConditionTreatment, { "TreatmentID": consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition[c].SubConditionTreatment[e].TreatmentID }).length;
                                                                        //No treatid
                                                                        if (treatidlength == 0) {
                                                                            dentalConditions[i].PatientCaseConditions[0].SubCondition[d].SubConditionTreatment.push(consultationinfo.progressArray[a].PatientCaseConditions[0].SubCondition[c].SubConditionTreatment[e]);
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            var p = 0;
                                            for (var h = 0; h < dentalConditions.length; h++) {
                                                if (dentalConditions[h].PatientCaseConditions[0].ConditionID == consultationinfo.progressArray[a].PatientCaseConditions[0].ConditionID) {
                                                    p++;
                                                }
                                            }
                                            if (p == 0) {
                                                var rowid = dentalConditions.length;
                                                rowid++;
                                                consultationinfo.progressArray[a].RowID = rowid;
                                                dentalConditions.push(consultationinfo.progressArray[a]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //Code for inserting past conditions first in the array and the newly added in the last
                var newConditionCount = consultationinfo.TreatmentConditionsObject.conditionforpatient.length +
                    consultationinfo.TreatmentConditionsObject.TreatmentMocusaArray.length +
                    consultationinfo.TreatmentConditionsObject.TreatmentTMJArray.length;
                var newCondition = dentalConditions.splice(0, newConditionCount);
                dentalConditions = dentalConditions.concat(newCondition);

            } else if (consultationinfo.progressArray.length != 0 && dentalConditions.length == 0) {
                //only  progressarray
                dentalConditions = consultationinfo.progressArray;
            }
            angular.forEach(dentalConditions, function (data, index) {
                data.RowID = index + 1;
            });


            console.log(JSON.stringify(dentalConditions));



            for (var i = 0; i < dentalConditions.length; i++) {
                if (dentalConditions[i].BodyParts != undefined) {
                    for (var j = 0; j < dentalConditions[i].PatientCaseConditions.length; j++) {
                        if (dentalConditions[i].PatientCaseConditions[j].ConditionTreatment != undefined) {
                            for (var k = 0; k < dentalConditions[i].PatientCaseConditions[j].ConditionTreatment.length; k++) {


                                if (dentalConditions[i].PatientCaseConditions[j].ConditionTreatment[k].TreatMentStatusCD == "") {

                                    dentalConditions[i].PatientCaseConditions[j].ConditionTreatment[k].TreatMentStatusCD = null;

                                } else {
                                    dentalConditions[i].PatientCaseConditions[j].ConditionTreatment[k].TreatMentStatusCD = parseInt(dentalConditions[i].PatientCaseConditions[j].ConditionTreatment[k].TreatMentStatusCD);
                                }



                            }
                        }
                    }
                } else {
                    for (var j = 0; j < dentalConditions[i].PatientCaseConditions.length; j++) {
                        for (var k = 0; k < dentalConditions[i].PatientCaseConditions[j].SubCondition.length; k++) {
                            if (dentalConditions[i].PatientCaseConditions[j].SubCondition[k].SubConditionTreatment != undefined) {
                                for (var f = 0; f < dentalConditions[i].PatientCaseConditions[j].SubCondition[k].SubConditionTreatment.length; f++) {

                                    if (dentalConditions[i].PatientCaseConditions[j].SubCondition[k].SubConditionTreatment[f].TreatMentStatusCD == "") {

                                        dentalConditions[i].PatientCaseConditions[j].SubCondition[k].SubConditionTreatment[f].TreatMentStatusCD = null;

                                    } else {

                                        dentalConditions[i].PatientCaseConditions[j].SubCondition[k].SubConditionTreatment[f].TreatMentStatusCD = parseInt(dentalConditions[i].PatientCaseConditions[j].SubCondition[k].SubConditionTreatment[f].TreatMentStatusCD);

                                    }



                                }
                            }
                        }
                    }
                }
            }



            //Delete Empty Treatments If No selection





            for (var i = 0; i < dentalConditions.length; i++) {

                if (dentalConditions[i].BodyParts != undefined) {

                    for (var j = 0; j < dentalConditions[i].PatientCaseConditions.length; j++) {

                        if (dentalConditions[i].PatientCaseConditions[j].ConditionTreatment != undefined && dentalConditions[i].PatientCaseConditions[j].ConditionTreatment.length != undefined) {
                            for (var k = 0; k < dentalConditions[i].PatientCaseConditions[j].ConditionTreatment.length; k++) {

                                //if (dentalConditions[i].PatientCaseConditions[j].ConditionTreatment[k].TreatmentIDDesc == "") {
                                //    dentalConditions[i].PatientCaseConditions[j].ConditionTreatment.splice(k, 1);
                                // }
                                if (dentalConditions[i].PatientCaseConditions[j].ConditionTreatment[k].TreatmentIDDesc == "") {
                                    dentalConditions[i].PatientCaseConditions[j].ConditionTreatment[k].TreatmentID = 0;
                                    dentalConditions[i].PatientCaseConditions[j].ConditionTreatment[k].Treatmentcost = 0;
                                }
                            }
                        }
                        if (dentalConditions[i].PatientCaseConditions[j].ConditionTreatment != undefined && dentalConditions[i].PatientCaseConditions[j].ConditionTreatment.length != undefined && dentalConditions[i].PatientCaseConditions[j].ConditionTreatment.length == 0) {
                            delete dentalConditions[i].PatientCaseConditions[j].ConditionTreatment;
                        }
                    }


                } else {

                    for (var m = 0; m < dentalConditions[i].PatientCaseConditions[0].SubCondition.length; m++) {

                        if (dentalConditions[i].PatientCaseConditions[0].SubCondition[m].SubConditionTreatment != undefined) {
                            for (var n = 0; n < dentalConditions[i].PatientCaseConditions[0].SubCondition[m].SubConditionTreatment.length; n++) {
                                //if (dentalConditions[i].PatientCaseConditions[0].SubCondition[m].SubConditionTreatment[n].TreatmentIDDesc == "") {
                                //   dentalConditions[i].PatientCaseConditions[0].SubCondition[m].SubConditionTreatment.splice(n, 1);
                                //}
                                if (dentalConditions[i].PatientCaseConditions[0].SubCondition[m].SubConditionTreatment[n].TreatmentIDDesc == "") {
                                    dentalConditions[i].PatientCaseConditions[0].SubCondition[m].SubConditionTreatment[n].TreatmentID = 0;
                                    dentalConditions[i].PatientCaseConditions[0].SubCondition[m].SubConditionTreatment[n].Treatmentcost = 0;
                                }
                            }
                        }
                        if (dentalConditions[i].PatientCaseConditions[0].SubCondition[m].SubConditionTreatment == 0) {
                            delete dentalConditions[i].PatientCaseConditions[0].SubCondition[m].SubConditionTreatment;
                        }
                    }
                }
            }

            return dentalConditions;
        }
        var tmpOfflineCaseDetails = hcueDoctorLoginService.getOfflineCaseDetails();

        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined) {
            tmpOfflineCaseDetails.ParentHospitalID = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
            tmpOfflineCaseDetails.HospitalID = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
            tmpOfflineCaseDetails.USRId = consultationinfo.doctorid;
            tmpOfflineCaseDetails.AuditLog.ParentHospitalID = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        }
        //tmpOfflineCaseDetails.ParentHospitalID = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        //tmpOfflineCaseDetails.HospitalID = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
        tmpOfflineCaseDetails.USRId = consultationinfo.doctorid;
        tmpOfflineCaseDetails.DoctorID = consultationinfo.doctorid;
        tmpOfflineCaseDetails.AuditLog.RequestBy = doctorinfo.doctordata.doctor[0].FullName;
        //tmpOfflineCaseDetails.HospitalID = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
        tmpOfflineCaseDetails.AuditLog.RequestDate = $filter('date')(new Date(), "yyyy-MM-dd");
        tmpOfflineCaseDetails.AuditLog.ConsultantDate = $filter('date')(new Date(), "yyyy-MM-dd");
        tmpOfflineCaseDetails.AuditLog.CaseSummary = allrequest;

        if (!JSON.parse(localStorage.getItem("isAppOnline"))) {
            tmpOfflineCaseDetails.AuditLog.CaseSummary.commCenter.sendMsg = "N";
            tmpOfflineCaseDetails.AuditLog.CaseSummary.commCenter.sendPrescriptionMsg = "N"
        }
        if (getLength(tmpOfflineCaseDetails.AuditLog.UpdateAppointment) == 0) {
            delete tmpOfflineCaseDetails.AuditLog["UpdateAppointment"];
            console.log(JSON.stringify(tmpOfflineCaseDetails));
        }
        if (getLength(tmpOfflineCaseDetails.AuditLog.UpdatePatient) == 0) {
            delete tmpOfflineCaseDetails.AuditLog["UpdatePatient"];
            console.log(JSON.stringify(tmpOfflineCaseDetails));
        }
        if (getLength(tmpOfflineCaseDetails.AuditLog.AddAppointment) == 0) {
            delete tmpOfflineCaseDetails.AuditLog["AddAppointment"];
            console.log(JSON.stringify(tmpOfflineCaseDetails));
        } else if (pauseVal == 1) {
            tmpOfflineCaseDetails.AuditLog.AddAppointment.AppointmentStatus = 'P';
        } else {
            tmpOfflineCaseDetails.AuditLog.AddAppointment.AppointmentStatus = 'E';
        }

        if (getLength(tmpOfflineCaseDetails.AuditLog.AddPatient) == 0 || tmpOfflineCaseDetails.AuditLog.CaseSummary.PatientCase.PatientID > 0 || tmpOfflineCaseDetails.AuditLog.AddPatient.PatientID > 0) {
            delete tmpOfflineCaseDetails.AuditLog["AddPatient"];
            console.log(JSON.stringify(tmpOfflineCaseDetails));
        }

        hcueDoctorLoginService.setOfflineCaseDetails(tmpOfflineCaseDetails)
        //console.log(JSON.stringify(hcueDoctorLoginService.getOfflineCaseDetails()));

        function getLength(obj) {

            if (obj !== undefined) {
                return Object.keys(obj).length;
            } else {
                return 0;
            }
        }

        //console.log("*************allrequest*******");
        console.log(offlineDBID);
        var tmpAppInfo = hcueDoctorLoginService.getAppointmentInfo();
        console.log(JSON.stringify(hcueDoctorLoginService.getAppointmentInfo()));
        if ((offlineDBID > 0) && (!JSON.parse(localStorage.getItem("isAppOnline"))) && tmpAppInfo.online == undefined) {
            //var tmpAddedAppointment = hcueDoctorLoginService.getAppointmentInfo();
            var offlinePost = $http.post(localStorage.getItem("offlineURL") + '/data/' + offlineDBID, tmpOfflineCaseDetails, header);
        } else {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addAllPatientCase, allrequest, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.docAddAppointment, tmpOfflineCaseDetails, header);
        }



        //


        offlinePost.success(function (data) {
            //console.log("*************Result Success*******");
            console.log(JSON.stringify(data));
            if (data.status == "Success") {
                callbackSuccess(data);
            } else if (data.message !== undefined && data.message == "Save data successfull") {
                callbackSuccess(data);
            }

            var clearOfflineCaseDetails = {
                "VersionID": 1,
                "DoctorID": 0,
                "USRId": 0,
                "USRType": "DOCTOR",
                "AuditLog": {
                    "RequestDate": "",
                    "ConsultantDate": "",
                    "RequestBy": "",
                    "ParentHospitalID": 0,
                    "HospitalID": 0,
                    "AddAppointment": {},
                    "UpdateAppointment": {},
                    "AddPatient": {},
                    "UpdatePatient": {},
                    "CaseSummary": {}
                }
            };


            hcueDoctorLoginService.setOfflineCaseDetails(clearOfflineCaseDetails);
            if (tmpAppInfo.online !== undefined && tmpAppInfo.online && offlineDBID > 0) {
                var onlineAppointmentStatusUpdate = $http.post(localStorage.getItem("offlineURL") + '/data/' + offlineDBID + '?status=1');
                onlineAppointmentStatusUpdate.success(function (statusUpdateData) {
                    offlineDBID = 0;
                    callbackSuccess(data);
                })
                    .error(function (statusUpdateData) {
                        callbackError(data);
                    });
            }
        })
            .error(function (data) {
                //console.log("*************Result Failed*******");
                //console.log(JSON.stringify(data));
                callbackError(data);
            })
    }

    this.AddPatientCase = function (consultationinfo, callbackSuccess, callbackError) {
        //alert('cum1');
        console.log(JSON.stringify(consultationinfo));
        var vitals = consultationinfo.vitalnoteinfo.vitals;
        var symptom = "";
        var diagnostics = "";
        var general = "";
        var doctornotes = "";
        var followup = "";
        if (consultationinfo.vitalnoteinfo.notes != null) {
            symptom = consultationinfo.vitalnoteinfo.notes.symptom;
            diagnostics = "";
            general = consultationinfo.vitalnoteinfo.notes.general;
            doctornotes = consultationinfo.vitalnoteinfo.notes.doctornotes;
            followup = consultationinfo.vitalnoteinfo.notes.followup;
        }

        var request = {
            ReVisitPatientCaseID: parseFloat(consultationinfo.caseid),
            AppointMentInfoObj: { OtherVisitRsn: consultationinfo.vitalnoteinfo.visitreason, ActualFees: consultationinfo.fees },
            FollowUpDay: parseInt(consultationinfo.followup),
            FollowUpNotes: followup,
            DoctorNotes: doctornotes,
            SymptomNotes: symptom,
            Diagnostic: diagnostics,
            TreatMent: general,
            PatientCaseVitals: {
                Vitals: {
                    TMP: vitals.temperature,
                    HGT: vitals.height,
                    WGT: vitals.weight,
                    SPP: vitals.sugarpp,
                    BPL: vitals.bplow,
                    BLG: vitals.bloodgroup,
                    SFT: vitals.sugarfast,
                    BPH: vitals.bphigh,
                    SCT: vitals.SCT,
                    LPP: vitals.LPP,
                    SRM: vitals.SRM,
                    HCF: vitals.HCF
                }
            },

            AppointmentID: consultationinfo.appointmentid,
            PatientID: consultationinfo.patientid,
            // CaseDocuments : consultationinfo.documents,
            DoctorID: consultationinfo.doctorid,
            USRType: "DOCTOR",
            USRId: consultationinfo.doctorid
        };
        if (!angular.equals(consultationinfo.documents, {})) {
            request.CaseDocuments = consultationinfo.documents;
        }
        var allrequest = {
            USRType: "DOCTOR",
            USRId: consultationinfo.doctorid,
            PatientCase: request,
            MedicalCasePrescription: { PharmaWrkFlowStatusID: 'FRDP', PharmaID: null, addUpdateRecord: [] },
            LabCasePrescription: [],
            CasePrescription: [{ "RowID": 1, "PatientCaseConditions": [], "USRId": consultationinfo.doctorid }],
            BillingInfo: [] //{ LabWrkFlowStatusID: 'FRDP', LabID: null, addUpdateRecord: [] }],
            // ScanCasePrescription: [{ LabWrkFlowStatusID: 'FRDP', LabID: null, addUpdateRecord: [] }]
        };

        //console.log("*************consultationinfo.prescription.medicines*******");
        //console.log(consultationinfo.prescription.medicines);

        //console.log("*************consultationinfo.prescription.medicines*******");
        if (consultationinfo.CasePrescription.PatientCaseConditions.length > 0) {
            allrequest.CasePrescription.PatientCaseConditions = consultationinfo.CasePrescription.PatientCaseConditions;
        }
        if (consultationinfo.prescription.medicines.length > 0) {
            allrequest.MedicalCasePrescription.PharmaID = (consultationinfo.prescription.pharmaid > 0) ? consultationinfo.prescription.pharmaid : null;

            allrequest.MedicalCasePrescription.addUpdateRecord = consultationinfo.prescription.medicines;
        }

        if (consultationinfo.labtests.length > 0) {
            /* var labrequest = { LabWrkFlowStatusID: 'FRDP', LabID: null, addUpdateRecord: [] };
             labrequest.LabID = (consultationinfo.labid > 0) ? consultationinfo.labid : null;
             labrequest.addUpdateRecord = consultationinfo.labtests;
             allrequest.LabCasePrescription.push(labrequest);*/
            allrequest.LabCasePrescription = consultationinfo.labtests;

        }

        if (consultationinfo.referrals.length > 0) {
            allrequest.ReferralInfo = consultationinfo.referrals;
        }

        if (consultationinfo.BillingInfo.length > 0) {

            allrequest.BillingInfo = consultationinfo.BillingInfo;
        }
        //-----------Use when scan is seperate tab. check/verfied, also enable tab scan in tab.html
        /* if (consultationinfo.scantests.length > 0) {
             var labrequest = { LabWrkFlowStatusID: 'FRDP', LabID: null, addUpdateRecord: [] };
             labrequest.LabID = (consultationinfo.scanid > 0) ? consultationinfo.scanid : null;
             labrequest.addUpdateRecord = consultationinfo.scantests;
             allrequest.LabCasePrescription.push(labrequest);

             //allrequest.ScanCasePrescription[0].LabID = (consultationinfo.scanid > 0) ? consultationinfo.scanid : null;
             //allrequest.ScanCasePrescription[0].addUpdateRecord = consultationinfo.scantests;
         } */

        //console.log("*************allrequest*******");
        //console.log(JSON.stringify(allrequest));


        //console.log("*************allrequest*******");
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addAllPatientCase, allrequest, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.docAddAppointment, allrequest, header);
        offlinePost.success(function (data) {
            //console.log("*************Result Success*******");
            //console.log(JSON.stringify(data));
            if (data == "Success")
                callbackSuccess(data);
        })
            .error(function (data) {
                //console.log("*************Result Failed*******");
                //console.log(JSON.stringify(data));
                callbackError(data);
            })
    }

    this.ConsultationPause = function (NewIVFDetail, consultationinfo, historyIds, historyNote, callbackSuccess, callbackError) {

        var vitals = consultationinfo.vitalnoteinfo.vitals;
        var symptom = "";
        var diagnostics = "";
        var general = "";
        var doctornotes = "";
        var followup = "";
        var PaymentInfo = consultationinfo.PaymentInfo;
        console.log(JSON.stringify(consultationinfo));

        if (consultationinfo.vitalnoteinfo.notes != null) {
            symptom = consultationinfo.vitalnoteinfo.notes.symptom;
            diagnostics = "";
            general = consultationinfo.vitalnoteinfo.notes.general;
            doctornotes = consultationinfo.vitalnoteinfo.notes.doctornotes;
            followup = consultationinfo.vitalnoteinfo.notes.followup;
        }
        /****IVF****/

        var IVFDetail = NewIVFDetail;

        //VOICE DOCUMENTS
        var VoiceNotesInSummary = consultationinfo.voiceDocuments.voiceDocumentArray;
        var voiceDcmnts = {};
        for (var i = 0; i < VoiceNotesInSummary.length; i++) {
            var documentidInSummary = VoiceNotesInSummary[i].voice.data;
            voiceDcmnts[i + 1] = documentidInSummary;
        }

        //Case Documents
        var caseDocumentsInSummary = consultationinfo.ImageNotesData;
        var caseDocuments = {};
        for (var i = 0; i < caseDocumentsInSummary.length; i++) {
            var casedocumentidInSummary = caseDocumentsInSummary[i].documentid;
            caseDocuments[i + 1] = casedocumentidInSummary;
        }
        /****Consent Form****/
        var ConsentFormsArrary = consultationinfo.PatientConsertArray.concat(consultationinfo.PatientPdfConsertArray);

        angular.forEach(ConsentFormsArrary, function (data, index) {

            var date = data.signedDate;
            var newdate = $filter('date')(new Date(date), "yyyy-MM-dd");
            //var newdate = date[2] + "-" + date[1] + "-" + date[0];
            data.signedDate = newdate;
            delete data.Type;
            delete data.url;
        });


        /****Lab Order ****/
        var labOrdersForsubmit = consultationinfo.LabTestObject.LabOrderArray;


        for (var i = 0; i < labOrdersForsubmit.length; i++) {

            var Tdate = labOrdersForsubmit[i].trialDate;
            var Fdate = labOrdersForsubmit[i].finalDelivaryDate;


            finalTdate = moment(Tdate, 'DD-MM-YYYY').format("YYYY-MM-DD");
            finalFdate = moment(Fdate, 'DD-MM-YYYY').format("YYYY-MM-DD");

            labOrdersForsubmit[i].trialDate = finalTdate;
            labOrdersForsubmit[i].finalDelivaryDate = finalFdate;

            if (labOrdersForsubmit[i].invoiceDate == undefined || labOrdersForsubmit[i].invoiceDate == "") {
                delete labOrdersForsubmit[i].invoiceDate;
            } else {
                var Idate = labOrdersForsubmit[i].invoiceDate;
                labOrdersForsubmit[i].invoiceDate = moment(Idate, 'DD-MM-YYYY').format("YYYY-MM-DD");
            }

            labOrdersForsubmit[i].LabID = parseInt(labOrdersForsubmit[i].LabID);
            labOrdersForsubmit[i].labOrderStatus = parseInt(labOrdersForsubmit[i].labOrderStatus);
            if (labOrdersForsubmit[i].cost == "") {
                delete labOrdersForsubmit[i].cost;
            } else {
                labOrdersForsubmit[i].cost = parseInt(labOrdersForsubmit[i].cost);
            }
        }


        //case Treatment
        var tempCollection = [];
        angular.forEach(consultationinfo.CaseTreatment, function (value) {
            if (value.TreatmentNotes != "" || value.TreatmentDesctription != "" || value.TreatmentDocuments.length > 0) {
                tempCollection.push(value);
            }
        });
        //Pastmedicalhistory
        var pastHistoryDetails = [];
        if (consultationinfo.treatmentCollection) {
            pastHistoryDetails = (consultationinfo.treatmentCollection.PastMedicalHistory) ? consultationinfo.treatmentCollection.PastMedicalHistory : [];
        }

        var request = {
            ReVisitPatientCaseID: parseFloat(consultationinfo.caseid),
            AppointMentInfoObj: { OtherVisitRsn: consultationinfo.vitalnoteinfo.visitreason, ActualFees: consultationinfo.fees },
            FollowUpDay: parseInt(consultationinfo.followup),
            FollowUpNotes: followup,
            DoctorNotes: doctornotes,
            SymptomNotes: symptom,
            Diagnostic: diagnostics,
            TreatMent: general,
            PatientCaseVitals: {
                Vitals: {
                    TMP: vitals.TMP,
                    HGT: vitals.HGT,
                    WGT: vitals.WGT,
                    SPP: vitals.SPP,
                    BPL: vitals.BPL,
                    BLG: vitals.BLG,
                    SFT: vitals.SFT,
                    BPH: vitals.BPH,
                    SCT: vitals.SCT,
                    LPP: vitals.LPP,
                    SRM: vitals.SRM,
                    PLS: vitals.PLS,
                    HCF: vitals.HCF
                },
                MedicalHistoryArray: consultationinfo.DentalConditionsArray.MedicalHistoryArray,
                DentalHistoryArray: consultationinfo.DentalConditionsArray.DentalHistoryArray,
                History: historyIds,
                HistotyNotes: historyNote,
                PastHealthHistoryNotes: pastHistoryDetails
            },
            CaseTreatment: tempCollection,
            Notes: (consultationinfo.treatmentCollection) ? consultationinfo.treatmentCollection : {},
            AppointmentID: consultationinfo.appointmentid,
            PatientID: consultationinfo.patientid,
            // CaseDocuments : consultationinfo.documents,
            DoctorID: consultationinfo.doctorid,
            USRType: "DOCTOR",
            USRId: consultationinfo.doctorid,
            CaseDocuments: caseDocuments,
            VoiceDocuments: voiceDcmnts,
            ivfdetail: IVFDetail
        };
        if (doctorinfo.specialitycode) {
            for (var specIndex in doctorinfo.specialitycode) {
                if (doctorinfo.specialitycode[specIndex] == "OPT") {
                    request["SpecialityCD"] = "OPT";
                }
            }
        }

        if (!angular.equals(consultationinfo.documents, {})) {
            request.CaseDocuments = consultationinfo.documents;
        }
        //billing
        var billingInfo = [];
        request.BillingInfo = consultationinfo.BillingInfo;
        angular.forEach(request.BillingInfo, function (value) {
            if (value.ProcedureID !== '') {
                value.InvoiceDate = $filter('date')(new Date(value.InvoiceDate), 'yyyy-MM-dd');
                value.BillDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                this.push(value);
            }
        }, billingInfo);
        request.BillingInfo = billingInfo;


        var commCenterrequest = { "sendPrescriptionMsg": (hcueDoctorLoginService.getsendPrescriptionMsg() == false) ? "N" : "Y" };
        var allrequest = {
            "JsonSettingID": consultationinfo.JSONID,
            "DoctorID": consultationinfo.doctorid,
            "CustomID": consultationinfo.appointmentid,
            "TemplateName": "",
            "ActiveIND": "Y",
            "CrtUSR": consultationinfo.doctorid,
            "CrtUSRType": "DOCTOR",
            "IsDefault": "Y",
            "PageType": "CONSULT",
            JsonSetting: {
                commCenter: commCenterrequest,
                USRType: "DOCTOR",
                USRId: consultationinfo.doctorid,
                PatientCase: request,
                TeethConditionsObj: consultationinfo.TeethConditionsObj,
                TreatmentConditionsObject: consultationinfo.TreatmentConditionsObject,
                TmjcustomArray: consultationinfo.TmjcustomArray,
                Mucosacustomarray: consultationinfo.Mucosacustomarray,
                labOrders: labOrdersForsubmit,
                PatientConsertArray: consultationinfo.PatientConsertArray,
                PatientPdfConsertArray: consultationinfo.PatientPdfConsertArray,
                Teeths: {
                    AdultTootharray: consultationinfo.AdultTootharray,
                    ChildTootharray: consultationinfo.ChildTootharray,
                    AdultTootharrayLab: consultationinfo.AdultTootharrayLab,
                    ChildTootharrayLab: consultationinfo.ChildTootharrayLab
                },
                showPrimary: consultationinfo.showPrimary,
                showAdultTeeth: consultationinfo.showAdultTeeth,
                showChildTeeth: consultationinfo.showChildTeeth,
                MedicalCasePrescription: { PharmaWrkFlowStatusID: 'FRDP', PharmaID: null, addUpdateRecord: [] },
                LabCasePrescription: [],
                CasePrescription: [{ "RowID": 1, "PatientCaseConditions": [], "USRId": consultationinfo.doctorid }],
            }

        };


        if (consultationinfo.CasePrescription[0].PatientCaseConditions.length > 0) {
            allrequest.JsonSetting.CasePrescription[0].PatientCaseConditions = consultationinfo.CasePrescription[0].PatientCaseConditions;
        }
        if (consultationinfo.prescription.medicines.length > 0) {
            allrequest.JsonSetting.MedicalCasePrescription.PharmaID = (consultationinfo.prescription.pharmaid > 0) ? consultationinfo.prescription.pharmaid : null;
            allrequest.JsonSetting.MedicalCasePrescription.addUpdateRecord = consultationinfo.prescription.medicines;
        }

        if (consultationinfo.labtests.length > 0) {

            allrequest.JsonSetting.LabCasePrescription = consultationinfo.labtests;

        }

        if (consultationinfo.referrals != undefined) {
            if (consultationinfo.referrals.length > 0) {
                allrequest.JsonSetting.ReferralInfo = consultationinfo.referrals;
            }
        }

        if (consultationinfo.JSONID == "" || consultationinfo.JSONID == undefined) {
            delete allrequest.JsonSettingID;
        }


        console.log(JSON.stringify(allrequest));
        $http.post(hcueServiceUrl.consultationPause, allrequest, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }

    this.AppointmentStatus = function (appstatus, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.appointmentStatus, appstatus, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }

    this.JsonConsultation = function (appstatus, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listJsonSettings, appstatus, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }

    this.GetAppointmentCaseHistory = function (doctorid, patientid, pageno, hospcode, callbackSuccess, callbackError) {
        if (hospcode !== undefined && patientid != 0) {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listPatientCase, { DoctorID: doctorid, "PatientID": patientid, "PageNumber": (pageno == undefined) ? 1 : pageno, "PageSize": 100, "HospitalCD": hospcode }, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + patientid + '/' + hcueOfflineServiceUrl.listPatientCase, { DoctorID: doctorid, "PatientID": patientid, "PageNumber": pageno, "PageSize": 3, "HospitalCD": hospcode });
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                })
        } else if (patientid != 0) {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listPatientCase, { DoctorID: doctorid, "PatientID": patientid, "PageNumber": (pageno == undefined) ? 1 : pageno, "PageSize": 100 }, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + patientid + '/' + hcueOfflineServiceUrl.listPatientCase, { DoctorID: doctorid, "PatientID": patientid, "PageNumber": pageno, "PageSize": 3 });
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            if (!(JSON.parse(localStorage.getItem("isAppOnline")))) {
                var data = { "message": "No data found!" };
                callbackSuccess(data);
            } else {
                if (hospcode !== undefined) {
                    var offlinePost = $http.post(hcueServiceUrl.listPatientCase, { DoctorID: doctorid, "PatientID": patientid, "PageNumber": (pageno == undefined) ? 1 : pageno, "PageSize": 3, "HospitalCD": hospcode }, header);
                    offlinePost.success(function (data) {
                        callbackSuccess(data);
                    })
                        .error(function (data) {
                            callbackError(data);
                        })
                } else {
                    var offlinePost = $http.post(hcueServiceUrl.listPatientCase, { DoctorID: doctorid, "PatientID": patientid, "PageNumber": (pageno == undefined) ? 1 : pageno, "PageSize": 3 }, header);
                    offlinePost.success(function (data) {
                        callbackSuccess(data);
                    })
                        .error(function (data) {
                            callbackError(data);
                        })
                }
            }


        }
    }
    this.GetAppointmentPrint = function (doctorid, patientid, appointmentid, pageno, hospcode, callbackSuccess, callbackError) {
        if (hospcode !== undefined) {
            $http.post(hcueServiceUrl.appointmentlistprint, { DoctorID: doctorid, "PatientID": patientid, "AppointmentID": appointmentid, "PatientCaseID": 0, "HospitalCD": hospcode, "ScreenType": "VISIT" }, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            $http.post(hcueServiceUrl.appointmentlistprint, { DoctorID: doctorid, "PatientID": patientid, "AppointmentID": appointmentid, "PatientCaseID": 0, "ScreenType": "VISIT" }, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })
        }
    }
    this.GetPatientCaseDetails = function (doctorid, appointmentid, patientid, caseid, callbackSuccess, callbackError) {
        var request = { DoctorID: doctorid, AppointmentID: appointmentid, PatientID: patientid, PatientCaseID: caseid, "RevisitCaseID": "Y" };

        $http.post(hcueServiceUrl.getPatientCaseDetails, request, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }
    this.GetPatientLabCaseDetails = function (doctorid, patientid, pageno, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getLabCaseDetails, { DoctorID: doctorid, "PatientID": patientid, "PageNumber": pageno, "PageSize": 3 }, header)
            //var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.docpastlabhistory, { DoctorID: doctorid, "PatientID": patientid, "PageNumber": pageno, "PageSize": 3 }, header) : $http.post(localStorage.getItem("offlineURL") + '/data/' + doctorid + '/' + hcueOfflineServiceUrl.docpastlabhistory, { DoctorID: doctorid, "PatientID": patientid, "PageNumber": pageno, "PageSize": 3 });
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {

                callbackError(data);
            })
    }

    function GetPatient(param, callbackSuccess, callbackError, appintmentSearch = false) {
        //$http.post(hcueServiceUrl.getPatient, param, header)
        //console.log(JSON.stringify(param));
        //$http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getdoctorPatient, param)
        // var searchval = (param.FirstName == undefined) ? 'PhoneNumber' : 'FirstName';
        // console.log(JSON.stringify(param));
        //var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getSMSpatients, param, header) : $http.get(localStorage.getItem("offlineURL") + '/pluck/' + doctorinfo.doctorid + '/'+ hcueOfflineServiceUrl.getdoctorPatient + '?searchkey='+ searchval+ '&search=' + param.searchval);
        var searchText = "FullName";
        var searchValue = param.FirstName;
        if (param.PhoneNumber !== "" && param.PhoneNumber.length > 0) {
            searchText = "PhNumber";
            searchValue = param.PhoneNumber;
        }
        console.log(searchText);
        //$http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getdoctorPatient, param);
        if (!appintmentSearch || JSON.parse(localStorage.getItem("isAppOnline"))) {
            //console.log(JSON.stringify(appintmentSearch) +" "+ JSON.stringify(navigator) );
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getPatient, param, header) : $http.get(localStorage.getItem("offlineURL") + '/search/0/' + hcueOfflineServiceUrl.getPatient + '?searchkey=' + searchText + '&search=' + searchValue);
            offlinePost.success(function (data) {

                callbackSuccess(data);

            })
                .error(function (data) {
                    callbackError(data);
                })

        } else {
            //var offlinePost =$http.get(localStorage.getItem("offlineURL") + '/pluck/' + doctorinfo.doctorid + '/'+ 
            //   hcueOfflineServiceUrl.getdoctorPatient + '?searchkey='+ searchText+ '&search=' + searchValue);
            var offlinePost = $http.get(localStorage.getItem("offlineURL") + '/patient/' + doctorinfo.doctorid + '?search=' + searchValue);
            offlinePost.success(function (data) {

                console.log(JSON.stringify(data));
                var offlineCollection = {};
                offlineCollection = { 'rows': [], 'count': 0 };
                offlineCollection.rows.push(data);
                offlineCollection.count = offlineCollection.rows.length;
                console.log(JSON.stringify(offlineCollection));
                callbackSuccess(offlineCollection);
            })
                .error(function (data) {
                    callbackError(data);
                })

        }


    }

    function GetPatientVerson(param, callbackSuccess, callbackError) {
        var searchText = "FullName";
        var searchValue = param.FirstName;
        if (param.PhoneNumber !== "" && param.PhoneNumber.length > 0) {
            searchText = "PhNumber";
            searchValue = param.PhoneNumber;
        }
        param.Version = "V1";
        $http.post(hcueServiceUrl.getPatient, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }

    function GetPatientthroughmobilenumber(param, callbackSuccess, callbackError, appintmentSearch = false) {

        if (!appintmentSearch || JSON.parse(localStorage.getItem("isAppOnline"))) {
            //console.log(JSON.stringify(appintmentSearch) +" "+ JSON.stringify(navigator) );

            var searchText = "SearchText";
            var searchValue = param.SearchText;
            if (param.PhoneNumber !== "" && param.PhoneNumber !== undefined) {
                searchText = "PhoneNumber";
                searchValue = param.PhoneNumber;
            }
            //console.log(searchText);

            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getPatientthroughmobilenumber, param, header) :
                $http.get(localStorage.getItem("offlineURL") + '/patient/' + doctorinfo.doctorid + '?search=' + searchValue);
            //$http.get(localStorage.getItem("offlineURL") + '/search/0/'+ hcueOfflineServiceUrl.getPatient + '?searchkey=' +searchText+'&search=' + searchValue);
            //$http.get(localStorage.getItem("offlineURL") + '/pluck/' + doctorinfo.doctorid + '/'+ 
            //hcueOfflineServiceUrl.getdoctorPatient + '?searchkey=' +searchText+'&search=' + searchValue);

            // var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getPatientthroughmobilenumber, param, header) : $http.get(localStorage.getItem("offlineURL") + '/data/'+ hcueOfflineServiceUrl.getSMSpatients);
            offlinePost.success(function (data) {

                callbackSuccess(data);

            })
                .error(function (data) {
                    callbackError(data);
                })

        } else {

            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getPatientthroughmobilenumber, param, header) : $http.get(localStorage.getItem("offlineURL") + '/patient/' + doctorinfo.doctorid + '?search=' + searchValue);
            offlinePost.success(function (data) {

                console.log(JSON.stringify(data));
                var offlineCollection = {};
                offlineCollection = { 'rows': [], 'count': 0 };
                offlineCollection.rows.push(data);
                offlineCollection.count = offlineCollection.rows.length;
                console.log(JSON.stringify(offlineCollection));
                callbackSuccess(offlineCollection);
            })
                .error(function (data) {
                    callbackError(data);
                })

        }

    }

    function GetDoctorPatient(param, callbackSuccess, callbackError) {

        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            //var params = {DoctorID:hcueDoctorLoginService.getLoginId(),PageSize :20,PageNumber :1,params.Sort:"asc",hasparam = true};

            var hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            param.HospitalCD = hospid;
            console.log(JSON.stringify(param));
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getSMSpatients, param, header) : $http.get(localStorage.getItem("offlineURL") + '/patient/' + doctorinfo.doctorid + "?pageno=" + param.PageNumber + "&pagesize=" + param.PageSize + "&search=" + param.SearchText, header);
            offlinePost.success(function (data) {
                if (callbackSuccess != null)
                    callbackSuccess(data);
            })
                .error(function (data) {
                    if (callbackError != null)
                        callbackError(data);
                })
        } else {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getSMSpatients, param, header) : $http.get(localStorage.getItem("offlineURL") + '/patient/' + doctorinfo.doctorid + "?pageno=" + param.PageNumber + "&pagesize=" + param.PageSize + "&search=" + param.SearchText, header);
            offlinePost.success(function (data) {
                if (callbackSuccess != null)
                    callbackSuccess(data);
            })
                .error(function (data) {
                    if (callbackError != null)
                        callbackError(data);
                })
        }

    }

    function GetDoctorPatient1(param, callbackSuccess, callbackError, sendhospcode) {

        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {
            if (sendhospcode == true) {
                delete param.HospitalIDLst;
            }
            var hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            param.HospitalCD = hospid;
            $http.post(hcueServiceUrl.getSMSpatients, param, header)
                .success(function (data) {
                    if (callbackSuccess != null)
                        callbackSuccess(data);
                })
                .error(function (data) {
                    if (callbackError != null)
                        callbackError(data);
                })
        } else {
            $http.post(hcueServiceUrl.getSMSpatients, param, header)
                .success(function (data) {
                    if (callbackSuccess != null)
                        callbackSuccess(data);
                })
                .error(function (data) {
                    if (callbackError != null)
                        callbackError(data);
                })
        }
    }


    //Review Patient Record
    this.updatePatientReview = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.updatePatientReview, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.GetPatientReview = function (PatientVisitRequest, patiendcaseid, callbackSuccess, callbackError) {
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {

            var hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            PatientVisitRequest.HospitalCD = hospcd;
            if (patiendcaseid !== undefined) {
                PatientVisitRequest.PatientCaseID = patiendcaseid;
                $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header)
                    .success(function (data) {
                        callbackSuccess(data);
                    })
                    .error(function (data) {
                        callbackError(data);
                    })
            } else {
                $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header)
                    .success(function (data) {
                        callbackSuccess(data);
                    })
                    .error(function (data) {
                        callbackError(data);
                    })
            }
        } else {
            if (patiendcaseid !== undefined) {
                PatientVisitRequest.PatientCaseID = patiendcaseid;
                $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header)
                    .success(function (data) {
                        callbackSuccess(data);
                    })
                    .error(function (data) {
                        callbackError(data);
                    })
            } else {
                $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header)
                    .success(function (data) {
                        callbackSuccess(data);
                    })
                    .error(function (data) {
                        callbackError(data);
                    })
            }
        }
    };

    //Visit Patient Past record

    this.GetPatientVisitPastRecord = function (PatientVisitRequest, patiendcaseid, callbackSuccess, callbackError) {
        console.log(JSON.stringify(patiendcaseid));
        if (patiendcaseid !== undefined) {
            PatientVisitRequest.PatientCaseID = patiendcaseid;
            if (PatientVisitRequest.PatientID == 0 && !(JSON.parse(localStorage.getItem("isAppOnline")))) {

                var data = { "message": "No data found!" };
                callbackSuccess(data);



            } else {

                var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + PatientVisitRequest.PatientID + '/' + hcueOfflineServiceUrl.listPatientCase, PatientVisitRequest);
                offlinePost.success(function (data) {
                    callbackSuccess(data);
                })
                    .error(function (data) {
                        callbackError(data);
                    })
            }

        } else {
            if (PatientVisitRequest.PatientID == 0 && !(JSON.parse(localStorage.getItem("isAppOnline")))) {

                var data = { "message": "No data found!" };
                callbackSuccess(data);



            } else {
                var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + PatientVisitRequest.PatientID + '/' + hcueOfflineServiceUrl.listPatientCase, PatientVisitRequest);
                offlinePost.success(function (data) {
                    callbackSuccess(data);
                })
                    .error(function (data) {
                        callbackError(data);
                    })
            }

        }

    };

    //Visit by CaseId Past Records 

    this.GetPatientCaseIdVisitPastRecord = function (PatientVisitRequest, patiendcaseid, callbackSuccess, callbackError) {
        console.log(JSON.stringify(patiendcaseid));
        if (patiendcaseid !== undefined) {
            PatientVisitRequest.PatientCaseID = patiendcaseid;
            $http.post(hcueServiceUrl.getPatientCaseDetails, PatientVisitRequest, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            $http.post(hcueServiceUrl.getPatientCaseDetails, PatientVisitRequest, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })
        }

    };


    //Medication patient past record

    this.GetPatientMedicationPastRecord = function (PatientVisitRequest, patiendcaseid, RowID, callbackSuccess, callbackError) {
        console.log(JSON.stringify(patiendcaseid));
        if (patiendcaseid !== undefined) {
            PatientVisitRequest.PatientCaseID = patiendcaseid;
            PatientVisitRequest.RowID = RowID;

            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + PatientVisitRequest.PatientID + '/' + hcueOfflineServiceUrl.listPatientCase, PatientVisitRequest);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + PatientVisitRequest.PatientID + '/' + hcueOfflineServiceUrl.listPatientCase, PatientVisitRequest);
            offlinePost.success(function (data) {
                //console.log(JSON.stringify(data));
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                })
        }

    };


    //Medication by CaseId Past Records 

    this.GetPatientCaseIdMedPastRecord = function (PatientVisitRequest, patiendcaseid, RowID, callbackSuccess, callbackError) {
        console.log(JSON.stringify(patiendcaseid));
        if (patiendcaseid !== undefined) {
            PatientVisitRequest.PatientCaseID = patiendcaseid;
            $http.post(hcueServiceUrl.getPatientCaseDetails, PatientVisitRequest, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            $http.post(hcueServiceUrl.getPatientCaseDetails, PatientVisitRequest, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })
        }

    };

    //Ivf patient past record

    this.GetPatientIVFPastRecord = function (PatientVisitRequest, patiendcaseid, RowID, callbackSuccess, callbackError) {
        console.log(JSON.stringify(patiendcaseid));
        if (patiendcaseid !== undefined) {
            PatientVisitRequest.PatientCaseID = patiendcaseid;
            PatientVisitRequest.RowID = RowID;

            $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })
        }

    };


    //Ivf by CaseId Past Records 

    this.GetPatientCaseIdIVFPastRecord = function (PatientVisitRequest, patiendcaseid, RowID, callbackSuccess, callbackError) {
        console.log(JSON.stringify(patiendcaseid));
        if (patiendcaseid !== undefined) {
            PatientVisitRequest.PatientCaseID = patiendcaseid;
            $http.post(hcueServiceUrl.getPatientCaseDetails, PatientVisitRequest, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            $http.post(hcueServiceUrl.getPatientCaseDetails, PatientVisitRequest, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })
        }

    };


    //Lab patient past record

    this.GetPatientLabPastRecord = function (PatientVisitRequest, patiendcaseid, RowID, callbackSuccess, callbackError) {
        console.log(JSON.stringify(patiendcaseid));
        if (patiendcaseid !== undefined) {
            PatientVisitRequest.PatientCaseID = patiendcaseid;
            PatientVisitRequest.RowID = RowID;

            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + PatientVisitRequest.PatientID + '/' + hcueOfflineServiceUrl.listPatientCase, PatientVisitRequest);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + PatientVisitRequest.PatientID + '/' + hcueOfflineServiceUrl.listPatientCase, PatientVisitRequest);
            offlinePost.success(function (data) {
                //console.log(JSON.stringify(data));
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                })
        }

    };

    //Lab by CaseId Past Records 

    this.GetPatientCaseIdLabPastRecord = function (PatientVisitRequest, patiendcaseid, RowID, callbackSuccess, callbackError) {
        console.log(JSON.stringify(patiendcaseid));
        if (patiendcaseid !== undefined) {
            PatientVisitRequest.PatientCaseID = patiendcaseid;
            $http.post(hcueServiceUrl.getPatientCaseDetails, PatientVisitRequest, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            $http.post(hcueServiceUrl.getPatientCaseDetails, PatientVisitRequest, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })
        }

    };

    //File patient past record

    this.GetPatientFilePastRecord = function (PatientVisitRequest, patiendcaseid, RowID, callbackSuccess, callbackError) {
        console.log(JSON.stringify(patiendcaseid));
        if (patiendcaseid !== undefined) {
            PatientVisitRequest.PatientCaseID = patiendcaseid;
            PatientVisitRequest.RowID = RowID;

            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + PatientVisitRequest.PatientID + '/' + hcueOfflineServiceUrl.listPatientCase, PatientVisitRequest);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + PatientVisitRequest.PatientID + '/' + hcueOfflineServiceUrl.listPatientCase, PatientVisitRequest);
            offlinePost.success(function (data) {
                //console.log(JSON.stringify(data));
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                })
        }

    };

    this.GetPatientFileVideoRecord = function (PatientVisitRequest, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listPatientCase, PatientVisitRequest, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    // Lab Report Download

    this.GetLabDocumentsDownload = function (params, onupdatesuccess, onupdateerror) {
        $http.post(hcueServiceUrl.GetLabDocumentsDownload, params, header).then(function successCallback(response) {
            onupdatesuccess(response);
        }, function errorCallback(response) {
            onupdateerror(response);
        });
    };


    // File Report Download

    this.GetFileDocumentsDownload = function (params, onupdatesuccess, onupdateerror) {
        console.log(JSON.stringify(params));
        $http.post(hcueServiceUrl.GetFileDownload, params, header).then(function successCallback(response) {
            //console.log(JSON.stringify(response));
            onupdatesuccess(response);
        }, function errorCallback(response) {
            onupdateerror(response);
        });
    };

    this.getPastConsultationData = function (doctorid, patientid, patienthistcaseid, pagenumber, callbacksuccess, callbackerror) {
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {

            var hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            $http.post(hcueServiceUrl.listPatientCase, { "DoctorID": doctorid, "PageSize": 10, "PageNumber": pagenumber, "PatientID": patientid, "HospitalCD": hospcd }, header)
                .success(function (data) {
                    //console.log(JSON.stringify(data));
                    if (data.count != 0) {
                        if (data.rows != undefined) {
                            var response = $filter("filter")(data.rows, { "caseID": patienthistcaseid })[0];
                            //console.log(JSON.stringify(response));
                            callbacksuccess(response)
                        }
                    }
                })
                .error(function (error) {
                    //console.log(JSON.stringify(error));
                    callbackerror(error);
                });
        } else {
            $http.post(hcueServiceUrl.listPatientCase, { "DoctorID": doctorid, "PageSize": 10, "PageNumber": pagenumber, "PatientID": patientid }, header)
                .success(function (data) {
                    //console.log(JSON.stringify(data));
                    if (data.count != 0) {
                        if (data.rows != undefined) {
                            var response = $filter("filter")(data.rows, { "caseID": patienthistcaseid })[0];
                            //console.log(JSON.stringify(response));
                            callbacksuccess(response)
                        }
                    }
                })
                .error(function (error) {
                    //console.log(JSON.stringify(error));
                    callbackerror(error);
                });
        }
    };

    // Get OPT reference Table
    this.GetOPTTable = function (onsuccess, onerror) {
        $http.get(hcueServiceUrl.getOPTTable, header).then(function successCallback(response) {
            //console.log(JSON.stringify(response));
            onsuccess(response);
        }, function errorCallback(response) {
            onerror(response);
        });
    }

    this.GetPreviousServicesinPastRecord = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.GetListDetailsForNurse, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.GetPatientEstimationRecord = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listPatientCase, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
}]);

hCueDoctorWebServices.service('hcueDoctorAppointmentService', ['$http', 'hcueServiceUrl', 'hcueDoctorDataService', 'hcueOfflineServiceUrl', 'hcueDoctorLoginService', '$filter', function ($http, hcueServiceUrl, hcueDoctorDataService, hcueOfflineServiceUrl, hcueDoctorLoginService, $filter) {

    this.WebAppointmentAdd = function (params, callbackSuccess, callbackError) {
        // var params = {
        // AppointmentStatus: "B",
        // StartTime: starttime, EndTime: endtime, DoctorID: doctorid, DayCD: daycd, VisitUserTypeID: "OPATIENT",  DoctorVisitRsnID: "OTHDNT",
        // ConsultationDt: consultdate, AddressConsultID: addressconsultid,
        // PatientID: patientid, USRId: doctorid, USRType: "DOCTOR"
        // };
        // 
        //alert(JSON.stringify(params));
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined) {
            var hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            var branchcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode;
            params.HospitalCD = hospcd;
            params.BranchCD = branchcd;
            console.log("A1");
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.bookAppointment, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.docAddAppointment, params, header);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            console.log("A2");
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.bookAppointment, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.docAddAppointment, params, header);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                })
        }
    }

    this.getWeekwiseMonthwiseAppointments = function (params, callbackSuccess, callbackError) {
        var offlineCollection = {};
        console.log(JSON.stringify(params));
        //if (blnChange == true && !JSON.parse(localStorage.getItem("isAppOnline")))
        if (!JSON.parse(localStorage.getItem("isAppOnline"))) {
            var offlinePost = $http.get(localStorage.getItem("offlineURL") + '/pluck/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getDoctorAvailAppointment + '?searchkey=AddressID' + '&search=' + params.AddressID);
            offlinePost.success(function (data) {
                offlineCollection = { 'rows': data, 'count': data.length };
                console.log(JSON.stringify(offlineCollection));

                callbackSuccess(offlineCollection);

            }).error(function (data) {
                callbackError(data);
            });
        } else {
            $http.post(hcueServiceUrl.getDoctorAvailableAppointments, params, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })
        }

    }

    this.getClinicDoctors = function (params, callbackSuccess, callbackError) {
        var offlinepost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getClinicDoctors, params, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getClinicDoctors, params);
        offlinepost.success(function (data) {

            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            })

    }

    //for consulted and supervisor doctors to get
    this.GetDoctorForDropdown = function (params, callbackSuccess, callbackError) {
        var offlinepost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.GetDoctorForDropdown, params, header) : $http.get(hcueOfflineServiceUrl.offlineHostURL + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.GetDoctorForDropdown, params);
        offlinepost.success(function (data) {

            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            })

    }
    //ia = > This function will not send sms .It contain extra param SendSMS

    this.AddImmediateApointment = function (starttime, endtime, doctorid, daycd, consultdate, patientid, addressid, callbackSuccess, callbackError) {
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo != undefined) {
            var hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            var branchcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode;
            var hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
            var parenthospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        }

        if (hospcd !== undefined && branchcd !== undefined) {
            var params = {
                "AppointmentStatus": "B",
                "StartTime": starttime,
                "EndTime": endtime,
                //"HospitalDetails": {
                //    "BranchCD": branchcd,
                //    "HospitalID": hospid,
                //    "HospitalCD": hospcd,
                //    "ParentHospitalID": parenthospid
                //},
                "DoctorID": doctorid,
                "DayCD": daycd,
                "VisitUserTypeID": "OPATIENT",
                "DoctorVisitRsnID": "OTHDNT",
                "ConsultationDt": consultdate,
                "AddressID": addressid,
                "PatientID": patientid,
                "USRId": doctorid,
                "USRType": "DOCTOR",
                //"SendSMS": "N",
                // "HospitalCD": hospcd,
                // "BranchCD": branchcd
            };

            console.log(JSON.stringify(params));
            var offlineCaseDetails = hcueDoctorLoginService.getOfflineCaseDetails();
            offlineCaseDetails.AuditLog.AddAppointment = params;
            hcueDoctorLoginService.setOfflineCaseDetails(offlineCaseDetails);

            //var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.bookAppointment, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorid + '/' + hcueOfflineServiceUrl.docAddAppointment, params, header); 
            if (JSON.parse(localStorage.getItem("isAppOnline"))) {
                var offlinePost = $http.post(hcueServiceUrl.addUpdateAppointment, params, header);
                offlinePost.success(function (data) {
                    callbackSuccess(data);
                })
                    .error(function (data) {
                        callbackError(data);
                    })
            } else {
                data = { "message": "Save data successfull" };
                callbackSuccess(data);
            }

        } else {
            var params = {
                "AppointmentStatus": "B",
                "StartTime": starttime,
                "EndTime": endtime,
                "DoctorID": doctorid,
                "DayCD": daycd,
                "VisitUserTypeID": "OPATIENT",
                "DoctorVisitRsnID": "OTHDNT",
                "ConsultationDt": consultdate,
                "AddressID": addressid,
                "PatientID": patientid,
                "USRId": doctorid,
                "USRType": "DOCTOR",
                //"SendSMS": "N"
            };
            console.log("789");
            var offlineCaseDetails = hcueDoctorLoginService.getOfflineCaseDetails();
            offlineCaseDetails.AuditLog.AddAppointment = params;
            hcueDoctorLoginService.setOfflineCaseDetails(offlineCaseDetails);

            if (JSON.parse(localStorage.getItem("isAppOnline"))) {
                var offlinePost = $http.post(hcueServiceUrl.addUpdateAppointment, params, header);
                offlinePost.success(function (data) {
                    callbackSuccess(data);
                })
                    .error(function (data) {
                        callbackError(data);
                    })
            } else {
                data = { "message": "Save data successfull" };
                callbackSuccess(data);
            }

            // var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.bookAppointment, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.docAddAppointment, params, header);
            // offlinePost.success(function (data) {
            //     callbackSuccess(data);
            // })
            //.error(function (data) {
            //    callbackError(data);
            //})
        }
    }

    //Nurse flow
    //
    this.AddApointmentNurseflow = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.addUpdateAppointment, params, header)
            .success(function (Data) {
                callbackSuccess(Data);
            })
            .error(function (Data) {
                callbackSuccess(Data);
            })

    }


    //


    this.AddApointment = function (starttime, endtime, doctorid, daycd, consultdate, patientid, addressid, visitreason, VisitRsnType, Sendemail, callbackSuccess, callbackError) {
        var tmpOfflineCaseDetails = hcueDoctorLoginService.getOfflineCaseDetails();
        if (!JSON.parse(localStorage.getItem("isAppOnline"))) {
            if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID !== undefined) {
                tmpOfflineCaseDetails.ParentHospitalID = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
                tmpOfflineCaseDetails.HospitalID = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
                tmpOfflineCaseDetails.USRId = doctorid;
                tmpOfflineCaseDetails.AuditLog.ParentHospitalID = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
            }

            tmpOfflineCaseDetails.USRId = doctorid;
            tmpOfflineCaseDetails.DoctorID = doctorid;
            tmpOfflineCaseDetails.AuditLog.RequestBy = doctorinfo.doctordata.doctor[0].FullName;
            tmpOfflineCaseDetails.AuditLog.RequestDate = $filter('date')(new Date(), "yyyy-MM-dd");
            tmpOfflineCaseDetails.AuditLog.ConsultantDate = $filter('date')(new Date(consultdate), "yyyy-MM-dd");

            function getLength(obj) {

                if (obj !== undefined) {
                    return Object.keys(obj).length;
                } else {
                    return 0;
                }
            }

            if (getLength(tmpOfflineCaseDetails.AuditLog.UpdateAppointment) == 0) {
                delete tmpOfflineCaseDetails.AuditLog["UpdateAppointment"];
                console.log(JSON.stringify(tmpOfflineCaseDetails));
            }
            if (getLength(tmpOfflineCaseDetails.AuditLog.UpdatePatient) == 0) {
                delete tmpOfflineCaseDetails.AuditLog["UpdatePatient"];
                console.log(JSON.stringify(tmpOfflineCaseDetails));
            }


            if (getLength(tmpOfflineCaseDetails.AuditLog.AddPatient) == 0 || tmpOfflineCaseDetails.AuditLog.CaseSummary == undefined && tmpOfflineCaseDetails.AuditLog.CaseSummary.PatientCase == undefined && tmpOfflineCaseDetails.AuditLog.CaseSummary.PatientCase.PatientID == undefined && tmpOfflineCaseDetails.AuditLog.CaseSummary.PatientCase.PatientID > 0) {
                delete tmpOfflineCaseDetails.AuditLog["AddPatient"];
                console.log(JSON.stringify(tmpOfflineCaseDetails));
            }
        }


        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined) {
            var hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            var branchcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode;
            var hospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalID;
            var parenthospid = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;

            var params = {
                "AppointmentStatus": "B",
                "StartTime": starttime,
                "EndTime": endtime,
                //"HospitalDetails": {
                //    "BranchCD": branchcd,
                //    "HospitalID": hospid,
                //    "HospitalCD": hospcd,
                //    "ParentHospitalID": parenthospid
                //},
                "DoctorID": doctorid,
                "DayCD": daycd,
                "VisitUserTypeID": "OPATIENT",
                "ConsultationDt": consultdate,
                "AddressID": addressid,
                "PatientID": patientid,
                "USRId": doctorid,
                "USRType": "DOCTOR",
                "OtherVisitRsn": visitreason,
                "DoctorVisitRsnID": VisitRsnType,
                "SendNotification": {
                    "sendPatientSMS": "Y",
                    "sendPatientEmail": Sendemail,
                    "sendDoctorReminderSms": "Y",
                    "sendDoctorPushNotification": "Y"
                },
                // "HospitalCD": hospcd,
                // "BranchCD": branchcd
            };
            tmpOfflineCaseDetails.AuditLog.AddAppointment = params;
            hcueDoctorLoginService.setOfflineCaseDetails(tmpOfflineCaseDetails)

            console.log("A4");
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addUpdateAppointment, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorid + '/' + hcueOfflineServiceUrl.docAddAppointment, tmpOfflineCaseDetails, header);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            var params = {
                "AppointmentStatus": "B",
                "StartTime": starttime,
                "EndTime": endtime,
                "DoctorID": doctorid,
                "DayCD": daycd,
                "VisitUserTypeID": "OPATIENT",
                "ConsultationDt": consultdate,
                "AddressID": addressid,
                "PatientID": patientid,
                "USRId": doctorid,
                "USRType": "DOCTOR",
                "OtherVisitRsn": visitreason,
                "DoctorVisitRsnID": VisitRsnType,
                "SendNotification": {
                    "sendPatientSMS": "Y",
                    "sendPatientEmail": "N",
                    "sendDoctorReminderSms": "Y",
                    "sendDoctorPushNotification": "Y"
                },
            };

            tmpOfflineCaseDetails.AuditLog.AddAppointment = params;
            hcueDoctorLoginService.setOfflineCaseDetails(tmpOfflineCaseDetails)

            console.log("A5");
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addUpdateAppointment, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorid + '/' + hcueOfflineServiceUrl.docAddAppointment, tmpOfflineCaseDetails, header);
            offlinePost.success(function (data) {
                if (!JSON.parse(localStorage.getItem("isAppOnline"))) {
                    if (tmpOfflineCaseDetails.AuditLog.AddPatient !== undefined && tmpOfflineCaseDetails.AuditLog.AddPatient.offlineDBID !== undefined) {

                        var offlineAddedPatUpdate = $http.post(localStorage.getItem("offlineURL") + '/data/' + tmpOfflineCaseDetails.AuditLog.AddPatient.offlineDBID + '?status=1');
                        offlineAddedPatUpdate.success(function (statusUpdateData) {
                            offlineDBID = 0;
                            callbackSuccess(data);
                        })
                            .error(function (statusUpdateData) {
                                callbackError(data);
                            });
                    }
                } else {
                    callbackSuccess(data);
                }

            })
                .error(function (data) {
                    callbackError(data);
                })
        }
    }

    //this.UpdateAppointment = function (appointmentobj, doctorid, consultDate, status, callbackSuccess, callbackError) {

    //var param = {
    //AppointmentID: appointmentobj.AppointmentID, AddressConsultID: appointmentobj.AddressConsultID,
    //DayCD: appointmentobj.DayCD,ConsultationDt: consultDate,StartTime: appointmentobj.StartTime,EndTime: appointmentobj.EndTime,
    //PatientID: appointmentobj.PatientID,
    // DoctorVisitRsnID: appointmentobj.DoctorVisitRsnID,OtherVisitRsn: appointmentobj.OtherVisitRsn,
    //AppointmentStatus: status,DoctorID: doctorid,USRType: "DOCTOR",USRId: 0
    //};

    this.UpdateAppointment = function (starttime, endtime, doctorid, daycd, consultdate, patientid, addressid, appointmentid, visitreason, VisitRsnType, sendSms, sendMail, callbackSuccess, callbackError) {
        var param = {
            "AppointmentID": appointmentid,
            "AddressID": addressid,
            "DayCD": daycd,
            "ConsultationDt": consultdate,
            "StartTime": starttime,
            "EndTime": endtime,
            "PatientID": patientid,
            "OtherVisitRsn": visitreason,
            "AppointmentStatus": "B",
            "DoctorID": doctorid,
            "USRType": "PATIENT",
            "USRId": patientid,
            "DoctorVisitRsnID": VisitRsnType,
            "VisitUserTypeID": "OPATIENT",
            "SendNotification": {
                "sendPatientSMS": sendSms,
                "sendPatientEmail": sendMail,
                "sendDoctorReminderSms": "Y",
                "sendDoctorPushNotification": "Y"
            },

        };
        //$http.post(hcueServiceUrl.updtDoctorAppointments, param, header)
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addUpdateAppointment, param, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.updatePatientAppointments, param, header);
        offlinePost.success(function (data) {
            if (data != null) {
                callbackSuccess(data, status);
            }
        })
            .error(function (data) {
                callbackerror(data, status);
            });
    }

    this.CancelAppointment = function (appointmentobj, addressConsultID, dayCD, doctorid, consultDate, status, startTime, endTime, VisitRsnType, sendSms, sendMail, callbackSuccess, callbackError) {

        // alert("  this.CancelAppointmen");

        var param = {
            "AppointmentID": appointmentobj.AppointmentID,
            "AddressConsultID": addressConsultID,
            "DayCD": dayCD,
            "ConsultationDt": consultDate,
            "StartTime": startTime,
            "EndTime": endTime,
            "PatientID": appointmentobj.PatientID,
            "AppointmentStatus": status,
            "DoctorID": doctorid,
            "USRType": "DOCTOR",
            "USRId": appointmentobj.PatientID,
            "DoctorVisitRsnID": VisitRsnType,
            "VisitUserTypeID": "OPATIENT",
            //"NotifySMS":sendSms,
            //"NotifyMail":sendMail
        };

        //$http.post(hcueServiceUrl.updtDoctorAppointments, param, header)
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addUpdateAppointment, param, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.updatePatientAppointments, param);
        offlinePost.success(function (data) {
            if (data != null) {
                callbackSuccess(data, status);
            }
        })
            .error(function (data) {
                callbackerror(data, status);
            });
    }
    this.AddUpdateEvent = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addUpdateEvent, params, header)
            .success(function (data) {
                callbackSuccess(data, status);
            })
            .error(function (data) {
                callbackError(data, status);
            });
    }
    this.GetNewBookedAppointment = function (dayCode, clinicaddresslist, consultDate, callbackSuccess, callbackError) {
        var addressConsultID = clinicaddresslist;

        if (addressConsultID.length == 0) {
            callbackError(null);
            return;
        }
        $http.post(hcueServiceUrl.getDoctorsAppointments, { Status: 'B', AddressConsultID: addressConsultID, ConsultationDt: consultDate, ShowDueAmount: "Y", Version: "V1" }, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }
    this.GetBookedAppointment = function (dayCode, clinicaddresslist, consultDate, callbackSuccess, callbackError) {
        GetAppointment('B', dayCode, clinicaddresslist, consultDate, callbackSuccess, callbackError);
    }

    this.GetPastAppointment = function (dayCode, clinicaddresslist, consultDate, callbackSuccess, callbackError) {
        GetAppointment('E', dayCode, clinicaddresslist, consultDate, callbackSuccess, callbackError);
    }

    function GetAppointment(status, dayCode, clinicaddresslist, consultDate, callbackSuccess, callbackError) {
        //Check for addressid working on the daycode, else return empty and doesnt make any post back to the server
        var addressConsultID = clinicaddresslist;

        if (addressConsultID.length == 0) {
            callbackError(null);
            return;
        }
        $http.post(hcueServiceUrl.getDoctorsAppointments, { Status: status, AddressConsultID: addressConsultID, ConsultationDt: consultDate, ShowDueAmount: "Y" }, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }
    this.AddApointmentHospcode = function (Source, SubSource, hospcode, branchcd, hospid, parenthospid, starttime, endtime, doctorid, userid, AddressConsultID, daycd, consultdate, patientid, addressid, visitreason, VisitRsnType, sendSms, sendMail, recurringAppointment, callbackSuccess, callbackError) {

        if (hospcode !== undefined && branchcd !== undefined) {
            var params = {
                AppointmentStatus: "B",
                StartTime: starttime,
                EndTime: endtime,
                DoctorID: doctorid,
                AddressConsultID: AddressConsultID,
                DayCD: daycd,
                VisitUserTypeID: "OPATIENT",
                ConsultationDt: consultdate,
                PatientID: patientid,
                USRId: doctorid,
                USRType: "DOCTOR",
                AddressID: addressid,
                OtherVisitRsn: visitreason,
                DoctorVisitRsnID: VisitRsnType,
                SendNotification: {
                    sendPatientSMS: sendSms,
                    sendPatientEmail: sendMail,
                    sendDoctorReminderSms: "Y",
                    sendDoctorPushNotification: "Y"
                },
                ReferralSourceDtls: { ReferralID: parseInt(Source), SubReferralID: parseInt(SubSource) },
            };
            if (Source == '' || Source == undefined) {
                delete params.ReferralSourceDtls
            }
            if (recurringAppointment != "") {
                params.AppointmentOtherDetails = recurringAppointment;
            }

            $http.post(hcueServiceUrl.addUpdateAppointment, params, header)
                .success(function (data) {
                    callbackSuccess(data);

                })
                .error(function (data) {
                    callbackError(data);
                })
        } else {
            var params = {
                AppointmentStatus: "B",
                StartTime: starttime,
                EndTime: endtime,
                DoctorID: doctorid,
                DayCD: daycd,
                VisitUserTypeID: "OPATIENT",
                ConsultationDt: consultdate,
                AddressID: addressid,
                AddressConsultID: AddressConsultID,
                PatientID: patientid,
                USRId: doctorid,
                USRType: "DOCTOR",
                OtherVisitRsn: visitreason,
                DoctorVisitRsnID: VisitRsnType,
                SendNotification: {
                    sendPatientSMS: sendSms,
                    sendPatientEmail: sendMail,
                    sendDoctorReminderSms: "Y",
                    sendDoctorPushNotification: "Y"
                },
                ReferralSourceDtls: { ReferralID: parseInt(Source), SubReferralID: parseInt(SubSource) },
            };

            if (Source == '' || Source == undefined) {
                delete params.ReferralSourceDtls
            }
            if (recurringAppointment != "") {
                params.AppointmentOtherDetails = recurringAppointment;
            }

            $http.post(hcueServiceUrl.addUpdateAppointment, params, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                })

        }
    }


}])

hCueDoctorWebServices.service('hcueNotesDrawingService', ['$http', '$q', 'hcueServiceUrl', function ($http, $q, hcueServiceUrl) {

    this.SaveNotesImage = function (patientid, documentid, imagestr, callbackSuccess, callbackError) {
        var params = { PatientID: patientid, DocumentNumber: documentid, ImageStr: imagestr };

        var dbOperation = [];
        dbOperation.push(SaveImage(params));

        $q.all(dbOperation)
            .then(function (resp) {

                if (typeof resp == "object" && resp[0].data != null) {
                    callbackSuccess(resp[0].data);
                } else {
                    callbackError(resp);
                }
            })
            .catch(function (data) {
                callbackError(data);
            });

    }

    //this.GetNotesImage = function (patientid,caseid, documentid, callbackSuccess, callbackError) {
    //var param = { PatientID: patientid, PatientCaseID: caseid, DocumentNumber: documentid };
    this.GetNotesImage = function (doctorid, patientid, caseid, documentid, uploadType, callbackSuccess, callbackError) {
        //var param = { PatientID: patientid, PatientCaseID: caseid, DocumentNumber: documentid };
        var param = {
            "USRType": "DOCTOR",
            "UpLoadType": uploadType,
            "PatientID": patientid,
            "PatientCaseID": caseid,
            "USRId": doctorid,
            "DocumentNumber": documentid
        };
        var dbOperation = [];
        dbOperation.push($http.post(hcueServiceUrl.getNotesDrawing, param, header));

        $q.all(dbOperation)
            .then(function (resp) {

                if (typeof resp == "object" && resp[0].data != null) {
                    callbackSuccess(resp[0].data);
                } else {
                    callbackError(resp);
                }
            })
            .catch(function (data) {
                callbackError(data);
            });
    }

    function SaveImage(params) {
        return $http.post(hcueServiceUrl.addDrawing, params, imageheader)

    }
}])

hCueDoctorWebServices.service('hcueDoctorProfileService', ['$http', '$q', 'hcueServiceUrl', 'hcueRegisterDoctorService', 'hcueDoctorDataService', 'hcueOfflineServiceUrl', function ($http, $q, hcueServiceUrl, hcueRegisterDoctorService, hcueDoctorDataService, hcueOfflineServiceUrl) {

    this.GetDoctorInformation = function (doctorId, callback, callbackError) {
        var urlget = hcueServiceUrl.getDoctorInfo + doctorId + "/getDoctor";

        $http.get(urlget, header)
            .success(function (data) {
                callback(data);
            })
            .error(function (data) { callbackError(data); });
    }
    this.SaveProfile = function (doctorinfo, page, callbackSuccess, callbackError) {

        var doctorDetails = { DoctorID: doctorinfo.doctorid, TermsAccepted: "Y" };
        var params = {
            USRType: "DOCTOR",
            USRId: doctorinfo.doctorid,
            doctorDetails: doctorDetails
        };
        var special = GetKeyValuePairSpceciality(doctorinfo.specialization);
        var custom_speciality = GetKeyValuePairCustomSpceciality(doctorinfo.specialization);
        var docpubupdate = [];
        var docAch = [];
        var docEdu = [];
        var i;
        //----------Page.2----About, Gender, Date of birth page
        if (page == 0) {
            var areacode = 1;
            var statecode = 1;
            if (doctorinfo.MobileNumber.toString().length > 9) {
                statecode = parseFloat(doctorinfo.MobileNumber.toString().substring(0, 4));
                areacode = parseFloat(doctorinfo.MobileNumber.toString().substring(4, 10));
            }

            doctorDetails = {
                DoctorID: doctorinfo.doctorid,
                TermsAccepted: "Y",
                FirstName: doctorinfo.Name,
                FullName: doctorinfo.Name,
                SpecialityCD: special, //{ 1: doctorinfo.specialization.DoctorSpecialityID },
            }
            params.DoctorEmail = [{ EmailID: doctorinfo.Email, EmailIDType: 'P', PrimaryIND: "Y" }],
                params.DoctorPhone = [{ "PhAreaCD": 1, "FullNumber": doctorinfo.MobileNumber, "PhCntryCD": parseFloat(doctorinfo.PhCntryCD), "PhNumber": parseFloat(doctorinfo.MobileNumber), "PhType": "M", "PrimaryIND": "Y", "PhStateCD": statecode }]
            params.CustomSpecialityDesc = custom_speciality
        } else if (page == 1) {
            if (doctorinfo.DOB !== null && doctorinfo.DOB !== "Invalid date") {
                doctorDetails = {
                    DoctorID: doctorinfo.doctorid,
                    TermsAccepted: "Y",
                    Exp: parseInt(doctorinfo.Experience),
                    About: { "1": doctorinfo.About },
                    Gender: doctorinfo.Gender,
                    DOB: doctorinfo.DOB,
                    MemberID: { 1: doctorinfo.MedicalRegNumber }
                }
            } else {
                doctorDetails = {
                    DoctorID: doctorinfo.doctorid,
                    TermsAccepted: "Y",
                    Exp: parseInt(doctorinfo.Experience),
                    About: { "1": doctorinfo.About },
                    Gender: doctorinfo.Gender,
                    MemberID: { 1: doctorinfo.MedicalRegNumber }
                }
            }

        } else if (page == 2) {

            for (i = 0; i < doctorinfo.DoctorPublishing.length; i++) {

                docpubupdate.push({
                    RowID: i,
                    Year: doctorinfo.DoctorPublishing[i].Year,
                    Indicator: "P",
                    Name: doctorinfo.DoctorPublishing[i].Name
                });
            }
            for (i = 0; i < doctorinfo.DoctorAchievements.length; i++) {

                docAch.push({
                    RowID: i,
                    Year: doctorinfo.DoctorAchievements[i].Year,
                    Indicator: "A",
                    Name: doctorinfo.DoctorAchievements[i].Name
                });
            }
            for (i = 0; i < doctorinfo.doctorEducation.length; i++) {
                var YearOfPassedOutval = doctorinfo.doctorEducation[i].edu_startyear + "-" + doctorinfo.doctorEducation[i].edu_endyear

                docEdu.push({
                    RowID: i,
                    EducationName: doctorinfo.doctorEducation[i].EducationName,
                    InstituteName: doctorinfo.doctorEducation[i].InstituteName,
                    YearOfPassedOut: YearOfPassedOutval
                });
            }
            var docEduQualification = [];
            for (i = 0; i < doctorinfo.doctorEducation.length; i++) {
                docEduQualification.push({
                    RowID: i,
                    EducationName: doctorinfo.doctorEducation[i].EducationName,
                    InstituteName: doctorinfo.doctorEducation[i].InstituteName,
                    YearOfPassedOut: doctorinfo.doctorEducation[i].FullName
                });
            }
            var docQualificationCollection = GetKeyValuePairEducationName(docEduQualification);
            doctorDetails = {
                DoctorID: doctorinfo.doctorid,
                TermsAccepted: "Y",
                Qualification: docQualificationCollection, //GetKeyValuePair(doctorinfo.Qualification),
                Services: GetKeyValuePair(doctorinfo.Services),
                Membership: GetKeyValuePair(doctorinfo.Memberships),
                Awards: GetKeyValuePair(doctorinfo.Awards)
            }
            var DoctorPublishing = docpubupdate;
            var DoctorAchievements = docAch;
            var DoctorEducation = docEdu;
        }
        params.doctorDetails = doctorDetails;
        params.DoctorPublishing = DoctorPublishing;
        params.DoctorAchievements = DoctorAchievements;
        params.DoctorEducation = DoctorEducation;

        //----------Page.2----About, Gender, Date of birth page
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.updateDoctor, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.updateDoctor, params);
        offlinePost.success(function (data) {

            callbackSuccess(data);
        })
            .error(function (data) { callbackError(data); });
    }
    this.SaveBusinessHours = function (doctorinfo, hospitalinfo, countryCodeValue, callbackSuccess, callbackError) {

        var hospital_phone = [];
        var phType;
        var getCountryCode;
        for (var i = 0; i < hospitalinfo.AddressCommunication.length; i++) {
            getCountryCode = hospitalinfo.AddressCommunication[i].PhCntryCD;
            if (hospitalinfo.AddressCommunication[i].PhType == "M") {
                phType = hospitalinfo.AddressCommunication[i].PhNumber.substring(0, 4);
            } else {
                phType = hospitalinfo.AddressCommunication[i].PhAreaCD;
            }
            hospital_phone.push({
                "PhCntryCD": countryCodeValue.toString(),
                "PhStateCD": "78",
                "PhAreaCD": phType.toString(),
                "PhNumber": hospitalinfo.AddressCommunication[i].PhNumber.toString(),
                "PhType": hospitalinfo.AddressCommunication[i].PhType.toString(),
                "RowID": i,
                "PrimaryIND": "Y",
                "PublicDisplay": "Y",
            });
        }

        var dbOperation = [];
        var UpdateBusinessHrs = [];
        var addBusinessHrs = []
        var updateItemexists = false;
        var addItemexists = false;

        var doctorid = doctorinfo.doctorid;
        var hospital = {
            HospitalId: parseFloat(hospitalinfo.HospitalId),
            //  AddressConsultID: doctorinfo.AddressConsultID,
            Active: hospitalinfo.Active,
            MinPerCase: hospitalinfo.MinPerCase,
            Fees: hospitalinfo.Fees,
            ClinicName: hospitalinfo.ClinicName,
            Country: hospitalinfo.Country,
            State: hospitalinfo.State,
            Address: hospitalinfo.Address,
            Address1: hospitalinfo.Address1,
            Location: hospitalinfo.Location,
            CityTown: hospitalinfo.CityTown,
            AddressCommunication: hospital_phone,
            Latitude: hospitalinfo.Latitude.toString(),
            Longitude: hospitalinfo.Longitude.toString(),
            ClinicImages: GetKeyValuePair(hospitalinfo.ClinicImages),
            businesshoursUI: hospitalinfo.businesshoursUI, //Fees,MinPerCase,
            CurrencyCode: hospitalinfo.CurrencyCode,
            CountryCode: countryCodeValue
        }

        angular.forEach(hospital.businesshoursUI, function (hr) {
            hr.MinPerCase = parseFloat(hospital.MinPerCase);
            hr.Fees = parseFloat(hospital.Fees);
            /*if (hr.AddressConsultID == 0) {
                addBusinessHrs.push(hr);
                addItemexists = true;
            }
            else {
                UpdateBusinessHrs.push(hr);
                updateItemexists = true;
            }*/
            UpdateBusinessHrs.push(hr);
            updateItemexists = true;
        });

        hcueRegisterDoctorService.AddHospitalAddress(doctorinfo, hospital, header)
            .success(function (data) {
                if (hospital.HospitalId == 0) {
                    var hospitalid = hcueRegisterDoctorService.GetNewlyAddedHospitalAddressId(data, hospital);
                    hospital.HospitalId = hospitalid;
                }
                var param = {
                    updateItemexists: updateItemexists,
                    doctorid: doctorid,
                    hospital: hospital,
                    UpdateBusinessHrs: UpdateBusinessHrs,
                    addItemexists: addItemexists,
                    addBusinessHrs: addBusinessHrs,
                    doctorinfo: doctorinfo
                }
                AddUpdateHours(param, doctorinfo, hospital, data.DoctorSetting, callbackSuccess, callbackError);

            });
    }
    //Doc profile image update 
    this.UpdateProfileImage = function (id, data, callbackSuccess, callbackError) {
        var params = { "DoctorID": parseFloat(id), "ImageStr": data };
        $http.post(hcueServiceUrl.imageUpload, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    //Doc profile image delete 
    this.deleteProfileImage = function (id, callbackSuccess, callbackError) {
        var params = { "DoctorID": parseFloat(id), "isDeleted": 'Y' };
        $http.post(hcueServiceUrl.imageUpload, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    //Doc profile image listing 
    this.getDocProfImgList = function (params, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.getDoctorPost, params, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };
    //Doc clinic logo image delete 
    this.deleteHospitalImage = function (params, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.deleteHospitalImage, params, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };
    //Doc clinic logo image listing 
    this.uploadHospitalClinicImage = function (params, callbackSuccess, callbackError) {
        $('#loaderDiv').show()
        var request = new XMLHttpRequest();
        request.open("POST", hcueServiceUrl.uploadHospitalClinicImage);
        request.send(params);
        request.onload = function () {
            if (request.status == 200) {
                callbackSuccess(request.responseText);
                $('#loaderDiv').hide()
            } else {
                callbackError(request.responseText);
                $('#loaderDiv').hide()
            }
        }
        request.onerror = function () {
            callbackError(request.responseText);
            $('#loaderDiv').hide()
        }
    };

    function AddUpdateHours(param, doctorinfo, hospital, settings, callbackSuccess, callbackError) {
        var dbOperation = [];
        if (param.addItemexists) {
            dbOperation.push(hcueRegisterDoctorService.AddHospitalHours(param.doctorid, hospital.HospitalId, param.addBusinessHrs));
        }

        if (param.updateItemexists) {
            dbOperation.push(hcueRegisterDoctorService.UpdateHospitalHours(param.doctorid, hospital.HospitalId, param.UpdateBusinessHrs));
        }

        //************Seperate Added Business Hours, Updated Business Hours
        $q.all(dbOperation)
            .then(function (resp) {
                if (resp.length == 0)
                    return;

                if (resp[0].data.DoctorAddress == undefined) {
                    callbackSuccess(resp[0].data);
                    return;
                }

                var respadd = resp[resp.length - 1];
                //Check and update the local envir variables.
                var existsinginfo = doctorinfo.doctordata.doctorConsultation;
                var existingaddressinfo = doctorinfo.doctordata.doctorAddress;
                var updatedinfo = respadd.data;
                //Delete existing infor from memory
                doctorinfo.doctorConsultation = [];
                doctorinfo.doctorAddress = [];
                var updateConsult = [];
                ////console.log('existsinginfo' + JSON.stringify(existsinginfo));
                doctorinfo.DoctorSetting = settings;
                angular.forEach(existsinginfo, function (item) {
                    if (hospital.HospitalId == item.AddressID) { } else {
                        doctorinfo.doctorConsultation.push(item);
                    }
                });
                angular.forEach(existingaddressinfo, function (item) {
                    ////console.log('Address ' + JSON.stringify(item));
                    if (hospital.HospitalId == item.AddressID) {
                        //do nothing
                    } else {
                        doctorinfo.doctorAddress.push(item);
                    }
                });
                // //console.log(JSON.stringify(updatedinfo.ConsultationDetails));
                angular.forEach(updatedinfo.ConsultationDetails, function (item) {
                    doctorinfo.doctorConsultation.push(item);
                });
                doctorinfo.doctorAddress.push(updatedinfo.DoctorAddress);

                callbackSuccess(doctorinfo);

            })
            .catch(function (fallback) {
                callbackError(fallback);
            });
    }


    this.getDoctorPostData = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getPostDoctor, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    //vp added new profile clinic img


}]);

hCueDoctorWebServices.service('hcueRegisterDoctorService', ['$http', '$q', 'hcueServiceUrl', 'hcueDoctorDataService', 'hcueOfflineServiceUrl', function ($http, $q, hcueServiceUrl, hcueDoctorDataService, hcueOfflineServiceUrl) {

    this.AddDoctor = function (register, hasHospitalInfo, callbackSuccess, callbackError) { //function (register,callbackSuccess, callbackIdExist, callbackAddHospitalAddress,callbackAddHospitalHours,callbackError) {

        var areacode = 1;
        var statecode = 1;
        if (register.MobileNumber.toString().length > 9) {
            statecode = parseFloat(register.MobileNumber.toString().substring(0, 4));
            areacode = parseFloat(register.MobileNumber.toString().substring(4, 10));
        }
        var qualifications = GetKeyValuePair(register.Qualification);
        var services = GetKeyValuePair(register.services);
        var memberships = GetKeyValuePair(register.Memberships);
        var practise = GetKeyValuePair(register.Practises);
        var awards = GetKeyValuePair(register.Awards);
        // var speciality = GetKeyValuePair(register.Speciality);
        var about = { "1": register.About };

        var addparams = {
            USRType: "ADMIN",
            USRId: 125,

            "doctorDetails": {
                FirstName: register.Name,
                TermsAccepted: "Y",
                DOB: register.DOB,
                FullName: register.Name,
                DoctorPassword: register.Password,
                DoctorLoginID: register.LoginId,
                LastName: "",
                Gender: register.Gender,
                Exp: parseInt(register.Experience),
                About: about,
                MemberID: practise,
                Qualification: qualifications,
                Services: services,
                Membership: memberships,
                Awards: awards,
                SpecialityCD: { 1: register.specialization.DoctorSpecialityID }

            },
            DoctorAddress: [{
                Address1: "",
                PrimaryIND: "Y",
                Address2: register.Address,
                PinCode: 0,
                timePerPatient: register.MinConsultTime,
                ClinicName: register.ClinicName,
                Active: register.Active,
                State: "TN",
                ExtDetails: { "timePerPatient": register.MinPerCase, "PrimaryIND": "Y", "Fees": register.Fees, "Avilability": "Mon-Sat", Latitude: register.Latitude.toString(), Longitude: register.Longitude.toString() },
                Street: "",
                Country: "IND",
                AddressType: "B",
                AddressID: 0,
                "CityTown": "MAS",
                "Location": "345"
            }],
            DoctorEmail: [{ EmailID: register.Email, EmailIDType: 'P', PrimaryIND: "Y" }],
            DoctorPhone: [{ "PhAreaCD": 1, "FullNumber": register.MobileNumber, "PhCntryCD": 91, "PhNumber": parseFloat(register.MobileNumber), "PhType": "M", "PrimaryIND": "Y", "PhStateCD": statecode }]

        };

        var hospital = {
            HospitalId: 0, // doctorinfo.doctorAddress[0].AddressID,
            AddressConsultID: 0,
            MinPerCase: register.MinPerCase,
            Fees: register.Fees,
            ClinicName: register.ClinicName,
            Active: register.Active,
            Address: register.Address,
            Latitude: register.Latitude.toString(),
            Longitude: register.Longitude.toString(),
            businesshoursUI: register.businesshoursUI //Fees,MinPerCase
        };

        $http.post(hcueServiceUrl.addDoctor, addparams, header)
            .success(function (data) {

                if (data.ExceptionType == "PSQLException") {
                    callbackError(data, "Login Id already Exists");
                    return;
                } else {
                    var doctorinfo = data;
                    doctorinfo.doctorid = data.doctor[0].DoctorID;
                    doctorinfo.memberid = data.doctor[0].MemberID;


                    if (hasHospitalInfo) {
                        hospital.HospitalId = doctorinfo.doctorAddress[0].AddressID;

                        AddHospitalAddressInfo(doctorinfo, hospital)
                            .success(function (adddata) {
                                var newid = adddata.doctorAddress[0].AddressID;

                                AddHospitalConsultHour(doctorinfo.doctor[0].DoctorID, newid, hospital.businesshoursUI)
                                    .success(function (hrsdata) {
                                        callbackSuccess(doctorinfo);
                                    })
                                    .error(function (hrsdata) {
                                        callbackError(hrsdata, "Error: Please check internet connectivity");
                                    })
                            })
                            .error(function (adddata) {
                                callbackError(adddata, "Error: Please check internet connectivity");
                            })

                        // if (callbackAddHospitalAddress != null)  // callbackAddHospitalAddress => AddHospitalAddress
                        //     callbackAddHospitalAddress(doctorinfo, hospital, callbackAddHospitalHours, callbackSuccess, callbackError); // callbackAddHospitalHours => AddBusinessHours
                    }
                }

            })
            .error(function (data) {
                callbackError(data, "Error: Please check internet connectivity");
            })
    }

    this.AddHospitalAddress = function (doctorinfo, hospital) {

        return AddHospitalAddressInfo(doctorinfo, hospital);
    }

    this.AddHospitalHours = function (doctorid, newaddressid, businesshrs) {

        return AddHospitalConsultHour(doctorid, newaddressid, businesshrs);
    }
    this.UpdateHospitalHours = function (doctorid, newaddressid, businesshrs) {

        return UpdateHospitalConsultHour(doctorid, newaddressid, businesshrs);
    }

    function AddHospitalAddressInfo(doctorinfo, hospital) {
        var lat = "0";
        var lng = "0";
        if (hospital.Latitude != null) {
            lat = hospital.Latitude;
        }
        if (hospital.Longitude != null) {
            lng = hospital.Longitude;
        }
        var hospitaladdress = {
            USRType: "DOCTOR",
            USRId: doctorinfo.doctorid,
            doctorDetails: {
                DoctorID: doctorinfo.doctorid,
                //MemberID: doctorinfo.memberid,
                TermsAccepted: "Y",
                SpecialityCD: doctorinfo.SpecialityCD
                //Fee:hospital.Fees
                //MinPerCase:hospital.MinPerCase
            },
            DoctorSettings: { 'CountryCode': hospital.CountryCode, 'CurrencyCode': hospital.CurrencyCode },
            DoctorAddress: [{
                PrimaryIND: "Y", // HardCode
                AddressID: hospital.HospitalId, // HardCode
                AddressType: 'B',
                Location: hospital.Location, // doctorinfo.doctorAddress[0].Location, //Hard code
                ClinicName: hospital.ClinicName,
                Active: hospital.Active,
                Street: '',
                Address1: hospital.Address1,
                Address2: hospital.Address,
                CityTown: hospital.CityTown,
                DistrictRegion: '',
                State: hospital.State,
                PinCode: 0,
                Country: hospital.Country,
                ExtDetails: { PrimaryIND: "Y", Avilability: "Mon-Sat", Latitude: lat.toString(), Longitude: lng.toString(), ClinicImages: hospital.ClinicImages, AddressCommunication: hospital.AddressCommunication }
            }]
        };

        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.updateDoctor, hospitaladdress, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.updateDoctor, hospitaladdress);

        return offlinePost;
    }

    function AddHospitalConsultHour(doctorid, newaddressid, businesshrs) {
        var businesshrparams = {
            USRType: "DOCTOR",
            USRId: doctorid,
            DoctorID: doctorid,
            AddressID: newaddressid,
            consultationRecord: businesshrs
        };
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addConsultation, businesshrparams, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addConsultation, businesshrparams, header);

        return offlinePost;
    }

    function UpdateHospitalConsultHour(doctorid, newaddressid, businesshrs) {
        var businesshrparams = {
            USRType: "DOCTOR",
            USRId: doctorid,
            DoctorID: doctorid,
            AddressID: newaddressid,
            consultationRecord: businesshrs
        };
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.updateConsultation, businesshrparams, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.updateConsultation, businesshrparams, header);

        return offlinePost;
    }

    this.GetNewlyAddedHospitalAddressId = function (data, hospital) {
        var addressid = 0;
        angular.forEach(data.doctorAddress, function (addressitem) {
            if (hospital.ClinicName == addressitem.ClinicName && addressitem.Address2 == hospital.Address) {
                addressid = addressitem.AddressID;
                return addressid;
            }
        });
        return addressid;
    }

    function GetKeyValuePair(lists) {
        var items = {};
        var rowno = 1;
        angular.forEach(lists, function (item) {
            items[rowno] = item.name;
            rowno++;
        });
        return items;
    }


    this.getLocations = function (getLocationParams, callbackSuccess, callbackError) {
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getLocationPost, getLocationParams, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + '0' + '/' + hcueOfflineServiceUrl.getLocationPost, getLocationParams, header);
        offlinePost.success(function (data) {
            //alert(JSON.stringify(data))
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getCity = function (getCityParams, callbackSuccess, callbackError) {
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getCityPost, getCityParams, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + '0' + '/' + hcueOfflineServiceUrl.getCityPost, getCityParams, header);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getState = function (getStateParams, callbackSuccess, callbackError) {
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getStatePost, getStateParams, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + '0' + '/' + hcueOfflineServiceUrl.getStatePost, getStateParams, header);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getCountry = function (getCountryParams, callbackSuccess, callbackError) {
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getCountryPost, getCountryParams, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + '0' + '/' + hcueOfflineServiceUrl.getCountryPost, getCountryParams, header);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getreadlookup = function (callbackSuccess, callbackError) {

        var param = { "ActiveIND": "Y", "SearchText": "" };
        $http.post(hcueServiceUrl.readLookup, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}])



hCueDoctorWebServices.factory('hcueLoginStatusService', function () {

    var loggedIn = 'false';

    var userLoginDetails = {
        userToken: '',
        userId: '',
        userName: '',
        displayName: '',
        role: '',
        mobileNumber: '',
        password: '',
        loggedinStatus: '',
        rememberme: ''
    };

    var getUserLoginDetails = function () {
        return userLoginDetails;
    }
    return {
        getUserLoginDetails: getUserLoginDetails,
        getStatus: function () {
            return loggedIn;
        },
        setStatus: function (value) {

            loggedIn = value;
            userLoginDetails.loggedinStatus = loggedIn;
        }
    }
})


hCueDoctorWebServices.factory('datacookieFactory', ['$cookies', function ($cookies) {
    return {

        getItem: function (key) {

            var returnobj = [];
            if ($cookies.get(key) != null) {
                //alert('get');
                var jsstring = $cookies.getObject(key);
                returnobj = jsstring;
                //alert(JSON.stringify(returnobj));
            }

            return returnobj;
        },
        getItemString: function (key) {
            var returnobj = [];
            if ($cookies.get(key) != null) {
                var jsstring = $cookies.getObject(key);
                returnobj = jsstring;
            }
            return returnobj;
        },
        set: function (key, value) {
            //alert(key);
            $cookies.put(key, JSON.stringify(value));
        },
        remove: function (key) {
            $cookies.remove(key);
        }
    };
}])


hCueDoctorWebServices.service('pouchdbMaintenanceService', ['$log', 'pouchDB', '$window', '$http', 'hcueServiceUrl', 'hcueConsultationInfo', 'datacookieFactory', function ($log, pouchDB, $window, $http, hcueServiceUrl, hcueConsultationInfo, datacookieFactory) {
    this.createDatabse = function () {
        var db = pouchDB('hCueMedicine');

        var db = pouchDB('hCueLab');

    }

    function insertData(input) {
        var db = pouchDB('hCueMedicine');
        db.bulkDocs(input).then(function (resultdata) { }).catch(function (resulterr) {
            //console.log(resulterr);

        });

    }

    this.deleteDatabase = function () {
        var db = pouchDB('hCueMedicine');
        var db = pouchDB('hCueLab');
        db.destroy('hCueMedicine');
        //db.sync();
        var db = pouchDB('hCueLab');
        db.destroy('hCueLab');
    }

    this.addBulkData = function () {
        //  var db = pouchDB('hCueMedicine');

        var userLoginDetails = datacookieFactory.getItem('loginData');
        //console.log(JSON.stringify(userLoginDetails));

        //console.log(userLoginDetails.specialisationCD);
        var medUrl = 'MedicineFull.txt?loaderStatus=false';
        if (userLoginDetails.specialisationCD == 'HYM') {
            medUrl = 'Homeo_Medicine.txt'
            //console.log(medUrl);
        }
        //console.log(medUrl);
        var inputData;
        $http({
            //url: 'New1000test.txt',
            url: medUrl,
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
            inputData = response;
            insertData(inputData);
            //alert('hhh');
        }).error(function (error) {
            inputData = 'error';
        });


        //alert(JSON.stringify(db));

    }
    //Makesh please use the below data add bulkdata for async testing
    /*  this.addBulkData = function() {

        var db = pouchDB('hCueMedicine');
        db.bulkDocs(
        
            [{Medicine:'ABAMUNE',Manufacturer:'CIPLA LTD',Strength:'300MG',MedicineType:'TAB',MRP:'1444.5',_id:'ABA0'},
             {Medicine:'ABAVIR',Manufacturer:'HETERO HC (GENX)',Strength:'300MG',MedicineType:'TAB',MRP:'1410',_id:'ABA1'},
             {Medicine:'ABAMUNE-L',Manufacturer:'EMCURE (ARV)',Strength:'300MG',MedicineType:'TAB',MRP:'1500',_id:'ABA2'},
             {Medicine:'ABACAVIR COMBINATIONS',Manufacturer:'CIPLA LTD',Strength:'ABAC 600MG, LAMIVUD 300MG',MedicineType:'TAB',MRP:'2950',_id:'abcde'},
             {Medicine:'ACARBOSE',Manufacturer:'ORCHID (DIABETES)',Strength:'25MG',MedicineType:'TAB',MRP:'50',_id:'ACA'},
             {Medicine:'ACARBOSE',Manufacturer:'ORCHID (DIABETES)',Strength:'50MG',MedicineType:'TAB',MRP:'87',_id:'ACA'},
             {Medicine:'ACARBOSE',Manufacturer:'INVISION',Strength:'25MG',MedicineType:'TAB',MRP:'55',_id:'ACA'},
             {Medicine:'ACARBOSE',Manufacturer:'INVISION',Strength:'50MG',MedicineType:'TAB',MRP:'85',_id:'ACA'},
             {Medicine:'ACARBOSE',Manufacturer:'JOHNLEE(VISTA)',Strength:'25MG',MedicineType:'TAB',MRP:'70',_id:'ACA'}]
        
        
        ).then (function(resultdata){
        }).catch(function(resulterr){
            //console.log(resulterr);

        });
    }*/

    //for async call (change the function to accept term)


    this.getBulkData = function (term) {

        var db = pouchDB('hCueMedicine');
        var medicinelist = [];
        db.allDocs({
            include_docs: true,
            startkey: term,
            endkey: term + '\uffff'

        }).then(function (result) {
            //alert(JSON.stringify(result));
            medicinelist.splice(0, medicinelist.length);
            angular.forEach(result.rows, function (item) {
                // //alert(JSON.stringify(item.doc));
                // //alert('here');
                // medicinelist.push({Medicine:item.doc.Medicine + "-" +item.doc.Strength
                // ,MedicineType:item.doc.MedicineType,_id:item.doc._id});//  item.doc);
                if (item.doc.Strength == "") {
                    medicinelist.push({
                        Medicine: item.doc.Medicine + item.doc.Strength,
                        MedicineType: item.doc.MedicineType,
                        _id: item.doc._id
                    });
                } else {
                    medicinelist.push({
                        Medicine: item.doc.Medicine + "-" + item.doc.Strength,
                        MedicineType: item.doc.MedicineType,
                        _id: item.doc._id
                    }); //  item.doc);
                }
            });
        }).catch(function (err) {
            //console.log(err);
        });
        return medicinelist;
    }

    this.getMedicinefromMyPharmacy = function (term, pharmaID, callbackPharmacySuccess, callbackPharmacyError) {

        // var url = 'http://hcue-api-dev.azurewebsites.net/products/search?externalID=93&searchText='+ term;
        var url = hcueServiceUrl.getPharmacyInventoryList + "externalID=" + pharmaID + "&searchText=" + term;
        var headerObj = { headers: { 'Access-Control-Allow-Origin': '*' } };
        var key = { loaderStatus: false };
        $http.get(url, key, headerObj)
            .success(function (data) {

                callbackPharmacySuccess(data);
            })
            .error(function (data) {
                callbackPharmacyError(data);
            });

    }

    this.getMedicinefromPharmacyCount = function (params, callbackdbSuccess, callbackdbError) {

        var url = 'https://hcue-api-doc-app.azurewebsites.net/products/SearchProducts'
        $http.post(url, params, header)
            .success(function (data) {
                callbackdbSuccess(data);
            })
            .error(function (data) {
                callbackdbError(data);
            });

    }



    // for lab data

    function insertLabData(input) {
        var db = pouchDB('hCueLab');
        db.bulkDocs(input).then(function (resultdata) { }).catch(function (resulterr) {
            //console.log(resulterr);

        });

    }



    this.addBulkDataLab = function () {
        //var db = pouchDB('hCueLab');

        var inputData;
        $http({
            url: 'labFull.txt',
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
            inputData = response;
            //alert(JSON.stringify(inputData));
            insertLabData(inputData);
            //alert('hhh');
        }).error(function (error) {
            inputData = 'error';
        });



    }
    var lablist = [];

    this.getBulkDataLab = function () {
        var db = pouchDB('hCueLab');
        db.allDocs({
            include_docs: true

        }).then(function (result) {
            //alert(JSON.stringify(result.rows));
            lablist.splice(0, lablist.length);
            angular.forEach(result.rows, function (item) {
                lablist.push({ TestName: item.doc.TestName, LabType: item.doc.LabType, TestGroup: item.doc.TestGroup, TestDescription: item.doc.TestDescription, _id: item.doc._id, TestUsage: item.doc.TestUsage }); //  item.doc);

            });


        }).catch(function (err) {
            //console.log(err);
        });

        return lablist;
    }
    this.getMedicinefromDB = function (params, callbackdbSuccess, callbackdbError) {


        $http.post(hcueServiceUrl.getMedicines, params, header)
            .success(function (data) {
                callbackdbSuccess(data);
            })
            .error(function (data) {
                callbackdbError(data);
            });

    }

}]);

hCueDoctorWebServices.factory('datapersistanceFactory', ['localStorageService', function (localStorageService) {
    return {

        getItem: function (key) {

            var returnobj = [];
            if (localStorageService.get(key) != null) {
                returnobj = localStorageService.get(key);
                returnobj = JSON.parse(returnobj)
            }
            //return returnobj;
            return returnobj;
        },
        /*getItemString: function (key) {
            var returnobj = [];
            if ($cookies.get(key) != null) {
                var jsstring = $cookies.getObject(key);
                returnobj = jsstring;
            }
            return returnobj;
        },*/
        getItemString: function (key) {
            var returnobj = [];
            if (localStorageService.get(key) != null) {
                var jsstring = localStorageService.get(key);
                //alert(jsstring);
                returnobj = jsstring;
            }
            return returnobj;
        },
        set: function (key, value) {
            localStorageService.set(key, JSON.stringify(value));
        },
        remove: function (key) {
            localStorageService.remove(key);
        }
    };
}])




hCueDoctorWebServices.factory('hcueDoctorLoginService', ['datapersistanceFactory', 'hcueLoginStatusService', 'datacookieFactory', 'hcueOfflineServiceUrl', function (datapersistanceFactory, hcueLoginStatusService, datacookieFactory, hcueOfflineServiceUrl) {
    //******************Used for the profile page of doctor
    /* var register = {
        DoctorID: 0, Name: '', MobileNumber: 91, Email: '', DOB: 0, specialization: '', MedicalRegNumber: '', Experience: '', LoginId: '', Password: '', ConfirmPassword: '',
        About: '',  Gender: 'M',
        Educations: [{ name: '' }],  Services: [{ name: '' }], Memberships: [{ name: '' }], Awards: [{ name: '' }], Practises: [{ name: '' }], Speciality: [{ name: '' }],
        ClinicName: '', ContactNumber: '', Fees: 0, MinPerCase: 0,  Address: '', Latitude: 0, Longitude: 0,

        Referrals: [{ ClinicName: '', DoctorName: '', Specialization: { SpecializationId: '', Name: '' }, Address1: '', Address2: '', ContactName: '', MobileNmber: '', EmailId: '' }],

        businesshoursUI: [
            { AddressConsultID: 0, DayCode: 'Monday', DayCD: 'MON', Active: 'Y', StartTimeHH: '00', StartTimeMM: '00', StartTime: '00:00 AM', EndTimeHH: '00', EndTimeMM: '00', EndTime: '21:00 PM' }
        ]
    };
  
    */
    //************Login starts
    //******************Used for persisting info when login is success
    var purpose = { "VisitUserTypeID": "OPATIENT", "VisitUserTypeDesc": "Out Patient", "ActiveIND": "Y" };

    var doctorinfo = {
        purposelist: purpose,
        specialitylist: [],
        visitreasonlist: [],
        referraldoctors: [],
        referralpharmacies: [],
        referrallabs: [],
        doctorid: 0,
        doctorloginid: '',
        selectedaddressconsultid: 0,
        name: '',
        specialitycode: '',
        groupID: '',
        hospitalID: 0,
        RoleID: '',
        DoctorSetting: {},
        doctordata: {},
        AccountAccessID: ['APPMT1', 'HELP1', 'SETT1', 'EDPR1', 'EDPR2', 'EDPR3', 'EDPR4', 'EDPR5', 'EDPR6', 'REGR1',
            'REGR2', 'REGR3', 'REGR4', 'REGR5', 'REGR6', 'DCRE1', 'SIGN1', 'SIGN2', 'PALT1', 'EMRC1', 'EMOT1', 'SEPR1', 'NTSM1', 'NTSD1', 'PRCR1', 'LAMN1',
            'SCMN1', 'ADRF1', 'SUMM1', 'SURE1', 'ADRE2', 'MYPH1', 'MYLA1', 'PAAP1', 'APED1', 'GTAP1', 'ADBR1', 'LOGI1', 'UNAU1', 'ORG1'
        ],
        isAccessRightsApplicable: false
    };
    //************Login ends'

    //************Add Patient starts
    var patientinfo = {
        doctorid: 0,
        familyid: "",
        hasphone: false,
        firstname: '',
        profileImages: '',
        lastname: '',
        dob: null,
        age: 0,
        gender: 'M',
        mobileNumber: '',
        landlineNumber: '',
        BloodGroup: '',
        phType: '',
        phAreaCD: '',
        phStdCD: '',
        alternativephone: '',
        emailid: '',
        vipFlag: '',
        stdCode: '',
        title: 'Mr',
        patType: false,
        emgymobilenumber: '',
        emgyrelationship: '',
        emgyalternatenumber: '',
        emgyname: '',
        emgytitle: 'Mr',
        othDetailmaritalstatus: 'M',
        othDetaileducation: '',
        othDetailoccupation: '',
        othDetailpaymentmode: 'OTHERS',
        othDetailreferralsource: { 'OTHERS': 'Others' },
        othrDetailNotes: "",
        patientPhone: [],
        patientAddress: [],
        Address2: "",
        LandMark: "",
        PrimaryIND: "Y",
        Latitude: "",
        Address1: "",
        DistrictRegion: "",
        Longitude: "",
        PinCode: 0,
        State: "",
        Street: "",
        Country: "",
        AddressType: "H",
        CityTown: "",
        Location: ""
    };

    var emergencycontactinfo = {
        mobilenumber: '',
        relationship: '',
        alternatenumber: '',
        name: '',
        title: 'Mr'

    };

    var patientotherdetails = {
        maritalstatus: 'M',
        education: '',
        occupation: '',
        paymentmode: 'OTHERS',
        referralsource: { 'OTHERS': 'Others' }

    };

    var patientUpdateinfo = {
        patientid: 0,
        doctorid: 0,
        hasphone: false,
        firstname: '',
        lastname: '',
        fullname: '',
        dob: null,
        age: 0,
        gender: 'M',
        phonenumber: '',
        alternativephone: '',
        emailid: '',
        address1: '',
        address2: '',
        citytown: 'MAS',
        lat: '0',
        lng: '0',
        country: 'IND',
        location: '345',
        stdCode: '',
        AdharNo: '',
        PanNo: '',
        Nationality: '',
        PassportNo: '',
        Visastatus: ''
    };

    //******************Add Appointment
    var appointmentinfo = {
        AppointmentID: 0,
        caseid: 0,
        /*, AddressConsultID: 0, DayCD: '', ConsultationDt: '', StartTime: "00:00", EndTime: "00:00",
        VisitUserTypeID: "OPATIENT", DoctorID: 0, FirstTimeVisit: "Y", DoctorVisitRsnID: "OTHDNT",
        otherreason: "", AppointmentStatus: "B", TokenNumber: "0",*/
        PatientID: 0,
        PatientDetails: {
            Patient: [], //[{ "FirstName": "", "FullName": "", "PatientID": 0, "Gender": "M", "DOB": 0, "FamilyHdID": 0, "CrtUSR": 0, "CrtUSRType": "DOCTOR", "UpdtUSR": 0, "UpdtUSRType": "DOCTOR" }],
            PatientPhone: [] //[{ "PhCntryCD": 91, "PhStateCD": 1, "PhAreaCD": 1, "PhNumber": 0, "PhType": "M", "PrimaryIND": "1" }],
            // PatientAddress: [], doctorEmail: [], PatientOtherIds: []
        }
    };
    //************Add Patient ends

    //***********Billing Info
    var billingProcedureStructure = {
        "BillDate": '',
        "RowID": 0,
        "ProcedureID": '',
        "ProcedureCost": 0,
        "ProcedureName": '',
        "DefaultNotes": '',
        "BilledCost": 0,
        "DiscountCost": 0,
        "ParentProcedureID": 1,
        "Multiplyfactor": 1,
        "AdvancePaid": 0,
        "Status": false,
        "DiscountAmount": 0,
        "DiscountType": 'PERCENT',
        "Offer": {
            "OfferCode": 'Custom',
            "OfferID": 'Custom',
            "OfferType": '',
            "ShortDescription": ''
        },
        "otherinfo": {
            "notes": '',
            "membershipamt": 0
        },
        "LinkedBillID": 0,
        "BillID": 0,
        "Balance": 0,
        "PatientCaseID": 0,
        "costStatus": true,
        "Previouspaid": 0,
        "PreviousBill": 'N',
        "ProcedureType": '',
        "BillingFrom": '',
        "ActiveBalance": "N",
        "LinkPatientCaseID": 0,
        "billedStatus": false,
        "PackageStatus": "N",
        "InvoiceID": "0",
        "BillNumber": "0",
        "InvoiceDate": '',
        "ClinicAmount": 0,
        "PreClinicAmount": 0,
        "Cgst": 0,
        "Sgst": 0,
        "totlcstaftrtax": 0,
        "Payout": {
            "Attender": 0,
            "Others": 0
        },
        "otherinfo": {
            "notes": "",
            "membershipamt": 0,
            "Status": "Init",
            "TreatmentSeqNumber": 0,
            "TaxDetails": {
                "Cgst": 0,
                "Sgst": 0
            },
            "TaxAmount": 0
        }
    };



    ////******Billing End

    //************Add Patient ends
    var patientsearchparam = { PageSize: 40, "Sort": "asc", hasparam: false }; //PageNumber: 1

    //************Login starts

    var getBillingProcedureStructure = function () {
        return billingProcedureStructure;
    }

    var getDoctorInfo = function () {

        doctorinfo = (datapersistanceFactory.getItem('doctorinfo').length === 0) ? doctorinfo : datapersistanceFactory.getItem('doctorinfo');
        return doctorinfo;
    }


    var setDoctorInfo = function (data) {
        doctorinfo = data;

        //alert(JSON.stringify(data));
        datapersistanceFactory.set('doctorinfo', doctorinfo);
        var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
        userLoginDetails = datacookieFactory.getItem('loginData');

        var specialisation = datacookieFactory.set('specialisation');
        var image = datacookieFactory.getItem('userLoginDetails.ProfileImage');
        if (!angular.isUndefined(doctorinfo.doctordata.doctor) && doctorinfo.doctordata.doctor[0] != null) {
            //alert(doctorinfo.doctordata.doctor[0].FullName);

            if (userLoginDetails.displayName == "") {
                userLoginDetails.displayName = doctorinfo.doctordata.doctor[0].FullName;
            }

            //alert("before if");
            //alert(userLoginDetails.specialisation);
            //if(angular.isUndefined(userLoginDetails.specialisation))
            //{ 
            //alert("inside if");
            //alert(doctorinfo.specialitycode[1]);
            if (doctorinfo.specialitycode != undefined)
                userLoginDetails.specialisationCD = doctorinfo.specialitycode[1];

            userLoginDetails.specialisation = doctorinfo.specialization;

            var data = datapersistanceFactory.getItem('lookup');

            var docSpecialitylist = data.doctorSpecialityType;
            var specialisation = datacookieFactory.set('docSpecialitylist');

            angular.forEach(docSpecialitylist, function (item) {
                if (item.DoctorSpecialityID == userLoginDetails.specialisationCD) {
                    //alert(item.DoctorSpecialityDesc);
                    userLoginDetails.specialisation = item.DoctorSpecialityDesc;
                }
            })
            //}
            datacookieFactory.set('loginData', userLoginDetails);



            if (datapersistanceFactory.getItem('doctorProfileImage') == "") {

                datapersistanceFactory.set('doctorProfileImage', doctorinfo.doctordata.doctor[0].ProfileImage);

            }

        }

        function onSuccessSpecialityLoad(data) {

            var specialitylist = data.doctorSpecialityType;

        }

        function onErrorSpecialityError() {



        }
    }

    var getAfterConsultationData = {
        "patientid": 0,
        "appointmentid": 0,
        "patientcaseid": 0
    };
    var addAfterConsultationData = function (data) {
        getAfterConsultationData = data;
        datapersistanceFactory.set('getAfterConsultationData', getAfterConsultationData);
    }
    var getAfterConsultationData = function () {
        getAfterConsultationData = (datapersistanceFactory.getItem('getAfterConsultationData').length === 0) ? getAfterConsultationData : datapersistanceFactory.getItem('getAfterConsultationData');

        return getAfterConsultationData;
    }
    var getUpdatePatientData = "";
    var addUpdatePatientData = function (data) {
        getUpdatePatientData = data;
        datapersistanceFactory.set('getUpdatePatientData', getUpdatePatientData);
    }
    var getUpdatePatientData = function () {
        getUpdatePatientData = (datapersistanceFactory.getItem('getUpdatePatientData').length === 0) ? getUpdatePatientData : datapersistanceFactory.getItem('getUpdatePatientData');

        return getUpdatePatientData;
    }
    var getDoctorId = function () {
        doctorinfo = (datapersistanceFactory.getItem('doctorinfo').length === 0) ? doctorinfo : datapersistanceFactory.getItem('doctorinfo');
        return doctorinfo.doctorid;
    }
    var addLoginId = function (newhcueLoginId) {
        doctorinfo = (datapersistanceFactory.getItem('doctorinfo').length === 0) ? doctorinfo : datapersistanceFactory.getItem('doctorinfo');
        doctorinfo.doctorloginid = newhcueLoginId;
        datapersistanceFactory.set('doctorinfo', doctorinfo);
    }
    var getLoginId = function () {
        doctorinfo = (datapersistanceFactory.getItem('doctorinfo').length === 0) ? doctorinfo : datapersistanceFactory.getItem('doctorinfo');
        //return doctorinfo.doctorloginid;
        return doctorinfo.doctorid;
    }
    var removeLoginId = function () {
        doctorinfo = (datapersistanceFactory.getItem('doctorinfo').length === 0) ? doctorinfo : datapersistanceFactory.getItem('doctorinfo');
        doctorinfo.doctorloginid = '';
        datapersistanceFactory.set('doctorinfo', doctorinfo);
    }
    //************Login ends
    //********* Home page starts

    var getDoctorConsultIdSelected = function () {
        doctorinfo = (datapersistanceFactory.getItem('doctorinfo').length === 0) ? doctorinfo : datapersistanceFactory.getItem('doctorinfo');


        return doctorinfo.doctordata.doctorConsultation;
    }

    //********* Home page ends
    //************Add Patient starts
    var addNewPatientInfo = function (data) {
        patientinfo = data;
        datapersistanceFactory.set('patientinfo', patientinfo);
    };
    var getNewPatientInfo = function () {
        patientinfo = (datapersistanceFactory.getItem('patientinfo').length === 0) ? patientinfo : datapersistanceFactory.getItem('patientinfo');
        return patientinfo;
    };
    var blnAddPatientwithoutConsult = false;
    var addAddPatientwithoutConsult = function (data) {
        blnAddPatientwithoutConsult = data;
        datapersistanceFactory.set('blnAddPatientwithoutConsult', blnAddPatientwithoutConsult);
    }


    var getAddPatientwithoutConsult = function () {
        blnAddPatientwithoutConsult = (datapersistanceFactory.getItem('blnAddPatientwithoutConsult').length === 0) ? blnAddPatientwithoutConsult : datapersistanceFactory.getItem('blnAddPatientwithoutConsult');

        return blnAddPatientwithoutConsult;
    }

    var redirectToPatientaddPage = false;
    var addredirectToPatientaddPage = function (data) {
        redirectToPatientaddPage = data;
    }
    var getredirectToPatientaddPage = function () {
        return redirectToPatientaddPage;
    }

    var Addpatientval = true;
    var Addpatientvalconsult = function (data) {
        Addpatientval = data;
        datapersistanceFactory.set('Addpatientval', Addpatientval);
    }


    var getPatientvalConsult = function () {
        Addpatientval = (datapersistanceFactory.getItem('Addpatientval').length === 0) ? Addpatientval : datapersistanceFactory.getItem('Addpatientval');

        return Addpatientval;
    }

    var blnsendPrescriptionMsg = false;
    var addsendPrescriptionMsg = function (data) {
        blnsendPrescriptionMsg = data;
        datapersistanceFactory.set('blnsendPrescriptionMsg', blnsendPrescriptionMsg);
    }


    var getsendPrescriptionMsg = function () {
        blnsendPrescriptionMsg = (datapersistanceFactory.getItem('blnsendPrescriptionMsg').length === 0) ? blnsendPrescriptionMsg : datapersistanceFactory.getItem('blnsendPrescriptionMsg');

        return blnsendPrescriptionMsg;
    }

    var blnPrintandSubmit = 'S';
    var addPrintandSubmit = function (data) {
        blnPrintandSubmit = data;
        datapersistanceFactory.set('blnPrintandSubmit', blnPrintandSubmit);
    }


    var getPrintandSubmit = function () {
        blnPrintandSubmit = (datapersistanceFactory.getItem('blnPrintandSubmit').length === 0) ? blnPrintandSubmit : datapersistanceFactory.getItem('blnPrintandSubmit');

        return blnPrintandSubmit;
    }

    var addEmergencycontactinfo = function (data) {
        emergencycontactinfo = data;
        datapersistanceFactory.set('emergencycontactinfo', emergencycontactinfo);
    }

    var getEmergencycontactinfo = function () {
        emergencycontactinfo = (datapersistanceFactory.getItem('emergencycontactinfo').length === 0) ? emergencycontactinfo : datapersistanceFactory.getItem('emergencycontactinfo');

        return emergencycontactinfo;
    }

    var addPatientotherdetails = function (data) {
        patientotherdetails = data;
        datapersistanceFactory.set('patientotherdetails', patientotherdetails);
    }

    var getPatientotherdetails = function () {
        patientotherdetails = (datapersistanceFactory.getItem('patientotherdetails').length === 0) ? patientotherdetails : datapersistanceFactory.getItem('patientotherdetails');

        return patientotherdetails;
    }
    var AddButtonval = true;
    var AddStartbuttonCheck = function (data) {
        AddButtonval = data;
        datapersistanceFactory.set('AddButtonval', AddButtonval);
    }
    var getStartbuttonCheck = function () {
        AddButtonval = (datapersistanceFactory.getItem('AddButtonval').length === 0) ? AddButtonval : datapersistanceFactory.getItem('AddButtonval');
        return AddButtonval;
    }

    var getUpdatePatientInfo = function () {
        return patientUpdateinfo;
    }

    var addAppointmentInfo = function (data) {
        appointmentinfo = data;
        datapersistanceFactory.set('appointmentinfo', appointmentinfo);

    }
    var getAppointmentInfo = function () {
        appointmentinfo = (datapersistanceFactory.getItem('appointmentinfo').length === 0) ? appointmentinfo : datapersistanceFactory.getItem('appointmentinfo');

        return appointmentinfo;
    }
    var dispid = ''
    var addDisplayID = function (data) {
        dispid = data;
        datapersistanceFactory.set('dispid', dispid);
    }
    var getDisplayID = function () {
        dispid = (datapersistanceFactory.getItem('dispid').length === 0) ? dispid : datapersistanceFactory.getItem('dispid');

        return dispid;
    }
    var appointmentinfoFrmPatient = [];
    var addAppointmentInfoFrmPatient = function (data) {
        appointmentinfoFrmPatient = data;
        datapersistanceFactory.set('appointmentinfoFrmPatient', appointmentinfoFrmPatient);

    }

    var appointmentClinicName = "";
    var addAppointmentClinicName = function (data) {
        appointmentClinicName = data;
        datapersistanceFactory.set('appointmentClinicName', appointmentClinicName);
    }


    var getAppointmentClinicName = function () {
        appointmentClinicName = (datapersistanceFactory.getItem('appointmentClinicName').length === 0) ? appointmentClinicName : datapersistanceFactory.getItem('appointmentClinicName');

        return appointmentClinicName;
    }
    var getAppointmentInfoFrmPatient = function () {
        appointmentinfoFrmPatient = (datapersistanceFactory.getItem('appointmentinfoFrmPatient').length === 0) ? appointmentinfoFrmPatient : datapersistanceFactory.getItem('appointmentinfoFrmPatient');

        return appointmentinfoFrmPatient;
    }

    var addPatientSearchInfo = function (data) {
        //alert(JSON.stringify(data));
        patientsearchparam = data;
        datapersistanceFactory.set('patientsearchparam', patientsearchparam);
    }
    var getPatientSearchInfo = function () {
        patientsearchparam = (datapersistanceFactory.getItem('patientsearchparam').length === 0) ? patientsearchparam : datapersistanceFactory.getItem('patientsearchparam');
        return patientsearchparam;
    }

    var docAddressId = 0;

    var addDocAddressIdInfo = function (data) {
        docAddressId = data;
        //alert("addDocAddressIdInfo "  + data);
        datapersistanceFactory.set('docAddressId', docAddressId);
    }
    var getDocAddressIdInfo = function () {
        docAddressId = (datapersistanceFactory.getItem('docAddressId').length === 0) ? docAddressId : datapersistanceFactory.getItem('docAddressId');
        //alert("getDocAddressIdInfo " + docAddressId);

        return docAddressId;
    }

    //Bala new code 18-11-2015

    var docMinPerCase = "";
    var addDocMinPerCaseInfo = function (data) {
        docMinPerCase = data;
        datapersistanceFactory.set('docMinPerCase', docMinPerCase);
    }
    var getDocMinPerCaseInfo = function () {
        docMinPerCase = (datapersistanceFactory.getItem('docMinPerCase').length === 0) ? docMinPerCase : datapersistanceFactory.getItem('docMinPerCase');

        return docMinPerCase;
    }

    var docAvailConsultTime = [];
    var addDocAvailConsultTimeInfo = function (data) {
        docAvailConsultTime = data;
        datapersistanceFactory.set('docAvailConsultTime', docAvailConsultTime);
    }
    var getDocAvailConsultTimeInfo = function () {
        docAvailConsultTime = (datapersistanceFactory.getItem('docAvailConsultTime').length === 0) ? docAvailConsultTime : datapersistanceFactory.getItem('docAvailConsultTime');

        return docAvailConsultTime;
    }

    //"ParentHospitalID": 0, "HospitalID": 0,
    var OfflineCaseDetails = {
        "VersionID": 1,
        "DoctorID": 0,
        "USRId": 0,
        "USRType": "DOCTOR",
        "AuditLog": {
            "RequestDate": "",
            "ConsultantDate": "",
            "RequestBy": "",
            "ParentHospitalID": 0,
            "HospitalID": 0,
            "AddAppointment": {},
            "UpdateAppointment": {},
            "AddPatient": {},
            "UpdatePatient": {},
            "CaseSummary": {}
        }
    };
    var setOfflineCaseDetails = function (data) {
        OfflineCaseDetails = data;
        datapersistanceFactory.set('OfflineCaseDetails', OfflineCaseDetails);
    }
    var getOfflineCaseDetails = function () {
        OfflineCaseDetails = (datapersistanceFactory.getItem('OfflineCaseDetails').length === 0) ? OfflineCaseDetails : datapersistanceFactory.getItem('OfflineCaseDetails');

        return OfflineCaseDetails;
    }

    var docBookedConsultTime = [];
    var addDocBookedConsultTimeInfo = function (data) {
        docBookedConsultTime = data;
        datapersistanceFactory.set('docBookedConsultTime', docBookedConsultTime);
    }
    var getDocBookedConsultTimeInfo = function () {
        docBookedConsultTime = (datapersistanceFactory.getItem('docBookedConsultTime').length === 0) ? docBookedConsultTime : datapersistanceFactory.getItem('docBookedConsultTime');

        return docBookedConsultTime;
    }

    var docEndConsultTime = [];
    var addEndConsultTime = function (data) {
        docEndConsultTime = data;
        datapersistanceFactory.set('docEndConsultTime', docEndConsultTime);
    }
    var getEndConsultTime = function () {
        docEndConsultTime = (datapersistanceFactory.getItem('docEndConsultTime').length === 0) ? docEndConsultTime : datapersistanceFactory.getItem('docEndConsultTime');

        return docEndConsultTime;
    }

    var docStartConsultTime = [];
    var addStartConsultTime = function (data) {
        docStartConsultTime = data;
        datapersistanceFactory.set('docStartConsultTime', docStartConsultTime);
    }
    var getStartConsultTime = function () {
        docStartConsultTime = (datapersistanceFactory.getItem('docStartConsultTime').length === 0) ? docStartConsultTime : datapersistanceFactory.getItem('docStartConsultTime');
        return docStartConsultTime;
    }

    var docDocPatientID = "";
    var addDocPatientID = function (data) {

        docDocPatientID = data;
        datapersistanceFactory.set('docDocPatientID', docDocPatientID);
    }
    var getDocPatientID = function () {
        docDocPatientID = (datapersistanceFactory.getItem('docDocPatientID').length === 0) ? docDocPatientID : datapersistanceFactory.getItem('docDocPatientID');

        return docDocPatientID;
    }

    //19-11-2015

    var docAddressConsultID = [];
    var addDocAddressConsultID = function (data) {
        docAddressConsultID = data;
        datapersistanceFactory.set('docAddressConsultID', docAddressConsultID);
    }
    var getDocAddressConsultID = function () {
        docAddressConsultID = (datapersistanceFactory.getItem('docAddressConsultID').length === 0) ? docAddressConsultID : datapersistanceFactory.getItem('docAddressConsultID');

        return docAddressConsultID;
    }


    var calenderInfo = {};
    var addcalenderInfo = function (data) {

        //calenderInfo = {};
        calenderInfo = data;

        datapersistanceFactory.set('calenderInfo', calenderInfo);
    }
    var getcalenderInfo = function () {
        calenderInfo = (JSON.stringify(datapersistanceFactory.getItem('calenderInfo')) === {}) ? calenderInfo : datapersistanceFactory.getItem('calenderInfo');

        return calenderInfo;
    }

    //20-11-2015
    var docSelectedAppointDt = "";
    var addSelectedAppointDt = function (data) {
        docSelectedAppointDt = data;
        datapersistanceFactory.set('docSelectedAppointDt', docSelectedAppointDt);
    }
    var getSelectedAppointDt = function () {
        docSelectedAppointDt = (datapersistanceFactory.getItem('docSelectedAppointDt').length === 0) ? docSelectedAppointDt : datapersistanceFactory.getItem('docSelectedAppointDt');

        return docSelectedAppointDt;
    }
    //20-11-2015
    var docSelectedAppointStartTime = "";
    var addSelectedAppointStartTime = function (data) {
        docSelectedAppointStartTime = data;
        datapersistanceFactory.set('docSelectedAppointStartTime', docSelectedAppointStartTime);
    }
    var getSelectedAppointStartTime = function () {
        docSelectedAppointStartTime = (datapersistanceFactory.getItem('docSelectedAppointStartTime').length === 0) ? docSelectedAppointStartTime : datapersistanceFactory.getItem('docSelectedAppointStartTime');

        return docSelectedAppointStartTime;
    }



    //20-11-2015
    var docSelectedAppointEndTime = "";
    var addSelectedAppointEndTime = function (data) {
        docSelectedAppointEndTime = data;
        datapersistanceFactory.set('docSelectedAppointEndTime', docSelectedAppointEndTime);
    }
    var getSelectedAppointEndTime = function () {
        docSelectedAppointEndTime = (datapersistanceFactory.getItem('docSelectedAppointEndTime').length === 0) ? docSelectedAppointEndTime : datapersistanceFactory.getItem('docSelectedAppointEndTime');

        return docSelectedAppointEndTime;
    }

    var docBookedAppointmentlist = [];
    var adddocBookedAppointmentlist = function (data) {
        docBookedAppointmentlist = data;
        datapersistanceFactory.set('docBookedAppointmentlist', docBookedAppointmentlist);
    }
    var getdocBookedAppointmentlist = function () {
        docBookedAppointmentlist = (datapersistanceFactory.getItem('docBookedAppointmentlist').length === 0) ? docBookedAppointmentlist : datapersistanceFactory.getItem('docBookedAppointmentlist');

        return docBookedAppointmentlist;
    }

    var docSelectedFollowupDay = 0;
    var adddocSelectedFollowupDay = function (data) {
        docSelectedFollowupDay = data;
        datapersistanceFactory.set('docSelectedFollowupDay', docSelectedFollowupDay);
    }
    var getdocSelectedFollowupDay = function () {
        docSelectedFollowupDay = (datapersistanceFactory.getItem('docSelectedFollowupDay').length === 0) ? docSelectedFollowupDay : datapersistanceFactory.getItem('docSelectedFollowupDay');

        return docSelectedFollowupDay;
    }

    var blnVisibleAppointmentFollowupDay = false;
    var addVisibleAppointmentFollowupDay = function (data) {
        blnVisibleAppointmentFollowupDay = data;
        datapersistanceFactory.set('blnVisibleAppointmentFollowupDay', blnVisibleAppointmentFollowupDay);
    }
    var getVisibleAppointmentFollowupDay = function () {
        blnVisibleAppointmentFollowupDay = (datapersistanceFactory.getItem('blnVisibleAppointmentFollowupDay').length === 0) ? blnVisibleAppointmentFollowupDay : datapersistanceFactory.getItem('blnVisibleAppointmentFollowupDay');

        return blnVisibleAppointmentFollowupDay;
    }

    var refdoclist = [];
    var addreferdoclist = function (data) {
        refdoclist = data;
        datapersistanceFactory.set('refdoclist', refdoclist);
    }
    var getreferdoclist = function () {
        refdoclist = (datapersistanceFactory.getItem('refdoclist').length === 0) ? refdoclist : datapersistanceFactory.getItem('refdoclist');
        return refdoclist;
    }


    var printblankline = [];
    var addprintblankline = function (data) {
        printblankline = data;

    }
    var getprintblankline = function () {
        return printblankline;
    }
    //added by arun for printheadersettings
    var printheadersetting = [];
    var addprintsettingsheader = function (data) {
        printheadersetting = data;
        datapersistanceFactory.set('printheadersetting', printheadersetting);

    }
    var getprintsettingsheader = function () {
        return printheadersetting;
    }

    var settingPreference = [];
    var addsettingpreference = function (data) {
        settingPreference = data;
        datapersistanceFactory.set('settingPreference', settingPreference);
    };
    var getsettingpreference = function () {
        settingPreference = (datapersistanceFactory.getItem('settingPreference').length === 0) ? settingPreference : datapersistanceFactory.getItem('settingPreference');
        return settingPreference;
    };

    var doctornotesval = [];
    var adddoctornotes = function (data) {
        doctornotesval = data;
    }
    var getdoctornotes = function () {
        return doctornotesval;
    }

    var addbill = [];
    var continuebill = [];
    var continuefollowupbilldecided = function (data) {
        continuebill = data;

        datacookieFactory.set('continuebill', continuebill);
    }
    var continuefollowupgetbilldecided = function () {
        continuebill = (datacookieFactory.getItem('continuebill').length === 0) ? continuebill : datacookieFactory.getItem('continuebill');
        return continuebill;
    }
    var addbilldecided = function (data) {
        addbill = data
        console.log(addbill);
        datacookieFactory.set('addbill', addbill);
    }
    var getbilldecided = function () {
        addbill = (datacookieFactory.getItem('addbill').length === 0) ? addbill : datacookieFactory.getItem('addbill');
        return addbill;
    }
    var appbill = false;
    var fromappbill = function (data) {
        appbill = data
        console.log(appbill);
        datacookieFactory.set('appbill', appbill);
    }
    var getappbill = function () {
        appbill = (datacookieFactory.getItem('appbill').length === 0) ? appbill : datacookieFactory.getItem('appbill');
        return appbill;
    }
    var followpast = false;
    var addfollowpast = function (data) {
        followpast = data
        console.log(followpast);
        datacookieFactory.set('followpast', followpast);
    }
    var getfollowpast = function () {
        followpast = (datacookieFactory.getItem('followpast').length === 0) ? followpast : datacookieFactory.getItem('followpast');
        return followpast;
    }

    var StartAppointmentInfo = [];
    var addStartAppointmentInfo = function (data) {
        StartAppointmentInfo = data;
        datacookieFactory.set('StartAppointmentInfo', StartAppointmentInfo);
    }
    var getStartAppointmentInfo = function () {
        StartAppointmentInfo = (datacookieFactory.getItem('StartAppointmentInfo').length === 0) ? StartAppointmentInfo : datacookieFactory.getItem('StartAppointmentInfo');

        return StartAppointmentInfo;
    }
    var billingInfo;
    var addBillingInfo = function (data) {
        billingInfo = data;

        datapersistanceFactory.set('PreviousBillingData', billingInfo);

    }
    var getBillingInfo = function () {
        var billingInfosaved = (datapersistanceFactory.getItem('PreviousBillingData').length === 0) ? billingInfo : datapersistanceFactory.getItem('PreviousBillingData');
        return billingInfosaved;

    }

    var patientData = [];
    var addpatientData = function (data) {
        patientData = data;
        datapersistanceFactory.set('patientData', patientData);

    }
    var getpatientData = function () {
        patientData = (datapersistanceFactory.getItem('patientData').length === 0) ? patientData : datapersistanceFactory.getItem('patientData');
        return patientData;
    }

    /* Consultation entry get & set */
    var entryConsultationPatientInfo;
    var setentryConsultationPatientInfo = function (data) {
        ////console.log("service " + JSON.stringify(data));
        entryConsultationPatientInfo = data;
    }
    var getentryConsultationPatientInfo = function () {
        ////console.log("get service " + JSON.stringify(entryConsultationPatientInfo));
        return entryConsultationPatientInfo;
    }

    var patTotalConsultTime = "";
    var addTotalConsultTime = function (data) {
        patTotalConsultTime = data;
        datapersistanceFactory.set('patTotalConsultTime', patTotalConsultTime);
    }
    var getTotalConsultTime = function () {
        patTotalConsultTime = (datapersistanceFactory.getItem('patTotalConsultTime').length === 0) ? patTotalConsultTime : datapersistanceFactory.getItem('patTotalConsultTime');

        return patTotalConsultTime;
    }


    var setstartappointmentclick = "";
    var setstartappointmentclick = function (data) {
        setstartappointmentclick = data;
        datapersistanceFactory.set('setstartappointmentclick', setstartappointmentclick);
    }
    var getstartappointmentclick = function () {
        setstartappointmentclick = (datapersistanceFactory.getItem('setstartappointmentclick').length === 0) ? setstartappointmentclick : datapersistanceFactory.getItem('setstartappointmentclick');

        return setstartappointmentclick;
    }
    var setcontinuefollowupclick = "";
    var setcontinuefollowupclick = function (data) {
        setcontinuefollowupclick = data;
        datapersistanceFactory.set('setcontinuefollowupclick', setcontinuefollowupclick);
    }
    var getcontinuefollowupclick = function () {
        setcontinuefollowupclick = (datapersistanceFactory.getItem('setcontinuefollowupclick').length === 0) ? setcontinuefollowupclick : datapersistanceFactory.getItem('setcontinuefollowupclick');

        return setcontinuefollowupclick;
    }
    //setcontinuefollowupclick
    var Pastrecordvalue = [];
    var setPastRecordsvalues = function (data) {
        datapersistanceFactory.set('Pastrecordvalue', Pastrecordvalue);
        Pastrecordvalue = data;

    }
    var getPastRecordsvalues = function () {
        Pastrecordvalue = (datapersistanceFactory.getItem('Pastrecordvalue').length === 0) ? Pastrecordvalue :
            datapersistanceFactory.getItem('Pastrecordvalue');
        return Pastrecordvalue;
    }

    var membershipDiscountData = []
    var addMembershipDiscountData = function (data) {
        console.log(JSON.stringify(data))

        // var intmtColl = {};
        if (data.response && data.response.rows) {
            angular.forEach(data.response.rows, function (values) {
                console.log(JSON.stringify(values))
                var objmember = { "MembershipId": 0, "Discount": 0, 'ActiveIND': "Y", "MembershipName": '' };
                objmember.MembershipId = values.MembershipID;
                objmember.MembershipName = values.MembershipName;
                objmember.Discount = values.Discount;
                objmember.ActiveIND = values.ActiveIND;

                this.push(objmember);
            }, membershipDiscountData);
        }
        console.log(JSON.stringify(membershipDiscountData));
        datapersistanceFactory.set('MembershipDiscount', membershipDiscountData);

    }
    var getMembershipDicountData = function () {
        membershipDiscountData = (datapersistanceFactory.getItem('MembershipDiscount').length === 0) ? membershipDiscountData :
            datapersistanceFactory.getItem('MembershipDiscount');
        return membershipDiscountData;
    }

    var offlineAppointmentDtls = {};
    var addOfflineAppointmentDtls = function (data) {
        offlineAppointmentDtls = data;
        datapersistanceFactory.set('offlineAppointmentDtls', offlineAppointmentDtls);
    }
    var getOfflineAppointmentDtls = function () {
        offlineAppointmentDtls = (datapersistanceFactory.getItem('offlineAppointmentDtls').length === 0) ? offlineAppointmentDtls : datapersistanceFactory.getItem('offlineAppointmentDtls');

        return offlineAppointmentDtls;
    }
    var valfrNurseflow = false;
    var setvalueforNurseflow = function (data) {
        valfrNurseflow = data;
        datapersistanceFactory.set('valfrNurseflow', valfrNurseflow);
    }
    var getvalueforNurseflow = function () {
        valfrNurseflow = followpast = (datacookieFactory.getItem('valfrNurseflow').length === 0) ? valfrNurseflow : datacookieFactory.getItem('valfrNurseflow');
        return valfrNurseflow;
    }
    var patNurseflow = "";
    var SetPatientforNurseService = function (data) {
        patNurseflow = data;
        datapersistanceFactory.set('patNurseflow', patNurseflow);
    }
    var getPatientforNurseService = function (data) {
        patNurseflow = (datapersistanceFactory.getItem('patNurseflow').length === 0) ? patNurseflow : datapersistanceFactory.getItem('patNurseflow');
        return patNurseflow;
    }

    var datanurseflow = "";
    var SetdataforNurseService = function (data) {
        datanurseflow = data;
        datapersistanceFactory.set('datanurseflow', datanurseflow);
    }
    var getdataforNurseService = function (data) {
        datanurseflow = (datapersistanceFactory.getItem('datanurseflow').length === 0) ? datanurseflow : datapersistanceFactory.getItem('datanurseflow');
        return datanurseflow;
    }

    var PreMedicalHistory = [];
    var addPreMedicalHistory = function (data) {
        PreMedicalHistory = data;
        datacookieFactory.set('PreMedicalHistory', PreMedicalHistory);
    }
    var getPreMedicalHistory = function () {
        PreMedicalHistory = (datacookieFactory.getItem('PreMedicalHistory').length === 0) ? PreMedicalHistory : datacookieFactory.getItem('PreMedicalHistory');

        return PreMedicalHistory;
    }

    var appointmentType = "";
    var addappointmentType = function (type) {
        appointmentType = type;

    }
    var getappointmentType = function () {
        return appointmentType;
    }
    var setClinicpopup = "";
    var setClinicpopup = function (data) {
        setClinicpopup = data;
        datapersistanceFactory.set('setClinicpopup', setClinicpopup);
    }
    var getClinicpopup = function () {
        setClinicpopup = (datapersistanceFactory.getItem('setClinicpopup').length === 0) ? setClinicpopup : datapersistanceFactory.getItem('setClinicpopup');

        return setClinicpopup;
    }
    var consultationType = "";
    var addconsultType = function (data) {
        consultationType = data;
        datapersistanceFactory.set('consultationType', consultationType);
    };
    var getconsultType = function () {
        consultationType = (datapersistanceFactory.getItem('consultationType').length === 0) ? consultationType : datapersistanceFactory.getItem('consultationType');
        return consultationType;
    };
    // added by arun for pastappointment selected date
    var pastappdate = "";
    var addSelectedPastAppDate = function (data) {
        pastappdate = data;
        datapersistanceFactory.set('pastappdate', pastappdate);
    }
    var getSelectedPastAppDate = function () {
        pastappdate = (datapersistanceFactory.getItem('pastappdate').length === 0) ? docSelectedAppointDt : datapersistanceFactory.getItem('pastappdate');
        return pastappdate;
    }

    var patientpasthistory = "";
    var addpastpatienthistory = function (data) {
        patientpasthistory = data;
        datapersistanceFactory.set('patientpasthistory', patientpasthistory);
    }
    var getpastpatienthistory = function () {
        patientpasthistory = (datapersistanceFactory.getItem('patientpasthistory').length === 0) ? patientpasthistory : datapersistanceFactory.getItem('patientpasthistory');
        return patientpasthistory;
    }
    var patientIvfhistory = "";
    var addivfpatienthistory = function (data) {
        patientIvfhistory = data;
        datapersistanceFactory.set('patientIvfhistory', patientIvfhistory);
    }
    var getivfpatienthistory = function () {
        patientIvfhistory = (datapersistanceFactory.getItem('patientIvfhistory').length === 0) ? patientIvfhistory : datapersistanceFactory.getItem('patientIvfhistory');
        return patientIvfhistory;
    }
    var walkinviewcase = true;
    var Addwalkinviewcase = function (data) {
        walkinviewcase = data;
        datapersistanceFactory.set('walkinviewcase', walkinviewcase);
    }
    var getwalkinviewcase = function () {
        walkinviewcase = (datapersistanceFactory.getItem('walkinviewcase').length === 0) ? walkinviewcase : datapersistanceFactory.getItem('walkinviewcase');
        return walkinviewcase;
    }

    var updatepatiententry = "";
    var addupdatepatiententry = function (data) {
        updatepatiententry = data;
        datapersistanceFactory.set('updatepatiententry', updatepatiententry);
    };
    var getupdatepatiententry = function () {
        updatepatiententry = (datapersistanceFactory.getItem('updatepatiententry').length === 0) ? updatepatiententry : datapersistanceFactory.getItem('updatepatiententry');
        return updatepatiententry;
    };


    var referralteamtab = "";
    var addreferralteamtab = function (data) {
        referralteamtab = data;
        datapersistanceFactory.set('referralteamtab', referralteamtab);
    };
    var getreferralteamtab = function () {
        referralteamtab = (datapersistanceFactory.getItem('referralteamtab').length === 0) ? referralteamtab : datapersistanceFactory.getItem('referralteamtab');
        return referralteamtab;
    };

    //************Add Patient ends
    return {
        //************Login starts
        getDoctorInfo: getDoctorInfo,
        setDoctorInfo: setDoctorInfo,

        getDoctorId: getDoctorId,

        addLoginId: addLoginId,
        getLoginId: getLoginId,
        removeLoginId: removeLoginId,
        //************Login ends
        addSelectedPastAppDate: addSelectedPastAppDate,
        getSelectedPastAppDate: getSelectedPastAppDate,
        //********* Home page starts
        getDoctorConsultIdSelected: getDoctorConsultIdSelected,
        //********* Home page ends
        //************Add Patient starts
        addNewPatientInfo: addNewPatientInfo,
        getNewPatientInfo: getNewPatientInfo,
        getUpdatePatientInfo: getUpdatePatientInfo,

        addEmergencycontactinfo: addEmergencycontactinfo,
        getEmergencycontactinfo: getEmergencycontactinfo,

        addPatientotherdetails: addPatientotherdetails,
        getPatientotherdetails: getPatientotherdetails,

        addAppointmentInfo: addAppointmentInfo,
        getAppointmentInfo: getAppointmentInfo,

        addAppointmentClinicName: addAppointmentClinicName,
        getAppointmentClinicName: getAppointmentClinicName,

        //************Add Patient ends
        addPatientSearchInfo: addPatientSearchInfo,
        getPatientSearchInfo: getPatientSearchInfo,
        //************Add Doctor address id
        addDocAddressIdInfo: addDocAddressIdInfo,
        getDocAddressIdInfo: getDocAddressIdInfo,
        //************Add Doctor Min time PerCase
        addDocMinPerCaseInfo: addDocMinPerCaseInfo,
        getDocMinPerCaseInfo: getDocMinPerCaseInfo,
        //************Add Doctor Available Consultation Time
        addDocAvailConsultTimeInfo: addDocAvailConsultTimeInfo,
        getDocAvailConsultTimeInfo: getDocAvailConsultTimeInfo,
        //************Add Doctor Patient ID
        addDocPatientID: addDocPatientID,
        getDocPatientID: getDocPatientID,
        //************Add Doctor Address ConsultID
        addDocAddressConsultID: addDocAddressConsultID,
        getDocAddressConsultID: getDocAddressConsultID,
        //************Add Doctor Selected Appoint Date
        addSelectedAppointDt: addSelectedAppointDt,
        getSelectedAppointDt: getSelectedAppointDt,
        //************Add Doctor Address ConsultID
        addSelectedAppointStartTime: addSelectedAppointStartTime,
        getSelectedAppointStartTime: getSelectedAppointStartTime,

        addredirectToPatientaddPage: addredirectToPatientaddPage,
        getredirectToPatientaddPage: getredirectToPatientaddPage,
        //************Add Doctor Selected Appoint Date
        addSelectedAppointEndTime: addSelectedAppointEndTime,
        getSelectedAppointEndTime: getSelectedAppointEndTime,
        //************List Booked Appoi list
        adddocBookedAppointmentlist: adddocBookedAppointmentlist,
        getdocBookedAppointmentlist: getdocBookedAppointmentlist,

        adddocSelectedFollowupDay: adddocSelectedFollowupDay,
        getdocSelectedFollowupDay: getdocSelectedFollowupDay,

        addBillingInfo: addBillingInfo,
        getBillingInfo: getBillingInfo,
        addbilldecided: addbilldecided,
        getbilldecided: getbilldecided,
        continuefollowupbilldecided: continuefollowupbilldecided,
        continuefollowupgetbilldecided: continuefollowupgetbilldecided,
        fromappbill: fromappbill,
        getappbill: getappbill,
        addfollowpast: addfollowpast,
        getfollowpast: getfollowpast,
        ///
        addStartAppointmentInfo: addStartAppointmentInfo,
        getStartAppointmentInfo: getStartAppointmentInfo,
        //
        setentryConsultationPatientInfo: setentryConsultationPatientInfo,
        getentryConsultationPatientInfo: getentryConsultationPatientInfo,
        addprintblankline: addprintblankline,
        getprintblankline: getprintblankline,
        adddoctornotes: adddoctornotes,
        getdoctornotes: getdoctornotes,
        addAddPatientwithoutConsult: addAddPatientwithoutConsult,
        getAddPatientwithoutConsult: getAddPatientwithoutConsult,
        addprintsettingsheader: addprintsettingsheader,
        getprintsettingsheader: getprintsettingsheader,

        addVisibleAppointmentFollowupDay: addVisibleAppointmentFollowupDay,
        getVisibleAppointmentFollowupDay: getVisibleAppointmentFollowupDay,
        addreferdoclist: addreferdoclist,
        getreferdoclist: getreferdoclist,
        addpatientData: addpatientData,
        getpatientData: getpatientData,

        getcalenderInfo: getcalenderInfo,
        addcalenderInfo: addcalenderInfo,
        addsendPrescriptionMsg: addsendPrescriptionMsg,
        getsendPrescriptionMsg: getsendPrescriptionMsg,
        addPrintandSubmit: addPrintandSubmit,
        getPrintandSubmit: getPrintandSubmit,
        Addpatientvalconsult: Addpatientvalconsult,
        getPatientvalConsult: getPatientvalConsult,

        addDocBookedConsultTimeInfo: addDocBookedConsultTimeInfo,
        getDocBookedConsultTimeInfo: getDocBookedConsultTimeInfo,

        addEndConsultTime: addEndConsultTime,
        getEndConsultTime: getEndConsultTime,

        addStartConsultTime: addStartConsultTime,
        getStartConsultTime: getStartConsultTime,

        addAppointmentInfoFrmPatient: addAppointmentInfoFrmPatient,
        getAppointmentInfoFrmPatient: getAppointmentInfoFrmPatient,

        addTotalConsultTime: addTotalConsultTime,
        getTotalConsultTime: getTotalConsultTime,
        AddStartbuttonCheck: AddStartbuttonCheck,
        getStartbuttonCheck: getStartbuttonCheck,

        setPastRecordsvalues: setPastRecordsvalues,
        getPastRecordsvalues: getPastRecordsvalues,

        setOfflineCaseDetails: setOfflineCaseDetails,
        getOfflineCaseDetails: getOfflineCaseDetails,

        addDisplayID: addDisplayID,
        getDisplayID: getDisplayID,

        addappointmentType: addappointmentType,
        getappointmentType: getappointmentType,

        addMembershipDiscountData: addMembershipDiscountData,
        getMembershipDicountData: getMembershipDicountData,
        setstartappointmentclick: setstartappointmentclick,
        getstartappointmentclick: getstartappointmentclick,
        setcontinuefollowupclick: setcontinuefollowupclick,
        getcontinuefollowupclick: getcontinuefollowupclick,
        addOfflineAppointmentDtls: addOfflineAppointmentDtls,
        getOfflineAppointmentDtls: getOfflineAppointmentDtls,
        setvalueforNurseflow: setvalueforNurseflow,
        getvalueforNurseflow: getvalueforNurseflow,
        SetPatientforNurseService: SetPatientforNurseService,
        getPatientforNurseService: getPatientforNurseService,
        addPreMedicalHistory: addPreMedicalHistory,
        getPreMedicalHistory: getPreMedicalHistory,
        getBillingProcedureStructure: getBillingProcedureStructure,


        setClinicpopup: setClinicpopup,
        getClinicpopup: getClinicpopup,
        addconsultType: addconsultType,
        getconsultType: getconsultType,
        addsettingpreference: addsettingpreference,
        getsettingpreference: getsettingpreference,
        getdataforNurseService: getdataforNurseService,
        SetdataforNurseService: SetdataforNurseService,

        addpastpatienthistory: addpastpatienthistory,
        getpastpatienthistory: getpastpatienthistory,
        addivfpatienthistory: addivfpatienthistory,
        getivfpatienthistory: getivfpatienthistory,
        getAfterConsultationData: getAfterConsultationData,
        addAfterConsultationData: addAfterConsultationData,
        Addwalkinviewcase: Addwalkinviewcase,
        getwalkinviewcase: getwalkinviewcase,
        addUpdatePatientData: addUpdatePatientData,
        getUpdatePatientData: getUpdatePatientData,

        addupdatepatiententry: addupdatepatiententry,
        getupdatepatiententry: getupdatepatiententry,
        addreferralteamtab: addreferralteamtab,
        getreferralteamtab: getreferralteamtab
    };
}])
hCueDoctorWebServices.factory('hcueConsultationInfo', ['datapersistanceFactory', function (datapersistanceFactory) {
    //*************item json structure used in the respective add item starts here
    var medicineitem = {
        RowID: 0,
        PharmaWrkFlowStatusID: 'FRDP',
        Medicine: '',
        BeforeAfter: false,
        NumberofDays: '',
        DayMode: 'D',
        DosageDayInterval: '',
        Dosage1: '',
        Dosage2: '',
        Dosage3: '',
        Dosage4: '',
        Manufacturer: '',
        Diagnostic: '',
        Quantity: 0,
        MRP: 0.00,
        MedicineGenericName: '',
        MedicineContainer: '',
        MedicineType: 'OTH',
        NewMedicine: false

    };
    var labtestitem = { RowID: 1, LabProcedureID: 0, LabWrkFlowStatusID: 'INIT', LabNotes: '', AdditionalNotes: '', LabGroupTag: '', LabTestName: '', LabTestTypeID: 'TEST', LabTestCode: '' }; // { sendto: 0, tests: [{ RowID: 1, LabNotes: '', LabGroupTag: '', LabTestName: '', LabTestTypeID: 'TEST', LabTestCode: '' }] };
    var scantestitem = { RowID: 1, LabWrkFlowStatusID: 'FRDP', LabNotes: '', AdditionalNotes: '', LabGroupTag: '', LabTestName: '', LabTestTypeID: 'SCAN', LabTestCode: '' };
    var referralitem = [{ DoctorID: 0, Notes: '' }];


    //var medicinelistitems = {Medicine:'',MedicineType:'',_id:''};
    var medicinelistitems = [];

    //*************item json structure used in the respective add item ends here
    var consultation_info = {
        prescription: { pharmaid: 0, medicines: [] },
        labid: 0,
        labtests: [],
        scanid: 0,
        scantests: [],
        referrals: [],
        documents: {},
        vitalnoteinfo: { visitreason: '', vitals: {}, notes: { symptom: '', general: '', followup: '', doctornotes: '' } },
        caseid: 0,
        appointmentid: 0,
        patientid: 0,
        doctorid: 0,
        fees: 0.00,
        followup: 0,
        followupInterval: 0,
        BillingInfo: [],
        ImageNotesData: [],
        DentalConditionsArray: { DentalHistoryArray: [], MedicalHistoryArray: [] },
        treatmentCollection: {
            VisitRsn: [],
            Diagnostic: [],
            Investigation: [],
        },
        voiceDocuments: { voiceDocumentArray: [], CasePrescription: [{ "RowID": 1, "PatientCaseConditions": [], "USRId": 0 }] }

    };

    /*var consultation = {
        prescription: { pharmaid: 0, medicines: [] }, labid: 0, labtests: [], scanid: 0, scantests: [], referrals: [], documents: {}, vitalnoteinfo: { visitreason: '', vitals: {}, notes: { symptom: '', general: '', followup: '', doctornotes: '' } }, 
        caseid: 0, appointmentid: 0, patientid: 0, doctorid: 0, fees: 0.00, followup: 0, followupInterval: 0,BillingInfo: [],  ImageNotesData: [], DentalConditionsArray: { DentalHistoryArray: [], MedicalHistoryArray: [] } ,voiceDocuments: { voiceDocumentArray: [] } , treatmentCollection: {
            VisitRsn: [],
            Diagnostic: [],
            Investigation: [],
            CaseTreatment: [],
        }, CasePrescription:[{"RowID": 1,"PatientCaseConditions":[],"USRId": 0}]

    };*/

    var consultation = {
        JSONID: '',
        OfflineAppointmentID: 0,
        OfflineDBID: 0,
        CaseTreatment: [],
        "prescription": { "pharmaid": 0, "medicines": [] },
        "labid": 0,
        "labtests": [],
        "scanid": 0,
        "scantests": [],
        "referrals": [],
        "documents": {},
        "vitalnoteinfo": {
            "visitreason": '',
            "vitals": {},
            "notes": {
                "symptom": '',
                "general": '',
                "followup": '',
                "doctornotes": ''
            }
        },
        "caseid": 0,
        "PatientCaseId": 0,
        "appointmentid": 0,
        "patientid": 0,
        "doctorid": 0,
        "fees": 0.00,
        "followup": 0,
        "followupInterval": 0,
        "BillingInfo": [],
        "IVFDetail": [],
        "ImageNotesData": [],
        "voiceDocuments": {
            "voiceDocumentArray": []
        },
        "LabTestObject": {
            "LabOrderArray": []
        },
        "TeethConditionsObj": {
            "teethconditionarray": [],
            "Gumsconditionarray": [],
            "mucosaarray": [],
            "tmjarray": []
        },
        "TreatmentConditionsObject": {
            "conditionforpatient": [],
            "onlyTreatments": [],
            "TreatmentTMJArray": [],
            "TreatmentMocusaArray": [],
            "FinalEstimationCost": 0,
            "FinalBillingCost": 0,
            "BalenceBillingCost": 0,
            "finalamount": 0,
            "TreatAddNotes": "",
            "newbilling": [],
            "conditionsArray": [],
            "BillingObject": [],
            "TotalBillingCost": 0,
            "TotalBillingBalance": 0,
            "TotalBillingDiscount": 0,
            "TotalBillingRecieved": 0
        },
        "DentalConditionsArray": {
            "DentalHistoryArray": [],
            "MedicalHistoryArray": []
        },
        "treatmentCollection": {
            "VisitRsn": [],
            "Diagnostic": [],
            "Investigation": [],
            "CaseTreatment": [],
            "PastMedicalHistory": []
        },
        "InvoiceNumbers": [],
        "patienthistcaseid": 0,
        "patientpagenumber": 0,
        "PastConsultationData": {},
        "AdultTootharray": [],
        "ChildTootharray": [],
        "AdultTootharrayLab": [],
        "ChildTootharrayLab": [],
        "showPrimary": "Y",
        "showAdultTeeth": true,
        "showChildTeeth": false,
        "showConditionTextbox": false,
        "InvoiceData": [],
        "progressArray": [],
        "EstimateData": [],
        "EstiImageDocumentArray": [],
        "EstiPdfdocArray": [],
        "PdfNotesData": [],
        "EstimationData": [],
        "TmjcustomArray": [],
        "Mucosacustomarray": [],
        "patientSignedConsentList": [],
        "PatientConsertArray": [],
        "PatientPdfConsertArray": [],
        "AuthorizeUser": 0,
        "CheckEstimate": 0,
        "CheckDentalTab": 0,
        "CheckNotesTab": 0,
        "CheckTreatmentTab": 0,
        "CheckLabTab": 0,
        "CheckBillingTab": 0,
        "CheckConsentTab": 0,
        "CheckConsultationScreen": 0,
        "CasePrescription": [{ "RowID": 1, "PatientCaseConditions": [], "USRId": 0 }],
        "PatientCaseMediaFile": { 'Condition': [], 'Treatment': [] },
        "MediaFileIDs": []

    };

    var dentalproblems = {
        "ProblemName": "",
        "ProblemId": 0,
        "ConditionNotes": "",
        "Treatments": [{
            "treatmentID": "",
            "TreatmentDesc": "",
            "NotesTreatments": "",
            "status": "",
            "statusid": "",
            "notesstatus": false,
            "treatmentcost": "",
            "ProcedureID": "",
            "TreatmentSeqNumber": 0,
            "TaxDetails": [],
            "TreatmentStatusChange": true

        }]
    }

    var tmjsCondition = {
        "SubConditionID": 0,
        "SubConditionNotes": "",
        "SubConditionDesc": "",
        "drctn": "",
        "SubConditionTreatment": [{
            "TaxDetails": [],
            "TreatmentID": "",
            "TreatmentIDDesc": "",
            "Treatmentcost": "",
            "TreatmentNotes": "",
            "notesstatus": false,
            "TreatMentStatusCD": "",
            "TreatMentStatusDesc": "",
            "TreatmentSeqNumber": 0,
            "TreatmentStatusChange": true
        }]

    }

    var mucosaCondition = {
        "SubConditionID": 0,
        "SubConditionNotes": "",
        "SubConditionDesc": "",
        "desc": "",
        "cndtn": "",
        "SubConditionTreatment": [{
            "TaxDetails": [],
            "TreatmentID": "",
            "TreatmentIDDesc": "",
            "Treatmentcost": "",
            "TreatmentNotes": "",
            "notesstatus": false,
            "TreatMentStatusCD": "",
            "TreatMentStatusDesc": "",
            "TreatmentSeqNumber": 0,
            "TreatmentStatusChange": true
        }]

    }

    var getMedicineListItems = function () {
        return medicinelistitems;
    }

    var setMedicineListItems = function (data) {
        medicinelistitems = data;
    }

    var getMedicineItem = function () {
        return medicineitem;
    }
    var getLabItem = function () {
        return labtestitem;
    }
    var getScanItem = function () {
        return scantestitem;
    }
    var addConsultationInfo = function (data) {

        var consultationNew = data;
        ////console.log(JSON.stringify(consultation));
        datapersistanceFactory.set('consultation', data);
    }



    var getConsultationInfo = function () {
        var consultationNew = (datapersistanceFactory.getItem('consultation').length === 0) ? consultation : datapersistanceFactory.getItem('consultation');
        return consultationNew;
    }

    var getDentalproblems = function () {
        return dentalproblems;
    }
    var getTmjsCondition = function () {
        return tmjsCondition;
    }

    var getMucosaCondition = function () {
        return mucosaCondition;
    }

    var editapatientvitals = [];
    var addaddeditpatientvitals = function (data) {
        editapatientvitals = data;
        datapersistanceFactory.set('editapatientvitals', data);

    }
    var getaddeditpatientvitals = function () {
        editapatientvitals = (datapersistanceFactory.getItem('editapatientvitals').length === 0) ? editapatientvitals : datapersistanceFactory.getItem('editapatientvitals');

        return editapatientvitals;
    }

    var getNewConsultInfoStructure = function () {
        return consultation;
    }
    // addfrmRegPatient
    var addfrmRegPatient = true;
    var addfrmRegPatient = function (data) {
        datapersistanceFactory.set('addfrmRegPatient', data);

    }
    var getfrmRegPatient = function () {
        var addfrmRegPatient = (datapersistanceFactory.getItem('addfrmRegPatient').length === 0) ? addfrmRegPatient : datapersistanceFactory.getItem('addfrmRegPatient');
        return addfrmRegPatient;
    }

    //Dental

    var getAdultTootharray = function () {
        var AdultTootharray = [{
            teethId: 18,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A1'
        },
        {
            teethId: 17,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A1'
        },
        {
            teethId: 16,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A1'
        },
        {
            teethId: 15,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A1'
        },
        {
            teethId: 14,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A1'
        },
        {
            teethId: 13,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A1'
        },
        {
            teethId: 12,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A1'
        },
        {
            teethId: 11,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A1'
        },
        {
            teethId: 21,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A2'
        },
        {
            teethId: 22,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A2'
        },
        {
            teethId: 23,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A2'
        },
        {
            teethId: 24,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A2'
        },
        {
            teethId: 25,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A2'
        },
        {
            teethId: 26,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A2'
        },
        {
            teethId: 27,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A2'
        },
        {
            teethId: 28,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A2'
        },
        {
            teethId: 38,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A3'
        },
        {
            teethId: 37,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A3'
        },
        {
            teethId: 36,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A3'
        },
        {
            teethId: 35,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A3'
        },
        {
            teethId: 34,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A3'
        },
        {
            teethId: 33,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A3'
        },
        {
            teethId: 32,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A3'
        },
        {
            teethId: 31,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A3'
        },
        {
            teethId: 41,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A4'
        },
        {
            teethId: 42,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A4'
        },
        {
            teethId: 43,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A4'
        },
        {
            teethId: 44,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A4'
        },
        {
            teethId: 45,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A4'
        },
        {
            teethId: 46,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A4'
        },
        {
            teethId: 47,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A4'
        },
        {
            teethId: 48,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'A4'
        }
        ];
        return AdultTootharray;
    };
    var getChildTootharray = function () {
        var ChildTootharray = [{
            teethId: 55,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C1'
        },
        {
            teethId: 54,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C1'
        },
        {
            teethId: 53,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C1'
        },
        {
            teethId: 52,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C1'
        },
        {
            teethId: 51,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C1'
        },
        {
            teethId: 61,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C2'
        },
        {
            teethId: 62,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C2'
        },
        {
            teethId: 63,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C2'
        },
        {
            teethId: 64,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C2'
        },
        {
            teethId: 65,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C2'
        },
        {
            teethId: 75,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C3'
        },
        {
            teethId: 74,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C3'
        },
        {
            teethId: 73,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C3'
        },
        {
            teethId: 72,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C3'
        },
        {
            teethId: 71,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C3'
        },
        {
            teethId: 81,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C4'
        },
        {
            teethId: 82,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C4'

        },
        {
            teethId: 83,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C4'
        },
        {
            teethId: 84,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C4'
        },
        {
            teethId: 85,
            teethFlag: "N",
            gumsUpFlag: "N",
            gumsDownFlag: "N",
            Teethactive: "",
            gumupactive: "",
            gumdownactive: "",
            ImgSrc: "",
            Quadrant: 'C4'
        }
        ];
        return ChildTootharray;
    };

    var getTmjcustomArray = function () {
        var TmjcustomArray = [{

            "tmjDescription": "Joint tenderness",
            "tmjRight": "Right",
            "tmjRightcolor": "grey",
            "tmjRightId": 0,
            "tmjLeft": "Left",
            "tmjLeftcolor": "grey",
            "tmjleftId": 1,
            "Group": "1"
        },
        {
            "tmjDescription": "Muscle tenderness",
            "tmjRight": "Right",
            "tmjRightcolor": "grey",
            "tmjRightId": 2,
            "tmjLeft": "Left",
            "tmjLeftcolor": "grey",
            "tmjleftId": 3,
            "Group": "1"
        },
        {

            "tmjDescription": "Restrictions/deviations of movements",
            "tmjRight": "Right",
            "tmjRightcolor": "grey",
            "tmjRightId": 4,
            "tmjLeft": "Left",
            "tmjLeftcolor": "grey",
            "tmjleftId": 5,
            "Group": "1"
        },
        {

            "tmjDescription": "Clicking",
            "tmjRight": "Right",
            "tmjRightcolor": "grey",
            "tmjRightId": 6,
            "tmjLeft": "Left",
            "tmjLeftcolor": "grey",
            "tmjleftId": 7,
            "Group": "1"
        },
        {
            "tmjId": 5,
            "tmjDescription": "Lymphadenopathy",
            "tmjactivecolor": "grey",
            "tmjactive": "Present",
            "tmjactiveId": 8,
            "Group": "2"
        },
        {
            "tmjId": 6,
            "tmjDescription": "Facial asymmetry",
            "tmjactivecolor": "grey",
            "tmjactive": "Present",
            "tmjactiveId": 9,
            "Group": "2"
        }

        ];
        return TmjcustomArray;
    };

    var getMucosacustomarray = function () {
        var Mucosacustomarray = [{
            "MucosaDescription": "Hard Palate",
            "MucosaNormalCondition": "Normal",
            "MucosaNormalactive": "grey",
            "MucosaAbnormalCondition": "Abnormal",
            "MucosaAbnormalactive": "grey",
            "MucosaGroup": "1"
        },
        {
            "MucosaDescription": "Soft Palate",
            "MucosaNormalCondition": "Normal",
            "MucosaNormalactive": "grey",
            "MucosaAbnormalCondition": "Abnormal",
            "MucosaAbnormalactive": "grey",
            "MucosaGroup": "1"
        },
        {
            "MucosaDescription": "Pharynx",
            "MucosaNormalCondition": "Normal",
            "MucosaNormalactive": "grey",
            "MucosaAbnormalCondition": "Abnormal",
            "MucosaAbnormalactive": "grey",
            "MucosaGroup": "1"
        },
        {
            "MucosaDescription": "Tongue",
            "MucosaNormalCondition": "Normal",
            "MucosaNormalactive": "grey",
            "MucosaAbnormalCondition": "Abnormal",
            "MucosaAbnormalactive": "grey",
            "MucosaGroup": "2"
        },
        {
            "MucosaDescription": "Floor of Mouth",
            "MucosaNormalCondition": "Normal",
            "MucosaNormalactive": "grey",
            "MucosaAbnormalCondition": "Abnormal",
            "MucosaAbnormalactive": "grey",
            "MucosaGroup": "2"
        },
        {
            "MucosaDescription": "Attached Gingiva",
            "MucosaNormalCondition": "Normal",
            "MucosaNormalactive": "grey",
            "MucosaAbnormalCondition": "Abnormal",
            "MucosaAnbormalactive": "grey",
            "MucosaGroup": "2"
        }
        ];
        return Mucosacustomarray;
    };


    return {
        getConsultationInfo: getConsultationInfo,
        addConsultationInfo: addConsultationInfo,
        //getConsoltationService:getConsoltationService,
        getMedicineItem: getMedicineItem,
        getLabItem: getLabItem,
        getScanItem: getScanItem,
        getMedicineListItems: getMedicineListItems,
        setMedicineListItems: setMedicineListItems,
        addaddeditpatientvitals: addaddeditpatientvitals,
        getaddeditpatientvitals: getaddeditpatientvitals,
        getNewConsultInfoStructure: getNewConsultInfoStructure,
        getDentalproblems: getDentalproblems,
        getTmjsCondition: getTmjsCondition,
        getMucosaCondition: getMucosaCondition,
        getfrmRegPatient: getfrmRegPatient,
        addfrmRegPatient: addfrmRegPatient,
        "getAdultTootharray": getAdultTootharray,
        "getChildTootharray": getChildTootharray,
        "getTmjcustomArray": getTmjcustomArray,
        "getMucosacustomarray": getMucosacustomarray

    };

}])
hCueDoctorWebServices.factory('hcueDoctorSignUp', ['datapersistanceFactory', function (datapersistanceFactory) {
    var register = {
        doctorid: 0,
        addressid: 0,
        LoginId: '',
        Password: '',
        ConfirmPassword: '',

        Name: '',
        MobileNumber: 0,
        Email: '',
        DOB: null,
        specialization: [{ DoctorSpecialityID: '' }],
        MedicalRegNumber: '',
        Experience: '',
        About: '',
        Gender: 'M',
        CurrencyCode: '',
        CountryCode: '',
        CityTown: '',
        Address1: '',
        State: '',
        Country: '',
        AddressCommunication: [{
            PhCntryCD: '',
            PhStateCD: '',
            PhAreaCD: '',
            PhNumber: '',
            PhType: "M",
            RowID: 0,
            PrimaryIND: "Y",
            PublicDisplay: "Y"
        }],
        Qualification: [{ name: '' }],
        Services: [{ name: '' }],
        Memberships: [{ name: '' }],
        Awards: [{ name: '' }],
        Speciality: [{ DoctorSpecialityID: '' }],
        DoctorAchievements: [{ RowID: '', Year: '', Indicator: '', Name: '' }],
        DoctorPublishing: [{ RowID: '', publishedBy: '', publishedBooks: '', Year: '', Name: '' }],
        doctorEducation: [{ RowID: '', EducationName: '', InstituteName: '', FullName: '' }],

        //Consult Info
        ClinicName: '',
        ContactNumber: '',
        Fees: 0,
        MinPerCase: 15,
        Address: '',
        Latitude: '0',
        Longitude: '0',
        businesshoursUI: [
            { ApplyTo: false, AddressConsultID: 0, DayCode: 'Monday', DayCD: 'MON', Active: 'Y', StartTimeHH: '00', StartTimeMM: '00', StartTime: '00:00', EndTimeHH: '00', EndTimeMM: '00', EndTime: '21:00' }
        ]
    };
    /*
        Name: '', MobileNumber: 91,  DOB: 0, specialization: '', MedicalRegNumber: '', Experience: '', LoginId: '', Password: '', ConfirmPassword: '',
        About: '',
        Gender: 'M',
        Educations: [{ name: '' }],
        Services: [{ name: '' }],
        Memberships: [{ name: '' }],
        Awards: [{ name: '' }],
        Practises: [{ name: '' }],
        Speciality: [{ name: '' }],

        ClinicName: '',
        ContactNumber: '',
        Fees: 0,
        MinPerCase: 0,
        Address: '',

        Latitude: 0,
        Longitude: 0,
        Referrals: [{
            ClinicName: '', DoctorName: '', Specialization: { SpecializationId: '', Name: '' }, Address1: '', Address2: '',
            ContactName: '', MobileNmber: '', EmailId: ''
        }],

        businesshoursUI: [
            { AddressConsultID: 0, DayCode: 'Monday', DayCD: 'MON', Active: 'Y', StartTimeHH: '00', StartTimeMM: '00', StartTime: '00:00 AM', EndTimeHH: '00', EndTimeMM: '00', EndTime: '21:00 PM' }
        ]
    };*/
    var addProfileInfo = function (data) {

        register = data;
        datapersistanceFactory.set('register', register);
    }
    var getProfileInfo = function () {
        register = (datapersistanceFactory.getItem('register').length === 0) ? register : datapersistanceFactory.getItem('register');
        return register;
    }
    return {
        getProfileInfo: getProfileInfo,
        addProfileInfo: addProfileInfo
    }
}])
hCueDoctorWebServices.factory('AutoCompleteHelper', function () {
    var item;
    var addSelectedItem = function (data) {
        item = data;
    }
    var getSelectedItem = function () {
        return item;
    }
    return {
        addSelectedItem: addSelectedItem,
        getSelectedItem: getSelectedItem
    };

})

function GetKeyValuePair(lists) {
    var items = {};
    var rowno = 1;
    angular.forEach(lists, function (item) {
        items[rowno] = item.name;
        rowno++;
    });
    return items;
}

function GetKeyValuePairSpceciality(lists) {
    var items = {};
    var rowno = 1;
    angular.forEach(lists, function (item) {
        items[rowno] = item.SepcialityCode;
        rowno++;
    });
    return items;
}

function GetKeyValuePairCustomSpceciality(lists) {
    var items = {};
    angular.forEach(lists, function (item) {
        if (item.CustomSpecialityDesc !== undefined) {
            items[item.SepcialityCode] = item.CustomSpecialityDesc;
        }
    });
    return items;
}

function GetKeyValuePairEducationName(lists) {
    var items = {};
    var rowno = 1;
    angular.forEach(lists, function (item) {
        items[rowno] = item.EducationName;
        rowno++;
    });
    return items;
}

function GetKeyValueSpPair(lists) {
    var items = {};
    var rowno = 1;
    angular.forEach(lists, function (item) {
        //alert('gpv' + JSON.stringify(item));
        // alert('first' +item.DoctorSpecialityID);
        // alert('Second' + item.DoctorSpecialityID);
        items[rowno] = item.DoctorSpecialityID;
        rowno++;
    });
    return items;
}

function GetImage(serverImage) {

    if (serverImage != null && serverImage.substring(0, 5).toLowerCase() == "some(") {
        serverImage = serverImage.substring(5);
        serverImage = serverImage.substring(0, serverImage.length - 1)
        return serverImage;
    }
    return "";
}

//bala code 

hCueDoctorWebServices.service('hCueDoctorAppoitmentService', ['$http', 'hcueServiceUrl', 'hcueOfflineServiceUrl', function ($http, hcueServiceUrl, hcueOfflineServiceUrl) {
    this.getDoctorAppoitmentInfo = function (callbackSuccess, callbackError, datestr) {
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getDoctorAppoitment, datestr) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getDoctorAvailAppointment, datestr);
        offlinePost.success(function (data) {
            //console.log("777");
            callbackSuccess(data);
        }).error(function (data) {
            callbackError(data);
        });

    }
}])

hCueDoctorWebServices.service('hCueAppointmentService', ['$http', 'hcueServiceUrl', 'hcueOfflineServiceUrl', '$filter', '$timeout', 'hcueDoctorLoginService', function ($http, hcueServiceUrl, hcueOfflineServiceUrl, $filter, $timeout, hcueDoctorLoginService) {
    this.getClinicDoctors = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.clinicDoctorList, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.GetListDetailsForNurse = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.GetListDetailsForNurse, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    //GetPreviousServices
    this.GetPreviousServices = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.GetListDetailsForNurse, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    //addNewServices for nurse flow
    this.addNewServices = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.AddPatientServices, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.savevitals = function (param, callbackSuccess, callbackError) {
        //console.log(JSON.stringify(param));
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.addPatientvitals, param, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.addPatientvitals, param, header);
        offlinePost.success(function (data) {
            //console.log(JSON.stringify(data));
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });

    }

    this.getvitals = function (param, callbackSuccess, callbackError) {
        ////console.log(JSON.stringify(param));
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getpatientvitals, param, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getpatientvitals, param);
        offlinePost.success(function (data) {
            //console.log(JSON.stringify(data));
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });

    }

    this.getOfflinePatientDetails = function (callbacksuccess, callbackError, addedAppointment) {
        //var offlinePatData = $http.get(localStorage.getItem("offlineURL") + '/pluck/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getdoctorPatient + '?searchkey=PatientID' + '&search=' + addedAppointment.PatientID);
        var offlinePatData = $http.get(localStorage.getItem("offlineURL") + '/patient/' + doctorinfo.doctorid + '?search=' + addedAppointment.PatientID);
        offlinePatData.success(function (patData) {
            console.log(JSON.stringify(patData));
            //if (patData.message == "No data found!") {
            //    //offlineCollection = { 'rows': loadData, 'count': loadData.length };
            //    //callbackSuccess(offlineCollection);
            //}
            //else {
            //    console.log(JSON.stringify(patData));
            //    patData[0].AppointmentStatus = "B";
            //    patData[0].MobileNumber = patData[0].PhoneNumber;
            //    console.log(JSON.stringify(addData));
            //    var fitAddData = { "sequence": 0, "StartTime": addData.StartTime, "EndTime": addData.EndTime, "Available": "N", "TokenNumber": "Offline", "PatientInfo": patData[0] };
            //    //loadData[0].SlotList.push(fitAddData);
            //    //offlineCollection = { 'rows': loadData, 'count': loadData.length };
            //    //console.log(JSON.stringify(offlineCollection));

            //    callbacksuccess(fitAddData);
            //}
            callbacksuccess(patData);
        })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getOfflineAppointmentListPatDetails = function (callbacksuccess, callbackError, addedAppointment) {
        var offlinePatData = $http.get(localStorage.getItem("offlineURL") + '/data/' + addedAppointment.PatientID + '/' + hcueOfflineServiceUrl.getPatient);
        offlinePatData.success(function (patData) {
            callbacksuccess(patData);
        })
            .error(function (data) {
                callbackError(data);
            });
    }
    //DOCHOSPAPPOINTMENTS
    this.getHospitalAppointments = function (param, hospcode, callbackSuccess, callbackError) {
        if (hospcode !== undefined) {
            // $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getHospitalAppointments, param);
            //   param.HospitalCD = hospcode;
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getHospitalAppointments, param, header) :
                $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getDoctorsAppointments + '?date=' + param.ConsultationDt);


            // $http.post( hcueServiceUrl.getHospitalAppointments, param, header)
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        }
        //else
        //{
        //   $http.post(hcueServiceUrl.getHospitalAppointments, param, header)
        //        .success(function (data) {
        //            callbackSuccess(data);
        //        })
        //        .error(function (data) {
        //            callbackError(data);
        //        }); 
        //}
    }

    this.getHospitalLabAppointment = function (param, hospcode, callbackSuccess, callbackError) {

        if (hospcode !== undefined) {
            param.HospitalCD = hospcode;
        }
        console.log(JSON.stringify(param));
        if (JSON.parse(localStorage.getItem("isAppOnline"))) {
            $http.post(hcueServiceUrl.getHospitalLabAppointments, param, header)
                .success(function (data) {
                    console.log(JSON.stringify(data));
                    callbackSuccess(data);

                })
                .error(function (data) {
                    callbackError(data);
                });
        }

    }

    this.UpdateHospitalAppointments = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.updatePatientAppointments, params, header)
            .success(function (data) {
                if (data != null) {
                    callbackSuccess(data, status);
                }
            })
            .error(function (data) {
                callbackError(data, status);
            });
    }

    this.AddHospitalAppointments = function (params, callbackSuccess, callbackError) {

        //var params = {
        //    AppointmentStatus: "B", StartTime: starttime, EndTime: endtime, DoctorID: doctorid, DayCD: daycd, VisitUserTypeID: "OPATIENT",
        //    ConsultationDt: consultdate, AddressConsultID: addressconsultid, PatientID: patientid, USRId: doctorid, USRType: "DOCTOR", OtherVisitRsn: visitreason, DoctorVisitRsnID: VisitRsnType
        //};

        $http.post(hcueServiceUrl.addHospitalAppointment, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }
    this.sendLabLeads = function (params, callbackSuccess, callbackError) {

        //var params = {
        //    AppointmentStatus: "B", StartTime: starttime, EndTime: endtime, DoctorID: doctorid, DayCD: daycd, VisitUserTypeID: "OPATIENT",
        //    ConsultationDt: consultdate, AddressConsultID: addressconsultid, PatientID: patientid, USRId: doctorid, USRType: "DOCTOR", OtherVisitRsn: visitreason, DoctorVisitRsnID: VisitRsnType
        //};

        $http.post(hcueServiceUrl.sendLableads, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }

    this.getManageDoctors = function (param, callbackSuccess, callbackError) {
        console.log(JSON.stringify(hcueServiceUrl.getManageDoctors) + " -- " + JSON.stringify(param));
        $http.post(hcueServiceUrl.getManageDoctors, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }

    this.getOfflinePatient = function (DocID, callbackSuccess, callbackError) {
        var tmpData = [];
        var tamPatValue = {
            "PatientID": 0,
            "Title": "",
            "FullName": "",
            "CurrentAge": {
                "year": 0,
                "month": 0,
                "day": 0
            },
            "Gender": "",
            "DisplayID": "",
            "FamilyDisplayID": "",
            "FamilyHdID": 0,
            "offlineAddedPatient": {},
            "patientMembershipDtls": []
        };

        var offlineGetAddPatient = $http.get(localStorage.getItem("offlineURL") + '/data/' + DocID + '/' + hcueOfflineServiceUrl.addPatient + '?includeid=1');

        offlineGetAddPatient.success(function (data) {
            console.log(JSON.stringify(data));

            if (data.message !== "No data found!") {
                if (data.length !== undefined && data.length > 0) {
                    for (var i = 0; data.length > i; i++) {
                        tamPatValue = {
                            "PatientID": 0,
                            "Title": "",
                            "FullName": "",
                            "CurrentAge": {
                                "year": 0,
                                "month": 0,
                                "day": 0
                            },
                            "Gender": "",
                            "DisplayID": "",
                            "FamilyDisplayID": "",
                            "FamilyHdID": 0,
                            "offlineAddedPatient": {},
                            "patientMembershipDtls": []
                        };

                        tamPatValue.PatientID = 0;
                        tamPatValue.Title = data[i].patientDetails.Title;
                        tamPatValue.FullName = data[i].patientDetails.FullName;
                        tamPatValue.CurrentAge.year = data[i].patientDetails.Age;
                        tamPatValue.Gender = data[i].patientDetails.Gender;
                        tamPatValue.DisplayID = data[i].GenIDDetails.PatientDisplayID;
                        tamPatValue.FamilyDisplayID = data[i].GenIDDetails.FamilyDisplayID;
                        tamPatValue.FamilyHdID = 0;
                        if (data[i].patientPhone.length !== 0) {
                            tamPatValue.PhoneNumber = data[i].patientPhone[0].PhNumber;
                        }
                        tamPatValue.offlineAddedPatient = data[i];
                        tmpData.push(tamPatValue)
                    }
                } else {
                    tamPatValue.PatientID = 0;
                    tamPatValue.Title = data.patientDetails.Title;
                    tamPatValue.FullName = data.patientDetails.FullName;
                    tamPatValue.CurrentAge.year = data.patientDetails.Age;
                    tamPatValue.Gender = data.patientDetails.Gender;
                    tamPatValue.DisplayID = data.GenIDDetails.PatientDisplayID;
                    tamPatValue.FamilyDisplayID = data.GenIDDetails.FamilyDisplayID;
                    tamPatValue.FamilyHdID = 0;
                    if (data.patientPhone.length !== 0) {
                        tamPatValue.PhoneNumber = data.patientPhone[0].PhNumber;
                    }
                    tamPatValue.offlineAddedPatient = data;
                    tmpData.push(tamPatValue)
                }


            }
            console.log(JSON.stringify(tmpData));
            callbackSuccess(tmpData);
            //callbackSuccess(tmpData);
        })
            .error(function (data) {
                callbackError(data);
            });
    }

    //Get Booked Appointment
    this.GetBookedAppointment = function (dayCode, clinicaddresslist, consultDate, callbackSuccess, callbackError) {
        GetAppointment('B', dayCode, clinicaddresslist, consultDate, callbackSuccess, callbackError);
    }

    function GetAppointment(status, dayCode, clinicaddresslist, consultDate, callbackSuccess, callbackError) {
        //Check for addressid working on the daycode, else return empty and doesnt make any post back to the server
        //console.log("2");
        var addressConsultID = clinicaddresslist;

        if (addressConsultID.length == 0) {
            callbackError(null);
            return;
        }

        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getDoctorsAppointments, { Status: status, AddressConsultID: addressConsultID, ConsultationDt: consultDate, ShowDueAmount: "Y" }, header) : $http.post(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getDoctorsAppointments, { Status: status, AddressConsultID: addressConsultID, ConsultationDt: consultDate });
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            })
    }
    //

    this.GetAllBookedAppointmentUsingAddressID = function (DocId, dayCode, addressID, consultDate, callbackSuccess, callbackError) {

        if (addressID == undefined) {
            addressID = 0;
        }
        $http.post(hcueServiceUrl.getDoctorsAppointments, { AddressID: addressID, ConsultationDt: consultDate, ShowDueAmount: "Y" }, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }
    this.GetAllNewBookedAppointmentUsingAddressID = function (DocId, dayCode, addressID, consultDate, callbackSuccess, callbackError) {

        if (addressID == undefined) {
            addressID = 0;
        }
        if (addressID == undefined) {
            addressID = 0;
        }
        $http.post(hcueServiceUrl.getDoctorsAppointments, { AddressID: addressID, ConsultationDt: consultDate, ShowDueAmount: "Y", Version: "V1" }, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }

    this.GetAllBookedAppointmentforAllDoctors = function (hospitalID, consultDate, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getDoctorsAppointments, { ConsultationDt: consultDate, HospitalID: hospitalID, ShowDueAmount: "Y" }, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }
    //  this.GetAllNewBookedAppointmentforAllDoctors = function (hospitalID, consultDate, callbackSuccess, callbackError){
    //      $http.post(hcueServiceUrl.getDoctorsAppointments, { ConsultationDt: consultDate,HospitalID: hospitalID,ShowDueAmount:"Y",Version:"V1"}, header)
    //        .success(function (data) {
    //            callbackSuccess(data);
    //        })
    //            .error(function (data) {
    //                callbackError(data);
    //            })


    //  }
    this.GetAllNewBookedAppointment = function (dayCode, clinicaddresslist, consultDate, callbackSuccess, callbackError, docid) {
        var addressConsultID = clinicaddresslist;
        var offlineData = [];
        if (addressConsultID.length == 0) {
            callbackError(null);
            return;
        }
        var DoctorID = (docid == 0) ? doctorinfo.doctorid : docid;
        var offlinePost = $http.post(hcueServiceUrl.getDoctorsAppointments, { AddressID: addressConsultID, ConsultationDt: consultDate, ShowDueAmount: "Y", "Version": "V1" }, header)
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            })
    }
    this.GetAllBookedAppointment = function (dayCode, clinicaddresslist, consultDate, callbackSuccess, callbackError, docid) {
        GetAllAppointment(dayCode, clinicaddresslist, consultDate, callbackSuccess, callbackError, docid);
    }


    function GetAllAppointment(dayCode, clinicaddresslist, consultDate, callbackSuccess, callbackError, docid) {
        var addressConsultID = clinicaddresslist;
        var offlineData = [];
        if (addressConsultID.length == 0) {
            callbackError(null);
            return;
        }
        var DoctorID = (docid == 0) ? doctorinfo.doctorid : docid;
        var offlinePost = $http.post(hcueServiceUrl.getDoctorsAppointments, { AddressID: addressConsultID, ConsultationDt: consultDate, ShowDueAmount: "Y" }, header)
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            })
    }

    function GetOfflineAddedAppointment(loadData, callbackSuccess, callbackError) {
        var cDate = $filter('date')(new Date(loadData[0].ConsultationDate), "yyyy-MM-dd");
        //$http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.docAddAppointment + '?date=' + cDate)

        var offlineGetAddAppointment = $http.get(localStorage.getItem("offlineURL") + '/search/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.docAddAppointment + '?searchkey=ConsultantDate&search=' + cDate);
        offlineGetAddAppointment.success(function (addData) {
            console.log(JSON.stringify(addData));
            if (addData.message == undefined) {
                var offlinePatData = $http.get(localStorage.getItem("offlineURL") + '/pluck/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getdoctorPatient + '?searchkey=PatientID' + '&search=' + addData.PatientID);
                offlinePatData.success(function (patData) {
                    console.log(JSON.stringify(patData));
                    patData[0].AppointmentStatus = "B";
                    patData[0].MobileNumber = patData[0].PhoneNumber;
                    console.log(JSON.stringify(addData));
                    var fitAddData = { "sequence": 0, "StartTime": addData.StartTime, "EndTime": addData.EndTime, "Available": "N", "TokenNumber": "Offline", "PatientInfo": patData[0] };
                    loadData[0].SlotList.push(fitAddData);
                    offlineCollection = { 'rows': loadData, 'count': loadData.length };
                    console.log(JSON.stringify(offlineCollection));

                    callbackSuccess(offlineCollection);

                });
            } else {
                offlineCollection = { 'rows': loadData, 'count': loadData.length };
                callbackSuccess(offlineCollection);
            }


        }).error(function (data) {
            callbackError(data);
        });
    }

    //Get OfflinePat Dtls

    this.getOffilineAppointment = function (DocID, docType, tmpDate, callbackSuccess, callbackError) {
        // var cDate = $filter('date')(new Date(), "yyyy-MM-dd");
        //hcueDoctorLoginService.addOfflineAppointmentDtls("");
        var tmpData = [];
        if (hcueOfflineServiceUrl.docAddAppointment == docType || hcueOfflineServiceUrl.onlineAppointment == docType) {

            var chkToDate = $filter('date')(new Date(), "yyyy-MM-dd");
            if (chkToDate != tmpDate && hcueOfflineServiceUrl.docAddAppointment == docType) {
                offlineGetAddAppointment = $http.get(localStorage.getItem("offlineURL") + '/search/' + DocID + '/' + docType + '?searchkey=ConsultantDate&search=' + tmpDate);
            } else {
                var offlineGetAddAppointment = $http.get(localStorage.getItem("offlineURL") + '/data/' + DocID + '/' + docType + '?includeid=1&date=' + tmpDate);
            }
        }
        //else
        //{
        //    var offlineGetAddAppointment = $http.get(localStorage.getItem("offlineURL") + '/data/' +  docType + '?includeid=1&date=' + tmpDate);
        //}
        /*
        if(DocID !=0 ){
            var offlineGetAddAppointment = $http.get(localStorage.getItem("offlineURL") + '/data/' + DocID + '/' + docType + '?date=' + tmpDate);
        }
        else{
            var offlineGetAddAppointment = $http.get(localStorage.getItem("offlineURL") + '/data/'  + docType + '?date=' + tmpDate);
        }
        */
        offlineGetAddAppointment.success(function (data) {

            console.log(JSON.stringify(data));
            //  if( hcueOfflineServiceUrl.docAddAppointment == docType && data.message !== "No data found!")
            if (data.message == undefined) {
                if (data.length !== undefined && data.length > 0) {
                    for (var i = 0; data.length > i; i++) {
                        if (data[i].AuditLog !== undefined && data[i].AuditLog.AddAppointment !== undefined) {
                            //data[i].AuditLog.AddAppointment.AppointmentStatus = "E";
                            //tmpData.push(data[i].AuditLog.AddAppointment); 
                        }
                        tmpData.push(data[i]);
                    }
                } else {
                    if (data.AuditLog !== undefined && data.AuditLog.AddAppointment !== undefined) {
                        //data.AuditLog.AddAppointment.AppointmentStatus = "E";
                        //tmpData.push(data.AuditLog.AddAppointment);
                    }
                    tmpData.push(data);
                }
            }

            hcueDoctorLoginService.addOfflineAppointmentDtls(tmpData);
            //   callbackSuccess(tmpData);
            //}
            //else
            //{
            var onlineDataValue = [];
            if (data.message == undefined) {
                tmpData = data;
            }

            var offlineGetAddAppointment = $http.get(localStorage.getItem("offlineURL") + '/data/' + DocID + '/' + hcueOfflineServiceUrl.onlineAppointment + '?includeid=1&date=' + tmpDate);
            offlineGetAddAppointment.success(function (onlineData) {
                console.log(JSON.stringify(onlineData));
                if (onlineData.message == undefined) {
                    console.log(JSON.stringify(tmpData));
                    if (tmpData.length != undefined && tmpData.length > 0) {
                        onlineDataValue = tmpData;
                        if (onlineData.length !== undefined && onlineData.length > 0) {
                            for (var k = 0; onlineData.length > k; k++) {
                                onlineData[k].online = true;
                                onlineDataValue.push(onlineData[k]);
                            }
                        } else {
                            onlineData.online = true;
                            tmpData.push(onlineData);
                            callbackSuccess(tmpData);
                        }
                        callbackSuccess(onlineDataValue);
                        //onlineDataValue.push(tmpData);
                    } else {
                        if (tmpData.message == undefined) {
                            if (onlineData.length !== undefined && onlineData.length > 0) {
                                for (var k = 0; onlineData.length > k; k++) {
                                    onlineData[k].online = true;
                                    onlineDataValue.push(onlineData[k]);
                                }
                                onlineDataValue = onlineData;
                                onlineDataValue.push(tmpData);
                            } else {
                                onlineData.online = true;
                                onlineDataValue.push(onlineData);
                                if (tmpData.message == undefined) {
                                    onlineDataValue.push(tmpData);
                                }

                            }
                            callbackSuccess(onlineDataValue);
                        } else {
                            onlineDataValue = onlineData;
                            callbackSuccess(onlineDataValue);
                        }


                    }


                } else {
                    if (data.message == undefined) {
                        tmpData = data;
                        callbackSuccess(tmpData);
                    } else {
                        callbackSuccess(data);
                    }
                }
            })
                .error(function (onlineData) {
                    callbackError(onlineData);
                });


            // }
            console.log(JSON.stringify(tmpData));

            //callbackSuccess(tmpData);
        })
            .error(function (data) {
                callbackError(data);
            });
    }

    //Get DoctorAppointmentDetails
    this.getDoctorAppoitmentInfo = function (callbackSuccess, callbackError, datestr, blnChange) {

        var offlineCollection = {};
        console.log(blnChange + " " + JSON.stringify(datestr) + " " + JSON.parse(localStorage.getItem("isAppOnline")));
        //if (blnChange == true && !JSON.parse(localStorage.getItem("isAppOnline")))
        if (!JSON.parse(localStorage.getItem("isAppOnline"))) {
            var offlinePost = $http.get(localStorage.getItem("offlineURL") + '/pluck/' + datestr.DoctorID + '/' + hcueOfflineServiceUrl.getDoctorAvailAppointment + '?searchkey=AddressID' + '&search=' + datestr.AddressID + '&date=' + datestr.filterByDate);
            offlinePost.success(function (loadData) {

                ////GetOfflineAddedAppointment(loadData, callbackSuccess, callbackError);
                //var cDate = $filter('date')(new Date(loadData[0].ConsultationDate), "yyyy-MM-dd");

                //var offlineGetAddAppointment = $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.docAddAppointment + '?date=' + cDate);
                //offlineGetAddAppointment.success(function (addData) {

                //    hcueDoctorLoginService.addOfflineAppointmentDtls(addData);

                //    console.log(JSON.stringify(addData));
                //    console.log(JSON.stringify(addData.message));
                //    if (addData.message == undefined) {
                //        var offlinePatData = $http.get(localStorage.getItem("offlineURL") + '/pluck/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getdoctorPatient + '?searchkey=PatientID' + '&search=' + addData.PatientID);
                //        offlinePatData.success(function (patData) {
                //            console.log(JSON.stringify(patData));
                //            if(patData.message =="No data found!")
                //            {
                //                offlineCollection = { 'rows': loadData, 'count': loadData.length };
                //                callbackSuccess(offlineCollection);
                //            }
                //           else{
                //                console.log(JSON.stringify(patData));
                //                patData[0].AppointmentStatus = "B";
                //                patData[0].MobileNumber = patData[0].PhoneNumber;
                //                console.log(JSON.stringify(addData));
                //                var fitAddData = { "sequence": 0, "StartTime": addData.StartTime, "EndTime": addData.EndTime, "Available": "N", "TokenNumber": "Offline", "PatientInfo": patData[0] };
                //                loadData[0].SlotList.push(fitAddData);
                //                offlineCollection = { 'rows': loadData, 'count': loadData.length };
                //                console.log(JSON.stringify(offlineCollection));

                //                callbackSuccess(offlineCollection);
                //            }

                //        });
                //    }
                //    else {
                //        offlineCollection = { 'rows': loadData, 'count': loadData.length };
                //        callbackSuccess(offlineCollection);
                //    }
                //    offlineCollection = { 'rows': loadData, 'count': loadData.length };
                //    callbackSuccess(offlineCollection);

                //}).error(function (data) {
                //   callbackError(data);
                //});
                if (loadData.message == "No data found!") {
                    offlineCollection = loadData;
                } else {
                    offlineCollection = { 'rows': loadData, 'count': loadData.length };
                }

                callbackSuccess(offlineCollection);

            }).error(function (data) {
                callbackError(data);
            });
        } else {
            //var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getDoctorAppoitment, datestr) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getDoctorAppoitment, datestr);
            var offlinePost = $http.post(hcueServiceUrl.getDoctorAppoitment, datestr);
            offlinePost.success(function (data) {
                if (JSON.parse(localStorage.getItem("isAppOnline")) == false) {
                    offlineData = data[0];
                    console.log(JSON.stringify(offlineData));
                    callbackSuccess(offlineData);
                } else {
                    callbackSuccess(data);
                }
            }).error(function (data) {
                callbackError(data);
            });
        }

    }
    this.getDoctorClinicData = function (param, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.getDocClinicData + param + '/lookup', header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getDoctorNewAppointmentInfo = function (callbackSuccess, callbackError, datestr, blnChange) {

        var offlineCollection = {};
        console.log(blnChange + " " + JSON.stringify(datestr) + " " + JSON.parse(localStorage.getItem("isAppOnline")));
        //if (blnChange == true && !JSON.parse(localStorage.getItem("isAppOnline")))
        if (!JSON.parse(localStorage.getItem("isAppOnline"))) {
            var offlinePost = $http.get(localStorage.getItem("offlineURL") + '/pluck/' + datestr.DoctorID + '/' + hcueOfflineServiceUrl.getDoctorAvailAppointment + '?searchkey=AddressID' + '&search=' + datestr.AddressID + '&date=' + datestr.filterByDate);
            offlinePost.success(function (loadData) {

                if (loadData.message == "No data found!") {
                    offlineCollection = loadData;
                } else {
                    offlineCollection = { 'rows': loadData, 'count': loadData.length };
                }

                callbackSuccess(offlineCollection);

            }).error(function (data) {
                callbackError(data);
            });
        } else {
            //var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.getDoctorAppoitment, datestr) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.getDoctorAppoitment, datestr);
            var offlinePost = $http.post(hcueServiceUrl.getDoctorAvailableAppointments, datestr);
            offlinePost.success(function (data) {
                if (JSON.parse(localStorage.getItem("isAppOnline")) == false) {
                    offlineData = data[0];
                    console.log(JSON.stringify(offlineData));
                    callbackSuccess(offlineData);
                } else {
                    callbackSuccess(data);
                }
            }).error(function (data) {
                callbackError(data);
            });
        }

    }
    this.CancelAppointmentList = function (appointmentobj, addressConsultID, dayCD, doctorid, consultDate, status, startTime, endTime, VisitRsnType, cancelreason, callbackSuccess, callbackError) {
        var param = {
            AppointmentID: appointmentobj.AppointmentID,
            AddressConsultID: addressConsultID,
            DayCD: dayCD,
            ConsultationDt: consultDate,
            StartTime: startTime,
            EndTime: endTime,
            PatientID: appointmentobj.PatientID,
            DoctorVisitRsnID: VisitRsnType,
            OtherVisitRsn: "CANCEL",
            AppointmentStatus: status,
            DoctorID: doctorid,
            USRType: "DOCTOR",
            USRId: appointmentobj.PatientID,
            VisitUserTypeID: "OPATIENT",
            Reason: cancelreason
        };
        if (param.Reason == "") {
            delete param.Reason;
        }
        //$http.post(hcueServiceUrl.updtDoctorAppointments, param, header)
        $http.post(hcueServiceUrl.updtDoctorAppointments, param, header)
            .success(function (data) {
                if (data != null) {
                    callbackSuccess(data, status);
                }
            })
            .error(function (data) {
                callbackError(data, status);
            });
    }

    this.CancelAppointment = function (appointmentID, addressConsultID, dayCD, doctorid, consultDate, status, startTime, endTime, patientID, VisitRsnType, callbackSuccess, callbackError) {
        var param = {
            AppointmentID: appointmentID,
            AddressConsultID: addressConsultID,
            DayCD: dayCD,
            ConsultationDt: consultDate,
            StartTime: startTime,
            EndTime: endTime,
            PatientID: patientID,
            DoctorVisitRsnID: VisitRsnType,
            OtherVisitRsn: "CANCEL",
            AppointmentStatus: status,
            DoctorID: doctorid,
            USRType: "DOCTOR",
            USRId: patientID,
            VisitUserTypeID: "OPATIENT"
        };

        $http.post(hcueServiceUrl.updtDoctorAppointments, param, header)
            .success(function (data) {
                if (data != null) {
                    callbackSuccess(data, status);
                }
            })
            .error(function (data) {
                callbackError(data, status);
            });
    }

    this.CancelCalAppointment = function (appointmentID, addressID, dayCD, doctorid, consultDate, status, startTime, endTime, patientID, VisitRsnType, sendSms, sendMail, cancelReason, callbackSuccess, callbackError) {
        var param = {
            AppointmentID: appointmentID,
            AddressID: addressID,
            DayCD: dayCD,
            ConsultationDt: consultDate,
            StartTime: startTime,
            EndTime: endTime,
            PatientID: patientID,
            DoctorVisitRsnID: VisitRsnType,
            AppointmentStatus: status,
            DoctorID: doctorid,
            USRType: "DOCTOR",
            USRId: patientID,
            VisitUserTypeID: "OPATIENT",
            AppointmentOtherDetails: { ReasonForCancellation: cancelReason },
            SendNotification: {
                sendPatientSMS: sendSms,
                sendPatientEmail: sendMail,
                sendDoctorReminderSms: "Y",
                sendDoctorPushNotification: "Y"
            },
        };

        $http.post(hcueServiceUrl.addUpdateAppointment, param, header)
            .success(function (data) {
                if (data != null) {
                    callbackSuccess(data, status);
                }
            })
            .error(function (data) {
                callbackError(data, status);
            });
    }

    this.GetPatientsbyId = function (patientid, callbackSuccess, callbackError) {
        console.log("getPatbyIDs");
        var url = hcueServiceUrl.getPatientbyId + "/" + patientid + "/getPatient";

        var rtnData = [];
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.get(url, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + patientid + '/' + hcueOfflineServiceUrl.getdoctorPatient);
        offlinePost.success(function (data) {
            //console.log(JSON.stringify(data));
            if (data.Patient[0].CurrentAge != undefined) {

                if (data.Patient[0].CurrentAge.month < 10) {
                    data.Patient[0].CurrentAge.month = "0" + data.Patient[0].CurrentAge.month;
                }
                //console.log(JSON.stringify(data));
            } else {
                data.Patient[0].Age = data.Patient[0].Age;
            }

            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            })
        return rtnData;

    }

    this.GetAppointmentsbyId = function (appointmentid, callbackSuccess, callbackError) {

        var url = hcueServiceUrl.getAppointments + "/" + appointmentid + "/appointmentStatus";

        var rtnData = [];
        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.get(url, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + appointmentid + '/' + hcueOfflineServiceUrl.getAppointmentStatus);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        }).error(function (data) { });
        return rtnData;

    }

    //getPatient post
    this.Getpatienteditid = function (param, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.updatePatientDetails, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }


    function checkOfflineStatus() {
        setTimeout(function () {
            $http.get(localStorage.getItem("offlineURL") + '/syncwatch')
                .success(function (data) {
                    if (data.SyncCount > 0) {
                        alert('Data not synced properly.Kindly sync again');
                    } else {
                        alert("Download completed successfully");
                    }
                })
                .error(function (data) {

                })
        }, 50000);
    }

    this.SyncOfflineData = function (callbackSuccess, callbackError) {
        checkOfflineStatus();
        var offlineUpPost = $http.get(localStorage.getItem("offlineURL") + '/sync/UPSTREAM/');
        offlineUpPost.success(function (data) {
            var param = { "Status": 1, "pagenumber": 1, "clearexisting": true, "doctorid": doctorinfo.doctorid, "trandate": $filter('date')(new Date(), "yyyy-MM-dd") };
            var doctype = "'" + hcueOfflineServiceUrl.getDoctorsAppointments + "','" + hcueOfflineServiceUrl.getDoctorAvailAppointment + "','" + hcueOfflineServiceUrl.readVisitReason + "','" + hcueOfflineServiceUrl.readVitals + "','" + hcueOfflineServiceUrl.listDoctorProcedure + "','" + hcueOfflineServiceUrl.readDoctorHistorySetting + "','" + hcueOfflineServiceUrl.readdiagnosis + "','" + hcueOfflineServiceUrl.getHospitalPaymentCompanyList + "','" + hcueOfflineServiceUrl.listDocSubCategorySettings + "','" + hcueOfflineServiceUrl.getdoctorPatient + "'";
            //console.log(JSON.stringify(doctype));
            var offlineRemoveData = $http.post(localStorage.getItem("offlineURL") + "/reload/" + doctype, param);
            offlineRemoveData.success(function (reloadData) {
                callbackSuccess(reloadData);
            })
                .error(function (reloadData) {
                    callbackError(reloadData);
                });
        })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.OfflineDownloadData = function (callbackSuccess, callbackError) {
        var offlineDownPost = $http.get(localStorage.getItem("offlineURL") + '/sync/DOWNSTREAM/');
        offlineDownPost.success(function (downData) {
            //console.log("D... " + JSON.stringify(downData));
            callbackSuccess(downData);
        })
            .error(function (downData) {
                callbackError(downData);
            });
    }

}])
hCueDoctorWebServices.service('hCuePaymentServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getPaymentCompanyList = function (params, callbackSuccess, callbackError) {



        $http.post(hcueServiceUrl.PaymentCompanyListing, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.addUpdatePaymentCompany = function (params, callbackSuccess, callbackError) {



        $http.post(hcueServiceUrl.AddUpdatePaymentCompany, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}])
//diagonsis services
hCueDoctorWebServices.service('hCueDiagnosisServices', ['$http', 'hcueServiceUrl', 'hcueOfflineServiceUrl', function ($http, hcueServiceUrl, hcueOfflineServiceUrl) {

    this.AddUpdateDiagnosisServicesList = function (params, hospid, callbackSuccess, callbackError) {
        if (hospid !== undefined) {
            params.HospitalCD = hospid;
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.diagnosisaddUpdate, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.diagnosisaddUpdate, params);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        } else {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.diagnosisaddUpdate, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.diagnosisaddUpdate, params);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        }

    }
    this.getDiagnosisServicesList = function (params, hospid, callbackSuccess, callbackError) {
        if (hospid !== undefined) {
            params.HospitalCD = hospid;
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readdiagnosis, params, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readdiagnosis, params, header);

            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        } else {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readdiagnosis, params, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readdiagnosis, params, header);

            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        }
    }

}])


hCueDoctorWebServices.service('hCueVisitReasonServices', ['$http', 'hcueServiceUrl', 'hcueOfflineServiceUrl', function ($http, hcueServiceUrl, hcueOfflineServiceUrl) {

    this.AddUpdateVisitReasonServicesList = function (params, hospid, callbackSuccess, callbackError) {
        if (hospid !== undefined) {

            params.HospitalCD = hospid;
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.visitReasonAddUpdate, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.visitReasonAddUpdate, params);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        } else {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.visitReasonAddUpdate, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.visitReasonAddUpdate, params);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        }

    }
    this.getVisitReasonServicesList = function (params, hospid, callbackSuccess, callbackError) {
        if (hospid !== undefined) {
            params.HospitalCD = hospid;
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readVisitReason, params, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readVisitReason, params, header);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        } else {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.readVisitReason, params, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.readVisitReason, params, header);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        }
    }

    this.addUpdateCheckIn = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.updtDoctorAppointments, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }


}])


hCueDoctorWebServices.service('hCueReferralServices', ['$http', 'hcueServiceUrl', 'hcueOfflineServiceUrl', function ($http, hcueServiceUrl, hcueOfflineServiceUrl) {

    this.AddUpdateReferralServicesList = function (params, hospid, callbackSuccess, callbackError) {
        //console.log(hcueServiceUrl.referalAddUpdate);
        if (hospid !== undefined) {
            params.HospitalCD = hospid;
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.referalAddUpdate, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.referalAddUpdate, params);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        } else {
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.referalAddUpdate, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.referalAddUpdate, params);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        }
    }


    this.getReferralServicesListForComm = function (params, callbackSuccess, callbackError) {
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined) {
            var hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            params.HospitalCD = hospcd;
            $http.post(hcueServiceUrl.referalread, params, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                });
        } else {
            console.log('1');
            $http.post(hcueServiceUrl.referalread, params, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                });

        }
    };

    this.getReferralServicesList = function (params, hospid, callbackSuccess, callbackError) {
        if (hospid !== undefined) {
            console.log('2');
            params.HospitalCD = hospid;
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.referalread, params, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.referalread, params, header);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });
        } else {
            console.log('1');
            var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.referalread, params, header) : $http.get(localStorage.getItem("offlineURL") + '/data/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.referalread, params, header);
            offlinePost.success(function (data) {
                callbackSuccess(data);
            })
                .error(function (data) {
                    callbackError(data);
                });

        }
    }

    this.addUpdateSubRef = function (param, callbacksuccess, callbackerror) {
        console.log(JSON.stringify(param));
        if (param.HospitalCD == "" || param.HospitalCD == undefined) {
            delete param.HospitalCD;
        }
        $http.post(hcueServiceUrl.addUpdateSubVisitRefSetting, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

    this.listSubVisitReferral = function (param, callbacksuccess, callbackerror) {
        console.log(JSON.stringify(param));
        if (param.HospitalCD == "" || param.HospitalCD == undefined) {
            delete param.HospitalCD;
        }
        $http.post(hcueServiceUrl.listSubVisitRefSetting, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

}])

hCueDoctorWebServices.service('hCueCommunicationServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {


    this.offerCodeForSMS = "";
    this.settingsRedirectFrom = 1;


    this.setOfferCodeForSMS = function (data) {
        console.log(data)
        this.offerCodeForSMS = data;
    }
    this.setSettingsRedirectFrom = function (tabno) {

        this.settingsRedirectFrom = tabno;
    }

    this.getOfferCodeForSms = function () {
        return this.offerCodeForSMS;
    }


    this.getSettingsRedirectFrom = function () {
        return this.settingsRedirectFrom;
    }






    this.SendCommunicationData = function (params, callbackSuccess, callbackError) {


        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined) {
            var hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            params.HospitalCD = hospcd;
            $http.post(hcueServiceUrl.communicationTab, params, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                });
        } else {
            $http.post(hcueServiceUrl.communicationTab, params, header)
                .success(function (data) {
                    callbackSuccess(data);
                })
                .error(function (data) {
                    callbackError(data);
                });
        }





    }

    this.getPatientServices = function (params, onGetcallSuccess, OnGetcallFail) {
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined) {
            var hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            params.HospitalCD = hospcd;
            $http.post(hcueServiceUrl.getSMSpatients, params, header)
                .success(function (data) {
                    onGetcallSuccess(data);
                })
                .error(function (data) {
                    OnGetcallFail(data);
                });
        } else {
            $http.post(hcueServiceUrl.getSMSpatients, params, header)
                .success(function (data) {
                    onGetcallSuccess(data);
                })
                .error(function (data) {
                    OnGetcallFail(data);
                });
        }
    }

    this.addCommunicationGroup = function (params, onGetcallSuccess, OnGetcallFail) {
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined) {
            var hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            params.HospitalCD = hospcd;
            $http.post(hcueServiceUrl.addCommunicationGroup, params, header)
                .success(function (data) {
                    onGetcallSuccess(data);
                })
                .error(function (data) {
                    OnGetcallFail(data);
                });
        } else {
            $http.post(hcueServiceUrl.addCommunicationGroup, params, header)
                .success(function (data) {
                    onGetcallSuccess(data);
                })
                .error(function (data) {
                    OnGetcallFail(data);
                });
        }
    }

    this.updtCommunicationGroup = function (params, onGetcallSuccess, OnGetcallFail) {
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined) {
            var hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            params.HospitalCD = hospcd;
            $http.post(hcueServiceUrl.updtCommunicationGroup, params, header)
                .success(function (data) {
                    onGetcallSuccess(data);
                })
                .error(function (data) {
                    OnGetcallFail(data);
                });
        } else {
            $http.post(hcueServiceUrl.updtCommunicationGroup, params, header)
                .success(function (data) {
                    onGetcallSuccess(data);
                })
                .error(function (data) {
                    OnGetcallFail(data);
                });
        }
    }

    this.listCommunicationGroup = function (params, onGetcallSuccess, OnGetcallFail) {
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined) {
            var hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            params.HospitalCD = hospcd;
            $http.post(hcueServiceUrl.listCommunicationGroup, params, header)
                .success(function (data) {
                    onGetcallSuccess(data);
                })
                .error(function (data) {
                    OnGetcallFail(data);
                });
        } else {
            $http.post(hcueServiceUrl.listCommunicationGroup, params, header)
                .success(function (data) {
                    onGetcallSuccess(data);
                })
                .error(function (data) {
                    OnGetcallFail(data);
                });
        }
    }


    this.listCommunicationTemplates = function (params, onGetcallSuccess, OnGetcallFail) {
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined) {
            var hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            params.HospitalCD = hospcd;
            $http.post(hcueServiceUrl.listJsonSettings, params, header)
                .success(function (data) {
                    onGetcallSuccess(data);
                })
                .error(function (data) {
                    OnGetcallFail(data);
                });
        } else {
            $http.post(hcueServiceUrl.listJsonSettings, params, header)
                .success(function (data) {
                    onGetcallSuccess(data);
                })
                .error(function (data) {
                    OnGetcallFail(data);
                });
        }
    }

    this.addUpdtCommunicationTemplates = function (params, onGetcallSuccess, OnGetcallFail) {
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode !== undefined) {
            var hospcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            params.HospitalCD = hospcd;
            $http.post(hcueServiceUrl.consultationPause, params, header)
                .success(function (data) {
                    onGetcallSuccess(data);
                })
                .error(function (data) {
                    OnGetcallFail(data);
                });
        } else {
            $http.post(hcueServiceUrl.consultationPause, params, header)
                .success(function (data) {
                    onGetcallSuccess(data);
                })
                .error(function (data) {
                    OnGetcallFail(data);
                });
        }
    }


}])
//ADMIN SERVICES
hCueDoctorWebServices.service('hCueAdminServices', ['$http', 'hcueServiceUrl', 'hcueOfflineServiceUrl', function ($http, hcueServiceUrl, hcueOfflineServiceUrl) {

    this.AddBranch = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addBranch, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.UpdateBranch = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.updateBranch, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.GetRoles = function (hospitalID, callbackSuccess, callbackError) {

        var url = hcueServiceUrl.getRoles + "/" + hospitalID + "/list";

        var rtnData = [];
        $http.get(url, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
        return rtnData;

    }
    this.GetBranch = function (hospitalID, callbackSuccess, callbackError) {

        var url = hcueServiceUrl.getBranchDetails + "/" + hospitalID + "/searchHospital";

        var rtnData = [];
        $http.get(url, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
        return rtnData;

    }

    this.AddUpdateRoles = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addUpdateRoles, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });


    }
    this.AddUser = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.addUser, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.UpdateUser = function (params, callbackSuccess, callbackError) {

        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.updateDoctor, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.updateDoctor, params);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.UpdateUserMoreInfo = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.updateDoctorMoreInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getAddressConsultationAvailable = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getAddressConsultationAvailable, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };


    this.UpdateDoctorTimings = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.updateDoctorTimings, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.AddUpdateDoctorTimings = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.updateConsultation, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };


    this.GetAdminLkup = function (id, callbackSuccess, callbackError) {

        $http.get(hcueServiceUrl.getAdminLkup + id + '/lookup')
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };




    this.SpecialityLkup = function (callbackSuccess, callbackError) {

        //$http.get(hcueServiceUrl.getSpecialities)
        var param = { "ActiveIND": "Y", "SearchText": "" };
        $http.post(hcueServiceUrl.readLookup, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };


    this.getLocation = function (getLocationParams, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getLocationPost, getLocationParams, header)
            .success(function (data) {
                //alert(JSON.stringify(data))
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getCity = function (getCityParams, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getCityPost, getCityParams, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getState = function (getStateParams, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getStatePost, getStateParams, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getCountry = function (getCountryParams, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getCountryPost, getCountryParams, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.GetCityLkup = function (callbackSuccess, callbackError) {


        $http.get(hcueServiceUrl.getcityvalues)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.GetLocationLkup = function (callbackSuccess, callbackError) {

        $http.get(hcueServiceUrl.LocationLookup)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.GetUserList = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getUsers, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.GetSpeciality = function (callbackSuccess, callbackError) {

        //$http.get(hcueServiceUrl.readLookup)
        var param = { "ActiveIND": "Y", "SearchText": "" };
        $http.post(hcueServiceUrl.readLookup, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.SearchUsers = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getUsers, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.SearchBranches = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getUsers, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.listRegistration = function (hospitalid, callbacksuccess, callbackerror) {
        var url = hcueServiceUrl.listRegistration + hospitalid;
        //console.log(url);
        $http.get(url, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };
    this.addPatientRegistration = function (regfiledarray, doctorid, hospitalid, callbacksuccess, callbackerror) {
        var object = {
            "USRType": "PATIENT",
            "Type": "Patient",
            "ScreenType": "DOCTOR",
            "RegSettings": regfiledarray,
            //id=hospitalid
            //userid=doctorid
            "ID": parseInt(hospitalid),
            "USRId": parseInt(doctorid),
            "AppType": "Web"
        };
        console.log(JSON.stringify(object));
        $http.post(hcueServiceUrl.addPatientRegistration, object, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };


    //Get the branch details using doctor id
    this.getBranchinfo = function (params, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.getAddressConsultationAvailable, params, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

    //Get doctor post method
    this.getDoctorPost = function (params, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.getDoctorPost, params, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

    //adminGroup
    this.AdminGroup = function (param, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.adminGroup, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

}]);

/* New admin services*/

//ADMIN SERVICES
hCueDoctorWebServices.service('hCueMainAdminServices', ['$http', 'hcueServiceUrl', 'hcueOfflineServiceUrl', function ($http, hcueServiceUrl, hcueOfflineServiceUrl) {

    this.AddBranch = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addBranch, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.UpdateBranch = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.updateBranch, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.GetRoles = function (hospitalID, callbackSuccess, callbackError) {

        var url = hcueServiceUrl.getRoles + "/" + hospitalID + "/list";

        var rtnData = [];
        $http.get(url, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
        return rtnData;

    }
    this.GetBranch = function (hospitalID, callbackSuccess, callbackError) {

        var url = hcueServiceUrl.getBranchDetails + "/" + hospitalID + "/searchHospital";

        var rtnData = [];
        $http.get(url, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
        return rtnData;

    }

    this.AddUpdateRoles = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addUpdateRoles, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });


    }
    this.AddUser = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.addUser, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.UpdateUser = function (params, callbackSuccess, callbackError) {

        var offlinePost = (JSON.parse(localStorage.getItem("isAppOnline"))) ? $http.post(hcueServiceUrl.updateDoctor, params, header) : $http.post(localStorage.getItem("offlineURL") + '/save/' + doctorinfo.doctorid + '/' + hcueOfflineServiceUrl.updateDoctor, params);
        offlinePost.success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.UpdateUserMoreInfo = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.updateDoctorMoreInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getAddressConsultationAvailable = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getAddressConsultationAvailable, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };


    this.UpdateDoctorTimings = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.updateDoctorTimings, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.AddUpdateDoctorTimings = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.updateConsultation, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };


    this.GetAdminLkup = function (id, callbackSuccess, callbackError) {

        $http.get(hcueServiceUrl.getAdminLkup + id + '/lookup')
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };




    this.SpecialityLkup = function (callbackSuccess, callbackError) {

        //$http.get(hcueServiceUrl.getSpecialities)
        var param = { "ActiveIND": "Y", "SearchText": "" };
        $http.post(hcueServiceUrl.readLookup, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };


    this.getLocation = function (getLocationParams, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getLocationPost, getLocationParams, header)
            .success(function (data) {
                //alert(JSON.stringify(data))
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getCity = function (getCityParams, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getCityPost, getCityParams, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getState = function (getStateParams, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getStatePost, getStateParams, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getCountry = function (getCountryParams, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getCountryPost, getCountryParams, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.GetCityLkup = function (callbackSuccess, callbackError) {


        $http.get(hcueServiceUrl.getcityvalues)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.GetLocationLkup = function (callbackSuccess, callbackError) {

        $http.get(hcueServiceUrl.LocationLookup)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.GetUserList = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getUsers, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getHospitalDetails = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getUsers, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.GetSpeciality = function (callbackSuccess, callbackError) {

        //$http.get(hcueServiceUrl.readLookup)
        var param = { "ActiveIND": "Y", "SearchText": "" };
        $http.post(hcueServiceUrl.readLookup, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.SearchUsers = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getUsers, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.SearchBranches = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getUsers, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.searchHospital = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.searchAdminHospitals, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.listRegistration = function (hospitalid, callbacksuccess, callbackerror) {
        var url = hcueServiceUrl.listRegistration + hospitalid;
        //console.log(url);
        $http.get(url, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

    this.mandatorRegistrationFields = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addPatientRegistration, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.addPatientRegistration = function (regfiledarray, doctorid, hospitalid, callbacksuccess, callbackerror) {
        var object = {
            "USRType": "PATIENT",
            "Type": "Patient",
            "ScreenType": "DOCTOR",
            "RegSettings": regfiledarray,
            //id=hospitalid
            //userid=doctorid
            "ID": parseInt(hospitalid),
            "USRId": parseInt(doctorid),
            "AppType": "Web"
        };
        console.log(JSON.stringify(object));
        $http.post(hcueServiceUrl.addPatientRegistration, object, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };


    //Get the branch details using doctor id
    this.getBranchinfo = function (params, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.getAddressConsultationAvailable, params, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

    //Get doctor post method
    this.getDoctorPost = function (params, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.getDoctorPost, params, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

    //adminGroup
    this.getRolesGroup = function (param, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.adminGroup, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };


    this.getPlaceDetails = function (lat, long, callbacksuccess, callbackerror) {
        $http.post("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + long + "&loaderStatus=false")
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

    this.listHcueAccessRights = function (param, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.listHcueAccessRights, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

    this.getDashboard = function (param, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.getAdminDashboard, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

    //adminGroup
    this.AdminGroup = function (param, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.adminGroup, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

}]);

/* End of new admin services */
//Department
hCueDoctorWebServices.service('hCueDepartmentServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    this.addDepartment = function (param, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.addUpdateDoctorDepartment, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

    this.listDepartment = function (param, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.listDoctorDepartment, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

}]);

hCueDoctorWebServices.service('hCueDashboardReportsServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    this.getHospitalDetails = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getUsers, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };


    this.getDashboard = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.dashboardList, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };


    this.patientInfoList = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getPatientVisitReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }


    this.getAppointmentReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.appointmentReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }


    this.getTotalCashReport = function (params, callbackSuccess, callbackError) {
        //console.log('get cash data = ' + JSON.stringify(params));
        $http.post(hcueServiceUrl.getTotalCashReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getOutstandingReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.topoutstandingReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getTopTreatments = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.toptrendingtreatments, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getTopPatientTreatment = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getpatientTreatment, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getReceptionistBillReport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getreceptionistReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getBranchesTopReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.trendingTopBranches, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }


    this.getDutyDoclist = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getDutyDocReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.addUpdateBranch = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addBranch, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getPayoutReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.payoutReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }


    this.getTotalExpensesReport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.expensesDepositList, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.getTopRatedDoctorsReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.topRatedDoctorReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.getDoctorDiaryReport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.doctorDiaryReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };


    this.getReferralReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.referralReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getInventoryReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listDoctorMedicineSetting, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getCampainReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.campaignReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getPharmaLead = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getPharmaLead, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getTrendingConsultReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.toptrendingConsultReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getDoctorClinicData = function (param, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.getDocClinicData + param + '/lookup', header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }


    this.getSMSListDetails = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getSMSListDetails, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getExpenseReport = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getexpensereport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getTotalCashByPaymentMode = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getTotalCashByPaymentMode, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getFeedbackReportInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getFeedBackReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getCancelBillingReportInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getCancelBillReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getLabOrdersInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getLabOrdersInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getSmsReportinfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getSmsReportinfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
}]);


hCueDoctorWebServices.service('hCueReportCashServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    this.getTotalCashReport = function (params, callbackSuccess, callbackError) {
        //console.log('get cash data = ' + JSON.stringify(params));
        $http.post(hcueServiceUrl.getTotalCashReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getDoctorClinicData = function (param, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.getDocClinicData + param + '/lookup', header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getDataDashboardData = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getDataDashboard, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.patientInfoList = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getPatientVisitReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    var dashboardData;

    this.addDashboardValue = function (data) {
        dashboardData = data;
    }

    this.getDashboardValue = function () {
        return dashboardData;
    }


    var PatientInfoData;

    this.addPatientValue = function (data) {
        PatientInfoData = data;
    }

    this.getPatientValue = function () {
        return PatientInfoData;
    }


    this.getBranchesTopReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.trendingTopBranches, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getDutyDoclist = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getDutyDocReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getTopTreatments = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.toptrendingtreatments, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getOutstandingReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.topoutstandingReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getTrendingConsultReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.toptrendingConsultReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getAppointmentReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.appointmentReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }


    this.getReferralReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.referralReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getPayoutReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.payoutReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getTotalExpensesReport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.expensesDepositList, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.getTopRatedDoctorsReports = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.topRatedDoctorReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.getDoctorDiaryReport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.doctorDiaryReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getDrDiaryConsulationlistReport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.consultationlistReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getDrDiaryConversionlistReport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.conversionlistReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getDrDiaryNonConversionlistReport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.nonconversionlistReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getDrDiaryInvestigationlistReport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.investigationlistReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.getDrDiaryTreatmentlistReport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.treatmentlistReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.doctorIncentiveReport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.doctorIncentiveReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.consolidatedIncentiveReport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.consolidatedIncentiveReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getDoctorIncetivesReportInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getDoctorIncetivesReportInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }



}]);

hCueDoctorWebServices.service('hCueforgotpasswordServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    this.validateEmail = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.forgotPasswordStep1, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    // this.sendOTPSMS = function(params, callbackSuccess, callbackError) {
    //     var SMSheaders = { headers: { "Content-Type": "application/json", "Accept-Encoding": "gzip", "Authorization": "47F7D969-B6F9-4860-8EE4-085F1BF3CCE7" } };

    //     $http.post(hcueServiceUrl.forgotPasswordCheckMobi, params, SMSheaders)
    //         .success(function(data) {
    //             callbackSuccess(data);
    //         })
    //         .error(function(data) {
    //             callbackError(data);
    //         });
    // };

    // this.sendOTPEmail = function(params, callbackSuccess, callbackError) {
    //     $http.post(hcueServiceUrl.forgotSendEmailOTP, params, header)
    //         .success(function(data) {
    //             callbackSuccess(data);
    //         })
    //         .error(function(data) {
    //             callbackError(data);
    //         });
    // };

    // this.updatePinPass = function(params, callbackSuccess, callbackError) {
    //     $http.post(hcueServiceUrl.forgotPasswordStep2, params, header)
    //         .success(function(data) {
    //             callbackSuccess(data);
    //         })
    //         .error(function(data) {
    //             callbackError(data);
    //         });
    // };

    // this.validatePinPass = function(params, callbackSuccess, callbackError) {
    //     $http.post(hcueServiceUrl.forgotPasswordValidate, params, header)
    //         .success(function(data) {
    //             callbackSuccess(data);
    //         })
    //         .error(function(data) {
    //             callbackError(data);
    //         });
    // };

    this.updatePassword = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.forgotUpdatePassword, params, header)
            .success(function (data) {
                callbackSuccess(data);

            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);


hCueDoctorWebServices.service('hCueSmsService', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    this.saveSmsSetting = function (param, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.consultationPause, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

    this.listSmsSetting = function (param, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.listJsonSettings, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };



}]);

/***Master Template***/
hCueDoctorWebServices.service('hCueMasterTemplateServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.addMasterTemplate = function (consultationinfo, caseTreatmentMaster, jsonId, disgnosisnotes, doctornotes, templateName, templateDesc, callbackSuccess, callbackError) {
        console.log(JSON.stringify(consultationinfo));
        var doctorid = doctorinfo.doctorid;
        var hospcode = "";
        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined &&
            doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode !== undefined) {

            hospcode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        }

        var request = {
            Diagnostic: consultationinfo.treatmentCollection.Diagnostic,
            Investigation: consultationinfo.treatmentCollection.Investigation,
            DoctorNotes: doctornotes,
            SymptomNotes: disgnosisnotes,
        }

        var allrequest = {
            "JsonSettingID": jsonId,
            "DoctorID": doctorid,
            "HospitalCD": hospcode,
            "TemplateName": templateName,
            'TemplateNotes': templateDesc,
            "ActiveIND": "Y",
            "CrtUSR": doctorid,
            "CrtUSRType": "DOCTOR",
            "IsDefault": "Y",
            "PageType": "TRTMNT",
            JsonSetting: {
                USRType: "DOCTOR",
                USRId: doctorid,
                PatientCase: request,
                CaseTreatment: caseTreatmentMaster,
                MedicalCasePrescription: { PharmaWrkFlowStatusID: 'FRDP', PharmaID: null, addUpdateRecord: [] },
                LabCasePrescription: [],
            }

        };

        if (hospcode == "") {
            delete allrequest.HospitalCD;
        }
        if (jsonId == 0) {
            delete allrequest.JsonSettingID;
        }
        if (consultationinfo.labtests.length > 0) {

            allrequest.JsonSetting.LabCasePrescription = consultationinfo.labtests;

        }

        if (consultationinfo.prescription.medicines.length > 0) {
            allrequest.JsonSetting.MedicalCasePrescription.PharmaID = (consultationinfo.prescription.pharmaid > 0) ? consultationinfo.prescription.pharmaid : null;
            allrequest.JsonSetting.MedicalCasePrescription.addUpdateRecord = consultationinfo.prescription.medicines;
        }

        $http.post(hcueServiceUrl.consultationPause, allrequest, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.listTemplates = function (appstatus, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listJsonSettings, appstatus, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }


}]);
/***End of Master Template***/
/***lablist***/
hCueDoctorWebServices.service('hCueLabServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.listLab = function (param, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listLabTest, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }
}]);
//SetDefaultPharmacy
hCueDoctorWebServices.service('hcueSetDefaultPharmacy', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    this.getDoctorClinicData = function (param, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.getDocClinicData + param + '/lookup', header)
            .success(function (data) {
                callbackSuccess(data);
                console.log(JSON.stringify(data));
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getListDefaultPharma = function (param, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listDefaultPharmacy, param, header)
            .success(function (data) {
                callbackSuccess(data);
                console.log(JSON.stringify(data));
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.addDefaultPharmSet = function (param, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addDefaultPharmacy, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
}]);
/***End lablist***/
/* Expenses & Deposit services */

hCueDoctorWebServices.service('hCueExpenseDepositServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    this.addUpdateExpenseDeposit = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addupdateExpenseDeposit, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.listingDepositsexpenses = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.expensesDepositList, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getExpDepCategoryList = function (id, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.getExpDepCategoryList + '/' + id + '/lookup')
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getSummaryExpenseIncome = function (params, callbacksuccess, callbackerror) {

        $http.post(hcueServiceUrl.getSummaryExpenseIncome, params, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    }

    this.getDataDashboardData = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getDataDashboard, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getSmsDashboard = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.smsDashboard, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getTotalCashReport = function (params, callbackSuccess, callbackError) {
        //console.log('get cash data = ' + JSON.stringify(params));
        $http.post(hcueServiceUrl.getTotalCashReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.getSMSListDetails = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.getSMSListDetails, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getDoctorClinicData = function (param, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.getDocClinicData + param + '/lookup', header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }


    this.getLocation = function (getLocationParams, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getLocationPost, getLocationParams, header)
            .success(function (data) {
                //alert(JSON.stringify(data))
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };



    this.getBranchDetails = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getUsers, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.addupdatePettyCash = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addupdatePettyCash, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.getPettyCashList = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getPettyCashList, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
}]);

/* End of expenses & Deposit services */

/* Incentive services */

hCueDoctorWebServices.service('hCueIncentiveServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    this.incentiveLookup = function (params, callbackSuccess, callbackerror) {
        $http.post(hcueServiceUrl.incentiveLookup, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.addUpdateIncentive = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addUpdateIncentive, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.listIncentive = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listIncentive, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getreadlookup = function (callbackSuccess, callbackError) {
        //$http.get(hcueServiceUrl.readLookup, header)
        var param = { "ActiveIND": "Y", "SearchText": "" };
        $http.post(hcueServiceUrl.readLookup, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.getDoctorClinicData = function (param, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.getDocClinicData + param + '/lookup', header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }


    this.incentiveHospitalLookup = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.incentiveHospitalLookup, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
}]);

/* End of Incentive services */

/*IoT Services*/
hCueDoctorWebServices.service('hcueExternalSolutions', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    this.listDentalImages = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listDentalImages, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.deleteDentalImages = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.deleteDentalImages, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.uploadDentalImages = function (params, callbackSuccess, callbackError) {
        $('#loaderDiv').show()
        var request = new XMLHttpRequest();
        request.open("POST", hcueServiceUrl.uploadDentalImages);
        request.send(params);
        request.onload = function () {
            if (request.status == 200) {
                callbackSuccess(request.responseText);
                $('#loaderDiv').hide()
            } else {
                callbackError(request.responseText);
                $('#loaderDiv').hide()
            }
        }
        request.onerror = function () {
            callbackError(request.responseText);
            $('#loaderDiv').hide()
        }

    };
    this.uploadOldDentalImages = function (params, callbackSuccess, callbackError) {
        //$('#loaderDiv').show()
        var request = new XMLHttpRequest();
        request.open("POST", hcueServiceUrl.uploadDentalImages);
        request.send(params);
        request.onload = function () {
            if (request.status == 200) {
                callbackSuccess(request.responseText);
                //$('#loaderDiv').hide()
            } else {
                callbackError(request.responseText);
                //$('#loaderDiv').hide()
            }
        }
        request.onerror = function () {
            callbackError(request.responseText);
            //$('#loaderDiv').hide()
        }

    };
    this.uploadNewDentalImages = function (params, rowid, SubType, callbackSuccess, callbackError) {
        //$('#loaderDiv').show()
        var request = new XMLHttpRequest();
        request.open("POST", hcueServiceUrl.uploadDentalImages);
        request.send(params);
        request.onload = function () {
            if (request.status == 200) {
                callbackSuccess(request.responseText, rowid, SubType);
                //$('#loaderDiv').hide()
            } else {
                callbackError(request.responseText, rowid, SubType);
                //$('#loaderDiv').hide()
            }
        }
        request.onerror = function () {
            callbackError(request.responseText, rowid);
            //$('#loaderDiv').hide()
        }

    };

    this.postTallySoftware = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.postTallySoftware, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
    this.stopDentalSoftware = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.stopDentalSoftware, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.startDentalSoftware = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.startDentalSoftware, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }


}]);


hCueDoctorWebServices.service('hcuePastVitalService', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    var doctorid = doctorinfo.doctorid;
    this.getPatientPastVitalsByPatientId = function (patientid, callbackSuccess, callbackError) {
        console.log(patientid);
        //return $http.get(hcueServiceUrl.readPatientActive + '/' + patientid + '/' + 'getPatientVitals');

        var url = hcueServiceUrl.readPatientActive + '/' + patientid + '/' + 'getPatientVitals';


        $http.get(url, header)
            .success(function (data) {

                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    };
}]);

/*End of IoT Services*/

//blog


hCueDoctorWebServices.service('hcueBlogService', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {


    this.getListingBlog = function (param, blogListSuccess, blogListError) {

        $http.post(hcueServiceUrl.listingBlog, param, header)
            .success(function (data) {
                blogListSuccess(data);
                console.log(JSON.stringify(data));
            })
            .error(function (data) {
                blogListError(data);
            });
    }




    this.blogAddUpdate = function (param, blogSuccess, blogError) {

        $http.post(hcueServiceUrl.addupdateBlog, param, header)
            .success(function (data) {
                blogSuccess(data);

            })
            .error(function (data) {
                blogError(data);
            });
    }
    this.blogImageUpload = function (param, blogSuccess, blogError) {
        var auth = "aGN1ZWFkbWluOlRoM0IzbGxUb2xsc0ZvclRoMzM=";
        // var imgHeader={ headers: { "Content-Type": undefined, "Authorization": 'Basic '+auth } };
        // $http.defaults.headers.common["Content-Disposition"]='attachment;filename=sasi.png';
        // $http.defaults.headers.common["Access-Control-Allow-Origin"]="*";
        // $http.post(hcueServiceUrl.uploadImageBlog,param,imgHeader)
        //  .success(function (data) {
        //     blogSuccess(data);

        // })
        // .error(function (data) {
        //     blogError(data);
        // });
        $.ajax({
            url: hcueServiceUrl.uploadImageBlog,
            method: 'POST',
            data: param.data,
            crossDomain: true,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                $('#loaderDiv').show()
                xhr.setRequestHeader('Authorization', 'Basic ' + auth);
            },
            success: function (data) {
                $('#loaderDiv').hide()
                blogSuccess(data);
            },
            error: function (error) {
                $('#loaderDiv').hide()
                blogError(error);
            }
        });

    }
}]);
/*End of Blog Services*/

/*MIS Report Services - Starts*/
hCueDoctorWebServices.service('hcueMISReportService', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    this.addReportRequest = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.addMISReportRequest, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };

    this.listMISReports = function (params, callbackSuccess, callbackError) {

        $http.post(hcueServiceUrl.listMISReports, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    };
}]);
/*MIS Report Services - Ends*/
/*Ask a public Question Change Starts- Goutham Sai*/
hCueDoctorWebServices.service('hcueAskaPublicQuestion', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.listingAnsweredQuestions = function (params, callbackSuccess, callbackerror) {
        $http.post(hcueServiceUrl.listingAskaquestion, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.addingAnswer = function (params, callbackSuccess, callbackerror) {
        $http.post(hcueServiceUrl.addAnswerService, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
}]);
/*Ask a public Question Change Ends- Goutham Sai*/



/*---- C7 Services ----*/
/*- referrel page -*/
hCueDoctorWebServices.service('referralregion', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.addRegion = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addRegion, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.updateRegion = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.updateRegion, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getRegionDetails = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getRegionDetails, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.listRegion = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listRegion, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.alllistRegion = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listAllRegionInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);
hCueDoctorWebServices.service('referralccg', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.addccg = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addccg, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.updateccg = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.updateccg, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getccgDetails = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getccgDetails, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.listccg = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listccg, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.alllistccg = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listAllCcgroupsInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
}]);
hCueDoctorWebServices.service('referralpractice', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.addpractice = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addPractice, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.updatepractice = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.updatePractice, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getpracticeDetails = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getPracticeDetails, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.listpractice = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listPractice, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.listAllPracticeInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listAllPracticeInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.downloadPracticeData = function (param, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.downloadPractice, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
}]);

hCueDoctorWebServices.service('referraldoctor', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.newlistdoctor = function (param, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.newlistdoctor, param, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getdoctorDetails = function (id, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.editdoctor + '/' + id + '/getDoctor')
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.newadddoctor = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.newadddoctor, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.newupdateDoctor = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.newupdateDoctor, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.deactivedoctorPractice = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.deactivedoctorPractice, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
}]);


hCueDoctorWebServices.service('c7AddUpdatePatient', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.addPatient = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addNewPatient, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.updatePatient = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.updateNewPatient, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.updatePatientEnquiry = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.updatePatientEnquiry, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getPatientDetails = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getPatientDetails, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getDocList = function (params, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.getDocList + params, header).success(function (data) {
            callbackSuccess(data);
        })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.uploadPatientDetails = function (params, callbackSuccess, callbackError) {
        $http({
            url: hcueServiceUrl.uploadPatientDetails,
            method: 'POST',
            data: params,
            headers: {
                "Content-Type": undefined
            }

        })
            // $http.post(, , fileheader)

            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.uploadLetterDocument = function (params, callbackSuccess, callbackError) {
        $http({
            url: hcueServiceUrl.uploadLetterDocument,
            method: 'POST',
            data: params,
            headers: {
                "Content-Type": undefined
            }

        })
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.logoupload = function (params, callbackSuccess, callbackError) {
        $http({
            url: hcueServiceUrl.logoupload,
            method: 'POST',
            data: params,
            headers: {
                "Content-Type": undefined
            }

        })
            // $http.post(, , fileheader)

            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }


}]);

hCueDoctorWebServices.service('c7AdminServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    //Dashboard start
    this.listDashBoardInfo = function (param, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.listDashBoardInfo, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };
    //Dashboard end

    //Location start
    this.getHospitals = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getHospitals, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.searchorganisation = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.searchorganisation, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.addorganisation = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addorganisation, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getOrganisation = function (params, callbackSuccess, callbackError) {

        $http.get(hcueServiceUrl.getOrganisation + params, header)
            .success(function (data) {
                callbackSuccess(data);
                console.log(data)
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.updateOrganisation = function (params, callbackSuccess, callbackError) {
        $http.put(hcueServiceUrl.updateOrganisation, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.listallorganisation = function (params, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.listallorganisation + '/' + params.pagenumber + '/' + params.pagesize, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.addUpdateHospital = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addUpdateHospital, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.searchBranch = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.searchBranch, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    //Location end

    //Profile setting start

    this.listHcueAccessRights = function (param, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.listHcueAccessRights, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

    this.listHospitalRole = function (param, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.listHospitalRole, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

    this.listProfile = function (hospitalID, callbackSuccess, callbackError) {
        var url = hcueServiceUrl.listProfile + "/" + hospitalID + "/list";

        var rtnData = [];
        $http.get(url, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
        return rtnData;

    }

    this.addUpdateProfile = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addUpdateProfile, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });

    }
    //Profile setting end

    //user start
    this.getDoctor = function (params, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.getDoctor, params, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };
    //user end

    //speciality start
    this.addSpecialitylkup = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addSpecialitylkup, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.Specialitycategory = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.Specialitycategory, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.updateSpecialitylkup = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.updateSpecialitylkup, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.SpecialityList = function (ID, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.SpecialityList + '/true' + '/' + ID)

            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.SpecialityListNew = function (ID, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.SpecialityListNew + '/true' + '/' + ID)

            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    //speciality end
    this.CategoryList = function (ID, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.CategoryList + '/' + ID)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    //Scheduling Start
    this.listAllScheduleInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listAllScheduleInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.hospitalscheduleadd = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.hospitalscheduleadd, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.hospitalscheduleupdate = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.hospitalscheduleupdate, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.hospitalScheduleDelete = function (params, callbackSuccess, callbackError) {
        $http.put(hcueServiceUrl.hospitalScheduleDelete, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    //Scheduling end

    //Calendar Start

    this.listAllCalendarInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listAllCalendarInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.listdoctorhospital = function (param, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.listdoctorhospital + param)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    //Calendar end
    // Delete doctor's Location by schedule validation
    this.deactiveDoctor = function (param, callbacksuccess, callbackerror) {
        $http.post(hcueServiceUrl.deactiveDoctor, param, header)
            .success(function (data) {
                callbacksuccess(data);
            })
            .error(function (data) {
                callbackerror(data);
            });
    };

}]);

hCueDoctorWebServices.service('c7EnquiriesServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.listAllEnquiry = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listAllEnquiry, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getNearHospitalList = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getNearHospitalList, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.listEnquiryStatus = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listEnquiryStatus, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.listManageStatus = function (callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.listManageStatus, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.listAvailableAppointment = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listAvailableAppointment, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.addupdateHospitalAppointments = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addupdateHospitalAppointments, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.listAppointmentStatus = function (params, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.listAppointmentStatus + params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.enquiryaddstatus = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.enquiryaddstatus, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getAppointmentDtl = function (params, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.getAppointmentDtl + params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.sendAppointmentSMS = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.sendAppointmentSMS, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.sendAppointmentEmail = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.sendAppointmentEmail, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.sendPatientDeclinedEmail = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.sendPatientDeclinedEmail, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.sendPatientDNAMail = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.sendPatientDNAMail, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.sendExclusionEmail = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.sendExclusionEmail, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.sendContactUsEmail = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.sendContactUsEmail, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.sendNoContactToGP = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.sendNoContactToGP, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.sendUrgentNoContactToGP = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.sendUrgentNoContactToGP, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.sendPatientWithdrawalMail = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.sendPatientWithdrawalMail, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.listrejectPatients = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listrejectPatients, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getCalendarlistviewInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getCalendarlistviewInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.listAllReferral = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listAllReferral, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.Referralcount = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.Referralcount, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.sendAppointmentdetailsMail = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.sendAppointmentdetailsMail, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.sendSupportworkerinfoEmail = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.sendSupportworkerinfoEmail, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.sendSonographerinfoEmail = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.sendSonographerinfoEmail, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }



}]);
hCueDoctorWebServices.service('c7AppointmentListServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.listHospitalAppointments = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listHospitalAppointments, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getDoctorAppointments = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getDoctorAvailableAppointments, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            })
    }

    this.AddApointmentHospcode = function (hospcode, branchcd, hospid, parenthospid, starttime, endtime, doctorid, userid, AddressConsultID, daycd, consultdate, patientid, addressid, eqid, isEmergencySlot, callbackSuccess, callbackError) {

        var params = null;

        if (isEmergencySlot == true) {

            params = {
                AppointmentStatus: "B",
                StartTime: "00:00",
                EndTime: "00:00",
                EmergencyEndTime: endtime,
                EmergencyStartTime: starttime,
                DoctorID: doctorid,
                AddressConsultID: AddressConsultID,
                DayCD: daycd,
                ScheduleDate: consultdate,
                PatientID: patientid,
                USRId: userid,
                USRType: "ADMIN",
                AddressID: addressid,
                Patientenquiryid: eqid,
                HospitalDetails: {
                    HospitalID: hospid,
                    HospitalCD: hospcode,
                    BranchCD: branchcd,
                    ParentHospitalID: parenthospid
                },
                HospitalCD: hospcode,
                BranchCD: "WWNE"
            };

        } else {

            params = {
                AppointmentStatus: "B",
                StartTime: starttime,
                EndTime: endtime,
                EmergencyEndTime: "00:00",
                EmergencyStartTime: "00:00",
                DoctorID: doctorid,
                AddressConsultID: AddressConsultID,
                DayCD: daycd,
                ScheduleDate: consultdate,
                PatientID: patientid,
                USRId: userid,
                USRType: "ADMIN",
                AddressID: addressid,
                Patientenquiryid: eqid,
                HospitalDetails: {
                    HospitalID: hospid,
                    HospitalCD: hospcode,
                    BranchCD: branchcd,
                    ParentHospitalID: parenthospid
                },
                HospitalCD: hospcode,
                BranchCD: "WWNE"
            };

        }

        $http.post(hcueServiceUrl.bookc7appointment, params, header)
            .success(function (data) {
                callbackSuccess(data);

            })
            .error(function (data) {
                callbackError(data);
            })
    }


}]);

hCueDoctorWebServices.service('c7PostCodeServices', ['$http', 'hcueServiceUrl', 'alertify', function ($http, hcueServiceUrl, alertify) {
    this.listpostcodeinfo = function (id, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.listpostcodeinfo + '/' + id)
            .success(function (data) {
                if (!data.error) {
                    data.addresses = data.addresses.sort(alphanumericsort);
                    callbackSuccess(data);
                } else {
                    if (data.error.message.includes('429')) {
                        alertify.error('Post code limit exceeded');
                    } else {
                        alertify.error('Post code error');
                    }
                }
            })
            .error(function (data) {
                callbackError(data);
            });
    }
}]);

hCueDoctorWebServices.service('c7Hl7ToMeshServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.publishHl7ToMesh = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.publishHl7ToMesh, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.sendAttachmentMail = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.sendAttachmentMail, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    // this.getHL7List = function(params, callbackSuccess, callbackError) {
    //     $http.get(hcueServiceUrl.getHL7List + params, header)
    //         .success(function(data) {
    //             callbackSuccess(data);
    //         })
    //         .error(function(data) {
    //             callbackError(data);
    //         });
    // }
    this.getHL7List = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getHL7List, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.triggerHL7MessageConnection = function (params, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.triggerHL7MessageConnection + params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }


}]);

hCueDoctorWebServices.service('c7DashboardServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getEnquiryDashboardInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getEnquiryDashboardInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getAgentstatsreporttotalInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getAgentstatsreporttotalInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getAgentstatsreportInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getAgentstatsreportInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getCustombookingreportInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getCustombookingreportInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getCustomadminreportInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getCustomadminreportInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    // this.getGeneralReport = function(params, callbackSuccess, callbackError) {
    //     $http.post(hcueServiceUrl.getGeneralReport, params, header)
    //         .success(function(data) {
    //             callbackSuccess(data);
    //         })
    //         .error(function(data) {
    //             callbackError(data);
    //         });
    // }
    this.getGeneralreportfile = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getGeneralreportfile, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.GeneralStatsReportFileList = function (params, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.GeneralStatsReportFileList + "/" + params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }


}]);

hCueDoctorWebServices.service('c7DailyReportServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getDailyReportInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getDailyReportInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);

hCueDoctorWebServices.service('c7DailySpecialityReportServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getDailySpecialityReportInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getDailySpecialityReportInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);

hCueDoctorWebServices.service('c7CostReportServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getCostReportInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getCostReportInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);

hCueDoctorWebServices.service('C7getForecastReportService', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getForecastReportInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getForecastReportInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);

hCueDoctorWebServices.service('c7ReferralReportServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getReferralReportInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getReferralReportInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);

hCueDoctorWebServices.service('c7AppointmentReportServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getAppointmentReport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getAppointmentReport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);

hCueDoctorWebServices.service('c7ClinicAuditReportServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getClinicauditcriteriareport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getClinicauditcriteriareport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);

hCueDoctorWebServices.service('c7PatientinReportServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getPatientinSystemreport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getPatientinSystemreport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getPatientinSystemreportnew = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getPatientinSystemreportnew, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.getPatientinSystemreporttotal = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getPatientinSystemreporttotal, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);

hCueDoctorWebServices.service('c7CostanalyticsReportServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getCostanalyticsreport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getCostanalyticsreport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);

hCueDoctorWebServices.service('c7CostReportWithRegionServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getCostReportwithregionInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getCostReportwithregionInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);


hCueDoctorWebServices.service('c7ExceptionNotesReportService', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getExceptionnotesreportInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getExceptionnotesreport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);


hCueDoctorWebServices.service('c7ForecastReportWithRegionServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getCostReportwithregionInfo = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getCostReportwithregionInfo, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
}]);

hCueDoctorWebServices.service('c7AppointmentindicationReportServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getAppointmentindicationreport = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getAppointmentindicationreport, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);

hCueDoctorWebServices.service('c7ReferralWOindicationServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.getReferralwithoutindication = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getReferralwithoutindication, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);

hCueDoctorWebServices.service('c7SearchServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {
    this.searchPatients = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.searchPatients, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }
    this.searchPatientsget = function (params, callbackSuccess, callbackError) {
        $http.get(hcueServiceUrl.searchPatientsget + params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.listPatientAllEnquiry = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.listPatientAllEnquiry, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);

hCueDoctorWebServices.service('c7ReminderServices', ['$http', 'hcueServiceUrl', function ($http, hcueServiceUrl) {

    this.addReminder = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.addReminder, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

    this.getReminderList = function (params, callbackSuccess, callbackError) {
        $http.post(hcueServiceUrl.getReminderList, params, header)
            .success(function (data) {
                callbackSuccess(data);
            })
            .error(function (data) {
                callbackError(data);
            });
    }

}]);

(function () {
    // Regular expression to separate the digit string from the non-digit strings.
    var reParts = /\d+|\D+/g;

    // Regular expression to test if the string has a digit.
    var reDigit = /\d/;

    // Add cmpStringsWithNumbers to the global namespace.  This function takes to
    // strings and compares them, returning -1 if `a` comes before `b`, 0 if `a`
    // and `b` are equal, and 1 if `a` comes after `b`.
    alphanumericsort = function (a, b) {
        // Get rid of casing issues.
        a = a.toUpperCase();
        b = b.toUpperCase();

        // Separates the strings into substrings that have only digits and those
        // that have no digits.
        var aParts = a.match(reParts);
        var bParts = b.match(reParts);

        // Used to determine if aPart and bPart are digits.
        var isDigitPart;

        // If `a` and `b` are strings with substring parts that match...
        if (aParts && bParts &&
            (isDigitPart = reDigit.test(aParts[0])) == reDigit.test(bParts[0])) {
            // Loop through each substring part to compare the overall strings.
            var len = Math.min(aParts.length, bParts.length);
            for (var i = 0; i < len; i++) {
                var aPart = aParts[i];
                var bPart = bParts[i];

                // If comparing digits, convert them to numbers (assuming base 10).
                if (isDigitPart) {
                    aPart = parseInt(aPart, 10);
                    bPart = parseInt(bPart, 10);
                }

                // If the substrings aren't equal, return either -1 or 1.
                if (aPart != bPart) {
                    return aPart < bPart ? -1 : 1;
                }

                // Toggle the value of isDigitPart since the parts will alternate.
                isDigitPart = !isDigitPart;
            }
        }

        // Use normal comparison.
        return (a >= b) - (a <= b);
    };
})();