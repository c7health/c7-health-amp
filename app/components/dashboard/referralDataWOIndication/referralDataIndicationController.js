hCueDoctorWebControllers.controller('referralDataIndicationController', function ($scope, $rootScope, $http, alertify, $window, $location, hcueLoginStatusService, hcueDoctorLoginService, datacookieFactory, c7ReferralWOindicationServices) {
    // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    setTimeout(function () {
        $('[data-toggle="tooltip"]').tooltip();
    }, 500);

    $scope.referralData = [];
    $scope.hospitalID = 0;

    $scope.getReportData = function () {
        var parm = {
            HospitalCode: (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != null &&
                doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != undefined) ?
                doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode : '',
            HospitalID: parseInt($scope.hospitalID),
            fromdate: $scope.refdaterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.refdaterange.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
            todate: $scope.refdaterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.refdaterange.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
        };

        c7ReferralWOindicationServices.getReferralwithoutindication(parm, ReferralIndicationDataSuccess, ReferralIndicationDataError);
    };

    function ReferralIndicationDataSuccess(data) {
        $scope.referralData = data;
        if ($scope.referralData.length > 0) {
            $scope.exportTOExcel();
        }
    }

    function ReferralIndicationDataError(data) { }

    $scope.exportTOExcel = function () {
        var exportData = [{
            TreatmentCycle: 'Treatment Cycle',
            ReferringDoctorsName: 'Referring Doctors Name',
            ReferingPractice: 'Refering Practice',
            PracticeCode: 'Practice Code',
            ReferralCreated: 'Referral Created',
            ReferralReceived: 'Referral Received',
            ReferradDate: 'Referrad Date',
            ReferralRejected: 'Referral Rejected',
            ReferralRejectedReason: 'Referral Rejected Reason',
            // Priority: 'Priority',
            NHSNumber: 'NHS Number',
            DWID: 'DW ID',
            PatientTitle: 'Patient Title',
            PatientForename: 'Patient Forename',
            PatientSurname: 'Patient Surname',
            Address1: 'Address1',
            Address2: 'Address2',
            Address3: 'Address3',
            Town: 'Town',
            County: 'County',
            Country: 'Country',
            Postcode: 'Postcode',
            CCG: 'CCG',
            AppointmentID: 'Appointment ID',
            SubSpeciality: 'Sub Speciality',
            Region: 'Region',
            DateBooked: 'Date Booked',
            AppointmentLetterID: 'Appointment Letter ID',
            LetterTemplate: 'Letter Template',
            LetterPrinted: 'Letter Printed',
            AppointmentStartTime: 'Appointment Start Time',
            AppointmentEndTime: 'Appointment End Time',
            AppointmentStatus: 'Appointment Status',
            CancellationReason: 'Cancellation Reason',
            // AppointmentDNA: 'Appointment DNA',
            DNAReason: 'DNA Reason',
            ReportID: 'Report ID',
            ReportName: 'Report Name',
            ReportPublished: 'Report Published',
            TriageCompleteDate: 'Triage Complete Date'
        }];

        for (var i = 0; i < $scope.referralData.length; i++) {
            var temp = {
                TreatmentCycle: $scope.referralData[i].TreatmentCycle,
                ReferringDoctorsName: $scope.referralData[i].DoctorName,
                ReferingPractice: $scope.referralData[i].PracticeName,
                PracticeCode: $scope.referralData[i].PracticeCode,
                ReferralCreated: ($scope.referralData[i].referalcreated != null && $scope.referralData[i].referalcreated != '') ?
                    moment($scope.referralData[i].referalcreated).format('DD-MM-YYYY') : '',
                ReferralReceived: ($scope.referralData[i].referalreceived != null && $scope.referralData[i].referalreceived != '') ?
                    moment($scope.referralData[i].referalreceived).format('DD-MM-YYYY') : '',
                ReferradDate: ($scope.referralData[i].referalprocessed != null && $scope.referralData[i].referalprocessed != '') ?
                    moment($scope.referralData[i].referalprocessed).format('DD-MM-YYYY') : '',
                ReferralRejected: $scope.referralData[i].referralrejected,
                ReferralRejectedReason: $scope.referralData[i].referralrejectedreason,
                // Priority: $scope.referralData[i].Priority,
                NHSNumber: $scope.referralData[i].nhsnumber,
                DWID: $scope.referralData[i].DWID,
                PatientTitle: $scope.referralData[i].Title,
                PatientForename: $scope.referralData[i].Forename,
                PatientSurname: $scope.referralData[i].Surname,
                Address1: $scope.referralData[i].Address1,
                Address2: $scope.referralData[i].Address2,
                Address3: $scope.referralData[i].Address3,
                Town: ($scope.referralData[i].CityTown == null) ? '' : $scope.referralData[i].CityTown,
                County: ($scope.referralData[i].county == null) ? '' : $scope.referralData[i].county,
                Country: ($scope.referralData[i].Country == null) ? '' : $scope.referralData[i].Country,
                Postcode: ($scope.referralData[i].Postcode == null) ? '' : $scope.referralData[i].Postcode,
                CCG: ($scope.referralData[i].ccgname == null) ? '' : $scope.referralData[i].ccgname,
                AppointmentID: ($scope.referralData[i].AppointmentID == null) ? '' : $scope.referralData[i].AppointmentID,
                SubSpeciality: ($scope.referralData[i].subspeciality == null) ? '' : $scope.referralData[i].subspeciality,
                Region: ($scope.referralData[i].regionname == null) ? '' : $scope.referralData[i].regionname,
                DateBooked: ($scope.referralData[i].DateBooked != null && $scope.referralData[i].DateBooked != '') ?
                    moment($scope.referralData[i].DateBooked).format('DD-MM-YYYY') : '',
                AppointmentLetterID: ($scope.referralData[i].AppointmentLetterID == null) ? '' : $scope.referralData[i].AppointmentLetterID,
                LetterTemplate: ($scope.referralData[i].LetterTemplate == null) ? '' : $scope.referralData[i].LetterTemplate,
                LetterPrinted: ($scope.referralData[i].LetterPrinted == null) ? '' : $scope.referralData[i].LetterPrinted,
                AppointmentStartTime: ($scope.referralData[i].StartTime == null) ? '' : $scope.referralData[i].StartTime,
                AppointmentEndTime: ($scope.referralData[i].EndTime == null) ? '' : $scope.referralData[i].EndTime,
                AppointmentStatus: ($scope.referralData[i].AppointmentStatus == null) ? '' : $scope.referralData[i].AppointmentStatus,
                CancellationReason: ($scope.referralData[i].cancellationreason == null) ? '' : $scope.referralData[i].cancellationreason,
                // AppointmentDNA: ($scope.referralData[i].AppointmentDNA == null) ? '' : $scope.referralData[i].AppointmentDNA,
                DNAReason: ($scope.referralData[i].dnareason == null) ? '' : $scope.referralData[i].dnareason,
                ReportID: ($scope.referralData[i].ReportID == null) ? '' : $scope.referralData[i].ReportID,
                ReportName: ($scope.referralData[i].ReportName == null) ? '' : $scope.referralData[i].ReportName,
                ReportPublished: ($scope.referralData[i].ReportPublished == null) ? '' : $scope.referralData[i].ReportPublished,
                TriageCompleteDate: ($scope.referralData[i].TriageCompleteDate != null && $scope.referralData[i].TriageCompleteDate != '') ?
                    moment($scope.referralData[i].TriageCompleteDate).format('DD-MM-YYYY') : ''
            };
            exportData.push(temp);
        }
        var xmls = fetchXML([exportData]);

        var ctx = {
            created: new Date().getTime(),
            worksheets: xmls
        };

        var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML =
                '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
                '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
                '<Styles>' +
                '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
                '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
                '</Styles>' +
                '{worksheets}</Workbook>',
            tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
            tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
            base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                });
            };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'ReferralDataWOIndication' + new Date().getTime() + '.xls';
        //'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    function fetchXML(stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
            var worksheetName = 'Data';
            //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
            wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
            // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
            var rowXML = '';
            for (j = 0; j < stores[i].length; j++) {
                rowXML += '<Row>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].TreatmentCycle + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferringDoctorsName + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferingPractice + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PracticeCode + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralCreated + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralReceived + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferradDate + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralRejected + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralRejectedReason + '</Data></Cell>';
                // rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Priority + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].NHSNumber + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].DWID + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PatientTitle + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PatientForename + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PatientSurname + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Address1 + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Address2 + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Address3 + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Town + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].County + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Country + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Postcode + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].CCG + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentID + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].SubSpeciality + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Region + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].DateBooked + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentLetterID + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].LetterTemplate + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].LetterPrinted + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentStartTime + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentEndTime + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentStatus + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].CancellationReason + '</Data></Cell>';
                // rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentDNA + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].DNAReason + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReportID + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReportName + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReportPublished + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].TriageCompleteDate + '</Data></Cell>';
                rowXML += '</Row>';
            }
            wrkbookXML += rowXML;
            wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
    }
});