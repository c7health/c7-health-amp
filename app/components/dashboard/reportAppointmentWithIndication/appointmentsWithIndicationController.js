hCueDoctorWebControllers.controller('appointmentsWithIndicationController', function($scope,$rootScope,$http,alertify,$window,$location,hcueLoginStatusService,hcueDoctorLoginService,referralregion,datacookieFactory,c7AppointmentindicationReportServices) 
{
  // Check login Params
  var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

  userLoginDetails = datacookieFactory.getItem('loginData');
  if (userLoginDetails.loggedinStatus === 'true') {
    $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
  } else {
    $location.path('/login');
  }
  var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

  if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
    parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
    hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
    newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
  }
  setTimeout(function() {
    $('[data-toggle="tooltip"]').tooltip();
  }, 500);

  $scope.appointmentData = [];  
  $scope.hospitalID = 0;

  $scope.getAppointmentData = function() {
    var parm = {
      Hospitalcode: (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != null 
        && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != undefined) ? 
        doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode : '' ,
      hospitalid: parseInt($scope.hospitalID),
      fromdate: $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
      todate: $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
    };

    c7AppointmentindicationReportServices.getAppointmentindicationreport(parm, appointmentDataSuccess, appointmentDataError);
  };

  function appointmentDataSuccess(data) {
    $scope.appointmentData = data;
    if($scope.appointmentData.length > 0){
      $scope.exportTOExcel();
    }
  }

  function appointmentDataError(data) {}

  $scope.exportTOExcel = function() {
    var exportData = [
      {
        DWID: 'DW ID',
        PatientTitle: 'Patient Title',
        PatientForename: 'Patient Forename',
        PatientSurname: 'Patient Surname',
        PatientPostcode: 'Patient Postcode',
        NHSNumber: 'NHS Number',
        GPPracticeCode: 'GP Practice Code',
        GPPracticeName: 'GP Practice Name',
        Dateoftreatment: 'Date of treatment ',
        SonographerName: 'Sonographer Name ',
        ReferralInformation: 'Referral Information',
        Dateofreferral: 'Date of referral',
        Appointmenttype: 'Appointment type',
        AppointmentLocationname: 'Appointment Location name',
        DNA: 'DNA',
        CCGName: 'CCG Name',
        AppointmentStartTime: 'Appointment Start Time',
        AppointmentFinishTime: 'Appointment Finish Time',
        Duration: 'Duration'
      }
    ];    

    for (var i = 0; i < $scope.appointmentData.length; i++) {
      var temp = {
        DWID: $scope.appointmentData[i].DWID,
        PatientTitle: $scope.appointmentData[i].Title != null ? $scope.appointmentData[i].Title : '',
        PatientForename: $scope.appointmentData[i].PatientForename != null ? $scope.appointmentData[i].PatientForename : '',
        PatientSurname: $scope.appointmentData[i].PatientSurname != null ? $scope.appointmentData[i].PatientSurname : '',
        PatientPostcode: $scope.appointmentData[i].PatientPostcode != null ? $scope.appointmentData[i].PatientPostcode : '',
        NHSNumber: $scope.appointmentData[i].NHSNumber != null ? $scope.appointmentData[i].NHSNumber : '',
        GPPracticeCode: $scope.appointmentData[i].PracticeCode != null ? $scope.appointmentData[i].PracticeCode : '',
        GPPracticeName: $scope.appointmentData[i].PracticeName != null ? $scope.appointmentData[i].PracticeName : '',
        Dateoftreatment: moment( $scope.appointmentData[i].Dateoftreatment).format('DD-MM-YYYY'),
        SonographerName: $scope.appointmentData[i].SonographerName != null ? $scope.appointmentData[i].SonographerName : '',
        ReferralInformation: '',
        Dateofreferral: moment($scope.appointmentData[i].ReferralRecevied).format('DD-MM-YYYY'),
        Appointmenttype: $scope.appointmentData[i].Appointmenttype != null ? $scope.appointmentData[i].Appointmenttype : '',
        AppointmentLocationname: $scope.appointmentData[i].ScanCentrename != null ? $scope.appointmentData[i].ScanCentrename : '',
        DNA: $scope.appointmentData[i].DNAStatus != null ? $scope.appointmentData[i].DNAStatus : '',
        CCGName: $scope.appointmentData[i].CCGName != null ? $scope.appointmentData[i].CCGName : '',
        AppointmentStartTime: $scope.appointmentData[i].AppointmentStartTime != null ? $scope.appointmentData[i].AppointmentStartTime : '',
        AppointmentFinishTime: $scope.appointmentData[i].AppointmentEndime != null ? $scope.appointmentData[i].AppointmentEndime : '',
        Duration: $scope.appointmentData[i].Duration != null ? $scope.appointmentData[i].Duration : ''
      };
      exportData.push(temp);     
    }
    var xmls = fetchXML([exportData]);

    var ctx = {
      created: new Date().getTime(),
      worksheets: xmls
    };

    var uri = 'data:application/vnd.ms-excel;base64,',
      tmplWorkbookXML =
        '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
        '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
        '<Styles>' +
        '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
        '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
        '</Styles>' +
        '{worksheets}</Workbook>',
      tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
      tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
      base64 = function(s) {
        return window.btoa(unescape(encodeURIComponent(s)));
      },
      format = function(s, c) {
        return s.replace(/{(\w+)}/g, function(m, p) {
          return c[p];
        });
      };
    var workbookXML = format(tmplWorkbookXML, ctx);
    var link = document.createElement('A');
    link.href = uri + base64(workbookXML);
    link.download = 'ReportAppointment_GPPractice_' + new Date().getTime() + '.xls';
    //'Workbook.xls';
    link.target = '_blank';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  function fetchXML(stores) {
    var wrkbookXML = '';
    for (i = 0; i < stores.length; i++) {
      var worksheetName = 'Data';
    //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
      wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
      // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
      var rowXML = '';
      for (j = 0; j < stores[i].length; j++) {
        rowXML += '<Row>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].DWID + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PatientTitle + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PatientForename + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PatientSurname + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PatientPostcode + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].NHSNumber + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].GPPracticeCode + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].GPPracticeName + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Dateoftreatment + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].SonographerName + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralInformation + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Dateofreferral + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Appointmenttype + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentLocationname + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].DNA + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].CCGName + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentStartTime + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentFinishTime + '</Data></Cell>';
        rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Duration + '</Data></Cell>';
        rowXML += '</Row>';
      }
      wrkbookXML += rowXML;
      wrkbookXML += '</Table></Worksheet>';
    }
    return wrkbookXML;
  }
});
