hCueDoctorWebControllers.controller('c7ForeCastReportController', function($scope, $rootScope, $http, alertify, $window, $location, hcueLoginStatusService, hcueDoctorLoginService, referralregion, C7getForecastReportService, datacookieFactory, c7ForecastReportWithRegionServices) {
    // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    setTimeout(function() {
        $('[data-toggle="tooltip"]').tooltip();
    }, 500);

    $scope.forecastReportData = [];
    $scope.forecastReportRegionData = [];

    function getRegionList() {
        var userParam = {
            hospitalcd: (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != null &&
                    doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != undefined) ?
                doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode : '',
            usrid: doctorinfo.doctorid,
            usrtype: 'ADMIN'
        };

        referralregion.listRegion(userParam, regionListSuccess, enquiryError);
    }

    function regionListSuccess(data) {
        $scope.specialityCollection = data;
        var allOption = {
            regionid: 0,
            regionname: ' All',
            active: true
        };
        $scope.specialityCollection.unshift(allOption);
        $scope.specialityCollection.selected = allOption;
    }

    function enquiryError(data) {}
    getRegionList();

    $scope.getForecastReportInfo = function() {
        var userParam = {
            Regionid: $scope.specialityCollection.selected.regionid,
            fromdate: $scope.fromDate == undefined ? moment().format('YYYY-MM-DD') : moment($scope.fromDate.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
            todate: $scope.fromDate == undefined ? moment().format('YYYY-MM-DD') : moment($scope.fromDate.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
        };
        C7getForecastReportService.getForecastReportInfo(userParam, forecastReportInfoSuccess, forecastReportInfoError);
    };

    function forecastReportInfoSuccess(data) {
        $scope.forecastReportData = data;
    }

    function forecastReportInfoError(data) {}

    $scope.exportDataToExcel = function() {
        if (parseInt($scope.specialityCollection.selected.regionid) == 0) {
            var userParam = {
                Hospitalcode: doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode,
                fromdate: $scope.fromDate == undefined ? moment().format('YYYY-MM-DD') : moment($scope.fromDate.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
                todate: $scope.fromDate == undefined ? moment().format('YYYY-MM-DD') : moment($scope.fromDate.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
            };
            c7ForecastReportWithRegionServices.getCostReportwithregionInfo(userParam, forecastReportwithregionSuccess, forecastReportwithregionError);

        } else {
            $scope.exportTOExcel();
        }
    }

    function forecastReportwithregionSuccess(data) {
        $scope.forecastReportRegionData = data;
        if ($scope.forecastReportRegionData.length > 0) {
            exportAllRegion();
        }
    }

    function forecastReportwithregionError(data) {

    }

    function exportAllRegion() {

        var excelData = [];
        for (var i = 0; i < $scope.forecastReportRegionData.length; i++) {

            excelData[i] = [{
                RegionName: 'RegionName',
                Date: 'Date',
                Practice: 'Practice',
                PTScanned: 'PTs Scanned',
                FullPerc: '% Full',
                Revenue: 'Revenue',
                SonographerBASE: 'Sonographer Cost',
                SonographerTRIAGE: 'TRIAGE',
                CSWBASE: 'CSW Cost',
                CSWEXPENSES: 'EXPENSES',
                ROOM: 'ROOM',
                CONSUMABLES: 'CONSUMABLES',
                TOTALCOST: 'TOTAL COST',
                GP: 'GP',
                GPPerc: 'GP %'
            }];

            for (var j = 0; j < $scope.forecastReportRegionData[i].list.length; j++) {
                var temp = {
                    RegionName: $scope.forecastReportRegionData[i].regionname,
                    Date: moment($scope.forecastReportRegionData[i].list[j].ScheduleDate).format('DD-MM-YYYY'),
                    Practice: $scope.forecastReportRegionData[i].list[j].HospitalName,
                    PTScanned: $scope.forecastReportRegionData[i].list[j].ptscaned,
                    FullPerc: $scope.forecastReportRegionData[i].list[j].round,
                    Revenue: $scope.forecastReportRegionData[i].list[j].revenue,
                    SonographerBASE: $scope.forecastReportRegionData[i].list[j].base,
                    SonographerTRIAGE: $scope.forecastReportRegionData[i].list[j].triage,
                    CSWBASE: $scope.forecastReportRegionData[i].list[j].baseinc,
                    CSWEXPENSES: $scope.forecastReportRegionData[i].list[j].expense,
                    ROOM: $scope.forecastReportRegionData[i].list[j].roomcost,
                    CONSUMABLES: $scope.forecastReportRegionData[i].list[j].consumables,
                    TOTALCOST: $scope.forecastReportRegionData[i].list[j].totalcost,
                    GP: $scope.forecastReportRegionData[i].list[j].grossprofit,
                    GPPerc: $scope.forecastReportRegionData[i].list[j].grossprofitpercent
                };
                excelData[i].push(temp);
            }
        }
        var xmls = fetchXML(excelData, 'all');
        var ctx = {
            created: new Date().getTime(),
            worksheets: xmls
        };

        var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML =
            '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
            '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
            '<Styles>' +
            '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
            '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
            '</Styles>' +
            '{worksheets}</Workbook>',
            tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
            tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
            base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                });
            };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);  
        link.download = 'ForecastReport_' + new Date().getTime() + '.xls';
        //'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    $scope.exportTOExcel = function() {
        var excelData = [{
            Date: 'Date',
            Practice: 'Practice',
            PTScanned: 'PTs Scanned',
            FullPerc: '% Full',
            Revenue: 'Revenue',
            SonographerBASE: 'Sonographer Cost',
            SonographerTRIAGE: 'TRIAGE',
            CSWBASE: 'CSW Cost',
            CSWEXPENSES: 'EXPENSES',
            ROOM: 'ROOM',
            CONSUMABLES: 'CONSUMABLES',
            TOTALCOST: 'TOTAL COST',
            GP: 'GP',
            GPPerc: 'GP %'
        }];
        for (var i = 0; i < $scope.forecastReportData.length; i++) {
            var temp = {
                Date: moment($scope.forecastReportData[i].Date).format('DD-MM-YYYY'),
                Practice: $scope.forecastReportData[i].HospitalName,
                PTScanned: $scope.forecastReportData[i].PTsscaned,
                FullPerc: $scope.forecastReportData[i].Fullpercent,
                Revenue: $scope.forecastReportData[i].Revenue,
                SonographerBASE: $scope.forecastReportData[i].Base,
                SonographerTRIAGE: $scope.forecastReportData[i].Triage,
                CSWBASE: $scope.forecastReportData[i].BaseInc,
                CSWEXPENSES: $scope.forecastReportData[i].Expense,
                ROOM: $scope.forecastReportData[i].RoomCost,
                CONSUMABLES: $scope.forecastReportData[i].Consumables,
                TOTALCOST: $scope.forecastReportData[i].Totalcost,
                GP: $scope.forecastReportData[i].Grossprofit,
                GPPerc: $scope.forecastReportData[i].Grossprofitpercent
            };
            excelData.push(temp);
        }
        // var xmls = fetchXML([canadaData, indiaData]);
        var xmls = fetchXML([excelData], 'one');
        var ctx = {
            created: new Date().getTime(),
            worksheets: xmls
        };

        var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML =
            '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
            '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
            '<Styles>' +
            '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
            '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
            '</Styles>' +
            '{worksheets}</Workbook>',
            tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
            tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
            base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                });
            };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'ForecastReport_' + new Date().getTime() + '.xls';
        //'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    function fetchXML(stores, typeRegion) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
            var worksheetName = 'Data';

            if (typeRegion == 'all') {
                worksheetName = stores[i][1].RegionName;
            } else {
                worksheetName = $scope.specialityCollection.filter(v => v.regionid == $scope.specialityCollection.selected.regionid)[0].regionname;
            }
            // i == 0 ? worksheetName = 'Canada' : worksheetName = 'India';
            wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
            // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
            var rowXML = '';
            for (j = 0; j < stores[i].length; j++) {
                rowXML += '<Row>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Date + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Practice + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PTScanned + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].FullPerc + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].SonographerBASE + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].SonographerTRIAGE + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].CSWBASE + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].CSWEXPENSES + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ROOM + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].CONSUMABLES + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].TOTALCOST + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Revenue + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].GP + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].GPPerc + '</Data></Cell>';
                rowXML += '</Row>';
            }
            wrkbookXML += rowXML;
            wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
    }

    $scope.exportTOExcel1 = function() {
        var data = [
            { Vehicle: 'BMW', Date: '30, Jul 2013 09:24 AM', Location: 'Hauz Khas, Enclave, New Delhi, Delhi, India', Speed: 42 },
            { Vehicle: 'Honda CBR', Date: '30, Jul 2013 12:00 AM', Location: 'Military Road,  West Bengal 734013,  India', Speed: 0 },
            { Vehicle: 'Supra', Date: '30, Jul 2013 07:53 AM', Location: "Sec-45, St. Angel's School, Gurgaon, Haryana, India", Speed: 58 },
            { Vehicle: 'Land Cruiser', Date: '30, Jul 2013 09:35 AM', Location: 'DLF Phase I, Marble Market, Gurgaon, Haryana, India', Speed: 83 },
            { Vehicle: 'Suzuki Swift', Date: '30, Jul 2013 12:02 AM', Location: 'Behind Central Bank RO, Ram Krishna Rd by-lane, Siliguri, West Bengal, India', Speed: 0 },
            { Vehicle: 'Honda Civic', Date: '30, Jul 2013 12:00 AM', Location: 'Behind Central Bank RO, Ram Krishna Rd by-lane, Siliguri, West Bengal, India', Speed: 0 },
            { Vehicle: 'Honda Accord', Date: '30, Jul 2013 11:05 AM', Location: 'DLF Phase IV, Super Mart 1, Gurgaon, Haryana, India', Speed: 71 }
        ];
        JSONToCSVConvertor(data, 'Vehicle Report', true);
    };

    function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
        //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
        var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

        var CSV = '';
        //Set Report title in first row or line

        CSV += ReportTitle + '\r\n\n';

        //This condition will generate the Label/Header
        if (ShowLabel) {
            var row = '';

            //This loop will extract the label from 1st index of on array
            for (var index in arrData[0]) {
                //Now convert each value to string and comma-seprated
                row += index + ',';
            }

            row = row.slice(0, -1);

            //append Label row with line break
            CSV += row + '\r\n';
        }

        //1st loop is to extract each row
        for (var i = 0; i < arrData.length; i++) {
            var row = '';

            //2nd loop will extract each column and convert it in string comma-seprated
            for (var index in arrData[i]) {
                row += '"' + arrData[i][index] + '",';
            }

            row.slice(0, row.length - 1);

            //add a line break after each row
            CSV += row + '\r\n';
        }

        if (CSV == '') {
            alert('Invalid data');
            return;
        }

        //Generate a file name
        var fileName = 'MyReport_';
        //this will remove the blank-spaces from the title and replace it with an underscore
        fileName += ReportTitle.replace(/ /g, '_');

        //Initialize file format you want csv or xls
        var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

        // Now the little tricky part.
        // you can use either>> window.open(uri);
        // but this will not work in some browsers
        // or you will not get the correct file extension

        //this trick will generate a temp <a /> tag
        var link = document.createElement('a');
        link.href = uri;

        //set the visibility hidden so it will not effect on your web-layout
        link.style = 'visibility:hidden';
        link.download = fileName + '.csv';

        //this part will append the anchor tag and remove it after automatic click
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
});