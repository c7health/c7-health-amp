hCueDoctorWebControllers.controller('c7PatientsinReportController', function($scope, $rootScope, $http, alertify, $window, $location, hcueLoginStatusService, hcueDoctorLoginService, referralregion, datacookieFactory, c7PatientinReportServices, c7AdminServices) {
    // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    setTimeout(function() {
        $('[data-toggle="tooltip"]').tooltip();
    }, 500);

    $scope.hospitalID = '0';
    $scope.patientsData = [];
    $scope.categoryID = "0";
    function categorylisting(){
        c7AdminServices.CategoryList(parentHospitalId,categorylistSuccess, specialityError)

        function categorylistSuccess(data){
            $scope.categoryCollection=data;

        }
        function specialityError(data){
            alertify.error("Error for loading Category");

        }
    }
    categorylisting();
    $scope.changeCategory = function () {
        $scope.getPatientsData(); 
    }


    // function scanCenterListing() {
    //   var param = {
    //     pageNumber: 1,
    //     HospitalID: parentHospitalId,
    //     pageSize: 1000,
    //     IncludeDoctors: 'N',
    //     IncludeBranch: 'Y',
    //     ActiveHospital: 'Y',
    //     ActiveDoctor: 'Y'
    //   };

    //   c7AdminServices.getHospitals(param, onGetHospitalsSuccess, listDashboardError);
    // }

    // function onGetHospitalsSuccess(data) {
    //   $scope.hospitalDoctorsList = data.rows;
    // }

    function listDashboardError(data) {}

    $scope.getPatientsData = function() {
        var parm = {
            CategoryID: Number ($scope.categoryID),
            Hospitalcode: (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != null &&
                    doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != undefined) ?
                doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode : '',
            hospitalid: 0,
            fromdate: moment().format('YYYY-MM-DD'),
            todate: moment().format('YYYY-MM-DD')
        };
        // fromdate: $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
        // todate: $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')

        // c7PatientinReportServices.getPatientinSystemreport(parm, patientsDataSuccess, patientsDataError);
        c7PatientinReportServices.getPatientinSystemreportnew(parm, patientsDataSuccess, patientsDataError);
        c7PatientinReportServices.getPatientinSystemreporttotal(parm, patientstotalDataSuccess, patientsDataError);
    };
    $scope.getPatientsData();

    function patientsDataSuccess(data) {
        $scope.patientsData = data;
        console.log(data);
    }

    function patientstotalDataSuccess(data) {
        $scope.patientsDataTotal = data;
    }

    function patientsDataError(data) {}



    $scope.exportTOExcel = function() {
        var exportData = [{
            Name: 'Name',
            New: 'New',
            Followups: 'Follow ups',
            NC: 'NC',
            Issues: 'Issues',
            TotalRegion: 'Total Region'
        }];

        for (var i = 0; i < $scope.patientsData.length; i++) {
            for (var j = 0; j < $scope.patientsData[i].Ccgroupdetails.length; j++) {
                var temp = {
                    Name: $scope.patientsData[i].Ccgroupdetails[j].Name,
                    New: $scope.patientsData[i].Ccgroupdetails[j].Triagetot,
                    Followups: $scope.patientsData[i].Ccgroupdetails[j].Followupstot,
                    NC: $scope.patientsData[i].Ccgroupdetails[j].NC,
                    Issues: $scope.patientsData[i].Ccgroupdetails[j].Issuestot,
                    TotalRegion: ($scope.patientsData[i].Ccgroupdetails[j].totregion == undefined) ? 0 : $scope.patientsData[i].Ccgroupdetails[j].totregion
                };
                exportData.push(temp);
            }
            var temp = {
                Name: $scope.patientsData[i].Name,
                New: $scope.patientsData[i].Triagetot,
                Followups: $scope.patientsData[i].Followupstot,
                NC: $scope.patientsData[i].NC,
                Issues: $scope.patientsData[i].Issuestot,
                TotalRegion: ($scope.patientsData[i].Regiontot == undefined) ? 0 : $scope.patientsData[i].Regiontot
            };
            exportData.push(temp);
        }
        for (var i = 0; i < $scope.patientsDataTotal.length; i++) {
            var temp = {
                Name: $scope.patientsDataTotal[i].Name,
                New: $scope.patientsDataTotal[i].Triagetot,
                Followups: $scope.patientsDataTotal[i].Followupstot,
                NC: $scope.patientsDataTotal[i].NC,
                Issues: $scope.patientsDataTotal[i].Issuestot,
                TotalRegion: ($scope.patientsDataTotal[i].Regiontot == undefined) ? 0 : $scope.patientsDataTotal[i].Regiontot
            };
            exportData.push(temp);
        }
        var xmls = fetchXML([exportData]);

        var ctx = {
            created: new Date().getTime(),
            worksheets: xmls
        };

        var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML =
            '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
            '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
            '<Styles>' +
            '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
            '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
            '</Styles>' +
            '{worksheets}</Workbook>',
            tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
            tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
            base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                });
            };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'PatientsInSystemReport_' + new Date().getTime() + '.xls';
        //'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    function fetchXML(stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
            var worksheetName = 'Data';
            //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
            wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
            // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
            var rowXML = '';
            for (j = 0; j < stores[i].length; j++) {
                rowXML += '<Row>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Name + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].New + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Followups + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].NC + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Issues + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].TotalRegion + '</Data></Cell>';
                rowXML += '</Row>';
            }
            wrkbookXML += rowXML;
            wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
    }

    // $scope.exportTOExcel = function() {
    //   var exportData = [
    //     {
    //       Date: 'Date',
    //       Triage: 'Triage',
    //       Followups: 'Follow ups',
    //       NC: 'NC',
    //       Issues: 'Issues',
    //       TotalRegion: 'Total Region',
    //       AwaitingBooking: 'Awaiting Booking'
    //     }
    //   ];    

    //   for (var i = 0; i < $scope.patientsData.length; i++) {
    //     var temp = {
    //       Date: moment($scope.patientsData[i].ReceivedDate).format('DD-MM-YYYY'),
    //       Triage: $scope.patientsData[i].Triagetot,
    //       Followups: $scope.patientsData[i].Followupstot,
    //       NC: $scope.patientsData[i].NCtot,
    //       Issues: $scope.patientsData[i].Issuestot,
    //       TotalRegion: $scope.patientsData[i].Regiontot,
    //       AwaitingBooking: $scope.patientsData[i].Awaitingtot
    //     };
    //     exportData.push(temp);     
    //   }
    //   var xmls = fetchXML([exportData]);

    //   var ctx = {
    //     created: new Date().getTime(),
    //     worksheets: xmls
    //   };

    //   var uri = 'data:application/vnd.ms-excel;base64,',
    //     tmplWorkbookXML =
    //       '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
    //       '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
    //       '<Styles>' +
    //       '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
    //       '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
    //       '</Styles>' +
    //       '{worksheets}</Workbook>',
    //     tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
    //     tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
    //     base64 = function(s) {
    //       return window.btoa(unescape(encodeURIComponent(s)));
    //     },
    //     format = function(s, c) {
    //       return s.replace(/{(\w+)}/g, function(m, p) {
    //         return c[p];
    //       });
    //     };
    //   var workbookXML = format(tmplWorkbookXML, ctx);
    //   var link = document.createElement('A');
    //   link.href = uri + base64(workbookXML);
    //   link.download = 'PatientsInSystemReport_' + new Date().getTime() + '.xls';
    //   //'Workbook.xls';
    //   link.target = '_blank';
    //   document.body.appendChild(link);
    //   link.click();
    //   document.body.removeChild(link);
    // };

    // function fetchXML(stores) {
    //   var wrkbookXML = '';
    //   for (i = 0; i < stores.length; i++) {
    //     var worksheetName = 'Data';
    //   //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
    //     wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
    //     // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
    //     var rowXML = '';
    //     for (j = 0; j < stores[i].length; j++) {
    //       rowXML += '<Row>';
    //       rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Date + '</Data></Cell>';
    //       rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Triage + '</Data></Cell>';
    //       rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Followups + '</Data></Cell>';
    //       rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].NC + '</Data></Cell>';
    //       rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Issues + '</Data></Cell>';
    //       rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].TotalRegion + '</Data></Cell>';
    //       rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AwaitingBooking + '</Data></Cell>';
    //       rowXML += '</Row>';
    //     }
    //     wrkbookXML += rowXML;
    //     wrkbookXML += '</Table></Worksheet>';
    //   }
    //   return wrkbookXML;
    // }
});