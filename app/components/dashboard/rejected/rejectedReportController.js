hCueDoctorWebControllers.controller('c7RejectedReportController', function($scope, $rootScope, $http, alertify, $window, $location, hcueLoginStatusService, hcueDoctorLoginService, referralregion, datacookieFactory, c7EnquiriesServices, c7AdminServices, referralccg) {
     // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
   
    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
  
    if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined){
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    setTimeout(function () {
        $('[data-toggle="tooltip"]').tooltip();
    }, 500);
    
    $scope.hospitalID = 0;
    $scope.rejectedData = [];
    $scope.ccgId = '0';
    $scope.date = moment().format('DD-MM-YYYY') + ' - ' + moment().format('DD-MM-YYYY');
    function scanCenterListing(){
        var param ={
            "pageNumber": 1,
            "HospitalID": parentHospitalId,
            "pageSize": 1000,
            "IncludeDoctors": "N",
            "IncludeBranch": "Y",
            "ActiveHospital": "Y",
            "ActiveDoctor": "Y"            
        }
        
        c7AdminServices.getHospitals(param,onGetHospitalsSuccess,listDashboardError);

    }
    
    function onGetHospitalsSuccess(data) {
        $scope.hospitalDoctorsList = data.rows;                       
    }

    function listDashboardError(data){

    }
    scanCenterListing();

    var userParam = {
        "pageNumber": 0,
        "pageSize": 1000,
        "usrid": doctorinfo.doctorid,
        "usrtype": "ADMIN",
        "hospitalcd": hospitalCode
    };
    referralccg.alllistccg(userParam, ccglistsuccess, listDashboardError);
    function ccglistsuccess(data) {
        $scope.allCCG = data.filter(data => data.active == true);
    }
    
    $scope.getrejectedData = function(){
        var parm ={
            hospitalCode: (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != null 
              && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != undefined) ? 
              doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode : '' ,
            hospitalID: parseInt($scope.hospitalID),
            fromdate: moment($scope.date.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
            todate: moment($scope.date.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD'),
            ccgID: parseInt($scope.ccgId)
        }

        c7EnquiriesServices.listrejectPatients(parm, rejectedDataSuccess, rejectedDataError);
    }
    $scope.getrejectedData();

    function rejectedDataSuccess(data){
        $scope.rejectedData = data;
    }

    function rejectedDataError(data){

    }
    
    $scope.exportTOExcel = function() {
        var exportAuditData = [
          {
            DWID: 'DW ID',
            Region: 'Region',
            CCG: 'CCG',
            PracticeName: 'Practice Name',
            ReferralDoctor: 'Referral Doctor',
            EnquirySource: 'Enquiry Source',
            ReferralCreated: 'Referral Created',
            ReferralReceived: 'Referral Received',
            ReferralProcessed: 'Referral Processed'
          }
        ];    
    
        for (var i = 0; i < $scope.rejectedData.length; i++) {
          var temp = {
            DWID: $scope.rejectedData[i].DWID,
            Region: $scope.rejectedData[i].RegionName,
            CCG: $scope.rejectedData[i].CCGName,
            PracticeName: $scope.rejectedData[i].PracticeName,
            ReferralDoctor: $scope.rejectedData[i].ReferralDoctor,
            EnquirySource: $scope.rejectedData[i].EnquirySource,
            ReferralCreated: moment($scope.rejectedData[i].ReferralCreated).format('DD-MM-YYYY'),
            ReferralReceived: moment($scope.rejectedData[i].ReferralRecevied).format('DD-MM-YYYY'),
            ReferralProcessed: moment($scope.rejectedData[i].ReferralProcessed).format('DD-MM-YYYY')
          };
          exportAuditData.push(temp);     
        }
        var xmls = fetchXML([exportAuditData]);
    
        var ctx = {
          created: new Date().getTime(),
          worksheets: xmls
        };
    
        var uri = 'data:application/vnd.ms-excel;base64,',
          tmplWorkbookXML =
            '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
            '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
            '<Styles>' +
            '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
            '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
            '</Styles>' +
            '{worksheets}</Workbook>',
          tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
          tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
          base64 = function(s) {
            return window.btoa(unescape(encodeURIComponent(s)));
          },
          format = function(s, c) {
            return s.replace(/{(\w+)}/g, function(m, p) {
              return c[p];
            });
          };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'ReferralReport_' + new Date().getTime() + '.xls';
        //'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      };
    
      function fetchXML(stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
          var worksheetName = 'Data';
        //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
          wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
          // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
          var rowXML = '';    
          for (j = 0; j < stores[i].length; j++) {
            rowXML += '<Row>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].DWID + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Region + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].CCG + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PracticeName + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralDoctor + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].EnquirySource + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralCreated + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralReceived + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralProcessed + '</Data></Cell>';
            rowXML += '</Row>';
          }
          wrkbookXML += rowXML;
          wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
      }
   
});