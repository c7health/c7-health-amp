hCueDoctorWebControllers.controller('exceptionNotesReportController', function ($scope, $rootScope, $http, alertify, $window, $location, datacookieFactory, hcueLoginStatusService, hcueDoctorLoginService, c7ExceptionNotesReportService) {
    // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    $scope.daterange = moment().format('DD-MM-YYYY') + ' - ' + moment().format('DD-MM-YYYY');
    
    $scope.exceptionData = [];
    

    $scope.getExceptionData = function () { 
        var data = {
            Hospitalcode: hospitalCode,            
            fromdate: $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
            todate: $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
        }
        
        c7ExceptionNotesReportService.getExceptionnotesreportInfo(data, excepNotesReportSuccess, error);       
         
    }    

    function excepNotesReportSuccess(data) {        
        $scope.exceptionData = data;
    }

    function error(data) {
        alertify.error(data);
    }
    

    $scope.exportTOExcel = function () {
        var exportNotesData = [{            
            DWID: 'DW ID',
            Name: 'Name',
            NoteDate: 'Noted Date',
            Description: 'Manage Description',
            Notes: 'Notes',           
        }];

        for (var i = 0; i < $scope.exceptionData.length; i++) {
            var temp = {                
                DWID: ($scope.exceptionData[i].DWID == undefined) ? '' : $scope.exceptionData[i].DWID,
                Name: ($scope.exceptionData[i].FullName == undefined) ? '' : $scope.exceptionData[i].FullName,
                NoteDate: ($scope.exceptionData[i].Notedate == undefined) ? '' : moment($scope.exceptionData[i].Notedate).format('DD-MM-YYYY'),
                Description: ($scope.exceptionData[i].ManageDesc == undefined) ? '' : $scope.exceptionData[i].ManageDesc,
                Notes: ($scope.exceptionData[i].notes == undefined) ? '' : $scope.exceptionData[i].notes,                
            };
            exportNotesData.push(temp);            
        }
        var xmls = fetchXML([exportNotesData]);

        var ctx = {
            created: new Date().getTime(),
            worksheets: xmls
        };

        var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML =
                '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
                '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
                '<Styles>' +
                '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
                '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
                '</Styles>' +
                '{worksheets}</Workbook>',
            tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
            tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
            base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                });
            };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'ExceptionReport_' + new Date().getTime() + '.xls';
        //'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    function fetchXML(stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
            var worksheetName = 'Data';
            //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
            wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
            // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
            var rowXML = '';
            for (j = 0; j < stores[i].length; j++) {
                rowXML += '<Row>';
                var key = Object.keys(stores[0][0]);
                for (var k = 0; k < key.length; k++) {
                    rowXML += '<Cell><Data ss:Type="String">' + stores[i][j][key[k]] + '</Data></Cell>';
                }
                rowXML += '</Row>';
            }
            wrkbookXML += rowXML;
            wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
    }

});