hCueDoctorWebControllers.controller('costAnalyticReportController', function($scope, $rootScope, $http, alertify, $window, $location, hcueLoginStatusService, hcueDoctorLoginService, referralregion, c7CostanalyticsReportServices, datacookieFactory) {
    // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    setTimeout(function() {
        $('[data-toggle="tooltip"]').tooltip();
    }, 500);


    function getRegionList() {
        var userParam = {
            hospitalcd: (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != null &&
                    doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != undefined) ?
                doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode : '',
            usrid: doctorinfo.doctorid,
            usrtype: 'ADMIN'
        };

        referralregion.listRegion(userParam, regionListSuccess, enquiryError);
    }

    function regionListSuccess(data) {
        $scope.costCollection = data;
        var allOption = {
            regionid: 0,
            regionname: ' All',
            active: true
        }
        $scope.costCollection.unshift(allOption);
        $scope.costCollection.selected = allOption;
    }

    function enquiryError(data) {}
    getRegionList();

    $scope.applyFilterData = function() {

        var parm = {
            "Regionid": parseInt($scope.costCollection.selected.regionid),
            "fromdate": $scope.fromDate == undefined ? moment().format('YYYY-MM-DD') : moment($scope.fromDate.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
            "todate": $scope.fromDate == undefined ? moment().format('YYYY-MM-DD') : moment($scope.fromDate.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
        }
        c7CostanalyticsReportServices.getCostanalyticsreport(parm, listCostAnalyticSuccess, listCostAnalyticError)
    }

    function listCostAnalyticSuccess(data) {
        $scope.costAnalyticCollection = data;
        if ($scope.costAnalyticCollection.length == 0) {
            $scope.costAnalyticCollection[0] = {
                "GPvalue": 0,
                "GPpercentvalue": 0,
                "Costvalue": 0,
                "Revenuevalue": 0,
                "Capacitypercent": 0,
            };
        }
    }

    function listCostAnalyticError(data) {

    }

});