hCueDoctorWebControllers.controller('c7AuditCriteriaReportController', function ($scope, $rootScope, $http, alertify, $window, $location, hcueLoginStatusService, hcueDoctorLoginService, referralregion, datacookieFactory, c7ClinicAuditReportServices, c7AdminServices, referralccg) {
    // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    setTimeout(function () {
        $('[data-toggle="tooltip"]').tooltip();
    }, 500);

    // $scope.hospitalID = null;
    $scope.clinicAuditData = [];

    function scanCenterListing() {
        var param = {
            "pageNumber": 1,
            "HospitalID": parentHospitalId,
            "pageSize": 1000,
            "IncludeDoctors": "N",
            "IncludeBranch": "Y",
            "ActiveHospital": "Y",
            "ActiveDoctor": "Y"
        }

        c7AdminServices.getHospitals(param, onGetHospitalsSuccess, listDashboardError);

    }

    function onGetHospitalsSuccess(data) {
        $scope.hospitalDoctorsList = data.rows;
    }

    function listDashboardError(data) {

    }
    scanCenterListing();

    var userParam = {
        "pageNumber": 0,
        "pageSize": 1000,
        "usrid": doctorinfo.doctorid,
        "usrtype": "ADMIN",
        "hospitalcd": hospitalCode
    };
    referralregion.alllistRegion(userParam, regionlistsuccess, listDashboardError);

    function regionlistsuccess(data) {
        $scope.allRegion = data.filter(data => data.active == true);
    }

    referralccg.alllistccg(userParam, ccglistsuccess, listDashboardError);

    function ccglistsuccess(data) {
        $scope.allCCG = data.filter(data => data.active == true);
    }
    $scope.hospitalID = '0';
    $scope.regionId = '0';
    $scope.ccgId = '0';
    $scope.getClinicAuditData = function () {
        var parm = {
            "Hospitalcode": (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != null &&
                doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != undefined) ?
                doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode : '',
            "hospitalid": parseInt($scope.hospitalID),
            "regionid": parseInt($scope.regionId),
            "ccgid": parseInt($scope.ccgId),
            "fromdate": $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
            "todate": $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
        }

        c7ClinicAuditReportServices.getClinicauditcriteriareport(parm, clinicAuditDataSuccess, clinicAuditDataError);
    }

    function clinicAuditDataSuccess(data) {
        $scope.clinicAuditData = data;
    }

    function clinicAuditDataError(data) {

    }

    $scope.exportTOExcel = function () {
        var exportAuditData = [{
            DWID: 'DW ID',
            PatientInitials: 'Patient Name',
            Scandate: 'Scan Date',
            Speciality: 'Speciality',
            Sonographername: 'Sonographer Name',
            Region: 'Region',
            ccg: 'CCG Name',
            AppointmentStatus: 'Appointment Status'
        }];

        for (var i = 0; i < $scope.clinicAuditData.length; i++) {
            var temp = {
                DWID: $scope.clinicAuditData[i].DWID,
                PatientInitials: $scope.clinicAuditData[i].Patientname,
                Scandate: moment($scope.clinicAuditData[i].Scandate).format('DD-MM-YYYY'),
                Speciality: $scope.clinicAuditData[i].Speciality,
                Sonographername: $scope.clinicAuditData[i].SonographerName,
                Region: $scope.clinicAuditData[i].RegionName,
                ccg: $scope.clinicAuditData[i].CcgName,
                AppointmentStatus: $scope.clinicAuditData[i].AppointmentStatus,
            };
            exportAuditData.push(temp);
        }
        var xmls = fetchXML([exportAuditData]);

        var ctx = {
            created: new Date().getTime(),
            worksheets: xmls
        };

        var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML =
                '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
                '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
                '<Styles>' +
                '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
                '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
                '</Styles>' +
                '{worksheets}</Workbook>',
            tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
            tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
            base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                });
            };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'ClinicalAuditCriteriaReport_' + new Date().getTime() + '.xls';
        //'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    function fetchXML(stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
            var worksheetName = 'Data';
            //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
            wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
            // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
            var rowXML = '';
            for (j = 0; j < stores[i].length; j++) {
                rowXML += '<Row>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].DWID + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PatientInitials + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Scandate + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Speciality + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Sonographername + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Region + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ccg + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentStatus + '</Data></Cell>';

                rowXML += '</Row>';
            }
            wrkbookXML += rowXML;
            wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
    }

});