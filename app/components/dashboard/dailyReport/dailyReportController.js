hCueDoctorWebControllers.controller('c7DailyReportController', function($scope, $rootScope, $http, alertify, $window, $location, hcueLoginStatusService, hcueDoctorLoginService, referralregion, datacookieFactory, c7DailyReportServices, c7DailySpecialityReportServices) {
    // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    setTimeout(function() {
        $('[data-toggle="tooltip"]').tooltip();
    }, 500);

    $scope.filterDate = moment().format('DD-MM-YYYY');
    $scope.hospitalID = parentHospitalId;
    $scope.sortRegion = false;
    $scope.dailySpecialityReportData = [];
    $scope.dailyReportData = [];

    $scope.setSortRegion = function() {
        $scope.sortRegion = !$scope.sortRegion;
    }

    function getDailyReport() {
        var userParam = {
            "Hospitalcode": (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != null &&
                    doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != undefined) ?
                doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode : '',
            "hospitalID": $scope.hospitalID,
            // "date": moment($scope.filterDate, 'DD-MM-YYYY').format('YYYY-MM-DD')
            "date": moment().format('YYYY-MM-DD')
        }
        c7DailyReportServices.getDailyReportInfo(userParam, dailyReportSuccess, dailyReportError);
    }

    function dailyReportSuccess(data) {
        $scope.dailyReportData = data;
    }

    function dailyReportError(data) {

    }

    function getDailySpecialityReport() {
        var userParam = {
            "Hospitalcode": (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != null &&
                    doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != undefined) ?
                doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode : '',
            "hospitalID": $scope.hospitalID,
            "date": moment().format('YYYY-MM-DD')
                // "date": moment($scope.filterDate, 'DD-MM-YYYY').format('YYYY-MM-DD')
        }
        c7DailySpecialityReportServices.getDailySpecialityReportInfo(userParam, dailySpecialityReportSuccess, dailySpecialityReportError);
    }

    function dailySpecialityReportSuccess(data) {
        $scope.dailySpecialityReportData = data;
    }

    function dailySpecialityReportError(data) {

    }

    $scope.reportData = function() {
        getDailyReport();
        getDailySpecialityReport();
    }
    $scope.reportData();

    $scope.exportTOExcel = function() {
        var exportDailyData = [{
            Region: 'Region',
            Newtobook: 'New to book',
            Followups: 'Follow ups',
            Nocontact: 'No contact',
            Issues: 'Issues',
            Total: 'Total'
        }];

        for (var i = 0; i < $scope.dailyReportData.length; i++) {
            var temp = {
                Region: $scope.dailyReportData[i].regionname,
                Newtobook: $scope.dailyReportData[i].newtriagebook,
                Followups: $scope.dailyReportData[i].followup,
                Nocontact: $scope.dailyReportData[i].nocontact,
                Issues: $scope.dailyReportData[i].issue,
                Total: $scope.dailyReportData[i].totalregion
            };
            exportDailyData.push(temp);
        }
        var xmls = fetchXML([exportDailyData]);

        var ctx = {
            created: new Date().getTime(),
            worksheets: xmls
        };

        var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML =
            '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
            '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
            '<Styles>' +
            '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
            '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
            '</Styles>' +
            '{worksheets}</Workbook>',
            tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
            tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
            base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                });
            };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'DailyReport_' + new Date().getTime() + '.xls';
        //'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    function fetchXML(stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
            var worksheetName = 'Data';
            //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
            wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
            // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
            var rowXML = '';
            for (j = 0; j < stores[i].length; j++) {
                rowXML += '<Row>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Region + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Newtobook + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Followups + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Nocontact + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Issues + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Total + '</Data></Cell>';
                rowXML += '</Row>';
            }
            wrkbookXML += rowXML;
            wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
    }


    $scope.exportIssueTOExcel = function() {
        var exportIssueData = [{
            DWID: 'DW ID',
            ReceivedDate: 'Received Date',
            Speciality: 'Speciality',
            Region: 'Region',
            Reason: 'Reason',
            
        }];

        for (var i = 0; i < $scope.dailySpecialityReportData.length; i++) {
            var temp = {
                DWID: $scope.dailySpecialityReportData[i].patientid,
                ReceivedDate: $scope.dailySpecialityReportData[i].referalreceived,
                Speciality: $scope.dailySpecialityReportData[i].speciality,
                Region: $scope.dailySpecialityReportData[i].regionname,
                Reason: $scope.dailySpecialityReportData[i].reason,
            };
            exportIssueData.push(temp);
        }
        var xmls = fetchXMLIssue([exportIssueData]);

        var ctx = {
            created: new Date().getTime(),
            worksheets: xmls
        };

        var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML =
            '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
            '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
            '<Styles>' +
            '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
            '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
            '</Styles>' +
            '{worksheets}</Workbook>',
            tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
            tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
            base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                });
            };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'DailyIssuesReport_' + new Date().getTime() + '.xls';
        //'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    function fetchXMLIssue(stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
            var worksheetName = 'Data';
            //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
            wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
            var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';

            for (j = 0; j < stores[i].length; j++) {
                rowXML += '<Row>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].DWID + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReceivedDate + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Speciality + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Region + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Reason + '</Data></Cell>';
                rowXML += '</Row>';
            }
            wrkbookXML += rowXML;
            wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
    }

    // function getRegionList() {
    //     var userParam = {
    //         "usrid":1,
    //         "usrtype":"ADMIN"
    //     }

    //     referralregion.listRegion(userParam, regionListSuccess, enquiryError);
    // }

    // function regionListSuccess(data){
    //     $scope.specialityCollection = data;
    //     var allOption = { 
    //                     "regionid" : 0,
    //                     "regionname": "All",
    //                     }
    //     $scope.specialityCollection.unshift(allOption);
    // }

    // function enquiryError(data){

    // }
    // getRegionList();



});