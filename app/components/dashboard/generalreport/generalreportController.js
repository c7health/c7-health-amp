hCueDoctorWebControllers.controller('c7GeneralReportController', function ($scope, $rootScope, $http, alertify, $window, $location, datacookieFactory, hcueLoginStatusService, hcueDoctorLoginService, c7DashboardServices) {
    // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    $scope.daterange = moment().format('DD-MM-YYYY') + ' - ' + moment().format('DD-MM-YYYY');
    
    $scope.reportCollfile = [];

    function autoload(){
        $('#loaderDiv').show();
        var today = new Date(); 
        var dd = today.getDate(); 
        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear(); 
        if(dd<10) { 
            dd='0'+dd; 
        } if(mm<10) { 
            mm='0'+mm; 
        }
        today =  yyyy +'-'+mm+'-'+dd; 
        // console.log(today);

        c7DashboardServices.GeneralStatsReportFileList(today, GeneralStatsReportFileListsuccess, error);
        function GeneralStatsReportFileListsuccess(data) {
            $scope.reportCollfile = data;
            $('#loaderDiv').hide();
 
        }
        function error(data) {
            alertify.error(data);
        }
    }
    autoload();
    

    //    Refresh Icon
     $scope.GeneralStatsReportFileList = function(){
        autoload();

    }

    
    // $scope.IsVisible = false;
    // $scope.testingFilterbyRange = function(){
    //     $scope.IsVisible = true;
    // }

    $scope.testingFilterbyRange = function () { 
        var data = {
            HospitalCode: hospitalCode,
            HospitalID: parentHospitalId,
            UserID: doctorinfo.doctordata.doctor[0].DoctorID,
            fromdate: $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
            todate: $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
        }
        // c7DashboardServices.getGeneralReport(data, generalreportsuccess, error);
        // function generalreportsuccess(data) {
        //     $scope.reportColl = data;
        // }
        // function error(data) {
        //     alertify.error(data);
        // }
         c7DashboardServices.getGeneralreportfile(data, generalreportfilesuccess, error);
        function generalreportfilesuccess(data) {
        
            $scope.reportCollfile = data;   

            autoload();
            alertify.success("Added record successfully");
            
        }
        function error(data) {
            alertify.error(data);
        }
         
    }
    
    // $scope.loadData = function () {
    //     var data = {
    //         HospitalCode: hospitalCode,
    //         HospitalID: parentHospitalId,
    //         fromdate: $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
    //         todate: $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
    //     }
    //     c7DashboardServices.getGeneralReport(data, generalreportsuccess, error);
    //     function generalreportsuccess(data) {
    //         $scope.reportColl = data;
    //     }
    //     function error(data) {
    //         alertify.error(data);
    //     }
    // }

    $scope.exportTOExcel = function () {
        var exportAuditData = [{
            AppointmentID: 'Appointment ID',
            DWID: 'DW ID',
            nhsnumber: 'NHS Number',
            Postcode: 'Post Code',
            ccgname: 'CCG Name',
            DoctorName: 'Doctor Name',
            PracticeCode: 'Practice Code',
            PracticeName: 'Practice Name',
            ReferralID: 'Referral ID',
            referalreceived: 'Referal Received',
            DateBooked: 'Date Booked',
            DateTreatment: 'Date Treatment',
            Appointmentlocation: 'Location',
            subspeciality: 'Sub Speciality',
            AppointmentType: 'Appointment Type',
            AppointmentStatus: 'Appointment Status',
            SonographerName: 'Sonographer Name',
            StartTime: 'Start Time',
            duration: 'Duration',
            ReportDate: 'Report Date',
            TriageDate: 'Triage Date',
            ClockStart: 'Clock Start',
        }];

        for (var i = 0; i < $scope.reportCollfile.length; i++) {
            var temp = {
                AppointmentID: ($scope.reportCollfile[i].AppointmentID == undefined) ? 0 : $scope.reportCollfile[i].AppointmentID,
                DWID: ($scope.reportCollfile[i].DWID == undefined) ? 0 : $scope.reportCollfile[i].DWID,
                nhsnumber: ($scope.reportCollfile[i].nhsnumber == undefined) ? 0 : $scope.reportCollfile[i].nhsnumber,
                Postcode: ($scope.reportCollfile[i].Postcode == undefined) ? 0 : $scope.reportCollfile[i].Postcode,
                ccgname: ($scope.reportCollfile[i].ccgname == undefined) ? 0 : $scope.reportCollfile[i].ccgname,
                DoctorName: ($scope.reportCollfile[i].DoctorName == undefined) ? 0 : $scope.reportCollfile[i].DoctorName,
                PracticeCode: ($scope.reportCollfile[i].PracticeCode == undefined) ? 0 : $scope.reportCollfile[i].PracticeCode,
                PracticeName: ($scope.reportCollfile[i].PracticeName == undefined) ? 0 : $scope.reportCollfile[i].PracticeName,
                ReferralID: ($scope.reportCollfile[i].ReferralID == undefined) ? 0 : $scope.reportCollfile[i].ReferralID,
                referalreceived: ($scope.reportCollfile[i].referalreceived == undefined) ? 0 : $scope.reportCollfile[i].referalreceived,
                DateBooked: ($scope.reportCollfile[i].DateBooked == undefined) ? 0 : $scope.reportCollfile[i].DateBooked,
                DateTreatment: ($scope.reportCollfile[i].DateTreatment == undefined) ? 0 : $scope.reportCollfile[i].DateTreatment,
                Appointmentlocation: ($scope.reportCollfile[i].Appointmentlocation == undefined) ? 0 : $scope.reportCollfile[i].Appointmentlocation,
                subspeciality: ($scope.reportCollfile[i].subspeciality == undefined) ? 0 : $scope.reportCollfile[i].subspeciality,
                AppointmentType: ($scope.reportCollfile[i].AppointmentType == undefined) ? 0 : $scope.reportCollfile[i].AppointmentType,
                AppointmentStatus: ($scope.reportCollfile[i].AppointmentStatus == undefined) ? 0 : $scope.reportCollfile[i].AppointmentStatus,
                SonographerName: ($scope.reportCollfile[i].SonographerName == undefined) ? 0 : $scope.reportCollfile[i].SonographerName,
                StartTime: ($scope.reportCollfile[i].StartTime == undefined) ? 0 : $scope.reportCollfile[i].StartTime,
                duration: ($scope.reportCollfile[i].duration == undefined) ? 0 : $scope.reportCollfile[i].duration,
                ReportDate: ($scope.reportCollfile[i].ReportDate == undefined) ? 0 : $scope.reportCollfile[i].ReportDate,
                TriageDate: ($scope.reportCollfile[i].TriageDate == undefined) ? 0 : $scope.reportCollfile[i].TriageDate,
                ClockStart: ($scope.reportCollfile[i].ClockStart == undefined) ? 0 : $scope.reportCollfile[i].ClockStart,
            };
            exportAuditData.push(temp);
        }
        var xmls = fetchXML([exportAuditData]);

        var ctx = {
            created: new Date().getTime(),
            worksheets: xmls
        };

        var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML =
                '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
                '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
                '<Styles>' +
                '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
                '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
                '</Styles>' +
                '{worksheets}</Workbook>',
            tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
            tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
            base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                });
            };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'GeneralReport_' + new Date().getTime() + '.xls';
        //'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    function fetchXML(stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
            var worksheetName = 'Data';
            //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
            wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
            // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
            var rowXML = '';
            for (j = 0; j < stores[i].length; j++) {
                rowXML += '<Row>';
                var key = Object.keys(stores[0][0]);
                for (var k = 0; k < key.length; k++) {
                    rowXML += '<Cell><Data ss:Type="String">' + stores[i][j][key[k]] + '</Data></Cell>';
                }
                rowXML += '</Row>';
            }
            wrkbookXML += rowXML;
            wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
    }

});