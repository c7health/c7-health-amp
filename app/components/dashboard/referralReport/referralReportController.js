hCueDoctorWebControllers.controller('c7ReferralReportController', function($scope, $rootScope, $http, alertify, $window, $location, hcueLoginStatusService, hcueDoctorLoginService, referralregion, datacookieFactory, c7ReferralReportServices, c7AdminServices) {
     // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
   
    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
  
    if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined){
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    setTimeout(function () {
        $('[data-toggle="tooltip"]').tooltip();
    }, 500);
    
    $scope.hospitalID = 0;
    $scope.referralData = [];

    function scanCenterListing(){
        var param ={
            "pageNumber": 1,
            "HospitalID": parentHospitalId,
            "pageSize": 1000,
            "IncludeDoctors": "N",
            "IncludeBranch": "Y",
            "ActiveHospital": "Y",
            "ActiveDoctor": "Y"            
        }
        
        c7AdminServices.getHospitals(param,onGetHospitalsSuccess,listDashboardError);

    }
    
    function onGetHospitalsSuccess(data) {
        $scope.hospitalDoctorsList = data.rows;                       
    }

    function listDashboardError(data){

    }
    scanCenterListing();

    $scope.getReferralData = function(){
        var parm ={
            "Hospitalcode": (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != null 
              && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != undefined) ? 
              doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode : '' ,
            "hospitalid": parseInt($scope.hospitalID),
            "fromdate": $scope.daterange == undefined ?moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
            "todate": $scope.daterange == undefined ?moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
        }

        c7ReferralReportServices.getReferralReportInfo(parm, referralDataSuccess, referralDataError);
    }

    function referralDataSuccess(data){
        $scope.referralData = data;
    }

    function referralDataError(data){

    }
    
    $scope.exportTOExcel = function() {
        var exportAuditData = [
          {
            DWID: 'DW ID',
            Region: 'Region',
            CCG: 'CCG',
            PracticeName: 'Practice Name',
            ReferralDoctor: 'Referral Doctor',
            EnquirySource: 'Enquiry Source',
            ReferralCreated: 'Referral Created',
            ReferralReceived: 'Referral Received',
            ReferralProcessed: 'Referral Processed',
            AppointmentType: 'Appointment Type',
            Speciality: 'Speciality'
          }
        ];    
    
        for (var i = 0; i < $scope.referralData.length; i++) {

          var arr = [];
            var sdetails = '';
            if ($scope.referralData[i].specialitydetails.length > 0) {
                for (var x = 0; x < $scope.referralData[i].specialitydetails.length; x++) {
                    arr.push($scope.referralData[i].specialitydetails[x].speciality);
                }
            }
            sdetails = arr.join(', ');

          var temp = {
            DWID: $scope.referralData[i].DWID,
            Region: $scope.referralData[i].RegionName,
            CCG: $scope.referralData[i].CCGName,
            PracticeName: $scope.referralData[i].PracticeName,
            ReferralDoctor: $scope.referralData[i].ReferralDoctor,
            EnquirySource: $scope.referralData[i].EnquirySource,
            ReferralCreated: moment($scope.referralData[i].ReferralCreated).format('DD-MM-YYYY'),
            ReferralReceived: moment($scope.referralData[i].ReferralRecevied).format('DD-MM-YYYY'),
            ReferralProcessed: moment($scope.referralData[i].ReferralProcessed).format('DD-MM-YYYY'),
            AppointmentType: $scope.referralData[i].AppointmentType,
            Speciality: sdetails
          };
          exportAuditData.push(temp);     
        }
        var xmls = fetchXML([exportAuditData]);
    
        var ctx = {
          created: new Date().getTime(),
          worksheets: xmls
        };
    
        var uri = 'data:application/vnd.ms-excel;base64,',
          tmplWorkbookXML =
            '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
            '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
            '<Styles>' +
            '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
            '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
            '</Styles>' +
            '{worksheets}</Workbook>',
          tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
          tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
          base64 = function(s) {
            return window.btoa(unescape(encodeURIComponent(s)));
          },
          format = function(s, c) {
            return s.replace(/{(\w+)}/g, function(m, p) {
              return c[p];
            });
          };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'ReferralReport_' + new Date().getTime() + '.xls';
        //'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      };
    
      function fetchXML(stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
          var worksheetName = 'Data';
        //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
          wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
          // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
          var rowXML = '';    
          for (j = 0; j < stores[i].length; j++) {
            rowXML += '<Row>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].DWID + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Region + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].CCG + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PracticeName + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralDoctor + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].EnquirySource + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralCreated + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralReceived + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralProcessed + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentType + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Speciality + '</Data></Cell>';
            rowXML += '</Row>';
          }
          wrkbookXML += rowXML;
          wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
      }
   
});