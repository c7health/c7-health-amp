hCueDoctorWebControllers.controller('c7AgentStatsController', function ($scope, $rootScope, $http, alertify, $window, $location, datacookieFactory, hcueLoginStatusService, hcueDoctorLoginService, c7DashboardServices) {
    // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    $scope.agentCollection = [];
    $scope.date = moment().format('DD-MM-YYYY');
    $scope.reloaddata = function () {
        var param = {
            Hospitalcode: hospitalCode,
            date: moment($scope.date, 'DD-MM-YYYY').format('YYYY-MM-DD')
        }
        c7DashboardServices.getAgentstatsreportInfo(param, agentsuccess, error)
    }
    $scope.reloaddata();

    function agentsuccess(data) {
        $scope.agentCollection = data;

        var param = {
            Hospitalcode: hospitalCode,
            date: moment($scope.date, 'DD-MM-YYYY').format('YYYY-MM-DD')
        }
        c7DashboardServices.getAgentstatsreporttotalInfo(param, agenttotalsuccess, error);
    }

    function agenttotalsuccess(data) {
        data.forEach(function (i) {
            i.Name = 'Total';
        })
        $scope.agentCollection = $scope.agentCollection.concat(data);
    }

    function error(data) {
        alertify.error('error');
    }


    $scope.exportTOExcel = function () {
        var exportAuditData = [{
            Name: 'Name',
            NewActivity: 'Triage',
            Awaitingappointment: 'Awaiting Appointment',
            FirstbookingCall: '1st booking follow-up call',
            SecondbookingCall: '2nd booking follow-up call',
            thirdbookingCall: '3rd booking follow-up call',
            FinalCall: 'Final call',
            FinalCalllettersend: 'Letter Sent',
            BookAppointment: 'Book appointment',
            RescheduleAppointment: 'Reschedule appointment',
            ReportPublished: 'Report published',
            ReferralIssues: 'Resolve referral issue',
            ReferralReject: 'Referral rejected final',
            Callattempt: 'Call Attempt',
            ScheduleBookingCall: 'Schedule booking follow-up call',
            Latecancellation: 'Late Cancellation',
            Total: 'Total'
        }];

        for (var i = 0; i < $scope.agentCollection.length; i++) {
            var temp = {
                Name: $scope.agentCollection[i].Name,
                NewActivity: ($scope.agentCollection[i].NewActivity == undefined) ? 0 : $scope.agentCollection[i].NewActivity,
                Awaitingappointment: ($scope.agentCollection[i].Awaitingappointment == undefined) ? 0 : $scope.agentCollection[i].Awaitingappointment,
                FirstbookingCall: ($scope.agentCollection[i].FirstbookingCall == undefined) ? 0 : $scope.agentCollection[i].FirstbookingCall,
                SecondbookingCall: ($scope.agentCollection[i].SecondbookingCall == undefined) ? 0 : $scope.agentCollection[i].SecondbookingCall,
                thirdbookingCall: ($scope.agentCollection[i].thirdbookingCall == undefined) ? 0 : $scope.agentCollection[i].thirdbookingCall,
                FinalCall: ($scope.agentCollection[i].FinalCall == undefined) ? 0 : $scope.agentCollection[i].FinalCall,
                FinalCalllettersend: ($scope.agentCollection[i].FinalCalllettersend == undefined) ? 0 : $scope.agentCollection[i].FinalCalllettersend,
                BookAppointment: ($scope.agentCollection[i].BookAppointment == undefined) ? 0 : $scope.agentCollection[i].BookAppointment,
                RescheduleAppointment: ($scope.agentCollection[i].RescheduleAppointment == undefined) ? 0 : $scope.agentCollection[i].RescheduleAppointment,
                ReportPublished: ($scope.agentCollection[i].ReportPublished == undefined) ? 0 : $scope.agentCollection[i].ReportPublished,
                ReferralIssues: ($scope.agentCollection[i].ReferralIssues == undefined) ? 0 : $scope.agentCollection[i].ReferralIssues,
                ReferralReject: ($scope.agentCollection[i].ReferralReject == undefined) ? 0 : $scope.agentCollection[i].ReferralReject,
                Callattempt: ($scope.agentCollection[i].Callattempt == undefined) ? 0 : $scope.agentCollection[i].Callattempt,
                ScheduleBookingCall: ($scope.agentCollection[i].ScheduleBookingCall == undefined) ? 0 : $scope.agentCollection[i].ScheduleBookingCall,
                Latecancellation: ($scope.agentCollection[i].Latecancellation == undefined) ? 0 : $scope.agentCollection[i].Latecancellation,
                Total: ($scope.agentCollection[i].Total == undefined) ? 0 : $scope.agentCollection[i].Total
            };
            exportAuditData.push(temp);
        }
        var xmls = fetchXML([exportAuditData]);

        var ctx = {
            created: new Date().getTime(),
            worksheets: xmls
        };

        var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML =
                '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
                '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
                '<Styles>' +
                '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
                '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
                '</Styles>' +
                '{worksheets}</Workbook>',
            tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
            tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
            base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                });
            };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'AgentStatsReport_' + new Date().getTime() + '.xls';
        //'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    function fetchXML(stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
            var worksheetName = 'Data';
            //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
            wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
            // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
            var rowXML = '';
            for (j = 0; j < stores[i].length; j++) {
                rowXML += '<Row>';
                var key = Object.keys(stores[0][0]);
                for (var k = 0; k < key.length; k++) {
                    rowXML += '<Cell><Data ss:Type="String">' + stores[i][j][key[k]] + '</Data></Cell>';
                }
                rowXML += '</Row>';
            }
            wrkbookXML += rowXML;
            wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
    }

    function fetchXML1(obj, stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
            var worksheetName = 'Data';
            //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
            wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
            // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
            var rowXML = '';
            var objKeys = Object.keys(obj);
            for (var p = 0; p < objKeys.length; p++) {
                rowXML += '<Row><Cell><Data ss:Type="String">' + objKeys[p] + '</Data></Cell><Cell><Data ss:Type="String">' + obj[objKeys[p]] + '</Data></Cell></Row>';
            }
            rowXML += '<Row><Cell></Cell></Row>';
            rowXML += '<Row><Cell></Cell></Row>';
            rowXML += '<Row><Cell></Cell></Row>';
            rowXML += '<Row><Cell></Cell></Row>';


            for (j = 0; j < stores[i].length; j++) {
                rowXML += '<Row>';
                var key = Object.keys(stores[0][0]);
                for (var k = 0; k < key.length; k++) {
                    var dat = stores[i][j][key[k]];
                    if (dat === null || dat == undefined) {
                        dat = '-';
                    }
                    rowXML += '<Cell><Data ss:Type="String">' + dat + '</Data></Cell>';
                }
                rowXML += '</Row>';
            }
            wrkbookXML += rowXML;
            wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
    }

    $scope.exportcusbooking = function () {
        var cusbooking = {
            Hospitalcode: hospitalCode,
            fromdate: moment($scope.rangeSelect.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
            todate: moment($scope.rangeSelect.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
        }
        c7DashboardServices.getCustombookingreportInfo(cusbooking, cusbookingsuccess, error);

        function cusbookingsuccess(data) {
            var exportAuditData = [{
                Created: 'Created',
                PatientID: 'Patient ID',
                ReferralID: 'Referral ID',
                EventType: 'Event Type',
                FullName: 'Admin Contact'
            }];

            for (var i = 0; i < data.length; i++) {
                var temp = {
                    Created: data[i].Created,
                    PatientID: data[i].PatientID,
                    ReferralID: data[i].ReferralID,
                    EventType: data[i].EventType,
                    FullName: data[i].FullName
                };
                exportAuditData.push(temp);
            }
            var xx = {
                'Custom': 'Booking Report',
                'Executed on': moment().format('DD/MM/YYYY HH:mm'),
                'Executed by': userLoginDetails.displayName,
                'Date range': $scope.rangeSelect,
            }

            var xmls = fetchXML1(xx, [exportAuditData]);

            var ctx = {
                created: new Date().getTime(),
                worksheets: xmls
            };

            var uri = 'data:application/vnd.ms-excel;base64,',
                tmplWorkbookXML =
                    '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
                    '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
                    '<Styles>' +
                    '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
                    '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
                    '</Styles>' +
                    '{worksheets}</Workbook>',
                tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
                tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
                base64 = function (s) {
                    return window.btoa(unescape(encodeURIComponent(s)));
                },
                format = function (s, c) {
                    return s.replace(/{(\w+)}/g, function (m, p) {
                        return c[p];
                    });
                };
            var workbookXML = format(tmplWorkbookXML, ctx);
            var link = document.createElement('A');
            link.href = uri + base64(workbookXML);
            link.download = 'CustomBookingReport_' + new Date().getTime() + '.xls';
            //'Workbook.xls';
            link.target = '_blank';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }

    $scope.exportcusadmin = function () {
        var cusadmin = {
            Hospitalcode: hospitalCode,
            fromdate: moment($scope.rangeSelect.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
            todate: moment($scope.rangeSelect.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
        }
        c7DashboardServices.getCustomadminreportInfo(cusadmin, customadminsuccess, error);

        function customadminsuccess(data) {
            var exportAuditData = [{
                Created: 'Created',
                Pid: 'Patient ID',
                EventType: 'Event Type',
                AdminContact: 'Admin Contact',
                RepPublished: 'Report Published Date',
                RepRejected: 'Rejection Reason'
            }];

            for (var i = 0; i < data.length; i++) {
                var temp = {
                    Created: data[i].Created,
                    Pid: data[i].PatientID,
                    EventType: data[i].EventType,
                    AdminContact: data[i].FullName,
                    RepPublished: data[i].reportpublished,
                    RejReason: data[i].rejectreason
                };
                exportAuditData.push(temp);
            }
            var xx = {
                'Custom': 'Admin Events',
                'Executed on': moment().format('DD/MM/YYYY HH:mm'),
                'Executed by': userLoginDetails.displayName,
                'Date range': $scope.rangeSelect,
            }
            var xmls = fetchXML1(xx, [exportAuditData]);

            var ctx = {
                created: new Date().getTime(),
                worksheets: xmls
            };

            var uri = 'data:application/vnd.ms-excel;base64,',
                tmplWorkbookXML =
                    '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
                    '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
                    '<Styles>' +
                    '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
                    '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
                    '</Styles>' +
                    '{worksheets}</Workbook>',
                tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
                tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
                base64 = function (s) {
                    return window.btoa(unescape(encodeURIComponent(s)));
                },
                format = function (s, c) {
                    return s.replace(/{(\w+)}/g, function (m, p) {
                        return c[p];
                    });
                };
            var workbookXML = format(tmplWorkbookXML, ctx);
            var link = document.createElement('A');
            link.href = uri + base64(workbookXML);
            link.download = 'CustomAdminReport_' + new Date().getTime() + '.xls';
            //'Workbook.xls';
            link.target = '_blank';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);

        }
    }

});