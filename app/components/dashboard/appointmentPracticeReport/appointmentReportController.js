hCueDoctorWebControllers.controller('c7AppointmentReportController', function($scope, $rootScope, $http, alertify, $window, $location, hcueLoginStatusService, hcueDoctorLoginService, referralregion, datacookieFactory, c7AdminServices, c7AppointmentReportServices, referralpractice) {
    // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }
    setTimeout(function() {
        $('[data-toggle="tooltip"]').tooltip();
    }, 500);

    $scope.hospitalID = '0';
    $scope.practiceId = '0';
    $scope.appointmentData = [];

    function scanCenterListing() {
        var param = {
            "pageNumber": 1,
            "HospitalID": parentHospitalId,
            "pageSize": 1000,
            "IncludeDoctors": "N",
            "IncludeBranch": "Y",
            "ActiveHospital": "Y",
            "ActiveDoctor": "Y"
        }

        c7AdminServices.getHospitals(param, onGetHospitalsSuccess, listDashboardError);

    }

    function onGetHospitalsSuccess(data) {
        $scope.hospitalDoctorsList = data.rows;
    }

    function listDashboardError(data) {

    }
    scanCenterListing();
    $scope.getAppointmentData = function() {
        var parm = {
            "Hospitalcode": (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != null &&
                    doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != undefined) ?
                doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode : '',
            "hospitalid": parseInt($scope.hospitalID),
            "practiceid": parseInt($scope.practiceId),
            "fromdate": $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),
            "todate": $scope.daterange == undefined ? moment().format('YYYY-MM-DD') : moment($scope.daterange.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
        }

        c7AppointmentReportServices.getAppointmentReport(parm, appointmentDataSuccess, appointmentDataError)
    }

    function appointmentDataSuccess(data) {
        $scope.appointmentData = data;
    }

    function appointmentDataError(data) {

    }

    $scope.allpractice = [];
    var userParam = {
        "pageNumber": 0,
        "pageSize": 1000,
        "hospitalcd": hospitalCode,
        "usrid": doctorinfo.doctorid,
        "usrtype": "ADMIN",

    };

    referralpractice.listAllPracticeInfo(userParam, practicelistsuccess, appointmentDataError);


    function practicelistsuccess(data) {
        $scope.allpractice = data.filter(data => data.active);
    }

    $scope.exportTOExcel = function() {
        var exportAppointmentData = [{
            DWID: 'DW ID',
            PatientTitle: 'Patient Title',
            PatientForename: 'Patient Forename',
            PatientSurname: 'Patient Surname',
            PatientPostcode: 'Patient Postcode',
            NHSNumber: 'NHS Number',
            GPPracticeCode: 'GP Practice Code',
            GPPracticeName: 'GP Practice Name',
            Dateoftreatment: 'Date of treatment',
            SonographerName: 'Sonographer Name',
            ReferralReceived: 'Referral Received',
            Speciality: 'Speciality',
            SubSpeciality: 'Sub-Speciality',
            ScanCentrename: 'Location Name',
            DNAStatus: 'DNA Status',
            CCG: 'CCG',
            AppointmentStartTime: 'Appointment Start Time',
            AppointmentFinishTime: 'Appointment Finish Time',
            Duration: 'Duration',
            // LastCancellationReason: 'Last Cancellation Reason'
        }];

        for (var i = 0; i < $scope.appointmentData.length; i++) {
            var temp = {
                DWID: $scope.appointmentData[i].DWID,
                PatientTitle: $scope.appointmentData[i].Title != null ? $scope.appointmentData[i].Title : '',
                PatientForename: $scope.appointmentData[i].PatientForename != null ? $scope.appointmentData[i].PatientForename : '',
                PatientSurname: $scope.appointmentData[i].PatientSurname != null ? $scope.appointmentData[i].PatientSurname : '',
                PatientPostcode: $scope.appointmentData[i].PatientPostcode != null ? $scope.appointmentData[i].PatientPostcode : '',
                NHSNumber: $scope.appointmentData[i].NHSNumber != null ? $scope.appointmentData[i].NHSNumber : '',
                GPPracticeCode: $scope.appointmentData[i].PracticeCode != null ? $scope.appointmentData[i].PracticeCode : '',
                GPPracticeName: $scope.appointmentData[i].PracticeName != null ? $scope.appointmentData[i].PracticeName : '',
                Dateoftreatment: moment($scope.appointmentData[i].Dateoftreatment).format('DD-MM-YYYY'),
                SonographerName: $scope.appointmentData[i].SonographerName != null ? $scope.appointmentData[i].SonographerName : '',
                ReferralReceived: moment($scope.appointmentData[i].ReferralRecevied).format('DD-MM-YYYY'),
                Speciality: $scope.appointmentData[i].Speciality != null ? $scope.appointmentData[i].Speciality : '',
                SubSpeciality: $scope.appointmentData[i].SubSpeciality != null ? $scope.appointmentData[i].SubSpeciality : '',
                ScanCentrename: $scope.appointmentData[i].ScanCentrename != null ? $scope.appointmentData[i].ScanCentrename : '',
                DNAStatus: $scope.appointmentData[i].DNAStatus,
                CCG: $scope.appointmentData[i].CCGName != null ? $scope.appointmentData[i].CCGName : '',
                AppointmentStartTime: $scope.appointmentData[i].AppointmentStartTime,
                AppointmentFinishTime: $scope.appointmentData[i].AppointmentEndime,
                Duration: $scope.appointmentData[i].Duration,
                // LastCancellationReason: $scope.appointmentData[i].LastCancellationReason
            };
            exportAppointmentData.push(temp);
        }
        var xmls = fetchXML([exportAppointmentData]);

        var ctx = {
            created: new Date().getTime(),
            worksheets: xmls
        };

        var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML =
            '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
            '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
            '<Styles>' +
            '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
            '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
            '</Styles>' +
            '{worksheets}</Workbook>',
            tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
            tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
            base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                });
            };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'AppointmentPracticeReport_' + new Date().getTime() + '.xls';
        //'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    function fetchXML(stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
            var worksheetName = 'Data';
            //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
            wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
            // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
            var rowXML = '';
            for (j = 0; j < stores[i].length; j++) {
                rowXML += '<Row>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].DWID + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PatientTitle + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PatientForename + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PatientSurname + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PatientPostcode + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].NHSNumber + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].GPPracticeCode + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].GPPracticeName + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Dateoftreatment + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].SonographerName + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReferralReceived + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Speciality + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].SubSpeciality + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ScanCentrename + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].DNAStatus + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].CCG + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentStartTime + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentFinishTime + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Duration + '</Data></Cell>';
                // rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].LastCancellationReason + '</Data></Cell>';
                rowXML += '</Row>';
            }
            wrkbookXML += rowXML;
            wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
    }

});