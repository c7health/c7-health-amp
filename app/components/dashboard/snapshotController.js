﻿hCueDoctorWebControllers.controller('c7DashboardSnapshotController', function($scope, $rootScope, alertify, $window, $location, datacookieFactory, hcueDoctorLoginService, hcueLoginStatusService, c7DashboardServices, c7AdminServices) {
    //  Check login Params

    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    var parentHospitalId;
    if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined){
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
       
    }
    $scope.selectBranch = 0;  
    $scope.fromDate = moment().format('DD-MM-YYYY');  

    $scope.applyFilter = function(){
        
        var parm ={
            "Hospitalcode": (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != null 
                            && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != undefined) ? 
                            doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode : '' ,
            "hospitalId": parseInt($scope.selectBranch),
            "fromdate": moment($scope.fromDate, 'DD-MM-YYYY').format('YYYY-MM-DD'),
            "todate": moment($scope.fromDate, 'DD-MM-YYYY').format('YYYY-MM-DD')
        }
        c7DashboardServices.getEnquiryDashboardInfo(parm, listDashboardSuccess, listDashboardError)
    }

    function listDashboardSuccess(data){
        $scope.dashboardCollection = data;        
    }    

    function listDashboardError(data){

    }

    $scope.applyFilter();
})