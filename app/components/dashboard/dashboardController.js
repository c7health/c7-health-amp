﻿hCueDoctorWebControllers.controller('c7DashboardController', function ($scope, $rootScope, alertify, $window, $location, datacookieFactory, hcueDoctorLoginService, hcueLoginStatusService) {
    //  Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }

    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    var access = doctorinfo.isAccessRightsApplicable

    $scope.showDashboardAccess = !access;
    $scope.showCostAnalyticsAccess = !access;
    $scope.showCostAccess = !access;
    $scope.ForecastshowCostAccess = !access;
    $scope.showDailyAccess = !access;
    $scope.showReferralAccess = !access;
    $scope.showAppointmentAccess = !access;
    $scope.showClinicalAccess = !access;
    $scope.showPatientAccess = !access;
    $scope.showReferralDataAccess = !access;
    $scope.showReportAccess = !access;
    $scope.showGeneralReportAccess = !access;
    $scope.showExceptionNotesReportAccess = !access;


    if (access) {
        if (doctorinfo.AccountAccessID.indexOf('LSTDASHBRD') !== -1) {
            $scope.showDashboardAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('COSTANALYTIC') !== -1) {
            $scope.showCostAnalyticsAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('COSTRPT') !== -1) {
            $scope.showCostAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('COSTRPT') !== -1) {
            $scope.ForecastshowCostAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('DAILYRPT') !== -1) {
            $scope.showDailyAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('REFERRALRPT') !== -1) {
            $scope.showReferralAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('APPPRARPT') !== -1) {
            $scope.showAppointmentAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('CLINICAUDIT') !== -1) {
            $scope.showClinicalAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('PATIENTRPT') !== -1) {
            $scope.showPatientAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('REFDATAWOIND') !== -1) {
            $scope.showReferralDataAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('GPPRACIND') !== -1) {
            $scope.showReportAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('AGENTRPT') !== -1) {
            $scope.showAgentstatsReportAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('GENERALRPT') !== -1) {
            $scope.showGeneralReportAccess = true;
        }
        if (doctorinfo.AccountAccessID.indexOf('EXCEPTNOTES') !== -1) {
            $scope.showExceptionNotesReportAccess = true;
        }       
    }


    if ($scope.showDashboardAccess == true) {
        $scope.selectedValue = 'Snapshot';
    } else if ($scope.showCostAnalyticsAccess == true) {
        $scope.selectedValue = 'CostAnalyticsReport';
    } else if ($scope.showCostAccess == true) {                    
        $scope.selectedValue = 'Report';
    } else if ($scope.ForecastshowCostAccess == true) {
        $scope.selectedValue = 'ForecastReport';
    } else if ($scope.showDailyAccess == true) {
        $scope.selectedValue = 'DailyReport';
    } else if ($scope.showReferralAccess == true) {
        $scope.selectedValue = 'ReferralReport';
    } else if ($scope.showAppointmentAccess == true) {
        $scope.selectedValue = 'AppointmentReport';
    } else if ($scope.showClinicalAccess == true) {
        $scope.selectedValue = 'AuditCriteria';
    } else if ($scope.showPatientAccess == true) {
        $scope.selectedValue = 'PatientsinSystemReport';
    } else if ($scope.showReferralDataAccess == true) {
        $scope.selectedValue = 'ReferralDataWOIndication';
    } else if ($scope.showReportAccess == true) {
        $scope.selectedValue = 'AppointmentsWithIndication';
    } else if ($scope.showAgentstatsReportAccess == true) {
        $scope.selectedValue = 'AgentStats';
    } else if ($scope.showGeneralReportAccess == true) {
        $scope.selectedValue = 'generalreport';
    } else if ($scope.showExceptionNotesReportAccess == true) {
        $scope.selectedValue = 'ExceptionNotes';
    }
})