﻿hCueDoctorWebControllers.controller('doctorlistController', ['$scope', '$window', '$filter', 'hcueDoctorLoginService', 'alertify', 'hCueSmsService', 'hcueLoginStatusService', 'referraldoctor', 'datacookieFactory', '$rootScope', 'referralpractice', function($scope, $window, $filter, hcueDoctorLoginService, alertify, hCueSmsService, hcueLoginStatusService, referraldoctor, datacookieFactory, $rootScope, referralpractice) {

    $scope.toggleModal = function(id) {
        $scope.modalId = id;
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    var hospitalcd
    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined) {
        hospitalcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
    }
    var userparam = {
        "pageNumber": 0,
        "pageSize": 1000,
        "hospitalcd": hospitalcd,
        "usrid": doctorinfo.doctorid,
        "usrtype": "ADMIN"
    }
    var doctorparam = {
        "pageNumber": 0,
        "pageSize": 10,
        "hospitalcd": hospitalcd,
        "usrid": doctorinfo.doctorid,
        "usrtype": "ADMIN"
    }
    $scope.searchexteranlparactice = '';
    $scope.alldoctor = [];
    $scope.allpractice = [];
    $scope.declaredoctorVariables = function() {

        $scope.activePractices = [];
        $scope.doctoredit = false;
        $scope.doctorerrormessage = false;
        $scope.addupdatedoctor = {
            "doctorDetails": {
                "Title": "",
                "FirstName": "",
                "FullName": "",
                "LastName": "",
                "Gender": "M",
                "SpecialityCD": {},
                "TermsAccepted": "N",
                "Prospect": "Y",
                "DOB": null
            },
            "DoctorPhone": [{
                "PhNumber": '',
                "PhType": "D",
                "PrimaryIND": "Y"
            }, {
                "PhNumber": '',
                "PhType": "E",
                "PrimaryIND": "Y"
            }, {
                "PhNumber": '',
                "PhType": "M",
                "PrimaryIND": "Y"
            }],
            "DoctorEmail": [{
                "EmailID": "",
                "EmailIDType": "P",
                "PrimaryIND": "Y"
            }],
            "doctorOtherDetails": {
                "faxno": '',
                "gpcode": "",
                "typevalue": "",
                "notes": ""
            },
            "PractiesDetails": []
        }

    }

    $scope.doctoredit = false;

    $scope.getdoctorList = function() {
        $scope.pageNumberDoctor = 1;
        doctorparam['searchtext'] = '';
        referraldoctor.newlistdoctor(doctorparam, doctorlistsuccess, doctorerror);
    }
    $scope.updateFilter = function() {
        if ($scope.searchDoc.length == 0 || $scope.searchDoc.length > 3) {
            doctorparam['searchtext'] = $scope.searchDoc;
            referraldoctor.newlistdoctor(doctorparam, doctorlistsuccess, doctorerror);
        }
    }

    function doctorlistsuccess(data) {
        if (data.length < 10) {
            $scope.pageNumberDoctor = undefined;
        } else if (data.length == 10 && $scope.pageNumberDoctor == undefined) {
            $scope.pageNumberDoctor = 1;
        }
        $scope.alldoctor = data;
        setTimeout(function() {
            $('[data-toggle="tooltip"]').tooltip();
        }, 500);
    }

    $rootScope.$on('refeeralpagenav', function() {
        $scope.tabno = datacookieFactory.getItem('referralteamtab');
        if ($scope.tabno == 4) {
            $scope.getdoctorList();
            $scope.getpracticeList();
            $scope.searchDoc = null;
        }
    });

    $scope.getpracticeList = function() {
        referralpractice.listAllPracticeInfo(userparam, practicelistsuccess, doctorerror);
    }

    function practicelistsuccess(data) {
        $scope.allpractice = data;
    }


    $scope.adddoctor = function() {
        if ($scope.addupdatedoctor.doctorDetails.Title == '' || $scope.addupdatedoctor.doctorDetails.FirstName == '' || $scope.addupdatedoctor.doctorDetails.LastName == '' || $scope.addupdatedoctor.doctorOtherDetails.gpcode == '') {
            $scope.doctorerrormessage = true;
            return;
        }

        if ($scope.newcount.length == 0) {
            $scope.doctorerrormessage = true;
            return;
        }
        $scope.doctorerrormessage = false;
        fillinfo()
        $scope.addupdatedoctor.USRId = doctorinfo.doctorid;
        $scope.addupdatedoctor.USRType = 'ADMIN';
        $scope.addupdatedoctor.hospitalcd = hospitalcd;
        $scope.addupdatedoctor.isPracticeDoctor = true;
        $scope.addupdatedoctor.doctorDetails.DOB = $scope.addupdatedoctor.doctorDetails.DOB != null ? moment($scope.addupdatedoctor.doctorDetails.DOB, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;
        $scope.addupdatedoctor.DoctorPhone[0].PhNumber = $scope.addupdatedoctor.DoctorPhone[0].PhNumber;
        $scope.addupdatedoctor.DoctorPhone[1].PhNumber = $scope.addupdatedoctor.DoctorPhone[1].PhNumber;
        $scope.addupdatedoctor.DoctorPhone[2].PhNumber = $scope.addupdatedoctor.DoctorPhone[2].PhNumber;
        $scope.addupdatedoctor.doctorOtherDetails.faxno = $scope.addupdatedoctor.doctorOtherDetails.faxno;
        $scope.addupdatedoctor.PractiesDetails = $scope.PractiesDetails;
        $scope.addupdatedoctor.DoctorEmail = $scope.addupdatedoctor.DoctorEmail[0].EmailID != '' ? $scope.addupdatedoctor.DoctorEmail : undefined;

        referraldoctor.newadddoctor($scope.addupdatedoctor, doctorAddSuccess, doctorerror);
    }

    function doctorAddSuccess(data) {
        if (!data.error) {
            if (data != 'ALREADY_EMAIL_EXISTS') {
                alertify.success("Doctor added successfully");
                $('#AddEditDoctor1').modal('hide');
                $('#deleteDoctorPopupWindow1').modal('hide');
                $scope.getdoctorList();
            } else if (data == 'ALREADY_EMAIL_EXISTS') {
                alertify.error('Email already exist')
            }

        } else {
            if (data.error.message.includes('already exists') && data.error.message.includes('Key (\"EmailID\")')) {
                alertify.error('Email already exist')
            } else if (data.error.message.includes('ALREADY_EMAIL_EXISTS')) {
                alertify.error('Email already exist')
            } else {
                alertify.error('Error in adding doctor')
            }
        }
    }

    $scope.updatedoctor = function() {
        if ($scope.addupdatedoctor.doctorDetails.Title == '' || $scope.addupdatedoctor.doctorDetails.FirstName == '' || $scope.addupdatedoctor.doctorDetails.LastName == '' || $scope.addupdatedoctor.doctorOtherDetails.gpcode == '') {
            $scope.doctorerrormessage = true;
            return;
        }

        if ($scope.newcount.length == 0) {
            $scope.doctorerrormessage = true;
            return;
        }
        $scope.doctorerrormessage = false;
        fillinfo()
        $scope.addupdatedoctor.USRId = doctorinfo.doctorid;
        $scope.addupdatedoctor.USRType = 'ADMIN';
        $scope.addupdatedoctor.hospitalcd = hospitalcd;
        $scope.addupdatedoctor.isPracticeDoctor = true;
        $scope.addupdatedoctor.doctorDetails.DOB = $scope.addupdatedoctor.doctorDetails.DOB != null ? moment($scope.addupdatedoctor.doctorDetails.DOB, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;
        $scope.addupdatedoctor.DoctorPhone[0].PhNumber = $scope.addupdatedoctor.DoctorPhone[0].PhNumber
        $scope.addupdatedoctor.DoctorPhone[1].PhNumber = $scope.addupdatedoctor.DoctorPhone[1].PhNumber
        $scope.addupdatedoctor.DoctorPhone[2].PhNumber = $scope.addupdatedoctor.DoctorPhone[2].PhNumber
        $scope.addupdatedoctor.doctorOtherDetails.faxno = $scope.addupdatedoctor.doctorOtherDetails.faxno;
        $scope.addupdatedoctor.PractiesDetails = $scope.PractiesDetails;
        $scope.addupdatedoctor.DoctorEmail = $scope.addupdatedoctor.DoctorEmail[0].EmailID != '' ? $scope.addupdatedoctor.DoctorEmail : undefined;

        referraldoctor.newupdateDoctor($scope.addupdatedoctor, doctorUpdateSuccess, doctorerror);
    }

    function doctorUpdateSuccess(data) {
        if (!data.error) {
            alertify.success("Doctor updated successfully");
            $('#AddEditDoctor1').modal('hide');
            $('#deleteDoctorPopupWindow1').modal('hide');
            $scope.getdoctorList();
        } else {
            if (data.error.message.includes('already exists') && data.error.message.includes('Key (\"EmailID\")')) {
                alertify.error('Emailid allready added')
            } else {
                alertify.error('Error in adding doctor')
            }
        }
    }



    $scope.getdoctorToEdit = function(data) {
        $scope.doctorerrormessage = false;
        referraldoctor.getdoctorDetails(data, editdoctorSuccess, doctorerror);
    }


    function editdoctorSuccess(data) {
        $scope.declaredoctorVariables();
        $scope.doctoredit = true;
        $scope.doctorcollection = angular.copy(data);
        $scope.addupdatedoctor.doctorDetails = $scope.doctorcollection.doctor[0];
        $scope.addupdatedoctor.doctorDetails.FirstName = $scope.doctorcollection.doctorOtherDetails.FirstName;
        $scope.addupdatedoctor.doctorDetails.LastName = $scope.doctorcollection.doctorOtherDetails.LastName;
        $scope.addupdatedoctor.doctorDetails.DOB = $scope.addupdatedoctor.doctorDetails.DOB != undefined ? moment($scope.addupdatedoctor.doctorDetails.DOB).format('DD-MM-YYYY') : null;
        for (var i = 0; i < $scope.doctorcollection.doctorPhone.length; i++) {
            $scope.addupdatedoctor.DoctorPhone.filter(v => v.PhType == $scope.doctorcollection.doctorPhone[i].PhType).map(k => k.PhNumber = $scope.doctorcollection.doctorPhone[i].PhNumber);
        }
        $scope.addupdatedoctor.DoctorEmail = $scope.doctorcollection.doctorEmail != undefined ? $scope.doctorcollection.doctorEmail : $scope.addupdatedoctor.DoctorEmail;
        $scope.addupdatedoctor.doctorOtherDetails = $scope.doctorcollection.doctorOtherDetails;

        $scope.activePractices = [];
        angular.forEach($scope.allpractice, function(i) {
            angular.forEach($scope.doctorcollection.PractiesDetails, function(k) {
                if (i.practiceid == k.practiceid) {
                    i.isActive = true;
                    $scope.activePractices.push(i);
                }
            })
        })
        $($scope.modalId).modal('show');
        $scope.modalId = '';

    }

    $scope.deletedoctor = function(list, value) {
        $($scope.modalId).modal('show');
        $scope.modalId = '';
        $scope.doctoractive = value;
        $scope.deletedoctorid = parseFloat(list.doctorid);
    }

    $scope.deleteDoctorpopupclick = function(activate) {
        $scope.activatenew = activate;

        var param = {
                "practiceid": 0,
                "doctorid": $scope.deletedoctorid,
                "usrid": doctorinfo.doctorid,
                "usrtype": "ADMIN",
                "hospitalcd": hospitalcd
            }
            // if (activate) {
        param.active = activate;
        // }
        referraldoctor.deactivedoctorPractice(param, doctorAddUpdateSuccess, doctorerror)
    }

    function doctorAddUpdateSuccess(data) {

        if ($scope.activatenew == true) {
            alertify.success("Doctor activated successfully");
        } else {
            alertify.success("Doctor inactivated successfully")
        }
        $('#AddEditDoctor1').modal('hide');
        $('#deleteDoctorPopupWindow1').modal('hide');
        $scope.getdoctorList();
    }


    function doctorerror(data) {
        alertify.log("Error in connectivity");
    }

    function fillinfo() {
        $scope.PractiesDetails = [];
        angular.forEach($scope.activePractices, function(j) {
            if (j.isActive == true) {
                j.postcode = j.postcode.toString();
                $scope.PractiesDetails.push(j);
            }

        })
    }
    $scope.externalpractices = function() {
        $scope.allpractice.selected.isActive = true;
        var available = false;
        if ($scope.activePractices.length > 0) {
            for (var i = 0; i < $scope.activePractices.length; i++) {
                if ($scope.activePractices[i].practiceid == $scope.allpractice.selected.practiceid) {
                    available = true;
                    break;
                }
            }
        }
        if (!available) {
            $scope.activePractices.push($scope.allpractice.selected);
            $scope.allpractice.selected = {};
        }
    }
    $scope.loadmoreDoctor = function() {
        var userDoctorParam = {
            "pageNumber": $scope.pageNumberDoctor,
            "pageSize": 10,
            "usrid": doctorinfo.doctorid,
            "usrtype": "ADMIN",
            "hospitalcd": hospitalcd
        };
        $scope.pageNumberDoctor = $scope.pageNumberDoctor + 1;
        userDoctorParam['searchtext'] = ($scope.searchDoc != undefined && $scope.searchDoc.length > 3) ? $scope.searchDoc : '';

        referraldoctor.newlistdoctor(userDoctorParam, doctorloadlistsuccess, doctorerror);

        function doctorloadlistsuccess(data) {
            if (data.length < 10) {
                $scope.pageNumberDoctor = undefined;
            }
            angular.forEach(data, function(i) {
                $scope.alldoctor.push(i)
            });
        }
    }

}]);