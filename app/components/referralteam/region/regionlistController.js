﻿hCueDoctorWebControllers.controller('regionController', ['$scope', '$window', '$filter', 'hcueDoctorLoginService', 'alertify', 'hCueSmsService', 'hcueLoginStatusService', 'referralregion', 'datacookieFactory', '$rootScope', 'referralccg', 'c7AdminServices', function($scope, $window, $filter, hcueDoctorLoginService, alertify, hCueSmsService, hcueLoginStatusService, referralregion, datacookieFactory, $rootScope, referralccg, c7AdminServices) {

    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    var hospitalcd
    var ParentHospitalID
    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined) {
        hospitalcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        ParentHospitalID = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;

    }

    $scope.allRegion = [];
    $scope.toggleModal = function(id) {
        // $(id).modal('show');
        $scope.modalId = id;
    }


    // Region
    $scope.declareRegionVariables = function() {
        $scope.regionedit = false;
        $scope.regionerrormessage = false;
        $scope.addupdateregion = {
            active: true,
            regionname: '',
            contactperson: '',
            contactnumber: '',
            email: '',
            postcode: '',
            SlaRecord: {
                slavalidfrom: '',
                slavalidto: '',
                routinereftriage: 0,
                urgentreftriage: 0,
                dvtreftriage: 0,
                urgentrefdatebook: 0,
                dvtrefdatebook: 0,
                routinerefscan: 0,
                urgentrefscan: 0,
                dvtrefscan: 0,
                routineappsendback: 0,
                urgentscanrepsendback: 0,
                dvtscanrepsendback: 0,
            }
        }
    }

    $scope.showregionccg = undefined;
    $scope.regionedit = false;
    $scope.updateslaedit = undefined;
    var userParam = {
        "pageNumber": 0,
        "pageSize": 10,
        "usrid": doctorinfo.doctorid,
        "usrtype": "ADMIN",
        "hospitalcd": hospitalcd
    };
    //region all list start
    $scope.getRegionList = function() {
        $scope.pageNumber = 1;
        delete userParam['regionid'];
        userParam['searchtext'] = '';
        referralregion.alllistRegion(userParam, regionlistsuccess, regionerror);
    }
    $scope.updateFilter = function() {
        if ($scope.searchRegion.length == 0 || $scope.searchRegion.length > 3) {
            userParam['searchtext'] = $scope.searchRegion;
            referralregion.alllistRegion(userParam, regionlistsuccess, regionerror);
        }
    }
    $rootScope.$on('refeeralpagenav', function() {
        $scope.tabno = datacookieFactory.getItem('referralteamtab');
        if ($scope.tabno == 1) {
            $scope.getRegionList();
            $scope.searchRegion = null;
        }
    });
    $scope.tabno = datacookieFactory.getItem('referralteamtab');
    if ($scope.tabno == 1) {
        $scope.getRegionList();
    }

    function regionlistsuccess(data) {
        if (data.length < 10) {
            $scope.pageNumber = undefined;
        } else if (data.length == 10 && $scope.pageNumber == undefined) {
            $scope.pageNumber = 1;
        }
        $scope.allRegion = data;
        angular.forEach($scope.allRegion, function(item) {
            item.ckbox = item.active;
        });
        setTimeout(function() {
            $('[data-toggle="tooltip"]').tooltip();
        }, 500);
    }

    //region all list end

    //region add update delete start
    $scope.addRegion = function() {
        if ($scope.addupdateregion.regionname == '' || $scope.addupdateregion.contactperson == '' || $scope.addupdateregion.contactnumber == '' || $scope.addupdateregion.SlaRecord.routinereftriage == null || $scope.addupdateregion.SlaRecord.urgentreftriage == null || $scope.addupdateregion.SlaRecord.dvtreftriage == null || $scope.addupdateregion.SlaRecord.urgentrefdatebook == null || $scope.addupdateregion.SlaRecord.dvtrefdatebook == null) {
            $scope.regionerrormessage = true;
            return;
        }
        if ($scope.addupdateregion.SlaRecord.routinerefscan == null || $scope.addupdateregion.SlaRecord.urgentrefscan == null || $scope.addupdateregion.SlaRecord.dvtrefscan == null || $scope.addupdateregion.SlaRecord.routineappsendback == null || $scope.addupdateregion.SlaRecord.urgentscanrepsendback == null || $scope.addupdateregion.SlaRecord.dvtscanrepsendback == null) {
            $scope.regionerrormessage = true;
            return;
        }
        if ($scope.addupdateregion.SlaRecord.slavalidfrom == '' || $scope.addupdateregion.SlaRecord.slavalidto == '' || $scope.addupdateregion.email == '') {
            $scope.regionerrormessage = true;
            return;
        }
        if (moment($scope.addupdateregion.SlaRecord.slavalidfrom, 'DD-MM-YYYY').isBefore(moment($scope.addupdateregion.SlaRecord.slavalidto, 'DD-MM-YYYY'))) {
            $scope.addupdateregion.postcode = ($scope.addupdateregion.postcode != '') ? $scope.addupdateregion.postcode.toUpperCase() : '';

            $scope.regionerrormessage = false;
            $scope.addupdateregion.usrid = doctorinfo.doctorid;
            $scope.addupdateregion.usrtype = 'ADMIN';
            $scope.addupdateregion.hospitalcd = hospitalcd;

            var addRegion = angular.copy($scope.addupdateregion);
            addRegion.SlaRecord.slavalidfrom = moment($scope.addupdateregion.SlaRecord.slavalidfrom, 'DD-MM-YYYY').format('YYYY-MM-DD');
            addRegion.SlaRecord.slavalidto = moment($scope.addupdateregion.SlaRecord.slavalidto, 'DD-MM-YYYY').format('YYYY-MM-DD');

            referralregion.addRegion(addRegion, regionAddSuccess, regionerror);
        } else {
            alertify.error('Choose To Date greater than From Date');
        }
    }

    function regionAddSuccess(data) {
        if (data == 'Success') {
            alertify.success("Region added successfully")
            $('#AddEditRegion').modal('hide');
            $('#deleteRegionPopupWindow').modal('hide');
            $scope.getRegionList()
        } else {
            alertify.error('Emailid allready added')
        }
    }

    $scope.getRegionToEdit = function(data) {
        $scope.regionerrormessage = false;
        userParam.regionid = data;
        referralregion.getRegionDetails(userParam, editRegionSuccess, regionerror);
    }


    function editRegionSuccess(data) {
        $scope.regionedit = true;
        $scope.addupdateregion = angular.copy(data[0]);
        if ($scope.addupdateregion.SlaRecord.length != 0) {
            $scope.addupdateregion.SlaRecord = $scope.addupdateregion.SlaRecord[$scope.addupdateregion.SlaRecord.length - 1];
            $scope.addupdateregion.SlaRecord.slavalidfrom = moment($scope.addupdateregion.SlaRecord.slavalidfrom).format('DD-MM-YYYY');
            $scope.addupdateregion.SlaRecord.slavalidto = moment($scope.addupdateregion.SlaRecord.slavalidto).format('DD-MM-YYYY');
        } else {
            $scope.addupdateregion.SlaRecord = {
                slavalidfrom: '',
                slavalidto: '',
                routinereftriage: 0,
                urgentreftriage: 0,
                dvtreftriage: 0,
                urgentrefdatebook: 0,
                dvtrefdatebook: 0,
                routinerefscan: 0,
                urgentrefscan: 0,
                dvtrefscan: 0,
                routineappsendback: 0,
                urgentscanrepsendback: 0,
                dvtscanrepsendback: 0,
            }
        }
        $scope.currentsla = angular.copy(data[0].SlaRecord);
        $scope.expiredsla = $scope.addupdateregion.ExpiredSlaRecord;
        $($scope.modalId).modal('show');
        $scope.modalId = '';
    }

    $scope.newSla = function() {
        delete $scope.addupdateregion.SlaRecord['slaid'];

        $scope.addupdateregion.SlaRecord.routinereftriage = '';
        $scope.addupdateregion.SlaRecord.urgentreftriage = '';
        $scope.addupdateregion.SlaRecord.dvtreftriage = '';
        $scope.addupdateregion.SlaRecord.urgentrefdatebook = '';
        $scope.addupdateregion.SlaRecord.dvtrefdatebook = '';
        $scope.addupdateregion.SlaRecord.routinerefscan = '';
        $scope.addupdateregion.SlaRecord.urgentrefscan = '';
        $scope.addupdateregion.SlaRecord.dvtrefscan = '';
        $scope.addupdateregion.SlaRecord.routineappsendback = '';
        $scope.addupdateregion.SlaRecord.urgentscanrepsendback = '';
        $scope.addupdateregion.SlaRecord.dvtscanrepsendback = '';
    }

    $scope.updateRegion = function() {
        if ($scope.addupdateregion.regionname == '' || $scope.addupdateregion.contactperson == '' || $scope.addupdateregion.contactnumber == '' || $scope.addupdateregion.SlaRecord.routinereftriage == null || $scope.addupdateregion.SlaRecord.urgentreftriage == null || $scope.addupdateregion.SlaRecord.dvtreftriage == null || $scope.addupdateregion.SlaRecord.urgentrefdatebook == null || $scope.addupdateregion.SlaRecord.dvtrefdatebook == null) {
            $scope.regionerrormessage = true;
            return;
        }
        if ($scope.addupdateregion.SlaRecord.routinerefscan == null || $scope.addupdateregion.SlaRecord.urgentrefscan == null || $scope.addupdateregion.SlaRecord.dvtrefscan == null || $scope.addupdateregion.SlaRecord.routineappsendback == null || $scope.addupdateregion.SlaRecord.urgentscanrepsendback == null || $scope.addupdateregion.SlaRecord.dvtscanrepsendback == null) {
            $scope.regionerrormessage = true;
            return;
        }
        if ($scope.addupdateregion.SlaRecord.slavalidfrom == '' || $scope.addupdateregion.SlaRecord.slavalidto == '' || !$scope.addupdateregion.email) {
            $scope.regionerrormessage = true;
            return;
        }
        if (moment($scope.addupdateregion.SlaRecord.slavalidfrom, 'DD-MM-YYYY').isBefore(moment($scope.addupdateregion.SlaRecord.slavalidto, 'DD-MM-YYYY'))) {
            $scope.addupdateregion.postcode = ($scope.addupdateregion.postcode != '') ? $scope.addupdateregion.postcode.toUpperCase() : '';
            $scope.regionerrormessage = false;
            $scope.addupdateregion.usrid = doctorinfo.doctorid;
            $scope.addupdateregion.usrtype = 'ADMIN';
            $scope.addupdateregion.active = true;
            $scope.addupdateregion.hospitalcd = hospitalcd;

            var uptRegion = angular.copy($scope.addupdateregion);
            uptRegion.SlaRecord.slavalidfrom = moment($scope.addupdateregion.SlaRecord.slavalidfrom, 'DD-MM-YYYY').format('YYYY-MM-DD');
            uptRegion.SlaRecord.slavalidto = moment($scope.addupdateregion.SlaRecord.slavalidto, 'DD-MM-YYYY').format('YYYY-MM-DD');

            referralregion.updateRegion(uptRegion, regionUpdateSuccess, regionerror);
        } else {
            alertify.error('Choose To Date greater than From Date');
        }
    }

    function regionUpdateSuccess(data) {
        if (data == 'Success') {
            alertify.success("Region updated successfully")
            $('#AddEditRegion').modal('hide');
            $('#deleteRegionPopupWindow').modal('hide');
            $scope.getRegionList()
        } else {
            alertify.error('Emailid allready added')
        }
    }

    $scope.deleteRegion = function(data, value) {
        $($scope.modalId).modal('show');
        $scope.modalId = '';
        userParam.regionid = data;
        $scope.activeregion = value;
        referralregion.getRegionDetails(userParam, editRegionSuccess, regionerror);
    }

    $scope.deletepopupRegion = function() {
        $scope.addupdateregion.usrid = doctorinfo.doctorid;
        $scope.addupdateregion.usrtype = 'ADMIN';
        $scope.addupdateregion.hospitalcd = hospitalcd;
        $scope.addupdateregion.active = $scope.activeregion == true ? false : true;

        var delRegion = angular.copy($scope.addupdateregion);

        if ($scope.addupdateregion.SlaRecord.slavalidfrom == "") {
            delRegion.SlaRecord.slavalidfrom = moment($scope.addupdateregion.ExpiredSlaRecord[0].slavalidfrom).format('YYYY-MM-DD');
        } else {
            delRegion.SlaRecord.slavalidfrom = moment($scope.addupdateregion.SlaRecord.slavalidfrom, 'DD-MM-YYYY').format('YYYY-MM-DD');
        }
        if ($scope.addupdateregion.SlaRecord.slavalidto == "") {
            delRegion.SlaRecord.slavalidto = moment($scope.addupdateregion.ExpiredSlaRecord[0].slavalidto).format('YYYY-MM-DD');
        } else {
            delRegion.SlaRecord.slavalidto = moment($scope.addupdateregion.SlaRecord.slavalidto, 'DD-MM-YYYY').format('YYYY-MM-DD');
        }

        referralregion.updateRegion(delRegion, regionAddUpdateSuccess, regionerror);
    }


    function regionAddUpdateSuccess(data) {
        if ($scope.activeregion == false) {
            alertify.success("Region activated successfully");
        } else {
            alertify.success("Region inactivated successfully")
        }
        $('#AddEditRegion').modal('hide');
        $('#deleteRegionPopupWindow').modal('hide');
        $scope.getRegionList()
    }

    $scope.toggleSubRegion = function(data) {

        if ($scope.showregionccg == data) {
            $scope.showregionccg = undefined;
        } else {
            $scope.showregionccg = data;
        }
    }


    //ccg
    var ccgParam = {
        "pageNumber": 0,
        "pageSize": 1000,
        "hospitalcd": hospitalcd,
        "usrid": doctorinfo.doctorid,
        "usrtype": "ADMIN"
    };
    $scope.declareccgVariables = function() {
        $scope.ccgedit = false;
        $scope.ccgerrormessage = false;
        $scope.addupdateccg = {
            active: true,
            regionid: '',
            ccgname: '',
            ccgreference: '',
            primarycontactperson: '',
            primarycontactno: '',
            primarycontactemail: '',
            officesserved: '',
            emponsystem: '',
            localcontact: '',
            assistant: '',
            invoice: '',
            contractrenewalremain: null,
            paymentterms: '',
            Contractdetails: {
                contactdetailsid: undefined,
                contractstartdate: '',
                contractenddate: '',
                minimunageforref: null,
                lessthanminutes: undefined,
                greaterthanminutes: undefined,
                tariffinfodvt: '',
                speciality: {},
                expiredstatus: true,
                Contractspeciality: [{
                    "Speciality": '',
                    "lessthanminutes": null,
                    "greaterthanminutes": null,
                    "fixedfee": null,
                    "active": true
                }]
            }
        }
    }


    $scope.getccgToEdit = function(data) {
        $scope.ccgerrormessage = false;
        ccgParam.ccgid = data;
        referralccg.getccgDetails(ccgParam, editccgSuccess, regionerror);
    }


    function editccgSuccess(data) {

        $scope.ccgedit = true;
        $scope.addupdateccg = angular.copy(data[0]);
        $scope.addupdateccg.regionid = $scope.addupdateccg.regionid.toString()
        $scope.addupdateccg.contractrenewalremain = $scope.addupdateccg.contractrenewalremain == undefined ? moment($scope.addupdateccg.contractrenewalremain).format('DD-MM-YYYY') : null;
        if ($scope.addupdateccg.Contractdetails.length != 0) {
            $scope.addupdateccg.Contractdetails = $scope.addupdateccg.Contractdetails[$scope.addupdateccg.Contractdetails.length - 1];
            $scope.addupdateccg.Contractdetails.contractstartdate = $scope.addupdateccg.Contractdetails.contractstartdate == '' ? '' : moment($scope.addupdateccg.Contractdetails.contractstartdate).format('DD-MM-YYYY');
            $scope.addupdateccg.Contractdetails.contractenddate = $scope.addupdateccg.Contractdetails.contractenddate == '' ? '' : moment($scope.addupdateccg.Contractdetails.contractenddate).format('DD-MM-YYYY');
            $scope.currentcontract = angular.copy($scope.addupdateccg.Contractdetails);
        } else {
            $scope.currentcontract = [];
            $scope.addupdateccg.Contractdetails = {
                contactdetailsid: undefined,
                contractstartdate: '',
                contractenddate: '',
                minimunageforref: null,
                lessthanminutes: undefined,
                greaterthanminutes: undefined,
                tariffinfodvt: '',
                speciality: {},
                expiredstatus: true,
                Contractspeciality: [{
                    "Speciality": '',
                    "lessthanminutes": null,
                    "greaterthanminutes": null,
                    "fixedfee": null,
                    "active": true
                }]
            }
        }
        $scope.expiredcontract = angular.copy(data[0].ExpiredContractdetails);
        $($scope.modalId).modal('show');
        $scope.modalId = '';

    }

    $scope.updateccg = function() {
        if ($scope.addupdateccg.regionid == '' || $scope.addupdateccg.ccgname == '' || $scope.addupdateccg.ccgreference == '' || $scope.addupdateccg.primarycontactperson == '' || $scope.addupdateccg.primarycontactno == '') {
            $scope.ccgerrormessage = true;
            return;
        }
        if ($scope.addupdateccg.Contractdetails.contractstartdate == '' || $scope.addupdateccg.Contractdetails.contractenddate == '' || $scope.addupdateccg.Contractdetails.minimunageforref == null || !$scope.addupdateccg.primarycontactemail) {
            $scope.ccgerrormessage = true;
            return;
        }
        if ($scope.addupdateccg.Contractdetails.Contractspeciality[0].Speciality == '') {
            $scope.ccgerrormessage = true;
            return;
        }
        for (var i = 0; i < $scope.addupdateccg.Contractdetails.Contractspeciality.length; i++) {

            if ($scope.addupdateccg.Contractdetails.Contractspeciality[i].lessthanminutes == null &&
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].fixedfee == null ||
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].greaterthanminutes == null &&
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].fixedfee == null ||
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].fixedfee == null &&
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].lessthanminutes == null &&
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].greaterthanminutes == null) {
                $scope.ccgerrormessage = true;
                return;
            }
        }
        $scope.ccgerrormessage = false;
        fillContract()
        $scope.addupdateccg.Contractdetails.Contractspeciality = $scope.tempContractspeciality;
        $scope.addupdateccg.usrid = doctorinfo.doctorid;
        $scope.addupdateccg.usrtype = 'ADMIN';
        $scope.addupdateccg.hospitalcd = hospitalcd;
        $scope.addupdateccg.regionid = JSON.parse($scope.addupdateccg.regionid);
        $scope.addupdateccg.active = true;



        var uptccgData = angular.copy($scope.addupdateccg);
        uptccgData.contractrenewalremain = uptccgData.contractrenewalremain != null ? moment(uptccgData.contractrenewalremain, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;
        uptccgData.Contractdetails.contractstartdate = uptccgData.Contractdetails.contractstartdate == '' ? '' : moment(uptccgData.Contractdetails.contractstartdate, 'DD-MM-YYYY').format('YYYY-MM-DD');
        uptccgData.Contractdetails.contractenddate = uptccgData.Contractdetails.contractenddate == '' ? '' : moment(uptccgData.Contractdetails.contractenddate, 'DD-MM-YYYY').format('YYYY-MM-DD');

        referralccg.updateccg(uptccgData, ccgUpdateSuccess, regionerror);
    }

    function fillContract() {
        $scope.tempContractspeciality = [];
        angular.forEach($scope.addupdateccg.Contractdetails.Contractspeciality, function(i) {
            if (i.Speciality != '') {
                $scope.tempContractspeciality.push(i);
            }
        })
    }

    function ccgUpdateSuccess(data) {
        if (data == 'Success') {
            alertify.success("CCG updated successfully")
            $('#AddEditCCG').modal('hide');
            $('#deleteCCGPopupWindow').modal('hide');
            delete ccgParam['ccgid'];
            $scope.getRegionList();
        } else {
            alertify.error('Emailid allready added')
        }
    }

    $scope.getccgDelete = function(data, value) {
        $($scope.modalId).modal('show');
        $scope.modalId = '';
        $scope.ccgActive = value;
        ccgParam.ccgid = data;
        referralccg.getccgDetails(ccgParam, editccgSuccess, regionerror);
    }

    $scope.ccgpopupDelete = function() {
        $scope.addupdateccg.usrid = doctorinfo.doctorid;
        $scope.addupdateccg.usrtype = 'ADMIN';
        $scope.addupdateccg.hospitalcd = hospitalcd;
        $scope.addupdateccg.regionid = JSON.parse($scope.addupdateccg.regionid);
        $scope.addupdateccg.active = $scope.ccgActive == true ? false : true;

        var delccgData = angular.copy($scope.addupdateccg);
        delccgData.contractrenewalremain = delccgData.contractrenewalremain != null ? moment(delccgData.contractrenewalremain, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;
        delccgData.Contractdetails.contractstartdate = delccgData.Contractdetails.contractstartdate == '' ? '' : moment(delccgData.Contractdetails.contractstartdate, 'DD-MM-YYYY').format('YYYY-MM-DD');
        delccgData.Contractdetails.contractenddate = delccgData.Contractdetails.contractenddate == '' ? '' : moment(delccgData.Contractdetails.contractenddate, 'DD-MM-YYYY').format('YYYY-MM-DD');

        referralccg.updateccg(delccgData, ccgAddUpdateSuccess, regionerror);
    }


    function ccgAddUpdateSuccess(data) {
        if ($scope.ccgActive == false) {
            alertify.success("CCG activated successfully");
        } else {
            alertify.success("CCG inactivated successfully")
        }
        $('#AddEditCCG').modal('hide');
        $('#deleteCCGPopupWindow').modal('hide');
        delete ccgParam['ccgid'];
        $scope.getRegionList();
    }

    function listing() {
        c7AdminServices.SpecialityList(ParentHospitalID, specialitylistSuccess, regionerror)

        function specialitylistSuccess(data) {
            $scope.specialityCollection = data;

        }
    }

    listing()

    $scope.addSpeciality = function() {
        var newadd = {
            "Speciality": '',
            "lessthanminutes": null,
            "greaterthanminutes": null,
            "fixedfee": null,
            "active": true
        }
        $scope.addupdateccg.Contractdetails.Contractspeciality.push(newadd);
    }
    $scope.deleteSpeciality = function(ind, value) {
        if (value == undefined) {
            $scope.addupdateccg.Contractdetails.Contractspeciality.splice(ind, 1);
        }
    }

    function regionerror(data) {
        alertify.error("Error in connectivity");
    }

    $scope.loadmoreregion = function() {
        var userRegionParam = {
            "pageNumber": $scope.pageNumber,
            "pageSize": 10,
            "usrid": doctorinfo.doctorid,
            "usrtype": "ADMIN",
            "hospitalcd": hospitalcd
        };
        $scope.pageNumber = $scope.pageNumber + 1;
        userParam['searchtext'] = ($scope.searchRegion != undefined && $scope.searchRegion.length > 3) ? $scope.searchRegion : '';
        referralregion.alllistRegion(userRegionParam, regionloadlistsuccess, regionerror);

        function regionloadlistsuccess(data) {
            if (data.length < 10) {
                $scope.pageNumber = undefined;
            }
            angular.forEach(data, function(i) {
                i.ckbox = i.active
                $scope.allRegion.push(i)
            });

            setTimeout(function() {
                $('[data-toggle="tooltip"]').tooltip();
            }, 500);
        }
    }

    $scope.dateValidationContract = function(sDate, eDate) {
        if (sDate == '') {
            return false;
        } else if (eDate == '') {
            return false;
        } else {
            $scope.start = sDate;
            $scope.end = eDate;
            var startdate = $scope.start.split("-");
            var newstartdate = startdate[2] + "-" + startdate[1] + "-" + startdate[0];
            var enddate = $scope.end.split("-");
            var newendDate = enddate[2] + "-" + enddate[1] + "-" + enddate[0];
            if (new Date(newstartdate) > new Date(newendDate)) {
                $scope.addupdateccg.Contractdetails.contractenddate = '';
                alertify.error("'End' date should be greater than or equal to 'Start' date");

                return false;
            }
        }
    }

}]);