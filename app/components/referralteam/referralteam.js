hCueDoctorWebApp.lazyController('c7ReferralTeam', ['$scope', '$location', 'datacookieFactory', 'hcueLoginStatusService', '$rootScope', 'hcueDoctorLoginService',
    function($scope, $location, datacookieFactory, hcueLoginStatusService, $rootScope, hcueDoctorLoginService) {
 
        var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
        userLoginDetails = datacookieFactory.getItem('loginData');
        if (userLoginDetails.loggedinStatus === "true") {
            $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
        } else {
            $location.path('/login');
        }

        var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
        var access = doctorinfo.isAccessRightsApplicable

        $scope.showRegionAccess = !access;
        $scope.showCCGAccess = !access;
        $scope.showPracticeAccess = !access;
        $scope.showDoctorAccess = !access;
    
        if (access) {
            if (doctorinfo.AccountAccessID.indexOf('REGION') !== -1) {
                $scope.showRegionAccess = true;
            }
            if (doctorinfo.AccountAccessID.indexOf('MNGCCG') !== -1) {
                $scope.showCCGAccess = true;
            }
            if (doctorinfo.AccountAccessID.indexOf('MNGPRACTICE') !== -1) {
                $scope.showPracticeAccess = true;
            }
            if (doctorinfo.AccountAccessID.indexOf('MNGREFDOCTOR') !== -1) {
                $scope.showDoctorAccess = true;
            }
            
        }

        if($scope.showRegionAccess == true){
            $scope.tabno = 1;
            datacookieFactory.set('referralteamtab', $scope.tabno);
        }
        else if($scope.showCCGAccess == true){
            $scope.tabno = 2;
            datacookieFactory.set('referralteamtab', $scope.tabno);
        }
        else if($scope.showPracticeAccess == true){
            $scope.tabno = 3;
            datacookieFactory.set('referralteamtab', $scope.tabno);
        }
        else if($scope.showDoctorAccess == true){
            $scope.tabno = 4;
            datacookieFactory.set('referralteamtab', $scope.tabno);
        }
       

        $scope.shownewtab = function (data) {
            $scope.tabno = data;
            datacookieFactory.set('referralteamtab', $scope.tabno);
            $rootScope.$emit("refeeralpagenav", {});
        }
}]);