﻿hCueDoctorWebControllers.controller('ccglistController', ['$scope', '$window', '$filter', 'hcueDoctorLoginService', 'alertify', 'hCueSmsService', 'hcueLoginStatusService', 'referralccg', 'datacookieFactory', '$rootScope', 'referralregion', 'referralpractice', 'c7AdminServices', 'c7PostCodeServices', function($scope, $window, $filter, hcueDoctorLoginService, alertify, hCueSmsService, hcueLoginStatusService, referralccg, datacookieFactory, $rootScope, referralregion, referralpractice, c7AdminServices, c7PostCodeServices) {
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    var hospitalcd
    var ParentHospitalID
    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined) {
        hospitalcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        ParentHospitalID = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;

    }

    var userParam = {
        "pageNumber": 0,
        "pageSize": 10,
        "usrid": doctorinfo.doctorid,
        "usrtype": "ADMIN",
        "hospitalcd": hospitalcd
    };
    $scope.toggleModal = function(id) {
        $scope.modalId = id;
    }

    // ccg
    $scope.allccg = [];
    $scope.allRegion = [];


    $scope.declareccgVariables = function() {
        $scope.ccgedit = false;
        $scope.ccgerrormessage = false;
        $scope.addupdateccg = {
            active: true,
            regionid: '',
            ccgname: '',
            ccgreference: '',
            primarycontactperson: '',
            primarycontactno: '',
            primarycontactemail: '',
            officesserved: '',
            emponsystem: '',
            localcontact: '',
            assistant: '',
            invoice: '',
            contractrenewalremain: null,
            paymentterms: '',
            Contractdetails: {
                contactdetailsid: undefined,
                contractstartdate: '',
                contractenddate: '',
                minimunageforref: null,
                lessthanminutes: undefined,
                greaterthanminutes: undefined,
                tariffinfodvt: '',
                speciality: {},
                expiredstatus: true,
                Contractspeciality: [{
                    "Speciality": '',
                    "lessthanminutes": null,
                    "greaterthanminutes": null,
                    "fixedfee": null,
                    "active": true
                }]
            }
        }
    }

    $scope.showccgpractice = undefined;
    $scope.ccgedit = false;

    //region list start
    var regionParam = {
        "pageNumber": 0,
        "pageSize": 1000,
        "usrid": doctorinfo.doctorid,
        "usrtype": "ADMIN",
        "hospitalcd": hospitalcd
    };
    $scope.getRegionList = function() {
        referralregion.alllistRegion(regionParam, regionlistsuccess, ccgerror);
    }


    function regionlistsuccess(data) {
        $scope.allRegion = data.filter(val => val.active == true);
        setTimeout(function() {
            $('[data-toggle="tooltip"]').tooltip();
        }, 500);
    }

    //region list end

    // ccg all list start
    $scope.getccgList = function() {
        $scope.pageNumberCCG = 1;
        delete userParam['ccgid'];
        userParam['searchtext'] = '';
        referralccg.alllistccg(userParam, ccglistsuccess, ccgerror);
    }
    $scope.updateFilter = function() {
        if ($scope.searchccg.length == 0 || $scope.searchccg.length > 3) {
            userParam['searchtext'] = $scope.searchccg;
            referralccg.alllistccg(userParam, ccglistsuccess, ccgerror);
        }
    }

    $rootScope.$on('refeeralpagenav', function() {
        $scope.tabno = datacookieFactory.getItem('referralteamtab');
        if ($scope.tabno == 2) {

            $scope.getccgList();
            $scope.getRegionList();
            $scope.searchccg = null;
        }
    });

    function ccglistsuccess(data) {
        if (data.length < 10) {
            $scope.pageNumberCCG = undefined;
        }
        $scope.allccg = data;
    }
    // ccg all list end

    // add and update delete ccg start

    $scope.addccg = function() {
        if ($scope.addupdateccg.regionid == '' || $scope.addupdateccg.ccgname == '' || $scope.addupdateccg.ccgreference == '' || $scope.addupdateccg.primarycontactperson == '' || $scope.addupdateccg.primarycontactno == '') {
            $scope.ccgerrormessage = true;
            return;
        }
        if ($scope.addupdateccg.Contractdetails.contractstartdate == '' || $scope.addupdateccg.Contractdetails.contractenddate == '' || $scope.addupdateccg.Contractdetails.minimunageforref == null || !$scope.addupdateccg.primarycontactemail) {
            $scope.ccgerrormessage = true;
            return;
        }
        if ($scope.addupdateccg.Contractdetails.Contractspeciality[0].Speciality == '') {
            $scope.ccgerrormessage = true;
            return;
        }
        for (var i = 0; i < $scope.addupdateccg.Contractdetails.Contractspeciality.length; i++) {

            if ($scope.addupdateccg.Contractdetails.Contractspeciality[i].lessthanminutes == null &&
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].fixedfee == null ||
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].greaterthanminutes == null &&
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].fixedfee == null ||
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].fixedfee == null &&
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].lessthanminutes == null &&
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].greaterthanminutes == null) {
                $scope.ccgerrormessage = true;
                return;
            }
        }
        fillContract()
        $scope.addupdateccg.Contractdetails.Contractspeciality = $scope.tempContractspeciality;
        $scope.addupdateccg.usrid = doctorinfo.doctorid;
        $scope.addupdateccg.usrtype = 'ADMIN';
        $scope.addupdateccg.hospitalcd = hospitalcd;
        $scope.addupdateccg.regionid = JSON.parse($scope.addupdateccg.regionid);

        var addccgData = angular.copy($scope.addupdateccg);
        addccgData.contractrenewalremain = addccgData.contractrenewalremain != null ? moment(addccgData.contractrenewalremain, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;
        addccgData.Contractdetails.contractstartdate = addccgData.Contractdetails.contractstartdate == '' ? '' : moment(addccgData.Contractdetails.contractstartdate, 'DD-MM-YYYY').format('YYYY-MM-DD');
        addccgData.Contractdetails.contractenddate = addccgData.Contractdetails.contractenddate == '' ? '' : moment(addccgData.Contractdetails.contractenddate, 'DD-MM-YYYY').format('YYYY-MM-DD');



        referralccg.addccg(addccgData, ccgAddSuccess, ccgerror);
        // $scope.declareccgVariables();

    }

    function fillContract() {
        $scope.tempContractspeciality = [];
        angular.forEach($scope.addupdateccg.Contractdetails.Contractspeciality, function(i) {
            if (i.Speciality != '') {
                $scope.tempContractspeciality.push(i);
            }
        })
    }

    function ccgAddSuccess(data) {
        if (data == 'Success') {
            alertify.success("CCG added successfully")
            $scope.declareccgVariables();
            $('#AddEditCCG1').modal('hide');
            $('#deleteCCGPopupWindow1').modal('hide');
            $scope.getccgList();
        } else {
            alertify.error('Email id already added')
        }
    }

    $scope.getccgToEdit = function(data) {
        $scope.ccgerrormessage = false;
        userParam.ccgid = data;
        referralccg.getccgDetails(userParam, editccgSuccess, ccgerror);
    }


    function editccgSuccess(data) {

        $scope.ccgedit = true;
        $scope.addupdateccg = angular.copy(data[0]);
        $scope.addupdateccg.regionid = $scope.addupdateccg.regionid.toString();
        $scope.addupdateccg.contractrenewalremain = $scope.addupdateccg.contractrenewalremain != undefined ? moment($scope.addupdateccg.contractrenewalremain).format('DD-MM-YYYY') : null;
        if ($scope.addupdateccg.Contractdetails.length != 0) {
            $scope.addupdateccg.Contractdetails = $scope.addupdateccg.Contractdetails[$scope.addupdateccg.Contractdetails.length - 1];
            $scope.addupdateccg.Contractdetails.contractstartdate = $scope.addupdateccg.Contractdetails.contractstartdate == '' ? '' : moment($scope.addupdateccg.Contractdetails.contractstartdate).format('DD-MM-YYYY');
            $scope.addupdateccg.Contractdetails.contractenddate = $scope.addupdateccg.Contractdetails.contractenddate == '' ? '' : moment($scope.addupdateccg.Contractdetails.contractenddate).format('DD-MM-YYYY');
            $scope.currentcontract = angular.copy($scope.addupdateccg.Contractdetails);
        } else {
            $scope.currentcontract = [];
            $scope.addupdateccg.Contractdetails = {
                contactdetailsid: undefined,
                contractstartdate: '',
                contractenddate: '',
                minimunageforref: null,
                lessthanminutes: undefined,
                greaterthanminutes: undefined,
                tariffinfodvt: '',
                speciality: {},
                expiredstatus: true,
                Contractspeciality: [{
                    "Speciality": '',
                    "lessthanminutes": null,
                    "greaterthanminutes": null,
                    "fixedfee": null,
                    "active": true
                }]
            }
        }
        $scope.expiredcontract = angular.copy(data[0].ExpiredContractdetails);
        $($scope.modalId).modal('show');
        $scope.modalId = '';

    }

    $scope.updateccg = function() {
        if ($scope.addupdateccg.regionid == '' || $scope.addupdateccg.ccgname == '' || $scope.addupdateccg.ccgreference == '' || $scope.addupdateccg.primarycontactperson == '' || $scope.addupdateccg.primarycontactno == '') {
            $scope.ccgerrormessage = true;
            return;
        }
        if ($scope.addupdateccg.Contractdetails.contractstartdate == '' || $scope.addupdateccg.Contractdetails.contractenddate == '' || $scope.addupdateccg.Contractdetails.minimunageforref == null || !$scope.addupdateccg.primarycontactemail) {
            $scope.ccgerrormessage = true;
            return;
        }
        if ($scope.addupdateccg.Contractdetails.Contractspeciality[0].Speciality == '') {
            $scope.ccgerrormessage = true;
            return;
        }
        for (var i = 0; i < $scope.addupdateccg.Contractdetails.Contractspeciality.length; i++) {

            if ($scope.addupdateccg.Contractdetails.Contractspeciality[i].lessthanminutes == null &&
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].fixedfee == null ||
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].greaterthanminutes == null &&
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].fixedfee == null ||
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].fixedfee == null &&
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].lessthanminutes == null &&
                $scope.addupdateccg.Contractdetails.Contractspeciality[i].greaterthanminutes == null) {
                $scope.ccgerrormessage = true;
                return;
            }
        }
        $scope.ccgerrormessage = false;
        fillContract()
        $scope.addupdateccg.Contractdetails.Contractspeciality = $scope.tempContractspeciality;
        $scope.addupdateccg.usrid = doctorinfo.doctorid;
        $scope.addupdateccg.usrtype = 'ADMIN';
        $scope.addupdateccg.hospitalcd = hospitalcd;
        $scope.addupdateccg.regionid = JSON.parse($scope.addupdateccg.regionid);
        $scope.addupdateccg.active = true;

        var uptccgData = angular.copy($scope.addupdateccg);
        uptccgData.contractrenewalremain = uptccgData.contractrenewalremain != null ? moment(uptccgData.contractrenewalremain, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;
        uptccgData.Contractdetails.contractstartdate = uptccgData.Contractdetails.contractstartdate == '' ? '' : moment(uptccgData.Contractdetails.contractstartdate, 'DD-MM-YYYY').format('YYYY-MM-DD');
        uptccgData.Contractdetails.contractenddate = uptccgData.Contractdetails.contractenddate == '' ? '' : moment(uptccgData.Contractdetails.contractenddate, 'DD-MM-YYYY').format('YYYY-MM-DD');

        referralccg.updateccg(uptccgData, ccgUpdateSuccess, ccgerror);

    }

    function ccgUpdateSuccess(data) {
        if (data == 'Success') {
            alertify.success("CCG updated successfully")
            $('#AddEditCCG1').modal('hide');
            $('#deleteCCGPopupWindow1').modal('hide');
            $scope.getccgList();
        } else {
            alertify.error('Emailid allready added')
        }
    }

    $scope.deleteccg = function(data, value) {
        $($scope.modalId).modal('show');
        $scope.modalId = '';
        userParam.ccgid = data;
        $scope.activeccg = value;
        referralccg.getccgDetails(userParam, editccgSuccess, ccgerror);
    }

    $scope.ccgpopupDelete = function() {
        $scope.addupdateccg.usrid = doctorinfo.doctorid;
        $scope.addupdateccg.usrtype = 'ADMIN';
        $scope.addupdateccg.hospitalcd = hospitalcd;
        $scope.addupdateccg.regionid = JSON.parse($scope.addupdateccg.regionid);
        $scope.addupdateccg.active = $scope.activeccg == true ? false : true;

        var delccgData = angular.copy($scope.addupdateccg);
        delccgData.contractrenewalremain = delccgData.contractrenewalremain != null ? moment(delccgData.contractrenewalremain, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;
        delccgData.Contractdetails.contractstartdate = delccgData.Contractdetails.contractstartdate == '' ? '' : moment(delccgData.Contractdetails.contractstartdate, 'DD-MM-YYYY').format('YYYY-MM-DD');
        delccgData.Contractdetails.contractenddate = delccgData.Contractdetails.contractenddate == '' ? '' : moment(delccgData.Contractdetails.contractenddate, 'DD-MM-YYYY').format('YYYY-MM-DD');

        referralccg.updateccg(delccgData, ccgAddUpdateSuccess, ccgerror);
    }

    function ccgAddUpdateSuccess(data) {
        if ($scope.activeccg == false) {
            alertify.success("CCG activated successfully");
        } else {
            alertify.success("CCG inactivated successfully")
        }
        $('#AddEditCCG1').modal('hide');
        $('#deleteCCGPopupWindow1').modal('hide');
        $scope.getccgList();
    }

    $scope.toggleSubccg = function(data) {
        console.log(data);
        if ($scope.showccgpractice == data) {
            $scope.showccgpractice = undefined;
        } else {
            $scope.showccgpractice = data;
        }
    }

    // add and update delete ccg end

    //practic

    var particeParam = {
        "pageNumber": 0,
        "pageSize": 1000,
        "usrid": doctorinfo.doctorid,
        "usrtype": "ADMIN",
        "hospitalcd": hospitalcd
    }
    $scope.declarepracticeVariables = function() {
        $scope.practiceedit = false;
        $scope.particeerrormessage = false;
        $scope.addupdatepractice = {
            ccgid: '',
            practicename: '',
            practicecode: '',
            postcode: '',
            addressdetl: {
                address1: '',
                address2: '',
                address3: '',
                address4: ''
            },
            citytown: '',
            county: '',
            country: 'UK',
            description: '',
            faxno: '',
            phonedetail: {
                telephoneday1: '',
                telephoneday2: ''
            },
            emaildetl: {
                practiceemail: '',
                repadminemail: '',
                appadminemail: ''
            },
            acceptsfaxmsg: undefined,
            odscode: '',
            autopostcom: undefined,
            practicepopulation: '',
            meshidentifier: '',
            active: true
        }

    }

    $scope.getpracticeToEdit = function(data) {
        $scope.particeerrormessage = false;
        particeParam.practiceid = data;
        referralpractice.getpracticeDetails(particeParam, editpracticeSuccess, ccgerror);
    }


    function editpracticeSuccess(data) {
        $scope.practiceedit = true;
        $scope.addupdatepractice = angular.copy(data[0]);
        $scope.addupdatepractice.ccgid = $scope.addupdatepractice.ccgid.toString()

        $($scope.modalId).modal('show');
        $scope.modalId = '';
    }

    $scope.updatepractice = function() {
        if ($scope.addupdatepractice.ccgid == '' || $scope.addupdatepractice.practicename == '' || $scope.addupdatepractice.practicecode == '' || $scope.addupdatepractice.postcode == '' || $scope.addupdatepractice.telephoneday1 == '') {
            $scope.particeerrormessage = true;
            return;
        }
        if (!$scope.addupdatepractice.emaildetl.practiceemail || !$scope.addupdatepractice.emaildetl.repadminemail || !$scope.addupdatepractice.emaildetl.appadminemail) {
            $scope.particeerrormessage = true;
            return;
        }
        $scope.addupdatepractice.postcode = $scope.addupdatepractice.postcode.toUpperCase();
        $scope.particeerrormessage = false;
        $scope.addupdatepractice.usrid = doctorinfo.doctorid;
        $scope.addupdatepractice.usrtype = 'ADMIN';
        $scope.addupdatepractice.hospitalcd = hospitalcd;
        $scope.addupdatepractice.ccgid = JSON.parse($scope.addupdatepractice.ccgid);
        $scope.addupdatepractice.active = true;
        referralpractice.updatepractice($scope.addupdatepractice, practiceAddUpdateSuccess, ccgerror);
    }

    function practiceAddUpdateSuccess(data) {
        if (data == 'Success') {
            alertify.success("Practice updated successfully")

            $('#AddEditPractice').modal('hide');
            $('#deletePracticePopupWindow').modal('hide');
            $('#activePracticePopupWindow').modal('hide');
            delete particeParam['practiceid'];
            $scope.getccgList();
        } else {
            alertify.error('Emailid allready added')
        }
    }

    $scope.getparticeDelete = function(data, value) {
        $($scope.modalId).modal('show');
        $scope.modalId = '';
        particeParam.practiceid = data;
        $scope.activepartice = value;
        referralpractice.getpracticeDetails(particeParam, editpracticeSuccess, ccgerror);
    }

    $scope.getparticepopupInactive = function() {
        $scope.addupdatepractice.usrid = doctorinfo.doctorid;
        $scope.addupdatepractice.usrtype = 'ADMIN';
        $scope.addupdatepractice.hospitalcd = hospitalcd;
        $scope.addupdatepractice.ccgid = JSON.parse($scope.addupdatepractice.ccgid);
        $scope.addupdatepractice.active = $scope.activepartice == true ? false : true;
        referralpractice.updatepractice($scope.addupdatepractice, practiceUpdateSuccess, ccgerror);
    }

    function practiceUpdateSuccess(data) {
        if ($scope.activepartice == false) {
            alertify.success("Practice activated successfully");
        } else {
            alertify.success("Practice inactivated successfully")
        }
        $('#AddEditPractice').modal('hide');
        $('#deletePracticePopupWindow').modal('hide');
        $('#activePracticePopupWindow').modal('hide');
        delete particeParam['practiceid'];
        $scope.getccgList();
    }


    function listing() {
        c7AdminServices.SpecialityList(ParentHospitalID, specialitylistSuccess, ccgerror)

        function specialitylistSuccess(data) {
            $scope.specialityCollection = data;

        }
    }

    listing()


    function ccgerror(data) {
        alertify.log("Error in connectivity");
    }


    $scope.getloockup = function() {
        $scope.addressCollection = [];
        if ($scope.addupdatepractice.postcode == '') {
            return false;
        }
        c7PostCodeServices.listpostcodeinfo($scope.addupdatepractice.postcode, postcodeSuccess, ccgerror)
    }

    function postcodeSuccess(data) {
        $scope.addressCollection = data.addresses;
    }

    $scope.selectpostcode = function(list) {
        $scope.addressSplit = list.split(',');
        $scope.addupdatepractice.addressdetl.address1 = $scope.addressSplit[0] == ' ' ? '' : $scope.addressSplit[0];
        $scope.addupdatepractice.addressdetl.address2 = $scope.addressSplit[1] == ' ' ? '' : $scope.addressSplit[1];
        $scope.addupdatepractice.addressdetl.address3 = $scope.addressSplit[2] == ' ' ? '' : $scope.addressSplit[2];
        $scope.addupdatepractice.addressdetl.address4 = $scope.addressSplit[3] == ' ' ? '' : $scope.addressSplit[3];
        $scope.addupdatepractice.citytown = $scope.addressSplit[5] == ' ' ? '' : $scope.addressSplit[5];
        $scope.addupdatepractice.county = $scope.addressSplit[6] == ' ' ? '' : $scope.addressSplit[6];
        // $scope.addupdatepractice.country = $scope.addressSplit[] == ' '? '' : $scope.addressSplit[];
    }



    $scope.addSpeciality = function() {
        var newadd = {
            "Speciality": '',
            "lessthanminutes": null,
            "greaterthanminutes": null,
            "fixedfee": null,
            "active": true
        }
        $scope.addupdateccg.Contractdetails.Contractspeciality.push(newadd);
    }
    $scope.deleteSpeciality = function(ind, value) {
        if (value == undefined) {
            $scope.addupdateccg.Contractdetails.Contractspeciality.splice(ind, 1);
        }
    }

    $scope.loadmoreCCG = function() {
        var userCCGParam = {
            "pageNumber": $scope.pageNumberCCG,
            "pageSize": 10,
            "usrid": doctorinfo.doctorid,
            "usrtype": "ADMIN",
            "hospitalcd": hospitalcd
        };
        $scope.pageNumberCCG = $scope.pageNumberCCG + 1;
        userCCGParam['searchtext'] = ($scope.searchccg != undefined && $scope.searchccg.length > 3) ? $scope.searchccg : '';
        referralccg.alllistccg(userCCGParam, CCGloadlistsuccess, ccgerror);

        function CCGloadlistsuccess(data) {
            if (data.length < 10) {
                $scope.pageNumberCCG = undefined;
            } else if (data.length == 10 && $scope.pageNumberCCG == undefined) {
                $scope.pageNumberCCG = 1;
            }
            angular.forEach(data, function(i) {
                $scope.allccg.push(i)
            });
        }
    }

    $scope.dateValidationContract = function(sDate, eDate) {
        if (sDate == '') {
            return false;
        } else if (eDate == '') {
            return false;
        } else {
            $scope.start = sDate;
            $scope.end = eDate;
            var startdate = $scope.start.split("-");
            var newstartdate = startdate[2] + "-" + startdate[1] + "-" + startdate[0];
            var enddate = $scope.end.split("-");
            var newendDate = enddate[2] + "-" + enddate[1] + "-" + enddate[0];
            if (new Date(newstartdate) > new Date(newendDate)) {
                $scope.addupdateccg.Contractdetails.contractenddate = '';
                alertify.error("'End' date should be greater than or equal to 'Start' date");

                return false;
            }
        }
    }

}]);