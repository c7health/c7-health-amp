﻿hCueDoctorWebControllers.controller('parcticelistController', ['$scope', '$window', '$filter', 'hcueDoctorLoginService', 'alertify', 'hCueSmsService', 'hcueLoginStatusService', 'referralpractice', 'datacookieFactory', '$rootScope', 'referralccg', 'referraldoctor', 'c7PostCodeServices', function($scope, $window, $filter, hcueDoctorLoginService, alertify, hCueSmsService, hcueLoginStatusService, referralpractice, datacookieFactory, $rootScope, referralccg, referraldoctor, c7PostCodeServices) {
    // practice
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
    var hospitalcd
    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined) {
        hospitalcd = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
    }

    var userParam = {
        "pageNumber": 0,
        "pageSize": 10,
        "hospitalcd": hospitalcd,
        "usrid": doctorinfo.doctorid,
        "usrtype": "ADMIN",

    };
    $scope.toggleModal = function(id) {
        $scope.modalId = id;
    }

    $scope.allpractice = [];
    $scope.allccg = [];
    $scope.declarepracticeVariables = function() {
        $scope.practiceedit = false;
        $scope.particeerrormessage = false;
        $scope.addupdatepractice = {
            ccgid: '',
            practicename: '',
            practicecode: '',
            postcode: '',
            addressdetl: {
                address1: '',
                address2: '',
                address3: '',
                address4: ''
            },
            citytown: '',
            county: '',
            country: 'UK',
            description: '',
            faxno: '',
            phonedetail: {
                telephoneday1: '',
                telephoneday2: ''
            },
            emaildetl: {
                practiceemail: '',
                repadminemail: '',
                appadminemail: ''
            },
            acceptsfaxmsg: undefined,
            odscode: '',
            autopostcom: undefined,
            practicepopulation: '',
            meshidentifier: '',
            active: true
        }

    }
    var ccgnewparam = {
        "pageNumber": 0,
        "pageSize": 1000,
        "hospitalcd": hospitalcd,
        "usrid": doctorinfo.doctorid,
        "usrtype": "ADMIN"
    }
    $scope.showpracticedoctor = undefined;
    $scope.practiceedit = false;
    $scope.getccgList = function() {
        referralccg.alllistccg(ccgnewparam, ccglistsuccess, practiceerror);
    }

    function ccglistsuccess(data) {
        $scope.allccg = data;
        setTimeout(function() {
            $('[data-toggle="tooltip"]').tooltip();
        }, 500);
    }
    $scope.getpracticeList = function() {
        $scope.pageNumberPractice = 1;
        delete userParam['practiceid'];
        userParam['searchtext'] = '';
        referralpractice.listAllPracticeInfo(userParam, practicelistsuccess, practiceerror);
    }
    $scope.updateFilter = function() {
        if ($scope.searchPractice.length == 0 || $scope.searchPractice.length > 3) {
            userParam['searchtext'] = $scope.searchPractice;
            referralpractice.listAllPracticeInfo(userParam, practicelistsuccess, practiceerror);
        }
    }

    $rootScope.$on('refeeralpagenav', function() {
        $scope.tabno = datacookieFactory.getItem('referralteamtab');
        if ($scope.tabno == 3) {

            $scope.getpracticeList();
            $scope.getccgList();
            $scope.searchPractice = null;
        }
    });



    function practicelistsuccess(data) {
        if (data.length < 10) {
            $scope.pageNumberPractice = undefined;
        } else if (data.length == 10 && $scope.pageNumberPractice == undefined) {
            $scope.pageNumberPractice = 1;
        }
        $scope.allpractice = data;
    }

    $scope.addpractice = function() {
        if ($scope.addupdatepractice.ccgid == '' || $scope.addupdatepractice.practicename == '' || $scope.addupdatepractice.practicecode == '' || $scope.addupdatepractice.postcode == '' || $scope.addupdatepractice.telephoneday1 == '' || $scope.addupdatepractice.odscode == '') {
            $scope.particeerrormessage = true;
            return;
        }
        if (!$scope.addupdatepractice.emaildetl.practiceemail || !$scope.addupdatepractice.emaildetl.repadminemail || !$scope.addupdatepractice.emaildetl.appadminemail) {
            $scope.particeerrormessage = true;
            return;
        }
        $scope.addupdatepractice.postcode = $scope.addupdatepractice.postcode.toUpperCase();
        $scope.particeerrormessage = false;
        $scope.addupdatepractice.usrid = doctorinfo.doctorid;
        $scope.addupdatepractice.usrtype = 'ADMIN';
        $scope.addupdatepractice.hospitalcd = hospitalcd;
        $scope.addupdatepractice.ccgid = JSON.parse($scope.addupdatepractice.ccgid);
        referralpractice.addpractice($scope.addupdatepractice, practiceAddSuccess, practiceerror);
    }

    function practiceAddSuccess(data) {
        if (data == 'Success') {
            alertify.success('Practice added successfully');
            $('#AddEditPractice1').modal('hide');
            $('#deletePracticePopupWindow1').modal('hide');
            $scope.getpracticeList();
        } else {
            alertify.error('Emailid allready added')
        }

    }

    $scope.getpracticeToEdit = function(data) {
        $scope.particeerrormessage = false;
        userParam.practiceid = data;
        referralpractice.getpracticeDetails(userParam, editpracticeSuccess, practiceerror);
    }


    function editpracticeSuccess(data) {
        $scope.practiceedit = true;
        $scope.addupdatepractice = angular.copy(data[0]);
        $scope.addupdatepractice.ccgid = $scope.addupdatepractice.ccgid.toString();

        $($scope.modalId).modal('show');
        $scope.modalId = '';
    }

    $scope.updatepractice = function() {
        if ($scope.addupdatepractice.ccgid == '' || $scope.addupdatepractice.practicename == '' || $scope.addupdatepractice.practicecode == '' || $scope.addupdatepractice.postcode == '' || $scope.addupdatepractice.telephoneday1 == '') {
            $scope.particeerrormessage = true;
            return;
        }
        if (!$scope.addupdatepractice.emaildetl.practiceemail || !$scope.addupdatepractice.emaildetl.repadminemail || !$scope.addupdatepractice.emaildetl.appadminemail) {
            $scope.particeerrormessage = true;
            return;
        }
        $scope.addupdatepractice.postcode = $scope.addupdatepractice.postcode.toUpperCase();
        $scope.particeerrormessage = false;
        $scope.addupdatepractice.usrid = doctorinfo.doctorid;
        $scope.addupdatepractice.usrtype = 'ADMIN';
        $scope.addupdatepractice.hospitalcd = hospitalcd;
        $scope.addupdatepractice.ccgid = JSON.parse($scope.addupdatepractice.ccgid);
        $scope.addupdatepractice.active = true;
        referralpractice.updatepractice($scope.addupdatepractice, practiceUpdateSuccess, practiceerror);
    }

    function practiceUpdateSuccess(data) {
        if (data == 'Success') {
            alertify.success('Practice updated successfully');
            $('#AddEditPractice1').modal('hide');
            $('#deletePracticePopupWindow1').modal('hide');
            $scope.getpracticeList();
        } else {
            alertify.error('Emailid allready added')
        }
    }

    $scope.deletepractice = function(data, value) {
        $($scope.modalId).modal('show');
        $scope.modalId = '';
        $scope.parcticeactive = value;
        userParam.practiceid = data;
        referralpractice.getpracticeDetails(userParam, editpracticeSuccess, practiceerror);
    }

    $scope.deleteparacticePopup = function() {
        $scope.addupdatepractice.usrid = doctorinfo.doctorid;
        $scope.addupdatepractice.usrtype = 'ADMIN';
        $scope.addupdatepractice.hospitalcd = hospitalcd;
        $scope.addupdatepractice.ccgid = JSON.parse($scope.addupdatepractice.ccgid);
        $scope.addupdatepractice.active = $scope.parcticeactive == true ? false : true;
        referralpractice.updatepractice($scope.addupdatepractice, practiceAddUpdateSuccess, practiceerror);
    }

    function practiceAddUpdateSuccess(data) {
        if ($scope.parcticeactive == true) {
            alertify.success("Practice inactivated successfully");
        } else {
            alertify.success("Practice activated successfully")
        }
        $('#AddEditPractice1').modal('hide');
        $('#deletePracticePopupWindow1').modal('hide');
        $scope.getpracticeList();
    }

    $scope.toggleSubpractice = function(data) {

        if ($scope.showpracticedoctor == data) {
            $scope.showpracticedoctor = undefined;
        } else {
            $scope.showpracticedoctor = data;
        }
    }

    function practiceerror(data) {
        alertify.log("Error in connectivity");
    }

    $scope.exportToExcel = function()
    {
        var param = {
            "Hospitalcode" : hospitalcd,
            "date": moment().format('YYYY-MM-DD')
        }
        referralpractice.downloadPracticeData(param, downloadDataSuccess, downloadDataError);

    }

    function downloadDataSuccess(data){
        var downloadData = data;

        var exportPracticeData = [
            {
              CCG: 'CCG',
              PracticeName: 'Practice Name',
              PracticeCode: 'Practice Code',
              Postcode: 'Postcode',
              Address1: 'Address1',
              Address2: 'Address2',
              Address3: 'Address3',
              Address4: 'Address4',
              Town: 'Town',
              County: 'County',
              Country: 'Country',
              Description: 'Description',
              FaxNo: 'Fax No',
              TelephoneDay1: 'Telephone Day 1',
              TelephoneDay2: 'Telephone Day 2',
              PracticeManagerEmail: 'Practice Manager Email',
              ReportAdminEmail: 'Report Admin Email',
              AppointmentAdminEmail: 'Appointment Admin Email',
              AcceptsFaxMsg: 'Accepts Fax Msg',
              AutopostCom: 'Auto-post Communication',
              ODSCode: 'ODS Code',
              PracticePopulation: 'Practice Population',
              MESHIdentifier: 'MESH Identifier'
            }
          ];

          for(i=0; i < downloadData.length; i++) {
            var temp = {
                CCG: downloadData[i].ccgname,
                PracticeName: downloadData[i].practicename,
                PracticeCode: downloadData[i].practicecode,
                Postcode: downloadData[i].postcode,
                Address1: downloadData[i].address1,
                Address2: downloadData[i].address2,
                Address3: downloadData[i].address3,
                Address4: downloadData[i].address4,
                Town: downloadData[i].citytown,
                County: downloadData[i].county,
                Country: downloadData[i].country,
                Description: downloadData[i].description,
                FaxNo: downloadData[i].faxno,
                TelephoneDay1: downloadData[i].telephoneday1,
                TelephoneDay2: downloadData[i].telephoneday2,
                PracticeManagerEmail: downloadData[i].practiceemail,
                ReportAdminEmail: downloadData[i].repadminemail,
                AppointmentAdminEmail: downloadData[i].appadminemail,
                AcceptsFaxMsg: downloadData[i].acceptsfaxmsg == null ? 'false' : downloadData[i].acceptsfaxmsg,
                AutopostCom: downloadData[i].autopostcom == null ? 'false' : downloadData[i].autopostcom,
                ODSCode: downloadData[i].odscode,
                PracticePopulation: downloadData[i].practicepopulation,
                MESHIdentifier: downloadData[i].meshidentifier
              };
              exportPracticeData.push(temp);
          }

          var xmls = fetchXML([exportPracticeData]);
          var ctx = {
            created: new Date().getTime(),
            worksheets: xmls
          };
          var uri = 'data:application/vnd.ms-excel;base64,',
          tmplWorkbookXML =
            '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
            '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
            '<Styles>' +
            '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
            '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
            '</Styles>' +
            '{worksheets}</Workbook>',
          tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
          tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
          base64 = function(s) {
            return window.btoa(unescape(encodeURIComponent(s)));
          },
          format = function(s, c) {
            return s.replace(/{(\w+)}/g, function(m, p) {
              return c[p];
            });
          };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'PracticeData_' + new Date().getTime() + '.xls';       
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);

    }

    function downloadDataError(data){
        console.log(data);
    }

    function fetchXML(stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
          var worksheetName = 'Data';        
          wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
          var rowXML = '';    
          for (j = 0; j < stores[i].length; j++) {
            rowXML += '<Row>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].CCG + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PracticeName + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PracticeCode + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Postcode + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Address1 + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Address2 + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Address3 + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Address4 + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Town + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].County + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Country + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Description + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].FaxNo + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].TelephoneDay1 + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].TelephoneDay2 + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PracticeManagerEmail + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ReportAdminEmail + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentAdminEmail + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AcceptsFaxMsg + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AutopostCom + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].ODSCode + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].PracticePopulation + '</Data></Cell>';
            rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].MESHIdentifier + '</Data></Cell>';
            rowXML += '</Row>';
          }
          wrkbookXML += rowXML;
          wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
    }

    $scope.declaredoctorVariables = function() {

        $scope.activePractices = [];
        $scope.doctoredit = false;
        $scope.doctorerrormessage = false;
        $scope.addupdatedoctor = {
            "doctorDetails": {
                "Title": "",
                "FirstName": "",
                "FullName": "",
                "LastName": "",
                "Gender": "M",
                "SpecialityCD": {
                    "1": "CAO"
                },
                "TermsAccepted": "N",
                "Prospect": "Y",
                "DOB": null
            },
            "DoctorPhone": [{
                "PhNumber": '',
                "PhType": "D",
                "PrimaryIND": "Y"
            }, {
                "PhNumber": '',
                "PhType": "E",
                "PrimaryIND": "Y"
            }, {
                "PhNumber": '',
                "PhType": "M",
                "PrimaryIND": "Y"
            }],
            "DoctorEmail": [{
                "EmailID": "",
                "EmailIDType": "P",
                "PrimaryIND": "Y"
            }],
            "doctorOtherDetails": {
                "faxno": '',
                "gpcode": "",
                "typevalue": "",
                "notes": ""
            },
            "PractiesDetails": []
        }

    }

    $scope.editdoctor = function(data) {
        $scope.doctorerrormessage = false;
        referraldoctor.getdoctorDetails(data, editdoctorSuccess, doctorerror);
    }

    function editdoctorSuccess(data) {
        $scope.declaredoctorVariables();
        $scope.doctoredit = true;
        $scope.doctorcollection = angular.copy(data);
        $scope.addupdatedoctor.doctorDetails = $scope.doctorcollection.doctor[0];
        $scope.addupdatedoctor.doctorDetails.FirstName = $scope.doctorcollection.doctorOtherDetails.FirstName;
        $scope.addupdatedoctor.doctorDetails.LastName = $scope.doctorcollection.doctorOtherDetails.LastName;
        $scope.addupdatedoctor.doctorDetails.DOB = $scope.addupdatedoctor.doctorDetails.DOB != undefined ? moment($scope.addupdatedoctor.doctorDetails.DOB).format('DD-MM-YYYY') : null;
        $scope.addupdatedoctor.DoctorPhone = $scope.doctorcollection.doctorPhone;
        $scope.addupdatedoctor.DoctorEmail = $scope.doctorcollection.doctorEmail != undefined ? $scope.doctorcollection.doctorEmail : $scope.addupdatedoctor.DoctorEmail;
        $scope.addupdatedoctor.doctorOtherDetails = $scope.doctorcollection.doctorOtherDetails;

        $scope.activePractices = [];
        angular.forEach($scope.allpractice, function(i) {
            angular.forEach($scope.doctorcollection.PractiesDetails, function(k) {
                if (i.practiceid == k.practiceid) {
                    i.isActive = true;
                    $scope.activePractices.push(i);
                }
            })
        })
        $($scope.modalId).modal('show');
        $scope.modalId = '';

    }

    $scope.updatedoctor = function() {
        if ($scope.addupdatedoctor.doctorDetails.Title == '' || $scope.addupdatedoctor.doctorDetails.FirstName == '' || $scope.addupdatedoctor.doctorDetails.LastName == '' || $scope.addupdatedoctor.doctorOtherDetails.gpcode == '') {
            $scope.doctorerrormessage = true;
            return;
        }

        if ($scope.newcount.length == 0) {
            $scope.doctorerrormessage = true;
            return;
        }
        $scope.doctorerrormessage = false;
        fillinfo()
        $scope.addupdatedoctor.USRId = doctorinfo.doctorid;
        $scope.addupdatedoctor.USRType = 'ADMIN';
        $scope.addupdatedoctor.hospitalcd = hospitalcd;
        $scope.addupdatedoctor.doctorDetails.DOB = $scope.addupdatedoctor.doctorDetails.DOB != null ? moment($scope.addupdatedoctor.doctorDetails.DOB, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;
        $scope.addupdatedoctor.DoctorPhone[0].PhNumber = $scope.addupdatedoctor.DoctorPhone[0].PhNumber;
        $scope.addupdatedoctor.DoctorPhone[1].PhNumber = $scope.addupdatedoctor.DoctorPhone[1].PhNumber;
        $scope.addupdatedoctor.DoctorPhone[2].PhNumber = $scope.addupdatedoctor.DoctorPhone[2].PhNumber;
        $scope.addupdatedoctor.doctorOtherDetails.faxno = $scope.addupdatedoctor.doctorOtherDetails.faxno;
        $scope.addupdatedoctor.PractiesDetails = $scope.PractiesDetails;
        $scope.addupdatedoctor.DoctorEmail = $scope.addupdatedoctor.DoctorEmail[0].EmailID != '' ? $scope.addupdatedoctor.DoctorEmail : undefined;

        referraldoctor.newupdateDoctor($scope.addupdatedoctor, doctorUpdateSuccess, doctorerror);
    }

    function doctorUpdateSuccess(data) {
        if (!data.error) {
            alertify.success("Doctor updated successfully")
            $('#deleteDoctorPopupWindow').modal('hide');
            $('#AddEditDoctor').modal('hide');
            $scope.getpracticeList();
        } else {
            if (data.error.message.includes('already exists') && data.error.message.includes('Key (\"EmailID\")')) {
                alertify.error('Emailid allready added')
            } else {
                alertify.error('Error in adding doctor')
            }
        }

    }


    function fillinfo() {
        $scope.PractiesDetails = [];
        angular.forEach($scope.activePractices, function(j) {
            if (j.isActive == true) {
                j.postcode = j.postcode.toString();
                $scope.PractiesDetails.push(j);
            }

        })
    }

    $scope.deletedoctor = function(list, value) {
        $($scope.modalId).modal('show');
        $scope.modalId = '';
        $scope.activedoctor = value;
        $scope.deletedoctorid = parseFloat(list.doctorid);
        $scope.deleteparticeid = parseFloat(list.practiceid);
    }

    $scope.deleteDoctorpopupclick = function(activate) {
        $scope.activatenew = activate;
        var param = {
            "practiceid": $scope.deleteparticeid,
            "doctorid": $scope.deletedoctorid,
            "usrid": doctorinfo.doctorid,
            "usrtype": "ADMIN",
            "active": activate,
            "hospitalcd": hospitalcd
        }
        referraldoctor.deactivedoctorPractice(param, doctorAddUpdateSuccess, doctorerror)
    }

    function doctorAddUpdateSuccess(data) {
        if ($scope.activatenew == true) {
            alertify.success("Doctor activated successfully");
        } else {
            alertify.success("Doctor inactivated successfully")
        }
        $('#deleteDoctorPopupWindow').modal('hide');
        $('#AddEditDoctor').modal('hide');
        $scope.getpracticeList();
    }


    function doctorerror(data) {
        alertify.log("Error in practice connectivity");
    }

    // Post code
    $scope.getloockup = function() {
        $scope.addressCollection = [];
        if ($scope.addupdatepractice.postcode == '') {
            return false;
        }
        c7PostCodeServices.listpostcodeinfo($scope.addupdatepractice.postcode, postcodeSuccess, doctorerror)
    }

    function postcodeSuccess(data) {
        $scope.addressCollection = data.addresses;
    }

    $scope.selectpostcode = function(list) {
        $scope.addressSplit = list.split(',');
        $scope.addupdatepractice.addressdetl.address1 = $scope.addressSplit[0] == ' ' ? '' : $scope.addressSplit[0];
        $scope.addupdatepractice.addressdetl.address2 = $scope.addressSplit[1] == ' ' ? '' : $scope.addressSplit[1];
        $scope.addupdatepractice.addressdetl.address3 = $scope.addressSplit[2] == ' ' ? '' : $scope.addressSplit[2];
        $scope.addupdatepractice.addressdetl.address4 = $scope.addressSplit[3] == ' ' ? '' : $scope.addressSplit[3];
        $scope.addupdatepractice.citytown = $scope.addressSplit[5] == ' ' ? '' : $scope.addressSplit[5];
        $scope.addupdatepractice.county = $scope.addressSplit[6] == ' ' ? '' : $scope.addressSplit[6];
        // $scope.addupdatepractice.country = $scope.addressSplit[6] == ' '? '' : $scope.addressSplit[6];
    }
    $scope.externalpractices = function() {
        $scope.allpractice.selected.isActive = true;
        var available = false;
        if ($scope.activePractices.length > 0) {
            for (var i = 0; i < $scope.activePractices.length; i++) {
                if ($scope.activePractices[i].practiceid == $scope.allpractice.selected.practiceid) {
                    available = true;
                    break;
                }
            }
        }
        if (!available) {
            $scope.activePractices.push($scope.allpractice.selected);
            $scope.allpractice.selected = {};
        }
    }

    $scope.loadmorePractice = function() {
        var userParcticParam = {
            "pageNumber": $scope.pageNumberPractice,
            "pageSize": 10,
            "usrid": doctorinfo.doctorid,
            "usrtype": "ADMIN",
            "hospitalcd": hospitalcd
        };
        $scope.pageNumberPractice = $scope.pageNumberPractice + 1;
        userParcticParam['searchtext'] = ($scope.searchPractice != undefined && $scope.searchPractice.length > 3) ? $scope.searchPractice : '';
        referralpractice.listAllPracticeInfo(userParcticParam, practiceloadlistsuccess, practiceerror);

        function practiceloadlistsuccess(data) {
            if (data.length < 10) {
                $scope.pageNumberPractice = undefined;
            }
            angular.forEach(data, function(i) {
                $scope.allpractice.push(i)
            });
        }
    }

}]);