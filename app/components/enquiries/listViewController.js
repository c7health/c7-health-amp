hCueDoctorWebControllers.controller('listViewController', function (
  $scope,
  $filter,
  alertify,
  hcueDoctorLoginService,
  c7EnquiriesServices,
  c7SearchServices,
  referraldoctor,
  c7AddUpdatePatient
) {
  
  var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
  $scope.searchshow = true;
  $scope.canledershow  = true;

  if (
    doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !==
    undefined &&
    doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo
      .ParentHospitalID !== undefined
  ) {
    parentHospitalId =
      doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo
        .ParentHospitalID;
    hospitalCode =
      doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo
        .HospitalCode;
    newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
  }

  $scope.date = moment().format('DD-MM-YYYY');
  $scope.getList = function () {
    // var param = {
    //     Hospitalcode: "TQKWQGLCTL",
    //     hospitalID: 73,
    //     date: "2018-08-11"
    // }
    var param = {
      Hospitalcode: hospitalCode,
      hospitalID: parentHospitalId,
      date: moment($scope.date, 'DD-MM-YYYY').format('YYYY-MM-DD')
    };

    c7EnquiriesServices.getCalendarlistviewInfo(param, listSuccess, listError);

    function listSuccess(data) {
      $scope.listCollection = data;
      // console.log(data);
      angular.forEach($scope.listCollection, function (val) {
        var slotcollobj = [];
        angular.forEach(val.slotcollection, function (p, key) {
          if (
            p == val.FromBreakTime1 ||
            (val.FromBreakTime1 > p &&
              val.FromBreakTime1 < val.slotcollection[key + 1])
          ) {
            slotcollobj.push({
              break: true,
              emergency: false,
              time: p,
              start: val.FromBreakTime1,
              end: val.ToBreakTime1,
              patient: false
            });
          } else if (
            val.FromBreakTime1 != '00:00' &&
            val.ToBreakTime1 != '00:00' &&
            p > val.FromBreakTime1 &&
            p < val.ToBreakTime1
          ) {
          } else if (
            p == val.FromBreakTime2 ||
            (val.FromBreakTime2 > p &&
              val.FromBreakTime2 < val.slotcollection[key + 1])
          ) {
            slotcollobj.push({
              break: true,
              emergency: false,
              time: p,
              start: val.FromBreakTime2,
              end: val.ToBreakTime2,
              patient: false
            });
          } else if (
            val.FromBreakTime2 != '00:00' &&
            val.ToBreakTime2 != '00:00' &&
            p > val.FromBreakTime2 &&
            p < val.ToBreakTime2
          ) {
          } else if (
            p == val.FromBreakTime3 ||
            (val.FromBreakTime3 > p &&
              val.FromBreakTime3 < val.slotcollection[key + 1])
          ) {
            slotcollobj.push({
              break: true,
              emergency: false,
              time: p,
              start: val.FromBreakTime3,
              end: val.ToBreakTime3,
              patient: false
            });
          } else if (
            val.FromBreakTime3 != '00:00' &&
            val.ToBreakTime3 != '00:00' &&
            p > val.FromBreakTime3 &&
            p < val.ToBreakTime3
          ) {
          } else if (
            p == val.EmergencyFromtime ||
            (val.EmergencyFromtime > p &&
              val.EmergencyFromtime < val.slotcollection[key + 1])
          ) {
            slotcollobj.push({
              break: false,
              emergency: true,
              time: p,
              start: val.EmergencyFromtime,
              end: val.EmergencyTotime,
              patient: false
            });
          } else if (
            val.EmergencyFromtime != '00:00' &&
            val.EmergencyTotime != '00:00' &&
            p > val.EmergencyFromtime &&
            p < val.EmergencyTotime
          ) {
            slotcollobj.push({
              break: false,
              emergency: true,
              time: p,
              start: val.EmergencyFromtime,
              end: val.EmergencyTotime,
              patient: false
            });
          } else {
            slotcollobj.push({
              emergency: false,
              break: false,
              time: p,
              patient: false
            });
          }
        });
        angular.forEach(slotcollobj, function (i) {
          var xx = true;
          angular.forEach(val.Patientdetails, function (j) {
            if (j && xx) {
              if (i.time == j.Time) {
                i.PatientName = j.PatientName;
                i.PatientID = j.PatientID;
                i.Dicomcode = j.Dicomcode;
                i.patient = true;
                i.Bookstatus = j.Bookstatus;
                i.Status = j.Status;
                i.emergency = j.emergency;
                xx = false;
              }
            }
          });
        });
        console.log(slotcollobj);
        val.updatedSlots = slotcollobj;
        if (val.Patientdetails[0]) {
          val.isSlotAvailable =
            val.Patientdetails[0].PatientID !== null ? false : true;
        }
        // val.updatedSlots = slotcollobj.filter(data => data.break || data.patient);
        // console.log(val.updatedSlots);
      });
    }
  };
  $scope.getList()
  // $scope.getList();
  function listError(data) {
    alertify.error('error');
  }

  $scope.$on('EnquiryTabClick', function (evt, data) {
    $scope.canledershow  = true;
    $scope.searchshow = true;    
    if (data == 'listview') {
      $scope.date = moment().format('DD-MM-YYYY');      
      $scope.getList();
    }
  });

  $scope.sendAptEmail = function (data) {
    if (data.isSlotAvailable) {
      alertify.error('No appointments to send mail');
      return false;
    }
    var param = {
      hospitalid: data.HospitalID,
      sonographerid: data.DoctorID,
      addressconsultid: data.AddressConsultID,
      scheduledate: data.ScheduleDate
    };
    c7EnquiriesServices.sendAppointmentdetailsMail(
      param,
      emailSuccess,
      emailError
    );

    function emailSuccess(data) {
      if (data.toLowerCase() == 'success') {
        alertify.success('Mail sent successfully');
      }
    }

    function emailError(data) {
      alertify.error(data);
    }
  };
  $scope.sendSupportEmail = function (data) {
    // console.log(data);
    if (data.isSlotAvailable) {
      alertify.error('No appointments to send mail');
      return false;
    }
    var param = {
      scheduledate: data.ScheduleDate,
      doctorid: data.cswid,
      regionname: data.regionname,
      sendemail: true,
      hospitalid: data.HospitalID,
      hospitalname: data.HospitalName,
      sonographername: data.DoctorName,
      cswname: data.cswname,
      addressconsultid: data.AddressConsultID
    };
    c7EnquiriesServices.sendSupportworkerinfoEmail(
      param,
      supportsuccess,
      emailError
    );

    function supportsuccess(data) {
      if (data.toLowerCase() == 'success') {
        alertify.success('Mail sent successfully');
      }
    }

    function emailError(data) {
      alertify.error(data);
    }
  };
  $scope.sendSonoEmail = function (data) {
    // console.log(data);
    if (data.isSlotAvailable) {
      alertify.error('No appointments to send mail');
      return false;
    }
    var param = {
      scheduledate: data.ScheduleDate,
      doctorid: data.DoctorID,
      sendemail: true,
      regionname: data.regionname,
      hospitalid: data.HospitalID,
      hospitalname: data.HospitalName,
      sonographername: data.DoctorName,
      cswname: data.cswname,
      addressconsultid: data.AddressConsultID
    };
    c7EnquiriesServices.sendSonographerinfoEmail(
      param,
      sonosuccess,
      emailError
    );

    function sonosuccess(data) {
      if (data.toLowerCase() == 'success') {
        alertify.success('Mail sent successfully');
      }
    }

    function emailError(data) {
      alertify.error(data);
    }
  };

  $scope.showPatientHistory = function(slotDetail){    
    $scope.canledershow  = false;
      $scope.activePatientId = slotDetail.PatientID;
      searchPatientId();
      $scope.searchshow = false;
      $scope.caseViewCollection = [];
      //$scope.selectUserInfo = slotDetail;
      var params = {
          patientid: slotDetail.PatientID,
          pagenumber: 0,
          pagesize: 1000
      }

      c7SearchServices.listPatientAllEnquiry(params, OnlistPatientAllEnquirySuccess, OnGetError);

      function OnlistPatientAllEnquirySuccess(data) {
          $scope.caseViewCollection = data;
          angular.forEach($scope.caseViewCollection, function (i, ind) {
              if (ind == 0) {
                  i.isSelected = true;
              } else {
                  i.isSelected = false;
              }
          })
          $scope.detailedView($scope.caseViewCollection[0], 0)
      }
  }

  function OnGetError(data) {

  }
function searchPatientId(){
  c7SearchServices.searchPatientsget($scope.activePatientId, OnGetPatientSuccess, OnGetError);
  function OnGetPatientSuccess(data) {
    $scope.patientname = data[0].patientname;
    $scope.imageurl = data[0].imageurl;
    $scope.age = data[0].age;
    $scope.gender = data[0].gender;
    $scope.nhsid = data[0].nhsid;
    $scope.patientid = data[0].patientid;

   
  }
}

  $scope.detailedView = function (data, index) {
    console.log(data, index)
    $scope.activeEnquiryId = data.patientenquiryid;
    $scope.activeLeftIndex = data.index;
    $scope.enquiriesCollection = [];
    angular.forEach($scope.caseViewCollection, function (j, ind) {
        if (index == ind) {
            j.isSelected = true;
        } else {
            j.isSelected = false;
        }
    })
    $scope.selectedCollection = data;
    var subParam = {
        patientenquiryid: data.patientenquiryid,
    }
    c7EnquiriesServices.listEnquiryStatus(subParam, listEnquiryStatusSuccess, OnGetError);
    c7EnquiriesServices.listAppointmentStatus(data.patientenquiryid + '/' + docid, subappointmentlistSuccess, OnGetError);

    $scope.enquiriesCollection = [];

    function listEnquiryStatusSuccess(data) {

        angular.forEach(data, function (val) {
            val.dateNew = moment.utc(val.date, 'YYYY-MM-DD HH:mm:ss').local().format('DD-MM-YYYY HH:mm:ss');
            val.timestamp = moment(val.date, 'YYYY-MM-DD HH:mm:ss').valueOf();
            if (val.enquirystatusid == 'appemail' ||
                val.enquirystatusid == 'appointEmail' ||
                val.enquirystatusid == 'exclusion' ||
                val.enquirystatusid == 'exclusionEmail' ||
                val.enquirystatusid == 'declinedEmail' ||
                val.enquirystatusid == 'patientDeclinedEmail' ||
                val.enquirystatusid == 'dnaEmail' ||
                val.enquirystatusid == 'contactusEmail' ||
                val.enquirystatusid == 'nocontactEmail' ||
                val.enquirystatusid == 'urgentnocontactEmail') {
                val.emailobj = {
                    patientenquiryid: $scope.activeEnquiryId,
                    appointmentid: 0,
                    sendemail: true,
                    sendletter: true
                }
            }
        })
        $scope.enquiriesCollection = $scope.enquiriesCollection.concat(data);
        $scope.sublistSussessCount += 1;
        if ($scope.sublistSussessCount == 2) {
            $scope.enquiriesCollection = $scope.enquiriesCollection.sort(function (a, b) {
                return b.timestamp - a.timestamp;
            });
            $('#loaderDiv').hide();
            $scope.sublistSussessCount = 0;
        }        
    }

    function subappointmentlistSuccess(data) {

        angular.forEach(data, function (val) {
            val.dateNew = moment.utc(val.date, 'YYYY-MM-DD HH:mm:ss').local().format('DD-MM-YYYY HH:mm:ss');
            val.timestamp = moment(val.date, 'YYYY-MM-DD HH:mm:ss').valueOf();
            if (val.enquirystatusid == 'appointEmail' || val.enquirystatusid == 'exclusionEmail' || val.enquirystatusid == 'declinedEmail' || val.enquirystatusid == 'dnaEmail' || val.enquirystatusid == 'contactusEmail' || val.enquirystatusid == 'nocontactEmail' || val.enquirystatusid == 'urgentnocontactEmail') {
                val.emailobj = {
                    patientenquiryid: $scope.activeEnquiryId,
                    appointmentid: 0,
                    sendemail: true,
                    sendletter: true
                }
            }
        })
        $scope.enquiriesCollection = $scope.enquiriesCollection.concat(data);
        $scope.sublistSussessCount += 1;
        if ($scope.sublistSussessCount == 2) {
            $scope.enquiriesCollection = $scope.enquiriesCollection.sort(function (a, b) {
                return b.timestamp - a.timestamp;
            });
            $scope.sublistSussessCount = 0;
        }        
    }

}
  
$scope.doctorDetails = function (id, val) {
  $scope.modalId = id;
  $scope.doctorInfo = [];
  referraldoctor.getdoctorDetails(val.referdoctorid, doctorDetailsSuccess, OnGetError);

  function doctorDetailsSuccess(data) {
      $scope.doctorInfo = data;

      if ($scope.modalId != '') {
          $($scope.modalId).modal('show');
          $scope.modalId = '';
      }
  }
}
$scope.modalId = '';
$scope.getPatientDetails = function (id) {
    $scope.modalId = id;
    var userParam = {
        PatientID: $scope.patientid,
        PatientEnquiryID: $scope.selectedCollection.patientenquiryid,
        USRId: doctorinfo.doctorid,
        USRType: "ADMIN",
        hospitalInfo: {
            HospitalID: parentHospitalId,
            HospitalCD: hospitalCode
        }
    }
    c7AddUpdatePatient.getPatientDetails(userParam, getpatientsuccess, OnGetError);

    function getpatientsuccess(data) {
        $scope.patientDetails = data;

        if ($scope.modalId != '') {
            $($scope.modalId).modal('show');
            $scope.modalId = '';
        }
    }
}

function updateEnquiryStatus(data) {
  if (data != "Success") {
      alertify.error('Status update error');
  } else {
      $scope.detailedView($scope.caseViewCollection[0], $scope.activeLeftIndex);
  }
}

$scope.addnotes = function () {
  $scope.notesparam = {
      patientid: $scope.activePatientId,
      patientenquiryid: $scope.activeEnquiryId,
      usrid: doctorinfo.doctorid,
      statustypeid: "Notes",
      documenturl: undefined
  };
  console.log($scope.notesparam);
}

$scope.submitnotes = function () {
  $scope.notesparam['documenturl'] = $scope.Gnnotes;
  console.log($scope.notesparam);
  $('#notes').modal('hide');
  c7EnquiriesServices.enquiryaddstatus($scope.notesparam, updateEnquiryStatus, uploadError);
}

$('body').on('change', '#UploadDocument3', function (e) {
  var droppedFiles1 = e.target.files;
  uploadFile1(droppedFiles1);
});

uploadFile1 = function (data) {
  var file = data[0];

  if (file == undefined) {
      return;
  }
  var formData = new FormData();
  $scope.fileurl = file.name;

  formData.append("fileUpload", file);
  formData.append("typeDoc", "LettersDocument");
  formData.append("patientID", $scope.activePatientId);
  formData.append("patientenquiryID", $scope.activeEnquiryId);
  var ext = file.name.split('.');
  formData.append("fileExtn", ext[ext.length - 1].toLowerCase());
  $scope.fileType = ext[ext.length - 1];

  formData.append("timestamp", moment().valueOf());
  formData.append("fileName", file.name.toLowerCase());

  c7AddUpdatePatient.uploadLetterDocument(formData, uploadSuccess, uploadError);

  document.getElementById('UploadDocument3').value = '';

}

function uploadError(data) {
  alertify.error(data);
}

function uploadSuccess(data) {
  if (data.url) {
      alertify.success('Upload Success');
      uploadFileToEnquiry(data.url);
  } else {
      alertify.error('Upload Error');
  }
}

function uploadFileToEnquiry(urlString) {
  var uploadParam = {
      patientid: $scope.activePatientId,
      patientenquiryid: $scope.activeEnquiryId,
      usrid: doctorinfo.doctorid,
      statustypeid: "DocumentUpload",
      documenturl: urlString
  };

  c7EnquiriesServices.enquiryaddstatus(uploadParam, updateEnquiryStatus, uploadError);
}

function updateEnquiryStatus(data) {
  if (data != "Success") {
      alertify.error('Status update error');
  } else {
      $scope.detailedView($scope.caseViewCollection[0], $scope.activeLeftIndex);
  }
}

});
