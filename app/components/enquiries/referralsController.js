hCueDoctorWebControllers.controller('referralsController', function ($scope, alertify, $location, hcueDoctorLoginService, c7EnquiriesServices, datacookieFactory) {
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
        docid = doctorinfo.doctorid;
    }

    $scope.$on('EnquiryTabClick', function (evt, data) {
        if (data == 'Referrals') {
            $scope.listing()
        }
    });
    $scope.listing = function () {
        $scope.listparams = {
            HospitalCode: hospitalCode,
            pagenumber: 1,
            pagesize: 10
        }
        c7EnquiriesServices.listAllReferral($scope.listparams, referralSuccess, error);

        function referralSuccess(data) {
            if (data != 'Error' || !data.error) {
                if (data.length < 10) {
                    $scope.loadMore = false;
                } else {
                    $scope.loadMore = true;
                }
                $scope.referralColl = data;
                $scope.referralColl.forEach(function (elm, ind) {
                    elm['sno'] = ind + 1;
                });
            } else {
                alertify.error('Referral list Error');
            }
        }
    }
    $scope.listing()

    $scope.nextPage = function () {
        $scope.listparams.pagenumber += 1;
        c7EnquiriesServices.listAllReferral($scope.listparams, referralSuccessnext, error);
    }

    $scope.loadMore = true;

    function referralSuccessnext(data) {
        if (data != 'Error' || !data.error) {
            if (data.length < 10) {
                $scope.loadMore = false;
            } else {
                $scope.loadMore = true;
            }
            $scope.referralColl = $scope.referralColl.concat(data);
            $scope.referralColl.forEach(function (elm, ind) {
                elm['sno'] = ind + 1;
            });
        } else {
            alertify.error('Referral list Error');
        }
    }

    function error(data) {
        alertify.error('Referral List error' + data.error.message);
    }

    // $scope.addReferral = function(data) {
    //     confirm.log(data);
    // }
    // $scope.issueReferral = function(data) {
    //     confirm.log(data);
    // }

    $scope.editPatient = function (pId, eqId) {
        datacookieFactory.set('editPatientId', pId);
        datacookieFactory.set('editPatientEnquiryId', eqId);
        datacookieFactory.set('doctorreferral', 'totriage');

        // setTimeout(function(){
        editPatientDetails();
        // }, 200);
    };

    function editPatientDetails() {
        var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
        var url = "/addUpdatePatient?kwid=" + (guid || "");
        $location.url(url);
    }
});