﻿hCueDoctorWebControllers.controller('appointmentListController', function ($scope, $rootScope, $http, alertify, $window, $location, hcueDoctorLoginService, c7EnquiriesServices, c7AddUpdatePatient, datacookieFactory, c7AppointmentListServices, c7AdminServices, c7Hl7ToMeshServices) {

    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
        docid = doctorinfo.doctorid;
    }

    $scope.modalId = '';
    $scope.toggleModal = function (id) {
        $scope.modalId = id;
    }
    $scope.addressId = 5;
    $scope.filteraptList = '';
    $scope.aptDate =moment().format('DD-MM-YYYY - DD-MM-YYYY');
    $scope.observationid = null;
    $scope.specialityDisplay = {
        speciality: '',
        subSpeciality: '',
        subSpecialityId: []
    }
    $scope.loadpatientDetails = function (mid, data) {
 
        $scope.embedPDF = false;

        $scope.modalId = mid;
        $scope.activeAppointmentId = data.AppointmentID;
        $scope.activeEnquiryId = data.Patientenquiryid;
        $scope.observationid = data.ReferrenceID;
        var userParam = {
            PatientID: data.PatientID,
            PatientEnquiryID: data.Patientenquiryid,
            USRId: doctorinfo.doctorid,
            USRType: "ADMIN",
            hospitalInfo: {
                HospitalID: parentHospitalId,
                HospitalCD: hospitalCode
            }
        }
        c7AddUpdatePatient.getPatientDetails(userParam, getpatientsuccess, aptError);
    }

    function getpatientsuccess(data) {

        $scope.embedPDF = true;

        if (data.Patient.length > 0) {
            $scope.aptPatientDetails = data;
            $scope.attachstatustype = '';
            if ($scope.aptPatientDetails.diagnosisReportURL.trim() == '') {
                $scope.aptPatientDetails.diagnosisReportURL = 'Not Availbale'
            }
            $scope.aptPatientDetails.EnquiryDetails.notes = '';
            $($scope.modalId).modal('show');
            $scope.modalId = '';
            if (data.EnquiryDetails.enquirystatusid == 'ISSUES') {
                $scope.attachstatustype = 'Issue';
                $scope.aptPatientDetails.EnquiryDetails.manageid = $scope.aptPatientDetails.EnquiryDetails.manageid.toString();
            }
            $scope.aptPatientDetails.EnquiryDetails.notes = '';
            // console.log($scope.aptPatientDetails);
            $scope.displaySpecialities($scope.aptPatientDetails.EnquiryDetails.specialitydetails);
        } else {
            alertify.error('No patient data found');
        }
    }

    function aptError(err) {
        console.log(err);
    }
   
    $scope.listing = function (hid, docid, phid) {
        var param = {
            DoctorID: docid,
            HospitalID: hid,
            StartDate:$scope.aptDate == undefined ? moment().format('YYYY-MM-DD') : moment($scope.aptDate.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD'),  
            EndDate: $scope.aptDate == undefined ? moment().format('YYYY-MM-DD') : moment($scope.aptDate.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD')
           
        }
        if (phid != undefined) {
            param.ParentHospitalID = phid;
        }
        c7AppointmentListServices.listHospitalAppointments(param, aptlistsuccess, aptError);
    }

    $scope.$on('EnquiryTabClick', function (evt, data) {
        if (data == 'appointment') {
            $scope.scancenterColl = '0';
            $scope.searchPatient = '';
            $scope.sonoColl = '';
            $scope.aptDate = moment().format('DD-MM-YYYY - DD-MM-YYYY');
            $scope.listing(0, 0, parentHospitalId);
        }
    })
    // $scope.listing(parentHospitalId, $scope.addressId);


    c7EnquiriesServices.listManageStatus(isserrSuccess, aptError);

    function isserrSuccess(data) {
        $scope.issueCollection = data.filter(data => data.managetype == 'ISSUES');
    }
    c7AdminServices.listdoctorhospital(parentHospitalId, sonolistSuccess, aptError);

    function sonolistSuccess(data) {
        // console.log(data);
        $scope.doctorList = data;
    }

    var hoslistparam = {
        "pageNumber": $scope.pageNumber,
        "HospitalID": parentHospitalId,
        "pageSize": 1000,
        "IncludeDoctors": "N",
        "IncludeBranch": "Y",
        "ActiveHospital": "Y",
        "ActiveDoctor": "Y"

    }
    $scope.attachstatustype = '';
    c7AdminServices.getHospitals(hoslistparam, onGetHospitalsSuccess, aptError);

    function onGetHospitalsSuccess(data) {
        $scope.hospitalCollection = data.rows;
    }

    function aptlistsuccess(data) {
        // console.log(data);
        // $scope.listCollection = data;
        $scope.listCollection = data.filter(data => data.AppointmentStatus != 'C' && data.AppointmentStatus != 'DNA');
        angular.forEach($scope.listCollection, function (val) {
            // if(val.ReferrenceID > 0 && val.AppointmentStatus == 'B') {
            //    val.ReferrenceID = 0; 
            // }
            val.title = val.PatientName.split('. ')[0];
            val.name = val.PatientName.split('. ')[1];
            val.req = false;
            val.reqstart = false;
            val.reqend = false;
            // if (val.Startscantime == '00:00') {
            //     val.Startscantime = '';
            // }
            // if (val.Completescantime == '00:00') {
            //     val.Completescantime = '';
            // }
        });
        // console.log($scope.listCollection);
        setTimeout(function () {
            $('[data-toggle="tooltip"]').tooltip();
        }, 500);
    }
    $scope.changeFilter = function (data) {
        if (data == 'date') {
            $scope.scancenterColl = '';
            $scope.sonoColl = '';
            $scope.listing(0, 0, parentHospitalId);
        }
        if (data == 'sono') {
            $scope.scancenterColl = '';
            $scope.listing(0, parseInt($scope.sonoColl), parentHospitalId);
        }
        if (data == 'scancenter') {
            $scope.sonoColl = '';
            $scope.listing(parseInt($scope.scancenterColl), 0, undefined);
        }
    }
    
    $scope.updateAptStatus = function (data) {
        if (data.AppointmentStatus == 'SC' && data.Startscantime == '00:00' && data.Completescantime == '00:00') {
            alertify.error('Please choose proper timing');
            return false;
        }
        if (data != undefined && !data.req) {
            $scope.updateparam = {
                AppointmentID: data.AppointmentID,
                AddressID: data.SonographerAddressID,
                AddressConsultID: data.AddressConsultID,
                DayCD: data.DayCD,
                ScheduleDate: data.ScheduleDate,
                StartTime: data.StartTime,
                EndTime: data.EndTime,
                Startscantime: data.Startscantime,
                Completescantime: data.Completescantime,
                EmergencyStartTime: data.EmergencyStartTime,
                EmergencyEndTime: data.EmergencyEndTime,
                DoctorID: data.DoctorID,
                PatientID: data.PatientID,
                Patientenquiryid: (data.Patientenquiryid == null) ? 0 : data.Patientenquiryid,
                AppointmentStatus: data.AppointmentStatus,
                HospitalDetails: {
                    HospitalID: data.HospitalID,
                    HospitalCD: (data.HospitalCode == null) ? '' : data.HospitalCode,
                    BranchCD: data.BranchCode,
                    ParentHospitalID: data.ParentHospitalID
                },
                HospitalCD: (data.HospitalCode == null) ? '' : data.HospitalCode,
                BranchCD: data.BranchCode,
                Notes: data.Notes,
                NoOfArea: parseInt(data.NoOfArea),    
                USRId: doctorinfo.doctorid,
                USRType: 'ADMIN'
              
            }
            var prefix = '';
            if ($scope.updateparam.AppointmentStatus == 'B') {
                prefix = 'Appointment Booked';
            } else if ($scope.updateparam.AppointmentStatus == 'A') {
                prefix = 'Patient Arrived';
            } else if ($scope.updateparam.AppointmentStatus == 'C') {
                prefix = 'Appointment Cancelled';
            } else if ($scope.updateparam.AppointmentStatus == 'DNA') {
                prefix = 'DNA';
            } else if ($scope.updateparam.AppointmentStatus == 'SC') {
                prefix = 'Scan Completed';
            } else if ($scope.updateparam.AppointmentStatus == 'FC') {
                prefix = 'Feedback Completed';
            }
            // $scope.updateparam.Notes = '';
            c7EnquiriesServices.addupdateHospitalAppointments($scope.updateparam, aptupdateSuccess, aptError);
            // console.log($scope.updateparam);
        }
    }


    function aptupdateSuccess(data) {
         console.log(data);
         if(data.AppointmentStatus == "C" || data.AppointmentStatus == "DNA")
         {
             $('#dropdowncancel').modal('show');
             $scope.emailData = {
                 sendsms: false
             }
         }
         else{
            $scope.listing(0, 0, parentHospitalId);
         }


        if(data =="Time Slot Already mapped"){
            alertify.error('Time Slot Already mapped')
        }
        if (data.error) {
            alertify.error('Appointment Save Error');
        }
        
    }

    $scope.SMSsend = function (data) {

        var paramSMS ={USRId: doctorinfo.doctorid, USRType: 'ADMIN', AppointmentID: data.AppointmentID};
        $scope.sendsms = ($scope.emailData.sendsms) ? true : false;
        
        if($scope.sendsms){
            c7EnquiriesServices.sendAppointmentSMS(paramSMS, SMSSuccess, enquiryError);
        }
        else{
            $scope.listing(0, 0, parentHospitalId);
        }
        var submitData = {
            patientid: data.PatientID,
            patientenquiryid: data.Patientenquiryid,
            usrid: doctorinfo.doctorid,
            statustypeid: 'appointSMS',
            appointmentdate: data.ScheduleDate,
            appointmenttime: data.StartTime != '00:00' ? data.StartTime : data.EmergencyStartTime,
            doctorid: data.DoctorID,
            appointmenthospitalid: data.HospitalID
        };
        if(data.AppointmentStatus == "C")
        {
            submitData['statustypeid'] = 'cancelSMS';
        }
        else if(data.AppointmentStatus == "DNA"){
            submitData['statustypeid'] = 'dnaSMS';
        }
        $scope.SMSUpdateParam = submitData;

    }
    function SMSSuccess(data) {
        if (data == "Success") {
            $scope.sendSMS = false;
            alertify.success('SMS Sent Successfully.');
            $scope.listing(0, 0, parentHospitalId);
            var submitData = $scope.SMSUpdateParam;
            c7EnquiriesServices.enquiryaddstatus(submitData, updateEnquiryStatus, enquiryError);

        } else {
            alertify.error('Error in SMS sending.');
            $scope.listing(0, 0, parentHospitalId);



        }

    }
    
    function enquiryError(data)
    {
        alertify.error(JSON.stringify(data));

    }
    function updateEnquiryStatus(data) {
        if (data != "Success") {
            alertify('Status update error');
        }
    }  


    $scope.onTimeSet = function (ne, old, id, index) {
        $('#' + id).dropdown('toggle');
        if ($scope.listCollection[index].Startscantime != '00:00' && $scope.listCollection[index].Completescantime != '00:00') {
            $scope.listCollection[index].req = false;
            // $scope.requiredData = true;
        } else {
            $scope.listCollection[index].req = true;
        }
    }
    $scope.setReq = function (index) {
        if ($scope.listCollection[index].AppointmentStatus == 'SC') {
            $scope.listCollection[index].req = true;
            $scope.listCollection[index].reqstart = true;
            $scope.listCollection[index].reqend = true;
            // $scope.requiredData = true;

            if ($scope.listCollection[index].Startscantime != '00:00' && $scope.listCollection[index].Completescantime != '00:00') {
                $scope.listCollection[index].req = false;
            } else {
                $scope.listCollection[index].req = true;
            }
        } else {
            $scope.listCollection[index].req = false;
            $scope.listCollection[index].reqstart = false;
            $scope.listCollection[index].reqend = false;
            // $scope.requiredData = true;
        }
    }

    // $scope.beforeRender = function ($view, $dates, minMinutes) {
    $scope.beforeRender = function ($leftDate, $rightDate, $upDate, $view, $dates, minReg, minEmer, equ) {
        $leftDate.selectable = false;
        $rightDate.selectable = false;
        if ($view == 'minute') {
            $upDate.display = 'Go Back';
            $upDate.selectable = true;
        } else {
            $upDate.selectable = false;
        }
        var minMinutes = (minReg == '00:00') ? minEmer : minReg;
        if ($view == "minute" && minMinutes != undefined && equ) {
            $dates.filter(function (date) {
                return date.display <= minMinutes;
            }).forEach(function (date) {
                date.selectable = false;
            });
        }
        if ($view == "minute" && minMinutes != undefined) {
            $dates.filter(function (date) {
                return date.display < minMinutes;
            }).forEach(function (date) {
                date.selectable = false;
            });
        }
    }

    $scope.startDateOnSetTime = function () {
        $scope.$broadcast('start-date-changed');
    }

    $scope.startDateBeforeRender = function ($view, $dates, minReg, minEmer) {
        var minMinutes = (minReg == '00:00') ? minEmer : minReg;
        if ($view == "hour" && minMinutes != undefined) {
            $dates.filter(function (date) {
                return date.display.split(':')[0] < minMinutes.split(':')[0];
            }).forEach(function (date) {
                date.selectable = false;
            });
        }
    }




    $scope.convertDateymd = function (date) {
        return moment(date).format('YYYY-MM-DD');
    }
    $scope.updateReport = function () {
        if (typeof $scope.aptPatientDetails.EnquiryDetails.manageid == "string") {
            $scope.aptPatientDetails.EnquiryDetails.manageid = parseInt($scope.aptPatientDetails.EnquiryDetails.manageid);
        }
        if ($scope.attachstatustype == 'Publish') {
            $scope.aptPatientDetails.EnquiryDetails.manageid = 18;
            $scope.aptPatientDetails.EnquiryDetails.enquirystatusid = "COMPLETE";
            $scope.aptPatientDetails.AppointmentDetails = {
                AppointmentID: $scope.activeAppointmentId,
                isPublishAction: true,
                PublishDate: moment().format('YYYY-MM-DD')
            }
        } else if ($scope.attachstatustype == 'Issue') {
            $scope.aptPatientDetails.EnquiryDetails.enquirystatusid = "ISSUES";
            // var prefix = $scope.issueCollection.filter(data => data.manageid == $scope.aptPatientDetails.EnquiryDetails.manageid)[0].managedesc;
            // if ($scope.aptPatientDetails.EnquiryDetails.notes.trim() != '') {
            //     $scope.aptPatientDetails.EnquiryDetails.notes = prefix + ': ' + $scope.aptPatientDetails.EnquiryDetails.notes;
            // }
        }


        if ($scope.aptPatientDetails.EnquiryDetails.manageid != undefined && !Number.isInteger($scope.aptPatientDetails.EnquiryDetails.manageid)) {
            $scope.aptPatientDetails.EnquiryDetails.manageid = parseInt($scope.aptPatientDetails.EnquiryDetails.manageid);
        }

        $scope.aptPatientDetails.EnquiryDetails.patientenquiryid = $scope.activeEnquiryId;
        $scope.aptPatientDetails.Patient[0].DOB = $scope.convertDateymd($scope.aptPatientDetails.Patient[0].DOB);
        $scope.aptPatientDetails.EnquiryDetails.referalcreated = $scope.convertDateymd($scope.aptPatientDetails.EnquiryDetails.referalcreated);
        $scope.aptPatientDetails.EnquiryDetails.referalreceived = $scope.convertDateymd($scope.aptPatientDetails.EnquiryDetails.referalreceived);
        $scope.aptPatientDetails.EnquiryDetails.referalprocessed = $scope.convertDateymd($scope.aptPatientDetails.EnquiryDetails.referalprocessed);
        var urlcoll = {};
        if ($scope.aptPatientDetails.EnquiryDetails.enquiryfileurl.enquiry_doc != undefined) {
            urlcoll.enquiry_real = $scope.aptPatientDetails.EnquiryDetails.enquiryfileurl.enquiry_doc;
        } else {
            urlcoll.enquiry_real = '';
        }
        if ($scope.aptPatientDetails.EnquiryDetails.enquiryfileurl.additional_doc != undefined) {
            urlcoll.additional_real = $scope.aptPatientDetails.EnquiryDetails.enquiryfileurl.additional_doc;
        } else {
            urlcoll.additional_real = '';
        }
        $scope.aptPatientDetails.EnquiryDetails.enquiryfile = urlcoll;
        delete $scope.aptPatientDetails.EnquiryDetails['enquiryfileurl'];
        angular.forEach($scope.aptPatientDetails.EnquiryDetails.specialitydetails, function (i) {
            i.activeInd = 'Y';
        });
        $scope.aptPatientDetails.EnquiryDetails.crtusr = doctorinfo.doctorid;

        mapdataUpdate($scope.aptPatientDetails);
    }

    function mapdataUpdate(data) {
        var email = undefined;
        if (data.PatientEmail.length > 0) {
            email = [{
                EmailID: data.PatientEmail[0].EmailID,
                PrimaryIND: data.PatientEmail[0].PrimaryIND,
                EmailIDType: data.PatientEmail[0].EmailIDType,
            }]
        }
        var updatedData = {
            USRType: "ADMIN",
            USRId: doctorinfo.doctorid,
            patientDetails: {
                Title: data.Patient[0].Title,
                FirstName: data.Patient[0].FirstName,
                LastName: data.Patient[0].LastName,
                Age: data.Patient[0].CurrentAge.year,
                DOB: moment(data.Patient[0].DOB).format('YYYY-MM-DD'),
                FullName: data.Patient[0].FullName,
                PatientID: data.Patient[0].PatientID,
                Gender: data.Patient[0].Gender,
                TermsAccepted: "Y",
                FamilyHeadIndicator: "Y",
                EnquiryDetails: data.EnquiryDetails,
                PatientOtherDetails: {
                    AadhaarID: data.Patient[0].PatientOtherDetails[0].AadhaarID
                }
            },
            patientAddress: data.PatientAddress,
            patientEmail: email,
            patientPhone: data.PatientPhone,
            patientReferral: data.patientReferral
        }
        if (data.AppointmentDetails) {
            updatedData.AppointmentDetails = data.AppointmentDetails
        }
        c7AddUpdatePatient.updatePatient(updatedData, patientupdatesuccess, aptError);
    }

    function patientupdatesuccess(data) {
        if (!data.error) {
            $scope.listing(0, 0, parentHospitalId);

            // $scope.aptPatientDetails.patientReferral.meshidentifier = 'R7QOT004';
            // $scope.aptPatientDetails.patientReferral.observationid = '61';
            if ($scope.aptPatientDetails.patientReferral != undefined &&
                $scope.aptPatientDetails.patientReferral.meshidentifier != undefined &&
                $scope.aptPatientDetails.patientReferral.meshidentifier == '') {
                if ($scope.aptPatientDetails.EnquiryDetails.enquirystatusid != 'ISSUES') {
                    $scope.publishToNoMesh();
                }
            } else if ($scope.aptPatientDetails.patientReferral && $scope.aptPatientDetails.patientReferral.meshidentifier &&
                $scope.observationid && $scope.observationid != 0) {
                $scope.publishToMesh();
            }
        }
    }

    $scope.publishToMesh = function () {
        var params = {
            hl7ReferenceID: parseInt($scope.observationid),
            toMailBoxID: $scope.aptPatientDetails.patientReferral.meshidentifier,
            USRId: doctorinfo.doctorid,
            usrtype: "ADMIN"
        }

        c7Hl7ToMeshServices.publishHl7ToMesh(params, publishSuccess, publishError);
    }
    $scope.publishToNoMesh = function () {
        var params = {
            responseId: parseInt($scope.observationid),
            PracticeID: $scope.aptPatientDetails.patientReferral.practiceid,
        }
        // usrid: doctorinfo.doctorid, 
        // usrtype: "ADMIN"

        c7Hl7ToMeshServices.sendAttachmentMail(params, publishSuccess, publishError);
    }

    function publishSuccess(data) {
        if (data == 'MESSAGE_ACCEPTED') {
            alertify.success('Published Successfully');
        } else if (data == 'Success') {
            alertify.success('Published Successfully & Email Sent');
        } else if (data == 'INVALID_MAILBOXID_AUTHENTICATION') {
            alertify.success('INVALID_MAILBOXID_AUTHENTICATION');
        }
        if (data.error) {
            publishError(data);
        }
    }

    function publishError(error) {
        if (error == 'INVALID_REFERENCE_ID') {
            alertify.error('Invalid Reference Id');
        } else if (error == 'REPORT_NOT_FOUND') {
            alertify.error('Report Not Found');
        } else if (error == 'MESSAGE_NOT_DELIVERY') {
            alertify.error('Message Not Delivered');
        } else if (error == 'INVALID_MAILBOXID_AUTHENTICATION') {
            alertify.error('Invalid MailBox Id');
        } else {
            alertify.error('Error Occurred');
        }
    }
    // Speciality
    c7AdminServices.SpecialityList(parentHospitalId, specialitylistSuccess, publishError)

    function specialitylistSuccess(data) {
        $scope.specialityCollection = data;
    }
    $scope.updateReq = function (data) {
        checktime($scope.listCollection[data].Startscantime);
        checktime($scope.listCollection[data].Completescantime);
    }
    $scope.displaySpecialities = function (data) {
        if (data != undefined && data != null) {
            $scope.specialityDisplay = {
                speciality: '',
                subSpeciality: '',
                subSpecialityId: []
            }
            var activeSpeciality = {};
            var activeSubSpeciality = [];
            var values = [];
            angular.forEach(data, function (i) {
                activeSpeciality = $scope.specialityCollection.filter(d => d.specialityID == i.specialityid)[0];

                if ($scope.specialityDisplay.speciality == '') {
                    $scope.specialityDisplay.speciality = activeSpeciality.specialitydesc;
                } else {
                    $scope.specialityDisplay.speciality = $scope.specialityDisplay.speciality + ', ' + activeSpeciality.specialitydesc;
                }
                activeSubSpeciality = activeSpeciality.subspeciality;
                values = Object.values(i.subSpecialities);
                angular.forEach(values, function (j) {
                    var sub = activeSubSpeciality.filter(x => x.DoctorSpecialityID == j)[0];
                    if ($scope.specialityDisplay.subSpeciality == '') {
                        $scope.specialityDisplay.subSpeciality = sub.DoctorSpecialityDesc;
                    } else {
                        $scope.specialityDisplay.subSpeciality = $scope.specialityDisplay.subSpeciality + ', ' + sub.DoctorSpecialityDesc;
                    }
                });
                $scope.specialityDisplay.subSpecialityId = $scope.specialityDisplay.subSpecialityId.concat(values);
            });
        }
        // console.log($scope.specialityDisplay.subSpecialityId);
    }
})