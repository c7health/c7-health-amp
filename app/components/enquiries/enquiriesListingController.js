hCueDoctorWebControllers.controller('enquiriesListingController', function ($scope, $rootScope, $http, alertify, $window, $location, hcueDoctorLoginService, c7EnquiriesServices, c7AddUpdatePatient, datacookieFactory, referralregion, c7AdminServices, referralccg, c7PostCodeServices, c7ReminderServices) {
    //  Check login Params
    // var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
    // userLoginDetails = datacookieFactory.getItem('loginData');
    // if (userLoginDetails.loggedinStatus === 'true') {
    //     $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    // } else {
    //     $location.path('/login');
    // }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
        docid = doctorinfo.doctorid;
    }
    $scope.aptfilterStatus = 'ALL';
    $scope.filterStatus = 'ALL';
    $scope.searchPatient = '';
    $scope.iss = null;
    $scope.rej = null;
    $scope.issue = null;
    $scope.reject = null;
    $scope.selectedLatitude = null;
    $scope.selectedLongitude = null;
    $scope.showPatientEnquiry = undefined;
    $scope.manageTabs = 'manageStatus';
    $scope.sla = 0;
    $scope.specialityDisplay = {
        speciality: '',
        subSpeciality: '',
        subSpecialityId: []
    }
    $scope.searchRadius = '50000';
    $scope.specialityColl = [];
    $scope.reSchedule = {
        status: false,
        startTime: '',
        endTime: '',
        eStartTime: '',
        eEndTime: '',
        scheduleDate: '',
        hospitalId: undefined
    }
    $scope.emailName = '';
    $scope.toggleModal = function (id) {
        // $(id).modal('show');
        $scope.modalId = id;
    }
    //listing enquiry services
    $scope.enquiriesCollection = [];
    $scope.downloadData = [];
    // var userParam = {
    //     "usrid":doctorinfo.doctorid,
    //     "usrtype":"ADMIN"
    // }
    $scope.Regpatient = function (data) {
        datacookieFactory.remove('editPatientId');
        datacookieFactory.remove('editPatientEnquiryId');
        var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
        var url = "/addUpdatePatient?kwid=" + (guid || "");
        $location.url(url);
    }
    $scope.enquiriesList = function (data) {
        if ($scope.selectedRegion != '0') {
            $scope.ccgList = $scope.allccgList.filter(v => v.regionid === parseInt($scope.selectedRegion));
        } else {
            $scope.ccgList = $scope.allccgList;
        }
        $scope.enquiriesListParam = {
            HospitalCode: hospitalCode,
            pagenumber: 1,
            pagesize: 10,
            enquirystatus: $scope.filterStatus,
            appointmenttype: $scope.aptfilterStatus,
            searchtext: '',
            regionid: ($scope.selectedRegion == undefined) ? 0 : parseInt($scope.selectedRegion),
            ccgid: ($scope.selectedCCG == undefined) ? 0 : parseInt($scope.selectedCCG),
            sortcolumn: "referalreceived",           
            sortorder: "DESC"
        }       
        
        if (data == 'region') {
            $scope.enquiriesListParam['ccgid'] = 0;
            $scope.selectedCCG = '0';
        }
        $scope.hideloadMore = false;
        c7EnquiriesServices.listAllEnquiry($scope.enquiriesListParam, listAllEnquirySuccess, enquiryError);
    }
    $scope.nextPage = function () {
        $scope.enquiriesListParam.pagenumber += 1;
        $scope.enquiriesListParam.enquirystatus = $scope.filterStatus;
        $scope.enquiriesListParam.appointmenttype = $scope.aptfilterStatus;
        c7EnquiriesServices.listAllEnquiry($scope.enquiriesListParam, listAllEnquirySuccessnext, enquiryError);
    }

    function getRegionList() {
        var userParam = {
            hospitalcd: hospitalCode,
            usrid: doctorinfo.doctorid,
            usrtype: 'ADMIN',
            pageNumber: 0,
            pageSize: 10000,
        };

        referralregion.listRegion(userParam, regionListSuccess, enquiryError);
    }

    function regionListSuccess(data) {
        $scope.regionList = data.filter(v => v.active);
        // var allOption = {
        //   regionid: 0,
        //   regionname: ' All',
        //   active: true
        // }   
        // $scope.regionList.unshift(allOption);
    }
    $scope.selectedRegion = '0';
    $scope.selectedCCG = '0';
    
    getRegionList();

    function getCCGList() {
        var referralParam = {
            usrid: doctorinfo.doctorid,
            usrtype: "ADMIN",
            pageNumber: 0,
            pageSize: 10000,
            hospitalcd: hospitalCode,
        };

        // Listing Referal details Services
        referralccg.alllistccg(referralParam, ccglistsuccess, enquiryError);

        function ccglistsuccess(data) {
            $scope.allccgList = angular.copy(data.filter(v => v.active));
            $scope.ccgList = angular.copy(data.filter(v => v.active));
        }
    }
    getCCGList()
    $scope.$on('EnquiryTabClick', function (evt, data) {
        if (data == 'enquiry') {
            $scope.searchPatient = '';
            $scope.enquiriesList();
        }
    });
    $scope.enquiriesList()

    function listAllEnquirySuccess(data) {
        $('#loaderDiv').hide();
        if (data != 'Error' || !data.error) {
            if (data.length < 10) {
                $scope.loadMore = false;
            } else {
                $scope.loadMore = true;
            }
            data = data.filter(data => data.enquirystatusid != 'REJECT');
            $scope.enquiriesCollection = data;
        } else {
            alertify.error('Enquiry list Error');
        }
    }
    $scope.loadMore = true;

    function listAllEnquirySuccessnext(data) {
        $('#loaderDiv').hide();
        if (data.length < 10) {
            $scope.loadMore = false;
        } else {
            $scope.loadMore = true;
        }
        data = data.filter(data => data.enquirystatusid != 'REJECT');
        $scope.enquiriesCollection = $scope.enquiriesCollection.concat(data);;
    }

    function enquiryError(data) {
        console.log(data);
        alertify.error('Error');
    }

    // Speciality
    c7AdminServices.SpecialityList(parentHospitalId, specialitylistSuccess, enquiryError)

    function specialitylistSuccess(data) {
        $scope.specialityCollection = data;
    }
    $scope.displaySpecialities = function (data) {
        if (data != undefined) {
            $scope.specialityDisplay = {
                speciality: '',
                subSpeciality: '',
                subSpecialityId: []
            }
            var activeSpeciality = {};
            var activeSubSpeciality = [];
            var values = [];
            angular.forEach(data, function (i) {
                activeSpeciality = $scope.specialityCollection.filter(d => d.specialityID == i.specialityid)[0];

                if(activeSpeciality != null && activeSpeciality != undefined){
                    if ($scope.specialityDisplay.speciality == '') {
                        $scope.specialityDisplay.speciality = activeSpeciality.specialitydesc;
                    } else {
                        $scope.specialityDisplay.speciality = $scope.specialityDisplay.speciality + ', ' + activeSpeciality.specialitydesc;
                    }
                    activeSubSpeciality = activeSpeciality.subspeciality;
                }
                
                values = Object.values(i.subSpecialities);
                angular.forEach(values, function (j) {
                    var sub = activeSubSpeciality.filter(x => x.DoctorSpecialityID == j)[0];
                    if(sub != null && sub != undefined){
                        if ($scope.specialityDisplay.subSpeciality == '') {
                            $scope.specialityDisplay.subSpeciality = sub.DoctorSpecialityDesc;
                        } else {
                            $scope.specialityDisplay.subSpeciality = $scope.specialityDisplay.subSpeciality + ', ' + sub.DoctorSpecialityDesc;
                        }
                    }
                    
                });
                $scope.specialityDisplay.subSpecialityId = $scope.specialityDisplay.subSpecialityId.concat(values);
            });
        }
        // console.log($scope.specialityDisplay.subSpecialityId);
    }
    $scope.editPatient = function (patientId) {
        datacookieFactory.set('editPatientId', patientId);
        datacookieFactory.set('editPatientEnquiryId', $scope.activeEnquiryId);

        var updateStatusParam = {
            patientid: patientId,
            patientenquiryid: $scope.activeEnquiryId,
            statustypeid: 'EnquirySeen',
            usrid: doctorinfo.doctorid
        }

        c7EnquiriesServices.enquiryaddstatus(updateStatusParam, editupdateEnquiryStatus, enquiryError);
    }

    function editupdateEnquiryStatus(data) {
        if (data == 'Success') {
            editPatientDetails();
        }
    }
    $scope.EnquiryStatus = false;

    $scope.getPatient = function (id, enquiryid, regionid, stat, enquiryStatus) {

        $scope.validateAttachStatus = {
            type: false,
            reason: false
        }
        $scope.validateManageStatus = {
            type: false,
            reason: false,
            stat: false
        }
        $scope.activeEnquiryId = enquiryid;
        $scope.activePatientId = id;
        var userParam = {
            PatientID: id,
            PatientEnquiryID: enquiryid,
            USRId: doctorinfo.doctorid,
            USRType: "ADMIN",
            hospitalInfo: {
                HospitalID: parentHospitalId,
                HospitalCD: hospitalCode
            }
        }
        var updateStatusParam = {
            patientid: id,
            patientenquiryid: enquiryid,
            usrid: doctorinfo.doctorid,
        };
        if (stat == 'rf') {
            updateStatusParam.statustypeid = 'ReferralForm'
        } else if (stat == 'mf') {
            updateStatusParam.statustypeid = 'ManageForm'
        }
        if (enquiryStatus == 'BOOKED' || enquiryStatus == 'SCANCOMPLETED') {
            $scope.EnquiryStatus = true;
        } else {
            $scope.EnquiryStatus = false;
        }
        $scope.getRegionToEdit(regionid);
        $scope.patientedit = true;

        $scope.embedPDF = false;

        c7AddUpdatePatient.getPatientDetails(userParam, getpatientsuccess, enquiryError);
        c7EnquiriesServices.listManageStatus(isserrSuccess, enquiryError);
        c7EnquiriesServices.enquiryaddstatus(updateStatusParam, updateEnquiryStatus, enquiryError);
    }

    function getpatientsuccess(data) {

        $scope.embedPDF = true;

        // console.log(data);
        if (!data.error) {
            $scope.patientDetails = data;
            $scope.currentStatus = angular.copy($scope.patientDetails.EnquiryDetails.enquirystatusid);
            if ($scope.patientDetails.EnquiryDetails.manageid != undefined && $scope.patientDetails.EnquiryDetails.manageid != null) {
                if ($scope.issueCollection.filter(data => data.manageid == $scope.patientDetails.EnquiryDetails.manageid).length > 0) {
                    $scope.attachstatustype = 'Issue';
                    $scope.manageenquirystatus = 'Issue';
                } else if ($scope.rejectCollection.filter(data => data.manageid == $scope.patientDetails.EnquiryDetails.manageid).length > 0) {
                    $scope.attachstatustype = 'Reject';
                    $scope.manageenquirystatus = 'Reject';
                } else if ($scope.exceptionCollection.filter(data => data.manageid == $scope.patientDetails.EnquiryDetails.manageid).length > 0) {
                    $scope.attachstatustype = 'ExceptionNotes';
                    $scope.manageenquirystatus = 'ExceptionNotes';
                } else if ($scope.patientDetails.EnquiryDetails.manageid == $scope.accepts.manageid) {
                    $scope.attachstatustype = 'Accept';
                } else {
                    $scope.attachstatustype = '';
                    $scope.manageenquirystatus = '';
                }
                $scope.patientDetails.EnquiryDetails.notes = ($scope.patientDetails.EnquiryDetails.notes == undefined) ? '' : $scope.patientDetails.EnquiryDetails.notes;
                // if ($scope.patientDetails.EnquiryDetails.notes != '') {
                //     var x = $scope.patientDetails.EnquiryDetails.notes.split(': ')[1];
                //     if (x != undefined) {
                //         $scope.patientDetails.EnquiryDetails.notes = $scope.patientDetails.EnquiryDetails.notes.split(': ')[1];
                //     }
                // }
                $scope.patientDetails.EnquiryDetails.manageid = $scope.patientDetails.EnquiryDetails.manageid.toString();
                //$scope.patientDetails.EnquiryDetails.notes = '';
            }
            if ($scope.modalId == '#manage') {
                $scope.attachstatustype = '';
            } else if ($scope.modalId == '#attach') {
                $scope.manageenquirystatus = '';
            }
            $($scope.modalId).modal('show');
            $scope.modalId = '';

            $scope.sla += 1;
            if ($scope.sla == 2) {
                $scope.calcSla();
                $scope.sla = 0;
            }
            loadccg($scope.patientDetails.patientReferral.ccgid);
            loadspeciality()
            $scope.displaySpecialities($scope.patientDetails.EnquiryDetails.specialitydetails);
        } else {
            alertify.error('patient error');
        }
    }

    function isserrSuccess(data) {
        $scope.AllStatusColl = angular.copy(data);
        $scope.issueCollection = data.filter(data => data.managetype == 'ISSUES');
        $scope.rejectCollection = data.filter(data => data.managetype == 'REJECT');
        $scope.exceptionCollection = data.filter(data => data.managetype == 'EXCEPTIONNOTES');
        $scope.accepts = data.filter(data => data.managetype == 'ACCEPT')[0];
    }

    function updateEnquiryStatus(data) {
        if (data != "Success") {
            alertify.error('Status update error');
        }
    }
    $scope.openModal = function (id) {
        $scope.modalId = id;
        $scope.manageTabs = 'bookAppointment';
        if (id == '#manage' && $scope.reSchedule.status == true) {
            $scope.manageTabs = 'bookedAppointment';
        }
        setTimeout(function () {
            $($scope.modalId).modal('show');
            $scope.modalId = '';
        }, 1000);
    }


    $scope.setReminderDetails = function (patientenquiryid, patientid, popup) {

        $scope.activeEnquiryId = patientenquiryid;
        $scope.activePatientId = patientid;
        $scope.reminderNotes = null;
        $scope.reminderDate = null;
        if (popup) {
            $(popup).modal('show');
        }
    }

    $scope.submitReminder = function () {

        $('#reminderpopup').modal('hide');
        var param = {
            patientid: $scope.activePatientId,
            patientenquiryid: $scope.activeEnquiryId,
            remindernotes: $scope.reminderNotes,
            reminderduedate: moment($scope.reminderDate, 'DD-MM-YYYY').format('YYYY-MM-DD')
        };

        var enquiryParam = {
            patientid: $scope.activePatientId,
            patientenquiryid: $scope.activeEnquiryId,
            usrid: doctorinfo.doctorid,
            statustypeid: "ReminderAdded",
            notes: $scope.reminderNotes,
            reminderduedate: moment($scope.reminderDate, 'DD-MM-YYYY').format('YYYY-MM-DD')
        };

        c7ReminderServices.addReminder(param, addReminderSuccess, addReminderError);
        c7EnquiriesServices.enquiryaddstatus(enquiryParam, updateEnquiryStatus, enquiryError);

    }

    function addReminderSuccess(data) {
        if (data == 'Success') {
            // $scope.enquiriesList();
        } else {
            addReminderError(data);
        }
    }

    function addReminderError(data) {
        console.log(data);
    }

    $('body').on('change', '#UploadDocument1', function (e) {
        var droppedFiles = e.target.files;
        uploadFile(droppedFiles);
    });

    uploadFile = function (data) {
        var file = data[0];

        if (file == undefined) {
            return;
        }
        var formData = new FormData();
        $scope.fileurl = file.name;

        formData.append("fileUpload", file);
        formData.append("typeDoc", "LettersDocument");
        formData.append("patientID", $scope.activePatientId);
        formData.append("patientenquiryID", $scope.activeEnquiryId);
        var ext = file.name.split('.');
        formData.append("fileExtn", ext[ext.length - 1].toLowerCase());
        $scope.fileType = ext[ext.length - 1];

        formData.append("timestamp", moment().valueOf());
        formData.append("fileName", file.name.toLowerCase());

        c7AddUpdatePatient.uploadLetterDocument(formData, uploadSuccess, uploadError);

        document.getElementById('UploadDocument1').value = '';

    }

    function uploadError(data) {
        alertify.error(data);
    }

    function uploadSuccess(data) {
        if (data.url) {
            alertify.success('Upload Success');
            uploadFileToEnquiry(data.url);
        } else {
            alertify.error('Upload Error');
        }
    }


    function uploadFileToEnquiry(urlString) {
        var uploadParam = {
            patientid: $scope.activePatientId,
            patientenquiryid: $scope.activeEnquiryId,
            usrid: doctorinfo.doctorid,
            statustypeid: "DocumentUpload",
            documenturl: urlString
        };

        c7EnquiriesServices.enquiryaddstatus(uploadParam, updateEnquiryStatus, enquiryError);
    }


    function editPatientDetails() {
        $('#attach').modal('hide');
        $('#manage').modal('hide');
        $('.modal-backdrop').remove();
        $('body').removeClass('modal-open');
        $('body').css('padding-right', '');
        var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
        // var url = "/registerPatient?kwid=" + (guid || "");
        var url = "/addUpdatePatient?kwid=" + (guid || "");
        $location.url(url);
    }

    // $scope.subListingType = false;
    // Patient history listing
    $scope.showSublist = function (id, enquiryid, index, aptStatus, attachview) {
        $scope.activeEnquiryId = enquiryid;
        $scope.activePatientId = id;
        if ($scope.showPatientEnquiry == index) {
            $scope.showPatientEnquiry = undefined;
        } else {
            if(attachview !="attachment view")
            {
                $scope.toggleIndex = index;
            }

            // $('#loaderDiv').show();
            $scope.subListing = [];
            var subParam = {
                patientenquiryid: enquiryid,
            }
            // if (aptStatus == 'BOOKED') {
            //     subParam.Booked = 'Y';
            //     $scope.subListingType = true;
            // } else {
            //     $scope.subListingType = false;
            // }
            c7EnquiriesServices.listAppointmentStatus(enquiryid + '/' + docid, subappointmentlistSuccess, enquiryError);
            c7EnquiriesServices.listEnquiryStatus(subParam, sublistSuccess, enquiryError);
        }
    }
    $scope.sublistSussessCount = 0;

    function sublistSuccess(data) {
        // if ($scope.subListingType) {
        //     angular.forEach(data, function (val) {
        //         val.appointmentdate = moment(val.appointmentdate).format('DD-MM-YYYY HH:mm');
        //         val.addr = val.Address1 + ', ' + val.Address2 + ', ' + val.Address3 + ', ' + val.Address4;
        //     });
        //     $scope.subListing = data;
        // } else {
        // }
        angular.forEach(data, function (val) {
            val.dateNew = moment.utc(val.date, 'YYYY-MM-DD HH:mm:ss').local().format('DD-MM-YYYY HH:mm:ss');
            val.timestamp = moment(val.date, 'YYYY-MM-DD HH:mm:ss').valueOf();
            if (val.enquirystatusid == 'appointEmail' || val.enquirystatusid == 'exclusionEmail' || val.enquirystatusid == 'declinedEmail' || val.enquirystatusid == 'dnaEmail' || val.enquirystatusid == 'contactusEmail' || val.enquirystatusid == 'nocontactEmail' || val.enquirystatusid == 'urgentnocontactEmail') {
                val.emailobj = {
                    patientenquiryid: $scope.activeEnquiryId,
                    appointmentid: 0,
                    sendemail: true,
                    sendletter: true
                }
            }
        })
        $scope.subListing = $scope.subListing.concat(data);
        $scope.sublistSussessCount += 1;
        if ($scope.sublistSussessCount == 2) {
            $scope.subListing = $scope.subListing.sort(function (a, b) {
                return b.timestamp - a.timestamp;
            });
            $('#loaderDiv').hide();
            $scope.sublistSussessCount = 0;
        }
        $scope.showPatientEnquiry = $scope.toggleIndex;
        // console.log($scope.subListing);
    }

    function subappointmentlistSuccess(data) {
        angular.forEach(data, function (val) {
            val.dateNew = moment.utc(val.date, 'YYYY-MM-DD HH:mm:ss').local().format('DD-MM-YYYY HH:mm:ss');
            val.timestamp = moment(val.date, 'YYYY-MM-DD HH:mm:ss').valueOf();
            if (val.enquirystatusid == 'appointEmail' || val.enquirystatusid == 'exclusionEmail' || val.enquirystatusid == 'declinedEmail' || val.enquirystatusid == 'dnaEmail' || val.enquirystatusid == 'contactusEmail' || val.enquirystatusid == 'nocontactEmail' || val.enquirystatusid == 'urgentnocontactEmail') {
                val.emailobj = {
                    patientenquiryid: $scope.activeEnquiryId,
                    appointmentid: 0,
                    sendemail: true,
                    sendletter: true
                }
            }
        })
        $scope.subListing = $scope.subListing.concat(data);
        $scope.sublistSussessCount += 1;
        if ($scope.sublistSussessCount == 2) {
            $scope.subListing = $scope.subListing.sort(function (a, b) {
                return b.timestamp - a.timestamp;
            });
            $('#loaderDiv').hide();
            $scope.sublistSussessCount = 0;
        }
    }
    // Status Update 
    $scope.convertDateymd = function (date) {
        return moment(date).format('YYYY-MM-DD');
    }

    // function checkNotesValidaity() {
    //     var regExpression = /^[ \nA-Za-z0-9@~`!@#$%^&*{}()|_=+\\[\\';:\]"/?>.<,-]*$/;
    //     var notesCheck = $scope.patientDetails.EnquiryDetails.notes;
    //     var testResult = regExpression.test(notesCheck);
    //     return testResult;
    // }


    $scope.updatePatientStatus = function (modalid) {
       
        $(modalid).modal('hide');
        
        if (modalid == '#manage' && $scope.patientDetails.EnquiryDetails.enquirystatusid == "AWAITING" && $scope.patientDetails.EnquiryDetails.manageid == "0") {
            $scope.patientDetails.EnquiryDetails.manageid = $scope.accepts.manageid;
        }
        if ($scope.attachstatustype == 'Accept') {
            $scope.patientDetails.EnquiryDetails.enquirystatusid = "AWAITING";
            $scope.patientDetails.EnquiryDetails.manageid = $scope.accepts.manageid;
        } else if ($scope.attachstatustype == 'Issue' || $scope.manageenquirystatus == 'Issue') {
            $scope.patientDetails.EnquiryDetails.enquirystatusid = "ISSUES";
        } else if ($scope.attachstatustype == 'Reject' || $scope.manageenquirystatus == 'Reject') {
            $scope.patientDetails.EnquiryDetails.enquirystatusid = "REJECT";
        }
        if ($scope.patientDetails.EnquiryDetails.manageid != undefined && !Number.isInteger($scope.patientDetails.EnquiryDetails.manageid)) {
            $scope.patientDetails.EnquiryDetails.manageid = parseInt($scope.patientDetails.EnquiryDetails.manageid);
        }
        var notes = '';
        // if ($scope.patientDetails.EnquiryDetails.manageid > 0) {
        //     notes = $scope.AllStatusColl.filter(data => data.manageid == $scope.patientDetails.EnquiryDetails.manageid)[0].managedesc;
        // }
        var exceptionStatus = $scope.manageenquirystatus;
        $scope.attachstatustype = '';
        $scope.manageenquirystatus = '';
        $scope.patientDetails.EnquiryDetails.patientenquiryid = $scope.activeEnquiryId;
        $scope.patientDetails.Patient[0].DOB = $scope.convertDateymd($scope.patientDetails.Patient[0].DOB);
        $scope.patientDetails.EnquiryDetails.referalcreated = $scope.convertDateymd($scope.patientDetails.EnquiryDetails.referalcreated);
        $scope.patientDetails.EnquiryDetails.referalreceived = $scope.convertDateymd($scope.patientDetails.EnquiryDetails.referalreceived);
        $scope.patientDetails.EnquiryDetails.referalprocessed = $scope.convertDateymd($scope.patientDetails.EnquiryDetails.referalprocessed);
        var urlcoll = {};
        if ($scope.patientDetails.EnquiryDetails.enquiryfileurl.enquiry_doc != undefined) {
            urlcoll.enquiry_real = $scope.patientDetails.EnquiryDetails.enquiryfileurl.enquiry_doc;
        } else {
            urlcoll.enquiry_real = '';
        }
        if ($scope.patientDetails.EnquiryDetails.enquiryfileurl.additional_doc != undefined) {
            urlcoll.additional_real = $scope.patientDetails.EnquiryDetails.enquiryfileurl.additional_doc;
        } else {
            urlcoll.additional_real = '';
        }
        $scope.patientDetails.EnquiryDetails.enquiryfile = urlcoll;
        delete $scope.patientDetails.EnquiryDetails['enquiryfileurl'];

        // if ($scope.edit != undefined || $scope.lookupStatus) {
        //     if ($scope.equid != [] && $scope.equid != undefined && $scope.equid.length != 0) {
        //         newEnquiryAddStatus();
        //     }
        // $scope.lookupStatus = false;
        // }
        fillspeciality();
        newEnquiryAddStatus(exceptionStatus);
        $scope.patientDetails.EnquiryDetails.specialitydetails = $scope.newSpecialityCollection;

        if(!($scope.patientDetails.EnquiryDetails.enquirystatusid == 'REJECT' && $scope.rejectCollection.filter(data => data.manageid == $scope.patientDetails.EnquiryDetails.manageid).length > 0)){
            if ($scope.agespeciality.age || $scope.agespeciality.speciality) {
                $scope.patientDetails.EnquiryDetails.enquirystatusid = 'ISSUES';
                $scope.patientDetails.EnquiryDetails.manageid = 1;
            }
        }
        
        mapdataUpdate($scope.patientDetails, notes);
    }

    function mapdataUpdate(data, note) {

        var email;
        if (data.PatientEmail.length > 0) {
            email = [{
                EmailID: data.PatientEmail[0].EmailID,
                PrimaryIND: data.PatientEmail[0].PrimaryIND,
                EmailIDType: data.PatientEmail[0].EmailIDType
            }]
        } else {
            email = [];
        }

        var updatedData = {
            USRType: "ADMIN",
            USRId: doctorinfo.doctorid,
            patientDetails: {
                Title: data.Patient[0].Title,
                FirstName: data.Patient[0].FirstName,
                LastName: data.Patient[0].LastName,
                Age: data.Patient[0].CurrentAge.year,
                DOB: moment(data.Patient[0].DOB).format('YYYY-MM-DD'),
                FullName: data.Patient[0].FullName,
                PatientID: data.Patient[0].PatientID,
                Gender: data.Patient[0].Gender,
                TermsAccepted: "Y",
                FamilyHeadIndicator: "Y",
                EnquiryDetails: data.EnquiryDetails,
                PatientOtherDetails: {
                    AadhaarID: data.Patient[0].PatientOtherDetails[0].AadhaarID
                }
            },
            patientAddress: data.PatientAddress,
            patientEmail: email,
            patientPhone: data.PatientPhone,
            patientReferral: data.patientReferral
        }
        $scope.patientDetails.EnquiryDetails.notes = ($scope.patientDetails.EnquiryDetails.notes == undefined) ? '' : $scope.patientDetails.EnquiryDetails.notes;
        // if (updatedData.patientDetails.EnquiryDetails.notes.trim() != '') {
        //     updatedData.patientDetails.EnquiryDetails.notes = note + ': ' + updatedData.patientDetails.EnquiryDetails.notes;
        // }
        $scope.patientDetails.EnquiryDetails.crtusr = doctorinfo.doctorid;

        c7AddUpdatePatient.updatePatient(updatedData, patientupdatesuccess, enquiryError);
        if ($scope.patientDetails.EnquiryDetails.manageid == 1 || $scope.patientDetails.EnquiryDetails.manageid == 8) {
            $('#emailExclusion').modal('show');
            $scope.emailexclusion = {
                patientenquiryid: $scope.activeEnquiryId,
                appointmentid: 0,
                sendemail: true,
                sendletter: true
            }
            $scope.emailUpdateParam = {
                patientid: $scope.activePatientId,
                patientenquiryid: $scope.activeEnquiryId,
                usrid: doctorinfo.doctorid,
                statustypeid: "exclusionEmail"
            }

        } else if ($scope.patientDetails.EnquiryDetails.manageid == 6) {
            $('#emailconfirmationDeclined').modal('show');
            $scope.emailDataDeclined = {
                patientenquiryid: $scope.activeEnquiryId,
                appointmentid: 0,
                sendemail: true,
                sendletter: true
            }
            $scope.emailUpdateParam = {
                patientid: $scope.activePatientId,
                patientenquiryid: $scope.activeEnquiryId,
                usrid: doctorinfo.doctorid,
                statustypeid: 'declinedEmail'
            };
        } else if ($scope.patientDetails.EnquiryDetails.manageid == 20) {
            $('#emailconfirmationDNA').modal('show');
            $scope.sendPatientDNAMail = {
                patientenquiryid: $scope.activeEnquiryId,
                appointmentid: 0,
                sendemail: true,
                sendletter: true
            }
            $scope.emailUpdateParam = {
                patientid: $scope.activePatientId,
                patientenquiryid: $scope.activeEnquiryId,
                usrid: doctorinfo.doctorid,
                statustypeid: 'dnaEmail'
            };
        } else if ($scope.patientDetails.EnquiryDetails.enquirystatusid == 'LETTERSENT') {
            $('#emailcontactus').modal('show');
            $scope.emailcontactus = {
                patientenquiryid: $scope.activeEnquiryId,
                appointmentid: 0,
                sendemail: true,
                sendletter: true
            }
            $scope.emailUpdateParam = {
                patientid: $scope.activePatientId,
                patientenquiryid: $scope.activeEnquiryId,
                usrid: doctorinfo.doctorid,
                statustypeid: 'contactusEmail'
            };
        } else if ($scope.patientDetails.EnquiryDetails.manageid == 5 && $scope.patientDetails.EnquiryDetails.appointmenttypeid != 'URGENT' || $scope.patientDetails.EnquiryDetails.manageid == 12 && $scope.patientDetails.EnquiryDetails.appointmenttypeid != 'URGENT') {
            $('#emailnocontact').modal('show');
            $scope.emailnocontact = {
                patientenquiryid: $scope.activeEnquiryId,
                appointmentid: 0,
                sendemail: true,
                sendletter: true
            }
            $scope.emailUpdateParam = {
                patientid: $scope.activePatientId,
                patientenquiryid: $scope.activeEnquiryId,
                usrid: doctorinfo.doctorid,
                statustypeid: 'nocontactEmail'
            };
        } else if ($scope.patientDetails.EnquiryDetails.manageid == 5 && $scope.patientDetails.EnquiryDetails.appointmenttypeid == 'URGENT' || $scope.patientDetails.EnquiryDetails.manageid == 12 && $scope.patientDetails.EnquiryDetails.appointmenttypeid == 'URGENT') {
            $('#emailurgentnocontact').modal('show');
            $scope.emailurgentnocontact = {
                patientenquiryid: $scope.activeEnquiryId,
                appointmentid: 0,
                sendemail: true,
                sendletter: true
            }
            $scope.emailUpdateParam = {
                patientid: $scope.activePatientId,
                patientenquiryid: $scope.activeEnquiryId,
                usrid: doctorinfo.doctorid,
                statustypeid: 'urgentnocontactEmail'
            };
        } else if ($scope.patientDetails.EnquiryDetails.manageid == 13) {
            $('#emailconfirmationDeclinedReject').modal('show');
            $scope.emailRejectDeclined = {
                patientenquiryid: $scope.activeEnquiryId,
                appointmentid: 0,
                sendemail: true,
                sendletter: true
            }
            $scope.emailUpdateParam = {
                patientid: $scope.activePatientId,
                patientenquiryid: $scope.activeEnquiryId,
                usrid: doctorinfo.doctorid,
                statustypeid: 'patientDeclinedEmail'
            };
        }
        // if($scope.patientDetails.EnquiryDetails.manageid == 20) {
        //     $('#emailconfirmationDNA').modal('show');
        //     $scope.emailDataDNA = {
        //         patientenquiryid: $scope.activeEnquiryId,
        //         appointmentid: 0, 
        //         sendemail: false,
        //         sendletter: false
        //     }
        // }
        // $scope.emailUpdateParam = {
        //     patientid: $scope.activePatientId,
        //     patientenquiryid: $scope.activeEnquiryId,
        //     usrid: doctorinfo.doctorid,
        //     statustypeid: 'appemail'
        // };
    }

    function patientupdatesuccess(data) {
        if(data == "Unable to change enquiry status,please refresh enquiries screen"){
            alertify.error("Unable to change enquiry status,please refresh enquiries screen");
        }
        if (!data.error) {
            $scope.enquiriesList();
            $scope.showPatientEnquiry = undefined;

            // $scope.emailexclusion = {
            //     patientenquiryid: 0,
            //     appointmentid: 0,
            //     sendemail: true,
            //     sendletter: true
            // }
            // $scope.emailUpdateParam = {
            //     patientid: 0,
            //     patientenquiryid: 0,
            //     usrid: docid,
            //     statustypeid: "exclusionEmail"
            // }
            // $scope.agespeciality = {
            //     age: false,
            //     speciality: false
            // }
            // $scope.validator = {
            //     minAge: undefined,
            //     speciality: [],
            //     status: false
            // }
        }
    }

    // Nearest clinic listing
    $('body').on('hidden.bs.modal', '#manage', function (e) {
        $scope.manageTabs = 'manageStatus';
    });
    $('body').on('hidden.bs.modal', '.modal', function (e) {
        $('#loaderDiv').hide();
    });
    // $('body').on('shown.bs.modal', '.modal', function () {
    //     setTimeout(function(){
    //         $('body').addClass('modal-open').css('padding-right','10px');
    //     }, 500);
    // });

    $scope.getNearHospitalList = function (newpostcode, lat, long) {

        if($scope.specialityDisplay.subSpecialityId.length > 0){
            if (newpostcode) {
                var userParam = {
                    ParentHospitalID: parentHospitalId,
                    postcode: $scope.newpostcodeSearchText,
                    latitude: lat,
                    longitude: long,
                    // SpecialityCD: $scope.patientDetails.EnquiryDetails.specialitydetails.specialityid,
                    SpecialityCD: $scope.specialityDisplay.subSpecialityId,
                    usrid: doctorinfo.doctorid,
                    usrtype: "ADMIN",
                    meterval: parseInt($scope.searchRadius)
                }
                c7EnquiriesServices.getNearHospitalList(userParam, hospitalListSuccess, enquiryError);
    
            } else {
                $scope.newpostcodeColl = [];
                $scope.newpostcodeSearchText = '';
                // $('#loaderDiv').show();
                $scope.hospitalList = [];
                if ($scope.patientDetails.PatientAddress.length > 0) {
                    if ($scope.patientDetails.PatientAddress[0].Latitude && $scope.patientDetails.PatientAddress[0].Longitude &&
                        $scope.patientDetails.PatientAddress[0].Postcode) {
                        var userParam = {
                            ParentHospitalID: parentHospitalId,
                            postcode: $scope.patientDetails.PatientAddress[0].Postcode,
                            latitude: $scope.patientDetails.PatientAddress[0].Latitude,
                            longitude: $scope.patientDetails.PatientAddress[0].Longitude,
                            // SpecialityCD: $scope.patientDetails.EnquiryDetails.specialitydetails.specialityid,
                            SpecialityCD: $scope.specialityDisplay.subSpecialityId,
                            usrid: doctorinfo.doctorid,
                            usrtype: "ADMIN",
                            meterval: parseInt($scope.searchRadius)
                        }
                        c7EnquiriesServices.getNearHospitalList(userParam, hospitalListSuccess, enquiryError);
                    } else {
                        alertify.error('Please update patient address and try again');
                    }
                } else {
                    alertify.error('No Patient address Found');
                }
            }

        } else{
            alertify.error('Speciality is not active in Speciality Master');
        }
        
    }

    function hospitalListSuccess(data) {
        if (!data.error && data.length > 0) {
            if ($scope.aptBookedDetails != undefined) {
                $scope.isHospital = data.filter(v => v.HospitalID == $scope.aptBookedDetails.HospitalID);
            }
            $scope.hospitalList = data;
            $scope.selectAllHospital = false;
            $scope.selectNoneHospital = false;
            // angular.forEach($scope.hospitalList, function(key,value){
            //     key.isSelected = false;
            //     var dislen = parseFloat(key.Distance.split(' ')[0]);
            //     var disType = key.Distance.split(' ')[1];
            //     var km = undefined;
            //     if (disType == 'mi') {
            //         km = dislen * 1.60934;
            //     } else if (disType == 'ft') {
            //         km = dislen * 0.0003048;
            //     } else if (disType == 'm') {
            //         km = dislen * 0.001;
            //     }
            //     key.sortvalue = Math.ceil(km);
            //     key.distance = km.toFixed(1);
            // });
            // $scope.hospitalList = $scope.hospitalList.sort(function(a, b){
            //     return a.sortvalue - b.sortvalue;
            // });
            $scope.selectAllHospital = true;
            $scope.selectAll(true);
            $('#loaderDiv').hide();
        } else {
            if (data.length == 0) {
                $scope.selectAllHospital = false;
                $scope.hospitalList = [];
                alertify.error('No Location found');
            } else {
                alertify.error(data.error.message);
            }
        }
    }

    $scope.getBookedDetails = function (id) {
        // $('#loaderDiv').show();
        c7EnquiriesServices.getAppointmentDtl($scope.activeEnquiryId, bookedStatusSuccess, enquiryError);

    }

    function bookedStatusSuccess(data) {
        $scope.aptBookedDetails = data[data.length - 1];
        if ($scope.aptBookedDetails.EmergencyStartTime == '00:00' && $scope.aptBookedDetails.EmergencyEndTime == '00:00') {
            var date = moment($scope.aptBookedDetails.ScheduleDate + ' ' + $scope.aptBookedDetails.StartTime, 'YYYY-MM-DD HH:mm').valueOf();
            $scope.aptBookedDetails.ispast = moment().isSameOrAfter(date);
        } else {
            var date = moment($scope.aptBookedDetails.ScheduleDate + ' ' + $scope.aptBookedDetails.EmergencyStartTime, 'YYYY-MM-DD HH:mm').valueOf();
            $scope.aptBookedDetails.ispast = moment().isSameOrAfter(date);
        }

        $scope.availableSlotParam = {
            HospitalID: [$scope.aptBookedDetails.HospitalID],
            fromDate: $scope.aptBookedDetails.ScheduleDate,
            toDate: moment($scope.aptBookedDetails.ScheduleDate, 'YYYY-MM-DD').add(2, 'days').format('YYYY-MM-DD'),
            traigeDate: $scope.convertDateymd($scope.patientDetails.EnquiryDetails.referalprocessed),
            appointmenttype: $scope.patientDetails.EnquiryDetails.appointmenttypeid
        }
        // $scope.getNearHospitalList();

        // $scope.aptBookedDetails.clinicnotes = '';
    }
    $scope.selectAll = function (data) {
        if ($scope.selectAllHospital && data == true) {
            $scope.selectNoneHospital = false;
            angular.forEach($scope.hospitalList, function (key, value) {
                key.isSelected = true;
            });
        } else if ($scope.selectNoneHospital && data == false) {
            $scope.selectAllHospital = false;
            angular.forEach($scope.hospitalList, function (key, value) {
                key.isSelected = false;
            })
        }
    }
    $scope.selectHospital = function () {
        var selectCount = 0;
        angular.forEach($scope.hospitalList, function (key, value) {
            if (key.isSelected == true) {
                selectCount++;
            }
        });
        if (selectCount == $scope.hospitalList.length) {
            $scope.selectAllHospital = true;
        } else {
            $scope.selectAllHospital = false;
        }
        if (selectCount > 0 && $scope.selectNoneHospital == true) {
            $scope.selectNoneHospital = false;
        }
    }

    $scope.hideSublist = function () {
        $scope.showPatientEnquiry = undefined;
    }
    // Get available Slosts
    $scope.getAvailableSlots = function (id) {
        // $('#loaderDiv').show();
        $scope.modalId = id;
        $scope.hosId = [];
        var currentDate = '';
        var edate = '';

        angular.forEach($scope.hospitalList, function (key, value) {
            if (key.isSelected == true) {
                $scope.hosId.push(key.HospitalID);
            }
        });
        $scope.availableSlotParam = {
            HospitalID: $scope.hosId,
            fromDate: '',
            toDate: '',
            traigeDate: $scope.convertDateymd($scope.patientDetails.EnquiryDetails.referalprocessed),
            appointmenttype: $scope.patientDetails.EnquiryDetails.appointmenttypeid,
            SpecialityCD: $scope.specialityDisplay.subSpecialityId
        }

        // var sdate = moment($scope.slaDates.scanDate, 'DD-MM-YYYY').format('YYYY-MM-DD');
        // var sdate = $scope.availableSlotParam.traigeDate;
        var tdate = moment($scope.slaDates.scanDate, 'DD-MM-YYYY').format('YYYY-MM-DD');
        $scope.availableSlotParam.fromDate = moment().format('YYYY-MM-DD');
        $scope.availableSlotParam.toDate = (tdate == 'Invalid date') ? moment().add(3, 'day').format('YYYY-MM-DD') : tdate;

        if (moment().startOf('day').isSameOrBefore($scope.availableSlotParam.toDate)) {
            c7EnquiriesServices.listAvailableAppointment($scope.availableSlotParam, availableSlotsSucess, enquiryError);
        } else {
            $scope.availableSlotParam.fromDate = moment().format('YYYY-MM-DD');
            $scope.availableSlotParam.toDate = moment().add(3, 'day').format('YYYY-MM-DD');
            c7EnquiriesServices.listAvailableAppointment($scope.availableSlotParam, availableSlotsSucess, enquiryError);
            // alertify.error('appointment cannot be booked for past date');
        }
    }

    function availableSlotsSucess(data) {

        if (data.length == 0) {
            alertify.error('No slots found');
            $('#bookslot').modal('hide');
            $('#manage').modal('show');
            $scope.selectSlots = [];
            $scope.slotCollection = [];
        } else {
            $scope.selectSlots = [];
            $scope.slotCollection = angular.copy(data);
            angular.forEach($scope.slotCollection, function (val, gpind) {
                // var data = val.availableslotslist.filter(data => data.AddressConsultID === val.availableslotslist[0].AddressConsultID);
                // if (data.length > 1) {
                //     data.length = 1;
                // }
                // console.log(data);
                // val.availableslotslist = data;
                // console.log(val.availableslotslist);
                angular.forEach(val.availableslotslist, function (i, pind) {
                    i.Bookbefore = moment(i.Bookbefore, 'YYYY-MM-DD').format('DD-MM-YYYY');
                    var slotColl = [];
                    var schDate = moment(val.scheduledate).format('DD-MM-YYYY');
                    angular.forEach(i.Workingtimeslot, function (j) {
                        var ts = moment(schDate + ' ' + j, 'DD-MM-YYYY HH:mm').valueOf();
                        slotColl.push({
                            time: j,
                            isactive: false,
                            type: 'reg',
                            booked: false,
                            visible: true,
                            strike: !moment().isBefore(ts),
                            timestamp: ts
                        });
                    });
                    angular.forEach(i.Emergencytimeslots, function (j) {
                        slotColl.filter(data => data.time == j).forEach(data => data.type = 'emer');
                    });
                    angular.forEach(i.BreakTime1, function (j) {
                        slotColl = slotColl.filter(data => data.time != j);
                    });
                    angular.forEach(i.BreakTime2, function (j) {
                        slotColl = slotColl.filter(data => data.time != j);
                    });
                    angular.forEach(i.BreakTime3, function (j) {
                        slotColl = slotColl.filter(data => data.time != j);
                    });
                    angular.forEach(i.bookingslot, function (j) {
                        slotColl.filter(data => data.time == j).forEach(data => {
                            data.booked = true;
                            data.visible = false;
                        });
                    });
                    var reschdate = moment(val.scheduledate).format('YYYY-MM-DD');
                    if ($scope.reSchedule.status && $scope.reSchedule.scheduleDate == reschdate) {
                        var startDate = undefined;
                        var endDate = undefined;
                        if ($scope.reSchedule.startTime == '00:00' && $scope.reSchedule.endTime == '00:00') {
                            startDate = $scope.reSchedule.eStartTime;
                            endDate = $scope.reSchedule.eEndTime;
                        } else {
                            startDate = $scope.reSchedule.startTime;
                            endDate = $scope.reSchedule.endTime;
                        }
                        $scope.activeSlotsCollection = i;
                        // $scope.selectSlots = slotColl.filter(data => (data.time >= startDate && data.time < endDate));

                        // $scope.selectSlots.forEach(function(data) {
                        //     data.visible = true;
                        //     data.isactive = true;
                        // });                    
                    }
                    slotColl.filter(data => data.visible == true).forEach((data, cind) => {
                        data.g = gpind;
                        data.p = pind;
                        data.c = cind;
                    });

                    i.slots = slotColl.filter(data => data.strike != true && data.visible == true);
                });
            });

            $($scope.modalId).modal('show');
            $scope.modalId = '';
            setTimeout(function () {
                var swiper = new Swiper('.swiper-container', {
                    slidesPerView: 2,
                    spaceBetween: 10,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    }
                });

                $('#daterange').data('daterangepicker').setStartDate(moment($scope.availableSlotParam.fromDate, 'YYYY-MM-DD').format('DD-MM-YYYY'));
                $('#daterange').data('daterangepicker').setEndDate(moment($scope.availableSlotParam.toDate, 'YYYY-MM-DD').format('DD-MM-YYYY'));

                if (!$scope.reSchedule.status) {
                    $scope.selectSlots = [];
                }
                $('[data-toggle="tooltip"]').tooltip();
            }, 500);
        }
        $('#loaderDiv').hide();
        // console.log($scope.slotCollection);
    }
    $scope.selectSlots = [];
    $scope.activeSlotsCollection = undefined;
    $scope.nextstatus = false;

    function getFormatedTime(date, time) {
        return moment(date + ' ' + time, 'YYYY-MM-DD HH:mm');
    }

    function getTimeDiffInMinutes(date, time1, time2) {
        return Math.abs(moment.duration(getFormatedTime(date, time1).diff(getFormatedTime(date, time2))).asMinutes());
    }
    $scope.selectSlot = function (collection, slot, gpi, pi, ci) {
        var first = ($scope.selectSlots.length > 0) ? 0 : undefined;
        var last = ($scope.selectSlots.length > 0) ? $scope.selectSlots.length - 1 : undefined;
        if ($scope.selectSlots.length > 0 &&
            getTimeDiffInMinutes(collection.ScheduleDate, $scope.selectSlots[first].time, slot.time) != collection.MinPerCase &&
            getTimeDiffInMinutes(collection.ScheduleDate, $scope.selectSlots[last].time, slot.time) != collection.MinPerCase &&
            getTimeDiffInMinutes(collection.ScheduleDate, $scope.selectSlots[first].time, slot.time) != 0 &&
            getTimeDiffInMinutes(collection.ScheduleDate, $scope.selectSlots[last].time, slot.time) != 0) {
            alertify.error('Please choose the proper timing');
            return false;
        }
        if ($scope.activeSlotsCollection == undefined || $scope.activeSlotsCollection.AddressConsultID != collection.AddressConsultID) {
            $scope.activeSlotsCollection = collection;

            if ($scope.selectSlots.length > 0) {
                angular.forEach($scope.selectSlots, function (val) {
                    $scope.slotCollection[val.g].availableslotslist[val.p].slots[val.c].isactive = false;
                });
            }
            $scope.selectSlots = [];
        }
        if ($scope.selectSlots.length > 0) {
            // $scope.selectedSlots.sort(function (a, b) {
            //   return a.c - b.c;
            // });
            // var st, et;
            // if () {}
            var emergency = $scope.selectSlots.findIndex(x => x.type == slot.type);
            if (emergency == -1) {
                alertify.error('Do not choose regular and emergency slot at a time.');
                return false;
            }
            if (($scope.selectSlots.findIndex(x => x.time == slot.time) == -1) &&
                $scope.selectSlots[$scope.selectSlots.length - 1].c != (ci - 1) &&
                $scope.selectSlots[$scope.selectSlots.length - 1].c != (ci + 1) &&
                $scope.selectSlots[0].c != (ci - 1) &&
                $scope.selectSlots[0].c != (ci + 1)) {
                alertify.error('Please Choose continues slot.');
                return false;
            }

            var val = $scope.selectSlots.length - 1;

            if ($scope.selectSlots.findIndex(x => x.g == gpi && x.p == pi && x.c == ci) != -1) {
                if ($scope.slotCollection[gpi].availableslotslist[pi].slots[ci - 1] != undefined &&
                    $scope.slotCollection[gpi].availableslotslist[pi].slots[ci - 1].isactive == true &&
                    $scope.slotCollection[gpi].availableslotslist[pi].slots[ci + 1] != undefined &&
                    $scope.slotCollection[gpi].availableslotslist[pi].slots[ci + 1].isactive == true) {
                    alertify.error('Please unselect slots continuously.');
                    return false;
                }
                $scope.slotCollection[gpi].availableslotslist[pi].slots[ci].isactive = false;
            } else if ($scope.slotCollection[gpi].availableslotslist[pi].slots[ci].isactive == true) {
                if ($scope.slotCollection[gpi].availableslotslist[pi].slots[ci - 1] != undefined &&
                    $scope.slotCollection[gpi].availableslotslist[pi].slots[ci - 1].isactive == true &&
                    $scope.slotCollection[gpi].availableslotslist[pi].slots[ci + 1] != undefined &&
                    $scope.slotCollection[gpi].availableslotslist[pi].slots[ci + 1].isactive == true) {
                    alertify.error('Please unselect slots continuously.');
                    return false;
                }
                $scope.slotCollection[gpi].availableslotslist[pi].slots[ci].isactive = false;
            } else {
                $scope.slotCollection[gpi].availableslotslist[pi].slots[ci].isactive = true;
            }

        } else {
            $scope.slotCollection[gpi].availableslotslist[pi].slots[ci].isactive = true;
        }

        var index = $scope.selectSlots.findIndex(x => x.time == slot.time);

        if (index == -1) {
            slot.g = gpi;
            slot.p = pi;
            slot.c = ci;
            $scope.selectSlots.push(slot);
        } else {
            $scope.selectSlots.splice(index, 1);
        }
        $scope.nextstatus = ($scope.selectSlots.length > 0) ? true : false;
    }

    // Sort Method for enquiries
    $scope.sort = {
        active: '',
        ASC: undefined
      }     
      
      $scope.changeSorting = function(column) {
      
        var sort = $scope.sort;

        $scope.enquiriesListParam = {
            HospitalCode: hospitalCode,
            pagenumber: 1,
            pagesize: 10,
            enquirystatus: $scope.filterStatus,
            appointmenttype: $scope.aptfilterStatus,
            searchtext: '',
            regionid: ($scope.selectedRegion == undefined) ? 0 : parseInt($scope.selectedRegion),
            ccgid: ($scope.selectedCCG == undefined) ? 0 : parseInt($scope.selectedCCG),
            sortcolumn: "referalreceived",            
            sortorder: "DESC"
        }
      
        if (sort.active == column) {
           sort.ASC = !sort.ASC;           
           $scope.enquiriesListParam.sortorder = sort.ASC == true ? "ASC" : "DESC";
        } 
        else {
          sort.active = column;
          sort.ASC = true;
          $scope.enquiriesListParam.sortorder = sort.ASC == true ? "ASC" : "DESC";
        }        
        
        $scope.hideloadMore = false;

        c7EnquiriesServices.listAllEnquiry($scope.enquiriesListParam, listAllEnquirySuccess, enquiryError);

      };
     
      $scope.getIcon = function(column) {
      
        var sort = $scope.sort;
       
        if (sort.active == column) {
          return sort.ASC
            ? 'glyphicon-arrow-up'
            : 'glyphicon-arrow-down';
          }
        //   else {
        //       return  'glyphicon-arrow-up';
        //   }
      }

    //   Refresh Icon
    $scope.listAllCcgroupsInfo = function(){
    location.reload();
    } 

    // $scope.IsVisible = false;

    // $scope.changeSorting = function(){
    //     $scope.IsVisible = true;
    // }

    $scope.confirmSlots = function (hide, id) {
        if ($scope.selectSlots.length == 0) {
            alertify.error('Please select atleast one slot');
            return false;
        }
        $(hide).modal('hide');
        $scope.modalId = id;
        $scope.selectedSlots = $scope.selectSlots;
        // $scope.selectedClinic = $scope.hospitalList.filter(data => data.HospitalName == $scope.activeSlotsCollection.HospitalName)[0];
        $scope.selectedClinic = $scope.activeSlotsCollection;
        $scope.selectedSlots.sort(function (a, b) {
            return a.c - b.c;
        });
        var starttime = moment($scope.selectedSlots[0].time, 'HH:mm').format('HH:mm');
        var endtime = moment($scope.selectedSlots[$scope.selectedSlots.length - 1].time, 'HH:mm').add($scope.activeSlotsCollection.MinPerCase, 'minutes').format('HH:mm');
        // $scope.activeSlotsCollection.slotDuration = moment($scope.selectedSlots[0].time, 'HH:mm').format('hh:mm A') + ' - ' + moment($scope.selectedSlots[$scope.selectedSlots.length - 1].time, 'HH:mm').add($scope.activeSlotsCollection.MinPerCase, 'minutes').format('hh:mm A');
        $scope.activeSlotsCollection.slotDuration = $scope.selectedSlots[0].time + ' - ' + moment($scope.selectedSlots[$scope.selectedSlots.length - 1].time, 'HH:mm').add($scope.activeSlotsCollection.MinPerCase, 'minutes').format('HH:mm');

        $scope.bookdetails = {
            AddressID: $scope.activeSlotsCollection.AddressID,
            AddressConsultID: $scope.activeSlotsCollection.AddressConsultID,
            DayCD: moment($scope.activeSlotsCollection.ScheduleDate, 'YYYY-MM-DD').format('ddd').toUpperCase(),
            ScheduleDate: $scope.activeSlotsCollection.ScheduleDate,
            StartTime: '',
            EndTime: '',
            EmergencyStartTime: '',
            EmergencyEndTime: '',
            Startscantime: '00:00',
            Completescantime: '00:00',
            DoctorID: $scope.activeSlotsCollection.SonographerID,
            PatientID: $scope.patientDetails.Patient[0].PatientID,
            Patientenquiryid: $scope.activeEnquiryId,
            AppointmentStatus: "B",
            HospitalDetails: {
                HospitalID: $scope.selectedClinic.HospitalID,
                HospitalCD: $scope.activeSlotsCollection.HospitalCD,
                BranchCD: $scope.activeSlotsCollection.BranchCD,
                ParentHospitalID: parentHospitalId
            },
            HospitalCD: $scope.activeSlotsCollection.HospitalCD,
            BranchCD: $scope.activeSlotsCollection.BranchCD,
            USRId: doctorinfo.doctorid,
            USRType: "ADMIN"
        }
        if ($scope.reSchedule.status) {
            $scope.bookdetails.AppointmentID = $scope.aptBookedDetails.appid;
        }
        if ($scope.selectedSlots[0].type == 'reg') {
            $scope.bookdetails.StartTime = starttime;
            $scope.bookdetails.EndTime = endtime;
            $scope.bookdetails.EmergencyStartTime = '00:00';
            $scope.bookdetails.EmergencyEndTime = '00:00';
        } else {
            $scope.bookdetails.StartTime = '00:00';
            $scope.bookdetails.EndTime = '00:00';
            $scope.bookdetails.EmergencyStartTime = starttime;
            $scope.bookdetails.EmergencyEndTime = endtime;
        }
        // console.log($scope.bookdetails);
        setTimeout(function () {
            $($scope.modalId).modal('show');
            $scope.modalId = '';
        }, 500);

        $scope.reSchedule = {
            status: false,
            startTime: '',
            endTime: '',
            eStartTime: '',
            eEndTime: '',
            scheduleDate: '',
            hospitalId: undefined
        }
    }
    $scope.submitBooking = function () {
        c7EnquiriesServices.addupdateHospitalAppointments($scope.bookdetails, bookSuccess, enquiryError);
    }

    function bookSuccess(data) {
        console.log(data);
        if(data == "Time Slot Already mapped"){
            alertify.error('Time Slot Already mapped')
        }
        if (data == 'success') {
            $scope.enquiriesList();
        } else if (data != 'success' && !data.error) {
            $('#emailconfirmationBooking').modal('show');
            $scope.enquiriesList();
            $scope.emailData = {
                appointmentid: data.AppointmentID,
                sendemail: true,
                sendletter: true,
                sendsms: false
            }
            $scope.emailUpdateParam = {
                patientid: $scope.activePatientId,
                patientenquiryid: $scope.activeEnquiryId,
                usrid: doctorinfo.doctorid,
                statustypeid: 'appointEmail'
            };
        }
    }

    // Email
    $scope.bookingEmail = function () {
        $scope.download = ($scope.emailData.sendletter) ? true : false;
        $scope.sendemail = ($scope.emailData.sendemail) ? true : false;
        $scope.sendsms = ($scope.emailData.sendsms) ? true : false;

        
        $scope.emailName = 'Appointment Email';
        $scope.appointmentEmailTrue = true;

        var submitData = $scope.emailUpdateParam;
        submitData.appointmentdate = $scope.bookdetails.ScheduleDate;
        submitData.appointmenttime = $scope.bookdetails.StartTime != '00:00' ? $scope.bookdetails.StartTime : $scope.bookdetails.EmergencyStartTime ;
        submitData.doctorid = $scope.bookdetails.DoctorID;
        submitData.appointmenthospitalid = $scope.bookdetails.HospitalDetails.HospitalID;

        if($scope.emailData.appointmentid == 'Time Slot Already mapped'){
            alertify.error('Time Slot Already mapped');
            return;
        }

        // if($scope.sendsms){
        //     var paramSMS = {
        //         "AppointmentID":$scope.emailData.appointmentid,
        //         "USRId":doctorinfo.doctorid,
        //         "USRType":"ADMIN"
        //     }
        //     c7EnquiriesServices.sendAppointmentSMS(paramSMS, SMSSuccess, enquiryError);

        // }

        var paramSMS = {
            "AppointmentID":$scope.emailData.appointmentid,
            "USRId":doctorinfo.doctorid,
            "USRType":"ADMIN"
        }
        c7EnquiriesServices.sendAppointmentSMS(paramSMS, SMSSuccess, enquiryError);
        
        $scope.SMSUpdateParam = submitData;

        c7EnquiriesServices.sendAppointmentEmail($scope.emailData, emailSuccess, enquiryError);
        c7EnquiriesServices.enquiryaddstatus(submitData, updateEnquiryStatus, enquiryError);

    }
    function SMSSuccess(data) {
        if (data == "Success") {
            $scope.sendSMS = false;
            alertify.success('SMS Sent Successfully.');
            var submitData = $scope.SMSUpdateParam;
            submitData.statustypeid = "appointSMS";
            c7EnquiriesServices.enquiryaddstatus(submitData, updateEnquiryStatus, enquiryError);

        } else {
            alertify.error('Error in SMS sending.');

        }
    }


    $scope.patientDeclinedemail = function () {
        $scope.download = ($scope.emailDataDeclined.sendletter) ? true : false;
        $scope.sendemail = ($scope.emailDataDeclined.sendemail) ? true : false;

        $scope.emailName = 'Patient Declined Email';
        c7EnquiriesServices.sendPatientDeclinedEmail($scope.emailDataDeclined, emailSuccess, enquiryError);
        c7EnquiriesServices.enquiryaddstatus($scope.emailUpdateParam, updateEnquiryStatus, enquiryError);
    }
    $scope.patientDeclinedemailReject = function () {
        $scope.download = ($scope.emailRejectDeclined.sendletter) ? true : false;
        $scope.sendemail = ($scope.emailRejectDeclined.sendemail) ? true : false;

        $scope.emailName = 'Patient Withdrawal Email';
        c7EnquiriesServices.sendPatientDeclinedEmail($scope.emailRejectDeclined, emailSuccess, enquiryError);
        c7EnquiriesServices.enquiryaddstatus($scope.emailUpdateParam, updateEnquiryStatus, enquiryError);
    }
    $scope.exclusionemail = function () {
        if ($scope.emailexclusion.sendletter) {
            $scope.download = true;
        } else {
            $scope.download = false;
        }
        c7EnquiriesServices.enquiryaddstatus($scope.emailUpdateParam, updateEnquiryStatus, enquiryError);
        c7EnquiriesServices.sendExclusionEmail($scope.emailexclusion, emailSuccess, enquiryError);
    }
    $scope.patientDnaemail = function () {
        $scope.download = ($scope.sendPatientDNAMail.sendletter) ? true : false;
        $scope.sendemail = ($scope.sendPatientDNAMail.sendemail) ? true : false;

        $scope.emailName = 'Patient DNA Email';
        c7EnquiriesServices.sendPatientDNAMail($scope.sendPatientDNAMail, emailSuccess, enquiryError);
        c7EnquiriesServices.enquiryaddstatus($scope.emailUpdateParam, updateEnquiryStatus, enquiryError);
    }
    $scope.emailcontactusfun = function () {
        $scope.download = ($scope.emailcontactus.sendletter) ? true : false;
        $scope.sendemail = ($scope.emailcontactus.sendemail) ? true : false;

        $scope.emailName = 'Contact Us Email';
        c7EnquiriesServices.sendContactUsEmail($scope.emailcontactus, emailSuccess, enquiryError);
        c7EnquiriesServices.enquiryaddstatus($scope.emailUpdateParam, updateEnquiryStatus, enquiryError);
    }
    $scope.emailnocontactfun = function () {
        $scope.download = ($scope.emailnocontact.sendletter) ? true : false;
        $scope.sendemail = ($scope.emailnocontact.sendemail) ? true : false;

        $scope.emailName = 'No Contact To GP Email';
        c7EnquiriesServices.sendNoContactToGP($scope.emailnocontact, emailSuccess, enquiryError);
        c7EnquiriesServices.enquiryaddstatus($scope.emailUpdateParam, updateEnquiryStatus, enquiryError);
    }
    $scope.emailurgentnocontactfun = function () {
        $scope.download = ($scope.emailurgentnocontact.sendletter) ? true : false;
        $scope.sendemail = ($scope.emailurgentnocontact.sendemail) ? true : false;

        $scope.emailName = 'Urgent No Contact To GP Email';
        $scope.emailurgentnocontact.emailtype = 'URGENT';
        c7EnquiriesServices.sendNoContactToGP($scope.emailurgentnocontact, emailSuccess, enquiryError);
        c7EnquiriesServices.enquiryaddstatus($scope.emailUpdateParam, updateEnquiryStatus, enquiryError);
    }

    function emailSuccess(data) {
        if (data.includes('<!doctype html') && $scope.download) {
            $scope.printmail(data);
            $scope.download = false;
        } else if (!data.error && $scope.sendemail) {
            $scope.sendemail = false;
            alertify.success('Email Sent Successfully.');
        } else if (data.error) {
            alertify.error('Error in email sending.');
        }
    }
    $scope.sendemail = false;
    $scope.download = false;
    $scope.emailActoin = function (data, downloadcheck, email) {
        $scope.download = downloadcheck;
        $scope.sendemail = email;
        if (data.enquirystatusid == 'appointEmail') {
            $scope.appointmentEmailTrue = true;
            $scope.emailName = 'Appointment Email';
            var submitData = {
                patientenquiryid : data.patientenquiryid,
                appointmentid : data.emailobj.appointmentid,
                sendemail :email,
                sendletter :downloadcheck,
                appointmentdate : $scope.convertDateymd(data.appointmentdate),
                appointmenttime : data.appointmenttime,
                doctorid : data.doctorid,
                appointmenthospitalid : data.appointmenthospitalid
            };
            c7EnquiriesServices.sendAppointmentEmail(submitData, emailactionSuccess, enquiryError);
        } else {
            $scope.appointmentEmailTrue = false;
        }
        if (data.enquirystatusid == 'declinedEmail') {
            $scope.emailName = 'Patient Declined Email';
            c7EnquiriesServices.sendPatientDeclinedEmail(data.emailobj, emailactionSuccess, enquiryError);
        }
        if (data.enquirystatusid == 'dnaEmail') {
            $scope.emailName = 'Patient DNA Email';
            c7EnquiriesServices.sendPatientDNAMail(data.emailobj, emailactionSuccess, enquiryError);
        }
        if (data.enquirystatusid == 'contactusEmail') {
            $scope.emailName = 'Contact Us Email';
            c7EnquiriesServices.sendContactUsEmail(data.emailobj, emailactionSuccess, enquiryError);
        }
        if (data.enquirystatusid == 'nocontactEmail') {
            $scope.emailName = 'No Contact To GP Email';
            c7EnquiriesServices.sendNoContactToGP(data.emailobj, emailactionSuccess, enquiryError);
        }
        if (data.enquirystatusid == 'urgentnocontactEmail') {
            data.emailobj.emailtype = 'URGENT';
            $scope.emailName = 'Urgent No Contact To GP Email';
            c7EnquiriesServices.sendNoContactToGP(data.emailobj, emailactionSuccess, enquiryError);
        }
        if (data.enquirystatusid == 'exclusionEmail') {
            $scope.emailName = 'Exclosion Email';
            c7EnquiriesServices.sendExclusionEmail(data.emailobj, emailactionSuccess, enquiryError);
        }
        if (data.enquirystatusid == 'patientDeclinedEmail') {
            $scope.emailName = 'Patient Declined Email';
            c7EnquiriesServices.sendPatientDeclinedEmail(data.emailobj, emailactionSuccess, enquiryError);
        }
    }

    function emailactionSuccess(data) {
        if (data.includes('<!doctype html') && $scope.download) {
            $scope.printmail(data);
            $scope.download = false;
        } else if (!data.error && $scope.sendemail) {
            alertify.success('Email Sent Successfully.');
        } else if (data.error) {
            alertify.error('Error in email sending.');
        }
    }

    $scope.dateChange = function () {
        // $('#loaderDiv').show();

        var dataParam = $scope.availableSlotParam;
        dataParam.fromDate = moment($scope.daterangeSlot.split(' - ')[0], 'DD-MM-YYYY').format('YYYY-MM-DD');
        dataParam.toDate = moment($scope.daterangeSlot.split(' - ')[1], 'DD-MM-YYYY').format('YYYY-MM-DD');

        $scope.availableSlotParam.fromDate = dataParam.fromDate;
        $scope.availableSlotParam.toDate = dataParam.toDate;

        c7EnquiriesServices.listAvailableAppointment(dataParam, availableSlotsSucess, enquiryError);
    }


    $scope.getRegionToEdit = function (data) {
        var regionParam = {
            "usrid": doctorinfo.doctorid,
            "usrtype": "ADMIN",
            "pageNumber": 0,
            "pageSize": 10,
            "hospitalcd": hospitalCode
        };
        regionParam.regionid = data;
        referralregion.getRegionDetails(regionParam, getRegionSuccess, enquiryError);
    }

    function getRegionSuccess(data) {
        var run = true;
        if (data.length > 0 && data[0].SlaRecord.length > 0) {
            angular.forEach(data[0].SlaRecord, function (val) {
                if (run) {
                    if (moment().startOf('day').isSameOrBefore(val.slavalidto)) {
                        $scope.slaRecords = val;
                        $scope.sla += 1;
                        run = false;
                    } else {
                        alertify.error('Sla Dates error');
                    }
                }
            })
        } else {
            alertify.error('No Sla found');
        }
        if ($scope.sla == 2) {
            $scope.calcSla();
            $scope.sla = 0;
        }
        // console.log($scope.slaRecords);
    }
    $scope.slaDates = {};
    $scope.calcSla = function () {
        $scope.slaDates.base = $scope.patientDetails.EnquiryDetails.referalreceived;
        if ($scope.patientDetails.EnquiryDetails.appointmenttypeid == "URGENT") {
            $scope.slaDates.apttype = 'urgent';
            $scope.slaDates.triage = moment($scope.slaDates.base).add($scope.slaRecords.urgentreftriage, 'days').format('DD-MM-YYYY');
            $scope.slaDates.dateBooked = moment($scope.slaDates.base).add($scope.slaRecords.urgentrefdatebook, 'days').format('DD-MM-YYYY');
            $scope.slaDates.scanDate = moment($scope.slaDates.base).add($scope.slaRecords.urgentrefscan, 'days').format('DD-MM-YYYY');
            $scope.slaDates.reportSentback = moment($scope.slaDates.scanDate, 'DD-MM-YYYY').add($scope.slaRecords.urgentscanrepsendback, 'days').format('DD-MM-YYYY');
        }
        if ($scope.patientDetails.EnquiryDetails.appointmenttypeid == "ROUTINE") {
            $scope.slaDates.apttype = 'routine';
            $scope.slaDates.triage = moment($scope.slaDates.base).add($scope.slaRecords.routinereftriage, 'days').format('DD-MM-YYYY');
            $scope.slaDates.dateBooked = '-';
            $scope.slaDates.scanDate = moment($scope.slaDates.base).add($scope.slaRecords.routinerefscan, 'days').format('DD-MM-YYYY');
            $scope.slaDates.reportSentback = moment($scope.slaDates.scanDate, 'DD-MM-YYYY').add($scope.slaRecords.routineappsendback, 'days').format('DD-MM-YYYY');
        }
        if ($scope.patientDetails.EnquiryDetails.appointmenttypeid == "DVT") {
            $scope.slaDates.apttype = 'dvt';
            $scope.slaDates.triage = moment($scope.slaDates.base).add($scope.slaRecords.dvtreftriage, 'days').format('DD-MM-YYYY');
            $scope.slaDates.dateBooked = moment($scope.slaDates.base).add($scope.slaRecords.dvtrefdatebook, 'days').format('DD-MM-YYYY');
            $scope.slaDates.scanDate = moment($scope.slaDates.base).add($scope.slaRecords.dvtrefscan, 'days').format('DD-MM-YYYY');
            $scope.slaDates.reportSentback = moment($scope.slaDates.scanDate, 'DD-MM-YYYY').add($scope.slaRecords.dvtscanrepsendback, 'days').format('DD-MM-YYYY');
        }
        $scope.slaDates.base = moment($scope.slaDates.base).format('DD-MM-YYYY');
        // console.log($scope.slaDates);
    }
    var schedule = {
        AddressID: 5,
        HospitalID: parentHospitalId,
        ScheduleDate: "2018-07-13"
    }
    // c7EnquiriesServices.listHospitalAppointments(schedule, listappointmentSucess, enquiryError);
    // function listappointmentSucess(data) {
    //     console.log(data);
    // }

    $scope.validateAttachStatus = {
        type: false,
        reason: false
    }
    $scope.validateAttach = function (nval, oval, type) {
        if (nval == 'Accept') {
            $scope.validateAttachStatus.type = true;
            $scope.validateAttachStatus.reason = true;
        } else {
            $scope.validateAttachStatus.type = false;
            $scope.validateAttachStatus.reason = false;
        }
        if (type == 'type' && nval != oval) {
            $scope.validateAttachStatus.reason = false;
            $scope.validateAttachStatus.type = true;
        } else if (type == 'reson' && nval != oval && nval != 0) {
            $scope.validateAttachStatus.type = true;
            $scope.validateAttachStatus.reason = true;
        }
    }
    $scope.validateManageStatus = {
        type: false,
        reason: false,
        stat: false
    }
    $scope.validateManage = function (nval, oval, type) {
        if (type == 'status' && nval != oval) {
            $scope.validateManageStatus.stat = true;
            if (nval != 'ISSUES' || nval != 'REJECTED') {
                $scope.manageenquirystatus = '';
                $scope.patientDetails.EnquiryDetails.manageid = '0';
            }
        } else {
            $scope.validateManageStatus.reason = false;
            $scope.validateManageStatus.stat = false;
        }
        if (type == 'type' && nval != oval) {
            $scope.validateManageStatus.type = true;
            $scope.validateManageStatus.reason = false;
        } else if (type == 'reason' && nval != oval && nval != 0) {
            $scope.validateManageStatus.type = true;
            $scope.validateManageStatus.reason = true;
        }
    }

    // Cancel Appointment
    $scope.cancelAppointment = function (data) {
        $scope.cancelDetails = data;
        $('#cancelconfirm').modal('show');
        $scope.cancelData = {
            sendsms: false
        }

    }
    $scope.cancelConfirmation = function () {

        var updateparam = {
            AppointmentID: $scope.cancelDetails.appid,
            AddressID: 0,
            AddressConsultID: $scope.cancelDetails.AddressConsultID,
            DayCD: $scope.cancelDetails.daycd,
            ScheduleDate: $scope.cancelDetails.ScheduleDate,
            StartTime: $scope.cancelDetails.StartTime,
            EndTime: $scope.cancelDetails.EndTime,
            Startscantime: '00:00',
            Completescantime: '00:00',
            EmergencyStartTime: $scope.cancelDetails.EmergencyStartTime,
            EmergencyEndTime: $scope.cancelDetails.EmergencyEndTime,
            DoctorID: $scope.cancelDetails.DoctorID,
            PatientID: $scope.cancelDetails.PatientID,
            Patientenquiryid: ($scope.cancelDetails.Patientenquiryid == null) ? 0 : $scope.cancelDetails.Patientenquiryid,
            AppointmentStatus: 'C',
            HospitalDetails: {
                HospitalID: $scope.cancelDetails.HospitalID,
                HospitalCD: '',
                BranchCD: '',
                ParentHospitalID: parentHospitalId
            },
            HospitalCD: '',
            BranchCD: '',
            Notes: '',
            USRId: doctorinfo.doctorid,
            USRType: 'ADMIN'
        }

        
        var submitData = {
            patientid:  $scope.cancelDetails.PatientID,
            patientenquiryid: ($scope.cancelDetails.Patientenquiryid == null) ? 0 : $scope.cancelDetails.Patientenquiryid,
            usrid: doctorinfo.doctorid,
            statustypeid: 'cancelSMS',
            appointmentdate: $scope.cancelDetails.ScheduleDate,
            appointmenttime: $scope.cancelDetails.StartTime != '00:00' ? $scope.cancelDetails.StartTime : $scope.cancelDetails.EmergencyStartTime,
            doctorid: $scope.cancelDetails.DoctorID,
            appointmenthospitalid: $scope.cancelDetails.HospitalID
        };
                $scope.SMSUpdateParam1 = submitData;


        c7EnquiriesServices.addupdateHospitalAppointments(updateparam, canelAppointmentSuccess, enquiryError);


    }

    function canelAppointmentSuccess(data) {
        if (data == "success" || typeof data.AppointmentID == 'number') {
            alertify.success('Appointment Successfully Cancelled');
            var paramSMS = {
                "AppointmentID":data.AppointmentID,
                "USRId":doctorinfo.doctorid,
                "USRType":"ADMIN"
            }
            $scope.sendsms = ($scope.cancelData.sendsms) ? true : false;

            if($scope.sendsms){
                c7EnquiriesServices.sendAppointmentSMS(paramSMS, SMSSuccess1, enquiryError);
            }

            $scope.enquiriesList();
            // $scope.listing(0, 0);
        }
        console.log(data);
        if(data =="Time Slot Already mapped"){

            alertify.error('Time Slot Already mapped')
        }
    }
    function SMSSuccess1(data) {
        if (data == "Success") {
            $scope.sendSMS = false;
            alertify.success('SMS Sent Successfully.');
            var submitData = $scope.SMSUpdateParam1;
            c7EnquiriesServices.enquiryaddstatus(submitData, updateEnquiryStatus, enquiryError);

        } else {
            alertify.error('Error in SMS sending.');

        }
    }
    // ReSchedule Appointment
    $scope.rescheduleAppointment = function (data, id) {
        // $scope.modalId = id;
        console.log(data);
        // $scope.availableSlotParam = {
        //     HospitalID: [data.HospitalID],
        //     fromDate: data.ScheduleDate,
        //     toDate: moment(data.ScheduleDate, 'YYYY-MM-DD').add(2, 'days').format('YYYY-MM-DD'),
        //     traigeDate: $scope.convertDateymd($scope.patientDetails.EnquiryDetails.referalprocessed),
        //     appointmenttype: $scope.patientDetails.EnquiryDetails.appointmenttypeid
        // }
        $scope.reSchedule = {
            status: true,
            startTime: data.StartTime,
            endTime: data.EndTime,
            eStartTime: data.EmergencyStartTime,
            eEndTime: data.EmergencyEndTime,
            scheduleDate: data.ScheduleDate,
            hospitalId: data.HospitalID
        }
        // $scope.getNearHospitalList();
        // c7EnquiriesServices.listAvailableAppointment($scope.availableSlotParam, availableSlotsSucess, enquiryError);
    }
    $scope.printmail = function (data) {
        var html = data;
        // var html = "<!doctype html>\r\n<html>\r\n\r\n<head>\r\n    <meta name=\"viewport\" content=\"width=device-width\">\r\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n    <title>Simple Transactional Email</title>\r\n    <style>\r\n    @media only screen and (max-width: 620px) {\r\n        table[class=body] h1 {\r\n            font-size: 28px !important;\r\n            margin-bottom: 10px !important;\r\n        }\r\n\r\n        table[class=body] p,\r\n        table[class=body] ul,\r\n        table[class=body] ol,\r\n        table[class=body] td,\r\n        table[class=body] span,\r\n        table[class=body] a {\r\n            font-size: 16px !important;\r\n        }\r\n\r\n        table[class=body] .wrapper,\r\n        table[class=body] .article {\r\n            padding: 10px !important;\r\n        }\r\n\r\n        table[class=body] .content {\r\n            padding: 0 !important;\r\n        }\r\n\r\n        table[class=body] .container {\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n\r\n        table[class=body] .main {\r\n            border-left-width: 0 !important;\r\n            border-radius: 0 !important;\r\n            border-right-width: 0 !important;\r\n        }\r\n\r\n        table[class=body] .btn table {\r\n            width: 100% !important;\r\n        }\r\n\r\n        table[class=body] .btn a {\r\n            width: 100% !important;\r\n        }\r\n\r\n        table[class=body] .img-responsive {\r\n            height: auto !important;\r\n            max-width: 100% !important;\r\n            width: auto !important;\r\n        }\r\n    }\r\n\r\n    @media all {\r\n        .ExternalClass {\r\n            width: 100%;\r\n        }\r\n\r\n        .ExternalClass,\r\n        .ExternalClass p,\r\n        .ExternalClass span,\r\n        .ExternalClass font,\r\n        .ExternalClass td,\r\n        .ExternalClass div {\r\n            line-height: 100%;\r\n        }\r\n\r\n        .apple-link a {\r\n            color: inherit !important;\r\n            font-family: inherit !important;\r\n            font-size: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n            text-decoration: none !important;\r\n        }\r\n\r\n        .btn-primary table td:hover {\r\n            background-color: #34495e !important;\r\n        }\r\n\r\n        .btn-primary a:hover {\r\n            background-color: #34495e !important;\r\n            border-color: #34495e !important;\r\n        }\r\n    }\r\n    </style>\r\n</head>\r\n\r\n<body class=\"\" style=\"background-color: #f6f6f6; font-family: Calibri; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;\">\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body\" style=\"border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f6f6f6; width: 100%;\" width=\"100%\" bgcolor=\"#f6f6f6\">\r\n        <tr>\r\n            <td style=\"font-family: Calibri; font-size: 14px; vertical-align: top;\" valign=\"top\">&nbsp;</td>\r\n            <td class=\"container\" style=\"font-family: Calibri; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto;\" width=\"580\" valign=\"top\">\r\n                <div class=\"content\" style=\"box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;\">\r\n                    <!-- START CENTERED WHITE CONTAINER -->\r\n                    <!-- <span class=\"preheader\" style=\"color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;\">This is preheader text. Some clients will show this text as a preview.</span> -->\r\n                    <table class=\"main\" style=\"border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #ffffff; border-radius: 3px; width: 100%;\" width=\"100%\">\r\n                        <!-- START MAIN CONTENT AREA -->\r\n                        <tr>\r\n                            <td class=\"wrapper\" style=\"font-family: Calibri; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;\" valign=\"top\">\r\n                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;\" width=\"100%\">\r\n                                    <tr>\r\n                                        <td style=\"font-family: Calibri; font-size: 14px; vertical-align: top;\" valign=\"top\">\r\n                                            <div style=\"text-align: left; float: left;\"><img src=\"https://s3-ap-southeast-1.amazonaws.com/c7health-amp-templates/images/dw-logo.jpg\" alt=\"\"></div>\r\n                                            <div style=\"text-align: left; float: right;\">\r\n                                                <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 10px;\"> 2018-08-03 </p>\r\n                                                <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 10px;\"> Mr nIVIN </p>\r\n                                                <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 10px;\">  </p>\r\n                                                <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 10px;\"> Sherwood Health Centre Elmswood Gardens Nottingham UK  </p>\r\n                                            </div>\r\n                                            <div style=\"clear: both;\"></div>\r\n                                            <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;\">Dear Mr nIVIN,</p>\r\n                                            <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0px 0px 0px 0px; margin-bottom: 15px;\"> Following our telephone conversation regarding your ultrasound scan, please accept this letter as confirmation of your appointment details as shown below:</p>\r\n\r\n                                            <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 10px;\">  Sunday, 5th Aug, 2018  at  01:00 AM </p>\r\n\r\n                                            <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 10px;\"> G58 Location National Savings,  Boydstone Road,  Glasgow, UK\r\n                                            </p>\r\n                                             <p style=\"font-family: Calibri; font-size: 16px; font-weight: normal; margin: 0; margin-bottom: 15px;\"><strong> Important Patient Information </strong></p>\r\n\r\n                                            <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;\">You have been scheduled to receive a URGENT ultrasound scan. Some preparation is required prior to your examination.</p>\r\n\r\n                                            <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;\"> \r\n                                                \r\n                                            </p>\r\n\r\n                                            <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;\"> Following your scan, you will need to book an appointment with your GP to obtain your scan results. Please note that results will not be given on the day when you have your scan. </p>\r\n\r\n                                            <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;\"> Should you require any dedicated services such as an interpreter, disabled access or any additional assistance not listed above, please telephone 0800 001 5171 immediately. </p>\r\n\r\n                                            <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;\"> On the day of your scan, please arrive 10 minutes before your appointment time and register with the receptionist. Your ultrasound scan will last approximately 20 minutes, please see enclosed information sheet or call 0800 001 5171, if you have any questions. </p>\r\n\r\n                                            <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;\"> Please bring a copy of this letter with you on the day of the appointment. </p>\r\n\r\n                                            <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin-top: 60px; margin-bottom: 10px;\">Yours sincerely </p>\r\n                                            <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 10px;\"> Patient Management Team </p>\r\n                                            <p style=\"font-family: Calibri; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 10px;\">Diagnostic World</p>\r\n                                            <div style=\"margin-top:300px;text-align: right;\"><img src=\"https://s3-ap-southeast-1.amazonaws.com/c7health-amp-templates/images/footer.png\" alt=\"\"></div>\r\n</td>\r\n</tr>\r\n</table>\r\n</td>\r\n</tr>\r\n</table>\r\n</div>\r\n</td>\r\n </tr>\r\n</table>\r\n</body>\r\n\r\n</html>";

        var WindowObject = window.open("", "PrintWindow", "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
        WindowObject.document.writeln(html.replace('Simple Transactional Email', $scope.emailName));
        WindowObject.document.close();
        setTimeout(function () {
            WindowObject.focus();
            if (!$scope.appointmentEmailTrue) {
                WindowObject.print();
                WindowObject.close();
            }
            $scope.appointmentEmailTrue = false;
        }, 2000)
        $scope.emailName = '';
    }
    // setTimeout(function(){
    //     $scope.printmail()
    // }, 2000);

    $scope.searchTextNew = function () {
        $scope.enquiriesListParam.pagenumber = 1;

        if ($scope.searchPatient != '' || $scope.filterStatus != 'ALL' || $scope.aptfilterStatus != 'ALL') {
            var param = {
                HospitalCode: hospitalCode,
                searchtext: $scope.searchPatient,
                enquirystatus: $scope.filterStatus,
                appointmenttype: $scope.aptfilterStatus,
                pagenumber: 1,
                pagesize: 10,
                regionid: parseInt($scope.selectedRegion),
                ccgid: parseInt($scope.selectedCCG)
            }
            if ($scope.searchPatient.length > 3 || $scope.filterStatus != 'ALL' || $scope.aptfilterStatus != 'ALL') {

                c7EnquiriesServices.listAllEnquiry(param, SearchAllEnquirySuccess, enquiryError);

                function SearchAllEnquirySuccess(data) {
                    $scope.loadMore = false;
                    $scope.loadMore = (data.length < 10) ? false : true;
                    // data = data.filter(data => data.enquirystatusid != 'REJECT');
                    $scope.enquiriesCollection = data;
                }
            }
        } else {
            $scope.enquiriesList()
        }
    }
    // Custom Postcode
    $scope.newpostcodeColl = [];
    $scope.newpostcodeSearchText = '';
    $scope.newPostcodeTyped = function (data) {
        $scope.newpostcodeSearchText = data;
    }
    $scope.newPostcodeLookup = function () {
        $scope.addressCollection = [];
        if ($scope.newpostcodeSearchText == '') {
            alertify.error('Please enter postcode to search');
            return false;
        }
        c7PostCodeServices.listpostcodeinfo($scope.newpostcodeSearchText, postcodeSuccess, postcodeerror);

        function postcodeSuccess(data) {
            $scope.newpostcodeColl = data.addresses;
            $scope.selectedLatitude = data.latitude.toString();
            $scope.selectedLongitude = data.longitude.toString();
            setTimeout(function () {
                $('#postcodeselect').find('.ui-select-toggle').click();
            }, 100)
        }

        function postcodeerror(data) {
            alertify.error(data);
        }
    }

    $scope.newPostcodeAppointmentSearch = function () {
        // latlong($scope.newpostcodeColl.selected);
        $scope.getNearHospitalList(true, $scope.selectedLatitude, $scope.selectedLongitude);
        console.log($scope.newpostcodeColl.selected);
    }

    function latlong(val) {
        var geocoder = new google.maps.Geocoder();
        var address = val;

        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var lat = results[0].geometry.location.lat().toString();
                var long = results[0].geometry.location.lng().toString();
                $scope.getNearHospitalList(true, lat, long);
            }
        });
    }

    $scope.clearform = function (form) {
        let controlNames = Object.keys(form).filter(key => key.indexOf('$') !== 0);

        for (let name of controlNames) {
            let control = form[name];
            control.$setViewValue(undefined);
        }
        $scope.reminderDate = '';
        $scope.reminderNotes = '';
        form.$setPristine();
        form.$setUntouched();
    }



    // Speciality
    c7AdminServices.SpecialityList(parentHospitalId, specialitylistSuccess, patientspecialityerror)

    function specialitylistSuccess(data) {
        $scope.specialityCollection = data;
        // $scope.statusSuccess.speciality += 1;
        // if ($scope.statusSuccess.speciality == 2) {
        // loadspeciality();
        // }
    }
    function patientspecialityerror(data) {
        alertify.error("Error in speciality list");
    }
    $scope.subSpecialityCollection = [];
    $scope.subSpecialityCollection.selected = [];
    $scope.selectspeciality = function () {
        angular.forEach($scope.specialityCollection.selected, function (i) {
            angular.forEach(i.subspeciality, function (j) {
                j.spelid = i.specialityID;
                j.speldesc = i.specialitydesc;
                if ($scope.subSpecialityCollection.findIndex(d => d.DoctorSpecialityID == j.DoctorSpecialityID) == -1) {
                    $scope.subSpecialityCollection.push(j);
                    // $scope.subSpecialityCollection.selected.push(j);
                }
            })
        });
        for (var i = 0; i < $scope.subSpecialityCollection.length; i++) {
            var index = $scope.specialityCollection.selected.findIndex(d => d.specialityID == $scope.subSpecialityCollection[i].spelid);
            if (index == -1) {
                $scope.subSpecialityCollection.splice(i, 1);
                i--;
            }
        }
        if ($scope.subSpecialityCollection.selected != undefined) {
            for (var i = 0; i < $scope.subSpecialityCollection.selected.length; i++) {
                var selindex = $scope.specialityCollection.selected.findIndex(d => d.specialityID == $scope.subSpecialityCollection.selected[i].spelid);
                if (selindex == -1) {
                    $scope.subSpecialityCollection.selected.splice(i, 1);
                    i--;
                }
            }
        }
        // $scope.addUpdatePat.patientDetails.EnquiryDetails.specialitydetails.specialityid = $scope.specialityCollection.selected.specialityID;
        // $scope.addUpdatePat.patientDetails.EnquiryDetails.specialitydetails.speciality = $scope.specialityCollection.selected.specialitydesc;
        // if ($scope.specialityCollection.selected.subspeciality.length > 0) {
        //     $scope.subSpecialityCollection = $scope.specialityCollection.selected.subspeciality;
        // } else {
        //     $scope.subSpecialityCollection = [];
        // }
        validateVal();
    }


    function loadspeciality() {

        if ($scope.specialityCollection != undefined) {

            $scope.subSpecialityCollection = [];
            $scope.specialityCollection.selected = [];
            $scope.subSpecialityCollection.selected = [];
            var curspel = angular.copy($scope.patientDetails.EnquiryDetails.specialitydetails);
            angular.forEach(curspel, function (i) {
                var data = $scope.specialityCollection.filter(data => data.specialityID == i.specialityid);
                $scope.specialityCollection.selected = $scope.specialityCollection.selected.concat(data);
                angular.forEach($scope.specialityCollection.selected, function (x) {
                    angular.forEach(x.subspeciality, function (y) {
                        y.spelid = x.specialityID;
                        if ($scope.subSpecialityCollection.findIndex(d => d.DoctorSpecialityID == y.DoctorSpecialityID) == -1) {
                            $scope.subSpecialityCollection.push(y);
                        }
                    })
                })
                var arr = Object.values(i.subSpecialities);
                // angular.forEach(j.subspeciality, function(p) { p.isActive = false });
                angular.forEach(arr, function (p) {
                    var subdata = $scope.subSpecialityCollection.filter(data => data.DoctorSpecialityID == p);
                    angular.forEach(subdata, function (x) {
                        if ($scope.subSpecialityCollection.selected.findIndex(d => d.DoctorSpecialityID == x.DoctorSpecialityID) == -1) {
                            $scope.subSpecialityCollection.selected.push(x);
                        }
                    })
                });

            });
            // angular.forEach($scope.specialityColl, function(i){
            //     var data = $scope.subSpecialityCollection.filter(data => data.specialityID == i.specialityid);
            //     $scope.subSpecialityColl = $scope.subSpecialityColl.concat(data);
            // });
            // // console.log($scope.specialityCollection);
            // if ($scope.specialityCollection.selected != undefined && $scope.specialityCollection.selected.subspeciality.length > 0) {
            //     $scope.subSpecialityCollection = $scope.specialityCollection.selected.subspeciality;
            //     $scope.subSpecialityCollection.selected = $scope.subSpecialityCollection.filter(data => data.DoctorSpecialityDesc == $scope.addUpdatePat.patientDetails.EnquiryDetails.specialitydetails.subspeciality)[0];
            // }
        }

        // $scope.statusSuccess.speciality = 0;
    }

    function fillspeciality() {
        $scope.newSpecialityCollection = [];
        angular.forEach($scope.specialityCollection.selected, function (i) {
            var params = {
                speciality: i.specialitydesc,
                specialityid: i.specialityID,
                activeInd: 'Y',
                subSpecialities: {}
            }
            if (i.id != undefined) {
                params.id = i.id;
            }
            angular.forEach(i.subspeciality, function (k, key) {
                if ($scope.subSpecialityCollection.selected.findIndex(p => p.DoctorSpecialityID == k.DoctorSpecialityID) > -1) {
                    params.subSpecialities[Object.keys(params.subSpecialities).length] = k.DoctorSpecialityID;
                }
            })
            $scope.newSpecialityCollection.push(params);
        })
        // angular.forEach($scope.receivedSpeciality, function(i){
        //     $scope.newSpecialityCollection.filter(data => data.specialityid == i.specialityid).forEach(function(j){
        //         j.id = i.id;
        //     })
        // })
    }

    function newEnquiryAddStatus(exceptStatus) {
        // $scope.enquiryNewid = datacookieFactory.getItem('editPatientEnquiryId');
        var params = {
            patientid: $scope.activePatientId,
            patientenquiryid: $scope.activeEnquiryId,
            usrid: docid,
            statustypeid: (exceptStatus == 'ExceptionNotes')? "ExceptionNotes" : "EnquiryModified" ,
            notes: (exceptStatus == 'ExceptionNotes')? 
                $scope.patientDetails.EnquiryDetails.issueReason : $scope.patientDetails.EnquiryDetails.notes
        }

        if(exceptStatus == 'ExceptionNotes') {
            params.manageid = $scope.patientDetails.EnquiryDetails.manageid.toString();
        }

        c7EnquiriesServices.enquiryaddstatus(params, enquiryaddstatussuccess, addstatuserror);
    }

    $scope.agespeciality = {
        age: false,
        speciality: false
    }
    function enquiryaddstatussuccess(data) {

    }
    function addstatuserror(data) {

    }
    function validateVal() {
        if ($scope.validator.status == true) {
            if ($scope.patientDetails.Patient[0].Age != undefined && $scope.patientDetails.Patient[0].Age < $scope.validator.minAge) {
                $scope.agespeciality.age = true;
                alertify.error('Age below required range');
            } else {
                $scope.agespeciality.age = false;
            }
            var specialityInCCG = {};
            if ($scope.validator.speciality != undefined && $scope.specialityCollection.selected.length > 0) {
                // var i = true;
                angular.forEach($scope.specialityCollection.selected, function (i) {
                    specialityInCCG[i.specialitydesc] = ($scope.validator.speciality.filter(data => data.Speciality == i.specialityID).length == 0) ? true : false;
                });
                for (var key in specialityInCCG) {
                    if (specialityInCCG[key] == true) {
                        alertify.error(key + ' - Speciality is not in CCG');
                    }
                }
                var val = Object.values(specialityInCCG);
                for (var i = 0; i < val.length; i++) {
                    if (val[i] == true) {
                        $scope.agespeciality.speciality = true;
                        break;
                    } else {
                        $scope.agespeciality.speciality = false;
                    }
                }
                // console.log(specialityInCCG);
                // if (specialityInCCG.length == 0) {
                //     $scope.agespeciality.speciality = true;
                //     alertify.error('Speciality is not in CCG');
                // } else {
                //     $scope.agespeciality.speciality = false;
                // }
            }
        }
        // console.log($scope.validator);
    }
    function loadccg(id) {

        var referralParam = {
            usrid: doctorinfo.doctorid,
            usrtype: "ADMIN",
            pageNumber: 0,
            pageSize: 1000,
            hospitalcd: hospitalCode,
            ccgid: id
        };
        referralccg.getccgDetails(referralParam, ccgdetailssuccess, ccgerror);
    }
    // Validation
    $scope.validator = {
        minAge: undefined,
        speciality: [],
        status: false
    }
    function ccgdetailssuccess(data) {
        $scope.activeCCG = angular.copy(data[0]);
        if($scope.activeCCG.Contractdetails.length > 0){
            $scope.activeccgContract = $scope.activeCCG.Contractdetails[$scope.activeCCG.Contractdetails.length - 1];
            $scope.validator.minAge = $scope.activeccgContract.minimunageforref;
            $scope.validator.speciality = $scope.activeccgContract.Contractspeciality;
            $scope.validator.status = true;
            validateVal();
        }
        
    }
    function ccgerror(data) {
    }

    $scope.exportToExcel = function(){        

        if ($scope.searchPatient.length > 3 || $scope.filterStatus != 'ALL' || $scope.aptfilterStatus != 'ALL') {
            var param = {
                HospitalCode: hospitalCode,
                searchtext: $scope.searchPatient,
                enquirystatus: $scope.filterStatus,
                appointmenttype: $scope.aptfilterStatus,
                pagenumber: 1,
                pagesize: 10000,
                regionid: parseInt($scope.selectedRegion),
                ccgid: parseInt($scope.selectedCCG)
            }
            if ($scope.searchPatient.length > 3 || $scope.filterStatus != 'ALL' || $scope.aptfilterStatus != 'ALL') {

                c7EnquiriesServices.listAllEnquiry(param, ExportEnquirySuccess, enquiryError);                
            }
        } else {
            alertify.error('Filters not selected');
        }
    }

    function ExportEnquirySuccess(data) {                                        
        $scope.downloadData = data;
        $scope.exportData();
    }

    $scope.exportData = function() {
        var exportExcelData = [{
            AppointmentType: 'Appointment Type',
            EnquiryStatus: 'Enquiry Status',
            Received : 'Received Date',
            Patient: 'Patient Name',
            DWID: 'DW ID',
            Referrer: 'Referrer',
            Speciality: 'Speciality'
        }];

        for (var i = 0; i < $scope.downloadData.length; i++) {

            var arr = [];
            var sdetails = '';
            if ($scope.downloadData[i].specialitydetails.length > 0) {
                for (var x = 0; x < $scope.downloadData[i].specialitydetails.length; x++) {
                    arr.push($scope.downloadData[i].specialitydetails[x].speciality);
                }
            }
            sdetails = arr.join(', ');

            var enquiryStatusDesc = '';
            enquiryStatusDesc = findEnquiryStatus($scope.downloadData[i].enquirystatusid)

            var temp = {
                AppointmentType: $scope.downloadData[i].appointmenttypeid,
                EnquiryStatus: enquiryStatusDesc,
                Received: ($scope.downloadData[i].referalreceived != null && $scope.downloadData[i].referalreceived != '') ?
                moment($scope.downloadData[i].referalreceived).format('DD-MM-YYYY') : '',
                Patient: $scope.downloadData[i].patientname,
                DWID: $scope.downloadData[i].dwid,
                Referrer: $scope.downloadData[i].doctorname,
                Speciality: sdetails
            };
            exportExcelData.push(temp);
        }
        var xmls = fetchXML([exportExcelData]);

        var ctx = {
            created: new Date().getTime(),
            worksheets: xmls
        };

        var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML =
            '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
            '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
            '<Styles>' +
            '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
            '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
            '</Styles>' +
            '{worksheets}</Workbook>',
            tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
            tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
            base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                });
            };
        var workbookXML = format(tmplWorkbookXML, ctx);
        var link = document.createElement('A');
        link.href = uri + base64(workbookXML);
        link.download = 'EnquiryReport_' + new Date().getTime() + '.xls';
        
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    function fetchXML(stores) {
        var wrkbookXML = '';
        for (i = 0; i < stores.length; i++) {
            var worksheetName = 'Data';
            //   i == 0 ? (worksheetName = 'Canada') : (worksheetName = 'India');
            wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
            // var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
            var rowXML = '';
            for (j = 0; j < stores[i].length; j++) {
                rowXML += '<Row>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].AppointmentType + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].EnquiryStatus + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Received + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Patient + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].DWID + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Referrer + '</Data></Cell>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].Speciality + '</Data></Cell>';
                rowXML += '</Row>';
            }
            wrkbookXML += rowXML;
            wrkbookXML += '</Table></Worksheet>';
        }
        return wrkbookXML;
    }

    function findEnquiryStatus(item) {
        var enquiryobj = [{
            id: 'TRIAGE',
            desc: 'To Triage'
        },
        {
            id: 'AWAITING',
            desc: 'Awaiting Appointment'
        },
        {
            id: 'BOOKED',
            desc: 'Booked'
        },
        {
            id: 'SCANCOMPLETED',
            desc: 'Scan Completed'
        },
        {
            id: 'AWAIT2CALL',
            desc: 'Awaiting 2nd Call'
        },
        {
            id: 'AWAIT3CALL',
            desc: 'Awaiting 3rd Call'
        },
        {
            id: 'AWAIT4CALL',
            desc: 'Awaiting 4th Call'
        },
        {
            id: 'FINALCALL',
            desc: 'Final Call'
        },
        {
            id: 'INACTIVE',
            desc: 'Inactive'
        },
        {
            id: 'COMPLETE',
            desc: 'Completed / Published'
        },
        {
            id: 'REJECT',
            desc: 'Rejected'
        },
        {
            id: 'ISSUES',
            desc: 'Issues'
        },
        {
            id: 'DNA',
            desc: 'DNA'
        },
        {
            id: 'PUBLISH',
            desc: 'Published'
        },
        {
            id: 'ReferralForm',
            desc: 'Referral Form Opened'
        },
        {
            id: 'ManageForm',
            desc: 'Manage Form Opened'
        },
        {
            id: 'EnquirySeen',
            desc: 'Enquiry Seen'
        },
        {
            id: 'EnquiryModified',
            desc: 'Enquiry Modified'
        },
        {
            id: 'ExceptionNotes',
            desc: 'Exception Notes'
        },
        {
            id: 'appemail',
            desc: 'Email Triggered'
        },
        {
            id: 'appointEmail',
            desc: 'Appointment email sent'
        },
        {
            id: 'appointSMS',
            desc: 'Appointment SMS sent'
        },
        {
            id: 'cancelSMS',
            desc: 'Appointment Cancel SMS sent'
        },
        {
            id: 'dnaSMS',
            desc: 'DNA SMS sent'
        },
        {
            id: 'exclusion',
            desc: 'Exclusion email sent'
        },
        {
            id: 'exclusionEmail',
            desc: 'Exclusion email sent'
        },
        {
            id: 'declinedEmail',
            desc: 'Declined email sent'
        },
        {
            id: 'patientDeclinedEmail',
            desc: 'Declined email sent'
        },
        {
            id: 'dnaEmail',
            desc: 'DNA email sent'
        },
        {
            id: 'contactusEmail',
            desc: 'Contact Us email sent'
        },
        {
            id: 'nocontactEmail',
            desc: 'No Contact email sent'
        },
        {
            id: 'urgentnocontactEmail',
            desc: 'Urgent No Contact email sent'
        },
        {
            id: 'DOCTORREFERRAL',
            desc: 'Referral From Doctor'
        },
        {
            id: 'B',
            desc: 'Appointment Booked'
        },
        {
            id: 'C',
            desc: 'Cancelled'
        },
        {
            id: 'SC',
            desc: 'Scan Completed'
        },
        {
            id: 'FC',
            desc: 'Feedback Completed'
        },
        {
            id: 'VR',
            desc: 'Verification Required'
        },
        {
            id: 'RS',
            desc: 'Appointment Rescheduled'
        },
        {
            id: 'SECOND DNA',
            desc: 'Second DNA'
        },
        {
            id: 'LETTERSENT',
            desc: 'Final Call Letter Sent'
        }, {
            id: 'LATECANCELLATION',
            desc: 'Late Cancellation'
        }, {
            id: 'LC',
            desc: 'Late Cancellation'
        },
        {
            id: 'ReminderAdded',
            desc: 'Reminder Added'
        },
        {
            id: 'ReminderCompleted',
            desc: 'Reminder Completed'
        },
        {
            id: 'DocumentUpload',
            desc: 'Document Upload'
        }, {
            id: 'Reasonnotes',
            desc: 'Issue Reason Notes Updated'
        }, {
            id: 'Notes',
            desc: 'Notes Updated'
        }
        ];  
        
        if (item != undefined) {
            return enquiryobj.filter(data => data.id == item)[0].desc;
        } else{
            return '';
        }
    
    }

});