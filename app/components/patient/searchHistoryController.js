hCueDoctorWebApp.lazyController('c7PatientSearchHistory', ['$scope', '$location', 'datacookieFactory', 'hcueLoginStatusService', 'alertify', '$uibModal', '$window', '$rootScope', 'c7SearchServices', 'referraldoctor', 'c7EnquiriesServices', 'hcueDoctorLoginService', 'c7AddUpdatePatient', 'c7Hl7ToMeshServices', 'referralccg', 'c7AdminServices', 'c7PostCodeServices',
    function ($scope, $location, datacookieFactory, hcueLoginStatusService, alertify, $uibModal, $window, $rootScope, c7SearchServices, referraldoctor, c7EnquiriesServices, hcueDoctorLoginService, c7AddUpdatePatient, c7Hl7ToMeshServices, referralccg, c7AdminServices, c7PostCodeServices) {

        var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

        userLoginDetails = datacookieFactory.getItem('loginData');
        if (userLoginDetails.loggedinStatus === "true") {
            $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
        } else {
            $location.path('/login');
        }
        var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

        if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
            parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
            hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
            branchCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.BranchCode;
            newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
            docid = doctorinfo.doctorid;
        }
        $scope.clearSearchPatient = function () {
            $scope.searchPatient = '';
            $scope.showResult = false;
            $scope.searchCollection = [];
        }
        $scope.emailName = '';
        $scope.clearSearchPatient();
        $scope.searchCollection = [];
        $scope.searchshow = true;
        $scope.activeEnquiryId = undefined;
        $scope.search = function () {
            if ($scope.searchPatient == '') {
                return false;
            }

            c7SearchServices.searchPatientsget($scope.searchPatient, OnGetPatientSuccess, OnGetError);

            function OnGetPatientSuccess(data) {
                $scope.searchCollection = [];
                angular.forEach(data, function (i) {
                    var array_list = ['#af77c6', '#e4c120', '#69b3e7', '#ed9945', '#90c752'];
                    var val = array_list[Math.floor(Math.random() * array_list.length)];
                    i.colors = val;
                    $scope.searchCollection.push(i)
                })
                $scope.showResult = true;
            }
        }
        $scope.statusSuccess = {
            speciality: 0,
            ccg: 0
        };
        $scope.agespeciality = {
            age: false,
            speciality: false
        }
        $scope.lookupStatus = false;
        $scope.declareVariable = function () {
            $scope.issuereason = false;
            $scope.addUpdatePat = {
                USRType: 'ADMIN',
                USRId: doctorinfo.doctorid,
                patientDetails: {
                    FirstName: '',
                    DOB: '',
                    FullName: '',
                    LastName: '',
                    Title: '',
                    Gender: '',
                    Age: undefined,
                    TermsAccepted: 'Y',
                    MobileID: undefined,
                    EnquiryDetails: {
                        referalcreated: '',
                        referalreceived: '',
                        referalprocessed: moment().format('DD-MM-YYYY'),
                        clinicalnotes: '',
                        clinicaldetails: '',
                        checkboxdetails: {
                            isVoicemail: false,
                            isEmail: false,
                            isLetter: false,
                            isSms: false,
                            isMobilityissues: false,
                            isCommunicationissues: false
                        },
                        specialitydetails: [{
                            speciality: '',
                            specialityid: '',
                            activeInd: 'Y',
                            subSpecialities: {}
                        }],
                        enquiryfile: {
                            enquiry_real: '',
                            additional_real: ''
                        },
                        manageid: undefined,
                        issueReason: '',
                        regionid: undefined,
                        appointmenttypeid: '',
                        enquirysourceid: '',
                        enquirystatusid: 'TRIAGE',
                        notes: '',
                        crtusr: doctorinfo.doctorid,
                        crtusrtype: 'ADMIN',
                        updtusr: doctorinfo.doctorid,
                        updtusrtype: 'ADMIN'
                    },
                    PatientOtherDetails: {
                        AadhaarID: ''
                    }
                },
                patientEmail: [{
                    EmailID: '',
                    EmailIDType: 'P',
                    PrimaryIND: 'Y'
                }],
                patientAddress: [{
                    Postcode: '',
                    Address1: '',
                    Address2: '',
                    Address3: '',
                    Address4: '',
                    Street: '',
                    State: '',
                    PinCode: 0,
                    CityTown: '',
                    Country: 'UK',
                    PrimaryIND: 'Y',
                    AddressType: 'H',
                    Latitude: '',
                    LandMark: '',
                    DistrictRegion: '',
                    Longitude: '',
                    Location: ''
                }],
                patientPhone: [],
                patientReferral: {
                    ccgid: 2,
                    ccgname: '',
                    practiceid: 3,
                    practicename: '',
                    doctorname: '',
                    doctorid: 5
                },
                GenIDDetails: {
                    BranchCD: branchCode,
                    HospitalCD: hospitalCode,
                    DoctorID: docid,
                    HospitalID: parentHospitalId
                }
                // imageurl: '',
                // filetype: ''
            }
            $scope.patientnumber = [{
                PhNumber: undefined,
                PhType: 'D',
                PrimaryIND: 'Y',
                FullNumber: '',
                PhDescription: ''
            }, {
                PhNumber: undefined,
                PhType: 'E',
                PrimaryIND: 'Y',
                FullNumber: '',
                PhDescription: ''
            }, {
                PhNumber: undefined,
                PhType: 'M',
                PrimaryIND: 'Y',
                FullNumber: '',
                PhDescription: ''
            }];
            // $scope.surname = '';
        }

        var referralParam = {
            usrid: doctorinfo.doctorid,
            usrtype: "ADMIN",
            pageNumber: 0,
            pageSize: 1000,
            hospitalcd: hospitalCode,
        };

        // Listing Referal details Services
        referralccg.alllistccg(referralParam, ccglistsuccess, ccgerror);

        function ccglistsuccess(data) {
            $scope.ccgData = angular.copy(data);
            $scope.ccgData = $scope.ccgData.filter(data => data.active == true);
            $scope.statusSuccess.ccg += 1;
            // console.log($scope.ccgData);
            if ($scope.statusSuccess.ccg == 2) {
                loadreferral();
            }
        }

        function ccgerror(data) {
            alertify.error("Error in getting CCG list");
        }

        // Listing Issues Services
        c7EnquiriesServices.listManageStatus(isserrSuccess, isserrError);

        function isserrSuccess(data) {
            $scope.issueCollection = data.filter(data => data.managetype == 'ISSUES');
        }

        function isserrError(data) {
            alertify.error("Error in getting CCG list");
        }

        $scope.updateccg = function () {
            console.log($scope.ccgData.selected);

            var ccgParam = angular.copy(referralParam);
            ccgParam.ccgid = $scope.ccgData.selected.ccgid;
            referralccg.getccgDetails(ccgParam, ccgdetailssuccess, ccgerror);

            if ($scope.ccgData.selected.practicedetailval.length == 0) {
                alertify.error("No Practices Found");
            }
            // else {
            // $scope.showPractice = true;
            // $scope.showPractice = false;
            // $scope.showDoctor = false;
            // }
            $scope.addUpdatePat.patientReferral.ccgid = $scope.ccgData.selected.ccgid;
            $scope.addUpdatePat.patientReferral.ccgname = $scope.ccgData.selected.ccgname;
            $scope.addUpdatePat.patientDetails.EnquiryDetails.regionid = $scope.ccgData.selected.regionid;

            $scope.practiceData = $scope.ccgData.selected.practicedetailval.filter(data => data.active == true);
        }
        $scope.updatePractice = function () {
            c7AddUpdatePatient.getDocList($scope.practiceData.selected.practiceid, docListsuccess, docListerror);
            // console.log($scope.practiceData.selected);
            $scope.addUpdatePat.patientReferral.practiceid = $scope.practiceData.selected.practiceid;
            $scope.addUpdatePat.patientReferral.practicename = $scope.practiceData.selected.practicename;

        }

        function docListsuccess(data) {
            if (data.length > 0) {
                // $scope.showDoctor = true;
                $scope.doctorData = angular.copy(data);
            } else {
                // $scope.showDoctor = false;
                alertify.error("No Doctors Found");
            }
        }

        function docListerror(data) {
            alertify.error("Error in getting Doctor list");
        }
        $scope.updateDoctor = function () {
            // console.log($scope.doctorData.selected);
            $scope.addUpdatePat.patientReferral.doctorname = $scope.doctorData.selected.doctorname;
            $scope.addUpdatePat.patientReferral.doctorid = $scope.doctorData.selected.doctorid;
        }

        $scope.checkNHSNumber = function () {
            var a = document.getElementById('getNHS').value;
            a = a.replace(/ /gi, '');
            document.getElementById('getNHS').value = a;

            var nhsValidity = checkNHSValidity();
            if (nhsValidity == false) {
                alertify.error('Invalid NHS Number');
            }
        }

        function checkNHSValidity() {

            $scope.addUpdatePat.patientDetails.PatientOtherDetails.AadhaarID = $scope.addUpdatePat.patientDetails.PatientOtherDetails.AadhaarID.replace(/ /g, '');

            var nhsNumberSample = $scope.addUpdatePat.patientDetails.PatientOtherDetails.AadhaarID;

            var chars = nhsNumberSample.split("");
            var charsLength = chars.length;
            var temp = 0;
            var multiplier = 10;

            if (nhsNumberSample.length > 10 || nhsNumberSample.length < 10) {
                return false;
            }

            for (var i = 0; i < charsLength - 1; i++) {
                temp = temp + (chars[i] * multiplier);
                multiplier--;
            }

            var moduloRemainder = temp % 11;
            var checkDigit = 11 - moduloRemainder;

            if (checkDigit == 11) {
                checkDigit = 0;
            }
            if (checkDigit == chars[charsLength - 1]) {
                return true;
            } else {
                return false;
            }

        }

        // NHS lookup
        $scope.nhslookup = function () {

            $scope.addUpdatePat.patientDetails.PatientOtherDetails.AadhaarID = $scope.addUpdatePat.patientDetails.PatientOtherDetails.AadhaarID.replace(/ /g, '');

            var userParams = {
                NHSNo: $scope.addUpdatePat.patientDetails.PatientOtherDetails.AadhaarID,
                USRId: doctorinfo.doctorid,
                USRType: "ADMIN",
                hospitalInfo: {
                    HospitalID: parentHospitalId,
                    HospitalCD: hospitalCode
                }
            }

            //$scope.patientedit = true;
            c7AddUpdatePatient.getPatientDetails(userParams, getlookupsuccess, getpatienterror);
        }

        // Edit enquiry
        $scope.edit = datacookieFactory.getItem('editPatientId');
        $scope.equid = datacookieFactory.getItem('editPatientEnquiryId');
        ($scope.edit.length == 0) ? $scope.edit = undefined : $scope.edit = $scope.edit;
        if ($scope.edit != null && $scope.edit != undefined) {
            var userParam = {
                PatientID: $scope.edit,
                PatientEnquiryID: $scope.equid,
                USRId: doctorinfo.doctorid,
                USRType: "ADMIN",
                hospitalInfo: {
                    HospitalID: parentHospitalId,
                    HospitalCD: hospitalCode
                }
            }
            $scope.patientedit = true;
            c7AddUpdatePatient.getPatientDetails(userParam, getpatientsuccess, getpatienterror);

        } else {
            $scope.patientedit = false;
            $scope.declareVariable();
        }

        function getpatientsuccess(data) {
            // console.log(data);
            if (data.Patient.length == 0) {
                $scope.declareVariable();
                alertify.error('No patient Found');
            } else {
                var uData = angular.copy(data);
                $scope.patientData = data;
                $scope.statusSuccess.ccg += 1;
                $scope.statusSuccess.speciality += 1;

                $scope.patientnumber = [{
                    PhNumber: undefined,
                    PhType: 'D',
                    PrimaryIND: 'Y',
                    FullNumber: '',
                    PhDescription: ''
                }, {
                    PhNumber: undefined,
                    PhType: 'E',
                    PrimaryIND: 'Y',
                    FullNumber: '',
                    PhDescription: ''
                }, {
                    PhNumber: undefined,
                    PhType: 'M',
                    PrimaryIND: 'Y',
                    FullNumber: '',
                    PhDescription: ''
                }];

                for (var i = 0; i < uData.PatientPhone.length; i++) {
                    var index = $scope.patientnumber.findIndex(x => x.PhType == uData.PatientPhone[i].PhType)
                    $scope.patientnumber[index].PhNumber = uData.PatientPhone[i].PhNumber;
                    $scope.patientnumber[index].FullNumber = uData.PatientPhone[i].FullNumber;
                    $scope.patientnumber[index].PhDescription = uData.PatientPhone[i].PhDescription;
                }

                $scope.email = [];
                if (uData.PatientEmail.length > 0) {
                    $scope.email = [{
                        EmailID: uData.PatientEmail[0].EmailID,
                        PrimaryIND: uData.PatientEmail[0].PrimaryIND,
                        EmailIDType: uData.PatientEmail[0].EmailIDType,
                    }]
                } else {
                    $scope.email = [{
                        EmailID: '',
                        PrimaryIND: 'Y',
                        EmailIDType: 'P'
                    }]
                }
                $scope.addUpdatePat = {
                    USRType: "ADMIN",
                    USRId: doctorinfo.doctorid,
                    patientDetails: {
                        MobileID: $scope.patientnumber[2].PhNumber,
                        Title: uData.Patient[0].Title,
                        FirstName: uData.Patient[0].FirstName,
                        Age: uData.Patient[0].CurrentAge.year,
                        LastName: uData.Patient[0].LastName,
                        // LastName: uData.Patient[0].FullName.replace(uData.Patient[0].FirstName, ''),
                        DOB: moment(uData.Patient[0].DOB).format('DD-MM-YYYY'),
                        FullName: uData.Patient[0].FullName,
                        PatientID: uData.Patient[0].PatientID,
                        Gender: uData.Patient[0].Gender,
                        TermsAccepted: "Y",
                        FamilyHeadIndicator: "Y",
                        EnquiryDetails: {
                            referalcreated: moment(uData.EnquiryDetails.referalcreated).format('DD-MM-YYYY'),
                            referalreceived: moment(uData.EnquiryDetails.referalreceived).format('DD-MM-YYYY'),
                            referalprocessed: moment(uData.EnquiryDetails.referalprocessed).format('DD-MM-YYYY'),
                            clinicalnotes: uData.EnquiryDetails.clinicalnotes,
                            clinicaldetails: uData.EnquiryDetails.clinicaldetails,
                            specialitydetails: uData.EnquiryDetails.specialitydetails,
                            appointmenttypeid: uData.EnquiryDetails.appointmenttypeid,
                            enquirysourceid: uData.EnquiryDetails.enquirysourceid,
                            enquiryfile: {
                                enquiry_real: (uData.EnquiryDetails.enquiryfileurl != undefined && uData.EnquiryDetails.enquiryfileurl.enquiry_doc != undefined) ? uData.EnquiryDetails.enquiryfileurl.enquiry_doc : '',
                                additional_real: (uData.EnquiryDetails.enquiryfileurl != undefined && uData.EnquiryDetails.enquiryfileurl.additional_doc != undefined) ? uData.EnquiryDetails.enquiryfileurl.additional_doc : ''
                            },
                            patientenquiryid: datacookieFactory.getItem('editPatientEnquiryId'),
                            regionid: uData.EnquiryDetails.regionid,
                            checkboxdetails: {
                                isEmail: (uData.EnquiryDetails.checkboxdetails.isEmail == undefined) ? false : uData.EnquiryDetails.checkboxdetails.isEmail,
                                isLetter: (uData.EnquiryDetails.checkboxdetails.isLetter == undefined) ? false : uData.EnquiryDetails.checkboxdetails.isLetter,
                                isVoicemail: (uData.EnquiryDetails.checkboxdetails.isVoicemail == undefined) ? false : uData.EnquiryDetails.checkboxdetails.isVoicemail,
                                isSms: (uData.EnquiryDetails.checkboxdetails.isSms == undefined) ? false : uData.EnquiryDetails.checkboxdetails.isSms,
                                isMobilityissues: (uData.EnquiryDetails.checkboxdetails.isMobilityissues == undefined) ? false : uData.EnquiryDetails.checkboxdetails.isMobilityissues,
                                isCommunicationissues: (uData.EnquiryDetails.checkboxdetails.isCommunicationissues == undefined) ? false : uData.EnquiryDetails.checkboxdetails.isCommunicationissues
                            },
                            issueReason: uData.EnquiryDetails.issueReason,
                            manageid: (uData.EnquiryDetails.manageid == undefined) ? undefined : uData.EnquiryDetails.manageid.toString(),
                            notes: (uData.EnquiryDetails.notes == undefined || uData.EnquiryDetails.notes.trim() == ':') ? '' : uData.EnquiryDetails.notes.trim(),
                            enquirystatusid: uData.EnquiryDetails.enquirystatusid,
                            crtusr: doctorinfo.doctorid,
                            crtusrtype: "ADMIN",
                            updtusr: doctorinfo.doctorid,
                            updtusrtype: "ADMIN"
                        },
                        PatientOtherDetails: {
                            AadhaarID: uData.Patient[0].PatientOtherDetails[0].AadhaarID
                        }
                    },
                    patientEmail: $scope.email,
                    patientAddress: [{
                        PinCode: 0,
                        Country: 'UK',
                        PrimaryIND: 'Y',
                        AddressType: 'H',
                        LandMark: '',
                        DistrictRegion: '',
                        Location: ''
                    }],
                    patientPhone: [],
                    patientReferral: {
                        ccgid: uData.patientReferral.ccgid,
                        ccgname: uData.patientReferral.ccgname,
                        practiceid: uData.patientReferral.practiceid,
                        practicename: uData.patientReferral.practicename,
                        doctorname: uData.patientReferral.doctorname,
                        doctorid: uData.patientReferral.doctorid
                    },
                    // imageurl: $scope.uploadResponseLink,
                    // filetype: $scope.fileType
                }
                if (uData.EnquiryDetails.enquiryfileurl == undefined) {
                    $scope.fileurl = '';
                } else {
                    $scope.fileurl = (uData.EnquiryDetails.enquiryfileurl.enquiry_doc == undefined) ? uData.EnquiryDetails.enquiryfileurl.additional_doc : uData.EnquiryDetails.enquiryfileurl.enquiry_doc;
                }
                if (uData.PatientAddress.length > 0) {
                    $scope.addUpdatePat.patientAddress[0].Address1 = uData.PatientAddress[0].Address1,
                        $scope.addUpdatePat.patientAddress[0].Address2 = uData.PatientAddress[0].Address2,
                        $scope.addUpdatePat.patientAddress[0].Address3 = uData.PatientAddress[0].Address3,
                        $scope.addUpdatePat.patientAddress[0].Address4 = uData.PatientAddress[0].Address4,
                        $scope.addUpdatePat.patientAddress[0].Latitude = uData.PatientAddress[0].Latitude,
                        $scope.addUpdatePat.patientAddress[0].Postcode = uData.PatientAddress[0].Postcode.toUpperCase(),
                        $scope.addUpdatePat.patientAddress[0].State = uData.PatientAddress[0].State,
                        $scope.addUpdatePat.patientAddress[0].Longitude = uData.PatientAddress[0].Longitude,
                        $scope.addUpdatePat.patientAddress[0].CityTown = uData.PatientAddress[0].CityTown
                }
                if ($scope.addUpdatePat.patientDetails.EnquiryDetails.manageid != undefined && $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid > 0 && $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid < 8) {
                    $scope.issuereason = true;
                } else {
                    $scope.issuereason = false;
                }
                // $scope.surname = uData.Patient[0].FullName.replace(uData.Patient[0].FirstName, '');

                if ($scope.statusSuccess.ccg == 2) {
                    loadreferral();
                }
                if ($scope.statusSuccess.speciality == 2) {
                    loadspeciality();
                    validateVal();
                }
            }
        }

        function getlookupsuccess(data) {
            // console.log(data);
            if (data.Patient.length == 0) {
                // $scope.declareVariable();
                alertify.error('No patient Found');
                $scope.patientedit = false;
                $scope.nhsstatus = false;
            } else {
                var uData = angular.copy(data);
                $scope.patientData = angular.copy(data);
                $scope.statusSuccess.ccg += 1;
                $scope.statusSuccess.speciality += 1;
                $scope.patientedit = true;
                $scope.nhsstatus = true;

                $scope.patientnumber = [{
                    PhNumber: undefined,
                    PhType: 'D',
                    PrimaryIND: 'Y',
                    FullNumber: '',
                    PhDescription: ''
                }, {
                    PhNumber: undefined,
                    PhType: 'E',
                    PrimaryIND: 'Y',
                    FullNumber: '',
                    PhDescription: ''
                }, {
                    PhNumber: undefined,
                    PhType: 'M',
                    PrimaryIND: 'Y',
                    FullNumber: '',
                    PhDescription: ''
                }];

                for (var i = 0; i < uData.PatientPhone.length; i++) {
                    var index = $scope.patientnumber.findIndex(x => x.PhType == uData.PatientPhone[i].PhType)
                    $scope.patientnumber[index].PhNumber = uData.PatientPhone[i].PhNumber;
                    $scope.patientnumber[index].FullNumber = uData.PatientPhone[i].FullNumber;
                    $scope.patientnumber[index].PhDescription = uData.PatientPhone[i].PhDescription;
                }
                $scope.email = [];
                if (uData.PatientEmail.length > 0) {
                    $scope.email = [{
                        EmailID: uData.PatientEmail[0].EmailID,
                        PrimaryIND: uData.PatientEmail[0].PrimaryIND,
                        EmailIDType: uData.PatientEmail[0].EmailIDType,
                    }]
                } else {
                    $scope.email = [{
                        EmailID: '',
                        PrimaryIND: 'Y',
                        EmailIDType: 'P'
                    }]
                }
                $scope.addUpdatePat = {
                    USRType: "ADMIN",
                    USRId: doctorinfo.doctorid,
                    patientDetails: {
                        MobileID: $scope.patientnumber[2].PhNumber,
                        Title: uData.Patient[0].Title,
                        FirstName: uData.Patient[0].FirstName,
                        LastName: uData.Patient[0].LastName,
                        // LastName: uData.Patient[0].FullName.replace(uData.Patient[0].FirstName, ''),
                        Age: uData.Patient[0].CurrentAge.year,
                        DOB: moment(uData.Patient[0].DOB).format('DD-MM-YYYY'),
                        FullName: uData.Patient[0].FullName,
                        PatientID: uData.Patient[0].PatientID,
                        Gender: uData.Patient[0].Gender,
                        TermsAccepted: "Y",
                        FamilyHeadIndicator: "Y",
                        EnquiryDetails: {
                            referalcreated: '',
                            referalreceived: '',
                            referalprocessed: moment().format(''),
                            clinicalnotes: '',
                            clinicaldetails: '',
                            checkboxdetails: {
                                isEmail: false,
                                isLetter: false,
                                isVoicemail: false,
                                isSms: false,
                                isMobilityissues: false,
                                isCommunicationissues: false
                            },
                            manageid: undefined,
                            issueReason: '',
                            regionid: undefined,
                            specialitydetails: [{
                                speciality: '',
                                specialityid: '',
                                activeInd: 'Y',
                                subSpecialities: {}
                            }],
                            appointmenttypeid: '',
                            enquirysourceid: '',
                            enquiryfile: {
                                enquiry_real: $scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile.enquiry_real,
                                additional_real: $scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile.additional_real
                            },
                            enquirystatusid: 'TRIAGE',
                            notes: '',
                            crtusr: doctorinfo.doctorid,
                            crtusrtype: 'ADMIN',
                            updtusr: doctorinfo.doctorid,
                            updtusrtype: 'ADMIN'
                        },
                        PatientOtherDetails: {
                            AadhaarID: uData.Patient[0].PatientOtherDetails[0].AadhaarID
                        }
                    },
                    patientEmail: $scope.email,
                    patientAddress: [{
                        PinCode: 0,
                        Country: 'UK',
                        PrimaryIND: 'Y',
                        AddressType: 'H',
                        LandMark: '',
                        DistrictRegion: '',
                        Location: ''
                    }],
                    patientPhone: [],
                    patientReferral: {
                        ccgid: undefined,
                        ccgname: '',
                        practiceid: undefined,
                        practicename: '',
                        doctorname: '',
                        doctorid: undefined
                    },
                    // imageurl: $scope.uploadResponseLink,
                    // filetype: $scope.fileType
                }
                // $scope.fileurl = (uData.EnquiryDetails.enquiryfileurl.enquiry_doc == undefined) ? uData.EnquiryDetails.enquiryfileurl.additional_doc : uData.EnquiryDetails.enquiryfileurl.enquiry_doc;
                if (uData.PatientAddress.length > 0) {
                    $scope.addUpdatePat.patientAddress[0].Address1 = uData.PatientAddress[0].Address1,
                        $scope.addUpdatePat.patientAddress[0].Address2 = uData.PatientAddress[0].Address2,
                        $scope.addUpdatePat.patientAddress[0].Address3 = uData.PatientAddress[0].Address3,
                        $scope.addUpdatePat.patientAddress[0].Address4 = uData.PatientAddress[0].Address4,
                        $scope.addUpdatePat.patientAddress[0].Latitude = uData.PatientAddress[0].Latitude,
                        $scope.addUpdatePat.patientAddress[0].Postcode = uData.PatientAddress[0].Postcode.toUpperCase(),
                        $scope.addUpdatePat.patientAddress[0].State = uData.PatientAddress[0].State,
                        $scope.addUpdatePat.patientAddress[0].Longitude = uData.PatientAddress[0].Longitude,
                        $scope.addUpdatePat.patientAddress[0].CityTown = uData.PatientAddress[0].CityTown
                }
                if ($scope.addUpdatePat.patientDetails.EnquiryDetails.manageid != undefined && $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid < 8) {
                    $scope.issuereason = true;
                } else {
                    $scope.issuereason = false;
                }
                $scope.lookupStatus = true;
                // $scope.surname = uData.Patient[0].FullName.replace(uData.Patient[0].FirstName, '');

                validateVal();
                // if ($scope.statusSuccess.ccg == 2) {
                //     loadreferral();
                // }
                // if ($scope.statusSuccess.speciality == 2) {
                //     loadspeciality();
                // }
            }
        }

        function getpatienterror() {
            alertify.error("Error in Getting Patient Details");
        }

        function loadreferral() {

            // $scope.showPractice = true;
            // $scope.showDoctor = true;
            angular.forEach($scope.ccgData, function (val) {
                if (val.ccgid == $scope.patientData.patientReferral.ccgid) {
                    $scope.ccgData.selected = val;
                    $scope.activeCCG = val;
                    $scope.addUpdatePat.patientDetails.EnquiryDetails.regionid = $scope.ccgData.selected.regionid;
                    if (val.practicedetailval.length > 0) {
                        $scope.practiceData = val.practicedetailval;
                        angular.forEach(val.practicedetailval, function (prac) {
                            if (prac.practiceid == $scope.patientData.patientReferral.practiceid) {
                                $scope.practiceData.selected = prac;
                            }
                        });
                        if ($scope.practiceData.selected == undefined) {
                            c7AddUpdatePatient.getDocList(0, doceditListsuccess, doceditListerror);
                        } else {
                            c7AddUpdatePatient.getDocList($scope.practiceData.selected.practiceid, doceditListsuccess, doceditListerror);
                        }
                    }
                }
            })

            var ccgParam = angular.copy(referralParam);
            ccgParam.ccgid = $scope.ccgData.selected.ccgid;
            referralccg.getccgDetails(ccgParam, ccgdetailssuccess, ccgerror);
            $scope.statusSuccess.ccg = 0;
        }

        function doceditListsuccess(data) {
            if (data.length > 0) {
                // $scope.showDoctor = true;
                $scope.doctorData = angular.copy(data);
                angular.forEach(data, function (val) {
                    if (val.doctorid == $scope.patientData.patientReferral.doctorid) {
                        $scope.doctorData.selected = val;
                    }
                });
            } else {
                // $scope.showDoctor = false;
                alertify.error("No Doctors edit Found");
            }
        }

        function doceditListerror(data) {
            alertify.error("Error in getting Doctor edit list");
        }

        $scope.calculateAge = function (birthday) {
            $scope.addUpdatePat.patientDetails.Age = moment().diff(moment(birthday, 'DD-MM-YYYY'), 'years');
            validateVal();
        }

        $scope.convertDateymd = function (date) {
            return moment(moment(date, 'DD-MM-YYYY')).format('YYYY-MM-DD')
        }
        // Add enquiry
        $scope.register = function () {

            $scope.addUpdatePat.patientAddress[0].Postcode = $scope.addUpdatePat.patientAddress[0].Postcode.toUpperCase();

            $scope.addUpdatePat.patientDetails.PatientOtherDetails.AadhaarID = $scope.addUpdatePat.patientDetails.PatientOtherDetails.AadhaarID.replace(/ /g, '');

            var nhsValidity = checkNHSValidity();
            if (nhsValidity == false) {
                alertify.error('Invalid NHS Number');
                return false;
            }

            if ($scope.addUpdatePat.patientAddress[0].Address1 == null || $scope.addUpdatePat.patientAddress[0].Address1 == '') {
                delete $scope.addUpdatePat.patientAddress[0].Address1;
            }
            if ($scope.addUpdatePat.patientAddress[0].Address2 == null || $scope.addUpdatePat.patientAddress[0].Address2 == '') {
                delete $scope.addUpdatePat.patientAddress[0].Address2;
            }
            if ($scope.addUpdatePat.patientAddress[0].Address3 == null || $scope.addUpdatePat.patientAddress[0].Address3 == '') {
                delete $scope.addUpdatePat.patientAddress[0].Address3;
            }
            if ($scope.addUpdatePat.patientAddress[0].Address4 == null || $scope.addUpdatePat.patientAddress[0].Address4 == '') {
                delete $scope.addUpdatePat.patientAddress[0].Address4;
            }
            if ($scope.addUpdatePat.patientAddress[0].CityTown == null || $scope.addUpdatePat.patientAddress[0].CityTown == '') {
                delete $scope.addUpdatePat.patientAddress[0].CityTown;
            }
            if ($scope.addUpdatePat.patientAddress[0].State == null || $scope.addUpdatePat.patientAddress[0].State == '') {
                delete $scope.addUpdatePat.patientAddress[0].State;
            }

            if ($scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile.enquiry_real == '' && $scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile.additional_real == '') {
                alertify.error('Please choose a document');
                return false;
            }
            if ($scope.issuereason && $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid == '0') {
                alertify.error('Choose issue reason or uncheck issues');
                return false;
            }

            if (moment($scope.addUpdatePat.patientDetails.DOB, 'DD-MM-YYYY').format('DD-MM-YYYY') == 'Invalid date') {
                alertify.error('Please give valid DOB');
                return false;
            }

            if ($scope.addUpdatePat.patientReferral.ccgname == '' || $scope.addUpdatePat.patientReferral.ccgname == null || $scope.addUpdatePat.patientReferral.practicename == '' ||
                $scope.addUpdatePat.patientReferral.practicename == '' || $scope.addUpdatePat.patientReferral.doctorname == '' || $scope.addUpdatePat.patientReferral.doctorname == '') {
                alertify.error('Please give valid Referrer Details');
                return false;
            }

            fillspeciality();
            $scope.addUpdatePat.patientDetails.EnquiryDetails.specialitydetails = $scope.newSpecialityCollection;
            if ($scope.addUpdatePat.patientEmail[0].EmailID == '') {
                $scope.addUpdatePat.patientEmail = [];
            }
            $scope.addUpdatePat.patientDetails.MobileID = ($scope.patientnumber[2].FullNumber == '') ? docid : parseFloat($scope.patientnumber[2].FullNumber);
            if ($scope.addUpdatePat.patientDetails.EnquiryDetails.manageid == undefined || $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid == null) {
                $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid = 16;
            } else {
                $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid = parseInt($scope.addUpdatePat.patientDetails.EnquiryDetails.manageid);
            }
            if ($scope.issuereason) {
                $scope.addUpdatePat.patientDetails.EnquiryDetails.enquirystatusid = 'ISSUES';
                var issueDesc = $scope.issueCollection.filter(data => data.manageid == $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid)[0].managedesc;
                $scope.addUpdatePat.patientDetails.EnquiryDetails.notes = ($scope.addUpdatePat.patientDetails.EnquiryDetails.notes == undefined) ? '' : $scope.addUpdatePat.patientDetails.EnquiryDetails.notes;
                // if ($scope.addUpdatePat.patientDetails.EnquiryDetails.notes.trim() != '') {
                //     if (issueDesc == 'others') {
                //         $scope.addUpdatePat.patientDetails.EnquiryDetails.notes = $scope.otherIssues + ': ' + $scope.addUpdatePat.patientDetails.EnquiryDetails.notes;
                //     } else {
                //         $scope.addUpdatePat.patientDetails.EnquiryDetails.notes = issueDesc + ': ' + $scope.addUpdatePat.patientDetails.EnquiryDetails.notes;
                //     }
                // }
            }
            if ($scope.agespeciality.age || $scope.agespeciality.speciality) {
                $scope.addUpdatePat.patientDetails.EnquiryDetails.enquirystatusid = 'ISSUES';
                $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid = 1;
            }
            for (var i = 0; i < $scope.patientnumber.length; i++) {
                if ($scope.patientnumber[i].FullNumber == '') {
                    $scope.patientnumber[i].PhNumber = undefined;
                    $scope.patientnumber.splice(i, 1);
                    i--;
                } else {
                    $scope.patientnumber[i].PhNumber = parseFloat($scope.patientnumber[i].FullNumber);
                }
            }
            $scope.addUpdatePat.patientPhone = angular.copy($scope.patientnumber);
            $scope.addUpdatePat.patientDetails.FullName = $scope.addUpdatePat.patientDetails.FirstName + ' ' + $scope.addUpdatePat.patientDetails.LastName;
            var regdata = angular.copy($scope.addUpdatePat);

            regdata.GenIDDetails = {
                BranchCD: branchCode,
                HospitalCD: hospitalCode,
                DoctorID: docid,
                HospitalID: parentHospitalId
            }
            regdata.patientDetails.DOB = $scope.convertDateymd(regdata.patientDetails.DOB);
            regdata.patientDetails.EnquiryDetails.referalcreated = $scope.convertDateymd(regdata.patientDetails.EnquiryDetails.referalcreated);
            regdata.patientDetails.EnquiryDetails.referalreceived = $scope.convertDateymd(regdata.patientDetails.EnquiryDetails.referalreceived);
            regdata.patientDetails.EnquiryDetails.referalprocessed = $scope.convertDateymd(regdata.patientDetails.EnquiryDetails.referalprocessed);
            c7AddUpdatePatient.addPatient(regdata, patientsuccess, addpatienterror);
            $scope.patientnumber = [{
                PhNumber: undefined,
                PhType: 'D',
                PrimaryIND: 'Y',
                FullNumber: '',
                PhDescription: ''
            }, {
                PhNumber: undefined,
                PhType: 'E',
                PrimaryIND: 'Y',
                FullNumber: '',
                PhDescription: ''
            }, {
                PhNumber: undefined,
                PhType: 'M',
                PrimaryIND: 'Y',
                FullNumber: '',
                PhDescription: ''
            }];
            if ($scope.addUpdatePat.patientEmail.length == 0) {
                $scope.addUpdatePat.patientEmail = [{
                    EmailID: '',
                    EmailIDType: 'P',
                    PrimaryIND: 'Y'
                }];
            }

            if ($scope.agespeciality.age || $scope.agespeciality.speciality) {
                $('#emailExclusion').modal('show');
                $scope.emailexclusion = {
                    patientenquiryid: 0,
                    appointmentid: 0,
                    sendemail: true,
                    sendletter: true
                }
                $scope.emailUpdateParam = {
                    patientid: 0,
                    patientenquiryid: 0,
                    usrid: docid,
                    statustypeid: "exclusionEmail"
                }
            }
        }
        function addpatienterror(data) {
            alertify.error('Error in add patient');
        }
        function updatepatienterror(data) {
            alertify.error('Error in add patient')
        }
        // Update enquiry
        $scope.update = function () {

            $scope.addUpdatePat.patientAddress[0].Postcode = $scope.addUpdatePat.patientAddress[0].Postcode.toUpperCase();

            $scope.addUpdatePat.patientDetails.PatientOtherDetails.AadhaarID = $scope.addUpdatePat.patientDetails.PatientOtherDetails.AadhaarID.replace(/ /g, '');

            var nhsValidity = checkNHSValidity();
            if (nhsValidity == false) {
                alertify.error('Invalid NHS Number');
                return false;
            }

            if ($scope.addUpdatePat.patientAddress[0].Address1 == null || $scope.addUpdatePat.patientAddress[0].Address1 == '') {
                delete $scope.addUpdatePat.patientAddress[0].Address1;
            }
            if ($scope.addUpdatePat.patientAddress[0].Address2 == null || $scope.addUpdatePat.patientAddress[0].Address2 == '') {
                delete $scope.addUpdatePat.patientAddress[0].Address2;
            }
            if ($scope.addUpdatePat.patientAddress[0].Address3 == null || $scope.addUpdatePat.patientAddress[0].Address3 == '') {
                delete $scope.addUpdatePat.patientAddress[0].Address3;
            }
            if ($scope.addUpdatePat.patientAddress[0].Address4 == null || $scope.addUpdatePat.patientAddress[0].Address4 == '') {
                delete $scope.addUpdatePat.patientAddress[0].Address4;
            }
            if ($scope.addUpdatePat.patientAddress[0].CityTown == null || $scope.addUpdatePat.patientAddress[0].CityTown == '') {
                delete $scope.addUpdatePat.patientAddress[0].CityTown;
            }
            if ($scope.addUpdatePat.patientAddress[0].State == null || $scope.addUpdatePat.patientAddress[0].State == '') {
                delete $scope.addUpdatePat.patientAddress[0].State;
            }

            // if ($scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile.enquiry_real == '' && $scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile.additional_real == '') {
            //     alertify.error('Please choose a document');
            //     return false;
            // }
            // if ($scope.issuereason && $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid == '0') {
            //     alertify.error('Choose issue reason or uncheck issues');
            //     return false;
            // }

            if (moment($scope.addUpdatePat.patientDetails.DOB, 'DD-MM-YYYY').format('DD-MM-YYYY') == 'Invalid date') {
                alertify.error('Please give valid DOB');
                return false;
            }

            // if ($scope.addUpdatePat.patientReferral.ccgname == '' || $scope.addUpdatePat.patientReferral.ccgname == null || $scope.addUpdatePat.patientReferral.practicename == '' ||
            //     $scope.addUpdatePat.patientReferral.practicename == '' || $scope.addUpdatePat.patientReferral.doctorname == '' || $scope.addUpdatePat.patientReferral.doctorname == '') {
            //     alertify.error('Please give valid Referrer Details');
            //     return false;
            // }

            if ($scope.selectUserInfo.patientid != undefined || $scope.lookupStatus) {
                if ($scope.selectedCollection.patientenquiryid != [] && $scope.selectedCollection.patientenquiryid != undefined && $scope.selectedCollection.patientenquiryid.length != 0) {

                    patientDetail();
                }
                fillspeciality();
                $scope.addUpdatePat.patientDetails.EnquiryDetails.specialitydetails = $scope.newSpecialityCollection;
                $scope.lookupStatus = false;
            }
            if ($scope.addUpdatePat.patientEmail[0].EmailID == '') {
                $scope.addUpdatePat.patientEmail = [];
            }
            $scope.patientphone = [{
                PhNumber: undefined,
                PhType: 'D',
                PrimaryIND: 'Y',
                FullNumber: '',
                PhDescription: ''
            }, {
                PhNumber: undefined,
                PhType: 'E',
                PrimaryIND: 'Y',
                FullNumber: '',
                PhDescription: ''
            }, {
                PhNumber: undefined,
                PhType: 'M',
                PrimaryIND: 'Y',
                FullNumber: '',
                PhDescription: ''
            }];


            for (var i = 0; i < $scope.patientData.PatientPhone.length; i++) {
                var index = $scope.patientphone.findIndex(x => x.PhType == $scope.patientData.PatientPhone[i].PhType)
                $scope.patientphone[index].PhNumber = $scope.patientData.PatientPhone[i].PhNumber;
                $scope.patientphone[index].FullNumber = $scope.patientData.PatientPhone[i].FullNumber;
                $scope.patientphone[index].PhDescription = $scope.patientData.PatientPhone[i].PhDescription;
            }

            $scope.email = [];
            if ($scope.patientData.PatientEmail.length > 0) {
                $scope.email = [{
                    EmailID: $scope.patientData.PatientEmail[0].EmailID,
                    PrimaryIND: $scope.patientData.PatientEmail[0].PrimaryIND,
                    EmailIDType: $scope.patientData.PatientEmail[0].EmailIDType,
                }]
            } else {
                $scope.email = [{
                    EmailID: '',
                    PrimaryIND: 'Y',
                    EmailIDType: 'P'
                }]
            }


            for (var i = 0; i < $scope.patientnumber.length; i++) {
                if ($scope.patientnumber[i].FullNumber == '') {
                    $scope.patientnumber.splice(i, 1);
                    i--;
                } else {
                    $scope.patientnumber[i].PhNumber = parseFloat($scope.patientnumber[i].FullNumber);
                }
            }


            //   $scope.patientData.PatientEmail[0].EmailID = $scope.addUpdatePat.PatientEmail;
            $scope.addUpdatePat.patientPhone = angular.copy($scope.patientnumber);
            // $scope.addUpdatePat.patientEmail = $scope.patientData.patientEmail;
            $scope.addUpdatePat.patientDetails.FullName = $scope.addUpdatePat.patientDetails.FirstName + ' ' + $scope.addUpdatePat.patientDetails.LastName;
            var uptdata = {
                USRType: "ADMIN",
                USRId: doctorinfo.doctorid,
                patientDetails: {
                    MobileID: $scope.addUpdatePat.patientDetails.MobileID,
                    Title: $scope.addUpdatePat.patientDetails.Title,
                    FirstName: $scope.addUpdatePat.patientDetails.FirstName,
                    Age: $scope.addUpdatePat.patientDetails.Age,
                    LastName: $scope.addUpdatePat.patientDetails.LastName,
                    DOB: $scope.addUpdatePat.patientDetails.DOB,
                    FullName: $scope.addUpdatePat.patientDetails.FullName,
                    PatientID: $scope.addUpdatePat.patientDetails.PatientID,
                    Gender: $scope.addUpdatePat.patientDetails.Gender,
                    TermsAccepted: $scope.addUpdatePat.patientDetails.TermsAccepted,
                    FamilyHeadIndicator: "Y",
                    PatientOtherDetails: {
                        AadhaarID: $scope.addUpdatePat.patientDetails.PatientOtherDetails.AadhaarID,
                    }
                },
                patientEmail: $scope.addUpdatePat.patientEmail,
                patientAddress: [{
                    PinCode: 0,
                    Country: $scope.addUpdatePat.patientAddress[0].Country,
                    PrimaryIND: "Y",
                    AddressType: "H",
                    LandMark: "",
                    DistrictRegion: "",
                    Location: "",
                    Address1: $scope.addUpdatePat.patientAddress[0].Address1,
                    Address2: $scope.addUpdatePat.patientAddress[0].Address2,
                    Latitude: "51.729129791259766",
                    Postcode: $scope.addUpdatePat.patientAddress[0].Postcode,
                    State: $scope.addUpdatePat.patientAddress[0].State,
                    Longitude: "-1.207820177078247",
                    CityTown: $scope.addUpdatePat.patientAddress[0].CityTown,
                }],
                patientPhone: $scope.addUpdatePat.patientPhone,
                GenIDDetails: {

                    BranchCD: branchCode,
                    HospitalCD: hospitalCode,
                    DoctorID: docid,
                    HospitalID: parentHospitalId
                }
            }
            uptdata.patientDetails.DOB = $scope.convertDateymd(uptdata.patientDetails.DOB);
            c7AddUpdatePatient.updatePatient(uptdata, patientsuccess, updatepatienterror);

            $scope.patientphone = [{
                PhNumber: undefined,
                PhType: 'D',
                PrimaryIND: 'Y',
                FullNumber: '',
                PhDescription: ''
            }, {
                PhNumber: undefined,
                PhType: 'E',
                PrimaryIND: 'Y',
                FullNumber: '',
                PhDescription: ''
            }, {
                PhNumber: undefined,
                PhType: 'M',
                PrimaryIND: 'Y',
                FullNumber: '',
                PhDescription: ''
            }];
            if ($scope.addUpdatePat.patientEmail.length == 0) {
                $scope.addUpdatePat.patientEmail = [{
                    EmailID: '',
                    EmailIDType: 'P',
                    PrimaryIND: 'Y'
                }];
            }

            if ($scope.agespeciality.age || $scope.agespeciality.speciality) {
                $('#emailExclusion').modal('show');
                $scope.emailexclusion = {
                    patientenquiryid: ($scope.equid == undefined) ? 0 : $scope.equid,
                    appointmentid: 0,
                    sendemail: true,
                    sendletter: true
                }
                $scope.emailUpdateParam = {
                    patientid: ($scope.edit.length == undefined) ? $scope.edit : 0,
                    patientenquiryid: $scope.equid,
                    usrid: docid,
                    statustypeid: "exclusionEmail"
                }
            }
        }
        $scope.download = false;
        $scope.exclusionemail = function () {
            if ($scope.emailexclusion.sendletter) {
                $scope.download = true;
            } else {
                $scope.download = false;
            }
            c7EnquiriesServices.enquiryaddstatus($scope.emailUpdateParam, enquiryaddstatussuccess, addstatuserror);
            c7EnquiriesServices.sendExclusionEmail($scope.emailexclusion, emailSuccess, exclusionerror);
        }
        function exclusionerror(data) {
            alertify.error('Error in sending email');
        }
        function addstatuserror(data) {
            alertify.error('Error in adding status');
        }
        function emailSuccess(data) {
            if (!data.error && data.includes('<!doctype html') && $scope.download) {
                $scope.printmail(data)
                $scope.download = false;
            } else if (!data.error) {
                alertify.success('Email Sent Successfuly.');
            } else {
                alertify.error('Error in email sending.');
            }
            $scope.cancel();
        }
        function patientDetail(notes) {
            // $scope.enquiryNewid = datacookieFactory.getItem('editPatientEnquiryId');
            var params = {
                patientid: $scope.selectUserInfo.patientid,
                patientenquiryid: $scope.selectedCollection.patientenquiryid,
                usrid: docid,
                statustypeid: "PatientDetail",
                //  notes: $scope.addUpdatePat.patientDetails.EnquiryDetails.notes
            }
            c7EnquiriesServices.enquiryaddstatus(params, enquiryaddstatussuccess, addstatuserror);
        }
        // function EnquiryAddStatus(notes) {
        //     // $scope.enquiryNewid = datacookieFactory.getItem('editPatientEnquiryId');
        //     var params = {
        //         patientid: $scope.selectUserInfo.patientid,
        //         patientenquiryid: $scope.selectedCollection.patientenquiryid,
        //         usrid: docid,
        //         statustypeid: "ReferralForm",
        //         //  notes: $scope.addUpdatePat.patientDetails.EnquiryDetails.notes
        //     }
        //     c7EnquiriesServices.enquiryaddstatus(params, enquiryaddstatussuccess, addstatuserror);
        // }

        function newEnquiryAddStatus(notes) {
            // $scope.enquiryNewid = datacookieFactory.getItem('editPatientEnquiryId');
            var params = {
                patientid: $scope.selectUserInfo.patientid,
                patientenquiryid: $scope.selectedCollection.patientenquiryid,
                usrid: docid,
                statustypeid: "ReferralDetail",
                //notes: $scope.addUpdatePat.patientDetails.EnquiryDetails.notes
            }
            c7EnquiriesServices.enquiryaddstatus(params, enquiryaddstatussuccess, addstatuserror);
        }

        function enquiryaddstatussuccess(data) {

        }
        $scope.addenquirystatus = function () {
            newEnquiryAddStatus();
        }

        $scope.cancel = function () {
            $('.modal-backdrop').remove();
            datacookieFactory.remove('editPatientId');
            datacookieFactory.remove('editPatientEnquiryId');
            var guid = (getKWID() + getKWID() + "-" + getKWID() + "-4" + getKWID().substr(0, 3) + "-" + getKWID() + "-" + getKWID() + getKWID() + getKWID()).toLowerCase();
            var url = "searchpatient/?kwid=" + (guid || "");
            $location.url(url);
        }

        function patientsuccess(data) {

            $scope.emailexclusion = {
                patientenquiryid: 0,
                appointmentid: 0,
                sendemail: true,
                sendletter: true
            }
            $scope.emailUpdateParam = {
                patientid: 0,
                patientenquiryid: 0,
                usrid: docid,
                statustypeid: "exclusionEmail"
            }

            // if (!data.error) {
            //     if (data.EnquiryDetails == undefined && data.patientIdDetails != undefined) {
            //         $scope.emailexclusion.patientenquiryid = data.patientIdDetails.PatientEnquiryID;
            //         $scope.emailUpdateParam.patientid = data.patientIdDetails.PatientID;
            //         $scope.emailUpdateParam.patientenquiryid = data.patientIdDetails.PatientEnquiryID;
            //     } else {
            //         $scope.emailexclusion.patientenquiryid = (data.EnquiryDetails.patientenquiryid == undefined) ? 0 : data.EnquiryDetails.patientenquiryid;
            //         $scope.emailUpdateParam.patientid = (data.EnquiryDetails == undefined || data.EnquiryDetails.patientid == undefined) ? 0 : data.EnquiryDetails.patientid;
            //         $scope.emailUpdateParam.patientenquiryid = (data.EnquiryDetails == undefined || data.EnquiryDetails.patientenquiryid == undefined) ? 0 : data.EnquiryDetails.patientenquiryid;
            //     }

            datacookieFactory.remove('editPatientId')
            datacookieFactory.remove('editPatientEnquiryId');
            $scope.declareVariable();
            if (!$scope.agespeciality.age && !$scope.agespeciality.speciality) {
                $scope.cancel();
            }
            else {
                if (data.error.message.includes('AadhaarID') && data.error.message.includes('NHS')) {
                    alertify.error("NHS Number Already Exist");
                } else {
                    alertify.error(data.error.message);
                }
            }
        }

        function patientspecialityerror(data) {
            alertify.error("Error in speciality list");
        }


        // Speciality
        c7AdminServices.SpecialityList(parentHospitalId, specialitylistSuccess, patientspecialityerror)

        function specialitylistSuccess(data) {
            $scope.specialityCollection = data;
            $scope.statusSuccess.speciality += 1;
            if ($scope.statusSuccess.speciality == 2) {
                loadspeciality();
            }
        }
        $scope.subSpecialityCollection = [];
        $scope.subSpecialityCollection.selected = [];
        $scope.selectspeciality = function () {
            angular.forEach($scope.specialityCollection.selected, function (i) {
                angular.forEach(i.subspeciality, function (j) {
                    j.spelid = i.specialityID;
                    j.speldesc = i.specialitydesc;
                    if ($scope.subSpecialityCollection.findIndex(d => d.DoctorSpecialityID == j.DoctorSpecialityID) == -1) {
                        $scope.subSpecialityCollection.push(j);
                        // $scope.subSpecialityCollection.selected.push(j);
                    }
                })
            });
            for (var i = 0; i < $scope.subSpecialityCollection.length; i++) {
                var index = $scope.specialityCollection.selected.findIndex(d => d.specialityID == $scope.subSpecialityCollection[i].spelid);
                if (index == -1) {
                    $scope.subSpecialityCollection.splice(i, 1);
                    i--;
                }
            }
            if ($scope.subSpecialityCollection.selected != undefined) {
                for (var i = 0; i < $scope.subSpecialityCollection.selected.length; i++) {
                    var selindex = $scope.specialityCollection.selected.findIndex(d => d.specialityID == $scope.subSpecialityCollection.selected[i].spelid);
                    if (selindex == -1) {
                        $scope.subSpecialityCollection.selected.splice(i, 1);
                        i--;
                    }
                }
            }
            // $scope.addUpdatePat.patientDetails.EnquiryDetails.specialitydetails.specialityid = $scope.specialityCollection.selected.specialityID;
            // $scope.addUpdatePat.patientDetails.EnquiryDetails.specialitydetails.speciality = $scope.specialityCollection.selected.specialitydesc;
            // if ($scope.specialityCollection.selected.subspeciality.length > 0) {
            //     $scope.subSpecialityCollection = $scope.specialityCollection.selected.subspeciality;
            // } else {
            //     $scope.subSpecialityCollection = [];
            // }
            validateVal();
        }

        // $scope.selectsubspeciality = function () {
        //     $scope.addUpdatePat.patientDetails.EnquiryDetails.specialitydetails.subspeciality = $scope.subSpecialityCollection.selected.DoctorSpecialityDesc;
        // }

        function loadspeciality() {

            if ($scope.specialityCollection != undefined) {

                $scope.subSpecialityCollection = [];
                $scope.specialityCollection.selected = [];
                $scope.subSpecialityCollection.selected = [];
                var curspel = angular.copy($scope.addUpdatePat.patientDetails.EnquiryDetails.specialitydetails);
                angular.forEach(curspel, function (i) {
                    var data = $scope.specialityCollection.filter(data => data.specialityID == i.specialityid);
                    $scope.specialityCollection.selected = $scope.specialityCollection.selected.concat(data);
                    angular.forEach($scope.specialityCollection.selected, function (x) {
                        angular.forEach(x.subspeciality, function (y) {
                            y.spelid = x.specialityID;
                            if ($scope.subSpecialityCollection.findIndex(d => d.DoctorSpecialityID == y.DoctorSpecialityID) == -1) {
                                $scope.subSpecialityCollection.push(y);
                            }
                        })
                    })
                    var arr = Object.values(i.subSpecialities);
                    // angular.forEach(j.subspeciality, function(p) { p.isActive = false });
                    angular.forEach(arr, function (p) {
                        var subdata = $scope.subSpecialityCollection.filter(data => data.DoctorSpecialityID == p);
                        angular.forEach(subdata, function (x) {
                            if ($scope.subSpecialityCollection.selected.findIndex(d => d.DoctorSpecialityID == x.DoctorSpecialityID) == -1) {
                                $scope.subSpecialityCollection.selected.push(x);
                            }
                        })
                    });

                });
                // angular.forEach($scope.specialityColl, function(i){
                //     var data = $scope.subSpecialityCollection.filter(data => data.specialityID == i.specialityid);
                //     $scope.subSpecialityColl = $scope.subSpecialityColl.concat(data);
                // });
                // // console.log($scope.specialityCollection);
                // if ($scope.specialityCollection.selected != undefined && $scope.specialityCollection.selected.subspeciality.length > 0) {
                //     $scope.subSpecialityCollection = $scope.specialityCollection.selected.subspeciality;
                //     $scope.subSpecialityCollection.selected = $scope.subSpecialityCollection.filter(data => data.DoctorSpecialityDesc == $scope.addUpdatePat.patientDetails.EnquiryDetails.specialitydetails.subspeciality)[0];
                // }
            }

            $scope.statusSuccess.speciality = 0;
        }

        function fillspeciality() {
            $scope.newSpecialityCollection = [];
            angular.forEach($scope.specialityCollection.selected, function (i) {
                var params = {
                    speciality: i.specialitydesc,
                    specialityid: i.specialityID,
                    activeInd: 'Y',
                    subSpecialities: {}
                }
                if (i.id != undefined) {
                    params.id = i.id;
                }
                angular.forEach(i.subspeciality, function (k, key) {
                    if ($scope.subSpecialityCollection.selected.findIndex(p => p.DoctorSpecialityID == k.DoctorSpecialityID) > -1) {
                        params.subSpecialities[key] = k.DoctorSpecialityID;
                    }
                })
                $scope.newSpecialityCollection.push(params);
            })
            // angular.forEach($scope.receivedSpeciality, function(i){
            //     $scope.newSpecialityCollection.filter(data => data.specialityid == i.specialityid).forEach(function(j){
            //         j.id = i.id;
            //     })
            // })
        }
        // Post code
        $scope.getloockup = function () {
            $scope.addressCollection = [];
            if ($scope.addUpdatePat.patientAddress[0].Postcode == '') {
                return false;
            }
            c7PostCodeServices.listpostcodeinfo($scope.addUpdatePat.patientAddress[0].Postcode, postcodeSuccess, postcodeerror)
        }

        function postcodeSuccess(data) {

            $scope.addressCollection = data.addresses;
            $scope.addUpdatePat.patientAddress[0].Latitude = data.latitude.toString();
            $scope.addUpdatePat.patientAddress[0].Longitude = data.longitude.toString();
        }

        function postcodeerror(data) {
            console.log(data);
        }

        $scope.selectpostcode = function (list) {
            // latlong(list);

            $scope.addressSplit = list.split(', ');
            $scope.addUpdatePat.patientAddress[0].Address1 = $scope.addressSplit[0] == ' ' ? '' : $scope.addressSplit[0];
            $scope.addUpdatePat.patientAddress[0].Address2 = $scope.addressSplit[1] == ' ' ? '' : $scope.addressSplit[1];
            $scope.addUpdatePat.patientAddress[0].Address3 = $scope.addressSplit[2] == ' ' ? '' : $scope.addressSplit[2];
            $scope.addUpdatePat.patientAddress[0].Address3 = $scope.addressSplit[3] == ' ' ? '' : $scope.addressSplit[3];
            $scope.addUpdatePat.patientAddress[0].CityTown = $scope.addressSplit[5] == ' ' ? '' : $scope.addressSplit[5];
            $scope.addUpdatePat.patientAddress[0].State = $scope.addressSplit[6] == ' ' ? '' : $scope.addressSplit[6];
        }

        function latlong(val) {
            var geocoder = new google.maps.Geocoder();
            var address = val;

            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $scope.addUpdatePat.patientAddress[0].Latitude = results[0].geometry.location.lat().toString();
                    $scope.addUpdatePat.patientAddress[0].Longitude = results[0].geometry.location.lng().toString();
                    // $scope.refreshMap()
                    // $scope.$apply()
                }
            });
        }

        // Validation
        $scope.validator = {
            minAge: undefined,
            speciality: [],
            status: false
        }

        function ccgdetailssuccess(data) {
            $scope.activeCCG = angular.copy(data[0]);
            $scope.activeccgContract = $scope.activeCCG.Contractdetails[$scope.activeCCG.Contractdetails.length - 1];
            $scope.validator.minAge = $scope.activeccgContract.minimunageforref;
            $scope.validator.speciality = $scope.activeccgContract.Contractspeciality;
            $scope.validator.status = true;
            validateVal();
        }

        function validateVal() {
            if ($scope.validator.status == true) {
                if ($scope.addUpdatePat.patientDetails.Age != undefined && $scope.addUpdatePat.patientDetails.Age < $scope.validator.minAge) {
                    $scope.agespeciality.age = true;
                    alertify.error('Age below required range');
                } else {
                    $scope.agespeciality.age = false;
                }
                var specialityInCCG = {};
                if ($scope.validator.speciality != undefined && $scope.specialityCollection.selected.length > 0) {
                    // var i = true;
                    angular.forEach($scope.specialityCollection.selected, function (i) {
                        specialityInCCG[i.specialitydesc] = ($scope.validator.speciality.filter(data => data.Speciality == i.specialityID).length == 0) ? true : false;
                    });
                    for (var key in specialityInCCG) {
                        if (specialityInCCG[key] == true) {
                            alertify.error(key + ' - Speciality is not in CCG');
                        }
                    }
                    var val = Object.values(specialityInCCG);
                    for (var i = 0; i < val.length; i++) {
                        if (val[i] == true) {
                            $scope.agespeciality.speciality = true;
                            break;
                        } else {
                            $scope.agespeciality.speciality = false;
                        }
                    }
                    // console.log(specialityInCCG);
                    // if (specialityInCCG.length == 0) {
                    //     $scope.agespeciality.speciality = true;
                    //     alertify.error('Speciality is not in CCG');
                    // } else {
                    //     $scope.agespeciality.speciality = false;
                    // }
                }
            }
            // console.log($scope.validator);
        }

        $scope.openPopup = function () {

            $('#myModal').modal('show');
            $scope.tabno = 1;
            $scope.photoTaken = false;
            $scope.myImage = '';
            document.getElementById("fileInput").value = "";
            $scope.openset = false;
        }
        $scope.clearImg = function () {
            $scope.myImage = '';
            document.getElementById("fileInput").value = "";
            $scope.browseOpenSet = true;
        }
        var _video = null,
            patData = null;

        $scope.patOpts = { x: 0, y: 0, w: 25, h: 25 };

        $scope.channel = {};

        $scope.webcamError = false;
        $scope.onError = function (err) {
            $scope.$apply(
                function () {
                    $scope.webcamError = err;
                }
            );
        };

        $scope.onSuccess = function () {

            _video = $scope.channel.video;
            $scope.$apply(function () {
                $scope.patOpts.w = _video.width;
                $scope.patOpts.h = _video.height;

            });
        };


        $scope.onStream = function (stream) {

        };
        $scope.openset = false;
        $scope.makeSnapshot = function () {
            $scope.openset = true;
            $scope.photoTaken = true;

            $scope.selectingTxtBox(true);
            if (_video) {
                var patCanvas = document.querySelector('#snapshot');
                if (!patCanvas) return;

                patCanvas.width = _video.width;
                patCanvas.height = _video.height;
                var ctxPat = patCanvas.getContext('2d');

                var idata = getVideoData($scope.patOpts.x, $scope.patOpts.y, $scope.patOpts.w, $scope.patOpts.h);
                ctxPat.putImageData(idata, 0, 0);

                sendSnapshotToServer(patCanvas.toDataURL());

                patData = idata;
            }
        };

        $scope.closeSet = function () {
            $scope.snapshotData = '//:0';
            $scope.openset = false;
        }

        $scope.browseOpenSet = true;

        $scope.brwseOpen = function () {
            $scope.browseOpenSet = false;
        }


        $scope.downloadSnapshot = function downloadSnapshot(dataURL) {
            window.location.href = dataURL;
        };

        var getVideoData = function getVideoData(x, y, w, h) {
            var hiddenCanvas = document.createElement('canvas');
            hiddenCanvas.width = _video.width;
            hiddenCanvas.height = _video.height;
            var ctx = hiddenCanvas.getContext('2d');
            ctx.drawImage(_video, 0, 0, _video.width, _video.height);
            return ctx.getImageData(x, y, w, h);
        };


        var sendSnapshotToServer = function sendSnapshotToServer(imgBase64) {
            $scope.snapshotData = imgBase64;
        };
        $scope.snap = function (val) {
            $scope.base64Data = val.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
            $scope.patientinfo.profileImages = $scope.base64Data;
            $scope.showCroppedImage = $scope.myCroppedImage;
        }

        //Upload Profile Image
        $scope.onFileSelectConsent = function ($files) {
            $scope.tabno = 2;
            var type = $files[0].type;
            Upload.base64DataUrl($files).then(function (urls) {
                $scope.snapshotData = urls.toString();
            });
        }
        $scope.myImage = '';
        $scope.myCroppedImage = '';

        var handleFileSelect = function (evt) {
            //$scope.selectingTxtBox(false);
            var file = evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.myImage = evt.target.result;
                });
            };
            reader.readAsDataURL(file);
            //onFileSelectConsent(file);
        };

        $scope.upload = function () {
            $scope.selectingTxtBox(false);

        }


        angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);

        $scope.selectingTxtBox = function (val) {
            $scope.blnCamera = val;

        }

        // Upload doc
        // $scope.onUpload = function (data) {
        //     c7AddUpdatePatient.uploadPatientDetails(params, uploadSuccess, uploadError);
        // }


        var isAdvancedUpload = function () {
            var div = document.createElement('div');
            return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
        }();
        var $form = $('.upload.box');

        if (isAdvancedUpload) {
            var droppedFiles = false;
            var $input = $form.find('#UploadDocument'),
                $label = $form.find('label'),
                showFiles = function (files) {
                    $label.text(files.length > 1 ? ($input.attr('data-multiple-caption') || '').replace('{count}', files.length) : files[0].name);
                };


            $form.on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
                e.preventDefault();
                e.stopPropagation();
            })
                .on('dragover dragenter', function () {
                    $form.addClass('is-dragover');
                })
                .on('dragleave dragend drop', function () {
                    $form.removeClass('is-dragover');
                })
                .on('drop', function (e) {
                    droppedFiles = e.originalEvent.dataTransfer.files;
                    uploadFile(droppedFiles);
                });
            $input.on('change', function (e) {
                droppedFiles = e.target.files;
                uploadFile(droppedFiles);
            });

            uploadFile = function (data) {
                var file = data[0];
                // var preview = document.getElementById('UploadDocument');
                // var file = preview.files[0];
                if ($form.hasClass('is-uploading')) return false;
                $form.addClass('is-uploading').removeClass('is-error');
                if (file == undefined) {
                    return;
                }
                var formData = new FormData();
                $scope.fileurl = file.name;

                formData.append("fileUpload", file);
                formData.append("typeDoc", "EnquiryDocument");
                formData.append("patientID", "qwqwe");
                var ext = file.name.split('.');
                formData.append("fileExtn", ext[ext.length - 1].toLowerCase());
                $scope.fileType = ext[ext.length - 1];
                // if (ext[ext.length - 1].toLowerCase() == 'jpg' || ext[ext.length - 1].toLowerCase() == 'jpeg' || ext[ext.length - 1].toLowerCase() == 'png') {
                //     formData.append("timestamp", moment().valueOf());
                // }
                formData.append("timestamp", moment().valueOf());
                formData.append("fileName", file.name.toLowerCase());
                // console.log(file);
                c7AddUpdatePatient.uploadPatientDetails(formData, uploadSuccess, uploadError);

                document.getElementById('UploadDocument').value = '';

            }

            function checkDateFormat(date) {
                return moment(date, ['DD-MM-YYYY', 'DD/MM/YYYY', 'YYYY-MM-DD', 'YYYY/MM/DD']).format('DD-MM-YYYY');
            }

            // function uploadSuccess(data) {
            //     // console.log(data);
            //     // $scope.surname = null;
            //     // $scope.uploadResponseLink = null;
            //     $form.removeClass('is-uploading');
            //     if (data == null) {
            //         $form.addClass('is-error');
            //         alertify.error('Please choose pdf, jpeg, png, doc, docx');
            //         return false;
            //     }
            //     if (data.patient && data.patient.nhsNumber != '' && data.patient.foreName != '' && data.patient.surName != '') {
            //         $form.addClass('is-success');

            //         // var addr = data.patient.address;

            //         $scope.patientnumber = [{
            //             FullNumber: data.patient.telephoneWork.trim(),
            //             PhNumber: parseInt((data.patient.telephoneWork).replace(' ', '')),
            //             PhType: 'D',
            //             PrimaryIND: 'Y',
            //             PhDescription: ''
            //         }, {
            //             FullNumber: data.patient.telephoneHome.trim(),
            //             PhNumber: parseInt((data.patient.telephoneHome).replace(' ', '')),
            //             PhType: 'E',
            //             PrimaryIND: 'Y',
            //             PhDescription: ''
            //         }, {
            //             FullNumber: data.patient.telephoneMobile.trim(),
            //             PhNumber: parseInt((data.patient.telephoneMobile).replace(' ', '')),
            //             PhType: 'M',
            //             PrimaryIND: 'Y',
            //             PhDescription: ''
            //         }];

            //         $scope.addUpdatePat = {
            //             USRType: 'ADMIN',
            //             USRId: doctorinfo.doctorid,
            //             patientDetails: {
            //                 FirstName: data.patient.foreName,
            //                 DOB: checkDateFormat(data.patient.dob),
            //                 FullName: '',
            //                 LastName: data.patient.surName,
            //                 Title: '',
            //                 Gender: data.patient.gender.substring(0, 1).toUpperCase(),
            //                 Age: moment().diff(moment(checkDateFormat(data.patient.dob), 'DD-MM-YYYY'), 'years'),
            //                 TermsAccepted: 'Y',
            //                 MobileID: $scope.patientnumber[2].PhNumber,
            //                 EnquiryDetails: {
            //                     referalcreated: checkDateFormat(data.referralDate.trim()),
            //                     referalreceived: '',
            //                     referalprocessed: moment().format('DD-MM-YYYY'),
            //                     clinicalnotes: data.complaint,
            //                     clinicaldetails: data.diagnosis,
            //                     checkboxdetails: {
            //                         isEmail: false,
            //                         isLetter: false,
            //                         isVoicemail: false,
            //                         isSms: false,
            //                         isMobilityissues: false,
            //                         isCommunicationissues: false
            //                     },
            //                     manageid: undefined,
            //                     issueReason: '',
            //                     regionid: undefined,
            //                     specialitydetails: [{
            //                         speciality: '',
            //                         specialityid: '',
            //                         activeInd: 'Y',
            //                         subSpecialities: {}
            //                     }],
            //                     appointmenttypeid: '',
            //                     enquirysourceid: '',
            //                     enquiryfile: {
            //                         enquiry_real: data.patient.nhsNumber + '.pdf',
            //                         additional_real: ''
            //                     },
            //                     enquirystatusid: 'TRIAGE',
            //                     notes: '',
            //                     crtusr: doctorinfo.doctorid,
            //                     crtusrtype: 'ADMIN',
            //                     updtusr: doctorinfo.doctorid,
            //                     updtusrtype: 'ADMIN'
            //                 },
            //                 PatientOtherDetails: {
            //                     AadhaarID: data.patient.nhsNumber
            //                 }
            //             },
            //             patientEmail: [{
            //                 EmailID: data.patient.eMail.trim(),
            //                 EmailIDType: 'P',
            //                 PrimaryIND: 'Y'
            //             }],
            //             patientAddress: [{
            //                 Postcode: data.patient.address.address6.toUpperCase().trim(),
            //                 Address1: data.patient.address.address1.trim(),
            //                 Address2: data.patient.address.address2.trim(),
            //                 Address3: data.patient.address.address3.trim(),
            //                 Address4: data.patient.address.address4.trim(),
            //                 CityTown: data.patient.address.address5.trim(),
            //                 State: data.patient.address.address7,
            //                 Street: '',
            //                 PinCode: 0,
            //                 Country: 'UK',
            //                 PrimaryIND: 'Y',
            //                 AddressType: 'H',
            //                 Latitude: '',
            //                 LandMark: '',
            //                 DistrictRegion: '',
            //                 Longitude: '',
            //                 Location: ''
            //             }],
            //             patientPhone: [],
            //             patientReferral: {
            //                 ccgid: undefined,
            //                 ccgname: '',
            //                 practiceid: undefined,
            //                 practicename: '',
            //                 doctorname: '',
            //                 doctorid: undefined
            //             },
            //             // imageurl: $scope.uploadResponseLink,
            //             // filetype: $scope.fileType
            //         }
            //         if ($scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile.enquiry_real != '') {
            //             $scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile.enquiry_real = $scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile.enquiry_real.replace(/ /g, '');
            //         }

            //     } else {
            //         if (data.url != '') {
            //             $form.addClass('is-success');
            //             // $scope.uploadResponseLink = data.url;
            //             $scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile.additional_real = data.url;
            //             $scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile.enquiry_real = '';
            //         } else {
            //             $form.addClass('is-error');
            //             alertify.error('No url received');
            //         }
            //     }
            //     if (data.patient && data.patient.address.address6.toUpperCase().trim() !== '') {
            //         c7PostCodeServices.listpostcodeinfo(data.patient.address.address6.toUpperCase().trim(), docpostcodeSuccess, docpostcodeError);

            //     }
            //     // $scope.calculateAge($scope.addUpdatePat.patientDetails.DOB);
            // }

            function docpostcodeSuccess(data) {
                $scope.addUpdatePat.patientAddress[0].Latitude = data.latitude.toString();
                $scope.addUpdatePat.patientAddress[0].Longitude = data.longitude.toString();
            }

            function docpostcodeError(data) {
                alertify.error('Patient address error \n ' + data);
            }

            function uploadError(data) {
                alertify.error(data);
            }
        }



        $scope.printmail = function (data) {
            var html = data;
            var WindowObject = window.open("", "PrintWindow",
                "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(html.replace('Simple Transactional Email', 'Exclusion Email'));
            WindowObject.document.close();
            setTimeout(function () {
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }, 2000);
        }


        $scope.hover = function (fruit) {
            return fruit.showHover = !fruit.showHover;
        };

        $scope.viewCase = function (val) {
            $scope.activePatientId = val.patientid;
            $scope.searchshow = false;
            $scope.caseViewCollection = [];
            $scope.selectUserInfo = val;
            var params = {
                patientid: val.patientid,
                pagenumber: 0,
                pagesize: 1000
            }

            c7SearchServices.listPatientAllEnquiry(params, OnlistPatientAllEnquirySuccess, OnGetError);

            function OnlistPatientAllEnquirySuccess(data) {
                $scope.caseViewCollection = data;
                angular.forEach($scope.caseViewCollection, function (i, ind) {
                    if (ind == 0) {
                        i.isSelected = true;
                    } else {
                        i.isSelected = false;
                    }
                })
                $scope.detailedView($scope.caseViewCollection[0], 0)
            }

        }

        $scope.sublistSussessCount = 0;
        $scope.detailedView = function (data, index) {
            console.log(data, index)
            $scope.activeEnquiryId = data.patientenquiryid;
            $scope.activeLeftIndex = data.index;
            $scope.enquiriesCollection = [];
            angular.forEach($scope.caseViewCollection, function (j, ind) {
                if (index == ind) {
                    j.isSelected = true;
                } else {
                    j.isSelected = false;
                }
            })
            $scope.selectedCollection = data;
            var subParam = {
                patientenquiryid: data.patientenquiryid,
            }
            c7EnquiriesServices.listEnquiryStatus(subParam, listEnquiryStatusSuccess, OnGetError);
            c7EnquiriesServices.listAppointmentStatus(data.patientenquiryid + '/' + docid, subappointmentlistSuccess, OnGetError);

            $scope.enquiriesCollection = [];

            function listEnquiryStatusSuccess(data) {

                angular.forEach(data, function (val) {
                    val.dateNew = moment.utc(val.date, 'YYYY-MM-DD HH:mm:ss').local().format('DD-MM-YYYY HH:mm:ss');
                    val.timestamp = moment(val.date, 'YYYY-MM-DD HH:mm:ss').valueOf();
                    if (val.enquirystatusid == 'appemail' ||
                        val.enquirystatusid == 'appointEmail' ||
                        val.enquirystatusid == 'exclusion' ||
                        val.enquirystatusid == 'exclusionEmail' ||
                        val.enquirystatusid == 'declinedEmail' ||
                        val.enquirystatusid == 'patientDeclinedEmail' ||
                        val.enquirystatusid == 'dnaEmail' ||
                        val.enquirystatusid == 'contactusEmail' ||
                        val.enquirystatusid == 'nocontactEmail' ||
                        val.enquirystatusid == 'urgentnocontactEmail') {
                        val.emailobj = {
                            patientenquiryid: $scope.activeEnquiryId,
                            appointmentid: 0,
                            sendemail: true,
                            sendletter: true
                        }
                    }
                })
                $scope.enquiriesCollection = $scope.enquiriesCollection.concat(data);
                $scope.sublistSussessCount += 1;
                if ($scope.sublistSussessCount == 2) {
                    $scope.enquiriesCollection = $scope.enquiriesCollection.sort(function (a, b) {
                        return b.timestamp - a.timestamp;
                    });
                    $('#loaderDiv').hide();
                    $scope.sublistSussessCount = 0;
                }
                // angular.forEach(data, function (val) {
                //     val.date = moment.utc(val.date, 'YYYY-MM-DD HH:mm:ss').local().format('DD-MM-YYYY HH:mm:ss');
                //     val.timestamp = moment(val.date, 'YYYY-MM-DD HH:mm:ss').valueOf();
                // })
                // $scope.enquiriesCollection = $scope.enquiriesCollection.concat(data);
            }

            function subappointmentlistSuccess(data) {

                angular.forEach(data, function (val) {
                    val.dateNew = moment.utc(val.date, 'YYYY-MM-DD HH:mm:ss').local().format('DD-MM-YYYY HH:mm:ss');
                    val.timestamp = moment(val.date, 'YYYY-MM-DD HH:mm:ss').valueOf();
                    if (val.enquirystatusid == 'appointEmail' || val.enquirystatusid == 'exclusionEmail' || val.enquirystatusid == 'declinedEmail' || val.enquirystatusid == 'dnaEmail' || val.enquirystatusid == 'contactusEmail' || val.enquirystatusid == 'nocontactEmail' || val.enquirystatusid == 'urgentnocontactEmail') {
                        val.emailobj = {
                            patientenquiryid: $scope.activeEnquiryId,
                            appointmentid: 0,
                            sendemail: true,
                            sendletter: true
                        }
                    }
                })
                $scope.enquiriesCollection = $scope.enquiriesCollection.concat(data);
                $scope.sublistSussessCount += 1;
                if ($scope.sublistSussessCount == 2) {
                    $scope.enquiriesCollection = $scope.enquiriesCollection.sort(function (a, b) {
                        return b.timestamp - a.timestamp;
                    });
                    $scope.sublistSussessCount = 0;
                }
                // angular.forEach(data, function (val) {
                //     val.date = moment.utc(val.date, 'YYYY-MM-DD HH:mm:ss').local().format('DD-MM-YYYY HH:mm:ss');
                //     val.timestamp = moment(val.date, 'YYYY-MM-DD HH:mm:ss').valueOf();
                // })
                // $scope.enquiriesCollection = $scope.enquiriesCollection.concat(data);
            }

        }

        $scope.doctorDetails = function (id, val) {
            $scope.modalId = id;
            $scope.doctorInfo = [];
            referraldoctor.getdoctorDetails(val.referdoctorid, doctorDetailsSuccess, OnGetError);

            function doctorDetailsSuccess(data) {
                $scope.doctorInfo = data;

                if ($scope.modalId != '') {
                    $($scope.modalId).modal('show');
                    $scope.modalId = '';
                }
            }
        }



        function OnGetError(data) {

        }
        $scope.modalId = '';
    

        $scope.getPatientDetails = function (id) {
            $scope.modalId = id;
            var userParam = {
                PatientID: $scope.selectUserInfo.patientid,
                PatientEnquiryID: $scope.selectedCollection.patientenquiryid,
                USRId: doctorinfo.doctorid,
                USRType: "ADMIN",
                hospitalInfo: {
                    HospitalID: parentHospitalId,
                    HospitalCD: hospitalCode
                }
            }
            if ($scope.selectUserInfo.patientid != undefined || $scope.lookupStatus) {
                if ($scope.selectedCollection.patientenquiryid != [] && $scope.selectedCollection.patientenquiryid != undefined && $scope.selectedCollection.patientenquiryid.length != 0) {

                 }
            }
            c7AddUpdatePatient.getPatientDetails(userParam, getpatientsuccess, OnGetError);

            function getpatientsuccess(data) {
                $scope.patientDetails = data;
                //console.log(patientDe)

                if ($scope.modalId != '') {
                    $($scope.modalId).modal('show');
                    $scope.modalId = '';


                }
                if (data.Patient.length == 0) {
                    $scope.declareVariable();
                    alertify.error('No patient Found');
                } else {
                    var uData = angular.copy(data);
                    $scope.patientData = angular.copy(data);
                    console.log($scope.patientData);
                    $scope.statusSuccess.ccg += 1;
                    $scope.statusSuccess.speciality += 1;

                    $scope.patientnumber = [{
                        PhNumber: undefined,
                        PhType: 'D',
                        PrimaryIND: 'Y',
                        FullNumber: '',
                        PhDescription: ''
                    }, {
                        PhNumber: undefined,
                        PhType: 'E',
                        PrimaryIND: 'Y',
                        FullNumber: '',
                        PhDescription: ''
                    }, {
                        PhNumber: undefined,
                        PhType: 'M',
                        PrimaryIND: 'Y',
                        FullNumber: '',
                        PhDescription: ''
                    }];

                    for (var i = 0; i < uData.PatientPhone.length; i++) {
                        var index = $scope.patientnumber.findIndex(x => x.PhType == uData.PatientPhone[i].PhType)
                        $scope.patientnumber[index].PhNumber = uData.PatientPhone[i].PhNumber;
                        $scope.patientnumber[index].FullNumber = uData.PatientPhone[i].FullNumber;
                        $scope.patientnumber[index].PhDescription = uData.PatientPhone[i].PhDescription;
                    }
                    for (var i = 0; i < $scope.patientData.PatientPhone.length; i++) {
                        var index = $scope.patientnumber.findIndex(x => x.PhType == $scope.patientData.PatientPhone[i].PhType)
                        $scope.patientnumber[index].PhNumber = $scope.patientData.PatientPhone[i].PhNumber;
                        $scope.patientnumber[index].FullNumber = $scope.patientData.PatientPhone[i].FullNumber;
                        $scope.patientnumber[index].PhDescription = $scope.patientData.PatientPhone[i].PhDescription;
                    }

                    $scope.email = [];
                    if (uData.PatientEmail.length > 0) {
                        $scope.email = [{
                            EmailID: uData.PatientEmail[0].EmailID,
                            PrimaryIND: uData.PatientEmail[0].PrimaryIND,
                            EmailIDType: uData.PatientEmail[0].EmailIDType,
                        }]
                    } else {
                        $scope.email = [{
                            EmailID: '',
                            PrimaryIND: 'Y',
                            EmailIDType: 'P'
                        }]
                    }
                    $scope.addUpdatePat = {
                        USRType: "ADMIN",
                        USRId: doctorinfo.doctorid,
                        patientDetails: {
                            MobileID: $scope.patientnumber[2].PhNumber,
                            Title: uData.Patient[0].Title,
                            FirstName: uData.Patient[0].FirstName,
                            Age: uData.Patient[0].CurrentAge.year,
                            LastName: uData.Patient[0].LastName,
                            // LastName: uData.Patient[0].FullName.replace(uData.Patient[0].FirstName, ''),
                            DOB: moment(uData.Patient[0].DOB).format('DD-MM-YYYY'),
                            FullName: uData.Patient[0].FullName,
                            PatientID: uData.Patient[0].PatientID,
                            Gender: uData.Patient[0].Gender,
                            TermsAccepted: "Y",
                            FamilyHeadIndicator: "Y",
                            EnquiryDetails: {
                                referalcreated: moment(uData.EnquiryDetails.referalcreated).format('DD-MM-YYYY'),
                                referalreceived: moment(uData.EnquiryDetails.referalreceived).format('DD-MM-YYYY'),
                                referalprocessed: moment(uData.EnquiryDetails.referalprocessed).format('DD-MM-YYYY'),
                                clinicalnotes: uData.EnquiryDetails.clinicalnotes,
                                clinicaldetails: uData.EnquiryDetails.clinicaldetails,
                                specialitydetails: uData.EnquiryDetails.specialitydetails,
                                appointmenttypeid: uData.EnquiryDetails.appointmenttypeid,
                                enquirysourceid: uData.EnquiryDetails.enquirysourceid,
                                enquiryfile: {
                                    enquiry_real: (uData.EnquiryDetails.enquiryfileurl != undefined && uData.EnquiryDetails.enquiryfileurl.enquiry_doc != undefined) ? uData.EnquiryDetails.enquiryfileurl.enquiry_doc : '',
                                    additional_real: (uData.EnquiryDetails.enquiryfileurl != undefined && uData.EnquiryDetails.enquiryfileurl.additional_doc != undefined) ? uData.EnquiryDetails.enquiryfileurl.additional_doc : ''
                                },
                                patientenquiryid: datacookieFactory.getItem('editPatientEnquiryId'),
                                regionid: uData.EnquiryDetails.regionid,
                                checkboxdetails: {
                                    isEmail: (uData.EnquiryDetails.checkboxdetails.isEmail == undefined) ? false : uData.EnquiryDetails.checkboxdetails.isEmail,
                                    isLetter: (uData.EnquiryDetails.checkboxdetails.isLetter == undefined) ? false : uData.EnquiryDetails.checkboxdetails.isLetter,
                                    isVoicemail: (uData.EnquiryDetails.checkboxdetails.isVoicemail == undefined) ? false : uData.EnquiryDetails.checkboxdetails.isVoicemail,
                                    isSms: (uData.EnquiryDetails.checkboxdetails.isSms == undefined) ? false : uData.EnquiryDetails.checkboxdetails.isSms,
                                    isMobilityissues: (uData.EnquiryDetails.checkboxdetails.isMobilityissues == undefined) ? false : uData.EnquiryDetails.checkboxdetails.isMobilityissues,
                                    isCommunicationissues: (uData.EnquiryDetails.checkboxdetails.isCommunicationissues == undefined) ? false : uData.EnquiryDetails.checkboxdetails.isCommunicationissues
                                },
                                issueReason: uData.EnquiryDetails.issueReason,
                                manageid: (uData.EnquiryDetails.manageid == undefined) ? undefined : uData.EnquiryDetails.manageid.toString(),
                                notes: (uData.EnquiryDetails.notes == undefined || uData.EnquiryDetails.notes.trim() == ':') ? '' : uData.EnquiryDetails.notes.trim(),
                                enquirystatusid: uData.EnquiryDetails.enquirystatusid,
                                crtusr: doctorinfo.doctorid,
                                crtusrtype: "ADMIN",
                                updtusr: doctorinfo.doctorid,
                                updtusrtype: "ADMIN"
                            },
                            PatientOtherDetails: {
                                AadhaarID: uData.Patient[0].PatientOtherDetails[0].AadhaarID
                            }
                        },
                        patientEmail: $scope.email,
                        patientAddress: [{
                            PinCode: 0,
                            Country: 'UK',
                            PrimaryIND: 'Y',
                            AddressType: 'H',
                            LandMark: '',
                            DistrictRegion: '',
                            Location: ''
                        }],
                        patientPhone: [],
                        patientReferral: {
                            ccgid: uData.patientReferral.ccgid,
                            ccgname: uData.patientReferral.ccgname,
                            practiceid: uData.patientReferral.practiceid,
                            practicename: uData.patientReferral.practicename,
                            doctorname: uData.patientReferral.doctorname,
                            doctorid: uData.patientReferral.doctorid
                        },
                        // imageurl: $scope.uploadResponseLink,
                        // filetype: $scope.fileType
                    }
                    if (uData.EnquiryDetails.enquiryfileurl == undefined) {
                        $scope.fileurl = '';
                    } else {
                        $scope.fileurl = (uData.EnquiryDetails.enquiryfileurl.enquiry_doc == undefined) ? uData.EnquiryDetails.enquiryfileurl.additional_doc : uData.EnquiryDetails.enquiryfileurl.enquiry_doc;
                    }
                    if (uData.PatientAddress.length > 0) {
                        $scope.addUpdatePat.patientAddress[0].Address1 = uData.PatientAddress[0].Address1,
                            $scope.addUpdatePat.patientAddress[0].Address2 = uData.PatientAddress[0].Address2,
                            $scope.addUpdatePat.patientAddress[0].Address3 = uData.PatientAddress[0].Address3,
                            $scope.addUpdatePat.patientAddress[0].Address4 = uData.PatientAddress[0].Address4,
                            $scope.addUpdatePat.patientAddress[0].Latitude = uData.PatientAddress[0].Latitude,
                            $scope.addUpdatePat.patientAddress[0].Postcode = uData.PatientAddress[0].Postcode.toUpperCase(),
                            $scope.addUpdatePat.patientAddress[0].State = uData.PatientAddress[0].State,
                            $scope.addUpdatePat.patientAddress[0].Longitude = uData.PatientAddress[0].Longitude,
                            $scope.addUpdatePat.patientAddress[0].CityTown = uData.PatientAddress[0].CityTown
                    }
                    if ($scope.addUpdatePat.patientDetails.EnquiryDetails.manageid != undefined && $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid > 0 && $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid < 8) {
                        $scope.issuereason = true;
                    } else {
                        $scope.issuereason = false;
                    }
                    // $scope.surname = uData.Patient[0].FullName.replace(uData.Patient[0].FirstName, '');

                    if ($scope.statusSuccess.ccg == 2) {
                        loadreferral();
                    }
                    if ($scope.statusSuccess.speciality == 2) {
                        loadspeciality();
                        validateVal();
                    }
                }




            }
        }

        $scope.UpdateEnquiry = function () {
            // if ($scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile.enquiry_real == '' && $scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile.additional_real == '') {
            //     alertify.error('Please choose a document');
            //     return false;
            // }
            if ($scope.issuereason && $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid == '0') {
                alertify.error('Choose issue reason or uncheck issues');
                return false;
            }

            if ($scope.addUpdatePat.patientReferral.ccgname == '' || $scope.addUpdatePat.patientReferral.ccgname == null || $scope.addUpdatePat.patientReferral.practicename == '' ||
                $scope.addUpdatePat.patientReferral.practicename == '' || $scope.addUpdatePat.patientReferral.doctorname == '' || $scope.addUpdatePat.patientReferral.doctorname == '') {
                alertify.error('Please give valid Referrer Details');
                return false;
            }


            if ($scope.selectUserInfo.patientid != undefined || $scope.lookupStatus) {
                if ($scope.selectedCollection.patientenquiryid != [] && $scope.selectedCollection.patientenquiryid != undefined && $scope.selectedCollection.patientenquiryid.length != 0) {

                    newEnquiryAddStatus();
                }
                fillspeciality();
                $scope.addUpdatePat.patientDetails.EnquiryDetails.specialitydetails = $scope.newSpecialityCollection;
                $scope.lookupStatus = false;
            }
            $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid = parseInt($scope.addUpdatePat.patientDetails.EnquiryDetails.manageid);
            if ($scope.issuereason && $scope.addUpdatePat.patientDetails.EnquiryDetails.enquirystatusid != 'BOOKED') {
                $scope.addUpdatePat.patientDetails.EnquiryDetails.enquirystatusid = 'ISSUES';
                var issueDesc = $scope.issueCollection.filter(data => data.manageid == $scope.addUpdatePat.patientDetails.EnquiryDetails.manageid)[0].managedesc;
                $scope.addUpdatePat.patientDetails.EnquiryDetails.notes = ($scope.addUpdatePat.patientDetails.EnquiryDetails.notes == undefined) ? '' : $scope.addUpdatePat.patientDetails.EnquiryDetails.notes;
                if ($scope.addUpdatePat.patientDetails.EnquiryDetails.notes.trim() != '') {
                    if (issueDesc == 'others') {
                        if (!$scope.addUpdatePat.patientDetails.EnquiryDetails.notes.includes($scope.otherIssues)) {
                            $scope.addUpdatePat.patientDetails.EnquiryDetails.notes = $scope.otherIssues + ': ' + $scope.addUpdatePat.patientDetails.EnquiryDetails.notes;
                        }
                    } else {
                        if (!$scope.addUpdatePat.patientDetails.EnquiryDetails.notes.includes(issueDesc)) {
                            $scope.addUpdatePat.patientDetails.EnquiryDetails.notes = issueDesc + ': ' + $scope.addUpdatePat.patientDetails.EnquiryDetails.notes;
                        }
                    }
                }
            }
            if (($scope.agespeciality.age || $scope.agespeciality.speciality) && $scope.addUpdatePat.patientDetails.EnquiryDetails.enquirystatusid != 'BOOKED') {
                $scope.addUpdatePat.patientDetails.EnquiryDetails.enquirystatusid = 'ISSUES';
            }
            var dcoReff = datacookieFactory.getItem('doctorreferral');
            (dcoReff.length == 0) ? dcoReff = undefined : dcoReff = dcoReff;

            if ($scope.addUpdatePat.patientDetails.EnquiryDetails.enquirystatusid == 'DOCTORREFERRAL' && dcoReff != undefined) {
                $scope.addUpdatePat.patientDetails.EnquiryDetails.enquirystatusid = 'TRIAGE';
                datacookieFactory.remove('doctorreferral');
            }
            $scope.newSpecialityCollection = [];
            angular.forEach($scope.specialityCollection.selected, function (i) {
                var params = {
                    speciality: i.specialitydesc,
                    specialityid: i.specialityID,
                    activeInd: 'Y',
                    subSpecialities: {}
                }
                if (i.id != undefined) {
                    params.id = i.id;
                }
                angular.forEach(i.subspeciality, function (k, key) {
                    if ($scope.subSpecialityCollection.selected.findIndex(p => p.DoctorSpecialityID == k.DoctorSpecialityID) > -1) {
                        params.subSpecialities[key] = k.DoctorSpecialityID;
                    }
                })
                $scope.newSpecialityCollection.push(params);
            })

            // if ($scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile == undefined) {
            //     var urlcoll = {};
            //     if ($scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfileurl.enquiry_doc != undefined) {
            //         urlcoll.enquiry_real = $scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfileurl.enquiry_doc;
            //     } else {
            //         urlcoll.enquiry_real = '';
            //     }
            //     if ($scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfileurl.additional_doc != undefined) {
            //         urlcoll.additional_real = $scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfileurl.additional_doc;
            //     } else {
            //         urlcoll.additional_real = '';
            //     }
            //     $scope.addUpdatePat.patientDetails.EnquiryDetails.enquiryfile = urlcoll;
            //     delete $scope.addUpdatePat.patientDetails.EnquiryDetails['enquiryfileurl'];

            // }

            var uptdata = {

                referalcreated: $scope.convertDateymd($scope.addUpdatePat.patientDetails.EnquiryDetails.referalcreated),
                referalreceived: $scope.convertDateymd($scope.addUpdatePat.patientDetails.EnquiryDetails.referalreceived),
                referalprocessed: $scope.convertDateymd($scope.addUpdatePat.patientDetails.EnquiryDetails.referalprocessed),
                clinicalnotes: $scope.addUpdatePat.patientDetails.EnquiryDetails.clinicalnotes,
                clinicaldetails: $scope.addUpdatePat.patientDetails.EnquiryDetails.clinicaldetails,
                specialitydetails: $scope.newSpecialityCollection,
                patientReferral: {
                    ccgid: $scope.addUpdatePat.patientReferral.ccgid,
                    ccgname: $scope.addUpdatePat.patientReferral.ccgname,
                    practiceid: $scope.addUpdatePat.patientReferral.practiceid,
                    practicename: $scope.addUpdatePat.patientReferral.practicename,
                    doctorname: $scope.addUpdatePat.patientReferral.doctorname,
                    doctorid: $scope.addUpdatePat.patientReferral.doctorid
                },
                appointmenttypeid: $scope.addUpdatePat.patientDetails.EnquiryDetails.appointmenttypeid,
                enquirysourceid: $scope.addUpdatePat.patientDetails.EnquiryDetails.enquirysourceid,
                patientenquiryid: $scope.selectedCollection.patientenquiryid,
                regionid:    $scope.addUpdatePat.patientDetails.EnquiryDetails.regionid,
                patientid: $scope.selectUserInfo.patientid,
                crtusr: doctorinfo.doctorid,
                crtusrtype: 'ADMIN',
                updtusr: doctorinfo.doctorid,
                updtusrtype: 'ADMIN'

            }

            $scope.addUpdatePat.patientDetails.EnquiryDetails.crtusr = doctorinfo.doctorid;
            // uptdata.patientDetails.EnquiryDetails.referalcreated = $scope.convertDateymd(uptdata.patientDetails.EnquiryDetails.referalcreated);
            // uptdata.patientDetails.EnquiryDetails.referalreceived = $scope.convertDateymd(uptdata.patientDetails.EnquiryDetails.referalreceived);
            // uptdata.patientDetails.EnquiryDetails.referalprocessed = $scope.convertDateymd(uptdata.patientDetails.EnquiryDetails.EnquiryDetails.referalprocessed);

            // $scope.addUpdatePat.patientDetails.EnquiryDetails.referalcreated = $scope.convertDateymd($scope.addUpdatePat.patientDetails.EnquiryDetails.referalcreated);
            // $scope.addUpdatePat.patientDetails.EnquiryDetails.referalreceived = $scope.convertDateymd($scope.addUpdatePat.patientDetails.EnquiryDetails.referalreceived);
            // $scope.addUpdatePat.patientDetails.EnquiryDetails.referalprocessed = $scope.convertDateymd($scope.addUpdatePat.patientDetails.EnquiryDetails.referalprocessed);
            c7AddUpdatePatient.updatePatientEnquiry(uptdata, enquirysuccess, enquiryerror);
            $scope.cancel();

        }
        function enquirysuccess(data) {
            $scope.data1 = data;
            console.log(data1);
        }
        function enquiryerror(data) {

        }


        $scope.getsacnreport = function () {

        }

        $scope.sendemail = false;
        $scope.download = false;
        $scope.emailActoin = function (data, downloadcheck, email) {
            $scope.download = downloadcheck;
            $scope.sendemail = email;
            // data.patientenquiryid = eqid;
            if (data.enquirystatusid == 'appointEmail') {
                $scope.appointmentEmailTrue = true;
                $scope.emailName = 'Appointment Email';
                var submitData = {
                    patientenquiryid: data.patientenquiryid,
                    appointmentid: data.emailobj.appointmentid,
                    sendemail: email,
                    sendletter: downloadcheck,
                    appointmentdate: moment(data.appointmentdate).format('YYYY-MM-DD'),
                    appointmenttime: data.appointmenttime,
                    doctorid: data.doctorid,
                    appointmenthospitalid: data.appointmenthospitalid
                };
                c7EnquiriesServices.sendAppointmentEmail(submitData, emailactionSuccess, OnGetError);
            } else {
                $scope.appointmentEmailTrue = false;
            }
            if (data.enquirystatusid == 'declinedEmail') {
                $scope.emailName = 'Patient Declined Email';
                c7EnquiriesServices.sendPatientDeclinedEmail(data.emailobj, emailactionSuccess, OnGetError);
            }
            if (data.enquirystatusid == 'dnaEmail') {
                $scope.emailName = 'Patient DNA Email';
                c7EnquiriesServices.sendPatientDNAMail(data.emailobj, emailactionSuccess, OnGetError);
            }
            if (data.enquirystatusid == 'contactusEmail') {
                $scope.emailName = 'Contact Us Email';
                c7EnquiriesServices.sendContactUsEmail(data.emailobj, emailactionSuccess, OnGetError);
            }
            if (data.enquirystatusid == 'nocontactEmail') {
                $scope.emailName = 'No Contact To GP Email';
                c7EnquiriesServices.sendNoContactToGP(data.emailobj, emailactionSuccess, OnGetError);
            }
            if (data.enquirystatusid == 'urgentnocontactEmail') {
                $scope.emailName = 'Urgent No Contact To GP Email';
                data.emailobj['emailtype'] = 'URGENT';
                c7EnquiriesServices.sendNoContactToGP(data.emailobj, emailactionSuccess, OnGetError);
            }
            if (data.enquirystatusid == 'exclusionEmail') {
                $scope.emailName = 'Exclusion Email';
                c7EnquiriesServices.sendExclusionEmail(data.emailobj, emailactionSuccess, OnGetError);
            }
            if (data.enquirystatusid == 'patientDeclinedEmail') {
                $scope.emailName = 'Patient Withdrawal Email';
                data.emailobj.appointmentid = 0;
                c7EnquiriesServices.sendPatientDeclinedEmail(data.emailobj, emailactionSuccess, OnGetError);
            }
        }

        function emailactionSuccess(data) {
            if (data.includes('<!doctype html') && $scope.download) {
                $scope.printmail(data);
                $scope.download = false;
            } else if (!data.error && $scope.sendemail) {
                $scope.sendemail = false;
                alertify.success('Email Sent Successfully.');
            } else {
                alertify.error('Error in email sending.');
            }
        }

        $scope.printmail = function (data) {
            var html = data;

            var WindowObject = window.open("", "PrintWindow",
                "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(html.replace('Simple Transactional Email', $scope.emailName));
            WindowObject.document.close();
            setTimeout(function () {
                WindowObject.focus();
                if (!$scope.appointmentEmailTrue) {
                    WindowObject.print();
                    WindowObject.close();
                }
                $scope.appointmentEmailTrue = false;
            }, 2000)
            $scope.emailName = '';
        }
        $scope.sendpublishemail = function (data) {
            console.log(data);

            var params = {
                responseId: data.observationid,
                PracticeID: data.reportprcticeid,
            }
            c7Hl7ToMeshServices.sendAttachmentMail(params, publishSuccess, publishError);
        }

        function publishSuccess(data) {
            if (data == 'MESSAGE_ACCEPTED') {
                alertify.success('Published Successfully');
            } else if (data == 'Success') {
                alertify.success('Published Successfully & Email Sent');
            } else if (data == 'INVALID_MAILBOXID_AUTHENTICATION') {
                alertify.success('INVALID_MAILBOXID_AUTHENTICATION');
            }
            if (data.error) {
                publishError(data);
            }
        }

        function publishError(error) {
            if (error == 'INVALID_REFERENCE_ID') {
                alertify.error('Invalid Reference Id');
            } else if (error == 'REPORT_NOT_FOUND') {
                alertify.error('Report Not Found');
            } else if (error == 'MESSAGE_NOT_DELIVERY') {
                alertify.error('Message Not Delivered');
            } else if (error == 'INVALID_MAILBOXID_AUTHENTICATION') {
                alertify.error('Invalid MailBox Id');
            } else {
                alertify.error('Error Occurred');
            }
        }

        
    $('body').on('change', '#UploadDoc3', function (e) {
        var droppedFiles = e.target.files;
        uploadFile(droppedFiles);
    });

    uploadFile = function (data) {
        var file = data[0];

        if (file == undefined) {
            return;
        }
        var formData = new FormData();
        $scope.fileurl = file.name;

        formData.append("fileUpload", file);
        formData.append("typeDoc", "LettersDocument");
        formData.append("patientID", $scope.activePatientId);
        formData.append("patientenquiryID", $scope.activeEnquiryId);
        var ext = file.name.split('.');
        formData.append("fileExtn", ext[ext.length - 1].toLowerCase());
        $scope.fileType = ext[ext.length - 1];

        formData.append("timestamp", moment().valueOf());
        formData.append("fileName", file.name.toLowerCase());

        c7AddUpdatePatient.uploadLetterDocument(formData, uploadSuccess, uploadError);

        document.getElementById('UploadDoc3').value = '';

    }

    function uploadError(data) {
        alertify.error(data);
    }

    function uploadSuccess(data) {
        if (data.url) {
            alertify.success('Upload Success');
            uploadFileToEnquiry(data.url);
        } else {
            alertify.error('Upload Error');
        }
    }

         function uploadFileToEnquiry(urlString) {
            var uploadParam = {
                patientid: $scope.activePatientId,
                patientenquiryid: $scope.activeEnquiryId,
                usrid: doctorinfo.doctorid,
                statustypeid: "DocumentUpload",
                documenturl: urlString
            };

            c7EnquiriesServices.enquiryaddstatus(uploadParam, updateEnquiryStatus, uploadError);
        }
        function updateEnquiryStatus(data) {
            if (data != "Success") {
                alertify.error('Status update error');
            } else {
                $scope.detailedView($scope.caseViewCollection[0], $scope.activeLeftIndex);
            }
        }
        $scope.addnotes = function () {
            $scope.notesparam = {
                patientid: $scope.activePatientId,
                patientenquiryid: $scope.activeEnquiryId,
                usrid: doctorinfo.doctorid,
                statustypeid: "Notes",
                documenturl: undefined
            };
            console.log($scope.notesparam);
        }
        $scope.submitnotes = function () {
            $scope.notesparam['documenturl'] = $scope.Gnnotes;
            console.log($scope.notesparam);
            $('#notes').modal('hide');
            c7EnquiriesServices.enquiryaddstatus($scope.notesparam, updateEnquiryStatus, uploadError);
        }
    }
]); 