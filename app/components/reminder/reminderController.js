hCueDoctorWebControllers.controller('c7ReminderController', function ($scope, $rootScope, $http, alertify, $window, $location, hcueLoginStatusService, hcueDoctorLoginService, c7ReminderServices, c7EnquiriesServices, datacookieFactory) {
    // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }

    $scope.reminderList = [];

    $scope.getAllReminderList = function () {

        var param = {
            HospitalCode: hospitalCode,
            pagenumber: 1,
            pagesize: 10

        };

        c7ReminderServices.getReminderList(param, getRemiderListSuccess, getRemiderListError);
    }

    $scope.getAllReminderList();

    function getRemiderListSuccess(data) {
        if (data.length > 0) {
            $scope.reminderList = data;
        }
    }

    function getRemiderListError(data) {
        alertify.error('Error in remainder listing');
        console.log(data);
    }

    $scope.reminderData = null;
    $scope.setReminderData = function (itemData) {
        $scope.reminderData = itemData;
    }

    $scope.updateReminder = function () {

        $('#reminderconfirm').modal('hide');
        var param = {
            patientid: $scope.reminderData.patientid,
            reminderid: $scope.reminderData.reminderid,
            patientenquiryid: $scope.reminderData.patientenquiryid,
            reminderconfirmdate: moment(new Date).format('YYYY-MM-DD')
        };

        var enquiryParam = {
            patientid: $scope.reminderData.patientid,
            patientenquiryid: $scope.reminderData.patientenquiryid,
            usrid: doctorinfo.doctorid,
            statustypeid: "ReminderCompleted",
            notes: $scope.reminderData.remindernotes,
            reminderduedate: moment(new Date).format('YYYY-MM-DD')
        };

        c7ReminderServices.addReminder(param, updateReminderSuccess, updateReminderError);
        c7EnquiriesServices.enquiryaddstatus(enquiryParam, updateEnquiryStatus, enquiryError);

    }

    function updateReminderSuccess(data) {
        if (data == 'Success') {
            alertify.success('Success');
            $scope.getAllReminderList();

        } else {
            updateReminderError(data);
        }

    }

    function updateReminderError(data) {
        $scope.reminderData = null;
        console.log(data);
        alertify.error('Error in update remainder');
    }

    function updateEnquiryStatus(data) {
        if (data != "Success") {
            alertify.success('Status update error');
        } else {
            $scope.getAllReminderList();
            $scope.reminderData = null;
        }
    }

    function enquiryError(data) {
        alertify.error('Error in update status');
        console.log(data);
    }

});