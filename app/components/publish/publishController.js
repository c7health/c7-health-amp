hCueDoctorWebControllers.controller('c7PublishController', function ($scope, $rootScope, $http, alertify, $window, $location, hcueLoginStatusService, hcueDoctorLoginService, referralregion, datacookieFactory, c7Hl7ToMeshServices) {
    // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();

    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();

    if (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined) {
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }

    $scope.hl7MessageList = [];
    $scope.maxSize = 10;
    $scope.currentPage = 1;
    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function (pageno) {
        $scope.currentPage = pageno;
        var param = {
            pageNumber: $scope.currentPage,
            pageSize: 20,
            HospitalID: doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalID,
            Date: moment($scope.changedatepublish, 'DD-MM-YYYY').format('YYYY-MM-DD')
          };
          c7Hl7ToMeshServices.getHL7List(param, hl7ListSuccess, hl7ListError);
    

    }

    $scope.changedatepublish = moment().format('DD-MM-YYYY');
    $scope.getList = function () {
      var param = {
        pageNumber: $scope.currentPage,
        pageSize: 20,
        HospitalID: doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalID,
        Date: moment($scope.changedatepublish, 'DD-MM-YYYY').format('YYYY-MM-DD')
      };
      c7Hl7ToMeshServices.getHL7List(param, hl7ListSuccess, hl7ListError);
    }  
    $scope.getList();


    function hl7ListSuccess(data){
        $scope.Count = data.Count;
        if(data.arrListObjHL7Message.length > 0){
            $scope.hl7MessageList = data.arrListObjHL7Message;
            $scope.totalItems = Math.ceil(data.Count / 20)*10;
        }
        
    }

    function hl7ListError(data){
        alertify.log("Error in connectivity");
    }

    $scope.triggerHL7Connection = function(id){
        if(id != null && id != ''){
            c7Hl7ToMeshServices.triggerHL7MessageConnection(id, triggerHL7ConnectionSuccess, triggerHL7ConnectionError)
        }

    }

    function triggerHL7ConnectionSuccess(data){
        if(data != null && data.status == 'failure'){
            triggerHL7ConnectionError(data);
        }
        else{
            $scope.getHl7MessageList();
        }
        
    }

    function triggerHL7ConnectionError(data){
        alertify.log("Error in connectivity");
    }

});