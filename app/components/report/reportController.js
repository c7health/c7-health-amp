hCueDoctorWebControllers.controller('c7ReportController', function($scope, $rootScope, $http, alertify, $window, $location, hcueLoginStatusService, hcueDoctorLoginService, referralregion, c7EnquiriesServices, c7AddUpdatePatient, datacookieFactory) {
     // Check login Params
    var userLoginDetails = hcueLoginStatusService.getUserLoginDetails();
   
    userLoginDetails = datacookieFactory.getItem('loginData');
    if (userLoginDetails.loggedinStatus === 'true') {
        $scope.setHeaderShouldShow(userLoginDetails.loggedinStatus);
    } else {
        $location.path('/login');
    }
    var doctorinfo = hcueDoctorLoginService.getDoctorInfo();
  
    if(doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo !== undefined && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID !== undefined){
        parentHospitalId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.ParentHospitalID;
        hospitalCode = doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode;
        newRoleId = doctorinfo.doctordata.doctorAddress[0].ExtDetails.RoleID;
    }

    function getRegionList() {
        var userParam = {
            "hospitalcd": (doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != null 
                && doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode != undefined) ? 
                doctorinfo.doctordata.doctorAddress[0].ExtDetails.HospitalInfo.HospitalCode : '' ,
            "usrid":doctorinfo.doctorid,
	        "usrtype":"ADMIN"
        }

        referralregion.listRegion(userParam, regionListSuccess, enquiryError);
    }

    function regionListSuccess(data){
        $scope.specialityCollection = data;
        var allOption = { 
                        "regionid" : 0,
                        "regionname": "All",
                        }
        $scope.specialityCollection.unshift(allOption);
    }

    function enquiryError(data){

    }
    getRegionList();

    $scope.exportTOExcel = function(){
        
                                    var canadaData = [
                                        {
                                        "name": "Bob",
                                        "state": "Ontario",
                                        "occupation": "Doctor"
                                        },
                                        {
                                        "name": "Sobers",
                                        "state": "PEI",
                                        "occupation": "Plumber"
                                        }
                                        ];
                                    var indiaData = [
                                        {
                                        "name": "Natasha",
                                        "state": "Maharashtra",
                                        "occupation": "Mechanic"
                                        },
                                        {
                                        "name": "Reese",
                                        "state": "Delhi state",
                                        "occupation": "Doctor"
                                        }
                                        ];
                                    var xmls = fetchXML([canadaData, indiaData]);
    
                                    var ctx = {
                                        created : (new Date()).getTime(),
                                        worksheets : xmls
                                    };
    
                                    var uri = 'data:application/vnd.ms-excel;base64,',
                                        tmplWorkbookXML = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' + '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' + '<Styles>' + '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' + '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' + '</Styles>' + '{worksheets}</Workbook>',
                                        tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
                                        tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
                                        base64 = function(s) {
                                        return window.btoa(unescape(encodeURIComponent(s)));
                                    },
                                        format = function(s, c) {
                                        return s.replace(/{(\w+)}/g, function(m, p) {
                                            return c[p];
                                        });
                                    };
                                    var workbookXML = format(tmplWorkbookXML, ctx);
                                    var link = document.createElement("A");
                                    link.href = uri + base64(workbookXML);
                                    link.download = "Data" + (new Date()).getTime() + '.xls';
                                    //'Workbook.xls';
                                    link.target = '_blank';
                                    document.body.appendChild(link);
                                    link.click();
                                    document.body.removeChild(link);
    }

    function fetchXML(stores){
        var wrkbookXML = '';
        for(i=0; i<stores.length; i++){
            var worksheetName = 'Data';
            i == 0 ? worksheetName = 'Canada' : worksheetName = 'India';
            wrkbookXML += '<Worksheet ss:Name="' + worksheetName + '"><Table>';
            var rowXML = '<Column ss:AutoFitWidth="1" ss:Width="450" /><Column ss:AutoFitWidth="1" ss:Width="450" />';
            
            for(j=0; j<stores[i].length; j++){
                rowXML += '<Row>';
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].name + "</Data></Cell>";
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].state + "</Data></Cell>";
                rowXML += '<Cell><Data ss:Type="String">' + stores[i][j].occupation + "</Data></Cell>";
                rowXML += '</Row>';     
            }
            wrkbookXML += rowXML;
            wrkbookXML += '</Table></Worksheet>';    
        }
        return wrkbookXML;
    }

    $scope.exportTOExcel1 = function(){

        var data = [{"Vehicle":"BMW","Date":"30, Jul 2013 09:24 AM","Location":"Hauz Khas, Enclave, New Delhi, Delhi, India","Speed":42},
        {"Vehicle":"Honda CBR","Date":"30, Jul 2013 12:00 AM","Location":"Military Road,  West Bengal 734013,  India","Speed":0},
        {"Vehicle":"Supra","Date":"30, Jul 2013 07:53 AM","Location":"Sec-45, St. Angel's School, Gurgaon, Haryana, India","Speed":58},
        {"Vehicle":"Land Cruiser","Date":"30, Jul 2013 09:35 AM","Location":"DLF Phase I, Marble Market, Gurgaon, Haryana, India","Speed":83},
        {"Vehicle":"Suzuki Swift","Date":"30, Jul 2013 12:02 AM","Location":"Behind Central Bank RO, Ram Krishna Rd by-lane, Siliguri, West Bengal, India","Speed":0},
        {"Vehicle":"Honda Civic","Date":"30, Jul 2013 12:00 AM","Location":"Behind Central Bank RO, Ram Krishna Rd by-lane, Siliguri, West Bengal, India","Speed":0},
        {"Vehicle":"Honda Accord","Date":"30, Jul 2013 11:05 AM","Location":"DLF Phase IV, Super Mart 1, Gurgaon, Haryana, India","Speed":71}];
        JSONToCSVConvertor(data, "Vehicle Report", true);
    }

    function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
        //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
        var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
        
        var CSV = '';    
        //Set Report title in first row or line
        
        CSV += ReportTitle + '\r\n\n';
    
        //This condition will generate the Label/Header
        if (ShowLabel) {
            var row = "";
            
            //This loop will extract the label from 1st index of on array
            for (var index in arrData[0]) {
                
                //Now convert each value to string and comma-seprated
                row += index + ',';
            }
    
            row = row.slice(0, -1);
            
            //append Label row with line break
            CSV += row + '\r\n';
        }
        
        //1st loop is to extract each row
        for (var i = 0; i < arrData.length; i++) {
            var row = "";
            
            //2nd loop will extract each column and convert it in string comma-seprated
            for (var index in arrData[i]) {
                row += '"' + arrData[i][index] + '",';
            }
    
            row.slice(0, row.length - 1);
            
            //add a line break after each row
            CSV += row + '\r\n';
        }
    
        if (CSV == '') {        
            alert("Invalid data");
            return;
        }   
        
        //Generate a file name
        var fileName = "MyReport_";
        //this will remove the blank-spaces from the title and replace it with an underscore
        fileName += ReportTitle.replace(/ /g,"_");   
        
        //Initialize file format you want csv or xls
        var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
        
        // Now the little tricky part.
        // you can use either>> window.open(uri);
        // but this will not work in some browsers
        // or you will not get the correct file extension    
        
        //this trick will generate a temp <a /> tag
        var link = document.createElement("a");    
        link.href = uri;
        
        //set the visibility hidden so it will not effect on your web-layout
        link.style = "visibility:hidden";
        link.download = fileName + ".csv";
        
        //this part will append the anchor tag and remove it after automatic click
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
});